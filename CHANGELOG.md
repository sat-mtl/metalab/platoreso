# Changelog

* 2016-12-19 - 1.10.11
    - Automated build, test + deploy
    
* 2016-12-05 - 1.10.10
    - Adjustments for mobile browsers

* 2016-10-27 - 1.10.9
    - Quickfix about markdown links with no attributes crashing the board
    
* 2016-10-26 - 1.10.8
    - Fixed various bugs and annoyances for SERI
        + Removed edit option from message moderation
        + Fixed duplicate jsx attribute that broke IE
        + Card stat icons are now clickable to open the card
        + Markdown links now open in _blank

* 2016-10-14 - 1.10.7
    - Fixed attachment removal when removing a message
    
* 2016-10-13 - 1.10.6
    - Added possibility to clear group chat
    - Added possibility to directly moderate message from conversation
    - Various fixes
    
* 2016-09-28 - 1.10.5
    - Fix for twitter trying to open without a hashtag to watch
    
* 2016-09-27 - 1.10.4
    - Reinforced group and project name schema

* 2016-09-26 - 1.10.3
    - Group Editor bugfix
    
* 2016-09-26 - 1.10.2
    - Official terms/privacy docs
    
* 2016-09-23 - 1.10.1

* 2016-09-22 - 1.10.0
    - Added navigation links in full page card
    - Fixed project/group creation and edition forms
    - Show both side of the links in cared details
    - Added possibility to like comments
    - Added group conversations
    - Added browser notifications
    - Added terms of service

* 2016-09-13 - 1.9.4
    - Fixed project creation form not getting its groups subscription
    - Fixed removal of links created prior to the new link system
    
* 2016-09-13 - 1.9.3
    - Bugfix that prevented creating cards
    
* 2016-09-13 - 1.9.2
    - Bugfix when clicking card comment feed
    
* 2016-09-13 - 1.9.1
    - Bugfix in FeedHooks
    
* 2016-09-13 - 1.9.0
    - Added group & project removal
    - Added card excerpts to save on bandwidth
    - Added project temperature for trending & top projects
    - Fixed moderation functionality

* 2016-08-30 - 1.8.1
    - Fix for invitation code being undefined when creating a group
    
* 2016-08-30 - 1.8.0
    - Group Invitation Codes
    - Card pinning to top of phases
    - Added recent projects in projects menu
    - Added code formatting with markdown
    - Phase archiving
    - Other bugfixes
    
* 2016-08-17 - 1.7.0
    - Group Invitation Codes
    - Added link type to feed and notifications
    - Added sorting options based on popularity (was forgotten when did the trending sorting)
    - Fix for emailNotifications of new users

* 2016-08-03 - 1.6.1
    - Fixed linked cards display issue
    - Fixed card image removal issue

* 2016-08-02 - 1.6.0
    - New link types are now possible
    - Card analytics shows trending and most popular cards
    - Gamification show most active users by projects

* 2016-05-10 - 1.5.1
    - Fixed relationship display in the graph

* 2016-04-15 - 1.5.0
    - Added Markdown + emoji support
    - Added card contextual menu on the board
    - Added email notifications
    - Added card history on the graph
    - Added options to unlock nodes from the force layout when dragged
    - Fixed horizontal scroll in board

* 2016-04-06 - 1.4.3
    - Fixed notification dropdown not showing up when logged in state was late
    - Fixed removal of a user from a group
    - Fixed group creation from admin
    - Fixed bad fix for circular deps in project schema...
    
* 2016-04-06 - 1.4.2
    - Fixed card creation from tweets
    
* 2016-04-05 - 1.4.1
    - Fixed project hashtag not being saved

* 2016-04-01 - 1.4.0
    - Updated to Meteor 1.3 official release
    - Self Service
    - Cards can be unlinked
    - New graph options: 
        - radial & cluster (legacy layout)
        - hover to preview card
        - click to highlight connections
        - double-click to view card
    - Fixed graph tooltip blocking mouse
    - Fixed timeline labels misalignment
    - Fixed a bug preventing the clearing of form inputs
    - Fixed a bug preventing users from adding attachments to an existing card

* 2016-03-29 - 1.3.3
    - Updated to Meteor 1.3-rc?
    - Fixed bug preventing a user from clearing a field in the card editor
    
* 2016-03-29 - 1.3.2
    - Updated to Meteor 1.3-rc?
    - Fixed bug preventing a user from adding files to a card that is already created
    
* 2016-03-18 - 1.3.1
    - Updated to Meteor 1.3-rc?
    - Fixed graph working only on the first visit (removed log.debug = null from destroy() ???)

* 2016-03-18 - 1.3.0
    - Updated to Meteor 1.3-beta8
    - Card Versioning
    - Minor UI stuff

* 2016-03-08 - 1.2.2
    - Layout Fixes
    
* 2016-03-03 - 1.2.0
    - New Graph
    - Phase visits
    - Card Versioning
    - Fixed Moderation permissions

* 2016-02-25 - 1.1.2
    - New feed layout
    - Fix for empty groups and projects
    - Added touch scroll for mobile devices
    - Better color palette
    - Retina images in header and homepage
    - Warning message when not on prod
    - Smaller card linking drop target and dropping on phase defaults to move
    - UI Fixes

* 2016-02-22 - 1.1.1
    - added tags
    - added notifications
    - added file uploads
    - added phase questions required field
    - added scrolling when dragging cards in phases
    - enhanced card preview rendering
    - fixed scrolling issues with modals
    - ui improvements all around
    
Also:
    - translations
    - search
    - ghosts
    

* 2016-01-?? - 1.0.0-alpha2
    - Second version deployed to dev.platoreso.sat.qc.ca
    - New Features
        + Graph
        
* 2016-01-?? - 1.0.0-alpha1
    - First version deployed to dev.platoreso.sat.qc.ca
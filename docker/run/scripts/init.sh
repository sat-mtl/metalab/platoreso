#!/usr/bin/env bash
set -e

# Install nodejs
apt-get update -y && apt-get install -y --no-install-recommends curl
bash $INIT_DIR/install_node.sh
apt-get remove --purge -y curl

# Install Run dependencies
bash $INIT_DIR/install_run_deps.sh

# General cleanup
bash $INIT_DIR/cleanup.sh
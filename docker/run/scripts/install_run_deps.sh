#!/usr/bin/env bash
set -e

apt-get update -y && apt-get install -y --no-install-recommends \
    graphicsmagick \
    libcairo2 \
    libpangocairo-1.0 \
    libjpeg62-turbo \
    libgif4
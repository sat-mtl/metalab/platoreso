#!/usr/bin/env bash
set -e

NODE_VERSION=4.6.2
NODE_ARCH=x64
NODE_DIST=node-v${NODE_VERSION}-linux-${NODE_ARCH}

curl http://nodejs.org/dist/v${NODE_VERSION}/${NODE_DIST}.tar.gz | tar xvz

rm -rf /opt/nodejs
mv ${NODE_DIST} /opt/nodejs

ln -sf /opt/nodejs/bin/node /usr/bin/node
ln -sf /opt/nodejs/bin/npm /usr/bin/npm
#!/usr/bin/env bash

sed -i -r 's/spyOn/sandbox.stub/g' $1
sed -i -r 's/jasmine\.createSpy\(\ "[a-zA-Z0-9]*" \)/sinon.stub()/g' $1
sed -i -r "s/jasmine\.createSpy\(\ '[a-zA-Z0-9]*' \)/sinon.stub()/g" $1
sed -i -r 's/and\.returnValue/returns/g' $1
sed -i -r 's/toThrow/to.throw/g' $1
sed -i -r 's/throwError/throw/g' $1
sed -i -r 's/not\.to\.throw/to.not.throw/g' $1
sed -i -r 's/expect\(\ (.*)\ \)\.toHaveBeenCalledWith(.*)\;/assert( \1.calledWith\2 );/g' $1
sed -i -r 's/expect\(\ (.*)\ \)\.toHaveBeenCalledWith(.*)/assert( \1.calledWith\2/g' $1
sed -i -r 's/expect\(\ (.*)\ \)\.not\.toHaveBeenCalledWith\(\)\;/assert.isFalse( \1.called );/g' $1
sed -i -r 's/expect\(\ (.*)\ \)\.not\.toHaveBeenCalledWith\(\)\;/assert.isFalse( \1.called );/g' $1
sed -i -r 's/expect\(\ (.*)\ \)\.not\.toHaveBeenCalled\(\)\;/assert.isFalse( \1.called );/g' $1
sed -i -r 's/calls\.count\(\)/callCount/g' $1
sed -i -r 's/calls\.argsFor\(\ ([0-9])\ \)/getCall(\1).args/g' $1
sed -i -r 's/toEqual/to.eql/g' $1
sed -i -r 's/toBeUndefined\(\)/to.be.undefined/g' $1
sed -i -r 's/toBe\(\ true\ \)/to.be.true/g' $1
sed -i -r 's/toBe\(\ false\ \)/to.be.false/g' $1
sed -i -r 's/toBe\(\ ([a-zA-Z0-9])*\ \)/to.equal(\1)/g' $1
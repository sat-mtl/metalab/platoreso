# Guide reading notes

* IN PROGRESS > Create a style guide application to test/skin reusable components ;) http://guide.meteor.com/ui-ux.html#styleguides



* use debounce or throttle for quick updates that need throttling 
* base something off https://github.com/okgrow/analytics/ for analytics
* Put denormalization in collection hooks
* Because of DDP's mergebox it is better to normalize data as deep fields are not dealt with
    - so we should flatten user data out of profile
        
            // Deny all client-side updates to user documents
            Meteor.users.deny({
              update() { return true; }
            });
    
* Migrations should probably live in a seperate application dedicated for migrations
* dburles:collection-helpers simpler transform management
* Best methods https://atmospherejs.com/mdg/validated-method
* Rate limit methods

        // Get list of all Method names on Lists
        const LISTS_METHODS = _.pluck(Lists.methods, 'name');
        
        // Only allow 5 list operations per connection per second
        DDPRateLimiter.addRule({
          name(name) {
            return _.contains(LISTS_METHODS, name);
          },
        
          // Rate limit per connection ID
          connectionId() { return true; }
        }, 5, 1000);
        
* Use a combination of methods and local collections for computation intensive things (only update some stats once in a while for example)
* Clever us of publishComposite

        Meteor.publishComposite('Todos.admin.inList', function(listId) {
          new SimpleSchema({
            listId: {type: String}
          }).validate({ listId });
        
          const userId = this.userId;
          return {
            find() {
              return Meteor.users.find({userId, admin: true});
            },
            children: [{
              find() {
                // We don't need to worry about the list.userId changing this time
                return [
                  Lists.find(listId),
                  Todos.find({listId})
                ];
              }  
            }]
          };
        });
    
* REST publication simple:rest
* DDP Publication of a REST endpoint:

        const POLL_INTERVAL = 5000;
        
        Meteor.publish('polled-publication', function() {
          const publishedKeys = {};
        
          const poll = () => {
            // Let's assume the data comes back as an array of JSON documents, with an _id field, for simplicity
            const data = HTTP.get(REST_URL, REST_OPTIONS);
        
            data.forEach((doc) => {
              if (publishedKeys[doc._id]) {
                this.changed(COLLECTION_NAME, doc._id, doc);
              } else {
                publishedKeys[doc._id] = true;
                if (publishedKeys[doc._id]) {
                  this.added(COLLECTION_NAME, doc._id, doc);
                }
              }
            });
          };
        
          poll();
          this.ready();
        
          const interval = Meteor.setInterval(poll, POLL_INTERVAL);
        
          this.onStop(() => {
            Meteor.clearInterval(interval);
          });
        });
        
        
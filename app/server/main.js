"use strict";

//  #### ##     ## ########   #######  ########  ########  ######
//   ##  ###   ### ##     ## ##     ## ##     ##    ##    ##    ##
//   ##  #### #### ##     ## ##     ## ##     ##    ##    ##
//   ##  ## ### ## ########  ##     ## ########     ##     ######
//   ##  ##     ## ##        ##     ## ##   ##      ##          ##
//   ##  ##     ## ##        ##     ## ##    ##     ##    ##    ##
//  #### ##     ## ##         #######  ##     ##    ##     ######

// Welcome message ;)
import "/imports/welcome";

// Configuration
import "/imports/config/email";
import "/imports/config/cron";
import "/imports/config/migrations";
import "/imports/config/accounts";

// Packages
import "/imports/settings/server/main";
import "/imports/jobs/server/main";
import "/imports/mailer/server/main";
import "/imports/twitter/server/main";
import "/imports/accounts/server/main";
import "/imports/notifications/server/main";
import "/imports/feed/server/main";
import "/imports/moderation/server/main";
import "/imports/comments/server/main";
import "/imports/groups/server/main";
import "/imports/projects/server/main";
import "/imports/cards/server/main";
import "/imports/messages/server/main";

//   ######  ########    ###    ########  ######## ##     ## ########
//  ##    ##    ##      ## ##   ##     ##    ##    ##     ## ##     ##
//  ##          ##     ##   ##  ##     ##    ##    ##     ## ##     ##
//   ######     ##    ##     ## ########     ##    ##     ## ########
//        ##    ##    ######### ##   ##      ##    ##     ## ##
//  ##    ##    ##    ##     ## ##    ##     ##    ##     ## ##
//   ######     ##    ##     ## ##     ##    ##     #######  ##

Meteor.startup( () => {

    /**
     * Meteor rate-limits logins by default, but this behavior interferes with
     * development and testing, so disable it in non production environments.
     */
    if ( process.env.NODE_ENV != 'production' ) {
        Accounts.removeDefaultRateLimit();
    }

});
"use strict";

/**
 * Settings Model
 *
 * @param doc
 * @constructor
 */
export default class Settings {
    constructor( doc ) {
        _.extend( this, doc );
    }
}
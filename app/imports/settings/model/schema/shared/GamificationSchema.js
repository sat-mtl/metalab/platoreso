"use strict";

import { SimpleSchema } from "meteor/aldeed:simple-schema";

const CardGamificationPointsSchema = new SimpleSchema( {
    creation: {
        type:         Number,
        decimal:      true,
        min:          0,
        max:          100,
        defaultValue: 1,
        optional:     true
    },
    like:     {
        type:         Number,
        decimal:      true,
        min:          0,
        max:          100,
        defaultValue: 1,
        optional:     true
    },
    comment:  {
        type:         Number,
        decimal:      true,
        min:          0,
        max:          100,
        defaultValue: 1,
        optional:     true
    },
    link:     {
        type:         Number,
        decimal:      true,
        min:          0,
        max:          100,
        defaultValue: 1,
        optional:     true
    },
    move:     {
        type:         Number,
        decimal:      true,
        min:          0,
        max:          100,
        defaultValue: 1,
        optional:     true
    }
} );

const CommentGamificationPointsSchema = new SimpleSchema( {
    like:     {
        type:         Number,
        decimal:      true,
        min:          0,
        max:          100,
        defaultValue: 1,
        optional:     true
    }
} );

const GamificationPointsSchema = new SimpleSchema( {
    card:     {
        type:     CardGamificationPointsSchema,
        optional: true
    },
    comment:     {
        type:     CommentGamificationPointsSchema,
        optional: true
    }
} );

const GamificationSchema = new SimpleSchema( {
    points: {
        type:     GamificationPointsSchema,
        optional: true
    }
} );

export default GamificationSchema;
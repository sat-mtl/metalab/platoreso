"use strict";

import {SimpleSchema} from "meteor/aldeed:simple-schema";

const CardHeatSchema = new SimpleSchema( {
    creation:        {
        type:         Number,
        decimal:      true,
        min:          0,
        max:          100,
        defaultValue: 1,
        optional:     true
    },
    update:          {
        type:         Number,
        decimal:      true,
        min:          0,
        max:          100,
        defaultValue: 1,
        optional:     true
    },
    view:            {
        type:         Number,
        decimal:      true,
        min:          0,
        max:          100,
        defaultValue: 1,
        optional:     true
    },
    like:            {
        type:         Number,
        decimal:      true,
        min:          0,
        max:          100,
        defaultValue: 1,
        optional:     true
    },
    comment:         {
        type:         Number,
        decimal:      true,
        min:          0,
        max:          100,
        defaultValue: 1,
        optional:     true
    },
    linkSource:      {
        type:         Number,
        decimal:      true,
        min:          0,
        max:          100,
        defaultValue: 1,
        optional:     true
    },
    linkDestination: {
        type:         Number,
        decimal:      true,
        min:          0,
        max:          100,
        defaultValue: 1,
        optional:     true
    },
    move:            {
        type:         Number,
        decimal:      true,
        min:          0,
        max:          100,
        defaultValue: 1,
        optional:     true
    }
} );

const CardAnalyticsSchema = new SimpleSchema( {
    cooldown: {
        type:         Number,
        decimal:      true,
        min:          0,
        max:          100,
        defaultValue: 0.1,
        optional:     true
    },
    heat:     {
        type:     CardHeatSchema,
        optional: true
    }
} );


const ProjectAnalyticsSchema = new SimpleSchema( {
    cooldown: {
        type:         Number,
        decimal:      true,
        min:          0,
        max:          100,
        defaultValue: 0.1,
        optional:     true
    }
} );

const AnalyticsSchema = new SimpleSchema( {
    card:    {
        type:     CardAnalyticsSchema,
        optional: true
    },
    project: {
        type:     ProjectAnalyticsSchema,
        optional: true
    }
} );

export default AnalyticsSchema;
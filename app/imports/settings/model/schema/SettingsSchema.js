"use strict";

import {SimpleSchema} from "meteor/aldeed:simple-schema";
import SharedSettingsSchema from "./SharedSettingsSchema";
import ServerSettingsSchema from "./ServerSettingsSchema";
import ClientSettingsSchema from "./ClientSettingsSchema";

const SettingsSchema = new SimpleSchema( {
    shared: {
        type:         SharedSettingsSchema,
        defaultValue: {},
        optional:     true
    },
    server: {
        type:         ServerSettingsSchema,
        defaultValue: {},
        optional:     true
    },
    client: {
        type:         ClientSettingsSchema,
        defaultValue: {},
        optional:     true
    }
} );

export default SettingsSchema;
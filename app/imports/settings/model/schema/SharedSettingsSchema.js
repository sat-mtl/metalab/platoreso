"use strict";

import { SimpleSchema } from "meteor/aldeed:simple-schema";
import AnalyticsSchema from "./shared/AnalyticsSchema";
import GamificationSchema from "./shared/GamificationSchema";

const SharedSettingsSchema = new SimpleSchema({
    environment: {
        type: String,
        optional: true
    },
    analytics: {
        type: AnalyticsSchema,
        optional: true
    },
    gamification: {
        type: GamificationSchema,
        optional: true
    }
});

export default SharedSettingsSchema;
"use strict";

import logger from "meteor/metalab:logger/Logger";
const log = logger('Settings');

import SettingsCollection from "../SettingsCollection";

Meteor.startup(() => {
    log.info("Subscribing to settings");
    // Automatically subscribe to settings, they should always be available
    Meteor.subscribe('settings');
});
"use strict";

import Logger from "meteor/metalab:logger/Logger";
import AccountsMeldHelper from "/imports/accounts/server/AccountsMeldHelper";
import SettingsCollection from "../SettingsCollection";

const log = Logger( "settings-meld" );

/**
 * Settings Melding
 */
AccountsMeldHelper.onMeld( ( src_user_id, dst_user_id ) => {
    log.debug( `Melding settings for user ${src_user_id} into ${dst_user_id}` );

    SettingsCollection.update( { 'createdBy': src_user_id }, { $set: { 'createdBy': dst_user_id } }, { multi: true } );
    SettingsCollection.update( { 'updatedBy': src_user_id }, { $set: { 'updatedBy': dst_user_id } }, { multi: true } );
} );
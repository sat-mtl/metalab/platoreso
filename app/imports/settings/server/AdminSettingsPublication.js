"use strict";

import logger from "meteor/metalab:logger/Logger";

import SettingsCollection from "../SettingsCollection";
import UserHelpers from "/imports/accounts/UserHelpers";

const log = logger("Settings");

/**
 * Settings (Public)
 */
Meteor.publish( "admin/settings", function ( ) {
    if ( !UserHelpers.isAdmin( this.userId ) ) {
        return this.ready();
    }

    return SettingsCollection.find({}, { limit: 1 });
} );
"use strict";

import logger from "meteor/metalab:logger/Logger";

import SettingsCollection from "../SettingsCollection";

const log = logger("Settings");

/**
 * Settings (Public)
 */
Meteor.publish( "settings", function ( ) {
    log.silly("[Publication] [settings]");
    return SettingsCollection.find({}, { fields: { client: 1, shared: 1 }, limit: 1 });
} );
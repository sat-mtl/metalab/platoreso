"use strict";

import logger from "meteor/metalab:logger/Logger";
const log = logger('Settings');

// Implied
import "./SettingsPublication";
import "./AdminSettingsPublication";
import "./SettingsMeld";

// Startup
import SettingsCollection from "../SettingsCollection";

// Set or update collection
Meteor.startup( () => {
    log.info("Initializing settings");

    // Find current settings and create it if we have none set yet
    const settings = SettingsCollection.findOne();
    if ( !settings ) {
        log.info("No settings found, creating initial entry");
        SettingsCollection.insert( {
            shared: {
                environment: Meteor.settings.environment
            }
        } );
    } else {
        SettingsCollection.update({_id: settings._id}, {
            $set: {
                'shared.environment': Meteor.settings.environment
            }
        })
    }
} );
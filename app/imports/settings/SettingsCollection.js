"use strict";

import SettingsSchema from "./model/schema/SettingsSchema";
import Settings from "./model/Settings";

class SettingsCollection extends Mongo.Collection {
    constructor(name, options = {}) {
        // Change transform before calling super, this is the only way to set it
        // Also, this won't suffer from module loading as it is a callback
        options.transform = settings => new Settings( settings );

        super(name, options);

        /**
         * Schema
         */
        this.attachSchema( SettingsSchema );

        /**
         * Behaviors
         */

        this.attachBehaviour( 'timestampable' );

        /**
         * Security
         */

        this.deny({
            insert: () => true,
            update: () => true,
            remove: () => true
        });
    }
}

export default new SettingsCollection("settings");
"use strict";

/**
 * "Global" Settings Object
 * It is synced with the database to it is always updated
 * @type {{}}
 */
const Settings = {};

export default Settings;
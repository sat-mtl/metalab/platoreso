"use strict";

import logger from "meteor/metalab:logger/Logger";

// COLLECTIONS
import SettingsCollection from "./SettingsCollection";

// METHODS
import './SettingsMethods';

// SETTINGS
import Settings from "./Settings";

Meteor.startup( () => {

    const log = logger( 'Settings' );

    /**
     * Sync settings to our "global" settings object
     * @param settings
     */
    function setSettings( settings ) {
        log.info( 'New settings available' );
        _.extend( Settings, settings );
    }

    log.info( 'Observing changes' );
    SettingsCollection.find( {} ).observe( {
        added( settings ) {
            setSettings( settings );
        },
        changed( settings ) {
            setSettings( settings );
        }
    } );

} );
"use strict";

import flatten from 'flat';

import Logger from "meteor/metalab:logger/Logger";
import SettingsSchema from './model/schema/SettingsSchema';
import SettingsCollection from './SettingsCollection';
import UserHelpers from "/imports/accounts/UserHelpers";

const log = Logger( "settings-methods" );

const SettingsMethods = {

    /**
     * Update settings
     *
     * @param {object} info settings update information
     */
    update: new ValidatedMethod( {
        name:     'pr/settings/update',
        validate: SettingsSchema.validator( { clean: true, removeEmptyStrings: false, getAutoValues: false } ),
        run( settings ) {

            // SECURITY

            if ( !UserHelpers.isAdmin( this.userId ) ) {
                throw new Meteor.Error( 'unauthorized' );
            }

            // UPDATE

            if ( !_.isEmpty( settings ) ) {
                log.debug( "update - Updating settings", settings );
                const modifier = { $set: flatten( settings, { safe: true } ) };
                SettingsCollection.update( {  }, modifier );
            }
        }
    } ),

};

export default SettingsMethods;
"use strict";

import ProjectCollection from "./ProjectCollection";

export default class ProjectHelpers {
    
    /**
     * Checks if a phase is active
     *
     * @param projectId
     * @param phaseId
     * @returns {*}
     */
    static phaseIsActive( projectId, phaseId ) {
        // Get project and phases with the fields required for the getter
        const project = ProjectCollection.findOne( {
            _id:          projectId,
            'phases._id': phaseId
        }, {
            fields: {
                //'phases': { $elemMatch: { _id: phaseId } }, // Minimongo doesn't support operators in projections yet.
                'phases._id':       1,
                'phases.startDate': 1,
                'phases.endDate':   1
            }
        } );

        // No project, too bad
        if ( !project ) {
            return false;
        }

        // No phases, too bad
        if ( !project.phases ) {
            return false;
        }

        // Find phase
        const phase = project.phases.find( phase => phase._id == phaseId );
        if ( !phase ) {
            return false;
        }

        // Deny if not active
        return phase.isActive;
    }
}
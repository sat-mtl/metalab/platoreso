"use strict";

import {chai, assert, expect} from 'meteor/practicalmeteor:chai';
import {sinon, spies, stubs} from 'meteor/practicalmeteor:sinon';

import ProjectCollection from "/imports/projects/ProjectCollection";
import ProjectHelpers from "/imports/projects/ProjectHelpers";

describe( 'projects/ProjectHelpers', function () {

    let sandbox;

    beforeEach( () => {
        sandbox = sinon.sandbox.create();
    } );

    afterEach( () => {
        sandbox.restore();
    } );
    
    describe( 'phaseIsActive', function () {

        beforeEach( function () {
            sandbox.stub( ProjectCollection, 'findOne' );
        } );

        it( 'should determine if a phase is active', function () {
            ProjectCollection.findOne.returns( {
                phases: [{
                    _id:      'phaseId',
                    isActive: true // Fake getter
                }]
            } );
            expect( ProjectHelpers.phaseIsActive( 'projectId', 'phaseId' ) ).to.be.true;
        } );

        it( 'should determine if a phase is not active', function () {
            ProjectCollection.findOne.returns( {
                phases: [{
                    _id:      'phaseId',
                    isActive: false // Fake getter
                }]
            } );
            expect( ProjectHelpers.phaseIsActive( 'projectId', 'phaseId' ) ).to.be.false;
        } );

        it( 'should return false if the project is not found', function () {
            ProjectCollection.findOne.returns( null );
            expect( ProjectHelpers.phaseIsActive( 'projectId', 'phaseId' ) ).to.be.false;
        } );

        it( 'should return false if the project has no phases', function () {
            ProjectCollection.findOne.returns( { name: 'project' } );
            expect( ProjectHelpers.phaseIsActive( 'projectId', 'phaseId' ) ).to.be.false;
        } );

        it( 'should return false if the phase is not found', function () {
            ProjectCollection.findOne.returns( {
                phases: [{
                    _id: 'anotherId'
                }]
            } );
            expect( ProjectHelpers.phaseIsActive( 'projectId', 'phaseId' ) ).to.be.false;
        } );

    } );

} );
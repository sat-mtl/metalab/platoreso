"use strict";

import GroupHelpers from "/imports/groups/GroupHelpers";
import GroupSecurity from "/imports/groups/GroupSecurity";
import UserHelpers from "/imports/accounts/UserHelpers";

export default class ProjectPublicationHelpers {

    /**
     * Get the project list query
     *
     * @param {String} userId Logged user
     * @param {String} [groupId] Group Id
     * @returns {Object|null}
     */
    static getProjectListQuery( userId, groupId = null ) {
        let query = { moderated: false };

        if ( groupId ) {
            // We want a specific group, so filter by group permission
            const { allowed, member } = GroupSecurity.getUserAccess( userId, groupId );
            if ( !allowed ) {
                return null;
            }
            query.groups = groupId;
            if ( !member ) {
                query.public = true;
            }

        } else if ( !UserHelpers.isExpert( userId ) ) {
            // Not a global expert and no specific group asked, so filter by group permissions or public projects
            query.$or = [{ public: true }];
            if ( userId ) {
                const userGroups = GroupHelpers.getGroupsForUser( userId );
                query.$or.push( { groups: { $in: userGroups } } );
            }
        }

        return query;
    }

}
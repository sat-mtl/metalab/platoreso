"use strict";

import GroupCollection from "/imports/groups/GroupCollection";
import GroupImagesCollection from "/imports/groups/GroupImagesCollection";
import ProjectCollection from "../../ProjectCollection";
import ProjectImagesCollection from "../../ProjectImagesCollection";
import ProjectSecurity from "../../ProjectSecurity";
import CollectionHelpers from "/imports/collections/CollectionHelpers";

/**
 * Project (Public)
 */
Meteor.publishComposite( "project", function ( projectId ) {
    check( projectId, String );

    // We accept both _id and slug for subscriptions so we need to get the real _id here
    // We prefetch what is needed for ProjectSecurity.canRead() to prevent multiple calls to the DB
    // We also disable transforms as Meteor's check() doesn't like constructor objects
    const project = ProjectCollection.findOne(
        { moderated: false, $or: [{ _id: projectId }, { slug: projectId }] },
        { fields: { _id: 1, public: 1, groups: 1 }, transform: null }
    );
    if ( !project ) {
        return this.ready();
    }

    // Check with the project instead of the _id to prevent another fetch from the db
    if ( !ProjectSecurity.canRead( this.userId, project ) ) {
        return this.ready();
    }

    return {
        find() {
            return ProjectCollection.find( { _id: project._id, moderated: false }, {
                fields: CollectionHelpers.getDefaultFields(
                    ProjectCollection,
                    this.userId,
                    {
                        _id:              1,
                        slug:             1,
                        name:             1,
                        description:      1,
                        showTimeline:     1,
                        phases:           1,
                        content:          1,
                        groups:           1,
                        hashtag:          1,
                        public:           1,
                        lastTemperature:  1,
                        totalTemperature: 1,
                        heatedAt:         1,
                        createdAt:        1,
                        updatedAt:        1
                    }
                )
            } );
        },
        children: [
            {
                find( project ) {
                    return ProjectImagesCollection.find( { owners: project._id } );
                }
            },
            {
                find( project ) {
                    return GroupCollection.find( { _id: { $in: project.groups } }, {
                        fields: {
                            _id:         1,
                            slug:        1,
                            name:        1,
                            description: 1
                        }
                    } );
                },
                children: [
                    {
                        find( group ) {
                            return GroupImagesCollection.find( { owners: group._id } );
                        }
                    }
                ]
            }
        ]
    };
} );
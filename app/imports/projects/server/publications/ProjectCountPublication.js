"use strict";

import ProjectCollection from "../../ProjectCollection";
import ProjectPublicationHelpers from "../../server/ProjectPublicationHelpers";

 import { Counts } from "meteor/tmeasday:publish-counts";

/**
 * Group Project Count (Public)
 */
Meteor.publish( "project/count", function ( groupId = null ) {
    check( groupId, Match.OneOf(String, null) );

    const query = ProjectPublicationHelpers.getProjectListQuery( this.userId, groupId );
    if ( !query ) {
        return this.ready();
    }

    Counts.publish(
        this,
        'project/count' + ( groupId ? '/' + groupId : '' ),
        ProjectCollection.find( query, { fields: { _id: 1 } } )
    );
} );
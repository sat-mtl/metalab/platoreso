"use strict";

import GroupCollection from "/imports/groups/GroupCollection";
import ProjectCollection from "../../ProjectCollection";
import ProjectSecurity from "../../ProjectSecurity";

/**
 * Group List for Project Edition
 * It returns all the groups that the user can manage/associate the project to.
 *
 * The passed projectId is used to at least include groups that the project is linked to.
 * Otherwise, a user with only access to certain groups will not be able to view at least
 * the groups the project is currently linked to.
 *
 * @param projectIOd
 */
Meteor.publishComposite( "project/edit/group/list", function ( projectId ) {
    check( projectId, Match.OneOf( String, null ) );

    if ( projectId ) {
        return {
            find() {
                if ( !ProjectSecurity.canModerate( this.userId, projectId ) ) {
                    return this.ready();
                }

                return ProjectCollection.find( { _id: projectId }, { fields: { groups: 1 } } );
            },
            children: [
                {
                    find( project ) {
                        return GroupCollection.findUserManagedGroups( this.userId, { _id: { $in: project.groups } } );
                    },
                    children: [
                        { find: group => group.findImage() }
                    ]
                }
            ]
        };
    } else {
        return {
            find() {
                return GroupCollection.findUserManagedGroups( this.userId );
            },
            children: [
                { find: group => group.findImage() }
            ]
        }
    }
} );
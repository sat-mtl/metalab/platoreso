"use strict";

import ProjectCollection from "../../ProjectCollection";
import ProjectSecurity from "../../ProjectSecurity";
import UserCollection from "../../../accounts/UserCollection";
import AvatarCollection from "../../../accounts/AvatarCollection";

/**
 * Project (Public)
 */
Meteor.publishComposite( "project/users/top", function ( projectId, limit = 5 ) {
    check( projectId, String );
    check( limit, Number );

    // We accept both _id and slug for subscriptions so we need to get the real _id here
    // We prefetch what is needed for ProjectSecurity.canRead() to prevent multiple calls to the DB
    // We also disable transforms as Meteor's check() doesn't like constructor objects
    const project = ProjectCollection.findOne(
        { moderated: false, $or: [{ _id: projectId }, { slug: projectId }] },
        { fields: { _id: 1, public: 1, groups: 1 }, transform: null }
    );
    if ( !project ) {
        return this.ready();
    }

    // Check with the project instead of the _id to prevent another fetch from the db
    if ( !ProjectSecurity.canRead( this.userId, project ) ) {
        return this.ready();
    }

    return {
        find() {
            return UserCollection.find( {
                [`pointsPerProject.${project._id}`]: { $exists: true }
            }, {
                sort:   { [`pointsPerProject.${project._id}`]: -1 },
                limit:  limit,
                fields: {
                    _id:                                 1,
                    [`pointsPerProject.${project._id}`]: 1,
                    'profile.firstName':                 1,
                    'profile.lastName':                  1,
                    'profile.avatarUrl':                 1
                }
            } );
        },
        children: [
            {
                find( user ) {
                    return AvatarCollection.find( { owners: user._id } );
                }
            }
        ]
    };
} );
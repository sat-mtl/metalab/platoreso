"use strict";

import ProjectCollection from "../../ProjectCollection";
import ProjectImagesCollection from "../../ProjectImagesCollection";

import UserHelpers from "/imports/accounts/UserHelpers";

/**
 * Project Edit (Administration)
 */
Meteor.publish( "admin/project/edit", function ( id ) {
    check( id, String );

    if ( !UserHelpers.isSuper( this.userId ) ) {
        return this.ready();
    }

    return [
        ProjectCollection.find( { _id: id } ),
        ProjectImagesCollection.find( { owners: id } )
    ];
} );
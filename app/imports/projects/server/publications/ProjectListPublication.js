"use strict";

import ProjectCollection from "../../ProjectCollection";
import ProjectImagesCollection from "../../ProjectImagesCollection";
import ProjectPublicationHelpers from "../../server/ProjectPublicationHelpers";

import CollectionHelpers from "/imports/collections/CollectionHelpers";

/**
 * Group Projects (Public)
 */
Meteor.publishComposite( "project/list", function ( groupId = null ) {
    check( groupId, Match.OneOf( String, null ) );

    const query = ProjectPublicationHelpers.getProjectListQuery( this.userId, groupId );
    if ( !query ) {
        return this.ready();
    }

    return {
        find() {
            return ProjectCollection.find(
                query,
                {
                    fields: CollectionHelpers.getDefaultFields(
                        ProjectCollection,
                        this.userId,
                        {
                            _id:              1,
                            slug:             1,
                            name:             1,
                            description:      1,
                            groups:           1,
                            public:           1,
                            lastTemperature:  1,
                            totalTemperature: 1,
                            heatedAt:         1,
                            createdAt:        1,
                            updatedAt:        1
                        }
                    ),
                    sort:   { updatedAt: -1 }
                } );
        },
        children: [
            {
                find( project ) {
                    return ProjectImagesCollection.find( { owners: project._id } );
                }
            }
        ]
    };
} );

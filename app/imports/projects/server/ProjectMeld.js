"use strict";

import Logger from "meteor/metalab:logger/Logger";
import AccountsMeldHelper from "/imports/accounts/server/AccountsMeldHelper";
import ProjectCollection from "../ProjectCollection";

const log = Logger("card-meld");

/**
 * Projects Melding
 */
AccountsMeldHelper.onMeld( (src_user_id, dst_user_id) => {
    log.debug(`Melding projects for user ${src_user_id} into ${dst_user_id}`);

    ProjectCollection.update( { 'createdBy': src_user_id }, { $set: { 'createdBy': dst_user_id } }, { multi: true } );
    ProjectCollection.update( { 'updatedBy': src_user_id }, { $set: { 'updatedBy': dst_user_id } }, { multi: true } );
});
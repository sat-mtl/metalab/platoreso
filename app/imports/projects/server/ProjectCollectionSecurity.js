"use strict";

import { RoleGroups } from "/imports/accounts/Roles";
import ProjectCollection from "../ProjectCollection";
import ProjectImagesCollection from "../ProjectImagesCollection";
import ProjectSecurity from "../ProjectSecurity";

/**
 * Deny if user cannot edit the image owner's group
 */
Security.defineMethod( "ifCanReadOwnerProject", {
    fetch:     [],
    transform: null,
    deny:      function ( type, arg, userId, doc, fields, modifier ) {
        return !_.some( doc.owners, owner => ProjectSecurity.canRead( userId, owner ) );
    }
} );

/**
 * Deny if user cannot edit the image owner's group
 */
Security.defineMethod( "ifCanEditOwnerProject", {
    fetch:     [],
    transform: null,
    deny:      function ( type, arg, userId, doc, fields, modifier ) {
        return !_.some( doc.owners, owner => ProjectSecurity.canEdit( userId, owner ) );
    }
} );

/**
 * Security Rules
 */

// Allow anyone with access to project to download images
Security.permit( ['download'] )
        .collections( [ProjectImagesCollection] )
        .ifCanReadOwnerProject()
        .allowInClientCode();

Security.permit( ['insert'] )
        .collections( [ProjectImagesCollection] )
        .ifLoggedIn()
        .ifHasRole( RoleGroups.experts )
        .allowInClientCode();

Security.permit( ['insert'] )
        .collections( [ProjectImagesCollection] )
        .ifLoggedIn()
        .ifCanEditOwnerProject()
        .allowInClientCode();

// MODERATION

Security.defineMethod( "ifCanModerateProject", {
    fetch:     [],
    transform: null,
    allow:     function ( type, arg, userId, doc, fields, modifier ) {
        return ProjectSecurity.canModerate( userId, doc );
    }
} );

ProjectCollection
    .permit( ['update'] )
    .onlyProps( ['moderated', 'reportCount', 'reportedBy'] )
    .ifLoggedIn()
    .ifCanModerateProject();
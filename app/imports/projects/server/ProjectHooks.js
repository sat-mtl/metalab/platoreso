"use strict";

import Logger from "meteor/metalab:logger/Logger";
import FeedCollection from "/imports/feed/FeedCollection";
import NotificationCollection from "/imports/notifications/NotificationCollection";
import UserCollection from "/imports/accounts/UserCollection";
import GroupCollection from "/imports/groups/GroupCollection";
import ProjectCollection from "../ProjectCollection";
import ProjectImagesCollection from "../ProjectImagesCollection";

const log = Logger( "project-hooks" );

/**
 * Group Removed
 */
GroupCollection.after.remove( function ( userId, group ) {
    // Defer, so that methods can return without waiting
    Meteor.defer( () => {
        log.debug( `Group "${group.name}" (${group._id}) removed, cleaning up relations` );

        // Remove projects that were only linked to that group
        ProjectCollection.remove( { groups: { $eq: group._id, $size: 1 } } );

        // Remove references to deleted groups in projects that are still linked to other groups
        ProjectCollection.update( { groups: group._id }, { $pull: { groups: group._id } }, { multi: true } );

    } );
} );

/**
 * Project Removed
 */
ProjectCollection.after.remove( function ( userId, project ) {
    // Defer, so that methods can return without waiting
    Meteor.defer( () => {
        log.debug( `Project "${project.name}" (${project._id}) removed, cleaning up relations` );

        // Remove associated image(s)
        ProjectImagesCollection.unlinkOwner( project._id );

        // Remove notifications
        NotificationCollection.unnotifyAllForObject( project._id, ProjectCollection.entityName );

        // Remove Stories
        FeedCollection.removeAllForObject( project._id, ProjectCollection.entityName );

        // Remove references in user profiles
        UserCollection.update( {}, {
            $pull:  { recentProjects: project._id },
            $unset: { [`pointsPerProject.${project._id}`]: null }
        }, { multi: true } );

    } );
} );
"use strict";

// GENERAL
import "./ProjectMeld";
import "./ProjectCollectionSecurity";
import "./ProjectHooks";

// PUBLICATIONS
import "./publications/AdminProjectEditPublication";
import "./publications/AdminProjectListPublication";
import "./publications/ProjectCountPublication";
import "./publications/ProjectListPublication";
import "./publications/ProjectPublication";
import "./publications/ProjectTopUsersPublication";
import "./publications/ProjectEditGroupListPublication";
import "../moderation/ProjectModerationReportListPublication";
import "../phases/QuestionResultsPublication";
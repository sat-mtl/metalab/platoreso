"use strict";

import PhaseService from "./PhaseService";

/**
 * Phase Types Registry
 */
export default class PhaseTypes {

    /**
     * Register a phase type
     *
     * @param {String} phaseType Name of the phase type
     * @param {PhaseService} service Instance of a PhaseService class
     */
    static register( phaseType, service ) {
        PhaseTypes.map[phaseType] = phaseType;
        PhaseTypes.serviceMap[phaseType] = service;
    }

    /**
     * List the phase types registered in the system
     *
     * @returns {Array<String>}
     */
    static get list() {
        return _.keys(PhaseTypes.serviceMap);
    }

    /**
     * Get the service class for a phase type
     *
     * @param {String} phaseType Name of the phase type
     * @returns {PhaseService} Service instance for the phase type
     */
    static get(phaseType) {
        return PhaseTypes.serviceMap[phaseType];
    }
}

PhaseTypes.map = {};
PhaseTypes.serviceMap = {};

// Register the basic phase type
PhaseTypes.register('basic', new PhaseService());
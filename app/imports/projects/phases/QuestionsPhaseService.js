"use strict";

import ProjectCollection from "../ProjectCollection";
import PhaseService from "../phases/PhaseService";
import PhaseTypes from "../phases/PhaseTypes";

export default class QuestionsPhaseService extends PhaseService {

    /**
     * @inheritDoc
     */
    update( projectId, phaseInfo ) {
        super.update( projectId, phaseInfo );

        // Convert bad boolean values from questions (semantic ui returns 'on' for checkboxes)
        if ( phaseInfo.data && phaseInfo.data.questions ) {
            _.keys( phaseInfo.data.questions.questions ).forEach( key => {
                phaseInfo.data.questions.questions[key].required = !!phaseInfo.data.questions.questions[key].required;
            });
        }

        //TODO: Combine both into one update call

        /* QUESTION REMOVAL */
        if ( phaseInfo.removedQuestions && !_.isEmpty(phaseInfo.removedQuestions) && _.isArray(phaseInfo.removedQuestions) ) {
            // Reduce the question ids into an object for $unset
            const modifier = { $unset: phaseInfo.removedQuestions.reduce((unset, questionId) => {
                unset['phases.$.data.questions.questions.' + questionId] = null;
                return unset;
            }, {}) };

            Security.can( this.userId ).update( projectId, modifier ).for( ProjectCollection ).throw();

            ProjectCollection.update( { _id: projectId, 'phases._id': phaseInfo._id }, modifier );

            //TODO: Remove related information
        }

        /* ANSWER REMOVAL */
        if ( phaseInfo.removedAnswers && !_.isEmpty( phaseInfo.removedAnswers ) && _.isObject( phaseInfo.removedAnswers ) ) {
            // Reduce the question ids into an object for $unset
            const modifier = {
                $unset: Object.keys( phaseInfo.removedAnswers ).reduce( ( unset, questionId ) => {
                    const answers = phaseInfo.removedAnswers[questionId];
                    if ( !_.isEmpty( answers ) && _.isArray( answers ) ) {
                        answers.forEach( answerId=> {
                            unset['phases.$.data.questions.questions.' + questionId + '.answers.' + answerId] = null;
                        } )
                    }
                    return unset;
                }, {} )
            };

            Security.can( this.userId ).update( projectId, modifier ).for( ProjectCollection ).throw();

            ProjectCollection.update( { _id: projectId, 'phases._id': phaseInfo._id }, modifier );

            //TODO: Remove related information
        }
    }

}

// Register the question phase type
PhaseTypes.register('questions', new QuestionsPhaseService());
"use strict";

/**
 * Base Phase Service Class
 */
export default class PhaseService {

    /**
     * Update the custom phase info
     *
     * @param {String} projectId
     * @param {Object} phaseInfo Object from the form to update the phase with (should contain the phase _id)
     */
    update( projectId, phaseInfo ) {
        check( projectId, String );
        check( phaseInfo, Match.ObjectIncluding( { _id: String } ) );
    }

}
"use strict";

import ProjectCollection from "../ProjectCollection";
import ProjectSecurity from "../ProjectSecurity";
import CardCollection from "/imports/cards/CardCollection";
import {Counts} from "meteor/tmeasday:publish-counts";

/**
 * Project (Public)
 */
Meteor.publish( "question/results", function ( projectId, phaseId, questionId ) {
    check( projectId, String );
    check( phaseId, String );
    check( questionId, String );

    const project = ProjectCollection.findOne(
        { _id: projectId },
        { fields: { _id: 1, public: 1, groups: 1, phases: 1 }, transform: null }
    );
    if ( !project ) {
        return this.ready();
    }

    // Check with the project instead of the _id to prevent another fetch from the db
    if ( !ProjectSecurity.canRead( this.userId, project ) ) {
        return this.ready();
    }

    if ( !project.phases ) {
        return this.ready();
    }

    const phase = project.phases.find( phase => phase._id == phaseId );
    if ( !phase || !phase.data || !phase.data.questions || !phase.data.questions.questions ) {
        return this.ready();
    }

    const question = phase.data.questions.questions[questionId];
    if ( !question ) {
        return this.ready();
    }

    const answers = question.answers;

    _.each( answers, ( answer, answerId ) => {
        //FIXME: This can get nasty ram-wise
        Counts.publish( this,
            'question/results/' + projectId + '/' + phaseId + '/' + questionId + '/' + answerId,
            CardCollection.find( {
                current:                                                       true,
                moderated:                                                     false,
                ['phaseData.' + phaseId + '.questions.answers.' + questionId]: answerId
            } )
        );
    } );

} );
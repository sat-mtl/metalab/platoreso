"use strict";

import slugify from "underscore.string/slugify";
import { Counts } from "meteor/tmeasday:publish-counts";
import GroupHelpers from "/imports/groups/GroupHelpers";
import GroupSecurity from "/imports/groups/GroupSecurity";
import UserHelpers from "/imports/accounts/UserHelpers";
import { RoleGroups } from "/imports/accounts/Roles";
import ProjectCollection from "../ProjectCollection";
import ProjectImagesCollection from "../ProjectImagesCollection";

/**
 * Moderation Report List (Administration)
 *
 * @params {Object} [options] Options for the find operation (ex: sort)
 */
Meteor.publishComposite( "project/moderation/report/list", function ( query = null, options = null ) {

    // Check query for only the allowed fields
    check( query, Match.OneOf( null, {
        name:      Match.Optional( Match.ObjectIncluding( {$regex: String} ) ),
        moderated: Match.Optional( Boolean )
    } ) );

    // Check options for only the allowed fields
    check( options, Match.OneOf( null, {
        sort:  Match.Optional( Object ),
        limit: Match.Optional( Number ),
        skip:  Match.Optional( Number )
    } ) );

    if ( !GroupSecurity.canModerateGroups( this.userId ) ) {
        return this.ready();
    }

    const topLevelModerator = UserHelpers.isModerator( this.userId );

    query = query || {};

    // Keep the original (unmodified by permissions, to publish counts to a unique URI)
    const originalQuery = _.clone( query );

    options = _.extend( options || {}, {
        fields: {
            _id:         1,
            slug:        1,
            name:        1,
            moderated:   1,
            reportCount: 1
        }
    } );

    return {
        find() {
            if ( !topLevelModerator ) {
                // User is not a top-level moderator so filter projects by projects he/she has access to as a group moderator
                const userGroups = GroupHelpers.getGroupsForUser( this.userId, RoleGroups.moderators );
                query.groups     = {$in: userGroups};
            }

            // Slugify the json query as a unique identifier for the count
            Counts.publish( this,
                'project/moderation/report/count/' + slugify( JSON.stringify( originalQuery ) ),
                ProjectCollection.findReported( query, {fields: {_id: 1}} ),
                {noReady: true}
            );

            return ProjectCollection.findReported( query, options )
        },
        children: [
            {
                find( project ) {
                    return ProjectImagesCollection.find( {owners: project._id} );
                }
            }
        ]
    };
} );
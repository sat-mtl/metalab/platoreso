"use strict";

import flatten from "flat";
import Logger from "meteor/metalab:logger/Logger";
import { ValidatedMethod } from "meteor/mdg:validated-method";
import UserCollection from "/imports/accounts/UserCollection";
import GroupCollection from "/imports/groups/GroupCollection";
import GroupSecurity from "/imports/groups/GroupSecurity";
import ProjectSchema, { ProjectUpdateSchema } from "./model/schema/ProjectSchema";
import ProjectPhaseSchema, { ProjectPhaseUpdateSchema } from "./model/schema/ProjectPhaseSchema";
import ProjectCollection from "./ProjectCollection";
import ProjectSecurity from "./ProjectSecurity";
import PhaseTypes from "./phases/PhaseTypes";
import CardCollection from "../cards/CardCollection";
import CardMethods from "../cards/CardMethods";

const log = Logger( "project-methods" );

const ProjectMethods = {

    /**
     * Create a project
     *
     * @param  {object} info User update information
     * @returns {string} Group Id
     */
    create: new ValidatedMethod( {
        name:     "pr/project/create",
        validate: new SimpleSchema( {
            project: {
                type: ProjectSchema.pick( [
                    "name",
                    "description",
                    "content",
                    "hashtag",
                    "public",
                    "showTimeline",
                    "groups",
                    "groups.$"
                ] )
            }
        } ).validator( { clean: true } ),
        run( { project } ) {

            // SECURITY

            if ( !ProjectSecurity.canCreate( this.userId, project ) ) {
                log.warn( `create - ${this.userId} is not authorized to create project`, project );
                throw new Meteor.Error( 'unauthorized' );
            }

            // If groups are specified, user must have the right to link projects to all of them
            if ( project.groups && project.groups.length && !GroupSecurity.canManageGroups( this.userId, project.groups, false, true ) ) {
                log.warn( `create - ${this.userId} is not authorized to assign new project to selected groups`, project.groups );
                throw new Meteor.Error( 'unauthorized' );
            }

            // UPDATE

            log.verbose( "create - Creating project", project );
            const projectId = ProjectCollection.insert( project );

            // AUDIT

            /*Audit.next( {
             domain:  ProjectCollection.entityName,
             message: ProjectAuditMessages.added,
             userId:  this.userId,
             projectId:  projectId
             } );*/

            return projectId;
        }
    } ),

    /**
     * View a project
     * TODO: This should be throttled
     *
     * @param {string} projectId Project Id
     */
    view: new ValidatedMethod( {
        name:     'pr/project/view',
        validate: new SimpleSchema( {
            projectId: { type: String }
        } ).validator(),
        run( { projectId } ) {

            // Running on server only as the project might not be available for
            // checks at this point on the client
            if ( Meteor.isServer ) {

                // PRECONDITIONS

                const project = ProjectCollection.findOne(
                    {
                        $or: [
                            { _id: projectId },
                            { slug: projectId }
                        ]
                    },
                    {
                        fields:    {
                            _id:    1,
                            public: 1, // For security
                            groups: 1  // For security
                        },
                        transform: null
                    }
                );
                if ( !project ) {
                    throw new Meteor.Error( 'project not found' );
                }

                // SECURITY

                if ( !ProjectSecurity.canRead( this.userId, project ) ) {
                    throw new Meteor.Error( 'unauthorized' )
                }

                // UPDATE

                if ( this.userId ) {
                    const user = UserCollection.findOne( { _id: this.userId }, { fields: { recentProjects: 1 } } );
                    if ( user ) {
                        let recentProjects = user.recentProjects || [];
                        recentProjects.unshift( project._id );
                        recentProjects = _.uniq( recentProjects );
                        recentProjects.splice( 10 );
                        UserCollection.update( { _id: this.userId }, { $set: { recentProjects } } );
                    }
                }
            }
        }
    } ),

    /**
     * Update a project
     *
     * @param {object} info project update information
     */
    update: new ValidatedMethod( {
        name:     'pr/project/update',
        validate: new SimpleSchema( {
            _id:     {
                type: String
            },
            changes: {
                type: ProjectUpdateSchema
            }
        } ).validator( { clean: true, removeEmptyStrings: false, getAutoValues: false } ),
        run( { _id, changes } ) {

            // SECURITY

            if ( !ProjectSecurity.canUpdate( this.userId, _id ) ) {
                log.warn( `update - ${this.userId} is not authorized to update project`, changes );
                throw new Meteor.Error( 'unauthorized' );
            }

            // If groups are specified, user must have the right to link projects to all of them
            if ( changes.groups && !GroupSecurity.canManageGroups( this.userId, changes.groups, false, true ) ) {
                log.warn( `create - ${this.userId} is not authorized to assign project ${_id} to selected groups`, changes.groups );
                throw new Meteor.Error( 'unauthorized' );
            }

            const project = ProjectCollection.findOne( { _id }, {
                fields:    { _id: 1 },
                transform: null
            } );
            if ( !project ) {
                throw new Meteor.Error( 'project not found' );
            }

            // UPDATE

            // Updating the project
            if ( !_.isEmpty( changes ) ) {
                log.debug( "update - Updating project", changes );

                const modifier = { $set: flatten( changes, { safe: true } ) };

                // Do the actual project update
                ProjectCollection.update( { _id: project._id }, modifier );
            }

            // AUDIT

            /*Audit.next( {
             domain:  ProjectCollection.entityName,
             message: ProjectAuditMessages.updated,
             userId:  this.userId,
             projectId:  project._id
             } );*/
        }
    } ),

    /**
     * Remove a project
     *
     * @param {string} projectId Project Id
     */
    remove: new ValidatedMethod( {
        name:     'pr/project/remove',
        validate: new SimpleSchema( {
            projectId: { type: String }
        } ).validator( { clean: true } ),
        run( { projectId } ) {

            // SECURITY
            if ( !ProjectSecurity.canRemove( this.userId, projectId ) ) {
                log.warn( `remove - ${this.userId} is not authorized to remove project ${projectId}` );
                throw new Meteor.Error( 'unauthorized' );
            }

            // UPDATE
            log.verbose( `remove - ${projectId}` );
            ProjectCollection.remove( { _id: projectId } );

            // AUDIT
            /*Audit.next( {
             domain:  ProjectCollection.entityName,
             message: ProjectAuditMessages.removed,
             userId:  this.userId,
             projectId:  projectId
             } );*/
        }
    } ),

    /**
     * Add group to a project
     *
     * @param {string} projectId Project Id
     * @param {string} groupId Group Id
     */
    addGroup: new ValidatedMethod( {
        name:     'pr/project/addGroup',
        validate: new SimpleSchema( {
            projectId: { type: String },
            groupId:   { type: String }
        } ).validator( { clean: true } ),
        run( { projectId, groupId } ) {

            // SECURITY

            // Only allow logged in experts or project experts
            if ( !ProjectSecurity.canEdit( this.userId, projectId ) || !GroupSecurity.canManageGroups( this.userId, groupId ) ) {
                log.warn( `addGroup - ${this.userId} is not authorized to add group ${groupId} to project ${projectId}` );
                throw new Meteor.Error( 'unauthorized' );
            }

            // UPDATE

            const project = ProjectCollection.findOne( { _id: projectId }, { fields: { _id: 1 }, transform: null } );
            if ( !project ) {
                throw new Meteor.Error( 'project not found' );
            }

            const group = GroupCollection.findOne( { _id: groupId }, { fields: { _id: 1 }, transform: null } );
            if ( !group ) {
                throw new Meteor.Error( 'group not found' );
            }

            log.verbose( `addGroup - ${groupId} -> ${projectId}` );
            const modifier = { $addToSet: { groups: groupId } };
            ProjectCollection.update( { _id: projectId }, modifier );

            // AUDIT
            /*Audit.next( {
             domain:  ProjectCollection.entityName,
             message: ProjectAuditMessages.removed,
             groupId:  this.groupId,
             projectId:  projectId
             } );*/
        }
    } ),

    /**
     * Remove a group from a project
     *
     * @param {string} projectId Project Id
     * @param {string} groupId Group Id
     */
    removeGroup: new ValidatedMethod( {
        name:     'pr/project/removeGroup',
        validate: new SimpleSchema( {
            projectId: { type: String },
            groupId:   { type: String }
        } ).validator( { clean: true } ),
        run( { projectId, groupId } ) {

            // SECURITY

            // Only allow logged in experts or project experts
            if ( !ProjectSecurity.canEdit( this.userId, projectId ) || !GroupSecurity.canManageGroups( this.userId, groupId ) ) {
                log.warn( `removeGroup - ${this.userId} is not authorized to remove group ${groupId} from project ${projectId}` );
                throw new Meteor.Error( 'unauthorized' );
            }

            // UPDATE

            const project = ProjectCollection.findOne( { _id: projectId }, { fields: { _id: 1 }, transform: null } );
            if ( !project ) {
                throw new Meteor.Error( 'project not found' );
            }

            log.verbose( `removeGroup - ${groupId} X ${projectId}` );
            const modifier = { $pull: { groups: groupId } };
            ProjectCollection.update( { _id: projectId }, modifier );

            // AUDIT
            /*Audit.next( {
             domain:  ProjectCollection.entityName,
             message: ProjectAuditMessages.removed,
             groupId:  this.groupId,
             projectId:  projectId
             } );*/
        }
    } ),

    /**
     * Add a project phase
     *
     * @param {string} projectId
     * @param {object} phase phase information
     */
    addPhase: new ValidatedMethod( {
        name:     'pr/project/addPhase',
        validate: new SimpleSchema( {
            projectId: {
                type: String
            },
            phase:     {
                type: ProjectPhaseSchema
            }
        } ).validator( { clean: true } ),
        run( { projectId, phase } ) {

            // SECURITY

            // Only allow logged in experts or project experts
            if ( !ProjectSecurity.canEdit( this.userId, projectId ) ) {
                log.warn( `addPhase - ${this.userId} is not authorized to add a phase to project ${projectId}` );
                throw new Meteor.Error( 'unauthorized' );
            }

            // UPDATE

            const project = ProjectCollection.findOne( { _id: projectId }, { fields: { _id: 1 }, transform: null } );
            if ( !project ) {
                throw new Meteor.Error( 'project not found' );
            }

            if ( phase.startDate > phase.endDate ) {
                throw new Meteor.Error( 'phase has negative length' );
            }

            const modifier = { $push: { phases: phase } };
            ProjectCollection.update( { _id: projectId }, modifier );

            return phase;

            // AUDIT
        }
    } ),

    /**
     * Update a project phase
     *
     * @param {string} projectId
     * @param {object} phase phase update information
     */
    updatePhase: new ValidatedMethod( {
        name:     'pr/project/updatePhase',
        validate: new SimpleSchema( {
            projectId: {
                type: String
            },
            phase:     {
                type: ProjectPhaseUpdateSchema
            }
        } ).validator( { clean: true, removeEmptyStrings: false, getAutoValues: false } ),
        run( { projectId, phase } ) {

            // SECURITY

            // Only allow logged in experts or project experts
            if ( !ProjectSecurity.canEdit( this.userId, projectId ) ) {
                log.warn( `updatePhase - ${this.userId} is not authorized to update phases in project ${projectId}` );
                throw new Meteor.Error( 'unauthorized' );
            }

            // UPDATE

            // Retrieve the original project
            const project = ProjectCollection.findOne( { _id: projectId, 'phases._id': phase._id }, {
                fields: {
                    'phases._id':  1,
                    'phases.type': 1
                }
            } );
            if ( !project ) {
                throw new Meteor.Error( 'project or phase not found' );
            }

            if ( !project.phases ) {
                throw new Meteor.Error( 'phase not found' );
            }

            if ( phase.startDate > phase.endDate ) {
                throw new Meteor.Error( 'phase has negative length' );
            }

            // Retrieve the original phase
            const originalPhase = _.findWhere( project.phases, { _id: phase._id } );
            if ( !originalPhase ) {
                throw new Meteor.Error( 'phase not found' );
            }

            // PHASE TYPE UPDATE
            const phaseType = phase.type || originalPhase.type;
            if ( phaseType ) {
                const phaseTypeService = PhaseTypes.get( phaseType );
                if ( !phaseTypeService ) {
                    throw new Meteor.Error( 'invalid phase type' );
                }

                // Update the phase type specific info
                phaseTypeService.update.call( this, projectId, phase );
            }

            // PHASE UPDATE
            let modifier = { $set: {} };

            // Clean the form data to keep only the schema fields
            // This is done so that the extra fields we are using for deletion get removed
            ProjectPhaseSchema.clean( phase, { removeEmptyStrings: false, getAutoValues: false } );

            // Flatten keys for modifier, we need to prepend mongo's current phase index
            _.each( flatten( phase ), ( value, key ) => {
                modifier.$set['phases.$.' + key] = value
            } );

            // Update the phase first
            ProjectCollection.update( { _id: projectId, 'phases._id': phase._id }, modifier );

            // AUDIT
        }
    } ),

    /**
     * Remove an Phase from a project
     *
     * @param {string} projectId Project Id
     * @param {string} phaseId Phase Id
     */
    removePhase: new ValidatedMethod( {
        name:     'pr/project/removePhase',
        validate: new SimpleSchema( {
            projectId:   {
                type: String
            },
            phaseId:     {
                type: String
            },
            action:      {
                type:          String,
                allowedValues: ['archive', 'move']
            },
            moveToPhase: {
                type:     String,
                optional: true,
                custom:   function () {
                    var shouldBeRequired = this.field( 'action' ).value == 'move';
                    if ( shouldBeRequired && (!this.isSet || this.value === null || this.value === "") ) {
                        return "required";
                    }
                }
            }
        } ).validator( { clean: true } ),
        run( { projectId, phaseId, action, moveToPhase } ) {

            // SECURITY

            // Only allow logged in experts or project experts
            if ( !ProjectSecurity.canEdit( this.userId, projectId ) ) {
                log.warn( `removePhase - ${this.userId} is not authorized to remove phase ${phaseId} from project ${projectId}` );
                throw new Meteor.Error( 'unauthorized' );
            }

            // PREREQUISITES

            const project = ProjectCollection.findOne( { _id: projectId, 'phases._id': phaseId }, {
                fields:    { _id: 1 },
                transform: null
            } );
            if ( !project ) {
                throw new Meteor.Error( 'phase not found' );
            }

            // UPDATE

            ProjectCollection.update( {
                _id:          projectId,
                'phases._id': phaseId
            }, { $set: { 'phases.$.removed': true } } );

            //FIXME: THIS IS NOT AT ALL THE BEST WAY TO DO THIS
            if ( action == 'archive' ) {
                CardCollection.find(
                    { project: projectId, phase: phaseId, current: true },
                    { fields: { id: 1 }, transform: null }
                ).forEach( card => CardMethods.archive.call( { cardId: card.id } ) );
            } else if ( action == 'move' ) {
                CardCollection.find(
                    { project: projectId, phase: phaseId, current: true },
                    { fields: { id: 1 }, transform: null }
                ).forEach( card => CardMethods.moveToPhase.call( { cardId: card.id, phaseId: moveToPhase } ) );
            }
            // AUDIT
        }
    } )

    /**
     * Move (reorder) a phase
     *
     * @param {String} projectId
     * @param {String} phaseId
     * @param {Number} order
     */
    /*static movePhase( projectId, phaseId, order ) {
     check( projectId, String );
     check( phaseId, String );
     check( order, Match.Where( x => {
     check( x, Number );
     return x >= 0;
     } ) );

     const project = ProjectCollection.findOne( { _id: projectId }, {
     fields:    {
     'phases': 1 //{ $elemMatch: { _id: phaseId } } //Minimongo doesn't support operators in projections yet.
     },
     transform: null
     } );
     if ( !project ) {
     throw new Meteor.Error( 'project not found' );
     }

     if ( !project.phases ) {
     throw new Meteor.Error( 'phase not found' );
     }

     const movedPhase = _.findWhere( project.phases, { _id: phaseId } );
     if ( !movedPhase ) {
     throw new Meteor.Error( 'phase not found' );
     }

     const preModifier = { $pull: { phases: { _id: movedPhase._id } } };
     const postModifier = { $push: { phases: { $each: [movedPhase], $position: order } } };

     // Check permission for both operations
     // so that we don't run the risk of passing the first one and failing the second one
     // thus resulting in just deleting a phase
     Security.can( this.userId ).update( projectId, preModifier ).for( ProjectCollection ).throw();
     Security.can( this.userId ).update( projectId, postModifier ).for( ProjectCollection ).throw();

     // We have to do both operation separately
     // https://jira.mongodb.org/browse/SERVER-1050

     // First, remove the moved phase from the array, direct, don't run any hooks on this delete
     ProjectCollection.direct.update( { _id: projectId }, preModifier );
     // Then, reinsert it at the correct position
     ProjectCollection.direct.update( { _id: projectId }, postModifier );
     }*/

};
export default ProjectMethods;
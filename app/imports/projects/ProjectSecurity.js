"use strict";

import {RoleList, RoleGroups} from "/imports/accounts/Roles";
import UserHelpers from "/imports/accounts/UserHelpers";
import GroupSecurity from "/imports/groups/GroupSecurity";
import ProjectCollection from "./ProjectCollection";

/**
 * Project Security
 *
 * For every method in Project Methods, a rule should exist here to allow/deny the action.
 * Collection-based security will be deprecated in favor of per-method security, thus limiting the potential
 * risk of allowing unwanted modifications to the collection.
 */
export default class ProjectSecurity {

    /**
     * Checks if a project can be read by a user
     *
     * @param {String} [userId]
     * @param {String|Object} project
     * @returns {Boolean}
     */
    static canRead( userId = null, project ) {
        check( userId, Match.OneOf( String, null, undefined ) );
        check( project, Match.OneOf(
            Match.ObjectIncluding( { public: Boolean, groups: [String] } ), // Already has the public/groups populated
            Match.Where( p => {
                check( p, Match.ObjectIncluding( { public: Boolean } ) );
                return p.public
            } ), // Is already public no need for more
            Match.ObjectIncluding( { _id: String } ), // Does not have the groups but at least has an id
            String // Project Id
        ) );

        if ( UserHelpers.isExpert( userId ) ) {
            // Expert, all access
            return true;

        } else if ( userId ) {
            // Logged-in, allow public projects and those where the user has access to the linked groups
            // Get the project if we were passed an id or an project without groups/public
            if ( _.isString( project ) ) {
                project = ProjectCollection.findOne( { _id: project }, { fields: { public: 1, groups: 1 }, transform: null } );
            } else if ( !project.public && !project.groups ) {
                project = ProjectCollection.findOne( { _id: project._id }, { fields: { public: 1, groups: 1 }, transform: null } );
            }

            if ( !project ) {
                return false;
            }

            if ( project.public ) {
                // Project is public so go ahead
                return true;

            } else {
                // Check that at least one group allow read access,
                // but for a member only, we don't care if the group is public here
                return project.groups.some( group => GroupSecurity.canRead( userId, group, true ) );
            }

        } else {
            // Anonymous, only allow public projects
            if ( _.isString( project ) ) {
                project = ProjectCollection.findOne( { _id: project, public: true }, { fields: { _id: 1, public: 1 }, transform: null } );
            } else if ( 'undefined' == typeof project.public ) {
                project = ProjectCollection.findOne( { _id: project._id, public: true }, { fields: { _id: 1, public: 1 }, transform: null } );
            }

            return !!project && project.public;
        }
    }

    /**
     * Check if a user can edit a project
     *
     * @param {String} [userId]
     * @param {String|Object} project
     * @returns {Boolean}
     */
    static canEdit( userId = null, project ) {
        check( userId, Match.OneOf( String, null, undefined ) );
        check( project, Match.OneOf(
            String, // Project Id
            Match.ObjectIncluding( { _id: String } ), // Does not have the groups but at least has an id
            Match.ObjectIncluding( { groups: [String] } ) // Already has the groups populated
        ) );

        if ( UserHelpers.isModerator( userId ) ) {
            // Skip all group verification if user is already a global moderator
            return true;

        } else if ( userId ) {
            // Get the project if we were passed an id or an project without groups
            if ( _.isString( project ) ) {
                project = ProjectCollection.findOne( { _id: project }, { fields: { groups: 1 }, transform: null } );
            } else if ( !project.groups ) {
                project = ProjectCollection.findOne( { _id: project._id }, { fields: { groups: 1 }, transform: null } );
            }

            if ( !project ) {
                // No project, goodbye
                return false;

            } else  {
                // User is at least a moderator for one of the groups, so allow
                return GroupSecurity.canModerateGroups( userId, project.groups );

            } /*else {
             // At least one group should allow the user to edit
             // TODO: Remove this as it should not be needed anymore
             return project.groups.some( groupId => GroupSecurity.canEdit( groupId, userId ) );
             }*/

        } else {
            // Anonymous, not allowed
            return false;
        }
    }
    
    /**
     * Can Create a Project
     *
     * @param userId
     * @param project
     * @returns {Boolean}
     */
    static canCreate( userId = null, project ) {
        check( userId, Match.OneOf( String, null ) );
        check( project, Match.ObjectIncluding( {
            groups: Match.OneOf( null, undefined, [String] )
        } ) );

        if ( UserHelpers.isExpert( userId ) ) {
            return true;
        } else if ( project.groups && project.groups.length ) {
            return GroupSecurity.canManageGroups( userId, project.groups );
        } else {
            return false;
        }
    }

    static canUpdate( userId = null, project ) {
        return ProjectSecurity.canEdit( userId, project );
    }

    static canModerate( userId = null, project ) {
        return ProjectSecurity.canEdit( userId, project );
    }

    static canRemove( userId = null, project ) {
        return ProjectSecurity.canEdit( userId, project );
    }

    /**
     * Checks if a project can be contributed to by a user
     *
     * @param {String} [userId]
     * @param {String|Object} project
     * @returns {Boolean}
     */
    static canContribute( userId = null, project ) {
        check( userId, Match.OneOf( String, null, undefined ) );
        check( project, Match.OneOf(
            String, // Project Id
            Match.ObjectIncluding( { _id: String } ), // Does not have the groups but at least has an id
            Match.ObjectIncluding( { groups: [String] } ) // Already has the groups populated
        ) );

        if ( UserHelpers.isExpert( userId ) ) {
            // Global expert, all access
            return true;

        } else if ( userId ) {
            // Get the project if we were passed an id
            if ( _.isString( project ) ) {
                project = ProjectCollection.findOne( { _id: project }, { fields: { groups: 1 }, transform: null } );
            } else if ( !project.groups ) {
                project = ProjectCollection.findOne( { _id: project._id }, { fields: { groups: 1 }, transform: null } );
            }

            if ( !project ) {
                return false;
            }

            // Check that at least one group allow read access,
            // but for a member only, we don't care if the group is public here
            return !!project.groups && project.groups.some( group => GroupSecurity.canRead( userId, group, true ) );

        } else {
            // Anonymous, not allowed
            return false;
        }
    }

}
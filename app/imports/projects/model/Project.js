"use strict";

import moment from "moment";
import md from "/imports/utils/markdown";
import Analytics from "/imports/analytics/Analytics";
import Settings from "/imports/settings/Settings";
import ProjectImagesCollection from "../ProjectImagesCollection";

/**
 * Phase Model
 *
 * @param {object} doc
 * @param {number} index
 * @constructor
 */
export class Phase {
    constructor( doc, index ) {
        _.extend( this, doc );

        // Since we are coloring phases based on index for now we keep the index from the sorting here
        // This way we can then later filter phases on the client side and still have access to the original
        // index from the unfiltered sorted list.
        this.index = index;
    }

    get hasStarted() {
        return this.startDate ? moment( this.startDate ).isBefore( moment() ) : true;
    }

    get hasEnded() {
        return this.endDate ? moment( this.endDate ).isBefore( moment() ) : false;
    }

    get isActive() {
        return !this.removed && this.hasStarted && !this.hasEnded;
    }
}

/**
 * Project Model
 *
 * @param doc
 * @constructor
 */
export default class Project {
    constructor( doc ) {
        _.extend( this, doc );

        if ( this.phases ) {
            this._phases = doc.phases;
            this.phases  = doc.phases
                .filter( phase => !phase.removed )
                // Sort by start date
                .sort( ( a, b )=> {
                    if ( a.startDate < b.startDate ) {
                        return -1
                    } else if ( a.startDate > b.startDate ) {
                        return 1;
                    } else if ( a.endDate < b.endDate ) {
                        return -1;
                    } else if ( a.endDate > b.endDate ) {
                        return 1;
                    } else {
                        return 0;
                    }
                } )
                // Wrap in a class
                .map( ( phase, index ) => new Phase( phase, index ) );
        }
    }

    /**
     * Get the project URI
     * This is used to identify the project in URL's
     *
     * @returns {String}
     */
    get uri() {
        return this.slug || this._id;
    }

    /**
     * Get description as rendered markdown
     *
     * @getter
     * @returns {String}
     */
    get descriptionMarkdown() {
        if ( !this._descriptionMarkdown ) {
            this._descriptionMarkdown = md( this.description || '' );
        }
        return this._descriptionMarkdown;
    }

    /**
     * Find image cursor for this project
     *
     * @param options
     * @returns {*}
     */
    findImage( options = {} ) {
        return ProjectImagesCollection.find( {
            'owners': this._id
        }, options );
    }

    /**
     * Find the image for this project
     *
     * @param options
     * @returns {*}
     */
    getImage( options = {} ) {
        return ProjectImagesCollection.findOne( {
            'owners': this._id
        }, options );
    }


    getPhase( phaseId ) {
        if ( !this.phases ) {
            return null;
        }
        return this.phases.find( phase => phase._id == phaseId )
    }

    /**
     * Card temperature
     * @type {Number}
     */
    get temperature() {
        if ( this.lastTemperature == null || this.heatedAt == null ) {
            throw new Meteor.Error( 'Missing fields lastTemperature and heatedAt to calculate project temperature' );
        }

        const now = new Date();
        return Analytics.getCurrentTemperature( this.lastTemperature, this.heatedAt, now, Settings.shared.analytics.project.cooldown );
    }
}
"use strict";

import slugify from "underscore.string/slugify";
import ProjectCollection from "../../ProjectCollection";
import ProjectPhaseSchema from "./ProjectPhaseSchema";

const validProjectName = new RegExp('(!?[a-zA-Z0-9])');

/**
 * Project Schema
 * @type {SimpleSchema}
 */
const ProjectSchema = new SimpleSchema( {
    name:         {
        type: String,
        min:  3,
        max:  140, // Like twitter, because reasons,
        custom: function() {
            if ( this.value.match(validProjectName) == null ) {
                return "notAllowed";
            }
        }
    },
    // Auto-valued field to sort by case-insensitive name
    name_sort:    {
        type:      String,
        index:     true,
        autoValue: function () {
            var name = this.field( "name" );
            if ( name.isSet ) {
                return name.value.toLowerCase();
            } else {
                this.unset(); // Prevent users from supplying their own value
            }
        }
    },
    slug:         {
        type:      String,
        index:     true,
        unique:    true,
        autoValue: function () {
            var name = this.field( "name" );
            let slug = null;
            if ( name.isSet ) {
                slug = slugify( name.value );
            } else if ( this.isSet ) {
                slug = slugify( this.value );
            }

            if ( slug != null ) {
                let count    = 0;
                let safeSlug = slug;
                while ( ProjectCollection.findOne( { _id: { $ne: this.docId }, slug: safeSlug } ) != null ) {
                    count++;
                    safeSlug = slug + ( count > 0 ? count.toString() : '' );
                }
                return safeSlug;
            }
        }
    },
    type:         {
        type:         String,
        defaultValue: 'challenge'
    },
    description:  {
        type: String
    },
    content:      {
        type:     String,
        optional: true
    },
    hashtag:      {
        type:     String,
        optional: true,
        regEx:    /^[a-zA-Z0-9_]*$/
    },
    public:       {
        type:         Boolean,
        defaultValue: false
    },
    showTimeline: {
        type:         Boolean,
        defaultValue: true
    },
    groups:       {
        type:         [String],
        defaultValue: [],
        optional:     true
    },
    phases:       {
        type:         [ProjectPhaseSchema],
        defaultValue: [],
        optional:     true
    },

    // Analytics

    totalTemperature: {
        type:         Number,
        decimal:      true,
        optional:     true,
        defaultValue: 0
    },

    lastTemperature:  {
        type:         Number,
        decimal:      true,
        optional:     true,
        defaultValue: 0
    },

    heatedAt:         {
        type:      Date,
        optional:  true,
        autoValue: function () {
            if ( !this.isSet && !this.isUpdate ) {
                return new Date();
            }
        }
    },
} );

export default ProjectSchema;

/**
 * Project Update Schema
 * @type {SimpleSchema}
 */
export const ProjectUpdateSchema = new SimpleSchema( [
    {
        name:        {
            type:     String,
            optional: true,
            min:  3,
            max:  140, // Like twitter, because reasons,
            custom: function() {
                if ( this.isSet && this.value.match(validProjectName) == null ) {
                    return "notAllowed";
                }
            }
        },
        description: {
            type:     String,
            optional: true
        },
        public:      {
            type:     Boolean,
            optional: true
        },
        showTimeline: {
            type:     Boolean,
            optional: true
        }
    },
    ProjectSchema.pick([
        "content",
        "hashtag",
        "groups",
        "groups.$"
    ])
] );
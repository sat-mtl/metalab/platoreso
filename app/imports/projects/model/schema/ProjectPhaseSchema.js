"use strict";

import { SimpleSchema } from "meteor/aldeed:simple-schema"
import PhaseTypes from "../../phases/PhaseTypes";

// QUESTIONS

const Question = new SimpleSchema({
    order: {
        type: Number,
        defaultValue: -1
    },
    prompt: {
        type: String
    }
});

const QuestionsPhaseTypeSchema = new SimpleSchema({
    questions : {
        type: Object, // [Question]
        blackbox: true
    }
});

// POLL

const PollChoice = new SimpleSchema( {
    answer: {
        type: String
    }
});

const PollQuestion = new SimpleSchema({
    prompt: {
        type: String
    },
    choices: {
        type: [PollChoice]
    }
});

const PollPhaseTypeSchema = new SimpleSchema({
    questions : {
        type: Object, // [PollQuestion]
        blackbox: true
    }
});

// PHASES DATA

const ProjectPhaseDataSchema = new SimpleSchema({
    questions : {
        type: QuestionsPhaseTypeSchema,
        optional: true
    },
    poll: {
        type: PollPhaseTypeSchema,
        optional: true
    }
});

// PHASES

const ProjectPhaseSchema = new SimpleSchema({

    _id: {
        type: String,
        autoValue: function(phase) {
            if ( !this.isSet ) {
                return Random.id();
            }
        }
    },

    removed: {
        type: Boolean,
        optional: true,
        defaultValue: false
    },

    name: {
        type: String
    },

    description: {
        type: String,
        optional: true
    },

    startDate: {
        type: Date,
        optional: true
    },

    endDate: {
        type: Date,
        optional: true
    },

    type: {
        type: String,
        defaultValue: 'basic',
        custom: function() {
            if ( this.isSet ) {
                if ( PhaseTypes.list.indexOf( this.value ) == -1 ) {
                    return 'notAllowed';
                }
            }
        }
    },

    data: {
        type: ProjectPhaseDataSchema,
        defaultValue: {}
    }

});

export default ProjectPhaseSchema;

export const ProjectPhaseUpdateSchema = new SimpleSchema([
    ProjectPhaseSchema.pick([
        '_id',
        'description',
        'startDate',
        'endDate'
    ]),
    {
        name: {
            type: String,
            optional: true
        },
        type: {
            type: String,
            custom: function() {
                if ( this.isSet ) {
                    if ( PhaseTypes.list.indexOf( this.value ) == -1 ) {
                        return 'notAllowed';
                    }
                }
            },
            optional: true
        },
        data: {
            type: ProjectPhaseDataSchema,
            optional: true
        }
    }
]);
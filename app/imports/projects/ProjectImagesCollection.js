"use strict";

import ImageCollection from "/imports/images/ImageCollection";

/**
 * Project Images Collection
 */
const ProjectImagesCollection = new ImageCollection('project', null, {
    overwritePrevious: true
});
export default ProjectImagesCollection;
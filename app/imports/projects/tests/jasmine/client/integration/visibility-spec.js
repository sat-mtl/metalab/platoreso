"use strict";

import ProjectCollection from "/imports/projects/ProjectCollection";

describe( 'project visibility', () => {

    beforeEach( done => {
        Meteor.call( 'tests/projects/reset', () => {
            done();
        } );
    } );

    afterEach( function ( done ) {
        // Logout
        Meteor.logout( () => {
            done();
        } );
    } );

    describe( 'list', () => {

        describe( 'as an admin', () => {

            let projectsSubscription;

            beforeEach( done => {
                Meteor.loginWithPassword( "admin1@test.com", "admin0123", err => {
                    expect( err ).toBeUndefined();
                    done();
                } );
            } );

            afterEach( done => {
                projectsSubscription.stop();
                done();
            } );

            it( 'should see all projects in a public group', done => {
                Meteor.call( '__group_find__', 'public-group', ( err, group ) => {
                    expect( err ).toBeUndefined();
                    projectsSubscription = Meteor.subscribe( 'project/list', group._id, () => {
                        expect( ProjectCollection.find( { groups: group._id } ).fetch().length ).toBe( 2 );
                        done();
                    } );
                } );
            } );

            it( 'should see all projects in a private group', done => {
                Meteor.call( '__group_find__', 'private-group', ( err, group ) => {
                    expect( err ).toBeUndefined();
                    projectsSubscription = Meteor.subscribe( 'project/list', group._id, () => {
                        expect( ProjectCollection.find( { groups: group._id } ).fetch().length ).toBe( 2 );
                        done();
                    } );
                } );
            } );

        } );

        describe( 'as a regular user', () => {

            let projectsSubscription;

            beforeEach( done => {
                Meteor.loginWithPassword( "normal@test.com", "normal0123", err => {
                    expect( err ).toBeUndefined();
                    done();
                } );
            } );

            afterEach( function ( done ) {
                projectsSubscription.stop();
                done();
            } );

            it( 'should see only public projects in a public group', done => {
                Meteor.call( '__group_find__', 'public-group', ( err, group ) => {
                    expect( err ).toBeUndefined();
                    projectsSubscription = Meteor.subscribe( 'project/list', group._id, () => {
                        const projects = ProjectCollection.find( { groups: group._id } ).fetch();
                        expect( projects.length ).toBe( 1 );
                        expect( projects[0].public ).toBe( true );
                        done();
                    } );
                } );
            } );

            it( 'should see no projects in a private group', done => {
                Meteor.call( '__group_find__', 'private-group', ( err, group ) => {
                    expect( err ).toBeUndefined();
                    projectsSubscription = Meteor.subscribe( 'project/list', group._id, () => {
                        const projects = ProjectCollection.find( { groups: group._id }).fetch();
                        expect( projects.length ).toBe( 0 );
                        done();
                    } );
                } );
            } );

        } );

        describe( 'as a participant', () => {

            let projectsSubscription;

            beforeEach( done => {
                Meteor.loginWithPassword( "normal@test.com", "normal0123", err => {
                    expect( err ).toBeUndefined();
                    done();
                } );
            } );

            afterEach( function ( done ) {
                projectsSubscription.stop();
                done();
            } );

            it( 'should see public and participating as expert projects in private group', ( done ) => {
                Meteor.call( '__group__addExpert__', 'private-group', 'normal@test.com', err=> {
                    expect( err ).toBeUndefined();
                    Meteor.call( '__group_find__', 'private-group', ( err, group ) => {
                        expect( err ).toBeUndefined();
                        projectsSubscription = Meteor.subscribe( 'project/list', group._id, () => {
                            const projects = ProjectCollection.find( { groups: group._id } ).fetch();
                            expect( projects.length ).toBe( 2 );
                            done();
                        } );
                    });
                } );
            } );

            it( 'should see public and participating as expert projects in private group', ( done ) => {
                Meteor.call( '__group__addModerator__', 'private-group', 'normal@test.com', err=> {
                    expect( err ).toBeUndefined();
                    Meteor.call( '__group_find__', 'private-group', ( err, group ) => {
                        expect( err ).toBeUndefined();
                        projectsSubscription = Meteor.subscribe( 'project/list', group._id, () => {
                            const projects = ProjectCollection.find( { groups: group._id } ).fetch();
                            expect( projects.length ).toBe( 2 );
                            done();
                        } );
                    });
                } );
            } );

            it( 'should see public and participating as expert projects in private group', ( done ) => {
                Meteor.call( '__group__addMember__', 'private-group', 'normal@test.com', err=> {
                    expect( err ).toBeUndefined();
                    Meteor.call( '__group_find__', 'private-group', ( err, group ) => {
                        expect( err ).toBeUndefined();
                        projectsSubscription = Meteor.subscribe( 'project/list', group._id, () => {
                            const projects = ProjectCollection.find( { groups: group._id } ).fetch();
                            expect( projects.length ).toBe( 2 );
                            done();
                        } );
                    });
                } );
            } );

        } );

        describe( 'as an anonymous user', () => {

            let projectsSubscription;

            afterEach( function ( done ) {
                projectsSubscription.stop();
                done();
            } );

            it( 'should see public projects in public group', done => {
                Meteor.call( '__group_find__', 'public-group', ( err, group ) => {
                    expect( err ).toBeUndefined();
                    projectsSubscription = Meteor.subscribe( 'project/list', group._id, () => {
                        const projects = ProjectCollection.find( { groups: group._id } ).fetch();
                        expect( projects.length ).toBe( 1 );
                        done();
                    } );
                });
            } );

            it( 'should not see public projects in private group', done => {
                Meteor.call( '__group_find__', 'private-group', ( err, group ) => {
                    expect( err ).toBeUndefined();
                    projectsSubscription = Meteor.subscribe( 'project/list', group._id, () => {
                        const projects = ProjectCollection.find( { groups: group._id }).fetch();
                        expect( projects.length ).toBe( 0 );
                        done();
                    } );
                });
            } );

        } );

    } );

    describe( 'single projects', () => {

        describe( 'as an admin', () => {

            let projectSubscription;

            beforeEach( done => {
                Meteor.loginWithPassword( "admin1@test.com", "admin0123", err => {
                    expect( err ).toBeUndefined();
                    done();
                } );
            } );

            afterEach( done => {
                projectSubscription.stop();
                done();
            } );

            it( 'should see private project', done => {
                Meteor.call( 'tests/projects/find', 'private-project', ( err, project ) => {
                    expect( err ).toBeUndefined();
                    projectSubscription = Meteor.subscribe( 'project', project._id, () => {
                        const p = ProjectCollection.findOne({_id:project._id});
                        expect( p ).not.toBeUndefined();
                        expect( p.slug ).toBe( 'private-project' );
                        done();
                    } );
                } );
            } );

            it( 'should see public project', done => {
                Meteor.call( 'tests/projects/find', 'public-project', ( err, project ) => {
                    expect( err ).toBeUndefined();
                    projectSubscription = Meteor.subscribe( 'project', project._id, () => {
                        const p = ProjectCollection.findOne({_id:project._id});
                        expect( p ).not.toBeUndefined();
                        expect( p.slug ).toBe( 'public-project' );
                        done();
                    } );
                } );
            } );

        } );

        describe( 'as a regular user', () => {

            let projectSubscription;

            beforeEach( done => {
                Meteor.loginWithPassword( "normal@test.com", "normal0123", err => {
                    expect( err ).toBeUndefined();
                    done();
                } );
            } );

            afterEach( function ( done ) {
                projectSubscription.stop();
                done();
            } );

            it( 'should not see private project', done => {
                Meteor.call( 'tests/projects/find', 'private-project', ( err, project ) => {
                    expect( err ).toBeUndefined();
                    projectSubscription = Meteor.subscribe( 'project', project._id, () => {
                        const p = ProjectCollection.findOne({_id:project._id});
                        expect( p ).toBeUndefined();
                        done();
                    } );
                } );
            } );

            it( 'should see public project', done => {
                Meteor.call( 'tests/projects/find', 'public-project', ( err, project ) => {
                    expect( err ).toBeUndefined();
                    projectSubscription = Meteor.subscribe( 'project', project._id, () => {
                        const p = ProjectCollection.findOne({_id:project._id});
                        expect( p ).not.toBeUndefined();
                        expect( p.slug ).toBe( 'public-project' );
                        done();
                    } );
                } );
            } );

        } );

        describe( 'as a participant', () => {

            let projectSubscription;

            beforeEach( done => {
                Meteor.loginWithPassword( "normal@test.com", "normal0123", err => {
                    expect( err ).toBeUndefined();
                    done();
                } );
            } );

            afterEach( function ( done ) {
                projectSubscription.stop();
                done();
            } );

            it( 'should see participating as expert project', ( done ) => {
                Meteor.call( '__group__addExpert__', 'private-group', 'normal@test.com', err=> {
                    expect( err ).toBeUndefined();
                    Meteor.call( 'tests/projects/find', 'private-project', ( err, project ) => {
                        expect( err ).toBeUndefined();
                        projectSubscription = Meteor.subscribe( 'project', project._id, () => {
                            const p = ProjectCollection.findOne({_id:project._id});
                            expect( p ).not.toBeUndefined();
                            expect( p.slug ).toBe( 'private-project' );
                            done();
                        } );
                    } );
                } );
            } );

            it( 'should participating as expert project', ( done ) => {
                Meteor.call( '__group__addModerator__', 'private-group', 'normal@test.com', err=> {
                    expect( err ).toBeUndefined();
                    Meteor.call( 'tests/projects/find', 'private-project', ( err, project ) => {
                        expect( err ).toBeUndefined();
                        projectSubscription = Meteor.subscribe( 'project', project._id, () => {
                            const p = ProjectCollection.findOne({_id:project._id});
                            expect( p ).not.toBeUndefined();
                            expect( p.slug ).toBe( 'private-project' );
                            done();
                        } );
                    } );
                } );
            } );

            it( 'should see participating as expert project', ( done ) => {
                Meteor.call( '__group__addMember__', 'private-group', 'normal@test.com', err=> {
                    expect( err ).toBeUndefined();
                    Meteor.call( 'tests/projects/find', 'private-project', ( err, project ) => {
                        expect( err ).toBeUndefined();
                        projectSubscription = Meteor.subscribe( 'project', project._id, () => {
                            const p = ProjectCollection.findOne({_id:project._id});
                            expect( p ).not.toBeUndefined();
                            expect( p.slug ).toBe( 'private-project' );
                            done();
                        } );
                    } );
                } );
            } );

        } );

        describe( 'as an anonymous user', () => {

            let projectSubscription;

            afterEach( function ( done ) {
                projectSubscription.stop();
                done();
            } );

            it( 'should not see private project', done => {
                Meteor.call( 'tests/projects/find', 'private-project', ( err, project ) => {
                    expect( err ).toBeUndefined();
                    projectSubscription = Meteor.subscribe( 'project', project._id, () => {
                        const p = ProjectCollection.findOne({_id:project._id});
                        expect( p ).toBeUndefined();
                        done();
                    } );
                } );
            } );

            it( 'should see public project', done => {
                Meteor.call( 'tests/projects/find', 'public-project', ( err, project ) => {
                    expect( err ).toBeUndefined();
                    projectSubscription = Meteor.subscribe( 'project', project._id, () => {
                        const p = ProjectCollection.findOne({_id:project._id});
                        expect( p ).not.toBeUndefined();
                        expect( p.slug ).toBe( 'public-project' );
                        done();
                    } );
                } );
            } );

        } );

    } );
} );
"use strict";

import PhaseTypes from "/imports/projects/phases/PhaseTypes";

describe('pr-projects-phases-questions/both/integration/PhaseService', ()=>{

    it('should be registered', () => {
        expect(PhaseTypes.list.indexOf('questions')).not.toBe(-1);
        expect(PhaseTypes.get('questions')).toBeDefined();
    })

});
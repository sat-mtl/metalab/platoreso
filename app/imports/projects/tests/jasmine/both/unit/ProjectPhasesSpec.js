"use strict";

import PhaseService from "/imports/projects/phases/PhaseService";
import PhaseTypes from "/imports/projects/phases/PhaseTypes";

describe('pr-projects/both/unit/ProjectPhases', () => {

    describe('PhaseService', ()=> {

        it('should have an update method', () => {
            const testService = new PhaseService();
            expect(testService.update).toBeDefined();
        });

        it('should throw on invalid project id (number)', () => {
            const testService = new PhaseService();
            expect(()=>testService.update(123, { _id: 'phaseId'})).toThrowError(/Match error/);
        });

        it('should throw on invalid project id (null)', () => {
            const testService = new PhaseService();
            expect(()=>testService.update(null, { _id: 'phaseId'})).toThrowError(/Match error/);
        });

        it('should throw on invalid phase (no _id)', () => {
            const testService = new PhaseService();
            expect(()=>testService.update('projectId', { name: 'name'})).toThrowError(/Match error/);
        });

        it('should throw on invalid phase (number)', () => {
            const testService = new PhaseService();
            expect(()=>testService.update('projectId', 123)).toThrowError(/Match error/);
        });

        it('should throw on invalid phase (null)', () => {
            const testService = new PhaseService();
            expect(()=>testService.update('projectId', null)).toThrowError(/Match error/);
        });

        it('should throw on invalid phase (undefined)', () => {
            const testService = new PhaseService();
            expect(()=>testService.update('projectId')).toThrowError(/Match error/);
        });

    });

    describe('PhaseTypes', ()=> {

        beforeEach(()=>{
        });

        it('should already have the basic type registered', ()=> {
            // We can't check if it is the only one since we are not in a real unit test environment
            // Other packages might already have registered types
            expect(PhaseTypes.list.indexOf('basic')).not.toEqual(-1);
        });

        it('should register/retrieve a phase type', () => {
            var testService = new PhaseService();
            PhaseTypes.register('test', testService);
            // We can't check for array equality since we are not in a real unit test environment
            // Other packages might already have registered types
            expect(PhaseTypes.list.indexOf('basic')).not.toEqual(-1);
            expect(PhaseTypes.list.indexOf('test')).not.toEqual(-1);
            expect(PhaseTypes.get('test')).toBe(testService);
        });

    });



});
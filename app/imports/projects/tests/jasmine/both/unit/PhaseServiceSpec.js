"use strict";

import ProjectCollection from "/imports/projects/ProjectCollection";

const { QuestionsPhaseService } = PR.QuestionsPhase;

describe( 'pr-projects-phases-questions/both/unit/PhaseService', ()=> {

    let context;
    let service;
    let security_update;
    let security_for;
    let security_throw;

    beforeEach( ()=> {
        service = new QuestionsPhaseService();

        context = { userId: '420' };

        security_throw  = jasmine.createSpy( 'throw' );
        security_for    = jasmine.createSpy( 'for' ).and.returnValue( { throw: security_throw } );
        security_update = jasmine.createSpy( 'update' ).and.returnValue( { for: security_for } );
        spyOn( Security, 'can' ).and.returnValue( { update: security_update } );

        spyOn( ProjectCollection, 'update' );
    } );

    describe('question removal', ()=> {

        it( 'should remove removedQuestions from the update info', () => {
            const info     = { _id: 'phaseId', removedQuestions: ['q1', 'q2'] };
            const modifier = {
                $unset: {
                    'phases.$.data.questions.questions.q1': null,
                    'phases.$.data.questions.questions.q2': null
                }
            };

            expect( ()=>service.update.call( context, 'projectId', info ) ).not.toThrow();

            expect( Security.can ).toHaveBeenCalledWith( context.userId );
            expect( security_update ).toHaveBeenCalledWith( 'projectId', modifier );
            expect( security_for ).toHaveBeenCalledWith( ProjectCollection );
            expect( security_throw ).toHaveBeenCalled();
            expect( ProjectCollection.update ).toHaveBeenCalledWith( { _id: 'projectId', 'phases._id': 'phaseId' }, modifier );
        } );

        it( 'should not remove removedQuestions and throw if not allowed', () => {
            const info = { _id: 'phaseId', removedQuestions: ['q1', 'q2'] };
            const modifier = {
                $unset: {
                    'phases.$.data.questions.questions.q1': null,
                    'phases.$.data.questions.questions.q2': null
                }
            };
            security_throw.and.throwError( 'security' );

            expect( ()=>service.update.call( context, 'projectId', info ) ).toThrowError( /security/ );

            expect( Security.can ).toHaveBeenCalledWith( context.userId );
            expect( security_update ).toHaveBeenCalledWith( 'projectId', modifier );
            expect( security_for ).toHaveBeenCalledWith( ProjectCollection );
            expect( security_throw ).toHaveBeenCalled();
            expect( ProjectCollection.update ).not.toHaveBeenCalled();
        } );

        function expectNotRemoved() {
            expect( Security.can ).not.toHaveBeenCalled();
            expect( security_update ).not.toHaveBeenCalled();
            expect( security_for ).not.toHaveBeenCalled();
            expect( security_throw ).not.toHaveBeenCalled();
            expect( ProjectCollection.update ).not.toHaveBeenCalled();
        }

        it( 'should not remove removedQuestions if undefined', () => {
            const info = { _id: 'phaseId' };
            expect( ()=>service.update.call( context, 'projectId', info ) ).not.toThrow();
            expectNotRemoved();
        } );

        it( 'should not remove removedQuestions if empty', () => {
            const info = { _id: 'phaseId', removedQuestions: [] };
            expect( ()=>service.update.call( context, 'projectId', info ) ).not.toThrow();
            expectNotRemoved();
        } );

        it( 'should not remove removedQuestions if not an array', () => {
            const info = { _id: 'phaseId', removedQuestions: 'not an array' };
            expect( ()=>service.update.call( context, 'projectId', info ) ).not.toThrow();
            expectNotRemoved();
        } );

    });

} );
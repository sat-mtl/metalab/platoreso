"use strict";

import ProjectCollection from "/imports/projects/ProjectCollection";
import ProjectImagesCollection from "/imports/projects/ProjectImagesCollection";

const publication = Meteor.server.publish_handlers['admin/project/edit'];

import UserHelpers from "/imports/accounts/UserHelpers";

describe( 'pr-projects/server/unit/publication/admin/project/edit', function () {

    let context;

    beforeEach( function () {
        context = {
            userId: '420',
            ready:  jasmine.createSpy( 'ready' ).and.returnValue( 'ready' ),
            onStop: jasmine.createSpy( 'onStop' ),
            added:  jasmine.createSpy( 'added' )
        };
        spyOn( UserHelpers, 'isSuper' );
        spyOn( ProjectCollection, 'find' ).and.returnValue( 'project_cursor' );
        spyOn( ProjectImagesCollection, 'find' ).and.returnValue( 'image_cursor' );
    } );

    it( 'should subscribe to a project if allowed', function () {
        UserHelpers.isSuper.and.returnValue( true );
        expect( ()=>expect(publication.call( context, 'projectId' )).toEqual(['project_cursor','image_cursor']) ).not.toThrow();
        expect( UserHelpers.isSuper ).toHaveBeenCalledWith( context.userId );
        expect( ProjectCollection.find ).toHaveBeenCalledWith( { _id: 'projectId' } );
        expect( ProjectImagesCollection.find ).toHaveBeenCalledWith( { owners: 'projectId' } );
        expect( context.ready ).not.toHaveBeenCalled();
    } );

    it( 'should not subscribe to a project if not allowed', function () {
        UserHelpers.isSuper.and.returnValue( false );
        expect( ()=>expect(publication.call( context, 'projectId' )).toEqual('ready') ).not.toThrow();
        expect( UserHelpers.isSuper ).toHaveBeenCalledWith( context.userId );
        expect( ProjectCollection.find ).not.toHaveBeenCalled();
        expect( ProjectImagesCollection.find ).not.toHaveBeenCalled();
        expect( context.ready ).toHaveBeenCalled();
    } );

    it( 'should throw on missing id', function () {
        expect( ()=>publication.call( context ) ).toThrowError(/Match error/);
        expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
        expect( ProjectCollection.find ).not.toHaveBeenCalled();
        expect( ProjectImagesCollection.find ).not.toHaveBeenCalled();
        expect( context.ready ).not.toHaveBeenCalled();
    } );

    it( 'should throw on invalid id', function () {
        expect( ()=>publication.call( context, 123 ) ).toThrowError(/Match error/);
        expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
        expect( ProjectCollection.find ).not.toHaveBeenCalled();
        expect( ProjectImagesCollection.find ).not.toHaveBeenCalled();
        expect( context.ready ).not.toHaveBeenCalled();
    } );
} );
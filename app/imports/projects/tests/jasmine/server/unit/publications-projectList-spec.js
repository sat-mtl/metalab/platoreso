"use strict";

import GroupSecurity from "/imports/groups/GroupSecurity";
import ProjectCollection from "/imports/projects/ProjectCollection";
import ProjectImagesCollection from "/imports/projects/ProjectImagesCollection";

const publication = Meteor.server.publish_handlers['project/list'];

describe( 'pr-projects/server/unit/publication/project/list', function () {

    let context;
    let query;
    let options;

    beforeEach( function () {
        context = {
            userId: '420',
            ready:  jasmine.createSpy( 'ready' ),
            onStop: jasmine.createSpy( 'onStop' ),
            added:  jasmine.createSpy( 'added' )
        };
        options = {};
        spyOn( GroupSecurity, 'getUserAccess' );
        spyOn( GroupSecurity, 'getGroupsForUser' );
        spyOn( ProjectCollection, 'find' );
    } );

    describe( 'filtered by group', () => {

        beforeEach( ()=> {
            query = { groups: 'groupId' };
        } );

        it( 'should subscribe to a project list if allowed and member', function () {
            GroupSecurity.getUserAccess.and.returnValue( { allowed: true, member: true } );
            expect( ()=>publication.call( context, 'groupId' ) ).not.toThrow();
            expect( GroupSecurity.getUserAccess ).toHaveBeenCalledWith( 'groupId', context.userId );
            expect( GroupSecurity.getGroupsForUser ).not.toHaveBeenCalled();
            expect( ProjectCollection.find ).toHaveBeenCalledWith( query, _.extend( options, {
                fields: jasmine.any( Object ),
                sort:   {
                    updatedAt: -1
                }
            } ) );
            expect( context.ready ).toHaveBeenCalled(); // Composite
        } );

        it( 'should subscribe to a project list if allowed and not member', function () {
            query.public = true;
            GroupSecurity.getUserAccess.and.returnValue( { allowed: true, member: false } );
            expect( ()=>publication.call( context, 'groupId' ) ).not.toThrow();
            expect( GroupSecurity.getUserAccess ).toHaveBeenCalledWith( 'groupId', context.userId );
            expect( GroupSecurity.getGroupsForUser ).not.toHaveBeenCalled();
            expect( ProjectCollection.find ).toHaveBeenCalledWith( query, _.extend( options, { fields: jasmine.any( Object ), sort: { updatedAt: -1 } } ) );
            expect( context.ready ).toHaveBeenCalled(); // Composite
        } );

        it( 'should not subscribe to a project list if not allowed', function () {
            GroupSecurity.getUserAccess.and.returnValue( { allowed: false, member: false } );
            expect( ()=>publication.call( context, 'groupId' ) ).not.toThrow();
            expect( GroupSecurity.getUserAccess ).toHaveBeenCalledWith( 'groupId', context.userId );
            expect( GroupSecurity.getGroupsForUser ).not.toHaveBeenCalled();
            expect( ProjectCollection.find ).not.toHaveBeenCalled();
            expect( context.ready ).toHaveBeenCalled(); // Composite
        } );

        it( 'should throw on invalid group id (number)', function () {
            expect( ()=>publication.call( context, 123 ) ).toThrowError( /Match error/ );
            expect( GroupSecurity.getUserAccess ).not.toHaveBeenCalled();
            expect( GroupSecurity.getGroupsForUser ).not.toHaveBeenCalled();
            expect( ProjectCollection.find ).not.toHaveBeenCalled();
            expect( context.ready ).not.toHaveBeenCalled();
        } );

    } );

    describe( 'all projects', () => {

        beforeEach( ()=> {
            query = {};
        } );

        it( 'should subscribe to a project list if allowed or public', function () {
            query.$or = [
                {
                    public: true
                },
                {
                    groups: {
                        $in: ['group1', 'group2']
                    }
                }
            ];

            GroupSecurity.getGroupsForUser.and.returnValue( ['group1', 'group2'] );
            expect( ()=>publication.call( context ) ).not.toThrow();
            expect( GroupSecurity.getUserAccess ).not.toHaveBeenCalled();
            expect( GroupSecurity.getGroupsForUser ).toHaveBeenCalledWith( context.userId );
            expect( ProjectCollection.find ).toHaveBeenCalledWith( query, _.extend( options, {
                fields: jasmine.any( Object ),
                sort:   {
                    updatedAt: -1
                }
            } ) );
            expect( context.ready ).toHaveBeenCalled(); // Composite
        } );

    } );
} );
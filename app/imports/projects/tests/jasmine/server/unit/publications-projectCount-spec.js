"use strict";

import ProjectCollection from "/imports/projects/ProjectCollection";
import ProjectPublicationHelpers from "/imports/projects/server/ProjectPublicationHelpers";

const publication = Meteor.server.publish_handlers['project/count'];

 import { Counts } from "meteor/tmeasday:publish-counts";

describe( 'pr-projects/server/unit/publication/project/count', function () {

    let context;
    let query;

    beforeEach( function () {
        context = {
            userId: '420',
            ready:  jasmine.createSpy( 'ready' )
        };
        query = { any: 'query' };
        spyOn( ProjectPublications, 'getProjectListQuery' );
        spyOn( Counts, 'publish' );
        spyOn( ProjectCollection, 'find' ).and.returnValue('cursor');
    } );

    it( 'should subscribe to a group project list', function () {
        ProjectPublicationHelpers.getProjectListQuery.and.returnValue( query ); // This method is tested separately
        expect( ()=>publication.call( context, 'groupId' ) ).not.toThrow();
        expect( ProjectPublicationHelpers.getProjectListQuery ).toHaveBeenCalledWith( context.userId, 'groupId' );
        expect( ProjectCollection.find ).toHaveBeenCalledWith( query, { fields: { _id: 1 } } );
        expect( Counts.publish ).toHaveBeenCalledWith( context, 'project/count/groupId', 'cursor' );
        expect( context.ready ).not.toHaveBeenCalled();
    } );

    it( 'should subscribe to a project list', function () {
        ProjectPublicationHelpers.getProjectListQuery.and.returnValue( query ); // This method is tested separately
        expect( ()=>publication.call( context ) ).not.toThrow();
        expect( ProjectPublicationHelpers.getProjectListQuery ).toHaveBeenCalledWith( context.userId, null );
        expect( ProjectCollection.find ).toHaveBeenCalledWith( query, { fields: { _id: 1 } } );
        expect( Counts.publish ).toHaveBeenCalledWith( context, 'project/count', 'cursor' );
        expect( context.ready ).not.toHaveBeenCalled();
    } );

    it( 'should not subscribe if query is null', function () {
        ProjectPublicationHelpers.getProjectListQuery.and.returnValue( null ); // This method is tested separately
        expect( ()=>publication.call( context ) ).not.toThrow();
        expect( ProjectPublicationHelpers.getProjectListQuery ).toHaveBeenCalledWith( context.userId, null );
        expect( ProjectCollection.find ).not.toHaveBeenCalled();
        expect( Counts.publish ).not.toHaveBeenCalled();
        expect( context.ready ).toHaveBeenCalled();
    } );

    it( 'should throw on invalid group id (number)', function () {
        expect( ()=>publication.call( context, 123 ) ).toThrowError( /Match error/ );
        expect( ProjectPublicationHelpers.getProjectListQuery ).not.toHaveBeenCalled( );
        expect( ProjectCollection.find ).not.toHaveBeenCalled();
        expect( Counts.publish ).not.toHaveBeenCalled();
        expect( context.ready ).not.toHaveBeenCalled();
    } );

} );
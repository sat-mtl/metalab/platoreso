"use strict";

import ProjectCollection from "/imports/projects/ProjectCollection";
import ProjectSecurity from "/imports/projects/ProjectSecurity";

const projectPublication = Meteor.server.publish_handlers['project'];

describe( 'project publication', function () {

    let context;

    beforeEach( function () {
        context = {
            userId: '420',
            ready:  jasmine.createSpy( 'ready' ),
            onStop: jasmine.createSpy( 'onStop' ),
            added:  jasmine.createSpy( 'added' )
        };
        spyOn( ProjectCollection, 'findOne' );
        spyOn( ProjectSecurity, 'canRead' );
        spyOn( ProjectCollection, 'find' );
    } );

    afterEach( function () {
    } );

    it( 'should subscribe to a project', function () {
        ProjectCollection.findOne.and.returnValue( { _id: 'projectId' } );
        ProjectSecurity.canRead.and.returnValue( true );
        expect( ()=>projectPublication.call( context, 'projectId' ) ).not.toThrow();
        expect( ProjectCollection.findOne ).toHaveBeenCalledWith(
            { $or: [{ _id: 'projectId' }, { slug: 'projectId' }] },
            { fields: { _id: 1, public: 1, groups: 1 }, transform: null }
        );
        expect( ProjectSecurity.canRead ).toHaveBeenCalledWith( context.userId, { _id: 'projectId' } );
        expect( ProjectCollection.find ).toHaveBeenCalledWith( { _id: 'projectId' }, { fields: jasmine.any( Object ) } );
        expect( context.ready ).toHaveBeenCalled();
    } );

    it( 'should not subscribe if project is not found', function () {
        ProjectCollection.findOne.and.returnValue( null );
        expect( ()=>projectPublication.call( context, 'projectId' ) ).not.toThrow();
        expect( ProjectCollection.findOne ).toHaveBeenCalledWith(
            { $or: [{ _id: 'projectId' }, { slug: 'projectId' }] },
            { fields: { _id: 1, public: 1, groups: 1 }, transform: null }
        );
        expect( ProjectSecurity.canRead ).not.toHaveBeenCalled();
        expect( ProjectCollection.find ).not.toHaveBeenCalled();
        expect( context.ready ).toHaveBeenCalled();
    } );

    it( 'shouldn\'t subscribe if user can\'t read', function () {
        ProjectCollection.findOne.and.returnValue( { _id: 'projectId' } );
        ProjectSecurity.canRead.and.returnValue( false );
        expect( ()=>projectPublication.call( context, 'projectId' ) ).not.toThrow();
        expect( ProjectCollection.findOne ).toHaveBeenCalledWith(
            { $or: [{ _id: 'projectId' }, { slug: 'projectId' }] },
            { fields: { _id: 1, public: 1, groups: 1 }, transform: null }
        );
        expect( ProjectSecurity.canRead ).toHaveBeenCalledWith( context.userId, { _id: 'projectId' } );
        expect( ProjectCollection.find ).not.toHaveBeenCalled();
        expect( context.ready ).toHaveBeenCalled();
    } );

    it( 'should throw on bad project id', function () {
        expect( ()=>projectPublication.call( context, null ) ).toThrowError( /Match\ error/ );
        expect( ProjectCollection.findOne ).not.toHaveBeenCalled();
        expect( ProjectSecurity.canRead ).not.toHaveBeenCalled();
        expect( ProjectCollection.find ).not.toHaveBeenCalled();
        expect( context.ready ).not.toHaveBeenCalled();
    } );

} );
"use strict";

import slugify from "underscore.string/slugify";
import ProjectCollection from "/imports/projects/ProjectCollection";
import ProjectImagesCollection from "/imports/projects/ProjectImagesCollection";

const publication = Meteor.server.publish_handlers['admin/project/list'];

 import { Counts } from "meteor/tmeasday:publish-counts";
import UserHelpers from "/imports/accounts/UserHelpers";

describe( 'pr-projects/server/unit/publication/admin/project/list', function () {

    let context;
    let query;
    let options;

    beforeEach( function () {
        context = {
            userId: '420',
            ready:  jasmine.createSpy( 'ready' ),
            onStop: jasmine.createSpy( 'onStop' ),
            added:  jasmine.createSpy( 'added' )
        };
        query   = {};
        options = {};
        spyOn( UserHelpers, 'isSuper' );
        spyOn( Counts, 'publish' );

        // First one for the Counts, second null so that publishComposite doesn't throw
        spyOn( ProjectCollection, 'find' ).and.returnValues( 'cursor', null );
    } );

    it( 'should subscribe to a project list if allowed', function () {
        UserHelpers.isSuper.and.returnValue( true );
        expect( ()=>publication.call( context, query, options ) ).not.toThrow();
        expect( UserHelpers.isSuper ).toHaveBeenCalledWith( context.userId );
        expect( ProjectCollection.find.calls.count() ).toEqual( 2 );
        expect( ProjectCollection.find.calls.argsFor( 0 ) ).toEqual( [{}, { fields: { _id: 1 } }] );
        expect( ProjectCollection.find.calls.argsFor( 1 ) ).toEqual( [{}, _.extend( options, { fields: jasmine.any( Object ) } )] );
        expect( Counts.publish ).toHaveBeenCalledWith( context, 'project/count/' + slugify( JSON.stringify(query) ), 'cursor', { noReady: true } );
        expect( context.ready ).toHaveBeenCalled(); // Composite
    } );

    it( 'should not subscribe to a project list if not allowed', function () {
        UserHelpers.isSuper.and.returnValue( false );
        expect( ()=>publication.call( context, query, options ) ).not.toThrow();
        expect( UserHelpers.isSuper ).toHaveBeenCalledWith( context.userId );
        expect( ProjectCollection.find ).not.toHaveBeenCalled();
        expect( Counts.publish ).not.toHaveBeenCalled();
        expect( context.ready ).toHaveBeenCalled(); // Composite
    } );

    it( 'should support null queries', function () {
        UserHelpers.isSuper.and.returnValue( true );
        expect( ()=>publication.call( context, null, options ) ).not.toThrow();
        expect( UserHelpers.isSuper ).toHaveBeenCalledWith( context.userId );
        expect( ProjectCollection.find.calls.count() ).toEqual( 2 );
        expect( ProjectCollection.find.calls.argsFor( 0 ) ).toEqual( [{}, { fields: { _id: 1 } }] );
        expect( ProjectCollection.find.calls.argsFor( 1 ) ).toEqual( [{}, _.extend( options, { fields: jasmine.any( Object ) } )] );
        expect( Counts.publish ).toHaveBeenCalledWith( context, 'project/count/' + slugify( JSON.stringify(query) ), 'cursor', { noReady: true } );
        expect( context.ready ).toHaveBeenCalled(); // Composite
    } );

    it( 'should support null options', function () {
        UserHelpers.isSuper.and.returnValue( true );
        expect( ()=>publication.call( context, query, {} ) ).not.toThrow();
        expect( UserHelpers.isSuper ).toHaveBeenCalledWith( context.userId );
        expect( ProjectCollection.find.calls.count() ).toEqual( 2 );
        expect( ProjectCollection.find.calls.argsFor( 0 ) ).toEqual( [{}, { fields: { _id: 1 } }] );
        expect( ProjectCollection.find.calls.argsFor( 1 ) ).toEqual( [{}, { fields: jasmine.any( Object ) }] );
        expect( Counts.publish ).toHaveBeenCalledWith( context, 'project/count/' + slugify( JSON.stringify(query) ), 'cursor', { noReady: true } );
        expect( context.ready ).toHaveBeenCalled(); // Composite
    } );

    it( 'should not support queries (for now)', function () {
        query.public = true;
        expect( ()=>publication.call( context, query, options ) ).toThrowError( /Match error/ );
        expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
        expect( ProjectCollection.find ).not.toHaveBeenCalled();
        expect( Counts.publish ).not.toHaveBeenCalled();
        expect( context.ready ).not.toHaveBeenCalled();
    } );

    it( 'should support sort option', function () {
        options.sort = { name: 1 };
        UserHelpers.isSuper.and.returnValue( true );
        expect( ()=>publication.call( context, query, options ) ).not.toThrow();
        expect( UserHelpers.isSuper ).toHaveBeenCalledWith( context.userId );

        expect( ProjectCollection.find.calls.count() ).toEqual( 2 );
        expect( ProjectCollection.find.calls.argsFor( 0 ) ).toEqual( [query, { fields: { _id: 1 } }] );
        expect( ProjectCollection.find.calls.argsFor( 1 ) ).toEqual( [query,  _.extend( options, { fields: jasmine.any( Object ) } ) ] );
        expect( Counts.publish ).toHaveBeenCalledWith( context, 'project/count/' + slugify( JSON.stringify(query) ), 'cursor', { noReady: true } );

        expect( context.ready ).toHaveBeenCalled(); // Composite
    } );

    it( 'should not support invalid sort option', function () {
        options.sort = "name";
        expect( ()=>publication.call( context, query, options ) ).toThrowError( /Match error/ );
        expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
        expect( ProjectCollection.find ).not.toHaveBeenCalled();
        expect( Counts.publish ).not.toHaveBeenCalled();
        expect( context.ready ).not.toHaveBeenCalled();
    } );

    it( 'should support limit option', function () {
        options.limit = 10;
        UserHelpers.isSuper.and.returnValue( true );
        expect( ()=>publication.call( context, query, options ) ).not.toThrow();
        expect( UserHelpers.isSuper ).toHaveBeenCalledWith( context.userId );

        expect( ProjectCollection.find.calls.count() ).toEqual( 2 );
        expect( ProjectCollection.find.calls.argsFor( 0 ) ).toEqual( [query, { fields: { _id: 1 } }] );
        expect( ProjectCollection.find.calls.argsFor( 1 ) ).toEqual( [query,  _.extend( options, { fields: jasmine.any( Object ) } ) ] );
        expect( Counts.publish ).toHaveBeenCalledWith( context, 'project/count/' + slugify( JSON.stringify(query) ), 'cursor', { noReady: true } );

        expect( context.ready ).toHaveBeenCalled(); // Composite
    } );

    it( 'should not support invalid limit option', function () {
        options.limit = "ten";
        expect( ()=>publication.call( context, query, options ) ).toThrowError( /Match error/ );
        expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
        expect( ProjectCollection.find ).not.toHaveBeenCalled();
        expect( Counts.publish ).not.toHaveBeenCalled();
        expect( context.ready ).not.toHaveBeenCalled();
    } );

    it( 'should support skip option', function () {
        options.skip = 10;
        UserHelpers.isSuper.and.returnValue( true );
        expect( ()=>publication.call( context, query, options ) ).not.toThrow();
        expect( UserHelpers.isSuper ).toHaveBeenCalledWith( context.userId );

        expect( ProjectCollection.find.calls.count() ).toEqual( 2 );
        expect( ProjectCollection.find.calls.argsFor( 0 ) ).toEqual( [query, { fields: { _id: 1 } }] );
        expect( ProjectCollection.find.calls.argsFor( 1 ) ).toEqual( [query,  _.extend( options, { fields: jasmine.any( Object ) } ) ] );
        expect( Counts.publish ).toHaveBeenCalledWith( context, 'project/count/' + slugify( JSON.stringify(query) ), 'cursor', { noReady: true } );

        expect( context.ready ).toHaveBeenCalled(); // Composite
    } );

    it( 'should not support invalid skip option', function () {
        options.skip = "ten";
        expect( ()=>publication.call( context, query, options ) ).toThrowError( /Match error/ );
        expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
        expect( ProjectCollection.find ).not.toHaveBeenCalled();
        expect( Counts.publish ).not.toHaveBeenCalled();
        expect( context.ready ).not.toHaveBeenCalled();
    } );

    it( 'should not support extra options', function () {
        options.extra = "option";
        expect( ()=>publication.call( context, query, options ) ).toThrowError( /Match error/ );
        expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
        expect( ProjectCollection.find ).not.toHaveBeenCalled();
        expect( Counts.publish ).not.toHaveBeenCalled();
        expect( context.ready ).not.toHaveBeenCalled();
    } );
} );
"use strict";

import GroupSecurity from "/imports/groups/GroupSecurity";
import ProjectPublicationHelpers from "/imports/projects/server/ProjectPublicationHelpers";

import UserHelpers from "/imports/accounts/UserHelpers";

describe( 'pr-projects/server/unit/publication/ProjectPublications', function () {

    describe( 'getProjectListQuery', () => {

        beforeEach( function () {
            spyOn( GroupSecurity, 'getUserAccess' );
            spyOn( GroupSecurity, 'getGroupsForUser' );
            spyOn( UserHelpers, 'isExpert' );
        } );

        describe( 'all projects', () => {

            it( 'should return a query for an expert', function () {
                UserHelpers.isExpert.and.returnValue( true );
                expect( ProjectPublicationHelpers.getProjectListQuery( '420' ) ).toEqual( {} );
                expect( UserHelpers.isExpert ).toHaveBeenCalledWith( '420' );
                expect( GroupSecurity.getUserAccess ).not.toHaveBeenCalled();
                expect( GroupSecurity.getGroupsForUser ).not.toHaveBeenCalled();
            } );

            it( 'should return a query for a non-expert', function () {
                UserHelpers.isExpert.and.returnValue( false );
                GroupSecurity.getGroupsForUser.and.returnValue( ['group1', 'group2'] );
                expect( ProjectPublicationHelpers.getProjectListQuery( '420' ) ).toEqual( {
                    $or: [
                        { public: true },
                        { groups: { $in: ['group1', 'group2'] } }
                    ]
                } );
                expect( UserHelpers.isExpert ).toHaveBeenCalledWith( '420' );
                expect( GroupSecurity.getUserAccess ).not.toHaveBeenCalled();
                expect( GroupSecurity.getGroupsForUser ).toHaveBeenCalledWith( '420' );
            } );

        } );

        describe( 'specific group', () => {

            it( 'should return a query for a member', function () {
                GroupSecurity.getUserAccess.and.returnValue( { allowed: true, member: true } );

                expect( ProjectPublicationHelpers.getProjectListQuery( '420', 'groupId' ) ).toEqual( {
                    groups: 'groupId'
                } );

                expect( UserHelpers.isExpert ).not.toHaveBeenCalled( );
                expect( GroupSecurity.getUserAccess ).toHaveBeenCalledWith('groupId', '420');
                expect( GroupSecurity.getGroupsForUser ).not.toHaveBeenCalled();
            } );

            it( 'should return a query for a non-member', function () {
                GroupSecurity.getUserAccess.and.returnValue( { allowed: true, member: false } );

                expect( ProjectPublicationHelpers.getProjectListQuery( '420', 'groupId' ) ).toEqual( {
                    groups: 'groupId',
                    public: true
                } );

                expect( UserHelpers.isExpert ).not.toHaveBeenCalled( );
                expect( GroupSecurity.getUserAccess ).toHaveBeenCalledWith('groupId', '420');
                expect( GroupSecurity.getGroupsForUser ).not.toHaveBeenCalled();
            } );

            it( 'should return null if not allowed', function () {
                GroupSecurity.getUserAccess.and.returnValue( { allowed: false, member: false } );

                expect( ProjectPublicationHelpers.getProjectListQuery( '420', 'groupId' ) ).toEqual( null );

                expect( UserHelpers.isExpert ).not.toHaveBeenCalled( );
                expect( GroupSecurity.getUserAccess ).toHaveBeenCalledWith('groupId', '420');
                expect( GroupSecurity.getGroupsForUser ).not.toHaveBeenCalled();
            } );

        } );

    } );
} );
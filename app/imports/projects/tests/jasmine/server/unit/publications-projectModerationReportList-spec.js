"use strict";

import GroupHelpers from "/imports/groups/GroupHelpers";
import GroupSecurity from "/imports/groups/GroupSecurity";
import ProjectCollection from "/imports/projects/ProjectCollection";
import ProjectImagesCollection from "/imports/projects/ProjectImagesCollection";

const publication = Meteor.server.publish_handlers['project/moderation/report/list'];

 import { Counts } from "meteor/tmeasday:publish-counts";
const { UserHelpers, RoleGroups } = PR.Accounts;

describe( 'pr-projects/server/unit/project moderation report list publication', function () {

    let context;
    let query;
    let options;

    beforeEach( function () {
        context = {
            userId: '420',
            ready:  jasmine.createSpy( 'ready' ),
            onStop: jasmine.createSpy( 'onStop' ),
            added:  jasmine.createSpy( 'added' )
        };
        query   = {};
        options = {};
        spyOn( UserHelpers, 'isModerator' );
        spyOn( GroupSecurity, 'canModerateGroups' );
        spyOn( GroupHelpers, 'getGroupsForUser' );
        spyOn( Counts, 'publish' );
        spyOn( ProjectCollection, 'findReported' );
    } );

    it( 'should publish when global moderator', function () {
        UserHelpers.isModerator.and.returnValue( true );
        GroupSecurity.canModerateGroups.and.returnValue( true );
        ProjectCollection.findReported.and.returnValues( 'cursor', null );

        expect( () => publication.call( context, _.clone( query ), _.clone( options ) ) ).not.toThrow();

        expect( UserHelpers.isModerator ).toHaveBeenCalledWith( context.userId );
        expect( GroupSecurity.canModerateGroups ).toHaveBeenCalledWith( context.userId );
        expect( GroupHelpers.getGroupsForUser ).not.toHaveBeenCalled();
        expect( Counts.publish ).toHaveBeenCalledWith( context, 'project/moderation/report/count/', 'cursor', { noReady: true } );
        expect( ProjectCollection.findReported.calls.count() ).toEqual( 2 );
        expect( ProjectCollection.findReported.calls.argsFor( 0 ) ).toEqual( [query, { fields: { _id: 1 } }] );
        expect( ProjectCollection.findReported.calls.argsFor( 1 ) ).toEqual( [query, _.extend( options, { fields: jasmine.any( Object ) } )] );
        expect( context.ready ).toHaveBeenCalled(); // Composite
    } );

    it( 'should publish when group moderator', function () {
        UserHelpers.isModerator.and.returnValue( false );
        GroupSecurity.canModerateGroups.and.returnValue( true );
        GroupHelpers.getGroupsForUser.and.returnValue( ['group1', 'group2'] );
        ProjectCollection.findReported.and.returnValues( 'cursor', null );

        expect( () => publication.call( context, _.clone( query ), _.clone( options ) ) ).not.toThrow();

        expect( UserHelpers.isModerator ).toHaveBeenCalledWith( context.userId );
        expect( GroupSecurity.canModerateGroups ).toHaveBeenCalledWith( context.userId );
        expect( GroupHelpers.getGroupsForUser ).toHaveBeenCalledWith( context.userId, RoleGroups.moderators );
        expect( Counts.publish ).toHaveBeenCalledWith( context, 'project/moderation/report/count/', 'cursor', { noReady: true } );
        expect( ProjectCollection.findReported.calls.count() ).toEqual( 2 );
        expect( ProjectCollection.findReported.calls.argsFor( 0 ) ).toEqual( [_.extend( query, { groups: { $in: ['group1', 'group2'] } } ), { fields: { _id: 1 } }] );
        expect( ProjectCollection.findReported.calls.argsFor( 1 ) ).toEqual( [_.extend( query, { groups: { $in: ['group1', 'group2'] } } ), _.extend( options, { fields: jasmine.any( Object ) } )] );
        expect( context.ready ).toHaveBeenCalled(); // Composite
    } );

    it( 'should not publish when not allowed', function () {
        GroupSecurity.canModerateGroups.and.returnValue( false );

        expect( () => publication.call( context, _.clone( query ), _.clone( options ) ) ).not.toThrow();

        expect( GroupSecurity.canModerateGroups ).toHaveBeenCalledWith( context.userId );
        expect( UserHelpers.isModerator ).not.toHaveBeenCalled( );
        expect( GroupHelpers.getGroupsForUser ).not.toHaveBeenCalled();
        expect( Counts.publish ).not.toHaveBeenCalled();
        expect( ProjectCollection.findReported.calls.count() ).toEqual( 0 );
        expect( context.ready ).toHaveBeenCalled(); // Composite
    } );

    it( 'should support null queries', function () {
        UserHelpers.isModerator.and.returnValue( true );
        GroupSecurity.canModerateGroups.and.returnValue( true );
        ProjectCollection.findReported.and.returnValues( 'cursor', null );

        expect( () => publication.call( context, null, _.clone( options ) ) ).not.toThrow();

        expect( GroupSecurity.canModerateGroups ).toHaveBeenCalledWith( context.userId );
        expect( UserHelpers.isModerator ).toHaveBeenCalledWith( context.userId );
        expect( GroupHelpers.getGroupsForUser ).not.toHaveBeenCalled();
        expect( Counts.publish ).toHaveBeenCalledWith( context, 'project/moderation/report/count/', 'cursor', { noReady: true } );
        expect( ProjectCollection.findReported.calls.count() ).toEqual( 2 );
        expect( ProjectCollection.findReported.calls.argsFor( 0 ) ).toEqual( [{}, { fields: { _id: 1 } }] );
        expect( ProjectCollection.findReported.calls.argsFor( 1 ) ).toEqual( [{}, _.extend( options, { fields: jasmine.any( Object ) } )] );
    } );

    it( 'should support null options', function () {
        UserHelpers.isModerator.and.returnValue( true );
        GroupSecurity.canModerateGroups.and.returnValue( true );
        ProjectCollection.findReported.and.returnValues( 'cursor', null );

        expect( () => publication.call( context, _.clone( query ), null ) ).not.toThrow();

        expect( GroupSecurity.canModerateGroups ).toHaveBeenCalledWith( context.userId );
        expect( UserHelpers.isModerator ).toHaveBeenCalledWith( context.userId );
        expect( GroupHelpers.getGroupsForUser ).not.toHaveBeenCalled();
        expect( Counts.publish ).toHaveBeenCalledWith( context, 'project/moderation/report/count/', 'cursor', { noReady: true } );
        expect( ProjectCollection.findReported.calls.count() ).toEqual( 2 );
        expect( ProjectCollection.findReported.calls.argsFor( 0 ) ).toEqual( [query, { fields: { _id: 1 } }] );
        expect( ProjectCollection.findReported.calls.argsFor( 1 ) ).toEqual( [query, _.extend( {}, { fields: jasmine.any( Object ) } )] );
    } );

    it( 'should not support queries (for now)', function () {
        query.enabled = true;
        expect( () => publication.call( context, query, _.clone( options ) ) ).toThrowError( /Match error/ );
        expect( UserHelpers.isModerator ).not.toHaveBeenCalled();
        expect( GroupSecurity.canModerateGroups ).not.toHaveBeenCalled();
        expect( GroupHelpers.getGroupsForUser ).not.toHaveBeenCalled();
        expect( Counts.publish ).not.toHaveBeenCalled();
        expect( ProjectCollection.findReported.calls.count() ).toEqual( 0 );
        expect( context.ready ).not.toHaveBeenCalled();
    } );

    it( 'should support sort option', function () {
        options.sort = { 'profile.firstName': 1 };
        UserHelpers.isModerator.and.returnValue( true );
        GroupSecurity.canModerateGroups.and.returnValue( true );
        ProjectCollection.findReported.and.returnValues( 'cursor', null );

        expect( () => publication.call( context, _.clone( query ), _.clone( options ) ) ).not.toThrow();

        expect( GroupSecurity.canModerateGroups ).toHaveBeenCalledWith( context.userId );
        expect( UserHelpers.isModerator ).toHaveBeenCalledWith( context.userId );
        expect( GroupHelpers.getGroupsForUser ).not.toHaveBeenCalled();
        expect( Counts.publish ).toHaveBeenCalledWith( context, 'project/moderation/report/count/', 'cursor', { noReady: true } );
        expect( ProjectCollection.findReported.calls.count() ).toEqual( 2 );
        expect( ProjectCollection.findReported.calls.argsFor( 0 ) ).toEqual( [query, { fields: { _id: 1 } }] );
        expect( ProjectCollection.findReported.calls.argsFor( 1 ) ).toEqual( [query, _.extend( options, { fields: jasmine.any( Object ) } )] );
    } );

    it( 'should not support invalid sort option', function () {
        options.sort = 'name';
        expect( () => publication.call( context, query, _.clone( options ) ) ).toThrowError( /Match error/ );
        expect( UserHelpers.isModerator ).not.toHaveBeenCalled();
        expect( GroupSecurity.canModerateGroups ).not.toHaveBeenCalled();
        expect( GroupHelpers.getGroupsForUser ).not.toHaveBeenCalled();
        expect( Counts.publish ).not.toHaveBeenCalled();
        expect( ProjectCollection.findReported.calls.count() ).toEqual( 0 );
        expect( context.ready ).not.toHaveBeenCalled();
    } );

    it( 'should support limit option', function () {
        options.limit = 10;
        UserHelpers.isModerator.and.returnValue( true );
        GroupSecurity.canModerateGroups.and.returnValue( true );
        ProjectCollection.findReported.and.returnValues( 'cursor', null );

        expect( () => publication.call( context, _.clone( query ), _.clone( options ) ) ).not.toThrow();

        expect( GroupSecurity.canModerateGroups ).toHaveBeenCalledWith( context.userId );
        expect( UserHelpers.isModerator ).toHaveBeenCalledWith( context.userId );
        expect( GroupHelpers.getGroupsForUser ).not.toHaveBeenCalled();
        expect( Counts.publish ).toHaveBeenCalledWith( context, 'project/moderation/report/count/', 'cursor', { noReady: true } );
        expect( ProjectCollection.findReported.calls.count() ).toEqual( 2 );
        expect( ProjectCollection.findReported.calls.argsFor( 0 ) ).toEqual( [query, { fields: { _id: 1 } }] );
        expect( ProjectCollection.findReported.calls.argsFor( 1 ) ).toEqual( [query, _.extend( options, { fields: jasmine.any( Object ) } )] );
    } );

    it( 'should not support invalid limit option', function () {
        options.limit = 'ten';
        expect( () => publication.call( context, query, _.clone( options ) ) ).toThrowError( /Match error/ );
        expect( UserHelpers.isModerator ).not.toHaveBeenCalled();
        expect( GroupSecurity.canModerateGroups ).not.toHaveBeenCalled();
        expect( GroupHelpers.getGroupsForUser ).not.toHaveBeenCalled();
        expect( Counts.publish ).not.toHaveBeenCalled();
        expect( ProjectCollection.findReported.calls.count() ).toEqual( 0 );
        expect( context.ready ).not.toHaveBeenCalled();
    } );

    it( 'should support limit option', function () {
        options.limit = 10;
        UserHelpers.isModerator.and.returnValue( true );
        GroupSecurity.canModerateGroups.and.returnValue( true );
        ProjectCollection.findReported.and.returnValues( 'cursor', null );

        expect( () => publication.call( context, _.clone( query ), _.clone( options ) ) ).not.toThrow();

        expect( GroupSecurity.canModerateGroups ).toHaveBeenCalledWith( context.userId );
        expect( UserHelpers.isModerator ).toHaveBeenCalledWith( context.userId );
        expect( GroupHelpers.getGroupsForUser ).not.toHaveBeenCalled();
        expect( Counts.publish ).toHaveBeenCalledWith( context, 'project/moderation/report/count/', 'cursor', { noReady: true } );
        expect( ProjectCollection.findReported.calls.count() ).toEqual( 2 );
        expect( ProjectCollection.findReported.calls.argsFor( 0 ) ).toEqual( [query, { fields: { _id: 1 } }] );
        expect( ProjectCollection.findReported.calls.argsFor( 1 ) ).toEqual( [query, _.extend( options, { fields: jasmine.any( Object ) } )] );
        expect( context.ready ).toHaveBeenCalled(); // Composite
    } );

    it( 'should not support invalid limit option', function () {
        options.limit = 'ten';
        expect( () => publication.call( context, query, _.clone( options ) ) ).toThrowError( /Match error/ );
        expect( UserHelpers.isModerator ).not.toHaveBeenCalled();
        expect( GroupSecurity.canModerateGroups ).not.toHaveBeenCalled();
        expect( GroupHelpers.getGroupsForUser ).not.toHaveBeenCalled();
        expect( Counts.publish ).not.toHaveBeenCalled();
        expect( ProjectCollection.findReported.calls.count() ).toEqual( 0 );
        expect( context.ready ).not.toHaveBeenCalled();
    } );

    it( 'should not support extra options', function () {
        options.extra = 'option';
        expect( () => publication.call( context, query, _.clone( options ) ) ).toThrowError( /Match error/ );
        expect( UserHelpers.isModerator ).not.toHaveBeenCalled();
        expect( GroupSecurity.canModerateGroups ).not.toHaveBeenCalled();
        expect( GroupHelpers.getGroupsForUser ).not.toHaveBeenCalled();
        expect( Counts.publish ).not.toHaveBeenCalled();
        expect( ProjectCollection.findReported.calls.count() ).toEqual( 0 );
        expect( context.ready ).not.toHaveBeenCalled();
    } );

} );
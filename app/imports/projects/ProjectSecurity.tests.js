"use strict";

import {chai, assert, expect} from 'meteor/practicalmeteor:chai';
import {sinon, spies, stubs} from 'meteor/practicalmeteor:sinon';

import GroupSecurity from "/imports/groups/GroupSecurity";
import ProjectCollection from "/imports/projects/ProjectCollection";
import ProjectSecurity from "/imports/projects/ProjectSecurity";
import UserHelpers from "/imports/accounts/UserHelpers";

describe( 'projects/ProjectSecurity', function () {

    let sandbox;

    beforeEach( () => {
        sandbox = sinon.sandbox.create();
    } );

    afterEach( () => {
        sandbox.restore();
    } );

    describe( 'canRead', function () {

        let project;

        beforeEach( function () {
            project = { _id: 'projectId', public: false, groups: ['group1', 'group2'] };
            sandbox.stub( UserHelpers, 'isExpert' );
            sandbox.stub( ProjectCollection, 'findOne' ).returns( project );
            sandbox.stub( GroupSecurity, 'canRead' );
        } );

        it( 'should return true if a project can be read by a global expert user', function () {
            UserHelpers.isExpert.returns( true );

            expect( ProjectSecurity.canRead( 'userId', 'projectId' ) ).to.be.true;

            assert( UserHelpers.isExpert.calledWith( 'userId' ) );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( GroupSecurity.canRead.called );
        } );

        it( 'should not fetch the project if already has the right values (already public)', function () {
            UserHelpers.isExpert.returns( false );

            expect( ProjectSecurity.canRead( 'userId', { public: true } ) ).to.be.true;

            assert( UserHelpers.isExpert.calledWith( 'userId' ) );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( GroupSecurity.canRead.called );
        } );

        it( 'should not fetch the project if already has the right values (already public, with groups)', function () {
            UserHelpers.isExpert.returns( false );

            expect( ProjectSecurity.canRead( 'userId', { public: true, groups: ['group1', 'group2'] } ) ).to.be.true;

            assert( UserHelpers.isExpert.calledWith( 'userId' ) );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( GroupSecurity.canRead.called );
        } );

        it( 'should not fetch the project if already has the right values (not public, with groups)', function () {
            UserHelpers.isExpert.returns( false );
            GroupSecurity.canRead.returns( true );

            expect( ProjectSecurity.canRead( 'userId', { public: false, groups: ['group1', 'group2'] } ) ).to.be.true;

            assert( UserHelpers.isExpert.calledWith( 'userId' ) );
            assert.isFalse( ProjectCollection.findOne.called );
            assert( GroupSecurity.canRead.calledWith( 'userId', 'group1', true ) );
        } );

        it( 'should fetch the project if it does not have the right values (only _id)', function () {
            UserHelpers.isExpert.returns( false );
            GroupSecurity.canRead.returns( true );

            expect( ProjectSecurity.canRead( 'userId', { _id: 'projectId' } ) ).to.be.true;

            assert( UserHelpers.isExpert.calledWith( 'userId' ) );
            assert( ProjectCollection.findOne.calledWith( { _id: 'projectId' }, { fields: { public: 1, groups: 1 }, transform: null } ) );
            assert( GroupSecurity.canRead.calledWith( 'userId', 'group1', true ) );
        } );

        it( 'should return false if a project is not found', function () {
            UserHelpers.isExpert.returns( false );
            ProjectCollection.findOne.returns( null );

            expect( ProjectSecurity.canRead( 'userId', 'projectId' ) ).to.be.false;

            assert( UserHelpers.isExpert.calledWith( 'userId' ) );
            assert( ProjectCollection.findOne.calledWith( { _id: 'projectId' }, { fields: { public: 1, groups: 1 }, transform: null } ) );
            assert.isFalse( GroupSecurity.canRead.called );
        } );

        it( 'should return true if a project is public and the user is not a global expert', function () {
            project.public = true;
            UserHelpers.isExpert.returns( false );

            expect( ProjectSecurity.canRead( 'userId', 'projectId' ) ).to.be.true;

            assert( UserHelpers.isExpert.calledWith( 'userId' ) );
            assert( ProjectCollection.findOne.calledWith( { _id: 'projectId' }, { fields: { public: 1, groups: 1 }, transform: null } ) );
            assert.isFalse( GroupSecurity.canRead.called );
        } );

        it( 'should return true if a project is not public but the user has access to one of the parent groups', function () {
            UserHelpers.isExpert.returns( false );
            GroupSecurity.canRead.returns( true );

            expect( ProjectSecurity.canRead( 'userId', 'projectId' ) ).to.be.true;

            assert( UserHelpers.isExpert.calledWith( 'userId' ) );
            assert( ProjectCollection.findOne.calledWith( { _id: 'projectId' }, { fields: { public: 1, groups: 1 }, transform: null } ) );
            assert( GroupSecurity.canRead.calledWith( 'userId', 'group1', true ) );
        } );

        it( 'should return true if a project is public and the user is anonymous', function () {
            project.public = true; // Actually just returning the project is sufficient in that case
            UserHelpers.isExpert.returns( false );

            expect( ProjectSecurity.canRead( null, 'projectId' ) ).to.be.true;

            assert( UserHelpers.isExpert.calledWith( null ) );
            assert( ProjectCollection.findOne.calledWith( { _id: 'projectId', public: true }, { fields: { _id: 1, public: 1 }, transform: null } ) );
            assert.isFalse( GroupSecurity.canRead.called );
        } );

        it( 'should return true if a project already is public and the user is anonymous', function () {
            project.public = true; // Actually just returning the project is sufficient in that case
            UserHelpers.isExpert.returns( false );

            expect( ProjectSecurity.canRead( null, { public: true } ) ).to.be.true;

            assert( UserHelpers.isExpert.calledWith( null ) );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( GroupSecurity.canRead.called );
        } );

        it( 'should return true if a project has an _id and the user is anonymous', function () {
            project.public = true; // Actually just returning the project is sufficient in that case
            UserHelpers.isExpert.returns( false );

            expect( ProjectSecurity.canRead( null, 'projectId' ) ).to.be.true;

            assert( UserHelpers.isExpert.calledWith( null ) );
            assert( ProjectCollection.findOne.calledWith( { _id: 'projectId', public: true }, { fields: { _id: 1, public: 1 }, transform: null } ) );
            assert.isFalse( GroupSecurity.canRead.called );
        } );

        it( 'should return false if a project is not public and the user is anonymous', function () {
            UserHelpers.isExpert.returns( false );
            ProjectCollection.findOne.returns( null );

            expect( ProjectSecurity.canRead( null, { _id: 'projectId' } ) ).to.be.false;

            assert( UserHelpers.isExpert.calledWith( null ) );
            assert( ProjectCollection.findOne.calledWith( { _id: 'projectId', public: true }, { fields: { _id: 1, public: 1 }, transform: null } ) );
            assert.isFalse( GroupSecurity.canRead.called );
        } );

        it( 'should throw on invalid project (number)', function () {
            expect( () => ProjectSecurity.canRead( 'userId', 123 ) ).to.throw( /Match error/ );

            assert.isFalse( UserHelpers.isExpert.called );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( GroupSecurity.canRead.called );
        } );

        it( 'should throw on invalid project (no id or public or groups)', function () {
            expect( () => ProjectSecurity.canRead( 'userId', { name: 'project' } ) ).to.throw( /Match error/ );

            assert.isFalse( UserHelpers.isExpert.called );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( GroupSecurity.canRead.called );
        } );

        it( 'should throw on null project', function () {
            expect( () => ProjectSecurity.canRead( 'userId', null ) ).to.throw( /Match error/ );

            assert.isFalse( UserHelpers.isExpert.called );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( GroupSecurity.canRead.called );
        } );

        it( 'should throw on projects without an _id', function () {
            expect( () => ProjectSecurity.canRead( 'userId', { name: 'project' } ) ).to.throw( /Match error/ );

            assert.isFalse( UserHelpers.isExpert.called );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( GroupSecurity.canRead.called );
        } );

        it( 'should throw on project with public false only', function () {
            expect( () => ProjectSecurity.canRead( 'userId', { public: false } ) ).to.throw( /Match error/ );

            assert.isFalse( UserHelpers.isExpert.called );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( GroupSecurity.canRead.called );
        } );

        it( 'should throw on invalid user id', function () {
            expect( () => ProjectSecurity.canRead( 123, 'projectId', 123 ) ).to.throw( /Match error/ );

            assert.isFalse( UserHelpers.isExpert.called );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( GroupSecurity.canRead.called );
        } );

    } );

    describe( 'canEdit', function () {

        let project;

        beforeEach( function () {
            project = {};
            sandbox.stub( UserHelpers, 'isModerator' );
            sandbox.stub( ProjectCollection, 'findOne' );
            sandbox.stub( GroupSecurity, 'canModerateGroups' );
        } );

        it( 'should return true if user is a global expert', function () {
            UserHelpers.isModerator.returns( true );

            expect( ProjectSecurity.canEdit( 'userId', 'whatever' ) ).to.be.true;

            assert( UserHelpers.isModerator.calledWith( 'userId' ) );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( GroupSecurity.canModerateGroups.called );
        } );

        it( 'should return true if the user can edit at least one of the parent groups', function () {
            project = { groups: ['group1', 'group2'] };
            UserHelpers.isModerator.returns( false );
            GroupSecurity.canModerateGroups.returns( true );

            expect( ProjectSecurity.canEdit( 'userId', project ) ).to.be.true;

            assert( UserHelpers.isModerator.calledWith( 'userId' ) );
            assert.isFalse( ProjectCollection.findOne.called );
            assert( GroupSecurity.canModerateGroups.calledWith( 'userId', ['group1', 'group2'] ) );
        } );

        it( 'should return false if the user can\'t edit any of the parent groups', function () {
            project = { groups: ['group1', 'group2'] };
            UserHelpers.isModerator.returns( false );
            GroupSecurity.canModerateGroups.returns( false );

            expect( ProjectSecurity.canEdit( 'userId', project ) ).to.be.false;

            assert( UserHelpers.isModerator.calledWith( 'userId' ) );
            assert.isFalse( ProjectCollection.findOne.called );
            assert( GroupSecurity.canModerateGroups.calledWith( 'userId', ['group1', 'group2'] ) );
        } );

        it( 'should fetch the project if passed a string', function () {
            project = { groups: ['group1', 'group2'] };
            UserHelpers.isModerator.returns( false );
            ProjectCollection.findOne.returns( project );
            GroupSecurity.canModerateGroups.returns( true );

            expect( ProjectSecurity.canEdit( 'userId', 'projectId' ) ).to.be.true;

            assert( UserHelpers.isModerator.calledWith( 'userId' ) );
            assert( ProjectCollection.findOne.calledWith( { _id: 'projectId' }, { fields: { groups: 1 }, transform: null } ) );
            assert( GroupSecurity.canModerateGroups.calledWith( 'userId', ['group1', 'group2'] ) );
        } );

        it( 'should fetch the project if it doesn\'t contain the groups', function () {
            project = { groups: ['group1', 'group2'] };
            UserHelpers.isModerator.returns( false );
            ProjectCollection.findOne.returns( project );
            GroupSecurity.canModerateGroups.returns( true );

            expect( ProjectSecurity.canEdit( 'userId', { _id: 'projectId' } ) ).to.be.true;

            assert( UserHelpers.isModerator.calledWith( 'userId' ) );
            assert( ProjectCollection.findOne.calledWith( { _id: 'projectId' }, { fields: { groups: 1 }, transform: null } ) );
            assert( GroupSecurity.canModerateGroups.calledWith( 'userId', ['group1', 'group2'] ) );
        } );

        it( 'should return false if the project cannot be found', function () {
            UserHelpers.isModerator.returns( false );
            ProjectCollection.findOne.returns( null );

            expect( ProjectSecurity.canEdit( 'userId', 'projectId' ) ).to.be.false;

            assert( UserHelpers.isModerator.calledWith( 'userId' ) );
            assert( ProjectCollection.findOne.calledWith( { _id: 'projectId' }, { fields: { groups: 1 }, transform: null } ) );
            assert.isFalse( GroupSecurity.canModerateGroups.called );
        } );

        it( 'should return false if no userId provided', function () {
            UserHelpers.isModerator.returns( false );

            expect( ProjectSecurity.canEdit( null, 'projectId' ) ).to.be.false;

            assert( UserHelpers.isModerator.calledWith( null ) );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( GroupSecurity.canModerateGroups.called );
        } );

        it( 'should throw on invalid project (number)', function () {
            expect( () => ProjectSecurity.canEdit( null, 123 ) ).to.throw( /Match error/ );

            assert.isFalse( UserHelpers.isModerator.called );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( GroupSecurity.canModerateGroups.called );
        } );

        it( 'should throw on invalid project (obj w/o groups)', function () {
            expect( () => ProjectSecurity.canEdit( 'userId', { name: 'project' } ) ).to.throw( /Match error/ );

            assert.isFalse( UserHelpers.isModerator.called );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( GroupSecurity.canModerateGroups.called );
        } );

        it( 'should throw on invalid user (number)', function () {
            expect( () => ProjectSecurity.canEdit( 123, 'projectId' ) ).to.throw( /Match error/ );

            assert.isFalse( UserHelpers.isModerator.called );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( GroupSecurity.canModerateGroups.called );
        } );

    } );

    describe( 'canContribute', function () {

        beforeEach( function () {
            sandbox.stub( UserHelpers, 'isExpert' );
            sandbox.stub( ProjectCollection, 'findOne' );
            sandbox.stub( GroupSecurity, 'canRead' );
        } );

        it( 'should return true if the user is an expert', function () {
            UserHelpers.isExpert.returns( true );

            expect( ProjectSecurity.canContribute( 'userId', 'projectId' ) ).to.be.true;

            assert( UserHelpers.isExpert.calledWith( 'userId' ) );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( GroupSecurity.canRead.called );
        } );

        it( 'should fetch the project if it is a string', function () {
            UserHelpers.isExpert.returns( false );
            ProjectCollection.findOne.returns( { groups: ['group1'] } );
            GroupSecurity.canRead.returns( true );

            expect( ProjectSecurity.canContribute( 'userId', 'projectId' ) ).to.be.true;

            assert( UserHelpers.isExpert.calledWith( 'userId' ) );
            assert( ProjectCollection.findOne.calledWith( { _id: 'projectId' }, { fields: { groups: 1 }, transform: null } ) );
            assert( GroupSecurity.canRead.calledWith( 'userId', 'group1', true ) );
        } );

        it( 'should fetch the project if it has an _id', function () {
            UserHelpers.isExpert.returns( false );
            ProjectCollection.findOne.returns( { groups: ['group1'] } );
            GroupSecurity.canRead.returns( true );

            expect( ProjectSecurity.canContribute( 'userId', { _id: 'projectId' } ) ).to.be.true;

            assert( UserHelpers.isExpert.calledWith( 'userId' ) );
            assert( ProjectCollection.findOne.calledWith( { _id: 'projectId' }, { fields: { groups: 1 }, transform: null } ) );
            assert( GroupSecurity.canRead.calledWith( 'userId', 'group1', true ) );
        } );

        it( 'should not fetch the project if it already has groups', function () {
            UserHelpers.isExpert.returns( false );
            GroupSecurity.canRead.returns( true );

            expect( ProjectSecurity.canContribute( 'userId', { groups: ['group1'] } ) ).to.be.true;

            assert( UserHelpers.isExpert.calledWith( 'userId' ) );
            assert.isFalse( ProjectCollection.findOne.called );
            assert( GroupSecurity.canRead.calledWith( 'userId', 'group1', true ) );
        } );

        it( 'should return false if fetched project does not have groups', function () {
            UserHelpers.isExpert.returns( false );
            ProjectCollection.findOne.returns( { _id: 'projectId' } );

            expect( ProjectSecurity.canContribute( 'userId', 'projectId' ) ).to.be.false;

            assert( UserHelpers.isExpert.calledWith( 'userId' ) );
            assert( ProjectCollection.findOne.calledWith( { _id: 'projectId' }, { fields: { groups: 1 }, transform: null } ) );
            assert.isFalse( GroupSecurity.canRead.called );
        } );

        it( 'should return false if fetched project is null', function () {
            UserHelpers.isExpert.returns( false );
            ProjectCollection.findOne.returns( null );

            expect( ProjectSecurity.canContribute( 'userId', 'projectId' ) ).to.be.false;

            assert( UserHelpers.isExpert.calledWith( 'userId' ) );
            assert( ProjectCollection.findOne.calledWith( { _id: 'projectId' }, { fields: { groups: 1 }, transform: null } ) );
            assert.isFalse( GroupSecurity.canRead.called );
        } );

        it( 'should return false if user can\'t access any of the parent groups', function () {
            UserHelpers.isExpert.returns( false );
            GroupSecurity.canRead.returns( false );

            expect( ProjectSecurity.canContribute( 'userId', { groups: ['group1'] } ) ).to.be.false;

            assert( UserHelpers.isExpert.calledWith( 'userId' ) );
            assert.isFalse( ProjectCollection.findOne.called );
            assert( GroupSecurity.canRead.calledWith( 'userId', 'group1', true ) );
        } );

        it( 'should return false if user is undefined', function () {
            UserHelpers.isExpert.returns( false );

            expect( ProjectSecurity.canContribute( null, 'projectId' ) ).to.be.false;

            assert( UserHelpers.isExpert.calledWith( null ) );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( GroupSecurity.canRead.called );
        } );

        it( 'should return false if user is null', function () {
            UserHelpers.isExpert.returns( false );

            expect( ProjectSecurity.canContribute( null, 'projectId' ) ).to.be.false;

            assert( UserHelpers.isExpert.calledWith( null ) );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( GroupSecurity.canRead.called );
        } );

        function expectCheckFailed() {
            assert.isFalse( UserHelpers.isExpert.called );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( GroupSecurity.canRead.called );
        }

        it( 'should throw on invalid project (number)', function () {
            expect( ()=>ProjectSecurity.canContribute( 'userId', 123 ) ).to.throw( /Match error/ );
            expectCheckFailed();
        } );

        it( 'should throw on invalid project (no _id nor groups)', function () {
            expect( ()=>ProjectSecurity.canContribute( 'userId', { name: 'project' } ) ).to.throw( /Match error/ );
            expectCheckFailed();
        } );

        it( 'should throw on invalid project (groups not an array of string)', function () {
            expect( ()=>ProjectSecurity.canContribute( 'userId', { groups: 'none' } ) ).to.throw( /Match error/ );
            expectCheckFailed();
        } );

        it( 'should throw on invalid user (number)', function () {
            expect( ()=>ProjectSecurity.canContribute( 123, 'projectId' ) ).to.throw( /Match error/ );
            expectCheckFailed();
        } );

    } );

} );
"use strict";

import CollectionHelpers from "/imports/collections/CollectionHelpers";
import ModerationHelpers from "/imports/moderation/ModerationHelpers";

import ProjectSchema from "./model/schema/ProjectSchema";
import Project from "./model/Project";

class ProjectCollection extends Mongo.Collection {
    constructor( name, options = {} ) {
        // Change transform before calling super, this is the only way to set it
        // Also, this won't suffer from module loading as it is a callback
        options.transform = project => new Project( project );

        super( name, options );

        this.entityName = 'project';

        /**
         * Schema
         */

        this.attachSchema( ProjectSchema );

        /**
         * Indexes
         */

        if ( Meteor.isServer ) {
            // Group project list
            this._ensureIndex( { moderated: 1, public: 1, groups: 1 }, { name: 'moderated_public_groups' } );
        }

        /**
         * Behaviors
         */

        this.attachBehaviour( 'timestampable' );

        /**
         * Security
         */

        this.deny( {
            insert: () => true,
            update: () => true,
            remove: () => true
        } );

        // The rest goes into a Meteor.startup because since we create the collection instance
        // right away when exporting from this module, we end up executing this constructor
        // before the imported modules are available (when there are circular dependencies).
        // This way we are sure the app is loaded when we execute the following code.
        Meteor.startup( () => {

            /**
             * Registration
             * TODO: Get rid of that system
             */

            CollectionHelpers.registerCollection( this.entityName, this, Project );

            /**
             * Helpers
             */

            ModerationHelpers.setup( this, Project );
        } );
    }
}

export default new ProjectCollection( "projects" );
"use strict";

import {chai, assert, expect} from "meteor/practicalmeteor:chai";
import {sinon, spies, stubs} from "meteor/practicalmeteor:sinon";
import UserCollection from "/imports/accounts/UserCollection";
import GroupSecurity from "/imports/groups/GroupSecurity";
import GroupCollection from "/imports/groups/GroupCollection";
import CardCollection from "/imports/cards/CardCollection";
import CardMethods from "/imports/cards/CardMethods";
import ProjectCollection from "/imports/projects/ProjectCollection";
import ProjectMethods from "/imports/projects/ProjectMethods";
import ProjectSecurity from "/imports/projects/ProjectSecurity";
import PhaseTypes from "/imports/projects/phases/PhaseTypes";

describe( 'projects/ProjectMethods', () => {

    let sandbox;

    beforeEach( () => {
        sandbox = sinon.sandbox.create();
    } );

    afterEach( () => {
        sandbox.restore();
    } );


    describe( 'create', () => {

        let context;
        let projectInfo;
        let cleanProjectInfo;

        beforeEach( () => {
            context = { userId: 'userId' };

            projectInfo      = {
                name:        'Test project',
                description: "This is a test project."
            };
            cleanProjectInfo = {
                name:         'Test project',
                //name_sort:    'test project',
                //slug:         'test-project',
                description:  "This is a test project.",
                //type:         'challenge',
                public:       false,
                showTimeline: true,
                groups:       [],
                //phases:       []
            };

            sandbox.stub( ProjectSecurity, 'canCreate' );
            sandbox.stub( ProjectCollection, 'insert' );
            sandbox.stub( GroupSecurity, 'canManageGroups' );
        } );

        describe( 'check successful', () => {

            afterEach( () => {
                assert( ProjectSecurity.canCreate.calledWith( "userId", cleanProjectInfo ) );
            } );

            it( 'should create a project if allowed', () => {
                ProjectSecurity.canCreate.returns( true );
                GroupSecurity.canManageGroups.returns( true );

                expect( () => ProjectMethods.create._execute( context, { project: _.clone( projectInfo ) } ) ).to.not.throw();

                assert.isFalse( GroupSecurity.canManageGroups.called );
                assert( ProjectCollection.insert.calledWith( cleanProjectInfo ) );
            } );

            it( 'should not create a project if now allowed', () => {
                ProjectSecurity.canCreate.returns( false );

                expect( () => ProjectMethods.create._execute( context, { project: _.clone( projectInfo ) } ) ).to.throw();

                assert.isFalse( GroupSecurity.canManageGroups.called );
                assert.isFalse( ProjectCollection.insert.called );
            } );

            it( 'should clean extra fields', () => {
                ProjectSecurity.canCreate.returns( true );
                GroupSecurity.canManageGroups.returns( true );

                projectInfo.pouet = 'pouet';
                expect( () => ProjectMethods.create._execute( context, { project: _.clone( projectInfo ) } ) ).to.not.throw();

                assert.isFalse( GroupSecurity.canManageGroups.called );
                assert( ProjectCollection.insert.calledWith( cleanProjectInfo ) );
            } );

            it( 'should not check for group permissions when no groups specified', () => {
                ProjectSecurity.canCreate.returns( true );

                expect( () => ProjectMethods.create._execute( context, { project: _.clone( projectInfo ) } ) ).to.not.throw();

                assert.isFalse( GroupSecurity.canManageGroups.called );
            } );

            it( 'should check groups for user permission', () => {
                ProjectSecurity.canCreate.returns( true );
                GroupSecurity.canManageGroups.returns( true );

                projectInfo.groups      = ["groupId1"];
                cleanProjectInfo.groups = ["groupId1"];
                expect( () => ProjectMethods.create._execute( context, { project: _.clone( projectInfo ) } ) ).to.not.throw();

                assert( GroupSecurity.canManageGroups.calledWith( "userId", projectInfo.groups, false, true ) );
                assert( ProjectCollection.insert.calledWith( cleanProjectInfo ) );
            } );

        } );

        describe( 'check failed', () => {

            afterEach( () => {
                assert.isFalse( ProjectSecurity.canCreate.called );
                assert.isFalse( ProjectCollection.insert.called );
            } );

            it( 'should fail on missing name', () => {
                delete projectInfo.name;
                expect( () => ProjectMethods.create._execute( context, { project: _.clone( projectInfo ) } ) ).to.throw( /validation-error/ );
            } );

            it( 'should fail on missing description', () => {
                delete projectInfo.description;
                expect( () => ProjectMethods.create._execute( context, { project: _.clone( projectInfo ) } ) ).to.throw( /validation-error/ );
            } );

        } );

    } );

    describe( 'update', () => {

        let context;
        let projectInfo;
        let modifier;

        beforeEach( () => {
            context = { userId: 'userId' };

            projectInfo = { name: 'some name' };
            modifier    = { $set: { name: 'some name' } }; // Id will be there but will be cleaned on update

            sandbox.stub( ProjectSecurity, 'canUpdate' );
            sandbox.stub( GroupSecurity, 'canManageGroups' );
            sandbox.stub( ProjectCollection, 'findOne' );
            sandbox.stub( ProjectCollection, 'update' );
        } );

        describe( 'check successful', () => {

            it( 'should update a project if allowed', () => {
                ProjectSecurity.canUpdate.returns( true );
                ProjectCollection.findOne.returns( { _id: "projectId" } );

                expect( () => ProjectMethods.update._execute( context, {
                    _id:     "projectId",
                    changes: _.clone( projectInfo )
                } ) ).to.not.throw();

                assert( ProjectSecurity.canUpdate.calledWith( "userId", "projectId" ) );
                assert( ProjectCollection.findOne.calledWith( { _id: "projectId" }, {
                    fields:    { _id: 1 },
                    transform: null
                } ) );
                assert( ProjectCollection.update.calledWith( { _id: "projectId" }, modifier ) );
            } );

            it( 'should not update a project if nothing to update', () => {
                ProjectSecurity.canUpdate.returns( true );
                ProjectCollection.findOne.returns( { _id: "projectId" } );

                delete projectInfo.name;
                expect( () => ProjectMethods.update._execute( context, {
                    _id:     "projectId",
                    changes: _.clone( projectInfo )
                } ) ).to.not.throw();

                assert( ProjectCollection.findOne.calledWith( { _id: "projectId" }, {
                    fields:    { _id: 1 },
                    transform: null
                } ) );
                assert.isFalse( ProjectCollection.update.called );
                assert( ProjectSecurity.canUpdate.calledWith( "userId", "projectId" ) );
            } );

            it( 'should not update a project if not allowed', () => {
                ProjectSecurity.canUpdate.returns( false );
                ProjectCollection.findOne.returns( { _id: "projectId" } );

                expect( () => ProjectMethods.update._execute( context, {
                    _id:     "projectId",
                    changes: _.clone( projectInfo )
                } ) ).to.throw();

                assert.isFalse( ProjectCollection.findOne.called );
                assert.isFalse( ProjectCollection.update.called );
            } );

            it( 'should throw if project is not found' );

        } );

        describe( 'check failed', () => {

            it( 'should fail on missing id', () => {
                expect( () => ProjectMethods.update._execute( context, {
                    changes: projectInfo
                } ) ).to.throw( /validation-error/ );

                assert.isFalse( ProjectSecurity.canUpdate.called );
                assert.isFalse( GroupSecurity.canManageGroups.called );
                assert.isFalse( ProjectCollection.findOne.called );
                assert.isFalse( ProjectCollection.update.called );
            } );

        } );

    } );

    describe( 'remove', () => {

        let context;
        let projectId;

        beforeEach( () => {
            context = { userId: 'userId' };

            projectId = 'projectId';

            sandbox.stub( ProjectSecurity, 'canRemove' );
            sandbox.stub( ProjectCollection, 'remove' );
        } );

        describe( 'check successful', () => {

            it( 'should remove a project if allowed', () => {
                ProjectSecurity.canRemove.returns( true );

                expect( () => ProjectMethods.remove._execute( context, { projectId: projectId } ) ).to.not.throw();

                assert( ProjectSecurity.canRemove.calledWith( "userId", "projectId" ) );
                assert( ProjectCollection.remove.calledWith( { _id: projectId } ) );
            } );

            it( 'should not remove a project if now allowed', () => {
                ProjectSecurity.canRemove.returns( false );

                expect( () => ProjectMethods.remove._execute( context, { projectId: projectId } ) ).to.throw();

                assert( ProjectSecurity.canRemove.calledWith( "userId", "projectId" ) );
                assert.isFalse( ProjectCollection.remove.called );
            } );

        } );

        describe( 'check failed', () => {

            /* NOT ANYMORE, ITS GOING TO BE CONVERTED TO STRING it( 'should fail on bad id (number)', () => {
             expect( () => ProjectMethods.remove._execute( context, { projectId: 123 } ) ).to.throw( /validation-error/ );

             assert.isFalse( ProjectSecurity.canRemove.called );
             assert.isFalse( ProjectCollection.remove.called );
             } ); */

            it( 'should fail on bad id (null)', () => {
                expect( () => ProjectMethods.remove._execute( context, { projectId: null } ) ).to.throw( /validation-error/ );

                assert.isFalse( ProjectSecurity.canRemove.called );
                assert.isFalse( ProjectCollection.remove.called );
            } );

            it( 'should fail on bad id (missing)', () => {
                expect( () => ProjectMethods.remove._execute( context, {} ) ).to.throw( /validation-error/ );

                assert.isFalse( ProjectSecurity.canRemove.called );
                assert.isFalse( ProjectCollection.remove.called );
            } );

        } );

    } );

    describe( 'addGroup', () => {

        let context;
        let modifier;

        beforeEach( ()=> {
            context  = { userId: 'userId' };
            modifier = { $addToSet: { groups: 'groupId' } };

            sandbox.stub( ProjectSecurity, 'canEdit' );
            sandbox.stub( GroupSecurity, 'canManageGroups' );
            sandbox.stub( ProjectCollection, 'findOne' );
            sandbox.stub( GroupCollection, 'findOne' );
            sandbox.stub( ProjectCollection, 'update' );
        } );

        it( 'should add group if allowed', () => {
            ProjectSecurity.canEdit.returns( true );
            GroupSecurity.canManageGroups.returns( true );
            ProjectCollection.findOne.returns( { _id: 'projectId' } );
            GroupCollection.findOne.returns( { _id: 'groupId' } );

            expect( ()=>ProjectMethods.addGroup._execute( context, {
                projectId: 'projectId',
                groupId:   'groupId'
            } ) ).to.not.throw();

            assert( ProjectSecurity.canEdit.calledWith( "userId", "projectId" ) );
            assert( GroupSecurity.canManageGroups.calledWith( "userId", "groupId" ) );
            assert( ProjectCollection.findOne.calledWith( { _id: 'projectId' }, {
                fields:    { _id: 1 },
                transform: null
            } ) );
            assert( GroupCollection.findOne.calledWith( { _id: 'groupId' }, { fields: { _id: 1 }, transform: null } ) );
            assert( ProjectCollection.update.calledWith( { _id: 'projectId' }, modifier ) );
        } );

        it( 'should not add group if not allowed to edit project', () => {
            ProjectSecurity.canEdit.returns( false );

            expect( ()=>ProjectMethods.addGroup._execute( context, {
                projectId: 'projectId',
                groupId:   'groupId'
            } ) ).to.throw( /unauthorized/ );

            assert( ProjectSecurity.canEdit.calledWith( "userId", "projectId" ) );
            assert.isFalse( GroupSecurity.canManageGroups.called );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( GroupCollection.findOne.called );
            assert.isFalse( ProjectCollection.update.called );
        } );

        it( 'should not add group if not allowed to manage the group', () => {
            ProjectSecurity.canEdit.returns( true );
            GroupSecurity.canManageGroups.returns( false );

            expect( ()=>ProjectMethods.addGroup._execute( context, {
                projectId: 'projectId',
                groupId:   'groupId'
            } ) ).to.throw( /unauthorized/ );

            assert( ProjectSecurity.canEdit.calledWith( "userId", "projectId" ) );
            assert( GroupSecurity.canManageGroups.calledWith( "userId", "groupId" ) );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( GroupCollection.findOne.called );
            assert.isFalse( ProjectCollection.update.called );
        } );

        it( 'should throw if group is not found', () => {
            ProjectSecurity.canEdit.returns( true );
            GroupSecurity.canManageGroups.returns( true );
            ProjectCollection.findOne.returns( { _id: 'projectId' } );
            GroupCollection.findOne.returns( null );

            expect( ()=>ProjectMethods.addGroup._execute( context, {
                projectId: 'projectId',
                groupId:   'groupId'
            } ) ).to.throw( /group not found/ );

            assert( ProjectSecurity.canEdit.calledWith( "userId", "projectId" ) );
            assert( GroupSecurity.canManageGroups.calledWith( "userId", "groupId" ) );
            assert( ProjectCollection.findOne.calledWith( { _id: 'projectId' }, {
                fields:    { _id: 1 },
                transform: null
            } ) );
            assert( GroupCollection.findOne.calledWith( { _id: 'groupId' }, { fields: { _id: 1 }, transform: null } ) );
            assert.isFalse( ProjectCollection.update.called );
        } );

        it( 'should throw if project is not found', () => {
            ProjectSecurity.canEdit.returns( true );
            GroupSecurity.canManageGroups.returns( true );
            ProjectCollection.findOne.returns( null );

            expect( ()=>ProjectMethods.addGroup._execute( context, {
                projectId: 'projectId',
                groupId:   'groupId'
            } ) ).to.throw( /project not found/ );

            assert( ProjectSecurity.canEdit.calledWith( "userId", "projectId" ) );
            assert( GroupSecurity.canManageGroups.calledWith( "userId", "groupId" ) );
            assert( ProjectCollection.findOne.calledWith( { _id: 'projectId' }, {
                fields:    { _id: 1 },
                transform: null
            } ) );
            assert.isFalse( GroupCollection.findOne.called );
            assert.isFalse( ProjectCollection.update.called );
        } );

        function expectMatchFailed() {
            assert.isFalse( ProjectSecurity.canEdit.called );
            assert.isFalse( GroupSecurity.canManageGroups.called );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( GroupCollection.findOne.called );
            assert.isFalse( ProjectCollection.update.called );
        }

        it( 'should throw on null projectId', () => {
            expect( ()=>ProjectMethods.addGroup._execute( context, {
                projectId: null,
                groupId:   'groupId'
            } ) ).to.throw( /validation-error/ );
            expectMatchFailed();
        } );

        it( 'should throw on undefined projectId', () => {
            expect( ()=>ProjectMethods.addGroup._execute( context, { groupId: 'groupId' } ) ).to.throw( /validation-error/ );
            expectMatchFailed();
        } );

        it( 'should throw on null groupId', () => {
            expect( ()=>ProjectMethods.addGroup._execute( context, {
                projectId: 'projectId',
                groupId:   null
            } ) ).to.throw( /validation-error/ );
            expectMatchFailed();
        } );

        it( 'should throw on undefined groupId', () => {
            expect( ()=>ProjectMethods.addGroup._execute( context, { projectId: 'projectId' } ) ).to.throw( /validation-error/ );
            expectMatchFailed();
        } );

        it( 'should throw on undefined projectId and groupId', () => {
            expect( ()=>ProjectMethods.addGroup._execute( context, {} ) ).to.throw( /validation-error/ );
            expectMatchFailed();
        } );

    } );

    describe( 'removeGroup', () => {

        let context;
        let modifier;

        beforeEach( ()=> {
            context  = { userId: 'userId' };
            modifier = { $pull: { groups: 'groupId' } };

            sandbox.stub( ProjectSecurity, 'canEdit' );
            sandbox.stub( GroupSecurity, 'canManageGroups' );
            sandbox.stub( ProjectCollection, 'findOne' );
            sandbox.stub( ProjectCollection, 'update' );
        } );

        it( 'should remove group if allowed', () => {
            ProjectSecurity.canEdit.returns( true );
            GroupSecurity.canManageGroups.returns( true );
            ProjectCollection.findOne.returns( { _id: 'projectId' } );

            expect( ()=>ProjectMethods.removeGroup._execute( context, {
                projectId: 'projectId',
                groupId:   'groupId'
            } ) ).to.not.throw();

            assert( ProjectSecurity.canEdit.calledWith( "userId", "projectId" ) );
            assert( GroupSecurity.canManageGroups.calledWith( "userId", "groupId" ) );
            assert( ProjectCollection.findOne.calledWith( { _id: 'projectId' }, {
                fields:    { _id: 1 },
                transform: null
            } ) );
            assert( ProjectCollection.update.calledWith( { _id: 'projectId' }, modifier ) );
        } );

        it( 'should not remove group if not allowed to edit project', () => {
            ProjectSecurity.canEdit.returns( false );

            expect( ()=>ProjectMethods.removeGroup._execute( context, {
                projectId: 'projectId',
                groupId:   'groupId'
            } ) ).to.throw( /unauthorized/ );

            assert( ProjectSecurity.canEdit.calledWith( "userId", "projectId" ) );
            assert.isFalse( GroupSecurity.canManageGroups.called );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( ProjectCollection.update.called );
        } );

        it( 'should not remove group if not allowed to manage group', () => {
            ProjectSecurity.canEdit.returns( true );
            GroupSecurity.canManageGroups.returns( false );

            expect( ()=>ProjectMethods.removeGroup._execute( context, {
                projectId: 'projectId',
                groupId:   'groupId'
            } ) ).to.throw( /unauthorized/ );

            assert( ProjectSecurity.canEdit.calledWith( "userId", "projectId" ) );
            assert( GroupSecurity.canManageGroups.calledWith( "userId", "groupId" ) );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( ProjectCollection.update.called );
        } );

        it( 'should throw if project is not found', () => {
            ProjectSecurity.canEdit.returns( true );
            GroupSecurity.canManageGroups.returns( true );
            ProjectCollection.findOne.returns( null );

            expect( ()=>ProjectMethods.removeGroup._execute( context, {
                projectId: 'projectId',
                groupId:   'groupId'
            } ) ).to.throw( /project not found/ );

            assert( ProjectSecurity.canEdit.calledWith( "userId", "projectId" ) );
            assert( GroupSecurity.canManageGroups.calledWith( "userId", "groupId" ) );
            assert( ProjectCollection.findOne.calledWith( { _id: 'projectId' }, {
                fields:    { _id: 1 },
                transform: null
            } ) );
            assert.isFalse( ProjectCollection.update.called );
        } );

        function expectMatchFailed() {
            assert.isFalse( ProjectSecurity.canEdit.called );
            assert.isFalse( GroupSecurity.canManageGroups.called );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( ProjectCollection.update.called );
        }

        it( 'should throw on null projectId', () => {
            expect( ()=>ProjectMethods.removeGroup._execute( context, {
                projectId: null,
                groupId:   'groupId'
            } ) ).to.throw( /validation-error/ );
            expectMatchFailed();
        } );

        it( 'should throw on undefined projectId', () => {
            expect( ()=>ProjectMethods.removeGroup._execute( context, { groupId: 'groupId' } ) ).to.throw( /validation-error/ );
            expectMatchFailed();
        } );

        it( 'should throw on null groupId', () => {
            expect( ()=>ProjectMethods.removeGroup._execute( context, {
                projectId: 'projectId',
                groupId:   null
            } ) ).to.throw( /validation-error/ );
            expectMatchFailed();
        } );

        it( 'should throw on undefined groupId', () => {
            expect( ()=>ProjectMethods.removeGroup._execute( context, { projectId: 'projectId' } ) ).to.throw( /validation-error/ );
            expectMatchFailed();
        } );

        it( 'should throw on undefined projectId and groupId', () => {
            expect( ()=>ProjectMethods.removeGroup._execute( context, {} ) ).to.throw( /validation-error/ );
            expectMatchFailed();
        } );

    } );

    describe( 'addPhase', () => {

        let context;
        let phase;
        let modifier;

        beforeEach( ()=> {
            context  = { userId: 'userId' };
            phase    = { name: 'Phase Name' };
            modifier = {
                $push: {
                    phases: sinon.match( {
                        _id:  sinon.match.string,
                        name: "Phase Name",
                        type: 'basic',
                        data: sinon.match.object
                    } )
                }
            };

            sandbox.stub( ProjectSecurity, 'canEdit' );
            sandbox.stub( ProjectCollection, 'findOne' );
            sandbox.stub( ProjectCollection, 'update' );
        } );

        it( 'should add group if allowed', () => {
            ProjectSecurity.canEdit.returns( true );
            ProjectCollection.findOne.returns( { _id: 'projectId' } );

            expect( ()=>ProjectMethods.addPhase._execute( context, {
                projectId: 'projectId',
                phase:     _.clone( phase )
            } ) ).to.not.throw();

            assert( ProjectSecurity.canEdit.calledWith( "userId", "projectId" ) );
            assert( ProjectCollection.findOne.calledWith( { _id: 'projectId' }, {
                fields:    { _id: 1 },
                transform: null
            } ) );
            assert( ProjectCollection.update.calledWith( { _id: 'projectId' }, modifier ) );
        } );

        it( 'should not add group if not allowed', () => {
            ProjectSecurity.canEdit.returns( false );

            expect( ()=>ProjectMethods.addPhase._execute( context, {
                projectId: 'projectId',
                phase:     _.clone( phase )
            } ) ).to.throw( /unauthorized/ );

            assert( ProjectSecurity.canEdit.calledWith( "userId", "projectId" ) );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( ProjectCollection.update.called );
        } );

        it( 'should throw if project is not found', () => {
            ProjectSecurity.canEdit.returns( true );
            ProjectCollection.findOne.returns( null );

            expect( ()=>ProjectMethods.addPhase._execute( context, {
                projectId: 'projectId',
                phase:     _.clone( phase )
            } ) ).to.throw( /project not found/ );

            assert( ProjectSecurity.canEdit.calledWith( "userId", "projectId" ) );
            assert( ProjectCollection.findOne.calledWith( { _id: 'projectId' }, {
                fields:    { _id: 1 },
                transform: null
            } ) );
            assert.isFalse( ProjectCollection.update.called );
        } );

        function expectMatchFailed() {
            assert.isFalse( ProjectSecurity.canEdit.called );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( ProjectCollection.update.called );
        }

        it( 'should throw on null projectId', () => {
            expect( ()=>ProjectMethods.addPhase._execute( context, {
                projectId: null,
                phase:     phase
            } ) ).to.throw( /validation-error/ );
            expectMatchFailed();
        } );

        it( 'should throw on undefined projectId', () => {
            expect( ()=>ProjectMethods.addPhase._execute( context, { phase: phase } ) ).to.throw( /validation-error/ );
            expectMatchFailed();
        } );

        it( 'should throw on phase not validated by the schema', () => {
            expect( ()=>ProjectMethods.addPhase._execute( context, {
                projectId: 'projectId',
                phase:     { not: 'a phase' }
            } ) ).to.throw( /validation-error/ );
            expectMatchFailed();
        } );

        it( 'should throw on null phase', () => {
            expect( ()=>ProjectMethods.addPhase._execute( context, {
                projectId: 'projectId',
                phase:     null
            } ) ).to.throw( /validation-error/ );
            expectMatchFailed();
        } );

        it( 'should throw on undefined phase', () => {
            expect( ()=>ProjectMethods.addPhase._execute( context, { projectId: 'projectId' } ) ).to.throw( /validation-error/ );
            expectMatchFailed();
        } );

        it( 'should throw on undefined projectId and phase', () => {
            expect( ()=>ProjectMethods.addPhase._execute( context, {} ) ).to.throw( /validation-error/ );
            expectMatchFailed();
        } );

    } );

    describe( 'updatePhase', ()=> {

        let context;

        beforeEach( ()=> {
            context = { userId: 'userId' };

            sandbox.stub( ProjectSecurity, 'canEdit' );
            sandbox.stub( ProjectCollection, 'findOne' );
            sandbox.stub( PhaseTypes, 'get' );
            sandbox.stub( ProjectCollection, 'update' );
        } );

        it( 'should update a phase', () => {
            const phaseInfo = { _id: 'phaseId', name: 'Phase name' };
            const project   = { phases: [{ _id: 'phaseId', type: 'phaseType' }] };
            const modifier  = { $set: { 'phases.$._id': 'phaseId', 'phases.$.name': 'Phase name' } };

            ProjectSecurity.canEdit.returns( true );
            ProjectCollection.findOne.returns( _.clone( project ) );
            const phaseTypeService = { update: sinon.stub() };
            PhaseTypes.get.returns( phaseTypeService );

            expect( ()=>ProjectMethods.updatePhase._execute( context, {
                projectId: 'projectId',
                phase:     _.clone( phaseInfo )
            } ) ).to.not.throw();

            assert( ProjectSecurity.canEdit.calledWith( "userId", "projectId" ) );
            assert( ProjectCollection.findOne.calledWith( {
                _id:          'projectId',
                'phases._id': 'phaseId'
            }, { fields: { 'phases._id': 1, 'phases.type': 1 } } ) );
            assert( PhaseTypes.get.calledWith( 'phaseType' ) );
            assert( phaseTypeService.update.calledWith( 'projectId', phaseInfo ) );
            assert( ProjectCollection.update.calledWith( { _id: 'projectId', 'phases._id': 'phaseId' }, modifier ) );
        } );

        it( 'should update a phase and use the new phase type', () => {
            PhaseTypes.register( 'newPhaseType', {} );

            const phaseInfo = { _id: 'phaseId', name: 'Phase name', type: 'newPhaseType' };
            const project   = { phases: [{ _id: 'phaseId', type: 'basic' }] };
            const modifier  = {
                $set: {
                    'phases.$._id':  'phaseId',
                    'phases.$.name': 'Phase name',
                    'phases.$.type': 'newPhaseType'
                }
            };

            ProjectSecurity.canEdit.returns( true );
            ProjectCollection.findOne.returns( project );
            const phaseTypeService = { update: sinon.stub() };
            PhaseTypes.get.returns( phaseTypeService );

            expect( ()=>ProjectMethods.updatePhase._execute( context, {
                projectId: 'projectId',
                phase:     _.clone( phaseInfo )
            } ) ).to.not.throw();

            assert( ProjectSecurity.canEdit.calledWith( "userId", "projectId" ) );
            assert( ProjectCollection.findOne.calledWith( {
                _id:          'projectId',
                'phases._id': 'phaseId'
            }, { fields: { 'phases._id': 1, 'phases.type': 1 } } ) );
            assert( PhaseTypes.get.calledWith( 'newPhaseType' ) );
            assert( phaseTypeService.update.calledWith( 'projectId', phaseInfo ) );
            assert( ProjectCollection.update.calledWith( { _id: 'projectId', 'phases._id': 'phaseId' }, modifier ) );
        } );

        it( 'should throw if project is not found', () => {
            const phaseInfo = { _id: 'phaseId', name: 'Phase name', extra_field: 'will not make it to the update' };

            ProjectSecurity.canEdit.returns( true );
            ProjectCollection.findOne.returns( null );

            expect( ()=>ProjectMethods.updatePhase._execute( context, {
                projectId: 'projectId',
                phase:     _.clone( phaseInfo )
            } ) ).to.throw( /project or phase not found/ );

            assert( ProjectSecurity.canEdit.calledWith( "userId", "projectId" ) );
            assert( ProjectCollection.findOne.calledWith( {
                _id:          'projectId',
                'phases._id': 'phaseId'
            }, { fields: { 'phases._id': 1, 'phases.type': 1 } } ) );
            assert.isFalse( PhaseTypes.get.called );
            assert.isFalse( ProjectCollection.update.called );
        } );

        it( 'should throw if project does not contain phases', () => {
            const phaseInfo = { _id: 'phaseId', name: 'Phase name', extra_field: 'will not make it to the update' };
            const project   = { something: 'else' };

            ProjectSecurity.canEdit.returns( true );
            ProjectCollection.findOne.returns( project );

            expect( ()=>ProjectMethods.updatePhase._execute( context, {
                projectId: 'projectId',
                phase:     _.clone( phaseInfo )
            } ) ).to.throw( /phase not found/ );

            assert( ProjectSecurity.canEdit.calledWith( "userId", "projectId" ) );
            assert( ProjectCollection.findOne.calledWith( {
                _id:          'projectId',
                'phases._id': 'phaseId'
            }, { fields: { 'phases._id': 1, 'phases.type': 1 } } ) );
            assert.isFalse( PhaseTypes.get.called );
            assert.isFalse( ProjectCollection.update.called );
        } );

        it( 'should throw if phase is not found', () => {
            const phaseInfo = { _id: 'phaseId', name: 'Phase name', extra_field: 'will not make it to the update' };
            const project   = { phases: [{ _id: 'anotherPhaseId', type: 'phaseType' }] };

            ProjectSecurity.canEdit.returns( true );
            ProjectCollection.findOne.returns( project );

            expect( ()=>ProjectMethods.updatePhase._execute( context, {
                projectId: 'projectId',
                phase:     _.clone( phaseInfo )
            } ) ).to.throw( /phase not found/ );

            assert( ProjectSecurity.canEdit.calledWith( "userId", "projectId" ) );
            assert( ProjectCollection.findOne.calledWith( {
                _id:          'projectId',
                'phases._id': 'phaseId'
            }, { fields: { 'phases._id': 1, 'phases.type': 1 } } ) );
            assert.isFalse( PhaseTypes.get.called );
            assert.isFalse( ProjectCollection.update.called );
        } );

        it( 'should throw if invalid phase type', () => {
            const phaseInfo = { _id: 'phaseId', name: 'Phase name', extra_field: 'will not make it to the update' };
            const project   = { phases: [{ _id: 'phaseId', type: 'phaseType' }] };
            PhaseTypes.get.returns( null );

            ProjectSecurity.canEdit.returns( true );
            ProjectCollection.findOne.returns( project );

            expect( ()=>ProjectMethods.updatePhase._execute( context, {
                projectId: 'projectId',
                phase:     _.clone( phaseInfo )
            } ) ).to.throw( /invalid phase type/ );

            assert( ProjectSecurity.canEdit.calledWith( "userId", "projectId" ) );
            assert( ProjectCollection.findOne.calledWith( {
                _id:          'projectId',
                'phases._id': 'phaseId'
            }, { fields: { 'phases._id': 1, 'phases.type': 1 } } ) );
            assert( PhaseTypes.get.calledWith( 'phaseType' ) );
            assert.isFalse( ProjectCollection.update.called );
        } );

        it( 'should throw if not allowed', () => {
            const phaseInfo = { _id: 'phaseId' };
            const project   = { phases: [{ _id: 'phaseId' }] };
            const modifier  = { $set: { 'phases.$._id': 'phaseId' } };

            ProjectSecurity.canEdit.returns( false );
            ProjectCollection.findOne.returns( project );

            expect( ()=>ProjectMethods.updatePhase._execute( context, {
                projectId: 'projectId',
                phase:     _.clone( phaseInfo )
            } ) ).to.throw( /unauthorized/ );

            assert( ProjectSecurity.canEdit.calledWith( "userId", "projectId" ) );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( PhaseTypes.get.called );
            assert.isFalse( ProjectCollection.update.called );
        } );

    } );

    describe( 'removePhase', () => {

        let context;

        beforeEach( ()=> {
            context = { userId: 'userId' };

            sandbox.stub( ProjectSecurity, 'canEdit' );
            sandbox.stub( ProjectCollection, 'findOne' );
            sandbox.stub( ProjectCollection, 'update' );
            sandbox.stub( CardCollection, 'find' );
            sandbox.stub( CardMethods.archive, 'call' );
            sandbox.stub( CardMethods.moveToPhase, 'call' );
        } );

        it( 'should remove phase if allowed', () => {
            ProjectSecurity.canEdit.returns( true );
            ProjectCollection.findOne.returns( { _id: 'projectId' } );
            CardCollection.find.returns([]);

            expect( ()=>ProjectMethods.removePhase._execute( context, {
                projectId: 'projectId',
                phaseId:   'phaseId',
                action: 'archive'
            } ) ).to.not.throw();

            assert( ProjectSecurity.canEdit.calledWith( "userId", "projectId" ) );
            assert( ProjectCollection.findOne.calledWith( {
                _id:          'projectId',
                'phases._id': 'phaseId'
            }, { fields: { _id: 1 }, transform: null } ) );
            assert( ProjectCollection.update.calledWith( {
                _id:          'projectId',
                'phases._id': 'phaseId'
            }, { $set: { 'phases.$.removed': true } } ) );
        } );

        it( 'should archive cards in phase', () => {
            ProjectSecurity.canEdit.returns( true );
            ProjectCollection.findOne.returns( { _id: 'projectId' } );
            CardCollection.find.returns([
                {id:'cardId'}
            ]);

            expect( ()=>ProjectMethods.removePhase._execute( context, {
                projectId: 'projectId',
                phaseId:   'phaseId',
                action: 'archive'
            } ) ).to.not.throw();

            assert( ProjectSecurity.canEdit.calledWith( "userId", "projectId" ) );
            assert( ProjectCollection.findOne.calledWith( {
                _id:          'projectId',
                'phases._id': 'phaseId'
            }, { fields: { _id: 1 }, transform: null } ) );
            assert( ProjectCollection.update.calledWith( {
                _id:          'projectId',
                'phases._id': 'phaseId'
            }, { $set: { 'phases.$.removed': true } } ) );
            assert(CardCollection.find.calledWith(
                { project: 'projectId', phase: 'phaseId', current: true },
                { fields: { id: 1 }, transform: null }
            ));
            assert(CardMethods.archive.call.calledWith(
                { cardId: 'cardId' }
            ));
        } );

        it( 'should move cards to another phase', () => {
            ProjectSecurity.canEdit.returns( true );
            ProjectCollection.findOne.returns( { _id: 'projectId' } );
            CardCollection.find.returns([
                {id:'cardId'}
            ]);

            expect( () => ProjectMethods.removePhase._execute( context, {
                projectId: 'projectId',
                phaseId:   'phaseId',
                action: 'move',
                moveToPhase: 'moveToPhase'
            } ) ).to.not.throw();

            assert( ProjectSecurity.canEdit.calledWith( "userId", "projectId" ) );
            assert( ProjectCollection.findOne.calledWith( {
                _id:          'projectId',
                'phases._id': 'phaseId'
            }, { fields: { _id: 1 }, transform: null } ) );
            assert( ProjectCollection.update.calledWith( {
                _id:          'projectId',
                'phases._id': 'phaseId'
            }, { $set: { 'phases.$.removed': true } } ) );
            assert(CardCollection.find.calledWith(
                { project: 'projectId', phase: 'phaseId', current: true },
                { fields: { id: 1 }, transform: null }
            ));
            assert(CardMethods.moveToPhase.call.calledWith(
                { cardId: 'cardId', phaseId: 'moveToPhase' }
            ));
        } );

        it( 'should not remove phase if not allowed', () => {
            ProjectSecurity.canEdit.returns( false );

            expect( ()=>ProjectMethods.removePhase._execute( context, {
                projectId: 'projectId',
                phaseId:   'phaseId',
                action: 'archive'
            } ) ).to.throw( /unauthorized/ );

            assert( ProjectSecurity.canEdit.calledWith( "userId", "projectId" ) );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( ProjectCollection.update.called );
        } );

        it( 'should throw if project is not found', () => {
            ProjectSecurity.canEdit.returns( true );

            expect( ()=>ProjectMethods.removePhase._execute( context, {
                projectId: 'projectId',
                phaseId:   'phaseId',
                action: 'archive'
            } ) ).to.throw( /phase not found/ );

            assert( ProjectSecurity.canEdit.calledWith( "userId", "projectId" ) );
            assert( ProjectCollection.findOne.calledWith( {
                _id:          'projectId',
                'phases._id': 'phaseId'
            }, { fields: { _id: 1 }, transform: null } ) );
            assert.isFalse( ProjectCollection.update.called );
        } );

        function expectMatchFailed() {
            assert.isFalse( ProjectSecurity.canEdit.called );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( ProjectCollection.update.called );
        }

        it( 'should throw on null projectId', () => {
            expect( ()=>ProjectMethods.removePhase._execute( context, {
                projectId: null,
                phaseId:   'phaseId',
                action:    'archive'
            } ) ).to.throw( /validation-error/ );
            expectMatchFailed();
        } );

        it( 'should throw on undefined projectId', () => {
            expect( ()=>ProjectMethods.removePhase._execute( context, {
                phaseId: 'phaseId',
                action:  'archive'
            } ) ).to.throw( /validation-error/ );
            expectMatchFailed();
        } );

        it( 'should throw on null phaseId', () => {
            expect( ()=>ProjectMethods.removePhase._execute( context, {
                projectId: 'projectId',
                phaseId:   null,
                action:    'archive'
            } ) ).to.throw( /validation-error/ );
            expectMatchFailed();
        } );

        it( 'should throw on undefined phaseId', () => {
            expect( ()=>ProjectMethods.removePhase._execute( context, {
                projectId: 'projectId',
                action:    'archive'
            } ) ).to.throw( /validation-error/ );
            expectMatchFailed();
        } );

        it( 'should throw on null action', () => {
            expect( ()=>ProjectMethods.removePhase._execute( context, {
                projectId: 'projectId',
                phaseId:   'phaseId',
                action:    null
            } ) ).to.throw( /validation-error/ );
            expectMatchFailed();
        } );

        it( 'should throw on undefined action', () => {
            expect( ()=>ProjectMethods.removePhase._execute( context, {
                projectId: 'projectId',
                phaseId:   'phaseId',
            } ) ).to.throw( /validation-error/ );
            expectMatchFailed();
        } );

        it( 'should throw on invalid action', () => {
            expect( ()=>ProjectMethods.removePhase._execute( context, {
                projectId: 'projectId',
                phaseId:   'phaseId',
                action:    'something'
            } ) ).to.throw( /validation-error/ );
            expectMatchFailed();
        } );

        it( 'should throw when moving without a destination phase', () => {
            expect( ()=>ProjectMethods.removePhase._execute( context, {
                projectId: 'projectId',
                phaseId:   'phaseId',
                action:    'move'
            } ) ).to.throw( /validation-error/ );
            expectMatchFailed();
        } );

    } );

    describe.skip( 'movePhase', ()=> {

        let context;

        let security_update;
        let security_for;
        let security_throw;

        beforeEach( ()=> {
            context = { userId: 'userId' };

            security_throw  = sinon.stub();
            security_for    = sinon.stub().returns( { throw: security_throw } );
            security_update = sinon.stub().returns( { for: security_for } );
            sandbox.stub( Security, 'can' ).returns( { update: security_update } );

            sandbox.stub( ProjectCollection, 'findOne' );
            sandbox.stub( ProjectCollection.direct, 'update' );
        } );

        it( 'should reorder a phase', ()=> {
            const preModifier  = { $pull: { phases: { _id: 'phaseId' } } };
            const postModifier = { $push: { phases: { $each: [{ _id: 'phaseId' }], $position: 8 } } };

            ProjectCollection.findOne.returns( { _id: 'projectId', phases: [{ _id: 'phaseId' }] } );

            expect( ()=> ProjectMethods.movePhase._execute( context, 'projectId', 'phaseId', 8 ) ).to.not.throw();

            assert( ProjectCollection.findOne.calledWith( { _id: 'projectId' }, {
                fields:    {
                    'phases': 1 // { $elemMatch: { _id: 'phaseId' } } // Minimongo doesn't support operators in projections yet.
                },
                transform: null
            } ) );

            expect( Security.can.callCount ).to.eql( 2 );
            expect( security_update.callCount ).to.eql( 2 );
            expect( security_for.callCount ).to.eql( 2 );
            expect( security_throw.callCount ).to.eql( 2 );

            expect( Security.can.getCall( 0 ).args ).to.eql( [context.userId] );
            expect( security_update.getCall( 0 ).args ).to.eql( ['projectId', preModifier] );
            expect( security_for.getCall( 0 ).args ).to.eql( [ProjectCollection] );

            expect( Security.can.getCall( 1 ).args ).to.eql( [context.userId] );
            expect( security_update.getCall( 1 ).args ).to.eql( ['projectId', postModifier] );
            expect( security_for.getCall( 1 ).args ).to.eql( [ProjectCollection] );

            expect( ProjectCollection.direct.update.callCount ).to.eql( 2 );
            expect( ProjectCollection.direct.update.getCall( 0 ).args ).to.eql( [{ _id: 'projectId' }, preModifier] );
            expect( ProjectCollection.direct.update.getCall( 1 ).args ).to.eql( [{ _id: 'projectId' }, postModifier] );
        } );

        function expectFailedBeforeUpdate() {
            assert.isFalse( Security.can.called );
            assert.isFalse( security_update.called );
            assert.isFalse( security_for.called );
            assert.isFalse( security_throw.called );
            assert.isFalse( ProjectCollection.direct.update.called );
            assert.isFalse( ProjectCollection.direct.update.called );
        }

        it( 'should throw if project is not found', ()=> {
            ProjectCollection.findOne.returns( null );
            expect( ()=> ProjectMethods.movePhase._execute( context, 'projectId', 'phaseId', 8 ) ).to.throw( /project not found/ );
            assert( ProjectCollection.findOne.calledWith( { _id: 'projectId' }, {
                fields:    {
                    'phases': 1 // { $elemMatch: { _id: 'phaseId' } } // Minimongo doesn't support operators in projections yet.
                },
                transform: null
            } ) );
            expectFailedBeforeUpdate();
        } );

        it( 'should throw if phase is not found', ()=> {
            ProjectCollection.findOne.returns( { _id: 'projectId', phases: [{ _id: 'otherPhaseId' }] } );
            expect( ()=> ProjectMethods.movePhase._execute( context, 'projectId', 'phaseId', 8 ) ).to.throw( /phase not found/ );
            assert( ProjectCollection.findOne.calledWith( { _id: 'projectId' }, {
                fields:    {
                    'phases': 1 // { $elemMatch: { _id: 'phaseId' } } // Minimongo doesn't support operators in projections yet.
                },
                transform: null
            } ) );
            expectFailedBeforeUpdate();
        } );

        it( 'should throw if no phases in project', ()=> {
            ProjectCollection.findOne.returns( { _id: 'projectId', phases: [] } );
            expect( ()=> ProjectMethods.movePhase._execute( context, 'projectId', 'phaseId', 8 ) ).to.throw( /phase not found/ );
            assert( ProjectCollection.findOne.calledWith( { _id: 'projectId' }, {
                fields:    {
                    'phases': 1 // { $elemMatch: { _id: 'phaseId' } } // Minimongo doesn't support operators in projections yet.
                },
                transform: null
            } ) );
            expectFailedBeforeUpdate();
        } );

        it( 'should throw if phases are null in project', ()=> {
            ProjectCollection.findOne.returns( { _id: 'projectId', phases: null } );
            expect( ()=> ProjectMethods.movePhase._execute( context, 'projectId', 'phaseId', 8 ) ).to.throw( /phase not found/ );
            assert( ProjectCollection.findOne.calledWith( { _id: 'projectId' }, {
                fields:    {
                    'phases': 1 // { $elemMatch: { _id: 'phaseId' } } // Minimongo doesn't support operators in projections yet.
                },
                transform: null
            } ) );
            expectFailedBeforeUpdate();
        } );

        it( 'should throw if phases are undefined in project', ()=> {
            ProjectCollection.findOne.returns( { _id: 'projectId' } );
            expect( ()=> ProjectMethods.movePhase._execute( context, 'projectId', 'phaseId', 8 ) ).to.throw( /phase not found/ );
            assert( ProjectCollection.findOne.calledWith( { _id: 'projectId' }, {
                fields:    {
                    'phases': 1 // { $elemMatch: { _id: 'phaseId' } } // Minimongo doesn't support operators in projections yet.
                },
                transform: null
            } ) );
            expectFailedBeforeUpdate();
        } );

        it( 'should throw if not allowed preModifier', () => {
            const preModifier  = { $pull: { phases: { _id: 'phaseId' } } };
            const postModifier = { $push: { phases: { $each: [{ _id: 'phaseId' }], $position: 8 } } };

            ProjectCollection.findOne.returns( { _id: 'projectId', phases: [{ _id: 'phaseId' }] } );
            security_throw.and.throw( 'security' );

            expect( ()=> ProjectMethods.movePhase._execute( context, 'projectId', 'phaseId', 8 ) ).to.throw( /security/ );

            assert( ProjectCollection.findOne.calledWith( { _id: 'projectId' }, {
                fields:    {
                    'phases': 1 // { $elemMatch: { _id: 'phaseId' } } // Minimongo doesn't support operators in projections yet.
                },
                transform: null
            } ) );

            expect( Security.can.callCount ).to.eql( 1 );
            expect( security_update.callCount ).to.eql( 1 );
            expect( security_for.callCount ).to.eql( 1 );
            expect( security_throw.callCount ).to.eql( 1 );

            expect( Security.can.getCall( 0 ).args ).to.eql( [context.userId] );
            expect( security_update.getCall( 0 ).args ).to.eql( ['projectId', preModifier] );
            expect( security_for.getCall( 0 ).args ).to.eql( [ProjectCollection] );

            assert.isFalse( ProjectCollection.direct.update.called );
        } );

        // We assume the postModifier would fail in the same way, didn't find how to throw on the second call with jasmine

        function expectMatchFailed() {
            assert.isFalse( ProjectCollection.findOne.called );
            expectFailedBeforeUpdate();
        }

        it( 'should throw on invalid project id (number)', () => {
            expect( ()=> ProjectMethods.movePhase._execute( context, 123, 'phaseId', 8 ) ).to.throw( /Match error/ );
            expectMatchFailed();
        } );

        it( 'should throw on invalid project id (null)', () => {
            expect( ()=> ProjectMethods.movePhase._execute( context, null, 'phaseId', 8 ) ).to.throw( /Match error/ );
            expectMatchFailed();
        } );

        it( 'should throw on invalid phase id (number)', () => {
            expect( ()=> ProjectMethods.movePhase._execute( context, 'projectId', 123, 8 ) ).to.throw( /Match error/ );
            expectMatchFailed();
        } );

        it( 'should throw on invalid phase id (null)', () => {
            expect( ()=> ProjectMethods.movePhase._execute( context, 'projectId', null, 8 ) ).to.throw( /Match error/ );
            expectMatchFailed();
        } );

        it( 'should throw on invalid order (<0)', () => {
            expect( ()=> ProjectMethods.movePhase._execute( context, 'projectId', 'phaseId', -1 ) ).to.throw( /Match error/ );
            expectMatchFailed();
        } );

        it( 'should throw on invalid order (string)', () => {
            expect( ()=> ProjectMethods.movePhase._execute( context, 'projectId', 123, 'eight' ) ).to.throw( /Match error/ );
            expectMatchFailed();
        } );

        it( 'should throw on invalid order (null)', () => {
            expect( ()=> ProjectMethods.movePhase._execute( context, 'projectId', 123, null ) ).to.throw( /Match error/ );
            expectMatchFailed();
        } );

        it( 'should throw on invalid order (undefined)', () => {
            expect( ()=> ProjectMethods.movePhase._execute( context, 'projectId', 123 ) ).to.throw( /Match error/ );
            expectMatchFailed();
        } );

        it( 'should throw on invalid phase & order (undefined)', () => {
            expect( ()=> ProjectMethods.movePhase._execute( context, 'projectId' ) ).to.throw( /Match error/ );
            expectMatchFailed();
        } );

        it( 'should throw on invalid project, phase & order (undefined)', () => {
            expect( ()=> ProjectMethods.movePhase._execute( context ) ).to.throw( /Match error/ );
            expectMatchFailed();
        } );
    } );
} );
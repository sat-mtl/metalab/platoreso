"use strict";

import { MS_PER_HOUR } from "/imports/utils/TimeUtils";

export default class Analytics {

    /**
     * Helper method to compute current temperature.
     *
     * @param {Number} lastTemperature - Last recorded temperature
     * @param {Date} heatedAt - Date/time when lastTemperature was recorded
     * @param {Date} now - Now!
     * @param {Number} cooldown - NUmber of points/degrees to remove per hour
     * @returns {number} Current temperature
     */
    static getCurrentTemperature( lastTemperature, heatedAt, now, cooldown ) {
        const delta      = now - heatedAt;
        const deltaHours = delta / MS_PER_HOUR;
        const heatLoss   = cooldown * deltaHours;

        return lastTemperature - heatLoss;
    }

}
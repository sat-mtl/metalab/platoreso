"use strict";

import { chai, assert, expect } from 'meteor/practicalmeteor:chai';
import { sinon, spies, stubs } from 'meteor/practicalmeteor:sinon';
import Analytics from "/imports/analytics/Analytics";

describe( 'analytics/Analytics', function () {

    let sandbox;
    beforeEach( () => {
        sandbox = sinon.sandbox.create();
    } );

    afterEach( () => {
        sandbox.restore();
    } );

    describe( "getCurrentTemperature", () => {

        it( "should work ;)", () => {
            expect( Analytics.getCurrentTemperature( 2, 0, 1000 * 60 * 60, 3 ) ).to.equal( -1 );
            expect( Analytics.getCurrentTemperature( 2, 0, 1000 * 60 * 60, 2 ) ).to.equal( 0 );
            expect( Analytics.getCurrentTemperature( 2, 0, 1000 * 60 * 60, 1 ) ).to.equal( 1 );
            expect( Analytics.getCurrentTemperature( 2, 0, 1000 * 60 * 60, 0.5 ) ).to.equal( 1.5 );
        } );

    } );
} );
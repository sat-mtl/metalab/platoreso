"use strict";

import Logger from "meteor/metalab:logger/Logger";
import UserCollection from "/imports/accounts/UserCollection";
import AccountsMeldHelper from "/imports/accounts/server/AccountsMeldHelper";
import FeedCollection from "../FeedCollection";

const log = Logger("feed-meld");

/**
 * Card Melding
 */
AccountsMeldHelper.onMeld( ( src_user_id, dst_user_id ) => {
    log.debug(`Melding stories for user ${src_user_id} into ${dst_user_id}`);

    FeedCollection.update( { 'subject': src_user_id }, { $set: { 'subject': dst_user_id } }, { multi: true } );
    FeedCollection.update( { 'objects.id': src_user_id, 'objects.type': UserCollection.entityName }, { $set: { 'objects.$.id': dst_user_id } }, { multi: true } );
} );
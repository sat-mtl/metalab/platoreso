"use strict";

import slugify from "underscore.string/slugify";
import Logger from "meteor/metalab:logger/Logger";
import { Counts } from "meteor/tmeasday:publish-counts";
import UserCollection from "/imports/accounts/UserCollection";
import UserHelpers from "/imports/accounts/UserHelpers";
import FeedCollection from "../FeedCollection";

const log = Logger( "feed-publication" );

/**
 * User Feed List (Administration)
 *
 * @params {String} id Id of the entity for the feed
 * @params {Boolean} internal Allow internal stories to be displayed
 * @params {Object} [queryParams] Additional query params (maxDate)
 * @params {Object} [options] Options for the find operation (ex: sort, limit)
 */
Meteor.publishComposite( "feed", function ( params ) {

    check( params, Match.ObjectIncluding( {
        entities: Match.Optional( Match.OneOf( String, [String] ) ),
        internal: Match.Optional( Boolean ),
        archived: Match.Optional( Boolean ),
        maxDate:  Match.Optional( Date ),
        limit:    Match.Optional( Number )
    } ) );

    // Only super+ can request internal stories
    if ( params.internal && !UserHelpers.isSuper( this.userId ) ) {
        params.internal = false;
    }

    if ( params.entities && !_.isArray( params.entities ) ) {
        params.entities = [params.entities];
    }

    const options = {
        limit: params.limit || 10,
        sort:  {
            createdAt: -1
        }
    };

    let query    = {};
    let subQuery = {};

    // Default is to not query for internal stories
    if ( !params.internal ) {
        subQuery.internal = false;
    }

    // Default is to not query for archived stories
    if ( !params.archived ) {
        subQuery.archived = false;
    }

    if ( params.maxDate ) {
        subQuery.createdAt = { $lte: params.maxDate };
    }

    if ( params.entities ) {
        query.$or = [
            _.extend( { 'subject': { $in: params.entities } }, subQuery ),
            _.extend( { 'objects.id': { $in: params.entities } }, subQuery )
        ];
    } else {
        query = subQuery;
    }

    //TODO: Permissions for the feed

    return {
        find() {
            //FIXME: This can get nasty ram-wise
            Counts.publish( this,
                'feed/count/' + slugify( JSON.stringify( query ) ),
                FeedCollection.find( query, { fields: { _id: 1 } } ),
                { noReady: true }
            );

            return FeedCollection.find( query, options )
        },
        children: [
            {
                find( story ) {
                    return UserCollection.find( { _id: story.subject }, {
                        fields: {
                            'profile.firstName': 1,
                            'profile.lastName':  1,
                            'profile.avatarUrl': 1
                        }
                    } );
                }
            }
        ]
    };
} );
"use strict";

import formDiff from "/imports/utils/formDiff";

export default class FeedHelpers {

    /**
     * Get the changed data and keep only certain keys
     *
     * @param {Object} doc new document with the changes
     * @param {Object} [old] previous document
     * @param {Array} [pick] keys to keep in the changes
     * @param {Array} [keep] keys to keep no matter if they changed or not
     * @returns {Object}
     */
    static getDiff( doc, old = null, pick = null, keep = null ) {
        // Get the full set of changes
        let diff = old ? formDiff( doc, old, keep ) : doc;
        // If we are picky reduce the changes to only what we're interested in
        if ( pick ) {
            diff = _.pick( diff, pick );
        }
        return diff;
    }

    /**
     * Get the changes for the feed story
     *
     * @param {Object} doc new document with the changes
     * @param {Object} [old] previous document
     * @param {Array} [pick] keys to keep in the changes
     * @param {Array} [keep] keys to keep no matter if they changed or not
     * @returns {Object}
     */
    static getChanges( doc, old = null, pick = null, keep = null ) {
        const diff   = FeedHelpers.getDiff( doc, old, pick, keep );
        // Check what those changed values were before
        const before = old ? _.pick( old, _.keys( diff ) ) : {};
        return {
            before: before,
            after:  diff
        };
    }

}
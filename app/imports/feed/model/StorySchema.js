"use strict";

export const StoryObjectSchema = new SimpleSchema({
    id: {
        type: String
    },
    type: {
        type: String
    },
    archived: {
        type: Boolean,
        defaultValue: false
    },
    role: {
        type: String,
        optional: true
    }
});

const StorySchema = new SimpleSchema({
    subject : {
        type: String
    },
    action: {
        type: String
    },
    objects: {
        type: [StoryObjectSchema]
    },
    data: {
        type: Object,
        optional: true,
        blackbox: true
    },
    internal: {
        type: Boolean
    },
    archived: {
        type: Boolean,
        defaultValue: false
    }
});

export default StorySchema;
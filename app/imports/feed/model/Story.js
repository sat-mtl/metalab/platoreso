"use strict";

/**
 * Story Model
 *
 * @param doc
 * @constructor
 */
export default class Story {

    constructor( doc ) {
        _.extend( this, doc );
    }

    /**
     * Helper Method to get the first object matching the provided type
     * @param {String} type Entity type name to look for
     * @param {String} [role] Entity role
     * @param {Boolean} [archived] Specify if we want archived or live objects
     */
    getObjectByType( type, role = null, archived = null ) {
        return _.values( this.objects )
                .find( object => object.type == type && ( !role || object.role == role ) && ( archived == null || object.archived == archived ) );
    }

    /**
     * Helper Method to get the first object id matching the provided type
     * @param {String} type Entity type name to look for
     * @param {String} [role] Entity role
     * @param {Boolean} [archived] Specify if we want archived or live objects
     */
    getObjectIdByType( type, role = null, archived = null ) {
        const object = this.getObjectByType( type, role, archived );
        return object ? object.id : null;
    }

    /**
     * Helper Method to get the objects matching the provided type
     * @param {String} type Entity type name to look for
     * @param {String} [role] Entity role
     * @param {Boolean} [archived] Specify if we want archived or live objects
     */
    getObjectsByType( type, role = null, archived = null ) {
        return _.values( this.objects )
                .filter( object => object.type == type && ( !role || object.role == role ) && ( archived == null || object.archived == archived ) );
    }

    /**
     * Helper Method to get the object ids matching the provided type
     * @param {String} type Entity type name to look for
     * @param {String} [role] Entity role
     * @param {Boolean} [archived] Specify if we want archived or live objects
     */
    getObjectIdsByType( type, role = null, archived = null ) {
        return this.getObjectsByType( type, role, archived )
                   .map( object => object.id );
    }
}
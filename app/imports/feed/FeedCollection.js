"use strict";

import Story from "./model/Story";
import StorySchema from "./model/StorySchema";

class FeedCollection extends Mongo.Collection {
    constructor( name, options = {} ) {
        // Change transform before calling super, this is the only way to set it
        // Also, this won't suffer from module loading as it is a callback
        options.transform = feed => new Story( feed );

        super( name, options );

        /**
         * Schema
         */

        this.attachSchema( StorySchema );

        /**
         * Indexes
         */

        if ( Meteor.isServer ) {
            // Usual feed query
            this._ensureIndex( { subject: 1, internal: 1, archived: 1, createdAt: 1 }, { name: 'subject_internal_archived_createdAt' } );
            this._ensureIndex( { 'objects.id': 1, internal: 1, archived: 1, createdAt: 1 }, { name: 'objects_internal_archived_createdAt' } );
        }

        /**
         * Security
         */

        this.deny({
            insert: () => true,
            update: () => true,
            remove: () => true
        });

        /**
         * Behaviors
         */

        this.attachBehaviour( 'timestampable' );
    }

    /**
     * Helper method to record an story
     * @param story
     */
    recordStory( story ) {
        this.insert( story );
    }

    /**
     * Makes a past story internal
     * @param story
     */
    makeInternal( story ) {
        // Don't bother if already internal
        story.internal = false;
        this.update( story, { $set: { internal: true } }, { multi: true } );
    }

    /**
     * Archive all stories for an object
     *
     * @param {String} id Entity ID
     * @param {String} entityType Entity type name
     */
    archiveAllForObject( id, entityType ) {
        this.update( { archived: false, 'objects.id': id, 'objects.type': entityType }, { $set: { archived: true, 'objects.$.archived': true } }, { multi: true } );
    }

    /**
     * Remove all stories match the passed story object
     *
     * @param story
     */
    removeStories( story ) {
        this.remove( story );
    }

    /**
     * Remove all stories matching the passed object.
     *
     * @param {String} id Entity ID
     * @param {String} entityType Entity type name
     */
    removeAllForObject( id, entityType ) {
        this.remove( { 'objects.id': id, 'objects.type': entityType } );
    }
}

export default new FeedCollection( 'feed' );

"use strict";

/**
 * Helper to wrap a Meteor method in a 'standalone' function
 *
 * @deprecated
 * @param methodName string Name of the Meteor method
 * @returns function Wrapped method
 */
export default function wrapMethod( methodName ) {
    //return ( ...args ) => Meteor.call( methodName, ...args );
    return Meteor.call.bind( Meteor, methodName );
}
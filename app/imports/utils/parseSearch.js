"use strict";

export default function parseSearch( search, allowedCommands = null ) {
    let rawCommands = search.match( /[a-zA-Z]+?:(".*?"|[^" ]+)/g );
    let commands    = null;
    if ( rawCommands ) {
        commands = rawCommands.map( command => {
            search      = search.replace( command, '' );
            const parts = command.split( ':' );
            return {
                field: parts[0],
                term:  parts[1].replace( /"/g, '' ).trim()
            }
        } );
        if ( allowedCommands ) {
            commands = commands.filter( command => allowedCommands.indexOf( command.field ) != -1 );
        }
    }

    let exact = search.match( /".*?"/g );
    if ( exact ) {
        exact.forEach( term => {
            search = search.replace( term, '' );
        } );
    }
    exact      = exact ? exact.map( term => term.replace( /"/g, '' ).trim() ).filter( term => term != '' ) : [];
    const ored = search.split( ' ' ).map( term => term.trim() ).filter( term => !_.isEmpty(term) );

    const terms = exact.concat( ored );
    return {
        commands: commands,
        terms:    terms
    }
};
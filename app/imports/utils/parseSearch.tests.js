"use strict";

import {chai, assert, expect} from 'meteor/practicalmeteor:chai';
import {sinon, spies, stubs} from 'meteor/practicalmeteor:sinon';

import parseSearch from "/imports/utils/parseSearch";

describe( 'utils/parseSearch', function () {

    let sandbox;

    beforeEach( () => {
        sandbox = sinon.sandbox.create();
    } );

    afterEach( () => {
        sandbox.restore();
    } );

    it( 'parses one term', function () {
        expect( parseSearch("test") ).to.eql({commands: null, terms:["test"]});
    } );

    it( 'parses two terms', function () {
        expect( parseSearch("test me") ).to.eql({commands: null, terms:["test", "me"]});
    } );

    it( 'parses exact terms', function () {
        expect( parseSearch("\"test me\"") ).to.eql({commands: null, terms:["test me"]});
    } );

    it( 'parses exact terms preceded by another term', function () {
        expect( parseSearch("just \"test me\"") ).to.eql({commands: null, terms:["test me", "just"]});
    } );

    it( 'parses exact terms followed by another term', function () {
        expect( parseSearch("\"test me\" already") ).to.eql({commands: null, terms:["test me", "already"]});
    } );

    it( 'parses exact terms surrounded by another term', function () {
        expect( parseSearch("just \"test me\" already") ).to.eql({commands: null, terms:["test me", "just", "already"]});
    } );

    it( 'parses one command', function () {
        expect( parseSearch("command:term") ).to.eql({commands: [{field: "command", term: "term"}], terms:[]});
    } );

    it( 'parses one command with exact terms', function () {
        expect( parseSearch("command:\"exact terms\"") ).to.eql({commands: [{field: "command", term: "exact terms"}], terms:[]});
    } );

    it( 'parses one preceded by a term', function () {
        expect( parseSearch("test command:term") ).to.eql({commands: [{field: "command", term: "term"}], terms:["test"]});
    } );

    it( 'parses one command followed by a term', function () {
        expect( parseSearch("command:term test") ).to.eql({commands: [{field: "command", term: "term"}], terms:["test"]});
    } );

    it( 'parses one command surrounded by two terms', function () {
        expect( parseSearch("first command:term second") ).to.eql({commands: [{field: "command", term: "term"}], terms:["first", "second"]});
    } );

    it( 'parses multiple commands surrounded by two terms', function () {
        expect( parseSearch("command:term another:\"here too\"") ).to.eql({commands: [{field: "command", term: "term"}, {field: "another", term: "here too"}], terms:[]});
    } );

    it( 'ignores unsupported commands', function () {
        expect( parseSearch("command:term another:\"here too\"", ["command"]) ).to.eql({commands: [{field: "command", term: "term"}], terms:[]});
    } );

    it( 'parses a big mess', function () {
        expect( parseSearch(
            "this is \"a big\" mess of:multiple command:\"types at\" the \"same time\" is:it",
            ["command", "is"]
        ) ).to.eql({
            commands: [
                {field: "command", term: "types at"},
                {field: "is", term: "it"}
            ],
            terms:["a big", "same time", "this", "is", "mess", "the"]
        });
    } );
} );
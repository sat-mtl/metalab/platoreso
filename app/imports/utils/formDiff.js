/**
 * Calculate deep object differences
 *
 * Very basic, mostly for form data use.
 * Will only report changes in a compared to b.
 * But not missing keys in a present in b.
 *
 * THIS IS NOT A COMPLETE DIFF ALGORITHM
 * THIS IS ONLY FOR COMPARING FORM VALUES
 * WILL UPDATE WHEN NEW REQUIREMENTS ARISE
 *
 * @param a
 * @param b
 * @param keep Array of keys to keep no matter what
 * @returns {{}}
 */
export default function formDiff(a,b,keep = null, options = {}) {
    if ( b == null ) {
        return a;
    }

    if ( keep == null ) {
        keep = [];
    } else if ( _.isString(keep) ) {
        keep = [keep];
    }

    var r = {};
    _.each(a, (v,k) => {

        // Not in base, keep
        if ( 'undefined' == typeof(b[k]) ) {
            r[k] = v;
            return;
        }

        // Equal and not in the keep list, flush
        if( b[k] === v && keep.indexOf(k) == -1) {
            return;
        }

        if ( _.isDate(v) && _.isDate(b[k]) ) {
            if ( v.getTime() != b[k].getTime() ) {
                r[k] = v;
            }
        } else if ( _.isObject(v) && !_.isArray(v) ) {
            // Compare objects, NOT ARRAYS
            const val = formDiff(v, b[k], keep);
            // Do not keep empty objects
            if ( !_.isEmpty(val) ) {
                r[k] = val;
            }
        } else if ( _.isArray(v) ) {
            // Keep value
            if ( _.difference(v, b[k] ).length > 0 || _.difference(b[k], v ).length > 0 ) {
                r[k] = v;
            }
        } else {
            // Keep value
            r[k] = v;
        }
    });
    return r;
};
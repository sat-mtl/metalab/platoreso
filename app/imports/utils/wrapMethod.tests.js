"use strict";

import {chai, assert, expect} from 'meteor/practicalmeteor:chai';
import {sinon, spies, stubs} from 'meteor/practicalmeteor:sinon';

import wrapMethod from "/imports/utils/wrapMethod";

describe('utils/wrapMethod', function () {

    let sandbox;

    beforeEach( () => {
        sandbox = sinon.sandbox.create();
    } );

    afterEach( () => {
        sandbox.restore();
    } );

    describe('wrapMethod', function() {

        it( 'works as expected', function () {
            sandbox.stub( Meteor, 'call' );
            wrapMethod( 'test' )( 'one', 'two', 'three' );
            assert( Meteor.call.calledWith( 'test', 'one', 'two', 'three' ) );
        } );

    });
});
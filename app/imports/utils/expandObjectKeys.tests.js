"use strict";

import {chai, assert, expect} from 'meteor/practicalmeteor:chai';
import {sinon, spies, stubs} from 'meteor/practicalmeteor:sinon';

import expandObjectKeys from "./expandObjectKeys";

describe( 'utils/expandObjectKey', function () {

    let sandbox;

    beforeEach( () => {
        sandbox = sinon.sandbox.create();
    } );

    afterEach( () => {
        sandbox.restore();
    } );

    it( 'expands simple keys', function () {
        const a = {'a.b': 1, 'a.c': 2};
        const b = expandObjectKeys( a );
        expect( b ).to.eql( {a: {b: 1, c: 2}} );
    } );

    it( 'expands simple keys and overwrites previous obj if found (key order dependent) #1', function () {
        const a = {'a': 0, 'a.b': 1, 'a.c': 2};
        const b = expandObjectKeys( a );
        expect( b ).to.eql( {a: {b: 1, c: 2}} );
    } );

    it( 'expands simple keys and overwrites previous obj if found (key order dependent) #2', function () {
        const a = {'a.b': 1, 'a.c': 2, 'a': 0};
        const b = expandObjectKeys( a );
        expect( b ).to.eql( {a: 0} );
    } );

    it( 'expands complex objects', function () {
        const a = {'a.b.c': 1, 'a.c': 2, 'a.b.d': 3};
        const b = expandObjectKeys( a );
        expect( b ).to.eql( {a: {b: {c: 1, d: 3}, c: 2}} );
    } );
} );
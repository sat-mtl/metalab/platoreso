"use strict";

import {chai, assert, expect} from 'meteor/practicalmeteor:chai';
import {sinon, spies, stubs} from 'meteor/practicalmeteor:sinon';

import formDiff from "/imports/utils/formDiff";

describe( 'utils/formDiff', function () {

    let sandbox;

    beforeEach( () => {
        sandbox = sinon.sandbox.create();
    } );

    afterEach( () => {
        sandbox.restore();
    } );

    it( 'returns shallow differences', function () {
        const a    = {a: 1, b: 2, c: 3};
        const b    = {a: 1, b: 3};
        const diff = formDiff( a, b );
        expect( diff ).to.eql( {b: 2, c: 3} );
    } );

    it( 'returns deep differences', function () {
        const a    = {a: 1, b: {d: 4, e: 5, f: 6}, c: 3};
        const b    = {a: 1, b: {d: 4, e: 4}};
        const diff = formDiff( a, b );
        expect( diff ).to.eql( {b: {e: 5, f: 6}, c: 3} );
    } );

    it( 'keep a key in the differences', function () {
        const a    = {a: 1, b: 2, c: 3};
        const b    = {a: 1, b: 3};
        const diff = formDiff( a, b, 'a' );
        expect( diff ).to.eql( {a: 1, b: 2, c: 3} );
    } );

    it( 'keep multiple keys in the differences', function () {
        const a    = {a: 1, b: 2, c: 3};
        const b    = {a: 1, b: 2};
        const diff = formDiff( a, b, ['a', 'b'] );
        expect( diff ).to.eql( {a: 1, b: 2, c: 3} );
    } );

    it( 'ignores unchanged arrays', function () {
        const a    = {a: [1, 2, 3]};
        const b    = {a: [3, 2, 1]};
        const diff = formDiff( a, b );
        expect( diff ).to.eql( {} );
    } );

    it( 'returns complete array on changes 1', function () {
        const a    = {a: [1, 2, 3]};
        const b    = {a: [3, 1]};
        const diff = formDiff( a, b );
        expect( diff ).to.eql( { a: [ 1, 2, 3 ]} );
    } );

    it( 'returns complete array on changes 2', function () {
        const a    = {a: [1, 3]};
        const b    = {a: [3, 2, 1]};
        const diff = formDiff( a, b );
        expect( diff ).to.eql( { a: [ 1, 3 ]} );
    } );

    it( 'returns complete array on changes 3', function () {
        const a    = {a: [1, 2, 3]};
        const b    = {a: null};
        const diff = formDiff( a, b );
        expect( diff ).to.eql( { a: [ 1, 2, 3 ]} );
    } );

    it( 'returns complete array on changes 4', function () {
        const a    = {a: [1, 2, 3]};
        const b    = {};
        const diff = formDiff( a, b );
        expect( diff ).to.eql( { a: [ 1, 2, 3 ]} );
    } );

} );
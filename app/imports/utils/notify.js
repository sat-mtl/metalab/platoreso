"use strict";

import {browserHistory} from "react-router";

const Notification = window.Notification || window.navigator.webkitNotifications;

export function canNotify() {
    return Notification && Notification.permission !== "denied";
}

export default function notify( title, link, config ) {
    if ( !Notification || !Notification.requestPermission ) {
        return;
    }

    Notification.requestPermission( permission => {
        if ( Notification.permission !== 'granted' ) {
            return;
        }
        const notification = new Notification( title, config );
        notification.onclick = event => {
            browserHistory.push( link );
            notification.close();
        }
    } );
}
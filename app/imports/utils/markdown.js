"use strict";

import md from "markdown-it";
import emoji from "markdown-it-emoji";
import twemoji from "twemoji";
import hljs from "highlight.js";

const markdown = md( {
    html:        false,
    breaks:      true,
    linkify:     true,
    typographer: true,
    langPrefix: 'language-',
    highlight: (str, lang) => {
        if (lang && hljs.getLanguage(lang)) {
            try {
                // I wish we could output a <code> here
                // but it gets wrapped inside a <pre><code><code> by markdownIt
                return `<pre class="hljs">${hljs.highlight(lang, str, true).value}</pre>`;
            } catch (e) { console.error(e); }
        }
        return `<code class="hljs">${markdown.utils.escapeHtml(str)}</code>`;
    }
} ).use( emoji );

// Beautify output of parser for html content
markdown.renderer.rules.table_open = function () {
    return '<table class="ui unstackable celled compact small table">\n';
};

// Add target="_blank" to links
markdown.renderer.rules.link_open = function (tokens, idx, options) {
    // Just in case, we don't want the board to crash because markdown has some weird structure with no attrs
    if ( tokens[idx] ) {
        if ( !tokens[idx].attrs ) {
            tokens[idx].attrs = [];
        }
        tokens[idx].attrs.push( ["target", "_blank"] );
    }
    return markdown.renderer.renderToken(tokens, idx, options);
};

// Use twemoji directly so that it can detect utf8 emojis
// Markdown-it emoji plugin only recognizes markup emojis
export default text => twemoji.parse( markdown.render( text ) );
export const mdInline = text => twemoji.parse( markdown.renderInline( text ) );
"use strict";

/**
 * Expand dotty object keys to objects
 *
 * @author Christoph Hermann
 * @source https://github.com/stoeffel/expand-object-keys
 * @copyright Christoph Hermann <schtoeffel@gmail.com> (stoeffel.github.io)
 * @license MIT
 *
 * @param obj
 * @returns {*}
 */
export default function expandObjectKeys(obj) {
    if (obj == null) return null;

    const expand = (newObj, key, idx, keys) => {
        var [head, ...rest] = key.split('.');

        return {
            ...newObj,
            [head]: rest.length <= 0 ? obj[keys[idx]] : expand(newObj[head] || {}, rest.join('.'), idx, keys)
        };
    };

    return Object.keys(obj).reduce(expand, {});
};
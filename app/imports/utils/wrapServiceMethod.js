"use strict";

/**
 * More advanced helper that not only wraps a meteor method into a function but also registers
 * it at the same time from a static method in a service class.
 *
 * @deprecated
 * @param {String} methodName Method name
 * @param {Function} service Function to wrap in a meteor method
 * @returns {Function} A callable function that wraps Meteor.call
 */
export default function wrapServiceMethod( methodName, service ) {
    Meteor.methods({[methodName]: service});
    return Meteor.call.bind( Meteor, methodName );
}
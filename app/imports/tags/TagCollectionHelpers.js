"use strict";

import TagsSchema from "./model/TagsSchema";

export default class TagCollectionHelpers {

    static setupCollection( collection ) {
        collection.attachSchema( TagsSchema );

        collection.getTags = function ( query = {} ) {
            if ( Meteor.isServer ) {
                // Run the actual query on the server
                return this.distinct( 'tags', query );
            } else {
                // On the client, just use whatever we have available
                const taggedEntities = this.find( { $and: [ query, { tags: { $exists: true } } ] }, { fields: { tags: 1 } } ).fetch();
                return _.uniq( _.flatten( _.pluck( taggedEntities, 'tags' ) ) ).filter(tag => tag != null);
            }
        };

        collection.removeTags = function ( tags ) {
            if ( !_.isArray( tags ) ) {
                tags = [tags];
            }

            this.update(
                {tags: {$in: tags}},
                {$pullAll: {tags: tags}},
                {multi: true}
            );
        };

        collection.renameTag = function ( oldTag, newTag ) {
            // Start by adding the new tag, we still need the old one to find and remove it
            this.update(
                {tags: oldTag},
                {$addToSet: {tags: newTag}},
                {multi: true}
            );
            // Then remove the old tag
            this.update(
                {tags: oldTag},
                {$pull: {tags: oldTag}},
                {multi: true}
            );
        }
    }
}
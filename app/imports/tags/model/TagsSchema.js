"use strict";

const TagsSchema = new SimpleSchema({
    tags: {
        type:         [String],
        optional:     true,
        //defaultValue: [],
        autoValue:    function () {
            // Automatically turn anything that's not an array into an empty array
            // Semantic-ui has a bad habit of turning empty multi-selects into nulls
            if ( this.isSet && !_.isArray( this.value ) ) {
                return [];
            }
        }
    }
});

export default TagsSchema;
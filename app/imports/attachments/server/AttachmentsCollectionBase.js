"use strict";

import Logger from "meteor/metalab:logger/Logger";
import { FS } from "meteor/cfs:base-package";
import FilesCollection from "../../files/FilesCollection";

const log = Logger( 'attachments-collection-base-server' );

export default class AttachmentsCollectionBase extends FilesCollection {
    constructor( name, options = {} ) {

        if ( !Meteor.settings || !Meteor.settings.uploads || !Meteor.settings.uploads.path) {
            log.error(`Upload path setting is missing`);
            throw new Meteor.Error('missing upload path setting in server/AttachmentsCollectionBase');
        }

        super( name, _.extend( options || {}, {
            stores: [
                new FS.Store.FileSystem( name, {
                    path: Meteor.settings.uploads.path + name
                } )
            ]
        } ) );

        // Indexes
        this.files._ensureIndex( {owners: 1}, {name: 'owners'} );
    }

}
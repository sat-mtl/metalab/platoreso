"use strict";

import { FS } from "meteor/cfs:base-package";
import FilesCollection from "../../files/FilesCollection";

export default class AttachmentsCollectionBase extends FilesCollection {
    constructor( name, options = {} ) {
        super( name, _.extend( options || {}, {
            stores: [
                new FS.Store.FileSystem( name )
            ]
        } ) );
    }
}
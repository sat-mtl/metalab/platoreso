"use strict";

import { FS } from "meteor/cfs:base-package";

let AttachmentCollectionBase;
if ( Meteor.isServer ) {
    AttachmentCollectionBase = require("./server/AttachmentsCollectionBase" ).default;
} else {
    AttachmentCollectionBase = require("./client/AttachmentsCollectionBase" ).default;
}

export default class AttachmentsCollection extends AttachmentCollectionBase {}
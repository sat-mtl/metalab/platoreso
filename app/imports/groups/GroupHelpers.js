"use strict";

import Logger from "meteor/metalab:logger/Logger";
import {GroupRoles, GROUP_PREFIX} from "./GroupRoles";
import UserHelpers from "/imports/accounts/UserHelpers";
import ConversationCollection from "/imports/messages/ConversationCollection";
import ConversationType from "/imports/messages/ConversationType";

const log = Logger( "group-helpers" );

export default class GroupHelpers {

    /**
     * Get the PlatoReso groups for a user and a set of roles
     * Filters to keep only group roles and match it to a group id
     *
     * @param user
     * @param roles
     * @returns {Array<String>}
     */
    static getGroupsForUser( user, roles = GroupRoles ) {
        check( user, String );
        check( roles, [String] );

        return UserHelpers.getGroupsForUser( user, roles )
            .map( roleGroup => roleGroup.split( '_' ) )
            .filter( splitRoleGroup => splitRoleGroup[0] === GROUP_PREFIX )
            .map( splitRoleGroup => splitRoleGroup[1] );
    }

    static updateConversationParticipation( userId ) {
        log.debug(`Updating group conversation participation for ${userId}`);

        const groups = GroupHelpers.getGroupsForUser( userId );

        ConversationCollection.addParticipant( {
            type:         ConversationType.group,
            entity:       { $in: groups }
        }, userId );

        ConversationCollection.removeParticipant( {
            type:         ConversationType.group,
            entity:       { $nin: groups }
        }, userId );
    }

    static clearConversation( groupId ) {
        const conversation = ConversationCollection.findOne({
            type: ConversationType.group,
            entity: groupId
        });
        if ( !conversation ) {
            throw new Meteor.Error( 'conversation not found' );
        }

        conversation.clear();
    }
}
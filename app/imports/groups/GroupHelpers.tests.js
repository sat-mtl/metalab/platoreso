"use strict";

import {chai, assert, expect} from 'meteor/practicalmeteor:chai';
import {sinon, spies, stubs} from 'meteor/practicalmeteor:sinon';

import GroupHelpers from "/imports/groups/GroupHelpers";
import {GroupRoles, GROUP_PREFIX} from "/imports/groups/GroupRoles";
import UserHelpers from "/imports/accounts/UserHelpers";

describe( 'groups/GroupHelpers', function () {

    let sandbox;

    beforeEach( () => {
        sandbox = sinon.sandbox.create();
    } );

    afterEach( () => {
        sandbox.restore();
    } );

    describe( 'getGroupsForUser', function () {

        beforeEach( function () {
            sandbox.stub( UserHelpers, 'getGroupsForUser' );
        } );

        it( 'should return the list of PlatoReso groups that are for a permissions group', function () {
            UserHelpers.getGroupsForUser.returns( ['group1', GROUP_PREFIX + '_group2', GROUP_PREFIX + '_group3'] );
            expect( GroupHelpers.getGroupsForUser( 'user' ) ).to.eql( ['group2', 'group3'] );
            assert( UserHelpers.getGroupsForUser.calledWith( 'user', GroupRoles ) );
        } );

        it( 'should limit to the set of roles passed as parameters', function () {
            UserHelpers.getGroupsForUser.returns( ['group1', GROUP_PREFIX + '_group2', GROUP_PREFIX + '_group3'] );
            expect( GroupHelpers.getGroupsForUser( 'user', ['role1', 'role2'] ) ).to.eql( ['group2', 'group3'] );
            assert( UserHelpers.getGroupsForUser.calledWith( 'user', ['role1', 'role2'] ) );
        } );

        it( 'should throw on null user', function () {
            expect( ()=>GroupHelpers.getGroupsForUser() ).to.throw( /Match error/ );
            assert.isFalse( UserHelpers.getGroupsForUser.called );
        } );

        it( 'should throw on invalid user', function () {
            expect( ()=>GroupHelpers.getGroupsForUser( 123, ['role1', 'role2'] ) ).to.throw( /Match error/ );
            assert.isFalse( UserHelpers.getGroupsForUser.called );
        } );

        it( 'should throw on null groups', function () {
            expect( ()=>GroupHelpers.getGroupsForUser( 'user', null ) ).to.throw( /Match error/ );
            assert.isFalse( UserHelpers.getGroupsForUser.called );
        } );

        it( 'should throw on invalid groups', function () {
            expect( ()=>GroupHelpers.getGroupsForUser( 'user', 123 ) ).to.throw( /Match error/ );
            assert.isFalse( UserHelpers.getGroupsForUser.called );
        } );
    } )
} );

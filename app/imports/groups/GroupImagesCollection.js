"use strict";

import ImageCollection from "/imports/images/ImageCollection";

/**
 * Group Images Collection
 */

const GroupImagesCollection = new ImageCollection('group', null, {
    overwritePrevious: true
});
export default GroupImagesCollection;
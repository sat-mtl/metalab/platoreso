"use strict";

import flatten from "flat";
import escapeStringRegexp from "escape-string-regexp";
import Logger from "meteor/metalab:logger/Logger";
import { ValidatedMethod } from "meteor/mdg:validated-method";
import { RoleList } from "/imports/accounts/Roles";
import GroupCollection from "./GroupCollection";
import GroupSchema, { GroupUpdateSchema } from "./model/GroupSchema";
import GroupSecurity from "./GroupSecurity";
import GroupHelpers from "./GroupHelpers";
import GroupNotificationHelpers from "./notifications/GroupNotificationHelpers";
import { GroupRoles, GROUP_PREFIX } from "./GroupRoles";
import UserCollection from "/imports/accounts/UserCollection";
import UserHelpers from "/imports/accounts/UserHelpers";

const log = Logger( "group-methods" );

const PermissionSchemaBase = new SimpleSchema( {
    user:  {
        type:     String,
        optional: true
    },
    email: {
        type:     String,
        regEx:    SimpleSchema.RegEx.Email,
        optional: true
    }
} );

const PermissionCreateSchema = new SimpleSchema( [
    PermissionSchemaBase,
    {
        roles:     {
            type: [String]
        },
        'roles.$': {
            allowedValues: GroupRoles
        }
    }
] );

const PermissionUpdateSchema = new SimpleSchema( [
    PermissionSchemaBase,
    {
        roles:     {
            type:     [String],
            optional: true
        },
        'roles.$': {
            allowedValues: GroupRoles
        },
        deleted:   {
            type:     Boolean,
            optional: true
        }
    }
] );

const GroupMethods = {

    /**
     * Create a group
     *
     * @param  {object} info User update information
     * @return {string} Group Id
     */
    create: new ValidatedMethod( {
        name:     'pr/group/create',
        validate: new SimpleSchema( {
            group:       {
                type: GroupSchema.pick( [
                    'name',
                    'description',
                    'content',
                    'public',
                    'showMembers'
                ] )
            },
            permissions: {
                type:     [PermissionCreateSchema],
                optional: true
            }
        } ).validator( { clean: true } ),
        run( { group, permissions } ) {

            // SECURITY

            if ( !GroupSecurity.canCreate( this.userId, group ) ) {
                log.warn( `create - ${this.userId} is not authorized to create group`, group );
                throw new Meteor.Error( 'unauthorized' );
            }

            // UPDATE

            log.verbose( "create - Creating group", group );
            const groupId = GroupCollection.insert( group );

            // PERMISSIONS

            // The creator is an expert, whatever he/she decided
            log.verbose( "create - Adding group creator as expert" );
            GroupMethods.addUser.call( { groupId, userId: this.userId, roles: [RoleList.expert] } );

            if ( Meteor.isServer ) {

                if ( permissions ) {
                    log.verbose( "create - Setting up group permissions", permissions );

                    permissions
                    // Map email users to real users, the group creator might not have access to the user list be we do
                        .map( permission => {
                            if ( permission.email ) {
                                const user = UserCollection.findOne( { 'emails.address': permission.email }, {
                                    fields:    { _id: 1 },
                                    transform: null
                                } );
                                if ( user ) {
                                    permission.user = user._id;
                                    delete permission.email;
                                }
                            }
                            return permission;
                        } )
                        // Filter out the group creator, they already got their permissions set
                        .filter( permission => permission.user != this.userId )
                        // Invite/Add the user to the group
                        .forEach( permission => {
                            let userId;
                            if ( permission.email ) {
                                userId = Accounts.createUser( { email: permission.email, profile: {} } );
                                Accounts.sendEnrollmentEmail( userId );
                            } else {
                                userId = permission.user;
                            }
                            GroupMethods.addUser.call( { groupId, userId, roles: permission.roles } );
                        } );
                }
            }

            // AUDIT

            /*Audit.next( {
             domain:  GroupCollection.entityName,
             message: GroupAuditMessages.added,
             userId:  this.userId,
             groupId:  groupId
             } );*/

            return groupId;
        }
    } ),

    /**
     * Update a group
     *
     * @param {object} info group update information
     */
    update: new ValidatedMethod( {
        name:     'pr/group/update',
        validate: new SimpleSchema( {
            _id:         {
                type: String
            },
            changes:     {
                type:     GroupUpdateSchema,
                optional: true
            },
            permissions: {
                type:     [PermissionUpdateSchema],
                optional: true
            }
        } ).validator( { clean: true, removeEmptyStrings: false, getAutoValues: false } ),
        run( { _id, changes, permissions } ) {

            // SECURITY

            if ( !GroupSecurity.canUpdate( this.userId, _id ) ) {
                log.warn( `update - ${this.userId} is not authorized to update group`, changes );
                throw new Meteor.Error( 'unauthorized' );
            }

            // CHECKS

            const group = GroupCollection.findOne( { _id }, {
                fields:    { _id: 1 },
                transform: null
            } );
            if ( !group ) {
                throw new Meteor.Error( 'group not found' );
            }

            // Check if invitation code already exists
            if ( changes.invitationCode ) {
                const codeExists = GroupCollection.find(
                        {
                            _id:            { $ne: group._id },
                            invitationCode: {
                                $regex:   `^${escapeStringRegexp( changes.invitationCode )}$`,
                                $options: 'i'
                            }
                        }
                    ).count() != 0;
                if ( codeExists ) {
                    throw new Meteor.Error( 'invitation code not available' );
                }
            }

            // UPDATE

            // Updating the group
            if ( !_.isEmpty( changes ) ) {
                log.debug( "update - Updating group", changes );

                const modifier = { $set: flatten( changes, { safe: true } ) };

                // Do the actual group update
                GroupCollection.update( { _id: group._id }, modifier );
            }

            // PERMISSIONS

            if ( Meteor.isServer ) {
                if ( permissions ) {
                    log.verbose( "update - Updating group permissions" );

                    permissions
                    // Map email users to real users, the group creator might not have access to the user list be we do
                        .map( permission => {
                            if ( permission.email ) {
                                const user = UserCollection.findOne( { 'emails.address': permission.email }, {
                                    fields:    { _id: 1 },
                                    transform: null
                                } );
                                if ( user ) {
                                    permission.user = user._id;
                                    delete permission.email;
                                }
                            }
                            return permission;
                        } )
                        // Invite/Add the user to the group
                        .forEach( permission => {
                            if ( permission.deleted ) {
                                GroupMethods.removeUser.call( { groupId: group._id, userId: permission.user } );

                            } else {
                                let userId;
                                if ( permission.email ) {
                                    userId = Accounts.createUser( { email: permission.email, profile: {} } );
                                    Accounts.sendEnrollmentEmail( userId );
                                } else {
                                    userId = permission.user;
                                }

                                GroupMethods.addUser.call( { groupId: group._id, userId, roles: permission.roles } );
                            }
                        } );
                }
            }

            // AUDIT

            /*Audit.next( {
             domain:  GroupCollection.entityName,
             message: GroupAuditMessages.updated,
             userId:  this.userId,
             groupId:  group._id
             } );*/
        }
    } ),

    /**
     * Remove a group
     *
     * @param {string} groupId Group Id
     */
    remove: new ValidatedMethod( {
        name:     'pr/group/remove',
        validate: new SimpleSchema( {
            groupId: { type: String }
        } ).validator(),
        run( { groupId } ) {

            // SECURITY
            if ( !GroupSecurity.canRemove( this.userId, groupId ) ) {
                log.warn( `remove - ${this.userId} is not authorized to remove group ${groupId}` );
                throw new Meteor.Error( 'unauthorized' );
            }

            // UPDATE
            log.verbose( `remove - ${groupId}` );
            GroupCollection.remove( { _id: groupId } );

            // AUDIT
            /*Audit.next( {
             domain:  GroupCollection.entityName,
             message: GroupAuditMessages.removed,
             userId:  this.userId,
             groupId:  groupId
             } );*/
        }
    } ),

    /**
     * Add user to a group
     *
     * @param {string} groupId Group Id
     * @param {string} userId User Id
     * @param {string} role User role in group
     */
    addUser: new ValidatedMethod( {
        name:     'pr/group/addUser',
        validate: new SimpleSchema( {
            groupId: { type: String },
            userId:  { type: String },
            roles:   { type: [String] }
        } ).validator(),
        run( { groupId, userId, roles } ) {

            // SECURITY

            const roleGroup = GROUP_PREFIX + '_' + groupId;

            // Only allow logged in experts or group experts
            if ( !UserHelpers.isExpert( this.userId, roleGroup ) ) {
                throw new Meteor.Error( 'unauthorized' );
            }

            // UPDATE

            const group = GroupCollection.findOne( { _id: groupId }, { fields: { _id: 1 } } );
            if ( !group ) {
                throw new Meteor.Error( 'group not found' );
            }

            const user = UserCollection.findOne( { _id: userId }, { fields: { _id: 1 } } );
            if ( !user ) {
                throw new Meteor.Error( 'user not found' );
            }

            if ( !_.every( roles, role => _.contains( GroupRoles, role ) ) ) {
                throw new Meteor.Error( 'invalid roles' );
            }

            const alreadyInGroup = _.contains( UserHelpers.getGroupsForUser( userId ), roleGroup );

            log.verbose( `addUser - ${user._id} -> ${roles} -> ${groupId}` );
            UserHelpers.setUserRoles( user._id, roles, roleGroup );

            // Update Conversation
            GroupHelpers.updateConversationParticipation( user._id );

            // NOTIFICATION
            //TODO: Move to audit
            if ( Meteor.isServer && user._id != this.userId && !alreadyInGroup ) {
                GroupNotificationHelpers.notifyAddedToGroup( user._id, this.userId, group._id, roles );
            }

            // AUDIT
            /*Audit.next( {
             domain:  GroupCollection.entityName,
             message: GroupAuditMessages.removed,
             userId:  this.userId,
             groupId:  groupId
             } );*/
        }
    } ),

    /**
     * Remove a user from a group
     *
     * @param {string} groupId Group Id
     * @param {string} userId User Id
     * @param {string} role User role in group
     */
    removeUser: new ValidatedMethod( {
        name:     'pr/group/removeUser',
        validate: new SimpleSchema( {
            groupId: { type: String },
            userId:  { type: String },
            role:    { type: String, optional: true }
        } ).validator(),
        run( { groupId, userId, role } ) {

            // SECURITY

            const roleGroup = GROUP_PREFIX + '_' + groupId;

            // Only allow logged in experts or group experts
            if ( !UserHelpers.isExpert( this.userId, roleGroup ) ) {
                throw new Meteor.Error( 'unauthorized' );
            }

            // UPDATE

            const group = GroupCollection.findOne( { _id: groupId }, { fields: { _id: 1 } } );
            if ( !group ) {
                throw new Meteor.Error( 'group not found' );
            }

            const user = UserCollection.findOne( { _id: userId }, { fields: { _id: 1 } } );
            if ( !user ) {
                throw new Meteor.Error( 'user not found' );
            }

            log.verbose( `removeUser - ${user._id} X ${role || GroupRoles.join( ', ' )} X ${groupId}` );
            UserHelpers.removeUsersFromRoles( user._id, role || GroupRoles, roleGroup );

            // Update Conversation
            GroupHelpers.updateConversationParticipation( user._id );

            // NOTIFICATION
            //TODO: Move to audit
            if ( Meteor.isServer && userId != this.userId ) {
                GroupNotificationHelpers.unnotifyAddedToGroup( user._id, this.userId, group._id, role );
            }

            // AUDIT
            /*Audit.next( {
             domain:  GroupCollection.entityName,
             message: GroupAuditMessages.removed,
             userId:  this.userId,
             groupId:  groupId
             } );*/
        }
    } ),

    /**
     * Validate an Invitation Code
     *
     * @param {string} invitationCode
     */
    validateInvitationCode: new ValidatedMethod( {
        name:     'pr/group/validateInvitationCode',
        validate: new SimpleSchema( {
            invitationCode: { type: String }
        } ).validator(),
        run( { invitationCode } ) {
            if ( Meteor.isServer ) {
                // PLATO-528 - We need to trim on order to prevent errors when people copy-paste the code
                invitationCode = invitationCode.trim();

                const group = GroupCollection.findOne(
                    {
                        invitationCode: {
                            $regex:   `^${escapeStringRegexp( invitationCode )}$`,
                            $options: 'i'
                        }
                    },
                    {
                        fields:    { _id: 1 },
                        transform: null
                    }
                );
                if ( !group ) {
                    throw new Meteor.Error( 'invalid code' );
                }

                const role      = RoleList.member;
                const roleGroup = GROUP_PREFIX + '_' + group._id;

                if ( !UserHelpers.userIsInRole( this.userId, role, roleGroup ) ) {
                    // Update Permissions
                    UserHelpers.addUsersToRoles( this.userId, role, roleGroup );

                    // Update Conversation
                    GroupHelpers.updateConversationParticipation( this.userId );

                    // NOTIFICATION
                    //TODO: Move to audit
                    GroupNotificationHelpers.notifyAddedToGroup( this.userId, null, group._id, [role] );
                }
            }

            return true;
        }
    } ),

    clearConversation: new ValidatedMethod( {
        name:     'pr/group/clearConversation',
        validate: new SimpleSchema( {
            groupId: { type: String }
        } ).validator(),
        run( { groupId } ) {

            // SECURITY

            const roleGroup = GROUP_PREFIX + '_' + groupId;

            // Only allow logged in experts or group experts
            if ( !UserHelpers.isExpert( this.userId, roleGroup ) ) {
                throw new Meteor.Error( 'unauthorized' );
            }

            // UPDATE

            GroupHelpers.clearConversation( groupId );
        }
    } )
};

export default GroupMethods;
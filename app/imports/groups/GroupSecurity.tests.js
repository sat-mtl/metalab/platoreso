"use strict";

import {chai, assert, expect} from 'meteor/practicalmeteor:chai';
import {sinon, spies, stubs} from 'meteor/practicalmeteor:sinon';

import GroupCollection from "/imports/groups/GroupCollection";
import GroupSecurity from "/imports/groups/GroupSecurity";
import {GroupRoles, GROUP_PREFIX} from "/imports/groups/GroupRoles";
import UserHelpers from "/imports/accounts/UserHelpers";
import {RoleList, RoleGroups} from "/imports/accounts/Roles";

describe( 'groups/GroupSecurity', function () {

    let sandbox;

    beforeEach( () => {
        sandbox = sinon.sandbox.create();
    } );

    afterEach( () => {
        sandbox.restore();
    } );

    describe( 'canRead', function () {

        beforeEach( function () {
            sandbox.stub( GroupSecurity, 'getUserAccess' );
        } );

        it( 'returns if the user is allowed to read/view the group', function () {
            GroupSecurity.getUserAccess.returns( { allowed: true } );
            expect( GroupSecurity.canRead( 'user', 'group', false ) ).to.be.true;
            assert( GroupSecurity.getUserAccess.calledWith( 'user', 'group', false ) );
        } );

        it( 'returns if the user is not allowed to read/view the group', function () {
            GroupSecurity.getUserAccess.returns( { allowed: false } );
            expect( GroupSecurity.canRead( 'user', 'group', false ) ).to.be.false;
            assert( GroupSecurity.getUserAccess.calledWith( 'user', 'group', false ) );
        } );

        it( 'returns if the user is allowed to read/view the group (memberOnly)', function () {
            GroupSecurity.getUserAccess.returns( { allowed: true } );
            expect( GroupSecurity.canRead( 'user', 'group', true ) ).to.be.true;
            assert( GroupSecurity.getUserAccess.calledWith( 'user', 'group', true ) );
        } );

        it( 'shortcut if public group object & not membersOnly', function () {
            expect( GroupSecurity.canRead( 'user', { _id: 'group', public: true }, false ) ).to.be.true;
            assert.isFalse( GroupSecurity.getUserAccess.called );
        } );

        it( 'bypass shortcut if not public group object & not membersOnly', function () {
            GroupSecurity.getUserAccess.returns( { allowed: true } );
            expect( GroupSecurity.canRead( 'user', { _id: 'group', public: false }, false ) ).to.be.true;
            assert( GroupSecurity.getUserAccess.calledWith( 'user', { _id: 'group', public: false }, false ) );
        } );

        it( 'bypass shortcut if public group object & membersOnly', function () {
            GroupSecurity.getUserAccess.returns( { allowed: true } );
            expect( GroupSecurity.canRead( 'user', { _id: 'group', public: true }, true ) ).to.be.true;
            assert( GroupSecurity.getUserAccess.calledWith( 'user', { _id: 'group', public: true }, true ) );
        } );

        it( 'bypass shortcut if public is not boolean', function () {
            GroupSecurity.getUserAccess.returns( { allowed: true } );
            expect( GroupSecurity.canRead( 'user', { _id: 'group', public: 'yes' }, false ) ).to.be.true;
            assert( GroupSecurity.getUserAccess.calledWith( 'user', { _id: 'group', public: 'yes' }, false ) );
        } );

        it( 'returns if the user is not allowed to read/view the group (memberOnly)', function () {
            GroupSecurity.getUserAccess.returns( { allowed: false } );
            expect( GroupSecurity.canRead( 'user', 'group', true ) ).to.be.false;
            assert( GroupSecurity.getUserAccess.calledWith( 'user', 'group', true ) );
        } );

        it( 'memberOnly should default to false', function () {
            GroupSecurity.getUserAccess.returns( { allowed: true } );
            GroupSecurity.canRead( 'user', 'group' );
            assert( GroupSecurity.getUserAccess.calledWith( 'user', 'group', false ) );
        } );

        it( 'user can be null', function () {
            GroupSecurity.getUserAccess.returns( { allowed: true } );
            GroupSecurity.canRead( null, 'group' );
            assert( GroupSecurity.getUserAccess.calledWith( null, 'group', false ) );
        } );

        it( 'should fail on invalid group parameter (null)', function () {
            expect( ()=>GroupSecurity.canRead( 'user', null, false ) ).to.throw( /Match error/ );
            assert.isFalse( GroupSecurity.getUserAccess.called );
        } );

        it( 'should fail on invalid group parameter (number)', function () {
            expect( ()=>GroupSecurity.canRead( 'user', 123, false ) ).to.throw( /Match error/ );
            assert.isFalse( GroupSecurity.getUserAccess.called );
        } );

        it( 'should fail on invalid group parameter (object without id)', function () {
            expect( ()=>GroupSecurity.canRead( 'user', { name: 'group', public: true }, false ) ).to.throw( /Match error/ );
            assert.isFalse( GroupSecurity.getUserAccess.called );
        } );

        it( 'should fail on invalid user parameter', function () {
            expect( ()=>GroupSecurity.canRead( 1234, 'group', false ) ).to.throw( /Match error/ );
            assert.isFalse( GroupSecurity.getUserAccess.called );
        } );

        it( 'should fail on invalid memberOnly parameter', function () {
            expect( ()=>GroupSecurity.canRead( 'user', 'group', 'pouet' ) ).to.throw( /Match error/ );
            assert.isFalse( GroupSecurity.getUserAccess.called );
        } );

    } );

    describe( 'canEdit', function () {

        beforeEach( function () {
            sandbox.stub( UserHelpers, 'isModerator' );
            sandbox.stub( UserHelpers, 'userIsInRole' );
        } );

        it( 'returns if the user is allowed to edit the group (global moderator)', function () {
            UserHelpers.isModerator.returns( true );
            expect( GroupSecurity.canEdit( 'user', 'group' ) ).to.be.true;
            assert( UserHelpers.isModerator.calledWith( 'user' ) );
            assert.isFalse( UserHelpers.userIsInRole.called );
        } );

        it( 'returns if the user is allowed to edit the group (group moderator)', function () {
            UserHelpers.isModerator.returns( false );
            UserHelpers.userIsInRole.returns( true );
            expect( GroupSecurity.canEdit( 'user', 'group' ) ).to.be.true;
            assert( UserHelpers.isModerator.calledWith( 'user' ) );
            assert( UserHelpers.userIsInRole.calledWith( 'user', RoleList.expert, GROUP_PREFIX + '_group' ) );
        } );

        it( 'returns if the user is not allowed to edit the group', function () {
            UserHelpers.isModerator.returns( false );
            UserHelpers.userIsInRole.returns( false );
            expect( GroupSecurity.canEdit( 'user', 'group' ) ).to.be.false;
            assert( UserHelpers.isModerator.calledWith( 'user' ) );
            assert( UserHelpers.userIsInRole.calledWith( 'user', RoleList.expert, GROUP_PREFIX + '_group' ) );
        } );

        it( 'returns false if the user not specified', function () {
            expect( GroupSecurity.canEdit( null, 'group' ) ).to.be.false;
            assert.isFalse( UserHelpers.userIsInRole.called );
        } );

        it( 'throws if the user is invalid', function () {
            expect( ()=>GroupSecurity.canEdit( 123, 'group' ) ).to.throw( /Match error/ );
            assert.isFalse( UserHelpers.isModerator.called );
            assert.isFalse( UserHelpers.userIsInRole.called );
        } );

        it( 'throws if the group is not specified/invalid', function () {
            expect( ()=>GroupSecurity.canEdit( null ) ).to.throw( /Match error/ );
            assert.isFalse( UserHelpers.isModerator.called );
            assert.isFalse( UserHelpers.userIsInRole.called );
        } );

    } );

    describe( 'getUserAccess', function () {

        beforeEach( function () {
            sandbox.stub( UserHelpers, 'userIsInRole' );
            sandbox.stub( GroupCollection, 'findOne' );
        } );

        it( 'returns member access to the group for a member', function () {
            UserHelpers.userIsInRole.returns( true );
            expect( GroupSecurity.getUserAccess( 'user', 'group', false ) ).to.eql( { allowed: true, member: true } );
            assert( UserHelpers.userIsInRole.calledWith( 'user', RoleGroups.experts.concat( GroupRoles ), GROUP_PREFIX + '_group' ) );
            assert.isFalse( GroupCollection.findOne.called );
        } );

        it( 'returns positive member access to the group for a non member', function () {
            UserHelpers.userIsInRole.returns( false );
            GroupCollection.findOne.returns( { _id: 'group', public: true } );
            expect( GroupSecurity.getUserAccess( 'user', 'group', false ) ).to.eql( { allowed: true, member: false } );
            assert( UserHelpers.userIsInRole.calledWith( 'user', RoleGroups.experts.concat( GroupRoles ), GROUP_PREFIX + '_group' ) );
            assert( GroupCollection.findOne.calledWith( { _id: 'group' }, { fields: { public: 1 }, transform: null } ) );
        } );

        it( 'returns negative member access to the group for a non member', function () {
            UserHelpers.userIsInRole.returns( false );
            GroupCollection.findOne.returns( { _id: 'group', public: false } );
            expect( GroupSecurity.getUserAccess( 'user', 'group', false ) ).to.eql( { allowed: false, member: false } );
            assert( UserHelpers.userIsInRole.calledWith( 'user', RoleGroups.experts.concat( GroupRoles ), GROUP_PREFIX + '_group' ) );
            assert( GroupCollection.findOne.calledWith( { _id: 'group' }, { fields: { public: 1 }, transform: null } ) );
        } );

        it( 'memberOnly defaults to false', function () {
            UserHelpers.userIsInRole.returns( false );
            GroupCollection.findOne.returns( { _id: 'group', public: true } );
            expect( GroupSecurity.getUserAccess( 'user', 'group' ) ).to.eql( { allowed: true, member: false } );
            assert( UserHelpers.userIsInRole.calledWith( 'user', RoleGroups.experts.concat( GroupRoles ), GROUP_PREFIX + '_group' ) );
            assert( GroupCollection.findOne.calledWith( { _id: 'group' }, { fields: { public: 1 }, transform: null } ) );
        } );

        it( 'returns positive member access to the group for a non user', function () {
            GroupCollection.findOne.returns( { _id: 'group', public: true } );
            expect( GroupSecurity.getUserAccess( null, 'group', false ) ).to.eql( { allowed: true, member: false } );
            assert.isFalse( UserHelpers.userIsInRole.called );
            assert( GroupCollection.findOne.calledWith( { _id: 'group' }, { fields: { public: 1 }, transform: null } ) );
        } );

        it( 'returns negative member access to the group for a non user', function () {
            GroupCollection.findOne.returns( { _id: 'group', public: false } );
            expect( GroupSecurity.getUserAccess( null, 'group', false ) ).to.eql( { allowed: false, member: false } );
            assert.isFalse( UserHelpers.userIsInRole.called );
            assert( GroupCollection.findOne.calledWith( { _id: 'group' }, { fields: { public: 1 }, transform: null } ) );
        } );

        it( 'returns member access to the group for a not found group', function () {
            GroupCollection.findOne.returns( null );
            expect( GroupSecurity.getUserAccess( null, 'group', false ) ).to.eql( { allowed: false, member: false } );
            assert.isFalse( UserHelpers.userIsInRole.called );
            assert( GroupCollection.findOne.calledWith( { _id: 'group' }, { fields: { public: 1 }, transform: null } ) );
        } );

        it( 'returns member access to the group for a non user when only allowing members', function () {
            GroupCollection.findOne.returns( { _id: 'group' } );
            expect( GroupSecurity.getUserAccess( null, 'group', true ) ).to.eql( { allowed: false, member: false } );
            assert.isFalse( UserHelpers.userIsInRole.called );
            assert.isFalse( GroupCollection.findOne.called );
        } );

        it( 'skips fetching the group if it already has the public key (true)', function () {
            expect( GroupSecurity.getUserAccess( null, { _id: 'group', public: true }, false ) ).to.eql( { allowed: true, member: false } );
            assert.isFalse( UserHelpers.userIsInRole.called );
            assert.isFalse( GroupCollection.findOne.called );
        } );

        it( 'skips fetching the group if it already has the public key (false)', function () {
            expect( GroupSecurity.getUserAccess( null, { _id: 'group', public: false }, false ) ).to.eql( { allowed: false, member: false } );
            assert.isFalse( UserHelpers.userIsInRole.called );
            assert.isFalse( GroupCollection.findOne.called );
        } );

        it( 'throws on invalid group parameter', function () {
            expect( () => GroupSecurity.getUserAccess( null ) ).to.throw( /Match error/ );
            assert.isFalse( UserHelpers.userIsInRole.called );
            assert.isFalse( GroupCollection.findOne.called );
        } );

        it( 'throws on invalid user parameter', function () {
            expect( () => GroupSecurity.getUserAccess( 123, 'group', 123 ) ).to.throw( /Match error/ );
            assert.isFalse( UserHelpers.userIsInRole.called );
            assert.isFalse( GroupCollection.findOne.called );
        } );

        it( 'throws on invalid memberOnly parameter', function () {
            expect( () => GroupSecurity.getUserAccess( 'user', 'group', 123 ) ).to.throw( /Match error/ );
            assert.isFalse( UserHelpers.userIsInRole.called );
            assert.isFalse( GroupCollection.findOne.called );
        } );

    } );

    describe( 'canManageGroups', function () {

        beforeEach( function () {
            sandbox.stub( UserHelpers, 'getGroupsForUser' );
        } );

        it( 'should tell if the user is a group expert for any group', function () {
            UserHelpers.getGroupsForUser.returns( [GROUP_PREFIX + '_group1', GROUP_PREFIX + '_group2'] );
            expect( GroupSecurity.canManageGroups( 'user' ) ).to.be.true;
            assert( UserHelpers.getGroupsForUser.calledWith( 'user', RoleGroups.experts ) );
        } );

        it( 'should tell if the user is a group expert for any group (strict)', function () {
            UserHelpers.getGroupsForUser.returns( [GROUP_PREFIX + '_group1', GROUP_PREFIX + '_group2'] );
            expect( GroupSecurity.canManageGroups( 'user', null, true ) ).to.be.true;
            assert( UserHelpers.getGroupsForUser.calledWith( 'user', RoleList.expert ) );
        } );

        it( 'should tell if the user is not a group expert for any group', function () {
            UserHelpers.getGroupsForUser.returns( [] );
            expect( GroupSecurity.canManageGroups( 'user' ) ).to.be.false;
            assert( UserHelpers.getGroupsForUser.calledWith( 'user', RoleGroups.experts ) );
        } );

        it( 'should tell if the user is not a group expert for any group (strict)', function () {
            UserHelpers.getGroupsForUser.returns( [] );
            expect( GroupSecurity.canManageGroups( 'user', null, true ) ).to.be.false;
            assert( UserHelpers.getGroupsForUser.calledWith( 'user', RoleList.expert ) );
        } );

        it( 'should return false if getGroupsForUser returns null or undefined', function () {
            UserHelpers.getGroupsForUser.returns( null );
            expect( GroupSecurity.canManageGroups( 'user' ) ).to.be.false;
            assert( UserHelpers.getGroupsForUser.calledWith( 'user', RoleGroups.experts ) );
        } );

        it( 'should tell if the user is a group expert for a specific group', function () {
            UserHelpers.getGroupsForUser.returns( [GROUP_PREFIX + '_group1', GROUP_PREFIX + '_group2'] );
            expect( GroupSecurity.canManageGroups( 'user', 'group1' ) ).to.be.true;
            assert( UserHelpers.getGroupsForUser.calledWith( 'user', RoleGroups.experts ) );
        } );

        it( 'should tell if the user is a group expert for at least one of the specified groups', function () {
            UserHelpers.getGroupsForUser.returns( [GROUP_PREFIX + '_group1', GROUP_PREFIX + '_group2'] );
            expect( GroupSecurity.canManageGroups( 'user', ['group1', 'group3'] ) ).to.be.true;
            assert( UserHelpers.getGroupsForUser.calledWith( 'user', RoleGroups.experts ) );
        } );

        it( 'should tell if the user is not a group expert for a specific group', function () {
            UserHelpers.getGroupsForUser.returns( [] );
            expect( GroupSecurity.canManageGroups( 'user', 'group1' ) ).to.be.false;
            assert( UserHelpers.getGroupsForUser.calledWith( 'user', RoleGroups.experts ) );
        } );

        it( 'should tell if the user is not a group expert for at least one of the specified groups', function () {
            UserHelpers.getGroupsForUser.returns( [] );
            expect( GroupSecurity.canManageGroups( 'user', ['group1', 'group2'] ) ).to.be.false;
            assert( UserHelpers.getGroupsForUser.calledWith( 'user', RoleGroups.experts ) );
        } );

        it( 'should support a user object', function () {
            UserHelpers.getGroupsForUser.returns( [GROUP_PREFIX + '_group1', GROUP_PREFIX + '_group2'] );
            expect( GroupSecurity.canManageGroups( { _id: 'user' } ) ).to.be.true;
            assert( UserHelpers.getGroupsForUser.calledWith( { _id: 'user' }, RoleGroups.experts ) );
        } );

        it( 'should return false on null user', function () {
            expect( GroupSecurity.canManageGroups() ).to.eql( false );
            assert.isFalse( UserHelpers.getGroupsForUser.called );
        } );

        it( 'should throw on invalid user', function () {
            expect( ()=>GroupSecurity.canManageGroups( 123 ) ).to.throw( /Match error/ );
            assert.isFalse( UserHelpers.getGroupsForUser.called );
        } );

        it( 'should throw on invalid groups', function () {
            expect( ()=>GroupSecurity.canManageGroups( 'user', 123 ) ).to.throw( /Match error/ );
            assert.isFalse( UserHelpers.getGroupsForUser.called );
        } );

    } );

    describe( 'canModerateGroups', function () {

        beforeEach( function () {
            sandbox.stub( UserHelpers, 'getGroupsForUser' );
        } );

        it( 'should tell if the user is a group moderator for any group', function () {
            UserHelpers.getGroupsForUser.returns( [GROUP_PREFIX + '_group1', GROUP_PREFIX + '_group2'] );
            expect( GroupSecurity.canModerateGroups( 'user' ) ).to.be.true;
            assert( UserHelpers.getGroupsForUser.calledWith( 'user', RoleGroups.moderators ) );
        } );

        it( 'should tell if the user is a group moderator for any group (strict)', function () {
            UserHelpers.getGroupsForUser.returns( [GROUP_PREFIX + '_group1', GROUP_PREFIX + '_group2'] );
            expect( GroupSecurity.canModerateGroups( 'user', null, true ) ).to.be.true;
            assert( UserHelpers.getGroupsForUser.calledWith( 'user', RoleList.moderator ) );
        } );

        it( 'should tell if the user is not a group moderator for any group', function () {
            UserHelpers.getGroupsForUser.returns( [] );
            expect( GroupSecurity.canModerateGroups( 'user' ) ).to.be.false;
            assert( UserHelpers.getGroupsForUser.calledWith( 'user', RoleGroups.moderators ) );
        } );

        it( 'should tell if the user is not a group moderator for any group (strict)', function () {
            UserHelpers.getGroupsForUser.returns( [] );
            expect( GroupSecurity.canModerateGroups( 'user', null, true ) ).to.be.false;
            assert( UserHelpers.getGroupsForUser.calledWith( 'user', RoleList.moderator ) );
        } );

        it( 'should return false if getGroupsForUser returns null or undefined', function () {
            UserHelpers.getGroupsForUser.returns( null );
            expect( GroupSecurity.canModerateGroups( 'user' ) ).to.be.false;
            assert( UserHelpers.getGroupsForUser.calledWith( 'user', RoleGroups.moderators ) );
        } );

        it( 'should tell if the user is a group moderator for a specific group', function () {
            UserHelpers.getGroupsForUser.returns( [GROUP_PREFIX + '_group1', GROUP_PREFIX + '_group2'] );
            expect( GroupSecurity.canModerateGroups( 'user', 'group1' ) ).to.be.true;
            assert( UserHelpers.getGroupsForUser.calledWith( 'user', RoleGroups.moderators ) );
        } );

        it( 'should tell if the user is a group moderator for at least one of the specified groups', function () {
            UserHelpers.getGroupsForUser.returns( [GROUP_PREFIX + '_group1', GROUP_PREFIX + '_group2'] );
            expect( GroupSecurity.canModerateGroups( 'user', ['group1', 'group3'] ) ).to.be.true;
            assert( UserHelpers.getGroupsForUser.calledWith( 'user', RoleGroups.moderators ) );
        } );

        it( 'should tell if the user is not a group moderator for a specific group', function () {
            UserHelpers.getGroupsForUser.returns( [] );
            expect( GroupSecurity.canModerateGroups( 'user', 'group1' ) ).to.be.false;
            assert( UserHelpers.getGroupsForUser.calledWith( 'user', RoleGroups.moderators ) );
        } );

        it( 'should tell if the user is not a group moderator for at least one of the specified groups', function () {
            UserHelpers.getGroupsForUser.returns( [] );
            expect( GroupSecurity.canModerateGroups( 'user', ['group1', 'group2'] ) ).to.be.false;
            assert( UserHelpers.getGroupsForUser.calledWith( 'user', RoleGroups.moderators ) );
        } );

        it( 'should support a user object', function () {
            UserHelpers.getGroupsForUser.returns( [GROUP_PREFIX + '_group1', GROUP_PREFIX + '_group2'] );
            expect( GroupSecurity.canModerateGroups( { _id: 'user' } ) ).to.be.true;
            assert( UserHelpers.getGroupsForUser.calledWith( { _id: 'user' }, RoleGroups.moderators ) );
        } );

        it( 'should return false on null user', function () {
            expect( GroupSecurity.canModerateGroups() ).to.eql( false );
            assert.isFalse( UserHelpers.getGroupsForUser.called );
        } );

        it( 'should throw on invalid user', function () {
            expect( ()=>GroupSecurity.canModerateGroups( 123 ) ).to.throw( /Match error/ );
            assert.isFalse( UserHelpers.getGroupsForUser.called );
        } );

        it( 'should throw on invalid groups', function () {
            expect( ()=>GroupSecurity.canModerateGroups( 'user', 123 ) ).to.throw( /Match error/ );
            assert.isFalse( UserHelpers.getGroupsForUser.called );
        } );

    } );

} );

"use strict";

import CollectionHelpers from "/imports/collections/CollectionHelpers";
import ModerationHelpers from "/imports/moderation/ModerationHelpers";
import UserHelpers from "/imports/accounts/UserHelpers";
import { RoleGroups } from "/imports/accounts/Roles";
import Group from "./model/Group";
import GroupSchema from "./model/GroupSchema";
import GroupHelpers from "./GroupHelpers";

class GroupCollection extends Mongo.Collection {
    constructor( name, options = {} ) {
        // Change transform before calling super, this is the only way to set it
        // Also, this won't suffer from module loading as it is a callback
        options.transform = group => new Group( group );

        super( name, options );

        this.entityName = "group";

        /**
         * Schema
         */

        this.attachSchema( GroupSchema );

        /**
         * Indexes
         */

        if ( Meteor.isServer ) {
            // Group list
            this._ensureIndex( { moderated: 1, public: 1 }, { name: 'moderated_public' } );
        }

        /**
         * Security
         */

        this.deny( {
            insert: () => true,
            update: () => true,
            remove: () => true
        } );

        /**
         * Behaviors
         */

        this.attachBehaviour( 'timestampable' );

        // The rest goes into a Meteor.startup because since we create the collection instance
        // right away when exporting from this module, we end up executing this constructor
        // before the imported modules are available (when there are circular dependencies).
        // This way we are sure the app is loaded when we execute the following code.
        Meteor.startup( () => {
            /**
             * Registration
             * TODO: Get rid of that system
             */

            CollectionHelpers.registerCollection( this.entityName, this, Group );

            /**
             * Helpers
             */
            ModerationHelpers.setup( this, Group );

        } );
    }

    findUserManagedGroups( userId, query = {}, fields = {}, options = {} ) {
        let q = _.extend( {
            $or: [{ moderated: false }, { moderated: { $exists: false } }]
        }, query );

        let o = _.extend( {
            fields: _.extend( {
                _id:  1,
                slug: 1,
                name: 1
            }, fields )
        }, options );

        if ( UserHelpers.isExpert( userId ) ) {
            // Allow all groups
            delete q._id;
        } else {
            // Merge query groups with groups the user has access to as an expert or more
            const userGroups = GroupHelpers.getGroupsForUser( userId, RoleGroups.experts );
            if ( !q._id ) {
                q._id = {};
            }
            if ( !q._id.$in ) {
                q._id.$in = [];
            }
            q._id.$in = _.uniq( userGroups.concat( q._id.$in ) );
        }

        return this.find( q, o );
    }
}

export default new GroupCollection( "groups" );
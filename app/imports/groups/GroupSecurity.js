"use strict";

import { RoleList, RoleGroups } from "/imports/accounts/Roles";
import UserHelpers from "/imports/accounts/UserHelpers";
import { GroupRoles, GROUP_PREFIX } from "./GroupRoles";
import GroupCollection from "./GroupCollection";

/**
 * Group Security
 *
 * For every method in Group Methods, a rule should exist here to allow/deny the action.
 * Collection-based security will be deprecated in favor of per-method security, thus limiting the potential
 * risk of allowing unwanted modifications to the collection.
 */
export default class GroupSecurity {

    /**
     * Checks if a group can be read
     *
     * @param {String} userId
     * @param {String|Object} group
     * @param {Boolean} memberOnly Allow access to public groups when false (useful when checking a project's permission)
     * @returns {Boolean}
     */
    static canRead( userId = null, group, memberOnly = false ) {
        check( group, Match.OneOf(
            String, // Group Id
            Match.ObjectIncluding( { _id: String } ), // Does not have the public field but at least has an id
            Match.ObjectIncluding( { _id: String, public: Boolean } ) // Already has the public field populated
        ) );
        check( userId, Match.OneOf( String, null ) );
        check( memberOnly, Boolean );

        // Skip a bunch of operations if public and allowing non-members
        // Check for strict equality in case we received a property that's not a boolean
        if ( !memberOnly && _.isObject( group ) && group.public === true ) {
            return true;
        }

        // Otherwise check the group user access
        return GroupSecurity.getUserAccess( userId, group, memberOnly ).allowed;
    }

    /**
     * Check if a user can edit a group
     * Either a global moderator or a group expert can edit a group
     *
     * @param {String} userId
     * @param {String|Object} group
     * @returns {Boolean}
     */
    static canEdit( userId = null, group ) {
        check( group, Match.OneOf(
            Match.ObjectIncluding( { _id: String } ),
            String
        ) );
        check( userId, Match.OneOf( String, null ) );

        if ( !userId ) {
            // Not logged in, scram
            return false;
        }

        if ( UserHelpers.isModerator( userId ) ) {
            // Global moderators and higher, allowed
            return true;
        }

        // Logged-in, find the group if we don't already have the project key
        const groupId = _.isString( group ) ? group : group._id;

        // Group experts are allowed
        return UserHelpers.userIsInRole( userId, RoleList.expert, GROUP_PREFIX + '_' + groupId );
    }

    /**
     * Can Create a Group
     *
     * @param userId
     * @param group
     * @returns {Boolean}
     */
    static canCreate( userId = null, group ) {
        check( userId, Match.OneOf( String, null ) );
        return UserHelpers.isExpert( userId );
    }

    static canUpdate( userId = null, group ) {
        return GroupSecurity.canEdit( userId, group );
    }

    static canRemove( userId = null, group ) {
        return GroupSecurity.canEdit( userId, group );
    }

    /**
     * Get user access to a group
     * Allows the differentiation between having access to a group because it is public or because the user is a member.
     *
     * @param {String} userId
     * @param {String} group
     * @param {Boolean} memberOnly Allow access to public groups when false (useful when checking a project's permission)
     * @returns {Object} allowed, member whether the user is allowed and is a member of the group
     */
    static getUserAccess( userId = null, group, memberOnly = false ) {
        check( group, Match.OneOf(
            String, // Group Id
            Match.ObjectIncluding( { _id: String } ), // Does not have the public field but at least has an id
            Match.ObjectIncluding( { _id: String, public: Boolean } ) // Already has the public field populated
        ) );
        check( userId, Match.OneOf( String, null ) );
        check( memberOnly, Boolean );

        const groupId = _.isObject( group ) ? group._id : group;
        let allowed   = false;
        let member    = false;

        // Check if user is a member of the group, by concatenating allowed global groups to group roles
        if ( userId && UserHelpers.userIsInRole( userId, RoleGroups.experts.concat( GroupRoles ), GROUP_PREFIX + '_' + groupId ) ) {
            allowed = true;
            member  = true;

        } else if ( !memberOnly ) {
            // Logged-in or not (doesn't matter here) and allowing non-members, check if group is public
            // Get the group if we were passed an id or a group without public
            if ( _.isString( group ) || _.isUndefined( group.public ) ) {
                group = GroupCollection.findOne( { _id: groupId }, { fields: { public: 1 }, transform: null } );
            }

            // Not-not used in case group.public is undefined
            allowed = !!group && !!group.public;
            member  = false;
        }

        return { allowed: allowed, member: member };
    }

    /**
     * Check if the user is a group expert
     *
     * @param user
     * @param {Array} [groups] Arrays of groups to check, otherwise just returns if user is in *any* group
     * @param {Boolean} [strict] Should we check for *only* experts and not higher ranked roles
     * @param {Boolean} [allGroups] Should all groups match or at least one
     * @returns {boolean}
     */
    static canManageGroups( user = null, groups = null, strict = false, allGroups = false ) {
        check( user, Match.OneOf( Object, String, null ) );
        check( groups, Match.OneOf( String, [String], null ) );
        check( strict, Boolean );

        if ( !user ) {
            return false;
        }

        if ( UserHelpers.isExpert( user ) ) {
            return true;
        }

        // Get the groups for which the user is expert (exact role or array, depending on strictness)
        const expertGroups = UserHelpers.getGroupsForUser( user, strict ? RoleList.expert : RoleGroups.experts );

        if ( !groups ) {
            // If we are merely testing that the user *is* a moderator, having at least one group is a go
            return !!expertGroups && expertGroups.length != 0;
        } else {
            // If we are checking for one or many groups then intersect both group lists
            if ( _.isString( groups ) ) {
                groups = [groups];
            }

            // Checked in this direction instead of moderatorGroups.some()
            // because it was easier to concatenate GROUP_PREFIX than remove it for the indexOf()
            if ( allGroups ) {
                return groups.every( group => expertGroups.indexOf( GROUP_PREFIX + '_' + group ) != -1 );
            } else {
                return groups.some( group => expertGroups.indexOf( GROUP_PREFIX + '_' + group ) != -1 );
            }
        }
    }

    /**
     * Check if the user is a group moderator
     *
     * @param user
     * @param {Array} [groups] Arrays of groups to check, otherwise just returns if user is in *any* group
     * @param {Boolean} [strict] Should we check for *only* moderators and not higher ranked roles
     * @returns {boolean}
     */
    static canModerateGroups( user = null, groups = null, strict = false ) {
        check( user, Match.OneOf( Object, String, null ) );
        check( groups, Match.OneOf( String, [String], null ) );
        check( strict, Boolean );

        if ( !user ) {
            return false;
        }

        if ( UserHelpers.isModerator( user ) ) {
            return true;
        }

        // Get the groups for which the user is moderator (exact role or array, depending on strictness)
        const moderatorGroups = UserHelpers.getGroupsForUser( user, strict ? RoleList.moderator : RoleGroups.moderators );

        if ( !groups ) {
            // If we are merely testing that the user *is* a moderator, having at least on group is a go
            return !!moderatorGroups && moderatorGroups.length != 0;
        } else {
            // If we are checking for one or many groups then intersect both group lists
            if ( _.isString( groups ) ) {
                groups = [groups];
            }

            // Checked in this direction instead of moderatorGroups.some()
            // because it was easier to concatenate GROUP_PREFIX than remove it for the indexOf()
            return groups.some( group => moderatorGroups.indexOf( GROUP_PREFIX + '_' + group ) != -1 );
        }
    }

}
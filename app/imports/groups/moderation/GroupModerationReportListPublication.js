"use strict";

import slugify from "underscore.string/slugify";
import { Counts } from "meteor/tmeasday:publish-counts";
import GroupCollection from "../GroupCollection";
import GroupImagesCollection from "../GroupImagesCollection";

import UserHelpers from "/imports/accounts/UserHelpers";

/**
 * Moderation Report List (Administration)
 *
 * @params {Object} [options] Options for the find operation (ex: sort)
 */
Meteor.publishComposite( "group/moderation/report/list", function (query = null, options = null) {

    // Check query for only the allowed fields
    check( query, Match.OneOf(
        null, {
            name: Match.Optional( Match.ObjectIncluding( {$regex: String} ) ),
            moderated: Match.Optional( Boolean )
        }
    ) );

    // Check options for only the allowed fields
    check( options, Match.OneOf( null, {
        sort: Match.Optional(Object),
        limit: Match.Optional(Number),
        skip: Match.Optional(Number)
    } ) );

    if ( !UserHelpers.isModerator( this.userId ) ) {
        return this.ready();
    }

    query = query || {};

    // Keep the original (unmodified by permissions, to publish counts to a unique URI)
    const originalQuery = _.clone( query );

    options = _.extend( options || {}, {
        fields: {
            _id: 1,
            slug: 1,
            name: 1,
            moderated:   1,
            reportCount: 1
        }
    });

    let children = [
        {
            find( group ) {
                return GroupImagesCollection.find( {owners: group._id} );
            }
        }
    ];

    return {
        find() {
            // Slugify the json query as a unique identifier for the count
            Counts.publish( this,
                'group/moderation/report/count/' + slugify( JSON.stringify(originalQuery) ),
                GroupCollection.findReported( query, {fields: {_id: 1} } ),
                { noReady: true }
            );

            return GroupCollection.findReported( query, options )
        },
        children: children
    };
} );
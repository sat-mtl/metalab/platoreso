"use strict";

import { RoleList } from "../accounts/Roles";
export const GROUP_PREFIX = 'group';

export const GroupRoles = [
    RoleList.expert,
    RoleList.moderator,
    RoleList.member
];
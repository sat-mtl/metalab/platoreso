"use strict";

import { chai, assert, expect } from "meteor/practicalmeteor:chai";
import { sinon, spies, stubs } from "meteor/practicalmeteor:sinon";
import GroupSecurity from "/imports/groups/GroupSecurity";
import GroupMethods from "/imports/groups/GroupMethods";
import GroupHelpers from "/imports/groups/GroupHelpers";
import GroupCollection from "/imports/groups/GroupCollection";
import { GROUP_PREFIX } from "/imports/groups/GroupRoles";
import GroupNotificationHelpers from "/imports/groups/notifications/GroupNotificationHelpers";
import UserCollection from "/imports/accounts/UserCollection";
import UserHelpers from "/imports/accounts/UserHelpers";
import { RoleList } from "/imports/accounts/Roles";

describe( 'groups/GroupMethods', function () {

    let sandbox;

    beforeEach( () => {
        sandbox = sinon.sandbox.create();
    } );

    afterEach( () => {
        sandbox.restore();
    } );

    describe( 'create', function () {

        let context;
        let groupInfo;
        let cleanGroupInfo;

        beforeEach( function () {
            context = { userId: 'userId' };

            groupInfo      = { name: 'Test group', description: "This is a test group." };
            cleanGroupInfo = {
                name:        'Test group',
                public:      false,
                showMembers: true,
                description: "This is a test group."
            };

            sandbox.stub( GroupSecurity, 'canCreate' );
            sandbox.stub( GroupCollection, 'insert' ).returns( "groupId" );
            sandbox.stub( GroupMethods.addUser, 'call' );
        } );

        describe( 'check successful', function () {

            it( 'should create a group if allowed', function () {
                GroupSecurity.canCreate.returns( true );

                expect( () => GroupMethods.create._execute( context, { group: _.clone( groupInfo ) } ) ).to.not.throw();

                assert( GroupSecurity.canCreate.calledWith( "userId", cleanGroupInfo ) );
                assert( GroupCollection.insert.calledWith( cleanGroupInfo ) );
                assert( GroupMethods.addUser.call.calledWith( {
                    groupId: "groupId",
                    userId:  "userId",
                    roles:   [RoleList.expert]
                } ) );
            } );

            it( 'should not create a group if now allowed', function () {
                GroupSecurity.canCreate.returns( false );

                expect( () => GroupMethods.create._execute( context, { group: _.clone( groupInfo ) } ) ).to.throw();

                assert( GroupSecurity.canCreate.calledWith( "userId", cleanGroupInfo ) );
                assert.isFalse( GroupCollection.insert.called );
                assert.isFalse( GroupMethods.addUser.call.called );
            } );

            it( 'should clean extra fields', function () {
                GroupSecurity.canCreate.returns( true );

                expect( () => GroupMethods.create._execute( context, { group: _.extend( { pouet: "poupou" }, groupInfo ) } ) ).to.not.throw();

                assert( GroupSecurity.canCreate.calledWith( "userId", cleanGroupInfo ) );
                assert( GroupCollection.insert.calledWith( cleanGroupInfo ) );
            } );

        } );

        describe( 'check failed', function () {

            it( 'should fail on missing name', function () {
                delete groupInfo.name;
                expect( () => GroupMethods.create._execute( context, groupInfo ) ).to.throw( /validation-error/ );
                assert.isFalse( GroupSecurity.canCreate.called );
                assert.isFalse( GroupCollection.insert.called );
                assert.isFalse( GroupMethods.addUser.call.called );
            } );

            it( 'should fail on missing description', function () {
                delete groupInfo.description;
                expect( () => GroupMethods.create._execute( context, groupInfo ) ).to.throw( /validation-error/ );
                assert.isFalse( GroupSecurity.canCreate.called );
                assert.isFalse( GroupCollection.insert.called );
                assert.isFalse( GroupMethods.addUser.call.called );
            } );

        } );

        if ( Meteor.isServer ) {

            describe( 'permissions', function () {

                beforeEach( () => {
                    sandbox.stub( UserCollection, "findOne" );
                    sandbox.stub( Accounts, "createUser" );
                    sandbox.stub( Accounts, "sendEnrollmentEmail" );
                } );

                it( 'should throw if missing role', () => {
                    GroupSecurity.canCreate.returns( true );

                    expect( () => GroupMethods.create._execute( context, {
                        group:       _.clone( groupInfo ),
                        permissions: [
                            {
                                user: "userId"
                            }
                        ]
                    } ) ).to.throw( /validation-error/ );

                    assert.isFalse( UserCollection.findOne.called );
                    assert.isFalse( Accounts.createUser.called );
                    assert.isFalse( Accounts.sendEnrollmentEmail.called );
                    assert.isFalse( GroupMethods.addUser.call.called );
                } );

                it( 'should throw if invalid role', () => {
                    GroupSecurity.canCreate.returns( true );

                    expect( () => GroupMethods.create._execute( context, {
                        group:       _.clone( groupInfo ),
                        permissions: [
                            {
                                user:  "userId",
                                roles: ["whatever"]
                            }
                        ]
                    } ) ).to.throw( /validation-error/ );

                    assert.isFalse( UserCollection.findOne.called );
                    assert.isFalse( Accounts.createUser.called );
                    assert.isFalse( Accounts.sendEnrollmentEmail.called );
                    assert.isFalse( GroupMethods.addUser.call.called );
                } );

                it( 'should filter out creator from permissions', () => {
                    GroupSecurity.canCreate.returns( true );

                    expect( () => GroupMethods.create._execute( context, {
                        group:       _.clone( groupInfo ),
                        permissions: [
                            {
                                user:  "userId",
                                roles: [RoleList.moderator]
                            }
                        ]
                    } ) ).to.not.throw();

                    assert.isFalse( UserCollection.findOne.called );
                    assert.isFalse( Accounts.createUser.called );
                    assert.isFalse( Accounts.sendEnrollmentEmail.called );
                    assert( GroupMethods.addUser.call.calledOnce );
                } );

                it( 'should try to find users by email', () => {
                    GroupSecurity.canCreate.returns( true );

                    expect( () => GroupMethods.create._execute( context, {
                        group:       _.clone( groupInfo ),
                        permissions: [
                            {
                                email: "me@you.com",
                                roles: [RoleList.moderator]
                            }
                        ]
                    } ) ).to.not.throw();

                    assert( UserCollection.findOne.calledWith( { 'emails.address': "me@you.com" }, {
                        fields:    { _id: 1 },
                        transform: null
                    } ) );
                } );

                it( 'should find user by email and add them', () => {
                    GroupSecurity.canCreate.returns( true );
                    UserCollection.findOne.returns( { _id: "otherUserId" } );

                    expect( () => GroupMethods.create._execute( context, {
                        group:       _.clone( groupInfo ),
                        permissions: [
                            {
                                email: "me@you.com",
                                roles: [RoleList.moderator]
                            }
                        ]
                    } ) ).to.not.throw();

                    assert( UserCollection.findOne.calledWith( { 'emails.address': "me@you.com" }, {
                        fields:    { _id: 1 },
                        transform: null
                    } ) );
                    assert.isFalse( Accounts.createUser.called );
                    assert.isFalse( Accounts.sendEnrollmentEmail.called );
                    expect( GroupMethods.addUser.call.callCount ).to.equal( 2 );
                    assert( GroupMethods.addUser.call.getCall( 1 ).calledWith( {
                        groupId: "groupId",
                        userId:  "otherUserId",
                        roles:   [RoleList.moderator]
                    } ) );
                } );

                it( 'should fail to find user by email and invite them', () => {
                    GroupSecurity.canCreate.returns( true );
                    UserCollection.findOne.returns( null );
                    Accounts.createUser.returns( "newUserId" );

                    expect( () => GroupMethods.create._execute( context, {
                        group:       _.clone( groupInfo ),
                        permissions: [
                            {
                                email: "me@you.com",
                                roles: [RoleList.moderator]
                            }
                        ]
                    } ) ).to.not.throw();

                    assert( UserCollection.findOne.calledWith( { 'emails.address': "me@you.com" }, {
                        fields:    { _id: 1 },
                        transform: null
                    } ) );
                    assert( Accounts.createUser.calledWith( { email: "me@you.com", profile: {} } ) );
                    assert( Accounts.sendEnrollmentEmail.calledWith( "newUserId" ) );
                    expect( GroupMethods.addUser.call.callCount ).to.equal( 2 );
                    assert( GroupMethods.addUser.call.getCall( 1 ).calledWith( {
                        groupId: "groupId",
                        userId:  "newUserId",
                        roles:   [RoleList.moderator]
                    } ) );
                } );

            } );

        }

    } );

    describe( 'update', function () {

        let context;
        let groupInfo;
        let modifier;

        beforeEach( function () {
            context = { userId: 'userId' };

            groupInfo = { name: 'some name' };
            modifier  = { $set: { name: 'some name' } };

            sandbox.stub( GroupSecurity, 'canUpdate' );
            sandbox.stub( GroupCollection, 'find' );
            sandbox.stub( GroupCollection, 'findOne' );
            sandbox.stub( GroupCollection, 'update' );
        } );

        describe( 'check successful', function () {

            it( 'should update a group if allowed', function () {
                GroupSecurity.canUpdate.returns( true );
                GroupCollection.findOne.returns( { _id: "groupId" } );

                expect( () => GroupMethods.update._execute( context, {
                    _id:     "groupId",
                    changes: _.clone( groupInfo )
                } ) ).to.not.throw();

                assert( GroupSecurity.canUpdate.calledWith( "userId", "groupId" ) );
                assert( GroupCollection.findOne.calledWith( { _id: "groupId" }, {
                    fields:    { _id: 1 },
                    transform: null
                } ) );
                assert( GroupCollection.update.calledWith( { _id: "groupId" }, modifier ) );
            } );

            it( 'should clean keys', function () {
                groupInfo.extra = "key";
                GroupSecurity.canUpdate.returns( true );
                GroupCollection.findOne.returns( { _id: "groupId" } );

                expect( () => GroupMethods.update._execute( context, {
                    _id:     "groupId",
                    changes: _.clone( groupInfo )
                } ) ).to.not.throw();

                assert( GroupSecurity.canUpdate.calledWith( "userId", "groupId" ) );
                assert( GroupCollection.findOne.calledWith( { _id: "groupId" }, {
                    fields:    { _id: 1 },
                    transform: null
                } ) );
                assert( GroupCollection.update.calledWith( { _id: "groupId" }, modifier ) );
            } );

            it( 'should not update a group if nothing to update', function () {
                delete groupInfo.name;
                GroupSecurity.canUpdate.returns( true );
                GroupCollection.findOne.returns( { _id: "groupId" } );

                expect( () => GroupMethods.update._execute( context, {
                    _id:     "groupId",
                    changes: _.clone( groupInfo )
                } ) ).to.not.throw();

                assert( GroupSecurity.canUpdate.calledWith( "userId", "groupId" ) );
                assert( GroupCollection.findOne.calledWith( { _id: "groupId" }, {
                    fields:    { _id: 1 },
                    transform: null
                } ) );
                assert.isFalse( GroupCollection.update.called );
            } );

            it( 'should not update a group if not allowed', function () {
                GroupSecurity.canUpdate.returns( false );

                expect( () => GroupMethods.update._execute( context, {
                    _id:     "groupId",
                    changes: _.clone( groupInfo )
                } ) ).to.throw( /unauthorized/ );

                assert( GroupSecurity.canUpdate.calledWith( "userId", "groupId" ) );
                assert.isFalse( GroupCollection.findOne.called );
                assert.isFalse( GroupCollection.update.called );
            } );

            it( 'should update if invitationCode is available', function () {
                const countStub = sinon.stub();
                countStub.returns( 0 );

                GroupSecurity.canUpdate.returns( true );
                GroupCollection.find.returns( { count: countStub } );
                GroupCollection.findOne.returns( { _id: "groupId" } );

                groupInfo.invitationCode     = "inviteMe";
                modifier.$set.invitationCode = "inviteMe";

                expect( () => GroupMethods.update._execute( context, {
                    _id:     "groupId",
                    changes: _.clone( groupInfo )
                } ) ).to.not.throw();

                assert( GroupSecurity.canUpdate.calledWith( "userId", "groupId" ) );
                assert( GroupCollection.find.calledWith( {
                    _id:            { $ne: 'groupId' },
                    invitationCode: { $regex: "^inviteMe$", $options: 'i' }
                } ) );
                assert( countStub.called );
                assert( GroupCollection.findOne.calledWith( { _id: "groupId" }, {
                    fields:    { _id: 1 },
                    transform: null
                } ) );
                assert( GroupCollection.update.calledWith( { _id: "groupId" }, modifier ) );
            } );

            it( 'should throw if invitationCode is not available', function () {
                const countStub = sinon.stub();
                countStub.returns( 1 );

                GroupSecurity.canUpdate.returns( true );
                GroupCollection.find.returns( { count: countStub } );
                GroupCollection.findOne.returns( { _id: "groupId" } );

                groupInfo.invitationCode = "inviteMe";

                expect( () => GroupMethods.update._execute( context, {
                    _id:     "groupId",
                    changes: _.clone( groupInfo )
                } ) ).to.throw( /invitation code not available/ );

                assert( GroupSecurity.canUpdate.calledWith( "userId", "groupId" ) );
                assert( GroupCollection.find.calledWith( {
                    _id:            { $ne: 'groupId' },
                    invitationCode: { $regex: "^inviteMe$", $options: 'i' }
                } ) );
                assert( countStub.called );
                assert( GroupCollection.findOne.calledWith( { _id: "groupId" }, {
                    fields:    { _id: 1 },
                    transform: null
                } ) );
                assert.isFalse( GroupCollection.update.called );
            } );

        } );

        describe( 'check failed', function () {

            it( 'should fail on missing id', function () {
                delete groupInfo._id;
                expect( () => GroupMethods.update._execute( context, { changes: _.clone( groupInfo ) } ) ).to.throw( /validation-error/ );
                assert.isFalse( GroupSecurity.canUpdate.called );
                assert.isFalse( GroupCollection.findOne.called );
                assert.isFalse( GroupCollection.update.called );
            } );

        } );

        if ( Meteor.isServer ) {

            describe( 'permissions', function () {

                beforeEach( () => {
                    sandbox.stub( GroupMethods.addUser, 'call' );
                    sandbox.stub( GroupMethods.removeUser, 'call' );
                    sandbox.stub( UserCollection, "findOne" );
                    sandbox.stub( Accounts, "createUser" );
                    sandbox.stub( Accounts, "sendEnrollmentEmail" );
                    GroupSecurity.canUpdate.returns( true );
                    GroupCollection.findOne.returns( { _id: "groupId" } );
                } );

                it( 'should throw if invalid role', () => {
                    expect( () => GroupMethods.update._execute( context, {
                        _id:         "groupId",
                        changes:     _.clone( groupInfo ),
                        permissions: [
                            {
                                user:  "userId",
                                roles: ["whatever"]
                            }
                        ]
                    } ) ).to.throw( /validation-error/ );

                    assert.isFalse( UserCollection.findOne.called );
                    assert.isFalse( Accounts.createUser.called );
                    assert.isFalse( Accounts.sendEnrollmentEmail.called );
                    assert.isFalse( GroupMethods.addUser.call.called );
                    assert.isFalse( GroupMethods.removeUser.call.called );
                } );

                it( 'should try to find users by email', () => {
                    expect( () => GroupMethods.update._execute( context, {
                        _id:         "groupId",
                        changes:     _.clone( groupInfo ),
                        permissions: [
                            {
                                email: "me@you.com",
                                roles: [RoleList.moderator]
                            }
                        ]
                    } ) ).to.not.throw();

                    assert( UserCollection.findOne.calledWith( { 'emails.address': "me@you.com" }, {
                        fields:    { _id: 1 },
                        transform: null
                    } ) );
                } );

                it( 'should find user by email and add them', () => {
                    UserCollection.findOne.returns( { _id: "otherUserId" } );

                    expect( () => GroupMethods.update._execute( context, {
                        _id:         "groupId",
                        changes:     _.clone( groupInfo ),
                        permissions: [
                            {
                                email: "me@you.com",
                                roles: [RoleList.moderator]
                            }
                        ]
                    } ) ).to.not.throw();

                    assert( UserCollection.findOne.calledWith( { 'emails.address': "me@you.com" }, {
                        fields:    { _id: 1 },
                        transform: null
                    } ) );
                    assert.isFalse( Accounts.createUser.called );
                    assert.isFalse( Accounts.sendEnrollmentEmail.called );
                    assert( GroupMethods.addUser.call.calledWith( {
                        groupId: "groupId",
                        userId:  "otherUserId",
                        roles:   [RoleList.moderator]
                    } ) );
                    assert.isFalse( GroupMethods.removeUser.call.called );
                } );

                it( 'should fail to find user by email and invite them', () => {
                    UserCollection.findOne.returns( null );
                    Accounts.createUser.returns( "newUserId" );

                    expect( () => GroupMethods.update._execute( context, {
                        _id:         "groupId",
                        changes:     _.clone( groupInfo ),
                        permissions: [
                            {
                                email: "me@you.com",
                                roles: [RoleList.moderator]
                            }
                        ]
                    } ) ).to.not.throw();

                    assert( UserCollection.findOne.calledWith( { 'emails.address': "me@you.com" }, {
                        fields:    { _id: 1 },
                        transform: null
                    } ) );
                    assert( Accounts.createUser.calledWith( { email: "me@you.com", profile: {} } ) );
                    assert( Accounts.sendEnrollmentEmail.calledWith( "newUserId" ) );
                    assert( GroupMethods.addUser.call.calledWith( {
                        groupId: "groupId",
                        userId:  "newUserId",
                        roles:   [RoleList.moderator]
                    } ) );
                    assert.isFalse( GroupMethods.removeUser.call.called );
                } );

                it( 'should remove users from group by user id', () => {
                    expect( () => GroupMethods.update._execute( context, {
                        _id:         "groupId",
                        changes:     _.clone( groupInfo ),
                        permissions: [
                            {
                                user:    "removedUserId",
                                deleted: true
                            }
                        ]
                    } ) ).to.not.throw();

                    assert.isFalse( UserCollection.findOne.called );
                    assert.isFalse( Accounts.createUser.called );
                    assert.isFalse( Accounts.sendEnrollmentEmail.called );
                    assert.isFalse( GroupMethods.addUser.call.called );
                    assert( GroupMethods.removeUser.call.calledWith( {
                        groupId: "groupId",
                        userId:  "removedUserId"
                    } ) );
                } );

                it( 'should remove users from group by user email (weird but it works)', () => {
                    UserCollection.findOne.returns( { _id: "removedUserId" } );

                    expect( () => GroupMethods.update._execute( context, {
                        _id:         "groupId",
                        changes:     _.clone( groupInfo ),
                        permissions: [
                            {
                                email:   "me@you.com",
                                deleted: true
                            }
                        ]
                    } ) ).to.not.throw();

                    assert( UserCollection.findOne.calledWith( { 'emails.address': "me@you.com" }, {
                        fields:    { _id: 1 },
                        transform: null
                    } ) );
                    assert.isFalse( Accounts.createUser.called );
                    assert.isFalse( Accounts.sendEnrollmentEmail.called );
                    assert.isFalse( GroupMethods.addUser.call.called );
                    assert( GroupMethods.removeUser.call.calledWith( {
                        groupId: "groupId",
                        userId:  "removedUserId"
                    } ) );
                } );

            } );

        }

    } );

    describe( 'remove', function () {

        let context;
        let groupId;

        beforeEach( function () {
            context = { userId: 'userId' };

            groupId = 'groupId';

            sandbox.stub( GroupSecurity, 'canRemove' );
            sandbox.stub( GroupCollection, 'remove' );
        } );

        describe( 'check successful', function () {

            it( 'should remove a group if allowed', function () {
                GroupSecurity.canRemove.returns( true );
                expect( () => GroupMethods.remove._execute( context, { groupId } ) ).to.not.throw();
                assert( GroupSecurity.canRemove.calledWith( "userId", "groupId" ) );
                assert( GroupCollection.remove.calledWith( { _id: groupId } ) );
            } );

            it( 'should not remove a group if not allowed', function () {
                GroupSecurity.canRemove.returns( false );
                expect( () => GroupMethods.remove._execute( context, { groupId } ) ).to.throw();
                assert( GroupSecurity.canRemove.calledWith( "userId", "groupId" ) );
                assert.isFalse( GroupCollection.remove.called );
            } );

        } );

        describe( 'check failed', function () {

            it( 'should fail on bad id (number)', function () {
                expect( () => GroupMethods.remove._execute( context, { groupId: 123 } ) ).to.throw( /validation-error/ );
                assert.isFalse( GroupSecurity.canRemove.called );
            } );

            it( 'should fail on bad id (null)', function () {
                expect( () => GroupMethods.remove._execute( context, { groupId: null } ) ).to.throw( /validation-error/ );
                assert.isFalse( GroupSecurity.canRemove.called );
            } );

            it( 'should fail on bad id (missing)', function () {
                expect( () => GroupMethods.remove._execute( context ) ).to.throw( /must be an object/ );
                assert.isFalse( GroupSecurity.canRemove.called );
            } );

        } );

    } );

    describe( 'addUser', function () {

        let context;
        let group;
        let user;
        let role;

        beforeEach( function () {
            context = { userId: 'userId' };
            group   = 'groupId';
            user    = 'userId';
            role    = RoleList.moderator;
            sandbox.stub( UserHelpers, 'isExpert' );
            sandbox.stub( GroupCollection, 'findOne' );
            sandbox.stub( UserCollection, 'findOne' );
            sandbox.stub( UserHelpers, 'setUserRoles' );
            sandbox.stub( GroupHelpers, 'updateConversationParticipation' );
        } );

        it( 'should add user to group', function () {
            UserHelpers.isExpert.returns( true );
            GroupCollection.findOne.returns( { _id: group } );
            UserCollection.findOne.returns( { _id: user } );

            expect( () => GroupMethods.addUser._execute( context, {
                groupId: group,
                userId:  user,
                roles:   [role]
            } ) ).to.not.throw();

            assert( UserHelpers.isExpert.calledWith( context.userId, GROUP_PREFIX + '_' + group ) );
            assert( GroupCollection.findOne.calledWith( { _id: group }, { fields: { _id: 1 } } ) );
            assert( UserCollection.findOne.calledWith( { _id: user }, { fields: { _id: 1 } } ) );
            assert( UserHelpers.setUserRoles.calledWith( user, [role], GROUP_PREFIX + '_' + group ) );
            assert( GroupHelpers.updateConversationParticipation.calledWith( user ) );
        } );

        it( 'should fail if not allowed', function () {
            UserHelpers.isExpert.returns( false );

            expect( () => GroupMethods.addUser._execute( context, {
                groupId: group,
                userId:  user,
                roles:   [role]
            } ) ).to.throw( /unauthorized/ );

            assert( UserHelpers.isExpert.calledWith( context.userId, GROUP_PREFIX + '_' + group ) );
            assert.isFalse( GroupCollection.findOne.called );
            assert.isFalse( UserCollection.findOne.called );
            assert.isFalse( UserHelpers.setUserRoles.called );
            assert.isFalse( GroupHelpers.updateConversationParticipation.called );
        } );

        it( 'should fail if group not found', function () {
            UserHelpers.isExpert.returns( true );
            GroupCollection.findOne.returns( null );

            expect( () => GroupMethods.addUser._execute( context, {
                groupId: group,
                userId:  user,
                roles:   [role]
            } ) ).to.throw( /group not found/ );

            assert( UserHelpers.isExpert.calledWith( context.userId, GROUP_PREFIX + '_' + group ) );
            assert( GroupCollection.findOne.calledWith( { _id: group }, { fields: { _id: 1 } } ) );
            assert.isFalse( UserCollection.findOne.called );
            assert.isFalse( UserHelpers.setUserRoles.called );
            assert.isFalse( GroupHelpers.updateConversationParticipation.called );
        } );

        it( 'should fail if user not found', function () {
            UserHelpers.isExpert.returns( true );
            GroupCollection.findOne.returns( { _id: group } );
            UserCollection.findOne.returns( null );

            expect( () => GroupMethods.addUser._execute( context, {
                groupId: group,
                userId:  user,
                roles:   [role]
            } ) ).to.throw( /user not found/ );

            assert( UserHelpers.isExpert.calledWith( context.userId, GROUP_PREFIX + '_' + group ) );
            assert( GroupCollection.findOne.calledWith( { _id: group }, { fields: { _id: 1 } } ) );
            assert( UserCollection.findOne.calledWith( { _id: user }, { fields: { _id: 1 } } ) );
            assert.isFalse( UserHelpers.setUserRoles.called );
            assert.isFalse( GroupHelpers.updateConversationParticipation.called );
        } );

        it( 'should fail if role is invalid', function () {
            role = 'pouet';
            UserHelpers.isExpert.returns( true );
            GroupCollection.findOne.returns( { _id: group } );
            UserCollection.findOne.returns( { _id: user } );

            expect( () => GroupMethods.addUser._execute( context, {
                groupId: group,
                userId:  user,
                roles:   [role]
            } ) ).to.throw( /invalid role/ );

            assert( UserHelpers.isExpert.calledWith( context.userId, GROUP_PREFIX + '_' + group ) );
            assert( GroupCollection.findOne.calledWith( { _id: group }, { fields: { _id: 1 } } ) );
            assert( UserCollection.findOne.calledWith( { _id: user }, { fields: { _id: 1 } } ) );
            assert.isFalse( UserHelpers.setUserRoles.called );
            assert.isFalse( GroupHelpers.updateConversationParticipation.called );
        } );

        it( 'should fail if invalid group parameter', function () {
            expect( () => GroupMethods.addUser._execute( context, {
                groupId: null,
                userId:  user,
                roles:   [role]
            } ) ).to.throw( /validation-error/ );
            assert.isFalse( UserHelpers.isExpert.called );
            assert.isFalse( GroupCollection.findOne.called );
            assert.isFalse( UserCollection.findOne.called );
            assert.isFalse( UserHelpers.setUserRoles.called );
            assert.isFalse( GroupHelpers.updateConversationParticipation.called );
        } );

        it( 'should fail if invalid user parameter', function () {
            UserHelpers.isExpert.returns( true );
            expect( () => GroupMethods.addUser._execute( context, {
                groupId: group,
                userId:  null,
                roles:   [role]
            } ) ).to.throw( /validation-error/ );
            assert.isFalse( UserHelpers.isExpert.called );
            assert.isFalse( GroupCollection.findOne.called );
            assert.isFalse( UserCollection.findOne.called );
            assert.isFalse( UserHelpers.setUserRoles.called );
            assert.isFalse( GroupHelpers.updateConversationParticipation.called );
        } );

        it( 'should fail if invalid role parameter', function () {
            expect( () => GroupMethods.addUser._execute( context, {
                groupId: group,
                userId:  user,
                roles:   null
            } ) ).to.throw( /validation-error/ );
            assert.isFalse( UserHelpers.isExpert.called );
            assert.isFalse( GroupCollection.findOne.called );
            assert.isFalse( UserCollection.findOne.called );
            assert.isFalse( UserHelpers.setUserRoles.called );
            assert.isFalse( GroupHelpers.updateConversationParticipation.called );
        } );
    } );

    describe( 'removeUser', function () {

        let context;
        let group;
        let user;
        let role;

        beforeEach( function () {
            context = { userId: 'userId' };
            group   = 'groupId';
            user    = 'userId';
            role    = RoleList.moderator;
            sandbox.stub( UserHelpers, 'isExpert' );
            sandbox.stub( GroupCollection, 'findOne' );
            sandbox.stub( UserCollection, 'findOne' );
            sandbox.stub( UserHelpers, 'removeUsersFromRoles' );
            sandbox.stub( GroupHelpers, 'updateConversationParticipation' );
        } );

        it( 'should remove user from group', function () {
            UserHelpers.isExpert.returns( true );
            GroupCollection.findOne.returns( { _id: group } );
            UserCollection.findOne.returns( { _id: user } );

            expect( () => GroupMethods.removeUser._execute( context, {
                groupId: group,
                userId:  user,
                role
            } ) ).to.not.throw();

            assert( UserHelpers.isExpert.calledWith( context.userId, GROUP_PREFIX + '_' + group ) );
            assert( GroupCollection.findOne.calledWith( { _id: group }, { fields: { _id: 1 } } ) );
            assert( UserCollection.findOne.calledWith( { _id: user }, { fields: { _id: 1 } } ) );
            assert( UserHelpers.removeUsersFromRoles.calledWith( user, role, GROUP_PREFIX + '_' + group ) );
            assert( GroupHelpers.updateConversationParticipation.calledWith( user ) );
        } );

        it( 'should fail if not allowed', function () {
            UserHelpers.isExpert.returns( false );
            GroupCollection.findOne.returns( { _id: group } );

            expect( () => GroupMethods.removeUser._execute( context, {
                groupId: group,
                userId:  user,
                role
            } ) ).to.throw( /unauthorized/ );

            assert( UserHelpers.isExpert.calledWith( context.userId, GROUP_PREFIX + '_' + group ) );
            assert.isFalse( GroupCollection.findOne.called );
            assert.isFalse( UserCollection.findOne.called );
            assert.isFalse( UserHelpers.removeUsersFromRoles.called );
            assert.isFalse( GroupHelpers.updateConversationParticipation.called );
        } );

        it( 'should fail if group not found', function () {
            UserHelpers.isExpert.returns( true );
            GroupCollection.findOne.returns( null );

            expect( () => GroupMethods.removeUser._execute( context, {
                groupId: group,
                userId:  user,
                role
            } ) ).to.throw( /group not found/ );

            assert( UserHelpers.isExpert.calledWith( context.userId, GROUP_PREFIX + '_' + group ) );
            assert( GroupCollection.findOne.calledWith( { _id: group }, { fields: { _id: 1 } } ) );
            assert.isFalse( UserCollection.findOne.called );
            assert.isFalse( UserHelpers.removeUsersFromRoles.called );
            assert.isFalse( GroupHelpers.updateConversationParticipation.called );
        } );

        it( 'should fail if user not found', function () {
            UserHelpers.isExpert.returns( true );
            GroupCollection.findOne.returns( { _id: group } );
            UserCollection.findOne.returns( null );

            expect( () => GroupMethods.removeUser._execute( context, {
                groupId: group,
                userId:  user,
                role
            } ) ).to.throw( /user not found/ );

            assert( UserHelpers.isExpert.calledWith( context.userId, GROUP_PREFIX + '_' + group ) );
            assert( GroupCollection.findOne.calledWith( { _id: group }, { fields: { _id: 1 } } ) );
            assert( UserCollection.findOne.calledWith( { _id: user }, { fields: { _id: 1 } } ) );
            assert.isFalse( UserHelpers.removeUsersFromRoles.called );
            assert.isFalse( GroupHelpers.updateConversationParticipation.called );
        } );

        it( 'should fail if invalid group parameter', function () {
            expect( () => GroupMethods.removeUser._execute( context, {
                groupId: null,
                userId:  user,
                role
            } ) ).to.throw( /validation-error/ );

            assert.isFalse( UserHelpers.isExpert.called );
            assert.isFalse( GroupCollection.findOne.called );
            assert.isFalse( UserCollection.findOne.called );
            assert.isFalse( UserHelpers.removeUsersFromRoles.called );
            assert.isFalse( GroupHelpers.updateConversationParticipation.called );
        } );

        it( 'should fail if invalid user parameter', function () {
            UserHelpers.isExpert.returns( true );

            expect( () => GroupMethods.removeUser._execute( context, {
                groupId: group,
                userId:  null,
                role
            } ) ).to.throw( /validation-error/ );

            assert.isFalse( UserHelpers.isExpert.called );
            assert.isFalse( GroupCollection.findOne.called );
            assert.isFalse( UserCollection.findOne.called );
            assert.isFalse( UserHelpers.removeUsersFromRoles.called );
            assert.isFalse( GroupHelpers.updateConversationParticipation.called );
        } );

        it( 'should fail if invalid role parameter', function () {
            expect( () => GroupMethods.removeUser._execute( context, {
                groupId: group,
                userid:  user,
                role:    null
            } ) ).to.throw( /validation-error/ );

            assert.isFalse( UserHelpers.isExpert.called );
            assert.isFalse( GroupCollection.findOne.called );
            assert.isFalse( UserCollection.findOne.called );
            assert.isFalse( UserHelpers.removeUsersFromRoles.called );
            assert.isFalse( GroupHelpers.updateConversationParticipation.called );
        } );
    } );

    describe( 'validateInvitationCode', () => {

        let context;

        beforeEach( () => {
            context = { userId: 'userId' };
            sandbox.stub( GroupCollection, 'findOne' );
            sandbox.stub( UserHelpers, 'userIsInRole' );
            sandbox.stub( UserHelpers, 'addUsersToRoles' );
            sandbox.stub( GroupNotificationHelpers, 'notifyAddedToGroup' );
        } );

        if ( Meteor.isClient ) {

            it( 'should do nothing on the client', () => {
                expect( GroupMethods.validateInvitationCode._execute( context, { invitationCode: 'code' } ) ).to.be.true;
                assert.isFalse( GroupCollection.findOne.called );
                assert.isFalse( UserHelpers.userIsInRole.called );
                assert.isFalse( UserHelpers.addUsersToRoles.called );
                assert.isFalse( GroupNotificationHelpers.notifyAddedToGroup.called );
            } );

        }

        if ( Meteor.isServer ) {

            it( 'should add the user to the group', () => {
                GroupCollection.findOne.returns( { _id: 'groupId' } );
                UserHelpers.userIsInRole.returns( false );

                expect( GroupMethods.validateInvitationCode._execute( context, { invitationCode: 'code' } ) ).to.be.true;

                assert( GroupCollection.findOne.calledWith(
                    {
                        invitationCode: {
                            $regex:   '^code$',
                            $options: 'i'
                        }
                    },
                    {
                        fields:    { _id: 1 },
                        transform: null
                    }
                ) );
                assert( UserHelpers.userIsInRole.calledWith( 'userId', RoleList.member, GROUP_PREFIX + '_' + 'groupId' ) );
                assert( UserHelpers.addUsersToRoles.calledWith( 'userId', RoleList.member, GROUP_PREFIX + '_' + 'groupId' ) );
                assert( GroupNotificationHelpers.notifyAddedToGroup.calledWith( 'userId', null, 'groupId', [RoleList.member] ) );
            } );

            it( 'should trim the code', () => {
                GroupCollection.findOne.returns( { _id: 'groupId' } );
                UserHelpers.userIsInRole.returns( false );

                expect( GroupMethods.validateInvitationCode._execute( context, { invitationCode: '   code   ' } ) ).to.be.true;

                assert( GroupCollection.findOne.calledWith(
                    {
                        invitationCode: {
                            $regex:   '^code$',
                            $options: 'i'
                        }
                    },
                    {
                        fields:    { _id: 1 },
                        transform: null
                    }
                ) );
                assert( UserHelpers.userIsInRole.calledWith( 'userId', RoleList.member, GROUP_PREFIX + '_' + 'groupId' ) );
                assert( UserHelpers.addUsersToRoles.calledWith( 'userId', RoleList.member, GROUP_PREFIX + '_' + 'groupId' ) );
                assert( GroupNotificationHelpers.notifyAddedToGroup.calledWith( 'userId', null, 'groupId', [RoleList.member] ) );
            } );

            it( 'should not add the user to the group if already member', () => {
                GroupCollection.findOne.returns( { _id: 'groupId' } );
                UserHelpers.userIsInRole.returns( true );

                expect( GroupMethods.validateInvitationCode._execute( context, { invitationCode: 'code' } ) ).to.be.true;

                assert( GroupCollection.findOne.calledWith(
                    {
                        invitationCode: {
                            $regex:   '^code$',
                            $options: 'i'
                        }
                    },
                    {
                        fields:    { _id: 1 },
                        transform: null
                    }
                ) );
                assert( UserHelpers.userIsInRole.calledWith( 'userId', RoleList.member, GROUP_PREFIX + '_' + 'groupId' ) );
                assert.isFalse( UserHelpers.addUsersToRoles.called );
                assert.isFalse( GroupNotificationHelpers.notifyAddedToGroup.called );
            } );

            it( 'should fail if invitation code is not found', () => {
                GroupCollection.findOne.returns( null );

                expect( () => GroupMethods.validateInvitationCode._execute( context, { invitationCode: 'code' } ) ).to.throw( /invalid code/ );

                assert( GroupCollection.findOne.calledWith(
                    {
                        invitationCode: {
                            $regex:   '^code$',
                            $options: 'i'
                        }
                    },
                    {
                        fields:    { _id: 1 },
                        transform: null
                    }
                ) );
                assert.isFalse( UserHelpers.userIsInRole.called );
                assert.isFalse( UserHelpers.addUsersToRoles.called );
                assert.isFalse( GroupNotificationHelpers.notifyAddedToGroup.called );
            } );

        }

        function assertCheckFailed() {
            assert.isFalse( GroupCollection.findOne.called );
            assert.isFalse( UserHelpers.userIsInRole.called );
            assert.isFalse( UserHelpers.addUsersToRoles.called );
            assert.isFalse( GroupNotificationHelpers.notifyAddedToGroup.called );
        }

        it( 'should throw on missing validation code', () => {
            expect( () => GroupMethods.validateInvitationCode._execute( context, {} ) ).to.throw( /validation-error/ );
            assertCheckFailed();
        } );

        it( 'should throw on invalid validation code', () => {
            expect( () => GroupMethods.validateInvitationCode._execute( context, { validationCode: 123 } ) ).to.throw( /validation-error/ );
            assertCheckFailed();
        } );

    } );
} );

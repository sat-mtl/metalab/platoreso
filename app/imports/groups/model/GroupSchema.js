"use strict";

import slugify from "underscore.string/slugify";
import GroupCollection from "/imports/groups/GroupCollection";
import { SimpleSchema } from "meteor/aldeed:simple-schema";

const validGroupName = new RegExp('(!?[a-zA-Z0-9])');

/**
 * Group Schema Base
 * @type {SimpleSchema}
 */
export const GroupSchemaBase = new SimpleSchema( {
    name:           {
        type: String,
        min:  3,
        max:  140, // Like twitter, because reasons,
        custom: function() {
            if ( this.value.match(validGroupName) == null ) {
                return "notAllowed";
            }
        }
    },
    description:    {
        type: String
    },
    content:        {
        type:     String,
        optional: true
    },
    public:         {
        type:         Boolean,
        defaultValue: false
    },
    showMembers:    {
        type:         Boolean,
        defaultValue: true
    },
    invitationCode: {
        type:     String,
        optional: true,
        index:    true,
        unique:   true
    }
} );

/**
 * Group Schema
 * @type {SimpleSchema}
 */
const GroupSchema = new SimpleSchema( [
    GroupSchemaBase,
    {
        // Auto-valued field to sort by case-insensitive name
        name_sort: {
            type:      String,
            index:     true,
            autoValue: function () {
                var name = this.field( "name" );
                if ( name.isSet ) {
                    return name.value.toLowerCase();
                } else {
                    this.unset(); // Prevent users from supplying their own value
                }
            }
        },
        slug:      {
            type:      String,
            index:     true,
            unique:    true,
            autoValue: function () {
                var name = this.field( "name" );
                let slug = null;
                if ( name.isSet ) {
                    slug = slugify( name.value );
                } else if ( this.isSet ) {
                    slug = slugify( this.value );
                }

                if ( slug != null ) {
                    let count    = 0;
                    let safeSlug = slug;
                    while ( GroupCollection.findOne( { _id: { $ne: this.docId }, slug: safeSlug } ) != null ) {
                        count++;
                        safeSlug = slug + ( count > 0 ? count.toString() : '' );
                    }
                    return safeSlug;
                }
            }
        }
    }
] );
export default GroupSchema;

/**
 * Group Update Schema
 * @type {SimpleSchema}
 */
export const GroupUpdateSchema = new SimpleSchema( [
    {
        name:        {
            type:     String,
            min:  3,
            max:  140, // Like twitter, because reasons,
            optional: true,
            custom: function() {
                if ( this.isSet && this.value.match(validGroupName) == null ) {
                    return "notAllowed";
                }
            }
        },
        description: {
            type:     String,
            optional: true
        },
        public:      {
            type:     Boolean,
            optional: true
        },
        showMembers: {
            type:     Boolean,
            optional: true
        }
    },
    GroupSchema.pick( [
        'content',
        'invitationCode'
    ] )
] );
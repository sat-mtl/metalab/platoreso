"use strict";

import capitalize from "underscore.string/capitalize";
import md from "/imports/utils/markdown";
import UserHelpers from "/imports/accounts/UserHelpers"
import {GroupRoles, GROUP_PREFIX} from "../GroupRoles";
import {RoleList, RoleGroups} from "/imports/accounts/Roles";
import GroupImagesCollection from "../GroupImagesCollection";

/**
 * Group Model
 * @constructor
 */
export default class Group {

    /**
     * @constructor
     * @param doc
     */
    constructor( doc ) {
        _.extend( this, doc );
    }

    /**
     * Get the card URI
     * This is used to identify the card in URL's
     *
     * @returns {String}
     */
    get uri() {
        return this.slug || this._id
    }

    /**
     * Get description as rendered markdown
     *
     * @getter
     * @returns {String}
     */
    get descriptionMarkdown() {
        if ( !this._descriptionMarkdown ) {
            this._descriptionMarkdown = md( this.description || '' );
        }
        return this._descriptionMarkdown;
    }

    /**
     * Get the groups's initials.
     *
     * @returns {String} Whatever we can come up with to display some identification
     */
    get initials( ) {
        const parts = this.name.split(' ');
        let name = '';
        if ( parts.length > 1 ) {
            name = (parts[0].substr(0, 1) + parts[1].substr(0, 1)).toUpperCase();
        } else {
            name = capitalize( parts[0].substr(0, 2), true );
        }
        return name;
    }

    /**
     * Find image cursor for this group
     *
     * @param options
     * @returns {*}
     */
    findImage( options = {} ) {
        return GroupImagesCollection.find( {
            'owners': this._id
        }, options );
    }

    /**
     * Find the image for this group
     *
     * @param options
     * @returns {*}
     */
    getImage( options = {} ) {
        return GroupImagesCollection.findOne( {
            'owners': this._id
        }, options );
    }

    /**
     * Checks if this group has the user as an expert
     *
     * @param {String} userId
     * @returns {Boolean}
     */
    isExpert( userId ) {
        return UserHelpers.userIsInRole( userId, RoleGroups.experts, GROUP_PREFIX + '_' + this._id );
    }

    /**
     * Checks if this group has the user as a moderator
     *
     * @param {String} userId
     * @returns {Boolean}
     */
    isModerator( userId ) {
        return UserHelpers.userIsInRole( userId, RoleGroups.moderators, GROUP_PREFIX + '_' + this._id );
    }

    /**
     * Checks if this group has the user as a member
     *
     * @param {String} userId
     * @returns {Boolean}
     */
    isMember( userId ) {
        return UserHelpers.userIsInRole( userId, RoleGroups.members, GROUP_PREFIX + '_' + this._id );
    }

    /**
     * Checks if this group has the user as a participant
     *
     * @param {String} userId
     * @returns {Boolean}
     */
    isParticipant( userId ) {
        return UserHelpers.userIsInRole( userId, GroupRoles, GROUP_PREFIX + '_' + this._id );
    }
};
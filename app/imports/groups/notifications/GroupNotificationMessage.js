"use strict";

import {NotificationTypes} from "/imports/groups/notifications/GroupNotifications";
import NotificationMessageHelpers from "/imports/notifications/NotificationMessageHelpers";

export default class GroupNotificationMessage {

    static getLink( notification, subject, { group } ) {
        // No uri used here because card might not be transformed
        return group ? `/group/${group.slug || group._id}` : null;
    }

    static getMessage( notification, subject, { group }, html ) {
        const i18nData = {
            user:  subject ? NotificationMessageHelpers.subjectSpan( subject, html ) : null,
            group: NotificationMessageHelpers.groupSpan( group, html ),
            role:  html ? `<span class="role data">${__( 'role.' + notification.data.role )}</span>` : __( 'role.' + notification.data.role )
        };

        let message = null;

        switch ( notification.action ) {
            case NotificationTypes.added:
                if ( subject ) {
                    message = __( '__user__ added you to the group __group__ with the role __role__', i18nData );
                } else {
                    message = __( 'You were added to the group __group__ with the role __role__', i18nData );
                }
                break;
        }

        return message;
    }

}
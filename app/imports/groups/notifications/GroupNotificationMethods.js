"use strict";

import {ValidatedMethod} from "meteor/mdg:validated-method";
import NotificationCollection from "/imports/notifications/NotificationCollection";
import GroupCollection from "../GroupCollection";
import GroupImagesCollection from "../GroupImagesCollection";
import GroupSecurity from "../GroupSecurity";

const GroupNotificationMethods = {

    fetch: new ValidatedMethod( {
            name:     'pr/notification/group',
            validate: new SimpleSchema( {
                notificationId: {
                    type: String
                }
            } ).validator( { clean: true } ),
            run( { notificationId } ) {

                // NOTIFICATION

                // Can only read own notifications, so also search by userId
                const notification = NotificationCollection.findOne( { _id: notificationId, user: this.userId }, {
                    fields: {
                        objects: 1
                    }
                } );
                if ( !notification ) {
                    throw new Meteor.Error( 'notification not found' );
                }

                // GROUP

                const groupId = notification.getObjectIdByType( GroupCollection.entityName );
                // Check the project directly, it skips a call to DB
                if ( !GroupSecurity.canRead( this.userId, groupId ) ) {
                    throw new Meteor.Error( 'unauthorized' );
                }

                return {
                    group:      GroupCollection.findOne( { _id: groupId }, {
                        fields: {
                            _id:  1,
                            slug: 1,
                            name: 1
                        }
                    } ),
                    groupImage: GroupImagesCollection.findOne( { owners: groupId } )
                }; // We don't fetch user as it is already fetched by the feed
            }
        }
    )
};

export default GroupNotificationMethods;
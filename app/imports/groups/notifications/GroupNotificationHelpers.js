"use strict";

import NotificationCollection from "/imports/notifications/NotificationCollection";
import GroupCollection from "../GroupCollection";
import {NotificationTypes} from "./GroupNotifications";

export default class GroupNotificationHelpers {

    /**
     * Generate a user added to group notification
     *
     * @param {String} userId
     * @param {String} subject
     * @param {String} groupId
     * @param {String[]} roles
     */
    static notifyAddedToGroup( userId, subject, groupId, roles ) {
        check( userId, String );
        check( subject, Match.OneOf( String, null ) );
        check( groupId, String );
        check( roles, [String] );

        // We need information from the group...
        const group = GroupCollection.findOne( { _id: groupId }, {
            fields:    {
                _id: 1
            },
            transform: null
        } );
        if ( !group ) {
            logger.warn( `Could not find group ${groupId} while generating user added to group notification.` );
            return;
        }

        // Notify the added user
        NotificationCollection.notify( {
            user:    userId,
            subject: subject,
            action:  NotificationTypes.added,
            objects: [
                {
                    id:   group._id,
                    type: 'group'
                }
            ],
            data:    {
                role: roles.join('/') // Temporary fix for the roles being an array, I didn't want to go into updating templates while the feature was not ready
            }
        } );
    }

    /**
     * Remove  a user added to group notification
     *
     * @param {String} userId
     * @param {String} subject
     * @param {String} groupId
     * @param {String} role
     */
    static unnotifyAddedToGroup( userId, subject, groupId, role ) {
        check( userId, String );
        check( subject, String );
        check( groupId, String );
        check( role, Match.OneOf( String, null, undefined ) );

        // Unnotify
        NotificationCollection.unnotify(
            userId,
            subject,
            NotificationTypes.added,
            groupId,
            GroupCollection.entityName,
            role ? { 'data.role': role } : {}
        );
    }

}
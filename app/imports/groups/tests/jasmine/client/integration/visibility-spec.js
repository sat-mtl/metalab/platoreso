"use strict";

import GroupCollection from "/imports/groups/GroupCollection";

describe( 'group visibility', () => {

    beforeEach( done => {
        Meteor.call( 'fixtures/groups/reset', () => {
            done();
        } );
    } );

    afterEach( function ( done ) {
        // Logout
        Meteor.logout( () => {
            done();
        } );
    } );

    describe( 'list', () => {

        describe( 'as an admin', () => {

            let groupsSubscription;

            beforeEach( function ( done ) {
                Meteor.call('fixtures/users/be/admin', function ( err ) {
                    expect( err ).toBeUndefined();
                    groupsSubscription = Meteor.subscribe( 'group/list', done );
                    done();
                } );
            } );

            afterEach( function ( done ) {
                groupsSubscription.stop();
                done();
            } );

            it( 'should see all groups', () => {
                expect( GroupCollection.find( {} ).fetch().length ).toBe( 2 );
            } );

        } );

        describe( 'as a regular user', () => {

            let groupsSubscription;

            beforeEach( function ( done ) {
                Meteor.call('fixtures/users/be/normal', function ( err ) {
                    expect( err ).toBeUndefined();
                    let started = false;
                    groupsSubscription = Meteor.subscribe( 'group/list', {
                        onStop( err ) {
                            expect( err ).toBeUndefined();
                            if ( !started ) {
                                expect( false ).toBe(true); // Should not stop so flag it here
                            }
                        },
                        onReady() {
                            started = true;
                            done();
                        }
                    } );
                } );
            } );

            afterEach( function ( done ) {
                groupsSubscription.stop();
                done();
            } );

            it( 'should see only public groups', () => {
                expect( GroupCollection.find( {} ).fetch().length ).toBe( 1 );
                expect( GroupCollection.find( {} ).fetch()[0].public ).toBe( true );
            } );

        } );

        describe( 'as a participant', () => {

            let groupsSubscription;

            beforeEach( function ( done ) {
                Meteor.call('fixtures/users/be/normal', function ( err ) {
                    expect( err ).toBeUndefined();
                    done();
                } );
            } );

            afterEach( function ( done ) {
                groupsSubscription.stop();
                done();
            } );

            it( 'should see public and participating as expert groups', ( done ) => {
                Meteor.call( '__group__addExpert__', 'private-group', 'normal@test.com', err=> {
                    expect( err ).toBeUndefined();
                    groupsSubscription = Meteor.subscribe( 'group/list', () => {
                        const groups = GroupCollection.find( {} ).fetch();
                        expect( groups.length ).toBe( 2 );
                        expect( groups[0].public ).toBe( true );
                        expect( groups[1].public ).toBe( false );
                        done();
                    } );
                } );
            } );

            it( 'should see public and participating as moderator groups', ( done ) => {
                Meteor.call( '__group__addModerator__', 'private-group', 'normal@test.com', err=> {
                    expect( err ).toBeUndefined();
                    groupsSubscription = Meteor.subscribe( 'group/list', () => {
                        const groups = GroupCollection.find( {} ).fetch();
                        expect( groups.length ).toBe( 2 );
                        expect( groups[0].public ).toBe( true ); //FIXME: We can't predict the order
                        expect( groups[1].public ).toBe( false ); //FIXME: We can't predict the order
                        done();
                    } );
                } );
            } );

            it( 'should see public and participating as member groups', ( done ) => {
                Meteor.call( '__group__addMember__', 'private-group', 'normal@test.com', err=> {
                    expect( err ).toBeUndefined();
                    groupsSubscription = Meteor.subscribe( 'group/list', () => {
                        const groups = GroupCollection.find( {} ).fetch();
                        expect( groups.length ).toBe( 2 );
                        expect( groups[0].public ).toBe( true ); //FIXME: We can't predict the order
                        expect( groups[1].public ).toBe( false ); //FIXME: We can't predict the order
                        done();
                    } );
                } );
            } );

        } );

        describe( 'as an anonymous user', () => {

            let groupsSubscription;

            beforeEach( done => {
                groupsSubscription = Meteor.subscribe( 'group/list', done );
            } );

            afterEach( function ( done ) {
                groupsSubscription.stop();
                done();
            } );

            it( 'should see only public groups', () => {
                expect( GroupCollection.find( {} ).fetch().length ).toBe( 1 );
                expect( GroupCollection.find( {} ).fetch()[0].public ).toBe( true );
            } );

        } );

    } );

    describe( 'single groups', () => {

        describe( 'as an admin', () => {

            let groupSubscription;

            beforeEach( function ( done ) {
                Meteor.call('fixtures/users/be/admin', function ( err ) {
                    expect( err ).toBeUndefined();
                    done();
                } );
            } );

            afterEach( function ( done ) {
                groupSubscription.stop();
                done();
            } );

            it( 'should see private group', done => {
                Meteor.call( '__group_find__', 'private-group', ( err, group ) => {
                    let id            = group._id;
                    groupSubscription = Meteor.subscribe( 'group', id, () => {
                        const group = GroupCollection.findOne( { _id: id } );
                        expect( group ).not.toBeUndefined();
                        expect( group.slug ).toBe( 'private-group' );
                        done();
                    } );
                } );
            } );

            it( 'should see public group', done => {
                Meteor.call( '__group_find__', 'public-group', ( err, group ) => {
                    let id            = group._id;
                    groupSubscription = Meteor.subscribe( 'group', id, () => {
                        const group = GroupCollection.findOne( { _id: id } );
                        expect( group ).not.toBeUndefined();
                        expect( group.slug ).toBe( 'public-group' );
                        done();
                    } );
                } );
            } );

        } );

        describe( 'as a regular user', () => {

            let groupSubscription;

            beforeEach( function ( done ) {
                Meteor.call('fixtures/users/be/normal', function ( err ) {
                    expect( err ).toBeUndefined();
                    done();
                } );
            } );

            afterEach( function ( done ) {
                groupSubscription.stop();
                done();
            } );

            it( 'should not see private group', done => {
                Meteor.call( '__group_find__', 'private-group', ( err, group ) => {
                    let id            = group._id;
                    groupSubscription = Meteor.subscribe( 'group', id, () => {
                        const group = GroupCollection.findOne( { _id: id } );
                        expect( group ).toBeUndefined();
                        done();
                    } );
                } );
            } );

            it( 'should see public group', done => {
                Meteor.call( '__group_find__', 'public-group', ( err, group ) => {
                    let id            = group._id;
                    groupSubscription = Meteor.subscribe( 'group', id, () => {
                        const group = GroupCollection.findOne( { _id: id } );
                        expect( group ).not.toBeUndefined();
                        expect( group.slug ).toBe( 'public-group' );
                        done();
                    } );
                } );
            } );

        } );

        describe( 'as a participant', () => {

            let groupSubscription;

            beforeEach( function ( done ) {
                Meteor.call('fixtures/users/be/normal', function ( err ) {
                    expect( err ).toBeUndefined();
                    done();
                } );
            } );

            afterEach( function ( done ) {
                groupSubscription.stop();
                done();
            } );

            it( 'should see participating as expert group', ( done ) => {
                Meteor.call( '__group__addExpert__', 'private-group', 'normal@test.com', err=> {
                    expect( err ).toBeUndefined();
                    Meteor.call( '__group_find__', 'private-group', ( err, group ) => {
                        let id            = group._id;
                        groupSubscription = Meteor.subscribe( 'group', id, () => {
                            const group = GroupCollection.findOne( { _id: id } );
                            expect( group ).not.toBeUndefined();
                            expect( group.slug ).toBe( 'private-group' );
                            done();
                        } );
                    } );
                } );
            } );

            it( 'should see participating as moderator group', ( done ) => {
                Meteor.call( '__group__addModerator__', 'private-group', 'normal@test.com', err=> {
                    expect( err ).toBeUndefined();
                    Meteor.call( '__group_find__', 'private-group', ( err, group ) => {
                        let id            = group._id;
                        groupSubscription = Meteor.subscribe( 'group', id, () => {
                            const group = GroupCollection.findOne( { _id: id } );
                            expect( group ).not.toBeUndefined();
                            expect( group.slug ).toBe( 'private-group' );
                            done();
                        } );
                    } );
                } );
            } );

            it( 'should see participating as member group', ( done ) => {
                Meteor.call( '__group__addMember__', 'private-group', 'normal@test.com', err=> {
                    expect( err ).toBeUndefined();
                    Meteor.call( '__group_find__', 'private-group', ( err, group ) => {
                        let id            = group._id;
                        groupSubscription = Meteor.subscribe( 'group', id, () => {
                            const group = GroupCollection.findOne( { _id: id } );
                            expect( group ).not.toBeUndefined();
                            expect( group.slug ).toBe( 'private-group' );
                            done();
                        } );
                    } );
                } );
            } );

        } );

        describe( 'as an anonymous user', () => {

            let groupSubscription;

            afterEach( function ( done ) {
                groupSubscription.stop();
                done();
            } );

            it( 'should not see private group', done => {
                Meteor.call( '__group_find__', 'private-group', ( err, group ) => {
                    let id            = group._id;
                    groupSubscription = Meteor.subscribe( 'group', id, () => {
                        const group = GroupCollection.findOne( { _id: id } );
                        expect( group ).toBeUndefined();
                        done();
                    } );
                } );
            } );

            it( 'should see public group', done => {
                Meteor.call( '__group_find__', 'public-group', ( err, group ) => {
                    let id            = group._id;
                    groupSubscription = Meteor.subscribe( 'group', id, () => {
                        const group = GroupCollection.findOne( { _id: id } );
                        expect( group ).not.toBeUndefined();
                        expect( group.slug ).toBe( 'public-group' );
                        done();
                    } );
                } );
            } );

        } );

    } );
} );
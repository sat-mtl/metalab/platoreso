"use strict";

const publication = Meteor.server.publish_handlers['admin/group/edit'];

import GroupCollection from "/imports/groups/GroupCollection";
import GroupImagesCollection from "/imports/groups/GroupImagesCollection";
import UserHelpers from "/imports/accounts/UserHelpers";

describe( 'pr-groups/jasmine/server/unit/list publication', function () {

    let context;

    beforeEach( function () {
        context = {
            userId: '420',
            ready:  jasmine.createSpy( 'ready' ).and.returnValue( 'ready' ),
            onStop: jasmine.createSpy( 'onStop' ),
            added:  jasmine.createSpy( 'added' )
        };
        spyOn( UserHelpers, 'isSuper' );
        spyOn( GroupCollection, 'find' ).and.returnValue( 'group_cursor' );
        spyOn( GroupImagesCollection, 'find' ).and.returnValue( 'image_cursor' );
    } );

    it( 'should subscribe to a group if allowed', function () {
        UserHelpers.isSuper.and.returnValue( true );
        expect( ()=>expect(publication.call( context, 'groupId' )).toEqual(['group_cursor','image_cursor']) ).not.toThrow();
        expect( UserHelpers.isSuper ).toHaveBeenCalledWith( context.userId );
        expect( GroupCollection.find ).toHaveBeenCalledWith( { _id: 'groupId' } );
        expect( GroupImagesCollection.find ).toHaveBeenCalledWith( { owners: 'groupId' } );
        expect( context.ready ).not.toHaveBeenCalled();
    } );

    it( 'should not subscribe to a group if not allowed', function () {
        UserHelpers.isSuper.and.returnValue( false );
        expect( ()=>expect(publication.call( context, 'groupId' )).toEqual('ready') ).not.toThrow();
        expect( UserHelpers.isSuper ).toHaveBeenCalledWith( context.userId );
        expect( GroupCollection.find ).not.toHaveBeenCalled();
        expect( GroupImagesCollection.find ).not.toHaveBeenCalled();
        expect( context.ready ).toHaveBeenCalled();
    } );

    it( 'should throw on missing id', function () {
        expect( ()=>publication.call( context ) ).toThrowError(/Match error/);
        expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
        expect( GroupCollection.find ).not.toHaveBeenCalled();
        expect( GroupImagesCollection.find ).not.toHaveBeenCalled();
        expect( context.ready ).not.toHaveBeenCalled();
    } );

    it( 'should throw on invalid id', function () {
        expect( ()=>publication.call( context, 123 ) ).toThrowError(/Match error/);
        expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
        expect( GroupCollection.find ).not.toHaveBeenCalled();
        expect( GroupImagesCollection.find ).not.toHaveBeenCalled();
        expect( context.ready ).not.toHaveBeenCalled();
    } );
} );
"use strict";

import slugify from "underscore.string/slugify";
const publication = Meteor.server.publish_handlers['admin/group/list'];

import GroupCollection from "/imports/groups/GroupCollection";
import GroupImagesCollection from "/imports/groups/GroupImagesCollection";
 import { Counts } from "meteor/tmeasday:publish-counts";
import UserHelpers from "/imports/accounts/UserHelpers";

describe( 'pr-groups/jasmine/server/unit/list publication', function () {

    let context;
    let query;
    let options;

    beforeEach( function () {
        context = {
            userId: '420',
            ready:  jasmine.createSpy( 'ready' ),
            onStop: jasmine.createSpy( 'onStop' ),
            added:  jasmine.createSpy( 'added' )
        };
        query = {};
        options = {};
        spyOn( UserHelpers, 'isSuper' );
        spyOn( Counts, 'publish' );

        // First one for the Counts, second null so that publishComposite doesn't throw
        spyOn( GroupCollection, 'find' ).and.returnValues( 'cursor', null );
    } );

    it( 'should subscribe to a group list if allowed', function () {
        UserHelpers.isSuper.and.returnValue( true );
        expect( ()=>publication.call( context, query, options ) ).not.toThrow();
        expect( UserHelpers.isSuper ).toHaveBeenCalledWith( context.userId );

        expect( GroupCollection.find.calls.count() ).toEqual( 2 );
        expect( GroupCollection.find.calls.argsFor( 0 ) ).toEqual( [{}, { fields: { _id: 1 } }] );
        expect( GroupCollection.find.calls.argsFor( 1 ) ).toEqual( [{}, _.extend( options, { fields: jasmine.any( Object ) } )] );
        expect( Counts.publish ).toHaveBeenCalledWith( context, 'group/count/' + slugify( JSON.stringify(query) ), 'cursor', { noReady: true } );

        expect( context.ready ).toHaveBeenCalled(); // Composite
    } );

    it( 'should not subscribe to a group list if not allowed', function () {
        UserHelpers.isSuper.and.returnValue( false );
        expect( ()=>publication.call( context, query, options ) ).not.toThrow();
        expect( UserHelpers.isSuper ).toHaveBeenCalledWith( context.userId );
        expect( GroupCollection.find ).not.toHaveBeenCalled();
        expect( Counts.publish ).not.toHaveBeenCalled();
        expect( context.ready ).toHaveBeenCalled(); // Composite
    } );

    it( 'should support null queries', function () {
        UserHelpers.isSuper.and.returnValue( true );
        expect( ()=>publication.call( context, null, options ) ).not.toThrow();
        expect( UserHelpers.isSuper ).toHaveBeenCalledWith( context.userId );

        expect( GroupCollection.find.calls.count() ).toEqual( 2 );
        expect( GroupCollection.find.calls.argsFor( 0 ) ).toEqual( [{}, { fields: { _id: 1 } }] );
        expect( GroupCollection.find.calls.argsFor( 1 ) ).toEqual( [{}, _.extend( options, { fields: jasmine.any( Object ) } )] );
        expect( Counts.publish ).toHaveBeenCalledWith( context, 'group/count/' + slugify( JSON.stringify(query) ), 'cursor', { noReady: true } );

        expect( context.ready ).toHaveBeenCalled(); // Composite
    } );

    it( 'should support null options', function () {
        UserHelpers.isSuper.and.returnValue( true );
        expect( ()=>publication.call( context, query, {} ) ).not.toThrow();
        expect( UserHelpers.isSuper ).toHaveBeenCalledWith( context.userId );


        expect( GroupCollection.find.calls.count() ).toEqual( 2 );
        expect( GroupCollection.find.calls.argsFor( 0 ) ).toEqual( [{}, { fields: { _id: 1 } }] );
        expect( GroupCollection.find.calls.argsFor( 1 ) ).toEqual( [{}, _.extend( {}, { fields: jasmine.any( Object ) } )] );
        expect( Counts.publish ).toHaveBeenCalledWith( context, 'group/count/' + slugify( JSON.stringify(query) ), 'cursor', { noReady: true } );

        expect( context.ready ).toHaveBeenCalled(); // Composite
    } );

    it( 'should not support queries (for now)', function () {
        query.public = true;
        expect( ()=>publication.call( context, query, options ) ).toThrowError(/Match error/);
        expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
        expect( GroupCollection.find ).not.toHaveBeenCalled();
        expect( Counts.publish ).not.toHaveBeenCalled();
        expect( context.ready ).not.toHaveBeenCalled();
    } );

    it( 'should support sort option', function () {
        options.sort = { name: 1 };
        UserHelpers.isSuper.and.returnValue( true );
        expect( ()=>publication.call( context, query, options ) ).not.toThrow();
        expect( UserHelpers.isSuper ).toHaveBeenCalledWith( context.userId );

        expect( GroupCollection.find.calls.count() ).toEqual( 2 );
        expect( GroupCollection.find.calls.argsFor( 0 ) ).toEqual( [{}, { fields: { _id: 1 } }] );
        expect( GroupCollection.find.calls.argsFor( 1 ) ).toEqual( [{}, _.extend( options, { fields: jasmine.any( Object ) } )] );
        expect( Counts.publish ).toHaveBeenCalledWith( context, 'group/count/' + slugify( JSON.stringify(query) ), 'cursor', { noReady: true } );

        expect( context.ready ).toHaveBeenCalled(); // Composite
    } );

    it( 'should not support invalid sort option', function () {
        options.sort = "name";
        UserHelpers.isSuper.and.returnValue( true );
        expect( ()=>publication.call( context, query, options ) ).toThrowError(/Match error/);
        expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
        expect( GroupCollection.find ).not.toHaveBeenCalled();
        expect( Counts.publish ).not.toHaveBeenCalled();
        expect( context.ready ).not.toHaveBeenCalled();
    } );

    it( 'should support limit option', function () {
        options.limit = 10;
        UserHelpers.isSuper.and.returnValue( true );
        expect( ()=>publication.call( context, query, options ) ).not.toThrow();
        expect( UserHelpers.isSuper ).toHaveBeenCalledWith( context.userId );

        expect( GroupCollection.find.calls.count() ).toEqual( 2 );
        expect( GroupCollection.find.calls.argsFor( 0 ) ).toEqual( [{}, { fields: { _id: 1 } }] );
        expect( GroupCollection.find.calls.argsFor( 1 ) ).toEqual( [{}, _.extend( options, { fields: jasmine.any( Object ) } )] );
        expect( Counts.publish ).toHaveBeenCalledWith( context, 'group/count/' + slugify( JSON.stringify(query) ), 'cursor', { noReady: true } );

        expect( context.ready ).toHaveBeenCalled(); // Composite
    } );

    it( 'should not support invalid limit option', function () {
        options.limit = "ten";
        UserHelpers.isSuper.and.returnValue( true );
        expect( ()=>publication.call( context, query, options ) ).toThrowError(/Match error/);
        expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
        expect( GroupCollection.find ).not.toHaveBeenCalled();
        expect( Counts.publish ).not.toHaveBeenCalled();
        expect( context.ready ).not.toHaveBeenCalled();
    } );

    it( 'should support skip option', function () {
        options.skip = 10;
        UserHelpers.isSuper.and.returnValue( true );
        expect( ()=>publication.call( context, query, options ) ).not.toThrow();
        expect( UserHelpers.isSuper ).toHaveBeenCalledWith( context.userId );

        expect( GroupCollection.find.calls.count() ).toEqual( 2 );
        expect( GroupCollection.find.calls.argsFor( 0 ) ).toEqual( [{}, { fields: { _id: 1 } }] );
        expect( GroupCollection.find.calls.argsFor( 1 ) ).toEqual( [{}, _.extend( options, { fields: jasmine.any( Object ) } )] );
        expect( Counts.publish ).toHaveBeenCalledWith( context, 'group/count/' + slugify( JSON.stringify(query) ), 'cursor', { noReady: true } );

        expect( context.ready ).toHaveBeenCalled(); // Composite
    } );

    it( 'should not support invalid skip option', function () {
        options.skip = "ten";
        UserHelpers.isSuper.and.returnValue( true );
        expect( ()=>publication.call( context, query, options ) ).toThrowError(/Match error/);
        expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
        expect( GroupCollection.find ).not.toHaveBeenCalled();
        expect( Counts.publish ).not.toHaveBeenCalled();
        expect( context.ready ).not.toHaveBeenCalled();
    } );

    it( 'should not support extra options', function () {
        options.extra = "option";
        UserHelpers.isSuper.and.returnValue( true );
        expect( ()=>publication.call( context, query, options ) ).toThrowError(/Match error/);
        expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
        expect( GroupCollection.find ).not.toHaveBeenCalled();
        expect( Counts.publish ).not.toHaveBeenCalled();
        expect( context.ready ).not.toHaveBeenCalled();
    } );
} );
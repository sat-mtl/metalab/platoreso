"use strict";

import GroupCollection from "/imports/groups/GroupCollection";
import GroupSecurity from "/imports/groups/GroupSecurity";
const groupPublication = Meteor.server.publish_handlers['group'];

describe( 'pr-groups/jasmine/server/unit/group publication', function () {

    let context;

    beforeEach( function () {
        context = {
            userId: '420',
            ready:  jasmine.createSpy( 'ready' ),
            onStop: jasmine.createSpy( 'onStop' ),
            added:  jasmine.createSpy( 'added' )
        };
        spyOn( GroupCollection, 'findOne' );
        spyOn( GroupSecurity, 'canRead' );
        spyOn( GroupCollection, 'find' );
    } );

    afterEach( function () {
    } );

    it( 'should subscribe to a group', function () {
        GroupCollection.findOne.and.returnValue( { _id: 'groupId' } );
        GroupSecurity.canRead.and.returnValue( true );
        expect( ()=>groupPublication.call( context, 'groupId' ) ).not.toThrow();
        expect( GroupCollection.findOne ).toHaveBeenCalledWith( { $or: [{ _id: 'groupId' }, { slug: 'groupId' }] }, { fields: { _id: 1, public: 1 }, transform: null } );
        expect( GroupSecurity.canRead ).toHaveBeenCalledWith( { _id: 'groupId' }, context.userId );
        expect( GroupCollection.find ).toHaveBeenCalledWith( { _id: 'groupId' }, { fields: jasmine.any( Object ) } );
        expect( context.ready ).toHaveBeenCalled();
    } );

    it( 'should not subscribe if group is not found', function () {
        GroupCollection.findOne.and.returnValue( null );
        expect( ()=>groupPublication.call( context, 'groupId' ) ).not.toThrow();
        expect( GroupCollection.findOne ).toHaveBeenCalledWith( { $or: [{ _id: 'groupId' }, { slug: 'groupId' }] }, { fields: { _id: 1, public: 1 }, transform: null } );
        expect( GroupSecurity.canRead ).not.toHaveBeenCalled();
        expect( GroupCollection.find ).not.toHaveBeenCalled();
        expect( context.ready ).toHaveBeenCalled();
    } );

    it( 'shouldn\'t subscribe if user can\'t read', function () {
        GroupCollection.findOne.and.returnValue( { _id: 'groupId' } );
        GroupSecurity.canRead.and.returnValue( false );
        expect( ()=>groupPublication.call( context, 'groupId' ) ).not.toThrow();
        expect( GroupCollection.findOne ).toHaveBeenCalledWith( { $or: [{ _id: 'groupId' }, { slug: 'groupId' }] }, { fields: { _id: 1, public: 1 }, transform: null } );
        expect( GroupSecurity.canRead ).toHaveBeenCalledWith( { _id: 'groupId' }, context.userId );
        expect( GroupCollection.find ).not.toHaveBeenCalled( );
        expect( context.ready ).toHaveBeenCalled();
    } );

    it( 'should throw on bad group id', function () {
        expect( ()=>groupPublication.call( context, null ) ).toThrowError( /Match\ error/ );
        expect( GroupCollection.findOne ).not.toHaveBeenCalled();
        expect( GroupSecurity.canRead ).not.toHaveBeenCalled();
        expect( GroupCollection.find ).not.toHaveBeenCalled();
        expect( context.ready ).not.toHaveBeenCalled();
    } );

} );
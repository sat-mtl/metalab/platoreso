"use strict";

const publication = Meteor.server.publish_handlers['group/list'];

import GroupCollection from "/imports/groups/GroupCollection";
import GroupHelpers from "/imports/groups/GroupHelpers";
import { GroupRoles, GROUP_PREFIX} from "/imports/groups/GroupRoles";

import UserHelpers from "/imports/accounts/UserHelpers";

describe( 'pr-groups/jasmine/server/unit/group/list publication', function () {

    let context;

    beforeEach( function () {
        context = {
            userId: '420',
            ready:  jasmine.createSpy( 'ready' ),
            onStop: jasmine.createSpy( 'onStop' ),
            added:  jasmine.createSpy( 'added' )
        };
        spyOn( UserHelpers, 'isMember' );
        spyOn( GroupHelpers, 'getGroupsForUser' );
        spyOn( GroupCollection, 'find' );
    } );

    it( 'should subscribe to a group list if universal member', function () {
        UserHelpers.isMember.and.returnValue( true );
        expect( ()=>publication.call( context ) ).not.toThrow();
        expect( UserHelpers.isMember ).toHaveBeenCalledWith( context.userId );
        expect( GroupHelpers.getGroupsForUser ).not.toHaveBeenCalled();
        expect( GroupCollection.find ).toHaveBeenCalledWith( {}, { fields: jasmine.any( Object ), sort: { updatedAt: -1 } } );
        expect( context.ready ).toHaveBeenCalled(); // Composite
    } );

    it( 'should subscribe to a group list if not logged in', function () {
        delete context.userId;
        expect( ()=>publication.call( context ) ).not.toThrow();
        expect( UserHelpers.isMember ).not.toHaveBeenCalled();
        expect( GroupHelpers.getGroupsForUser ).not.toHaveBeenCalled();
        expect( GroupCollection.find ).toHaveBeenCalledWith( { public: true }, { fields: jasmine.any( Object ), sort: { updatedAt: -1 } } );
        expect( context.ready ).toHaveBeenCalled(); // Composite
    } );

    it( 'should subscribe to a group list if not member', function () {
        UserHelpers.isMember.and.returnValue( false );
        GroupHelpers.getGroupsForUser.and.returnValue( ['group1', 'group2'] );
        expect( ()=>publication.call( context ) ).not.toThrow();
        expect( UserHelpers.isMember ).toHaveBeenCalledWith( context.userId );
        expect( GroupHelpers.getGroupsForUser ).toHaveBeenCalledWith( context.userId );
            expect( GroupCollection.find ).toHaveBeenCalledWith( { $or: [{ public: true }, { _id: { $in: ['group1', 'group2'] } }] }, { fields: jasmine.any( Object ), sort: { updatedAt: -1 } } );
        expect( context.ready ).toHaveBeenCalled(); // Composite
    } );

} );
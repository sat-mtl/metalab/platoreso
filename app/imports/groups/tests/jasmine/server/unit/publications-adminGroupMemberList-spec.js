"use strict";

const publication = Meteor.server.publish_handlers['admin/group/member/list'];

const { UserHelpers, UserCollection } = PR.Accounts;
import { GroupRoles, GROUP_PREFIX } from "/imports/groups/GroupRoles";

describe( 'pr-groups/jasmine/server/unit/member/list publication', function () {

    let context;

    beforeEach( function () {
        context = {
            userId: '420',
            ready:  jasmine.createSpy( 'ready' ).and.returnValue('ready'),
            onStop: jasmine.createSpy( 'onStop' ),
            added:  jasmine.createSpy( 'added' )
        };
        spyOn( UserHelpers, 'isSuper' );
        spyOn( UserCollection, 'find' ).and.returnValue('cursor');
        //spyOn( UserHelpers, 'getUsersInRole' ).and.returnValue('cursor');
    } );

    it( 'should subscribe to a group member list if allowed', function () {
        UserHelpers.isSuper.and.returnValue( true );
        expect( ()=>expect(publication.call( context, 'groupId' )).toEqual('cursor') ).not.toThrow();
        expect( UserHelpers.isSuper ).toHaveBeenCalledWith( context.userId );
        expect( UserCollection.find ).toHaveBeenCalledWith( { ['roles.' + GROUP_PREFIX + '_groupId']: { $in: GroupRoles } }, { fields: {
            _id: 1,
            profile: 1,
            ['roles.' + GROUP_PREFIX + '_groupId']: 1
        } } );
        //expect( UserHelpers.getUsersInRole ).toHaveBeenCalledWith( GroupRoles, GROUP_PREFIX + '_groupId', { fields: { _id: 1 } } );
        expect( context.ready ).not.toHaveBeenCalled();
    } );

    it( 'should not subscribe to a group member list if not allowed', function () {
        UserHelpers.isSuper.and.returnValue( false );
        expect( ()=>expect(publication.call( context, 'groupId' )).toEqual('ready') ).not.toThrow();
        expect( UserHelpers.isSuper ).toHaveBeenCalledWith( context.userId );
        expect( UserCollection.find ).not.toHaveBeenCalled();
        //expect( UserHelpers.getUsersInRole ).not.toHaveBeenCalled();
        expect( context.ready ).toHaveBeenCalled();
    } );

    it( 'should be able to pass a list of roles we want to limit the list to', function () {
        UserHelpers.isSuper.and.returnValue( true );
        expect( ()=>expect(publication.call( context, 'groupId', ['role1'] )).toEqual('cursor') ).not.toThrow();
        expect( UserHelpers.isSuper ).toHaveBeenCalledWith( context.userId );
        expect( UserCollection.find ).toHaveBeenCalledWith( { ['roles.' + GROUP_PREFIX + '_groupId']: { $in: ['role1'] } }, { fields: {
            _id: 1,
            profile: 1,
            ['roles.' + GROUP_PREFIX + '_groupId']: 1
        } } );
        //expect( UserHelpers.getUsersInRole ).toHaveBeenCalledWith( ['role1'], GROUP_PREFIX + '_groupId', { fields: { _id: 1 } } );
        expect( context.ready ).not.toHaveBeenCalled();
    } );

    it( 'should throw on missing id', function () {
        expect( ()=>publication.call( context ) ).toThrowError(/Match error/);
        expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
        expect( UserCollection.find ).not.toHaveBeenCalled();
        //expect( UserHelpers.getUsersInRole ).not.toHaveBeenCalled();
        expect( context.ready ).not.toHaveBeenCalled();
    } );

    it( 'should throw on invalid id', function () {
        expect( ()=>publication.call( context, 123 ) ).toThrowError(/Match error/);
        expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
        expect( UserCollection.find ).not.toHaveBeenCalled();
        //expect( UserHelpers.getUsersInRole ).not.toHaveBeenCalled();
        expect( context.ready ).not.toHaveBeenCalled();
    } );

    it( 'should throw on invalid roles', function () {
        expect( ()=>publication.call( context, 'groupId', 123 ) ).toThrowError(/Match error/);
        expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
        expect( UserCollection.find ).not.toHaveBeenCalled();
        //expect( UserHelpers.getUsersInRole ).not.toHaveBeenCalled();
        expect( context.ready ).not.toHaveBeenCalled();
    } );

} );
"use strict";

import GroupCollection from "/imports/groups/GroupCollection";
import GroupSecurity from "/imports/groups/GroupSecurity";
import { GroupRoles, GROUP_PREFIX} from "/imports/groups/GroupRoles";
import UserCollection from "/imports/accounts/UserCollection";

const groupMemberListPublication = Meteor.server.publish_handlers['group/member/list'];

describe( 'pr-groups/jasmine/server/unit/member/list publication', function () {

    let context;

    beforeEach( function () {
        context = {
            userId: '420',
            ready:  jasmine.createSpy( 'ready' ),
            onStop: jasmine.createSpy( 'onStop' ),
            added:  jasmine.createSpy( 'added' )
        };
        spyOn( GroupSecurity, 'canRead' );
        spyOn( GroupCollection, 'findOne' );
        //spyOn( UserHelpers, 'getUsersInRole' );
        spyOn( UserCollection, 'find' );
    } );

    it( 'should subscribe to a group member list', function () {
        GroupSecurity.canRead.and.returnValue( true );
        GroupCollection.findOne.and.returnValue( { _id: 'groupId', showMembers: true } );
        expect( ()=>groupMemberListPublication.call( context, 'groupId' ) ).not.toThrow();
        expect( GroupSecurity.canRead ).toHaveBeenCalledWith( 'groupId', context.userId );
        expect( GroupCollection.findOne ).toHaveBeenCalledWith( { _id: 'groupId' }, { fields: { showMembers: 1 } } );
        //expect( UserHelpers.getUsersInRole ).toHaveBeenCalledWith( GroupRoles, GROUP_PREFIX + '_groupId', { fields: { _id: 1, profile: 1, ['roles.' + GROUP_PREFIX + '_groupId']: 1 } } );
        expect( UserCollection.find ).toHaveBeenCalledWith( {
            completed: true,
            ['roles.' + GROUP_PREFIX + '_groupId']: { $in: GroupRoles }
        }, { fields: {
            _id: 1,
            profile: 1,
            ['roles.' + GROUP_PREFIX + '_groupId']: 1
        } } );
        expect( context.ready ).not.toHaveBeenCalled();
    } );

    it( 'should not subscribe to a group member list if group prevents it', function () {
        GroupSecurity.canRead.and.returnValue( true );
        GroupCollection.findOne.and.returnValue( { _id: 'groupId', showMembers: false } );
        expect( ()=>groupMemberListPublication.call( context, 'groupId' ) ).not.toThrow();
        expect( GroupSecurity.canRead ).toHaveBeenCalledWith( 'groupId', context.userId );
        expect( GroupCollection.findOne ).toHaveBeenCalledWith( { _id: 'groupId' }, { fields: { showMembers: 1 } } );
        //expect( UserHelpers.getUsersInRole ).not.toHaveBeenCalled();
        expect( UserCollection.find ).not.toHaveBeenCalled();
        expect( context.ready ).toHaveBeenCalled();
    } );

    it( 'should not subscribe to a group member list if user is not allowed', function () {
        GroupSecurity.canRead.and.returnValue( false );
        expect( ()=>groupMemberListPublication.call( context, 'groupId' ) ).not.toThrow();
        expect( GroupSecurity.canRead ).toHaveBeenCalledWith( 'groupId', context.userId );
        expect( GroupCollection.findOne ).not.toHaveBeenCalled();
        //expect( UserHelpers.getUsersInRole ).not.toHaveBeenCalled();
        expect( UserCollection.find ).not.toHaveBeenCalled();
        expect( context.ready ).toHaveBeenCalled();
    } );

    it( 'should not subscribe to a group member list if group is not found', function () {
        GroupSecurity.canRead.and.returnValue( true );
        GroupCollection.findOne.and.returnValue( null );
        expect( ()=>groupMemberListPublication.call( context, 'groupId' ) ).not.toThrow();
        expect( GroupSecurity.canRead ).toHaveBeenCalledWith( 'groupId', context.userId );
        expect( GroupCollection.findOne ).toHaveBeenCalledWith( { _id: 'groupId' }, { fields: { showMembers: 1 } } );
        //expect( UserHelpers.getUsersInRole ).not.toHaveBeenCalled();
        expect( UserCollection.find ).not.toHaveBeenCalled();
        expect( context.ready ).toHaveBeenCalled();
    } );

    it( 'should throw on missing group id', function () {
        expect( ()=>groupMemberListPublication.call( context ) ).toThrowError( /Match\ error/ );
        expect( GroupSecurity.canRead ).not.toHaveBeenCalled();
        expect( GroupCollection.findOne ).not.toHaveBeenCalled();
        //expect( UserHelpers.getUsersInRole ).not.toHaveBeenCalled();
        expect( UserCollection.find ).not.toHaveBeenCalled();
        expect( context.ready ).not.toHaveBeenCalled();
    } );

    it( 'should throw on bad group id', function () {
        expect( ()=>groupMemberListPublication.call( context, 123 ) ).toThrowError( /Match\ error/ );
        expect( GroupSecurity.canRead ).not.toHaveBeenCalled();
        expect( GroupCollection.findOne ).not.toHaveBeenCalled();
        //expect( UserHelpers.getUsersInRole ).not.toHaveBeenCalled();
        expect( UserCollection.find ).not.toHaveBeenCalled();
        expect( context.ready ).not.toHaveBeenCalled();
    } );

} );
"use strict";

const publication = Meteor.server.publish_handlers['group/moderation/report/list'];

import GroupCollection from "/imports/groups/GroupCollection";
import UserHelpers from "/imports/accounts/UserHelpers";
 import { Counts } from "meteor/tmeasday:publish-counts";

describe( 'pr-groups/jasmine/server/unit/moderation/report/list publication', function () {

    let context;
    let query;
    let options;

    beforeEach( function () {
        context = {
            userId: '420',
            ready:  jasmine.createSpy( 'ready' ),
            onStop: jasmine.createSpy( 'onStop' ),
            added:  jasmine.createSpy( 'added' )
        };
        query   = {};
        options = {};
        spyOn( UserHelpers, 'isModerator' );
        spyOn( Counts, 'publish' );
        spyOn( GroupCollection, 'findReported' );
    } );

    it( 'should publish when allowed', function () {
        UserHelpers.isModerator.and.returnValue( true );
        GroupCollection.findReported.and.returnValues( 'cursor', null );
        expect( () => publication.call( context, _.clone( query ), _.clone( options ) ) ).not.toThrow();
        expect( UserHelpers.isModerator ).toHaveBeenCalledWith( context.userId );
        expect( Counts.publish ).toHaveBeenCalledWith( context, 'group/moderation/report/count/', 'cursor', { noReady: true } );
        expect( GroupCollection.findReported.calls.count() ).toEqual( 2 );
        expect( GroupCollection.findReported.calls.argsFor( 0 ) ).toEqual( [query, { fields: { _id: 1 } }] );
        expect( GroupCollection.findReported.calls.argsFor( 1 ) ).toEqual( [query, _.extend( options, { fields: jasmine.any( Object ) } )] );
        expect( context.ready ).toHaveBeenCalled(); // Composite
    } );

    it( 'should not publish when not allowed', function () {
        UserHelpers.isModerator.and.returnValue( false );
        expect( () => publication.call( context, _.clone( query ), _.clone( options ) ) ).not.toThrow();
        expect( UserHelpers.isModerator ).toHaveBeenCalledWith( context.userId );
        expect( Counts.publish ).not.toHaveBeenCalled();
        expect( GroupCollection.findReported.calls.count() ).toEqual( 0 );
        expect( context.ready ).toHaveBeenCalled(); // Composite
    } );

    it( 'should support null queries', function () {
        UserHelpers.isModerator.and.returnValue( true );
        GroupCollection.findReported.and.returnValues( 'cursor', null );
        expect( () => publication.call( context, null, _.clone( options ) ) ).not.toThrow();
        expect( UserHelpers.isModerator ).toHaveBeenCalledWith( context.userId );
        expect( Counts.publish ).toHaveBeenCalledWith( context, 'group/moderation/report/count/', 'cursor', { noReady: true } );
        expect( GroupCollection.findReported.calls.count() ).toEqual( 2 );
        expect( GroupCollection.findReported.calls.argsFor( 0 ) ).toEqual( [query, { fields: { _id: 1 } }] );
        expect( GroupCollection.findReported.calls.argsFor( 1 ) ).toEqual( [{}, _.extend( options, { fields: jasmine.any( Object ) } )] );
        expect( context.ready ).toHaveBeenCalled(); // Composite
    } );

    it( 'should support null options', function () {
        UserHelpers.isModerator.and.returnValue( true );
        GroupCollection.findReported.and.returnValues( 'cursor', null );
        expect( () => publication.call( context, _.clone( query ), null ) ).not.toThrow();
        expect( UserHelpers.isModerator ).toHaveBeenCalledWith( context.userId );
        expect( Counts.publish ).toHaveBeenCalledWith( context, 'group/moderation/report/count/', 'cursor', { noReady: true } );
        expect( GroupCollection.findReported.calls.count() ).toEqual( 2 );
        expect( GroupCollection.findReported.calls.argsFor( 0 ) ).toEqual( [query, { fields: { _id: 1 } }] );
        expect( GroupCollection.findReported.calls.argsFor( 1 ) ).toEqual( [query, _.extend( {}, { fields: jasmine.any( Object ) } )] );
        expect( context.ready ).toHaveBeenCalled(); // Composite
    } );

    it( 'should not support queries (for now)', function () {
        query.enabled = true;
        expect( () => publication.call( context, query, _.clone( options ) ) ).toThrowError(/Match error/);
        expect( UserHelpers.isModerator ).not.toHaveBeenCalled( );
        expect( Counts.publish ).not.toHaveBeenCalled();
        expect( GroupCollection.findReported.calls.count() ).toEqual( 0 );
        expect( context.ready ).not.toHaveBeenCalled();
    } );

    it( 'should support sort option', function () {
        options.sort = { 'profile.firstName': 1 };
        UserHelpers.isModerator.and.returnValue( true );
        GroupCollection.findReported.and.returnValues( 'cursor', null );
        expect( () => publication.call( context, _.clone( query ), _.clone( options ) ) ).not.toThrow();
        expect( UserHelpers.isModerator ).toHaveBeenCalledWith( context.userId );
        expect( Counts.publish ).toHaveBeenCalledWith( context, 'group/moderation/report/count/', 'cursor', { noReady: true } );
        expect( GroupCollection.findReported.calls.count() ).toEqual( 2 );
        expect( GroupCollection.findReported.calls.argsFor( 0 ) ).toEqual( [query, { fields: { _id: 1 } }] );
        expect( GroupCollection.findReported.calls.argsFor( 1 ) ).toEqual( [query, _.extend( options, { fields: jasmine.any( Object ) } )] );
        expect( context.ready ).toHaveBeenCalled(); // Composite
    } );
    
    it( 'should not support invalid sort option', function () {
        options.sort = 'name';
        expect( () => publication.call( context, query, _.clone( options ) ) ).toThrowError(/Match error/);
        expect( UserHelpers.isModerator ).not.toHaveBeenCalled( );
        expect( Counts.publish ).not.toHaveBeenCalled();
        expect( GroupCollection.findReported.calls.count() ).toEqual( 0 );
        expect( context.ready ).not.toHaveBeenCalled();
    } );

    it( 'should support limit option', function () {
        options.limit = 10;
        UserHelpers.isModerator.and.returnValue( true );
        GroupCollection.findReported.and.returnValues( 'cursor', null );
        expect( () => publication.call( context, _.clone( query ), _.clone( options ) ) ).not.toThrow();
        expect( UserHelpers.isModerator ).toHaveBeenCalledWith( context.userId );
        expect( Counts.publish ).toHaveBeenCalledWith( context, 'group/moderation/report/count/', 'cursor', { noReady: true } );
        expect( GroupCollection.findReported.calls.count() ).toEqual( 2 );
        expect( GroupCollection.findReported.calls.argsFor( 0 ) ).toEqual( [query, { fields: { _id: 1 } }] );
        expect( GroupCollection.findReported.calls.argsFor( 1 ) ).toEqual( [query, _.extend( options, { fields: jasmine.any( Object ) } )] );
        expect( context.ready ).toHaveBeenCalled(); // Composite
    } );

    it( 'should not support invalid limit option', function () {
        options.limit = 'ten';
        expect( () => publication.call( context, query, _.clone( options ) ) ).toThrowError(/Match error/);
        expect( UserHelpers.isModerator ).not.toHaveBeenCalled( );
        expect( Counts.publish ).not.toHaveBeenCalled();
        expect( GroupCollection.findReported.calls.count() ).toEqual( 0 );
        expect( context.ready ).not.toHaveBeenCalled();
    } );

    it( 'should support limit option', function () {
        options.limit = 10;
        UserHelpers.isModerator.and.returnValue( true );
        GroupCollection.findReported.and.returnValues( 'cursor', null );
        expect( () => publication.call( context, _.clone( query ), _.clone( options ) ) ).not.toThrow();
        expect( UserHelpers.isModerator ).toHaveBeenCalledWith( context.userId );
        expect( Counts.publish ).toHaveBeenCalledWith( context, 'group/moderation/report/count/', 'cursor', { noReady: true } );
        expect( GroupCollection.findReported.calls.count() ).toEqual( 2 );
        expect( GroupCollection.findReported.calls.argsFor( 0 ) ).toEqual( [query, { fields: { _id: 1 } }] );
        expect( GroupCollection.findReported.calls.argsFor( 1 ) ).toEqual( [query, _.extend( options, { fields: jasmine.any( Object ) } )] );
        expect( context.ready ).toHaveBeenCalled(); // Composite
    } );

    it( 'should not support invalid limit option', function () {
        options.limit = 'ten';
        expect( () => publication.call( context, query, _.clone( options ) ) ).toThrowError(/Match error/);
        expect( UserHelpers.isModerator ).not.toHaveBeenCalled( );
        expect( Counts.publish ).not.toHaveBeenCalled();
        expect( GroupCollection.findReported.calls.count() ).toEqual( 0 );
        expect( context.ready ).not.toHaveBeenCalled();
    } );

    it( 'should not support extra options', function () {
        options.extra = 'option';
        expect( () => publication.call( context, query, _.clone( options ) ) ).toThrowError(/Match error/);
        expect( UserHelpers.isModerator ).not.toHaveBeenCalled( );
        expect( Counts.publish ).not.toHaveBeenCalled();
        expect( GroupCollection.findReported.calls.count() ).toEqual( 0 );
        expect( context.ready ).not.toHaveBeenCalled();
    } );

} );
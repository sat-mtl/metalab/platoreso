"use strict";

import Logger from "meteor/metalab:logger/Logger";
import AccountsMeldHelper from "/imports/accounts/server/AccountsMeldHelper";
import GroupCollection from "../GroupCollection";

const log = Logger( "group-meld" );

/**
 * Group Melding
 */
AccountsMeldHelper.onMeld( ( src_user_id, dst_user_id ) => {
    log.debug( `Melding groups for user ${src_user_id} into ${dst_user_id}` );

    GroupCollection.update( { 'createdBy': src_user_id }, { $set: { 'createdBy': dst_user_id } }, { multi: true } );
    GroupCollection.update( { 'updatedBy': src_user_id }, { $set: { 'updatedBy': dst_user_id } }, { multi: true } );
} );
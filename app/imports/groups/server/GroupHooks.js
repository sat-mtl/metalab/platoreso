"use strict";

import Logger from "meteor/metalab:logger/Logger";
import FeedCollection from "/imports/feed/FeedCollection";
import NotificationCollection from "/imports/notifications/NotificationCollection";
import ConversationCollection from "/imports/messages/ConversationCollection";
import ConversationType from "/imports/messages/ConversationType";
import GroupCollection from "../GroupCollection";
import GroupImagesCollection from "../GroupImagesCollection";

const log = Logger( "group-hooks" );

/**
 * Group Inserted
 */
GroupCollection.after.insert( function ( userId, group ) {
    // Defer, so that methods can return without waiting
    Meteor.defer( () => {
        log.debug( `Group "${group.name}" (${group._id}) added` );

        // Create a conversation for the group
        ConversationCollection.insert( {
            type:   ConversationType.group,
            entity: group._id,
            slug:   group.name,
            name:   group.slug,
            topic:  group.description
        } );
    } );
} );

/**
 * Group Updated
 */
GroupCollection.after.update( function ( userId, group, fieldNames, modifier, options ) {
    // Defer, so that methods can return without waiting
    Meteor.defer( () => {
        log.debug( `Group "${group.name}" (${group._id}) updated` );

        if ( fieldNames.indexOf( 'name' ) != -1 || fieldNames.indexOf( 'description' ) != -1 ) {
            // Rename the group conversation
            ConversationCollection.update(
                {
                    type:   ConversationType.group,
                    entity: group._id
                },
                {
                    $set: {
                        slug:  group.slug,
                        name:  group.name,
                        topic: group.description
                    }
                }
            );
        }
    } );
}, { fetchPrevious: false } );

/**
 * Group Removed
 */
GroupCollection.after.remove( function ( userId, group ) {
    // Defer, so that methods can return without waiting
    Meteor.defer( () => {
        log.debug( `Group "${group.name}" (${group._id}) removed, cleaning up relations` );

        // Remove associated image(s)
        GroupImagesCollection.unlinkOwner( group._id );

        // Remove notifications
        NotificationCollection.unnotifyAllForObject( group._id, GroupCollection.entityName );

        // Remove Stories
        FeedCollection.removeAllForObject( group._id, GroupCollection.entityName );

        // Remove Conversations
        ConversationCollection.remove( { type: ConversationType.group, entity: group._id } );

        //TODO: Remove permissions & roles

    } );
} );
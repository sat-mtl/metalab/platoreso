"use strict";

import GroupCollection from "../../GroupCollection";
import GroupImagesCollection from "../../GroupImagesCollection";
import GroupSecurity from "../../GroupSecurity";
import CollectionHelpers from "/imports/collections/CollectionHelpers";

/**
 * Group Page (Public)
 */
Meteor.publishComposite( "group", function ( groupId ) {
    check( groupId, String );

    // We accept both _id and slug for subscriptions so we need to get the real _id here
    // We prefetch what is needed for GroupSecurity.canRead() to prevent multiple calls to the DB
    // We also disable transforms as Meteor's check() doesn't like constructor objects
    const group = GroupCollection.findOne( { moderated: false, $or: [{ _id: groupId }, { slug: groupId }] }, { fields: { _id: 1, public: 1 }, transform: null } );
    if ( !group ) {
        return this.ready();
    }

    // Check with the project instead of the _id to prevent another fetch from the db
    if ( !GroupSecurity.canRead( this.userId, group ) ) {
        return this.ready();
    }

    const fields = CollectionHelpers.getDefaultFields(
        GroupCollection,
        this.userId,
        {
            _id:         1,
            slug:        1,
            name:        1,
            description: 1,
            public:      1,
            showMembers: 1,
            content:     1
        }
    );

    if ( GroupSecurity.canManageGroups( this.userId, group._id ) ) {
        fields.invitationCode = 1;
    }

    return {
        find() {
            return GroupCollection.find( { _id: group._id, moderated: false }, {
                fields: fields
            } );
        },
        children: [
            { find: group => group.findImage() }
        ]
    }
} );
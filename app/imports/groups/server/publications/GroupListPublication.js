"use strict";

import CollectionHelpers from "/imports/collections/CollectionHelpers";
import UserHelpers from "/imports/accounts/UserHelpers";
import GroupCollection from "../../GroupCollection";
import GroupHelpers from "../../GroupHelpers";

/**
 * Group List (Public)
 */
Meteor.publishComposite( "group/list", function () {
    return {
        find() {
            let query = { moderated: false };

            if ( !this.userId ) {
                query.public = true;
            } else if ( !UserHelpers.isMember( this.userId ) ) {
                // If not a universal member, check for public or allowed groups
                query.$or = [
                    { public: true },
                    { _id: { $in: GroupHelpers.getGroupsForUser( this.userId ) } }
                ];
            }

            return GroupCollection.find( query, {
                fields: CollectionHelpers.getDefaultFields(
                    GroupCollection,
                    this.userId,
                    {
                        _id: 1,
                        slug: 1,
                        name: 1,
                        description: 1,
                        public: 1
                    }
                ),
                sort: { updatedAt: -1 }
            } );
        },
        children: [
            {
                find: group => group.findImage()
            }
        ]
    };
} );
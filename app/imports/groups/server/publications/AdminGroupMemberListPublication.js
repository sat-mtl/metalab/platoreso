"use strict";

import { GroupRoles, GROUP_PREFIX } from "../../GroupRoles";
import UserCollection from "/imports/accounts/UserCollection";
import UserHelpers from "/imports/accounts/UserHelpers";

/**
 * Group Members List (Administration)
 */
Meteor.publish( "admin/group/member/list", function ( groupId, role = GroupRoles ) {
    check( groupId, String );
    check( role, Match.OneOf( String, [String] ) );

    if ( !UserHelpers.isSuper( this.userId ) ) {
        return this.ready();
    }

    if ( !_.isArray(role) ) {
        role = [role];
    }

    // Doing the query manually here as the roles package does not let us ignore the global roles
    // We need to return roles (for that group only) so that client-side UserHelpers.getUsersInRole works too
    // We don't return all roles as this could be a security risk
    return UserCollection.find( {
        ['roles.' + GROUP_PREFIX + '_' + groupId]: { $in: role }
    }, { fields: {
        _id: 1,
        profile: 1,
        ['roles.' + GROUP_PREFIX + '_' + groupId]: 1
    } } );
    //return UserHelpers.getUsersInRole(role, GROUP_PREFIX + '_' + groupId, { fields: { _id: 1 } } );
} );
"use strict";

import GroupCollection from "../../GroupCollection";
import GroupImagesCollection from "../../GroupImagesCollection";
import UserHelpers from "/imports/accounts/UserHelpers";

/**
 * Group Edit (Administration)
 */
Meteor.publish( "admin/group/edit", function ( id ) {
    check( id, String );

    if ( !UserHelpers.isSuper( this.userId ) ) {
        return this.ready();
    }

    return [
        GroupCollection.find({_id: id}),
        GroupImagesCollection.find({owners:id})
    ];
} );
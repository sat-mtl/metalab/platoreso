"use strict";

import GroupCollection from "../../GroupCollection";
import GroupSecurity from "../../GroupSecurity";
import { GroupRoles, GROUP_PREFIX } from "../../GroupRoles";
import UserCollection from "/imports/accounts/UserCollection";

/**
 * Group member list
 * Will only send the publicly available data
 */
Meteor.publish( "group/member/list", function ( groupId ) {
    check( groupId, String );

    if ( !GroupSecurity.canRead( this.userId, groupId ) ) {
        return this.ready();
    }

    const group = GroupCollection.findOne( {_id: groupId, moderated: false, showMembers: true}, {fields: {_id: 1}} );
    if ( !group ) {
        return this.ready();
    }

    // Doing the query manually here as the roles package does not let us ignore the global roles
    // We need to return roles (for that group only) so that client-side UserHelpers.getUsersInRole works too
    // We don't return all roles as this could be a security risk
    return UserCollection.find( {
        completed:                                 true,
        ['roles.' + GROUP_PREFIX + '_' + groupId]: {$in: GroupRoles}
    }, {
        fields: {
            _id:                                       1,
            profile:                                   1,
            ['roles.' + GROUP_PREFIX + '_' + groupId]: 1
        }
    } );

    // We need to return roles (for that group only) so that client-side UserHelpers.getUsersInRole works too
    // We don't return all roles as this could be a security risk
    /*return UserHelpers.getUsersInRole(GroupRoles, GROUP_PREFIX + '_' + groupId, { fields: {
     _id: 1,
     profile: 1,
     ['roles.' + GROUP_PREFIX + '_' + groupId]: 1
     } } );*/
} );
"use strict";

import slugify from "underscore.string/slugify";
import { Counts } from "meteor/tmeasday:publish-counts";
import GroupCollection from "../../GroupCollection";
import GroupImagesCollection from "../../GroupImagesCollection";
import UserHelpers from "/imports/accounts/UserHelpers";

/**
 * Group List (Administration)
 *
 * @params {Object} [options] Options for the find operation (ex: sort)
 */
Meteor.publishComposite( "admin/group/list", function ( query = null, options = null ) {

    // Check query for only the allowed fields
    check( query, Match.OneOf(
        null, {
            name: Match.Optional( Match.ObjectIncluding( {$regex: String} ) )
        }
    ) );

    // Check options for only the allowed fields
    check( options, Match.OneOf( null, {
        sort:  Match.Optional( Object ),
        limit: Match.Optional( Number ),
        skip:  Match.Optional( Number )
    } ) );

    if ( !UserHelpers.isSuper( this.userId ) ) {
        return this.ready();
    }

    query   = query || {};
    options = _.extend( options || {}, {
        fields: {
            _id:       1,
            slug:      1,
            name:      1,
            public:    1,
            createdAt: 1,
            createdBy: 1,
            updatedAt: 1,
            updatedBy: 1
        }
    } );

    return {
        find() {
            // Slugify the json query as a unique identifier for the count
            Counts.publish( this,
                'group/count/' + slugify( JSON.stringify( query ) ),
                GroupCollection.find( query, {fields: {_id: 1}} ),
                {noReady: true}
            );
            return GroupCollection.find( query, options )
        },
        children: [
            {
                find( group ) {
                    return GroupImagesCollection.find( {owners: group._id} );
                }
            }
        ]
    }
} );
"use strict";

// GENERAL
import "./GroupMeld";
import "./GroupHooks";
import "./GroupCollectionSecurity";

// PUBLICATIONS
import "./publications/AdminGroupEditPublication";
import "./publications/AdminGroupListPublication";
import "./publications/AdminGroupListPublication";
import "./publications/GroupListPublication";
import "./publications/GroupMemberListPublication";
import "./publications/GroupPublication";
import "../moderation/GroupModerationReportListPublication";
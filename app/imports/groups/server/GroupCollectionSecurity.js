"use strict";

import {RoleGroups} from "/imports/accounts/Roles";
import UserHelpers from "/imports/accounts/UserHelpers";
import ConversationType from "/imports/messages/ConversationType";
import ConversationCollection from "/imports/messages/ConversationCollection";
import ConversationHelpers from "/imports/messages/ConversationHelpers";
import MessageCollection from "/imports/messages/MessageCollection";
import GroupImagesCollection from "../GroupImagesCollection";
import GroupCollection from "../GroupCollection";
import GroupSecurity from "../GroupSecurity";
import { GROUP_PREFIX } from "../GroupRoles";

/**
 * Deny if user cannot read the group
 */
Security.defineMethod( "ifCanReadOwnerGroup", {
    fetch:     [],
    transform: null,
    deny:      function ( type, arg, userId, doc, fields, modifier ) {
        return !_.some( doc.owners, owner => GroupSecurity.canRead( userId, owner ) );
    }
} );

/**
 * Deny if user cannot edit the image owner's group
 */
Security.defineMethod( "ifCanEditOwnerGroup", {
    fetch:     [],
    transform: null,
    deny:      function ( type, arg, userId, doc, fields, modifier ) {
        return !_.some( doc.owners, owner => GroupSecurity.canEdit( userId, owner ) );
    }
} );

/**
 * Security Rules
 */

// Allow anyone with access to group to download images
Security.permit( ['download'] )
    .collections( [GroupImagesCollection] )
    .ifCanReadOwnerGroup()
    .allowInClientCode();

Security.permit( ['insert'] )
    .collections( [GroupImagesCollection] )
    .ifLoggedIn()
    .ifHasRole( RoleGroups.experts )
    .allowInClientCode();

Security.permit( ['insert'] )
    .collections( [GroupImagesCollection] )
    .ifLoggedIn()
    .ifCanEditOwnerGroup()
    .allowInClientCode();


// MODERATION

Security.defineMethod( "ifCanModerateGroup", {
    fetch:     [],
    transform: null,
    allow:     function ( type, arg, userId, doc, fields, modifier ) {
        return UserHelpers.isModerator( userId );
    }
} );

GroupCollection
    .permit( ['update'] )
    .onlyProps( ['moderated', 'reportCount', 'reportedBy'] )
    .ifLoggedIn()
    .ifCanModerateGroup();

Security.defineMethod( "ifCanModerateGroupMessage", {
    fetch:     [],
    transform: null,
    allow:     function ( type, arg, userId, message, fields, modifier ) {
        return ConversationHelpers.canModerate(userId, message.conversation );
        //const conversation = ConversationCollection.findOne( {
        //    _id:  message.conversation,
        //    type: ConversationType.group
        //}, { fields: { entity: 1 } } );
        //return conversation && UserHelpers.userIsInRole( userId, RoleGroups.moderators, GROUP_PREFIX + '_' + conversation.entity );
    }
} );

MessageCollection
    .permit( ['update'] )
    .onlyProps( ['moderated', 'reportCount', 'reportedBy'] )
    .ifLoggedIn()
    .ifCanModerateGroupMessage();

MessageCollection
    .permit( ['remove'] )
    .ifLoggedIn()
    .ifCanModerateGroupMessage();
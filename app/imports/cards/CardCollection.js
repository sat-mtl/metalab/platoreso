"use strict";

import Logger from "meteor/metalab:logger/Logger";

import CollectionHelpers from "/imports/collections/CollectionHelpers";
import ModerationHelpers from "/imports/moderation/ModerationHelpers";

import Card from "./model/Card";
import CardSchema from "./model/CardSchema";
import TagCollectionHelpers from "../tags/TagCollectionHelpers";
import CommentCollection from "../comments/CommentCollection";

const log = Logger( "cards-collection" );

class CardCollection extends Mongo.Collection {
    constructor( name, options = {} ) {
        // Change transform before calling super, this is the only way to set it
        // Also, this won't suffer from module loading as it is a callback
        options.transform = card => new Card( card );

        super( name, options );

        this.entityName = 'card';

        /**
         * Schema
         */

        this.attachSchema( CardSchema );

        /**
         * Indexes
         */

        if ( Meteor.isServer ) {
            // Graph, no phase + Trending cards observer
            this._ensureIndex( { project: 1, current: 1, moderated: 1 }, { name: 'project_current_moderated' } );
            // Board phase
            this._ensureIndex( { project: 1, phase: 1, current: 1, moderated: 1 }, { name: 'project_phase_current_moderated' } );
            // Snapshots
            this._ensureIndex( { id: 1, current: 1, moderated: 1, version: 1 }, { name: 'id_current_moderated_version' } );
            // Slug, partial unique for current cards
            this._ensureIndex(
                {
                    slug: 1,
                },
                {
                    name:                    'unique_slug_for_current',
                    unique:                  true,
                    partialFilterExpression: { current: true }
                }
            );
        }

        /**
         * Security
         */

        this.deny( {
            insert: () => true,
            update: () => true,
            remove: () => true
        } );

        /**
         * Behaviors
         */

        this.attachBehaviour( 'timestampable' );

        // The rest goes into a Meteor.startup because since we create the collection instance
        // right away when exporting from this module, we end up executing this constructor
        // before the imported modules are available (when there are circular dependencies).
        // This way we are sure the app is loaded when we execute the following code.
        Meteor.startup( () => {

            /**
             * Registration
             * TODO: Get rid of that system
             */

            CollectionHelpers.registerCollection( this.entityName, this, Card );

            /**
             * Helpers
             */

            TagCollectionHelpers.setupCollection( this );
            ModerationHelpers.setup( this, Card, entityId => ({ id: entityId, current: true }) );
            CommentCollection.setupCollection( this, entityId => ({ id: entityId, current: true }) );

        } );
    }

    /**
     * Make a history snapshot of the passed card
     *
     * If card is a string it will find the card and if it is an object it will assume it is the complete up-to-date
     * card to be saved to history. Be careful not to send partial card objects to this method.
     *
     * If a conflict occurs during the insert, it will increment the version
     * and continue trying to snapshot. It will then return that version number
     * so that the hook can update to the "real" version number.
     *
     * @param {String} userId
     * @param {String|Object} card
     * @returns {Number} version
     */
    snapshot( userId, card ) {
        check( userId, String );
        check( card, Match.OneOf(
            String,
            Match.ObjectIncluding( {
                id:      String,
                version: Number
            } )
        ) );

        let snapshotCard = null;

        if ( typeof card == "string" ) {
            log.debug( "snapshot - Passed card was a string, finding the actual card" );
            snapshotCard = this.findOne( { id: card, current: true }, { transform: null } );
        } else {
            log.debug( "snapshot - Passed card was an object, praying for it to be the complete card :P" );
            snapshotCard = _.clone( card );
        }

        if ( !snapshotCard ) {
            log.error( `snapshot - Trying to snapshot a non-existent card.`, card );
            throw new Meteor.Error( "snapshot card not found" );
        }

        delete snapshotCard._id;
        snapshotCard.current     = false;
        snapshotCard.versionedAt = new Date();
        snapshotCard.versionedBy = userId;

        // Insert the snapshot
        let retry      = false;
        let retryCount = 0;
        do {
            retry = false;
            try {
                // We want to copy the card, don't bother with any hooks and/or validation
                this.direct.insert( snapshotCard, { bypassCollection2: true } );
            } catch ( e ) {
                // Only retry on duplicate key, log the rest
                if ( e.code && e.code == 11000 && retryCount < 10 ) {
                    log.warn( e );
                    snapshotCard.version++;
                    retry = true;
                    retryCount++;
                } else if ( retryCount >= 10 ) {
                    log.error( `snapshot - Error while saving snapshot version ${snapshotCard.version} for card ${snapshotCard.id}, maximum number of retries attained.` );
                    throw new Meteor.Error( "snapshot error" );
                } else {
                    log.error( `snapshot - Error while saving snapshot version ${snapshotCard.version} for card ${snapshotCard.id}` );
                    throw e;
                }
            }
        } while ( retry );

        // Return the new version
        return snapshotCard.version + 1;
    }

    /**
     * Make a history snapshot and directly update the card to the new version
     * Use this method when versioning a card without modifying it (ie: when changing the image)
     * Otherwise use snapshot() and change the modifier in a before.* hook.
     *
     * @param {String} userId
     * @param {String|Object} card
     * @returns {Number} Version
     */
    snapshotAndUpdate( userId, card ) {
        check( userId, String );
        check( card, Match.OneOf(
            String,
            Match.ObjectIncluding( {
                id:      String,
                version: Number
            } )
        ) );

        let id = null;

        if ( typeof card == "string" ) {
            id = card;
        } else if ( card.id ) {
            id = card.id;
        }

        const version = this.snapshot( userId, card );

        this.update( { id: id, current: true }, { $set: { version: version } } );

        return version;
    }
}

export default new CardCollection( "cards" );
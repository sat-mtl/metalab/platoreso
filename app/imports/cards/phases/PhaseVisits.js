"use strict";

export default class PhaseVisits {

    /**
     * Helper method to get a phase visit object
     *
     * @param {String} phase id
     * @param {String} type
     * @param {Number} version
     * @param {String} [from] Originating phase id
     * @returns {{phase: String, type: String, enteredVersion: Number, enteredAt: Date, [enteredFrom]: String}}
     */
    static createVisit( phase, type, version = 0, from = null ) {
        let visit = {
            phase:          phase,
            type:           type,
            enteredVersion: version,
            enteredAt:      new Date()
        };

        if ( from ) {
            visit.enteredFrom = from
        }

        return visit;
    }
}

PhaseVisits.visitTypes = {
    created: 'created',
    moved:   'moved',
    copied:  'copied'
};
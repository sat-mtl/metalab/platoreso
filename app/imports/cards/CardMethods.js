"use strict";

import flatten from "flat";
import Logger from "meteor/metalab:logger/Logger";
import { ValidatedMethod } from "meteor/mdg:validated-method";
import expandObjectKeys from "../utils/expandObjectKeys";
import Audit from "../audit/Audit";
import { CardAuditMessages } from "./CardAudit";
import ProjectCollection from "../projects/ProjectCollection";
import ProjectSecurity from "../projects/ProjectSecurity";
import PhaseTypes from "../projects/phases/PhaseTypes";
import CardCollection from "./CardCollection";
import CardSchema, { CardUpdateSchema } from "./model/CardSchema";
import CardLinksCollection from "./links/CardLinksCollection";
import { CardLinkTypeValues } from "./links/CardLinkTypes";
import CardSecurity from "./CardSecurity";
import CardFilesHelpers from "./files/CardFilesHelpers";
import CardImagesCollection from "./images/CardImagesCollection";
import CardAttachmentsCollection from "./attachments/CardAttachmentsCollection";
import PhaseVisits from "./phases/PhaseVisits";
import Settings from "../settings/Settings";
import CardAnalytics from "./analytics/CardAnalytics";
import Gamification from "../gamification/Gamification";

const log = Logger( "card-methods" );

/**
 * Fields that trigger a history snapshot to be made when they are changed
 * @deprecated Since we do it in the update method any change is good for snapshot
 * @type {string[]}
 */
const versioningFields = ["name", "content", "tags", "phase", "phaseData"];

/**
 * Makes a snapshot of the card and modifies the passed modifier in place
 *
 * @param {String} userId
 * @param {Object} card
 * @param {Object} modifier
 * @returns {Number} Current card version after snapshot
 */
function snapshotAndSetModifier( userId, card, modifier ) {
    // Pass the _id so that a fresh copy is fetched for the snapshot
    const currentVersion = CardCollection.snapshot( userId, card.id );

    // Update card's version while doing the rest
    modifier.$set         = modifier.$set || {};
    modifier.$set.version = currentVersion;

    return currentVersion;
}

const CardMethods = {

          /**
           * Create a card
           *
           * @param  {object} info User update information
           * @returns {string} Group Id
           */
          create: new ValidatedMethod( {
              name:     'pr/card/create',
              validate: CardSchema.pick( [
                  'project',
                  'phase',
                  'name',
                  'content',
                  'tags',
                  'tags.$',
                  'phaseData'
              ] ).validator( { clean: true } ),
              run( card ) {
                  // SECURITY

                  card.author = this.userId; // Assign the card author

                  if ( !CardSecurity.canCreate( this.userId, card ) ) {
                      log.warn( `create - ${this.userId} is not authorized to create card`, card );
                      throw new Meteor.Error( 'unauthorized' );
                  }

                  // UPDATE

                  //TODO: Should expanding here be really necessary? It feels dirty to me but we had to do it since
                  //      data is a blackbox (I don't know why it behaves that way)
                  const formObj = expandObjectKeys( card );

                  // Add initial phase visit
                  formObj.phaseVisits = [PhaseVisits.createVisit( formObj.phase, PhaseVisits.visitTypes.created )];

                  // Generate an Id, stolen from Meteor mongo package
                  // Using Random.id() created two different cards on server and client
                  // So the ui flickered while it was syncing
                  const cardId = CardCollection._makeNewID();
                  formObj.id   = cardId;

                  // Add initial "heat"
                  CardAnalytics.initialHeatData( Settings.shared.analytics.card.heat.creation, formObj );

                  // Insert
                  log.verbose( "create - Creating card", formObj );
                  CardCollection.insert( formObj );

                  // User points
                  Gamification.awardPoints( this.userId, Settings.shared.gamification.points.card.creation, card.project );

                  // AUDIT

                  Audit.next( {
                      domain:  CardCollection.entityName,
                      message: CardAuditMessages.added,
                      userId:  this.userId,
                      cardId:  cardId
                  } );

                  return cardId;
              }
          } ),

          /**
           * Update a card
           *
           * @param {object} info card update information
           */
          update: new ValidatedMethod( {
              name:     'pr/card/update',
              validate: new SimpleSchema( {
                  id:             {
                      type: String
                  },
                  changes:        {
                      type:     CardUpdateSchema,
                      optional: true
                  },
                  changedPicture: {
                      type:     Boolean,
                      optional: true
                  },
                  removedPicture: {
                      type:     Boolean,
                      optional: true
                  },
                  addedFiles:     {
                      type:     Boolean,
                      optional: true
                  },
                  removedFiles:   {
                      type:     [String],
                      optional: true
                  }
              } ).validator( { clean: true, removeEmptyStrings: false, getAutoValues: false } ),
              run( { id, changes, changedPicture, removedPicture, addedFiles, removedFiles } ) {

                  // SECURITY

                  if ( !CardSecurity.canUpdate( this.userId, id ) ) {
                      log.warn( `update - ${this.userId} is not authorized to update card`, changes );
                      throw new Meteor.Error( 'unauthorized' );
                  }

                  const card = CardCollection.findOne( { id, current: true }, {
                      fields:    {
                          id:              1,
                          version:         1,
                          project:         1,
                          phase:           1,
                          lastTemperature: 1,
                          heatedAt:        1
                      },
                      transform: null
                  } );
                  if ( !card ) {
                      throw new Meteor.Error( 'card not found' );
                  }

                  // PHASE DATA

                  const project = ProjectCollection.findOne(
                      { _id: card.project },
                      { fields: { phases: 1 } }
                  );
                  if ( !project ) {
                      throw new Meteor.Error( 'project not found' );
                  }

                  // Retrieve the phase
                  const phase = _.findWhere( project.phases, { _id: card.phase } );
                  if ( !phase ) {
                      throw new Meteor.Error( 'phase not found' );
                  }

                  // PHASE TYPE UPDATE

                  if ( phase.type ) {
                      const phaseTypeService = PhaseTypes.get( phase.type );
                      if ( !phaseTypeService ) {
                          throw new Meteor.Error( 'invalid phase type' );
                      }

                      // Update the phase type specific changes
                      if ( phaseTypeService.updateComment ) {
                          phaseTypeService.updateComment.call( this, card.id, card.phase, changes );
                      }
                  }

                  // UPDATE

                  let currentVersion = null;

                  // Updating the card
                  if ( !_.isEmpty( changes ) ) {
                      log.debug( "update - Updating card", changes );

                      let modifier = { $set: flatten( changes, { safe: true } ) };

                      // Only do a snapshot when certain fields change
                      if ( _.intersection( versioningFields, _.keys( changes ) ).length > 0 ) {
                          log.verbose( "update - A field that requires versioning was changed, creating a snapshot" );
                          currentVersion = snapshotAndSetModifier( this.userId, card, modifier );
                          //TODO: In an ideal world the current version should be used to make the update so that we don't overwrite a concurrent update
                      }

                      // Apply "heat"
                      CardAnalytics.heatModifier( card, Settings.shared.analytics.card.heat.update, modifier );

                      // Do the actual card update
                      CardCollection.update( { id: card.id, current: true }, modifier );
                  }

                  // REMOVED ATTACHMENTS
                  if ( removedFiles && removedFiles.length ) {
                      log.debug( `update - Committing last version for deleted attachments`, removedFiles );
                      // We query by card in commitLastVersion so it is already secure by the fact that we are allowed to edit the card
                      const didRemoveFiles = CardFilesHelpers.commitLastVersion( CardAttachmentsCollection, card, removedFiles );
                      // Make a snapshot only if we need to (currentVersion is null means it was not previously made)
                      if ( didRemoveFiles && currentVersion == null ) {
                          // Pass the id so that a fresh copy is fetched for the snapshot
                          currentVersion = CardCollection.snapshotAndUpdate( this.userId, card.id );
                      }
                  }

                  if ( changedPicture ) {
                      log.debug( `update - Committing picture` );
                      // Check for pending uploaded files
                      // This is done here instead of in the after update hook because sometimes the hook will not run
                      // as the card might not contain any changes but we have new files that were uploaded.
                      // Only allow creating a card snapshot if we don't yet have made one (currentVersion being null).
                      currentVersion = CardFilesHelpers.commitPending( CardImagesCollection, this.userId, card, currentVersion );
                  } else if ( removedPicture ) {
                      log.verbose( `update - Removing picture` );
                      const didRemovePicture = CardFilesHelpers.commitLastVersion( CardImagesCollection, card );
                      // Make a snapshot only if we need to (currentVersion is null means it was not previously made)
                      if ( didRemovePicture && currentVersion == null ) {
                          // Pass the id so that a fresh copy is fetched for the snapshot
                          currentVersion = CardCollection.snapshotAndUpdate( this.userId, card.id );
                      }
                  }

                  if ( addedFiles ) {
                      log.debug( `update - Committing pending files` );
                      // Check for pending uploaded files
                      // This is done here instead of in the after update hook because sometimes the hook will not run
                      // as the card might not contain any changes but we have new files that were uploaded.
                      // Only allow creating a card snapshot if we don't yet have made one (currentVersion being null).
                      currentVersion = CardFilesHelpers.commitPending( CardAttachmentsCollection, this.userId, card, currentVersion );
                  }

                  if ( addedFiles || ( removedFiles && removedFiles.length ) ) {
                      log.debug( `update - Counting attachments` );
                      CardAttachmentsCollection.updateAttachmentCount( card.id, currentVersion || card.version );
                  }

                  // AUDIT
                  Audit.next( {
                      domain:  CardCollection.entityName,
                      message: CardAuditMessages.updated,
                      userId:  this.userId,
                      cardId:  card.id
                  } );
              }
          } ),

          /**
           * Archive a card
           *
           * @param {string} cardId Card Id
           */
          archive: new ValidatedMethod( {
              name:     'pr/card/archive',
              validate: new SimpleSchema( {
                  cardId: { type: String }
              } ).validator(),
              run( { cardId } ) {

                  log.verbose( `archive - ${cardId}` );

                  // SECURITY

                  if ( !CardSecurity.canArchive( this.userId, cardId ) ) {
                      log.warn( `archive - ${this.userId} is not authorized to archive card ${cardId}` );
                      throw new Meteor.Error( 'unauthorized' );
                  }

                  // PRECONDITIONS

                  // It is important to fetch all fields
                  // since we are going to make snapshots
                  const card = CardCollection.findOne( {
                      id:      cardId,
                      current: true
                  }, { transform: null } );
                  if ( !card ) {
                      throw new Meteor.Error( 'card not found' );
                  }

                  // UPDATE

                  // SIMULATION
                  // The removal procedure is far too convoluted for the client
                  // and leaves the board suspendend in an intermediary state
                  // so we just remove the card directly
                  if ( Meteor.isClient ) {
                      CardCollection.direct.remove( { id: cardId, current: true } );
                      return;
                  }

                  // Snapshot the last card state
                  log.debug( `archive - Making snapshot of the last version of card ${card.id}` );
                  card.current      = false;
                  const lastVersion = CardCollection.snapshot( this.userId, _.clone( card ) );

                  // Update version (will be used when unlinking)
                  card.version = lastVersion;

                  // Set card as deleted
                  //TODO: Deleted timestamps
                  CardCollection.direct.update( { _id: card._id }, {
                      $set: {
                          current: false,
                          deleted: true,
                          version: lastVersion
                      }
                  } );

                  // Commit the last card version to the files
                  log.debug( `archive - Committing last file version ${lastVersion} for card ${card.id}` );
                  CardFilesHelpers.commitLastVersion( [
                      CardImagesCollection,
                      CardAttachmentsCollection
                  ], { id: card.id, version: lastVersion } );

                  // Deactivate links
                  log.debug( `archive - Removing links targeting card ${card.id}` );
                  CardLinksCollection.unlinkArchivedCard( this.userId, _.clone( card ) );

                  // User points, remove creation points if removing your own card (prevents abuse)
                  if ( this.userId == card.author ) {
                      Gamification.awardPoints( card.author, -Settings.shared.gamification.points.card.creation, card.project );
                  }

                  // AUDIT

                  Audit.next( {
                      domain:  CardCollection.entityName,
                      message: CardAuditMessages.archived,
                      userId:  this.userId,
                      cardId:  cardId
                  } );
              }
          } ),

          /**
           * Remove a card
           *
           * @param {string} cardId Card Id
           */
          remove: new ValidatedMethod( {
              name:     'pr/card/remove',
              validate: new SimpleSchema( {
                  cardId: { type: String }
              } ).validator(),
              run( { cardId } ) {

                  log.verbose( `remove - ${cardId}` );

                  // SECURITY

                  if ( !CardSecurity.canRemove( this.userId, cardId ) ) {
                      log.warn( `remove - ${this.userId} is not authorized to remove card ${cardId}` );
                      throw new Meteor.Error( 'unauthorized' );
                  }

                  // PRECONDITIONS

                  const card = CardCollection.findOne( {
                          id:      cardId,
                          current: true
                      },
                      {
                          fields:    {
                              author: 1
                          },
                          transform: null
                      } );
                  if ( !card ) {
                      throw new Meteor.Error( 'card not found' );
                  }

                  // UPDATE

                  CardCollection.remove( { id: cardId } );

                  // User points, remove creation points if removing your own card (prevents abuse)
                  if ( this.userId == card.author ) {
                      Gamification.awardPoints( card.author, -Settings.shared.gamification.points.card.creation, card.project );
                  }

                  // AUDIT

                  Audit.next( {
                      domain:  CardCollection.entityName,
                      message: CardAuditMessages.removed,
                      userId:  this.userId,
                      cardId:  cardId
                  } );
              }
          } ),

          /**
           * View a card
           * TODO: This should be throttled
           *
           * @param {string} cardId Card Id
           */
          view: new ValidatedMethod( {
              name:     'pr/card/view',
              validate: new SimpleSchema( {
                  cardId: { type: String }
              } ).validator(),
              run( { cardId } ) {

                  // Running on server only as the card might not be available for
                  // checks at this point on the client
                  if ( Meteor.isServer ) {

                      // SECURITY

                      if ( !CardSecurity.canRead( this.userId, cardId ) ) {
                          throw new Meteor.Error( 'unauthorized' )
                      }

                      // PRECONDITIONS

                      const card = CardCollection.findOne(
                          {
                              id:      cardId,
                              current: true
                          },
                          {
                              fields:    {
                                  id:              1,
                                  author:          1,
                                  lastTemperature: 1,
                                  heatedAt:        1
                              },
                              transform: null
                          }
                      );
                      if ( !card ) {
                          throw new Meteor.Error( 'card not found' );
                      }

                      // Don't record view to your own cards
                      if ( this.userId == card.author ) {
                          return;
                      }

                      // UPDATE

                      const modifier = { $inc: { viewCount: 1 } };

                      // Apply "heat"
                      CardAnalytics.heatModifier( card, Settings.shared.analytics.card.heat.view, modifier );

                      // Update
                      CardCollection.update( { id: cardId, current: true }, modifier );
                  }
              }
          } ),

          /**
           * Like a card
           *
           * @param {string} cardId Card Id
           */
          like: new ValidatedMethod( {
              name:     'pr/card/like',
              validate: new SimpleSchema( {
                  cardId: { type: String }
              } ).validator(),
              run( { cardId } ) {

                  // SECURITY

                  if ( !CardSecurity.canLike( this.userId, cardId ) ) {
                      throw new Meteor.Error( 'unauthorized' );
                  }

                  // PRECONDITIONS

                  const card = CardCollection.findOne(
                      {
                          id:      cardId,
                          current: true
                      },
                      {
                          fields:    {
                              author:          1,
                              project:         1,
                              likes:           1,
                              lastTemperature: 1,
                              heatedAt:        1
                          },
                          transform: null
                      }
                  );
                  if ( !card ) {
                      throw new Meteor.Error( 'card not found' );
                  }

                  // UPDATE

                  let modifier       = {};
                  const alreadyLiked = card.likes && card.likes.indexOf( this.userId ) != -1;
                  if ( alreadyLiked ) {
                      // Unlike
                      modifier.$pull = { likes: this.userId };
                  } else {
                      // Like
                      modifier.$addToSet = { likes: this.userId };
                  }

                  // Set the like count after checking the permissions, as the user can never change those directly
                  if ( Meteor.isServer ) {
                      // On the server calculate the real like count since we have access to likes
                      const likeCount = card.likes ? card.likes.length : 0;
                      modifier.$set   = { likeCount: likeCount + ( alreadyLiked ? -1 : 1 ) };

                  } else {
                      // On the client just inc/dec temporarily
                      modifier.$inc = { likeCount: alreadyLiked ? -1 : 1 };
                  }

                  // Apply "heat" only if not liked/unliked by the card author
                  //FIXME: We don't know when the card was liked so we can't cool it with the right amount
                  if ( this.userId != card.author ) {
                      CardAnalytics.heatModifier( card, ( alreadyLiked ? -1 : 1 ) * Settings.shared.analytics.card.heat.like, modifier );
                  }

                  // Update card
                  CardCollection.update( { id: cardId, current: true }, modifier );

                  // Give the user points only if not liking/unliking its own card
                  if ( this.userId != card.author ) {
                      Gamification.awardPoints( this.userId, ( alreadyLiked ? -1 : 1 ) * Settings.shared.gamification.points.card.like, card.project );
                  }

                  // AUDIT

                  Audit.next( {
                      domain:  CardCollection.entityName,
                      message: alreadyLiked ? CardAuditMessages.unliked : CardAuditMessages.liked,
                      userId:  this.userId,
                      cardId:  cardId
                  } );

              }
          } ),

          /**
           * Link two cards together
           *
           * @param {String} linkCardId Card being linked
           * @param {String} toCardId Card receiving the link
           */
          linkCards: new ValidatedMethod( {
              name:     'pr/card/linkCards',
              validate: new SimpleSchema( {
                  sourceCardId:      { type: String },
                  destinationCardId: { type: String },
                  linkType:          { type: String, allowedValues: CardLinkTypeValues }
              } ).validator( { clean: true } ),
              run( { sourceCardId, destinationCardId, linkType } ) {

                  // PREFETCHING FOR SECURITY

                  if ( sourceCardId == destinationCardId ) {
                      throw new Meteor.Error( 'cannot link card to itself' );
                  }

                  const sourceCard = CardCollection.findOne(
                      {
                          id:      sourceCardId,
                          current: true
                      }, {
                          transform: null
                      }
                  );

                  if ( !sourceCard ) {
                      throw new Meteor.Error( 'source card not found' );
                  }

                  const destinationCard = CardCollection.findOne(
                      {
                          id:      destinationCardId,
                          current: true
                      },
                      {
                          transform: null
                      }
                  );

                  if ( !destinationCard ) {
                      throw new Meteor.Error( 'destination card not found' );
                  }

                  if ( sourceCard.project != destinationCard.project ) {
                      throw new Meteor.Error( 'cards not from same project' );
                  }

                  // SECURITY

                  if ( !CardSecurity.canLink( this.userId, sourceCard, destinationCard ) ) {
                      throw new Meteor.Error( 'unauthorized' );
                  }

                  // PRECONDITIONS

                  const link = CardLinksCollection.findOne( {
                      'source.id':               sourceCard.id,
                      'source.versionFrom':      { $lte: sourceCard.version },
                      'source.versionTo':        { $exists: false },
                      'destination.id':          destinationCard.id,
                      'destination.versionFrom': { $lte: destinationCard.version },
                      'destination.versionTo':   { $exists: false }
                  } );

                  // UPDATE

                  if ( !link ) {
                      // Not present, create it
                      log.debug( `linkCards - Creating card link: ${sourceCard.id} -> ${linkType} -> ${destinationCard.id}` );

                      const sourceVersion      = CardCollection.snapshotAndUpdate( this.userId, sourceCard );
                      const destinationVersion = CardCollection.snapshotAndUpdate( this.userId, destinationCard );

                      CardLinksCollection.insert( {
                          source:      {
                              id:          sourceCard.id,
                              versionFrom: sourceVersion
                          },
                          destination: {
                              id:          destinationCard.id,
                              versionFrom: destinationVersion
                          },
                          type:        linkType,
                          linkedAt:    new Date(),
                          linkedBy:    this.userId
                      } );

                      // Apply "heat" to linked cards only if not linked by their author
                      if ( this.userId != sourceCard.author ) {
                          CardAnalytics.heat( sourceCard, Settings.shared.analytics.card.heat.linkSource );
                      }
                      if ( this.userId != destinationCard.author ) {
                          CardAnalytics.heat( destinationCard, Settings.shared.analytics.card.heat.linkDestination );
                      }

                      // Give reputation points to the user only if using someone else's card in the link
                      if ( this.userId != sourceCard.author || this.userId != destinationCard.author ) {
                          Gamification.awardPoints( this.userId, Settings.shared.gamification.points.card.link, destinationCard.project );
                      }

                      // AUDIT

                      Audit.next( {
                          domain:            CardCollection.entityName,
                          message:           CardAuditMessages.linked,
                          userId:            this.userId,
                          sourceCardId:      sourceCard.id,
                          destinationCardId: destinationCard.id,
                          linkType:          linkType
                      } );

                  } else if ( link.type != linkType ) {
                      // Already present, update it
                      log.debug( `linkCards - Updating card link: ${sourceCardId.id} -> ${linkType} -> ${destinationCard.id}` );
                      CardLinksCollection.update(
                          {
                              _id: link._id,
                          },
                          {
                              $set: {
                                  type:      linkType,
                                  updatedAt: new Date(),
                                  updatedBy: this.userId
                              }
                          }
                      );
                  }
              }
          } ),

          /**
           * Unlink two cards
           *
           * @param {String} cardId Card holding the relation
           * @param {String} unlinkCardId Card being unlinked
           */
          unlink: new ValidatedMethod( {
              name:     'pr/card/unlinkCards',
              validate: new SimpleSchema( {
                  linkId: { type: String }
              } ).validator( { clean: true } ),
              run( { linkId } ) {

                  // PRECONDITIONS

                  const link = CardLinksCollection.findOne(
                      {
                          _id: linkId
                      },
                      {
                          fields: {
                              _id:         1,
                              source:      1,
                              destination: 1,
                              type:        1,
                              linkedBy:    1,
                              linkedAt:    1
                          }
                      }
                  );
                  if ( !link ) {
                      throw new Meteor.Error( 'link not found' );
                  }

                  const sourceCard = CardCollection.findOne(
                      {
                          id:      link.source.id,
                          current: true
                      },
                      {
                          transform: null
                      }
                  );

                  if ( !sourceCard ) {
                      throw new Meteor.Error( 'card not found' );
                  }

                  const destinationCard = CardCollection.findOne(
                      {
                          id:      link.destination.id,
                          current: true
                      },
                      {
                          transform: null
                      }
                  );

                  if ( !destinationCard ) {
                      throw new Meteor.Error( 'card not found' );
                  }

                  // SECURITY

                  if ( !CardSecurity.canUnlink( this.userId, sourceCard, destinationCard ) ) {
                      throw new Meteor.Error( 'unauthorized' );
                  }

                  // UPDATE

                  CardLinksCollection.update(
                      {
                          _id: link._id
                      },
                      {
                          $set: {
                              'source.versionTo':      sourceCard.version,
                              'destination.versionTo': destinationCard.version,
                              unlinkedAt:              new Date(),
                              unlinkedBy:              this.userId
                          }
                      }
                  );

                  CardCollection.snapshotAndUpdate( this.userId, sourceCard );
                  CardCollection.snapshotAndUpdate( this.userId, destinationCard );

                  // Apply "heat" to linked cards only if the card author isn't the user unlinking
                  // Of course here, if someone unlinks one of its cards (A) linked by someone else (B), this will
                  // remove points on (B) not originally gained when linked by (B), we"re testing this,
                  // it could serve as a punishment for doing a wrong link ;)
                  if ( this.userId != sourceCard.author ) {
                      CardAnalytics.cool( sourceCard, link.linkedAt, Settings.shared.analytics.card.heat.linkSource );
                  }
                  if ( this.userId != destinationCard.author ) {
                      CardAnalytics.cool( destinationCard, link.linkedAt, Settings.shared.analytics.card.heat.linkDestination );
                  }

                  // Remove reputation points from the user only if removing a link they created
                  // that is using someone else's cards (links using two of the user's card don't count when linking)
                  if ( link.linkedBy && ( link.linkedBy != sourceCard.author || link.linkedBy != destinationCard.author ) ) {
                      Gamification.awardPoints( link.linkedBy, -Settings.shared.gamification.points.card.link, destinationCard.project );
                  }

                  // AUDIT

                  Audit.next( {
                      domain:            CardCollection.entityName,
                      message:           CardAuditMessages.unlinked,
                      userId:            this.userId,
                      sourceCardId:      sourceCard.id,
                      destinationCardId: destinationCard.id,
                      linkType:          link.type
                  } );
              }
          } ),

          /**
           * Move a card in a new phase
           *
           * @param {String} cardId
           * @param {String} phaseId
           */
          moveToPhase: new ValidatedMethod( {
              name:     'pr/card/moveToPhase',
              validate: new SimpleSchema( {
                  cardId:  { type: String },
                  phaseId: { type: String }
              } ).validator(),
              run( { cardId, phaseId } ) {
                  // SECURITY

                  if ( !CardSecurity.canMove( this.userId, cardId, phaseId ) ) {
                      throw new Meteor.Error( 'unauthorized' )
                  }

                  // PRECONDITIONS

                  const card = CardCollection.findOne( { id: cardId, current: true }, {
                      fields:    { id: 1, author: 1, project: 1, phase: 1, version: 1, lastTemperature: 1, heatedAt: 1 },
                      transform: null
                  } );
                  if ( !card ) {
                      throw new Meteor.Error( 'card not found' );
                  }

                  const project = ProjectCollection.findOne( { _id: card.project, 'phases._id': phaseId }, {
                      fields:    { _id: 1 },
                      transform: null
                  } );
                  if ( !project ) {
                      throw new Meteor.Error( 'phase not found' );
                  }

                  // TODO: CHECK PHASE IS AVAILABLE FOR MOVING

                  // UPDATE

                  // Record the version on phase exit
                  // This has to go before we snapshot since it's exit information that the snapshot needs
                  CardCollection.update( {
                      id:          cardId,
                      current:     true,
                      phaseVisits: { $elemMatch: { exitedVersion: { $exists: false } } }
                  }, {
                      $set: {
                          'phaseVisits.$.exitedVersion': card.version,
                          'phaseVisits.$.exitedAt':      new Date(),
                          'phaseVisits.$.exitedTo':      phaseId
                      }
                  } );

                  // Snapshot
                  const newVersion = CardCollection.snapshot( this.userId, card.id );

                  // Update the card
                  const modifier = {
                      $set:  {
                          phase:   phaseId,
                          version: newVersion
                      },
                      $push: {
                          phaseVisits: PhaseVisits.createVisit( phaseId, PhaseVisits.visitTypes.moved, newVersion, card.phase )
                      }
                  };

                  // Apply "heat" only if not moved by the card author
                  if ( this.userId != card.author ) {
                      CardAnalytics.heatModifier( card, Settings.shared.analytics.card.heat.move, modifier );
                  }

                  // Do the update
                  CardCollection.update( { id: cardId, current: true }, modifier );

                  // AUDIT

                  Audit.next( {
                      domain:      CardCollection.entityName,
                      message:     CardAuditMessages.moved,
                      userId:      this.userId,
                      cardId:      cardId,
                      fromPhaseId: card.phase,
                      toPhaseId:   phaseId
                  } );
              }
          } ),

          /**
           * Pin card
           *
           * @param {string} cardIdId
           */
          pin: new ValidatedMethod( {
              name:     'pr/card/pin',
              validate: new SimpleSchema( {
                  cardId: {
                      type: String
                  }
              } ).validator(),
              run( { cardId } ) {

                  // PREREQUISITES

                  const card = CardCollection.findOne( { id: cardId, current: true }, { fields: { id: 1, project: 1, pinned: 1 }, transform: null } );
                  if ( !card ) {
                      throw new Meteor.Error( 'card not found' )
                  }

                  if ( card.pinned ) {
                      log.debug( `pin - Card ${cardId} already pinned.` );
                      return;
                  }

                  // SECURITY

                  // Only allow logged in experts or project experts
                  if ( !CardSecurity.canPin( this.userId, card ) ) {
                      log.warn( `pin - ${this.userId} is not authorized to pin card ${cardId}` );
                      throw new Meteor.Error( 'unauthorized' );
                  }

                  // UPDATE
                  const modifier = { $set: { pinned: true } };
                  snapshotAndSetModifier( this.userId, card, modifier );
                  CardCollection.update( { id: card.id, current: true }, modifier );

                  // AUDIT

                  Audit.next( {
                      domain:  CardCollection.entityName,
                      message: CardAuditMessages.pinned,
                      userId:  this.userId,
                      cardId:  cardId
                  } );
              }
          } ),

          /**
           * Unpin card
           *
           * @param {string} cardIdId
           */
          unpin: new ValidatedMethod( {
              name:     'pr/card/unpin',
              validate: new SimpleSchema( {
                  cardId: {
                      type: String
                  }
              } ).validator(),
              run( { cardId } ) {

                  // PREREQUISITES

                  const card = CardCollection.findOne( { id: cardId, current: true }, { fields: { id: 1, project: 1, pinned: 1 }, transform: null } );
                  if ( !card ) {
                      throw new Meteor.Error( 'card not found' )
                  }

                  if ( !card.pinned ) {
                      log.debug( `unpin - Card ${cardId} not pinned.` );
                      return;
                  }

                  // SECURITY

                  // Only allow logged in experts or project experts
                  if ( !CardSecurity.canPin( this.userId, card ) ) {
                      log.warn( `pin - ${this.userId} is not authorized to unpin card ${cardId}` );
                      throw new Meteor.Error( 'unauthorized' );
                  }

                  // UPDATE
                  const modifier = { $set: { pinned: false } };
                  snapshotAndSetModifier( this.userId, card, modifier );
                  CardCollection.update( { id: card.id, current: true }, modifier );

                  // AUDIT

                  Audit.next( {
                      domain:  CardCollection.entityName,
                      message: CardAuditMessages.unpinned,
                      userId:  this.userId,
                      cardId:  cardId
                  } );
              }
          } ),

          /**
           * Copy a card in a new phase
           *
           * @deprecated
           * @param {String} cardId
           * @param {String} phaseId
           */
          /*copyToPhase: new ValidatedMethod( {
           name:     'pr/card/copyToPhase',
           validate: new SimpleSchema( {
           cardId:  { type: String },
           phaseId: { type: String }
           } ).validator(),
           run( { cardId, phaseId } ) {
           // SECURITY

           if ( !CardSecurity.canCopy( this.userId, cardId, phaseId ) ) {
           throw new Meteor.Error( 'unauthorized' )
           }

           // PRECONDITIONS

           const card = CardCollection.findOne( { _id: cardId }, {
           fields:    {
           author:       0, // We change the author when copying
           commentCount: 0, // We don't keep the comments when copying
           viewCount:    0, // No stealing view counts from a popular card
           likes:        0, // No stealing likes from a popular card
           likeCount:    0, // No Stealing like counts from a popular card
           version:      0, // Version restarts to 0 when copying
           updatedAt:    0, // Can't be set on insert
           updatedBy:    0  // Can't be set on insert
           },
           transform: null // No transforms, we only want the plain object
           } );
           if ( !card ) {
           throw new Meteor.Error( 'card not found' );
           }

           const project = ProjectCollection.findOne( {
           _id:          card.project,
           'phases._id': phaseId
           }, {
           fields:    { _id: 1 },
           transform: null
           } );
           if ( !project ) {
           throw new Meteor.Error( 'phase not found' );
           }

           // UPDATE

           // Delete the original id
           const originalCardId = card.id;
           delete card.id;

           // Change the author
           //TODO: track the original author somehow
           card.author = this.userId;

           // Change the phase
           card.phase = phaseId;

           // Link to the original card
           // FIXME: Create a "copy" link
           //card.linkedCards = [originalCardId];

           // Make double sure of some defaults
           // Even though the query should not give us those fields
           // We might want to project against unfortunate future refactoring
           card.commentCount = 0;
           card.viewCount    = 0;
           card.likeCount    = 0;
           card.likes        = [];
           card.version      = 0;

           // Add the phase visit
           card.phaseVisits = [PhaseVisits.createVisit( phaseId, PhaseVisits.visitTypes.copied, card.version )];

           // Update the card
           const newCardId = CardCollection.insert( card );
           // RELATIONSHIPS

           // Add the new card as owner of the image
           // This was patched to allow copied cards to reference the same image but will
           // need to be replaced by a more robust method later
           //TODO: Replace by a more robust image ownership schema
           CardImagesCollection.update( { owners: originalCardId }, { $addToSet: { owners: newCardId } } );

           // AUDIT

           Audit.next( {
           domain:    CardCollection.entityName,
           message:   CardAuditMessages.copied,
           userId:    this.userId,
           cardId:    cardId,
           newCardId: newCardId
           } );
           }
           } ),*/

          /**
           * Get all available tags for a project
           *
           * @params { String } project
           */
          getAvailableTagList: new ValidatedMethod( {
              name:     'pr/card/tags/list',
              validate: new SimpleSchema( {
                  project: { type: String }
              } ).validator(),
              run( { project } ) {
                  // Check that the user has access to the project
                  if ( !ProjectSecurity.canRead( this.userId, project ) ) {
                      throw new Meteor.Error( 'unauthorized' );
                  }
                  return CardCollection.getTags( { project: project } );
              }
          } )

      }
    ;

export default CardMethods;

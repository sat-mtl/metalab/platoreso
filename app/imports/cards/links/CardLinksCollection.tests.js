"use strict";

import { chai, assert, expect } from 'meteor/practicalmeteor:chai';
import { sinon, spies, stubs } from 'meteor/practicalmeteor:sinon';
import { Factory } from "meteor/dburles:factory";

import CardCollection from "../CardCollection";
import CardLinksCollection from "./CardLinksCollection";

describe( 'cards/links/CardLinksCollection', function () {

    let sandbox;

    beforeEach( () => {
        sandbox = sinon.sandbox.create();
    } );

    afterEach( () => {
        sandbox.restore();
    } );

    describe( "unlinkArchivedCard", () => {

        beforeEach( () => {
            sandbox.stub( CardLinksCollection, "find" );
            sandbox.stub( CardCollection, "findOne" );
            sandbox.stub( CardLinksCollection, "update" );
            sandbox.stub( CardCollection, "snapshotAndUpdate" );

            // We don't really care about the returned version...
            // we just have to return something for the cache to work
            CardCollection.snapshotAndUpdate.returns(999);
        } );

        it( 'should remove links when removed card is source', () => {
            // SETUP
            CardLinksCollection.find.returns( [
                {
                    _id:         'linkId',
                    source:      {
                        id: 'cardId'
                    },
                    destination: {
                        id: 'destinationId'
                    }
                }
            ] );
            CardCollection.findOne.returns( {
                id:      'destinationId',
                version: 1
            } );

            // EXECUTION
            CardLinksCollection.unlinkArchivedCard( 'userId', { id: 'cardId', version: 10 } );

            // TESTS
            expect( CardCollection.findOne.callCount ).to.eql( 1 );
            assert( CardCollection.findOne.getCall( 0 ).calledWith(
                { id: 'destinationId', current: true },
                { transform: null }
            ) );
            expect( CardLinksCollection.update.callCount ).to.eql( 1 );
            assert( CardLinksCollection.update.calledWith( { _id: 'linkId' },
                {
                    $set: {
                        'source.versionTo':      10,
                        'destination.versionTo': 1,
                        unlinkedAt:              sinon.match.date,
                        unlinkedBy:              'userId'
                    }
                } ) );
            expect( CardCollection.snapshotAndUpdate.callCount ).to.eql( 1 );
            assert( CardCollection.snapshotAndUpdate.calledWith( 'userId', {
                id: 'destinationId',
                version: 1
            } ) );
        } );

        it( 'should remove links when removed card is destination', () => {
            // SETUP
            CardLinksCollection.find.returns( [
                {
                    _id:         'linkId',
                    source:      {
                        id: 'sourceId'
                    },
                    destination: {
                        id: 'cardId'
                    }
                }
            ] );
            CardCollection.findOne.returns( {
                id:      'sourceId',
                version: 1
            } );

            // EXECUTION
            CardLinksCollection.unlinkArchivedCard( 'userId', { id: 'cardId', version: 10 } );

            // TESTS
            expect( CardCollection.findOne.callCount ).to.eql( 1 );
            assert( CardCollection.findOne.getCall( 0 ).calledWith(
                { id: 'sourceId', current: true },
                { transform: null }
            ) );
            expect( CardLinksCollection.update.callCount ).to.eql( 1 );
            assert( CardLinksCollection.update.calledWith( { _id: 'linkId' },
                {
                    $set: {
                        'source.versionTo':      1,
                        'destination.versionTo': 10,
                        unlinkedAt:              sinon.match.date,
                        unlinkedBy:              'userId'
                    }
                } ) );
            expect( CardCollection.snapshotAndUpdate.callCount ).to.eql( 1 );
            assert( CardCollection.snapshotAndUpdate.calledWith( 'userId', {
                id: 'sourceId',
                version: 1
            } ) );
        } );

        it( 'should remove links when removed card is both source and destination', () => {
            // SETUP
            CardLinksCollection.find.returns( [
                {
                    _id:         'link1Id',
                    source:      {
                        id: 'cardId'
                    },
                    destination: {
                        id: 'destinationId'
                    }
                },
                {
                    _id:         'link2Id',
                    source:      {
                        id: 'sourceId'
                    },
                    destination: {
                        id: 'cardId'
                    }
                }
            ] );
            CardCollection.findOne.onFirstCall().returns( {
                id:      'destinationId',
                version: 1
            } );
            CardCollection.findOne.onSecondCall().returns( {
                id:      'sourceId',
                version: 2
            } );

            // EXECUTION
            CardLinksCollection.unlinkArchivedCard( 'userId', { id: 'cardId', version: 10 } );

            // TESTS
            expect( CardCollection.findOne.callCount ).to.eql( 2 );
            assert( CardCollection.findOne.getCall( 0 ).calledWith(
                { id: 'destinationId', current: true },
                { transform: null }
            ) );
            assert( CardCollection.findOne.getCall( 1 ).calledWith(
                { id: 'sourceId', current: true },
                { transform: null }
            ) );
            expect( CardLinksCollection.update.callCount ).to.eql( 2 );
            assert( CardLinksCollection.update.getCall(0).calledWith( { _id: 'link1Id' },
                {
                    $set: {
                        'source.versionTo':      10,
                        'destination.versionTo': 1,
                        unlinkedAt:              sinon.match.date,
                        unlinkedBy:              'userId'
                    }
                } ) );
            assert( CardLinksCollection.update.getCall(1).calledWith( { _id: 'link2Id' },
                {
                    $set: {
                        'source.versionTo':      2,
                        'destination.versionTo': 10,
                        unlinkedAt:              sinon.match.date,
                        unlinkedBy:              'userId'
                    }
                } ) );
            expect( CardCollection.snapshotAndUpdate.callCount ).to.eql( 2 );
            assert( CardCollection.snapshotAndUpdate.getCall(0).calledWith( 'userId', {
                id: 'destinationId',
                version: 1
            } ) );
            assert( CardCollection.snapshotAndUpdate.getCall(1).calledWith( 'userId', {
                id: 'sourceId',
                version: 2
            } ) );
        } );

        it( 'should not snapshot the same card twice', () => {
            // SETUP
            CardLinksCollection.find.returns( [
                {
                    _id:         'link1Id',
                    source:      {
                        id: 'cardId'
                    },
                    destination: {
                        id: 'otherId'
                    }
                },
                {
                    _id:         'link2Id',
                    source:      {
                        id: 'otherId'
                    },
                    destination: {
                        id: 'cardId'
                    }
                }
            ] );
            CardCollection.findOne.onFirstCall().returns( {
                id:      'otherId',
                version: 1
            } );
            CardCollection.findOne.onSecondCall().returns( {
                id:      'otherId',
                version: 1
            } );

            // EXECUTION
            CardLinksCollection.unlinkArchivedCard( 'userId', { id: 'cardId', version: 10 } );

            // TESTS
            expect( CardCollection.findOne.callCount ).to.eql( 2 );
            assert( CardCollection.findOne.getCall( 0 ).calledWith(
                { id: 'otherId', current: true },
                { transform: null }
            ) );
            assert( CardCollection.findOne.getCall( 1 ).calledWith(
                { id: 'otherId', current: true },
                { transform: null }
            ) );
            expect( CardLinksCollection.update.callCount ).to.eql( 2 );
            assert( CardLinksCollection.update.getCall(0).calledWith( { _id: 'link1Id' },
                {
                    $set: {
                        'source.versionTo':      10,
                        'destination.versionTo': 1,
                        unlinkedAt:              sinon.match.date,
                        unlinkedBy:              'userId'
                    }
                } ) );
            assert( CardLinksCollection.update.getCall(1).calledWith( { _id: 'link2Id' },
                {
                    $set: {
                        'source.versionTo':      1,
                        'destination.versionTo': 10,
                        unlinkedAt:              sinon.match.date,
                        unlinkedBy:              'userId'
                    }
                } ) );
            expect( CardCollection.snapshotAndUpdate.callCount ).to.eql( 1 );
            assert( CardCollection.snapshotAndUpdate.getCall(0).calledWith( 'userId', {
                id: 'otherId',
                version: 1
            } ) );
        } );

    } );

} );
"use strict";

import Logger from "meteor/metalab:logger/Logger";
import CardCollection from "../CardCollection";

const log = Logger( "cardLink" );

/**
 * Card Link Model
 *
 * @param doc
 * @constructor
 */
export default class CardLink {

    /**
     * @constructor
     * @param doc
     */
    constructor( doc ) {
        _.extend( this, doc );
    }

    get sourceCardQuery() {
        const query = {
            id: this.source.id
        };

        if ( this.source.versionTo != null ) {
            query.$and = [
                { version: { $gte: this.source.versionFrom } },
                { version: { $lte: this.source.versionTo } },
            ];
        } else {
            query.version = { $gte: this.source.versionFrom }
        }

        return query;
    }

    findSourceLinkedCard( options ) {
        return CardCollection.find(
            this.sourceCardQuery,
            _.defaults( { sort: { version: -1 }, limit: 1 }, options )
        );
    }

    getSourceLinkedCard( options ) {
        return CardCollection.findOne(
            this.sourceCardQuery,
            _.defaults( { sort: { version: -1 } }, options )
        );
    }

    get destinationCardQuery() {
        const query = {
            id: this.destination.id
        };

        if ( this.destination.versionTo != null ) {
            query.$and = [
                { version: { $gte: this.destination.versionFrom } },
                { version: { $lte: this.destination.versionTo } },
            ];
        } else {
            query.version = { $gte: this.destination.versionFrom }
        }

        return query;
    }

    findDestinationLinkedCard( options ) {
        return CardCollection.find(
            this.destinationCardQuery,
            _.defaults( { sort: { version: -1 }, limit: 1 }, options )
        );    }

    getDestinationLinkedCard( options ) {
        return CardCollection.findOne(
            this.destinationCardQuery,
            _.defaults( { sort: { version: -1 } }, options )
        );    }
}
"use strict";

import Logger from "meteor/metalab:logger/Logger";
import CollectionHelpers from "/imports/collections/CollectionHelpers";
import CardLink from "./CardLink";
import CardLinkSchema from "./CardLinkSchema";
import CardCollection from "../CardCollection";

const log = Logger( "cards-links-collection" );

class CardLinksCollection extends Mongo.Collection {
    constructor( name, options = {} ) {
        // Change transform before calling super, this is the only way to set it
        // Also, this won't suffer from module loading as it is a callback
        options.transform = cardLink => new CardLink( cardLink );

        super( name, options );

        this.entityName = 'cardLink';

        /**
         * Schema
         */

        this.attachSchema( CardLinkSchema );

        /**
         * Indexes
         */

        if ( Meteor.isServer ) {
            this._ensureIndex( {
                'source.id':          1,
                'source.versionFrom': 1,
                'source.versionTo':   1
            }, { name: 'source_id_versionFrom_versionTo' } );

            this._ensureIndex( {
                'destination.id':          1,
                'destination.versionFrom': 1,
                'destination.versionTo':   1
            }, { name: 'destination_id_versionFrom_versionTo' } );
        }

        /**
         * Security
         */

        this.deny( {
            insert: () => true,
            update: () => true,
            remove: () => true
        } );

        // The rest goes into a Meteor.startup because since we create the collection instance
        // right away when exporting from this module, we end up executing this constructor
        // before the imported modules are available (when there are circular dependencies).
        // This way we are sure the app is loaded when we execute the following code.
        Meteor.startup( () => {

            /**
             * Registration
             * TODO: Get rid of that system
             */

            CollectionHelpers.registerCollection( this.entityName, this, CardLink );

        } );
    }

    /**
     * Deactivate links using the passed card.
     *
     * @param {String} userId - Id of the user doing the removal
     * @param {Object} card
     */
    unlinkArchivedCard( userId, card ) {
        if ( !userId ) {
            log.error( 'unlinkArchivedCard - missing userId parameter' );
            return false;
        }

        if ( !card ) {
            log.error( 'unlinkArchivedCard - missing card parameter' );
            return false;
        }

        if ( card.id == null ) {
            log.error( 'unlinkArchivedCard - id unavailable on card' );
            return false;
        }

        if ( card.version == null ) {
            log.error( 'unlinkArchivedCard - version unavailable on card' );
            return false;
        }

        const snapshots = {};

        this.find( {
            $or: [{
                'source.id':        card.id,
                'source.versionTo': { $exists: false }
            }, {
                'destination.id':        card.id,
                'destination.versionTo': { $exists: false }
            }]
        } ).forEach( link => {
            let sourceCard;
            if ( card.id == link.source.id ) {
                sourceCard = card;
            } else {
                sourceCard = CardCollection.findOne( {
                    id:      link.source.id,
                    current: true
                }, { transform: null } );
            }
            if ( !sourceCard ) {
                return;
            }

            let destinationCard;
            if ( card.id == link.destination.id ) {
                destinationCard = card;
            } else {
                destinationCard = CardCollection.findOne( {
                    id:      link.destination.id,
                    current: true
                }, { transform: null } );
            }
            if ( !destinationCard ) {
                return
            }

            this.update( { _id: link._id }, {
                $set: {
                    'source.versionTo':      sourceCard.version,
                    'destination.versionTo': destinationCard.version,
                    unlinkedAt:              new Date(),
                    unlinkedBy:              userId
                }
            } );

            if ( card.id != sourceCard.id && !snapshots[sourceCard.id] ) {
                snapshots[sourceCard.id] = CardCollection.snapshotAndUpdate( userId, _.clone( sourceCard ) );
            }

            if ( card.id != destinationCard.id && !snapshots[destinationCard.id] ) {
                snapshots[destinationCard.id] = CardCollection.snapshotAndUpdate( userId, _.clone( destinationCard ) );
            }
        } );
    }

    unlinkRemovedCard( cardId ) {
        this.remove( {
            $or: [
                { 'source.id': cardId },
                { 'destination.id': cardId }
            ]
        } );
    }
}

export default new CardLinksCollection( "cardLinks" );
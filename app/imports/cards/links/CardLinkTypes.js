"use strict";

//NOTE: Don't forget to edit the corresponding CSS rules & arrow markers for the graph links

const CardLinkTypes = {
    references: {
        id: 'references',
        label: 'card.links.references'
    },
    complements: {
        id: 'complements',
        label: 'card.links.complements'
    },
    derives: {
        id: 'derives',
        label: 'card.links.derives'
    }
};

export default CardLinkTypes;

export const CardLinkTypeValues = _.values( CardLinkTypes ).map( linkType => linkType.id );
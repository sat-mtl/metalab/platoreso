"use strict";

import {SimpleSchema} from "meteor/aldeed:simple-schema";
import {CardLinkTypeValues} from "../links/CardLinkTypes";

const LinkedCardSchema = new SimpleSchema( {
    id:          {
        type: String
    },
    versionFrom: {
        type: Number
    },
    versionTo:   {
        type:     Number,
        optional: true
    }
} );

/**
 * Linked Card Schema
 *
 * @type {SimpleSchema}
 */
const CardLinkSchema = new SimpleSchema( {

    /**
     * Id of the source linked card
     */

    source:      {
        type: LinkedCardSchema
    },

    /**
     * Id of the destination linked card
     */

    destination: {
        type: LinkedCardSchema
    },

    /**
     * Type of link
     */

    type:        {
        type:          String,
        allowedValues: CardLinkTypeValues
    },

    linkedAt: {
        type: Date
    },

    linkedBy: {
        type: String
    },

    updatedAt: {
        type: Date,
        optional: true
    },

    updatedBy: {
        type: String,
        optional: true
    },

    unlinkedAt: {
        type: Date,
        optional: true
    },

    unlinkedBy: {
        type: String,
        optional: true
    }
} );

export default CardLinkSchema;
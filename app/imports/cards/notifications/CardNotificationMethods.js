"use strict";

import {ValidatedMethod} from "meteor/mdg:validated-method";
import NotificationCollection from "/imports/notifications/NotificationCollection";
import ProjectCollection from "../../projects/ProjectCollection";
import ProjectSecurity from "../../projects/ProjectSecurity";
import CardCollection from "../CardCollection";
import CardImagesCollection from "../images/CardImagesCollection";

const CardNotificationMethods = {

    fetch: new ValidatedMethod( {
            name:     'pr/notification/card',
            validate: new SimpleSchema( {
                notificationId: {
                    type: String
                }
            } ).validator( { clean: true } ),
            run( { notificationId } ) {

                // NOTIFICATION

                // Can only read own notifications, so also search by userId
                const notification = NotificationCollection.findOne( { _id: notificationId, user: this.userId }, {
                    fields: {
                        objects: 1
                    }
                } );
                if ( !notification ) {
                    throw new Meteor.Error( 'notification not found' );
                }

                // PROJECT

                const projectId = notification.getObjectIdByType( ProjectCollection.entityName );
                // Check the project directly, it skips a call to DB
                if ( !ProjectSecurity.canRead( this.userId, projectId ) ) {
                    throw new Meteor.Error( 'unauthorized' );
                }

                // DATA

                return {
                    cards:     CardCollection.find( {
                        id:      { $in: notification.getObjectIdsByType( CardCollection.entityName ) },
                        current: true
                    }, {
                        fields: {
                            _id:     1,
                            id:      1,
                            slug:    1,
                            name:    1,
                            version: 1,
                            current: 1
                        }
                    } ).fetch(),
                    cardImage: CardImagesCollection.findOne( { owners: notification.getObjectIdByType( CardCollection.entityName ) } ),
                    project:   ProjectCollection.findOne( { _id: projectId }, {
                        fields: {
                            _id:           1,
                            slug:          1,
                            name:          1,
                            'phases._id':  1,
                            'phases.name': 1
                        }
                    } )
                };
            }
        }
    )
};

export default CardNotificationMethods;
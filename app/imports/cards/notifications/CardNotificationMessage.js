"use strict";

import {NotificationTypes} from "/imports/cards/notifications/CardNotifications";
import NotificationMessageHelpers from "/imports/notifications/NotificationMessageHelpers";

export default class CardNotificationMessage {

    static getLink( notification, subject, { cards, project } ) {
        const card = NotificationMessageHelpers.getCard( notification, cards );
        // No uri used here because card might not be transformed
        return card ? `/card/${card.slug || card.id}` : null;
    }

    static getMessage( notification, subject, { cards, project }, html ) {
        if ( !subject || !cards || cards.length == 0 || !project ) {
            return null;
        }

        function getLinkNotificationData( notification, subject, cards, project ) {
            const sourceCard = NotificationMessageHelpers.getCard( notification, cards, 'source' );
            if ( !sourceCard ) {
                return null;
            }
            const destinationCard = NotificationMessageHelpers.getCard( notification, cards, 'destination' );
            if ( !destinationCard ) {
                return null;
            }
            return {
                i18n: {
                    user:     NotificationMessageHelpers.subjectSpan( subject, html ),
                    card:     NotificationMessageHelpers.cardSpan( sourceCard, html ),
                    toCard:   NotificationMessageHelpers.cardSpan( destinationCard, html ),
                    linkType: notification.data.linkType,
                    project:  NotificationMessageHelpers.projectSpan( project, html )
                }
            };
        }

        function getBasicNotificationData( notification, subject, cards, project ) {
            const card = NotificationMessageHelpers.getCard( notification, cards );
            return {
                i18n: {
                    user:    NotificationMessageHelpers.subjectSpan( subject, html ),
                    card:    NotificationMessageHelpers.cardSpan( card, html ),
                    project: NotificationMessageHelpers.projectSpan( project, html )
                }
            };
        }

        let data    = null;
        let message = null;

        switch ( notification.action ) {

            case NotificationTypes.liked:
                data    = getBasicNotificationData( notification, subject, cards, project );
                message = __( '__user__ liked your card __card__ in __project__', data.i18n );
                break;

            case NotificationTypes.pinned:
                data    = getBasicNotificationData( notification, subject, cards, project );
                message = __( '__user__ pinned your card __card__ in __project__', data.i18n );
                break;

            case NotificationTypes.linked:
                data = getLinkNotificationData( notification, subject, cards, project );
                if ( data ) {
                    if ( notification.data.linkType ) {
                        message = __( `notification.card.linked.${notification.data.linkType}`, data.i18n );
                    } else {
                        message = __( '__user__ associated your card __card__ to __toCard__ in __project__', data.i18n );
                    }
                } else {
                    return null;
                }
                break;

            case NotificationTypes.linkedTo:
                data = getLinkNotificationData( notification, subject, cards, project );
                if ( data ) {
                    if ( notification.data.linkType ) {
                        message = __( `notification.card.linkedTo.${notification.data.linkType}`, data.i18n );
                    } else {
                        message = __( '__user__ associated __card__ to your card __toCard__ in __project__', data.i18n );
                    }
                } else {
                    return null;
                }
                break;

            case NotificationTypes.linkedBoth:
                data = getLinkNotificationData( notification, subject, cards, project );
                if ( data ) {
                    if ( notification.data.linkType ) {
                        message = __( `notification.card.linkedBoth.${notification.data.linkType}`, data.i18n );
                    } else {
                        message = __( '__user__ associated your cards __card__ and __toCard__ in __project__', data.i18n );
                    }
                } else {
                    return null;
                }
                break;

            case NotificationTypes.moved:
                const movedCard = NotificationMessageHelpers.getCard( notification, cards );
                if ( !movedCard ) {
                    return null;
                }
                const movedFromPhase = NotificationMessageHelpers.getPhase( notification, project, 'fromPhase' );
                const movedToPhase   = NotificationMessageHelpers.getPhase( notification, project, 'toPhase' );
                const movedI18n      = {
                    user:      NotificationMessageHelpers.subjectSpan( subject, html ),
                    card:      NotificationMessageHelpers.cardSpan( movedCard, html ),
                    fromPhase: NotificationMessageHelpers.phaseSpan( movedFromPhase, html ),
                    toPhase:   NotificationMessageHelpers.phaseSpan( movedToPhase, html ),
                    project:   NotificationMessageHelpers.projectSpan( project, html )
                };
                if ( movedFromPhase && movedToPhase ) {
                    message = __( '__user__ moved your card __card__ from __fromPhase__ to __toPhase__ in __project__', movedI18n );
                } else if ( movedFromPhase ) {
                    message = __( '__user__ moved your card __card__ from __fromPhase__ in __project__', movedI18n );
                } else if ( movedToPhase ) {
                    message = __( '__user__ moved your card __card__ to __toPhase__ in __project__', movedI18n );
                } else {
                    message = __( '__user__ moved your card __card__ in __project__', movedI18n );
                }
                break;

            case NotificationTypes.copied:
                const originalCard = NotificationMessageHelpers.getCard( notification, cards, 'original' );
                if ( !originalCard ) {
                    return null;
                }
                const copiedCard = NotificationMessageHelpers.getCard( notification, cards, 'copy' );
                if ( !copiedCard ) {
                    return null;
                }
                const copiedFromPhase = NotificationMessageHelpers.getPhase( notification, project, 'fromPhase' );
                const copiedToPhase   = NotificationMessageHelpers.getPhase( notification, project, 'toPhase' );
                const samePhase       = notification.data.fromPhase == notification.data.toPhase;
                const copiedI18n      = {
                    user:      NotificationMessageHelpers.subjectSpan( subject, html ),
                    card:      NotificationMessageHelpers.cardSpan( originalCard, html ),
                    copy:      NotificationMessageHelpers.cardSpan( copiedCard, html ),
                    fromPhase: NotificationMessageHelpers.phaseSpan( copiedFromPhase, html ),
                    toPhase:   NotificationMessageHelpers.phaseSpan( copiedToPhase, html ),
                    project:   NotificationMessageHelpers.projectSpan( project, html )
                };
                if ( copiedFromPhase && copiedToPhase && !samePhase ) {
                    message = __( '__user__ copied your card __card__ from __fromPhase__ into __toPhase__ in __project__', copiedI18n );
                } else if ( copiedFromPhase ) {
                    message = __( '__user__ copied your card __card__ from __fromPhase__ in __project__', copiedI18n );
                } else if ( copiedToPhase ) {
                    message = __( '__user__ copied your card __card__ into __toPhase__ in __project__', copiedI18n );
                } else {
                    message = __( '__user__ copied your card __card__ in __project__', copiedI18n );
                }
                break;
        }

        return message;
    }

}
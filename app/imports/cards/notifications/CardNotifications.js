"use strict";

export const NotificationTypes = {
    liked:      'liked',
    linked:     'linked',
    linkedBoth: 'linked-both',
    linkedTo:   'linked-to',
    moved:      'moved',
    copied:     'copied',
    pinned:     'pinned'
};
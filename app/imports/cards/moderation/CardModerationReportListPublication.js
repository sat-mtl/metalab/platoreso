"use strict";

import slugify from "underscore.string/slugify";
import { Counts } from "meteor/tmeasday:publish-counts";
import UserCollection from "/imports/accounts/UserCollection";
import UserHelpers from "/imports/accounts/UserHelpers";
import { RoleGroups } from "/imports/accounts/Roles";
import GroupHelpers from "/imports/groups/GroupHelpers";
import GroupSecurity from "/imports/groups/GroupSecurity";
import ProjectCollection from "../../projects/ProjectCollection";
import PhaseTypes from "../../projects/phases/PhaseTypes";
import CardCollection from "../CardCollection";
import CardImagesCollection from "../images/CardImagesCollection";

/**
 * Moderation Report List (Administration)
 *
 * @params {Object} [options] Options for the find operation (ex: sort)
 */
Meteor.publishComposite( "card/moderation/report/list", function ( query = null, options = null ) {

    // Check query for only the allowed fields
    check( query, Match.OneOf( null, {
        name:      Match.Optional( Match.ObjectIncluding( { $regex: String } ) ),
        project:   Match.Optional( String ),
        author:    Match.Optional( String ),
        moderated: Match.Optional( Boolean )
    } ) );

    // Check options for only the allowed fields
    check( options, Match.OneOf( null, {
        sort:  Match.Optional( Object ),
        limit: Match.Optional( Number ),
        skip:  Match.Optional( Number )
    } ) );

    if ( !GroupSecurity.canModerateGroups( this.userId ) ) {
        return this.ready();
    }

    const topLevelModerator = UserHelpers.isModerator( this.userId );

    query = query || {};

    // !important
    query.current = true;

    // Keep the original (unmodified by permissions, to publish counts to a unique URI)
    const originalQuery = _.clone( query );

    options = _.extend( options || {}, {
        fields: {
            id:          1,
            slug:        1,
            name:        1,
            project:     1,
            author:      1,
            moderated:   1,
            reportCount: 1,
            current:     1
        }
    } );

    let composite = {
        find() {
            if ( !topLevelModerator ) {
                // User is not a top-level moderator so filter cards by projects he/she has access to as a group moderator
                const userGroups = GroupHelpers.getGroupsForUser( this.userId, RoleGroups.moderators );
                const projects   = ProjectCollection.find( { groups: { $in: userGroups } }, { fields: { _id: 1 } } );
                query.project    = { $in: projects.map( project => project._id ) }
            }

            // Slugify the json query as a unique identifier for the count
            Counts.publish( this,
                'card/moderation/report/count/' + slugify( JSON.stringify( originalQuery ) ),
                CardCollection.findReported( query, { fields: { _id: 1 } } ),
                { noReady: true } // Important otherwise the cursor is considered ready before running the next query
            );

            return CardCollection.findReported( query, options )
        },
        children: [
            {
                find( card ) {
                    return CardImagesCollection.find( { owners: card.id } );
                }
            }
        ]
    };

    // If no project was passed, we'll need the card's projects in the publication
    if ( !query.project ) {
        composite.children.push( {
            find( card ) {
                return ProjectCollection.find( { _id: card.project }, { fields: { _id: 1, slug: 1, name: 1 } } );
            }
        } );
    }

    // If no author was passed, we'll need the card's author in the publication
    if ( !query.author ) {
        composite.children.push( {
            find( card ) {
                return UserCollection.find( { _id: card.author }, {
                    fields: {
                        'profile.firstName': 1, 'profile.lastName': 1
                    }
                } );
            }
        } );
    }

    return composite;
} );
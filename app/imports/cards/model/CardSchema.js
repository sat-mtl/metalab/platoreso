"use strict";

import prune from "underscore.string/prune";
import slugify from "underscore.string/slugify";
import { SimpleSchema } from "meteor/aldeed:simple-schema";
import CardCollection from "../CardCollection";
import PhaseVisits from "../phases/PhaseVisits";
import TagsSchema from "../../tags/model/TagsSchema";

const QuestionsDataSchema = new SimpleSchema( {
    answers: {
        type:     Object, // [Answer], questionId:"Given answer"
        blackbox: true
    }
} );
const PollDataSchema      = new SimpleSchema( {
    answers: {
        type:     Object, // [Choice], questionId:answerId
        blackbox: true
    }
} );

/**
 * Card Data Schema
 *
 * @type {SimpleSchema}
 */
const CardDataSchema = new SimpleSchema( {
    questions: {
        type:     QuestionsDataSchema,
        optional: true
    },
    poll:      {
        type:     PollDataSchema,
        optional: true
    }
} );

/**
 * Phase Data Schema
 *
 * @type {SimpleSchema}
 */
const PhaseDataSchema = new SimpleSchema( {
    phase: {
        type: String
    },
    data:  {
        type:     Object,
        optional: true,
        blackbox: true
    }
} );

/**
 * Phase Visit Schema
 *
 * @type {SimpleSchema}
 */
const PhaseVisitSchema = new SimpleSchema( {
    phase:          {
        type: String
    },
    type:           {
        type:          String,
        allowedValues: _.values( PhaseVisits.visitTypes )
    },
    enteredVersion: {
        type: Number
    },
    exitedVersion:  {
        type:     Number,
        optional: true
    },
    enteredAt:      {
        type: Date
    },
    exitedAt:       {
        type:     Date,
        optional: true
    },
    enteredFrom:    {
        type:     String,
        optional: true
    },
    exitedTo:       {
        type:     String,
        optional: true
    }
} );

/**
 * Base Card Schema
 *
 * @type {SimpleSchema}
 */
const CardSchemaBase = new SimpleSchema( [{

    // Meteor _id

    _id: {
        type: String
    },

    // Card Id, this is used because of the history snapshots
    id: {
        type:      String,
        autoValue: function ( card ) {
            if ( !this.isUpdate && !this.isSet ) {
                return Random.id();
            }
        }
    },

    // Versioning

    current: {
        type:         Boolean,
        defaultValue: true
    },

    version: {
        type:         Number,
        defaultValue: 0
    },

    versionedAt: {
        type:      Date,
        autoValue: function () {
            if ( !this.isSet && this.isInsert ) {
                return new Date();
            }
        }
    },

    versionedBy: {
        type:      String,
        autoValue: function () {
            if ( !this.isSet && this.isInsert ) {
                return this.userId;
            }
        }
    },

    // Soft-deleted flag
    deleted: {
        type:         Boolean,
        defaultValue: false
    },

    // Relationships

    author: {
        type: String
    },

    phase: {
        type: String
    },

    project: {
        type: String
    },

    // Card Data

    slug: {
        type: String
    },

    name: {
        type: String,
        min:  3,
        max:  255
    },

    name_sort: {
        type: String
    },

    content: {
        type:         String,
        optional:     true,
        defaultValue: ""
    },

    excerpt: {
        type:      String,
        optional:  true,
        autoValue: function () {
            if ( !this.isSet && this.field( 'content' ).isSet ) {
                return prune( this.field( 'content' ).value || '', 512, '' );
            }
        }
    },

    // Extra data

    data: {
        type:     CardDataSchema,
        optional: true
    },

    // Phase data

    phaseVisits: {
        type:         [PhaseVisitSchema],
        defaultValue: []
    },

    phaseData: {
        //type: [PhaseDataSchema],
        //optional: true,
        //defaultValue: []
        type:     Object,
        optional: true,
        blackbox: true
    },

    // Stats

    viewCount: {
        type:         Number,
        optional:     true,
        defaultValue: 0
    },

    likes: {
        type:         [String],
        optional:     true,
        defaultValue: []
    },

    likeCount: {
        type:         Number,
        optional:     true,
        defaultValue: 0
    },

    attachmentCount: {
        type:         Number,
        optional:     true,
        defaultValue: 0
    },

    // Analytics

    totalTemperature: {
        type:         Number,
        decimal:      true,
        optional:     true,
        defaultValue: 0
    },

    lastTemperature: {
        type:         Number,
        decimal:      true,
        optional:     true,
        defaultValue: 0
    },

    heatedAt: {
        type:      Date,
        optional:  true,
        autoValue: function () {
            if ( !this.isSet && !this.isUpdate ) {
                return new Date();
            }
        }
    },

    // Misc

    pinned: {
        type:         Boolean,
        optional:     true,
        defaultValue: false
    }
},
    TagsSchema
] );

/**
 * Card Schema
 *
 * @type {SimpleSchema}
 */
const CardSchema = new SimpleSchema( [
    CardSchemaBase,
    {
        // Auto-valued field to sort by case-insensitive name
        name_sort: {
            type:      String,
            index:     true,
            autoValue: function () {
                var name = this.field( "name" );
                if ( name.isSet ) {
                    return name.value.toLowerCase();
                } else {
                    this.unset(); // Prevent users from supplying their own value
                }
            }
        },

        slug: {
            type:      String,
            autoValue: function () {
                var name = this.field( "name" );
                let slug = null;
                if ( name.isSet ) {
                    slug = slugify( name.value );
                } else if ( this.isSet ) {
                    slug = slugify( this.value );
                }

                if ( slug != null ) {
                    let count    = 0;
                    let safeSlug = slug;
                    while ( CardCollection.findOne( { id: { $ne: this.docId }, current: true, slug: safeSlug } ) != null ) {
                        count++;
                        safeSlug = slug + ( count > 0 ? count.toString() : '' );
                    }
                    return safeSlug;
                }
            }
        }
    }
] );
export default CardSchema;

/**
 * Card Update Schema
 *
 * @type {SimpleSchema}
 */
export const CardUpdateSchema = new SimpleSchema( [
    {
        name:    {
            type:     String,
            optional: true,
            min:      3,
            max:      255
        },
        content: {
            type:     String,
            optional: true
        }
    },
    CardSchema.pick( [
        'tags',
        'tags.$',
        'phaseData'
        //'publishedAt'
    ] )
] );
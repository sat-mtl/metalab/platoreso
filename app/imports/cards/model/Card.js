"use strict";

import Logger from "meteor/metalab:logger/Logger";
import prune from "underscore.string/prune";
import md from "../../utils/markdown";
import Analytics from "/imports/analytics/Analytics";
import Settings from "/imports/settings/Settings";
import UserCollection from "../../accounts/UserCollection";
import ProjectCollection from "../../projects/ProjectCollection";
import CardCollection from "../CardCollection";
import CardLinksCollection from "../links/CardLinksCollection";
import CardLinkTypes from "../links/CardLinkTypes";
import CardImagesCollection from "../images/CardImagesCollection";
import CardAttachmentsCollection from "../attachments/CardAttachmentsCollection";

const log = Logger( "card" );

/**
 * Card Model
 *
 * @param doc
 * @constructor
 */
export default class Card {

    /**
     * @constructor
     * @param doc
     */
    constructor( doc ) {
        _.extend( this, doc );
    }

    /**
     * Get the card URI
     * This is used to identify the card in URL's
     *
     * @returns {String}
     */
    get uri() {
        let uri = this.slug || this.id;
        if ( !this.current && this.version != null ) {
            uri += `/${this.version}`;
        }
        return uri;
    }

    /**
     * Get the name with markdown rendered
     *
     * @returns {String}
     */
    get nameMarkdown() {
        if ( !this._nameMarkdown ) {
            this._nameMarkdown = md( this.name || '' );
        }
        return this._nameMarkdown;
    }

    /**
     * Checks if we have content (empty string still counts as no content)
     *
     * @returns {boolean}
     */
    get hasContent() {
        return !_.isEmpty( this.content );
    }

    /**
     * Checks if we have an excerpt (empty string still counts as no excerpt)
     *
     * @returns {boolean}
     */
    get hasExcerpt() {
        return !_.isEmpty( this.excerpt );
    }

    /**
     * Get an excerpt as rendered markdown
     * We have to prune BEFORE markdown, otherwise we run the risk of having broken markup.
     *
     * @parms {Number} length Number of characters in the excerpt
     * @returns {String}
     */
    excerptMarkdown( length = 140 ) {
        if ( !this._excerptMarkdown ) {
            this._excerptMarkdown = {};
        }
        if ( !this._excerptMarkdown[length] ) {
            this._excerptMarkdown[length] = md( prune( this.excerpt || this.content || '', length ) );
        }
        return this._excerptMarkdown[length];
    }

    /**
     * Get content as rendered markdown
     *
     * @getter
     * @returns {String}
     */
    get contentMarkdown() {
        if ( !this._contentMarkdown ) {
            this._contentMarkdown = md( this.content || '' );
        }
        return this._contentMarkdown;
    }

    /**
     * Get the last complete phase visit
     *
     * @returns {*}
     */
    get lastPhaseVisit() {
        if ( !this.phaseVisits ) {
            return null;
        }

        // Sort and filter the list
        return this.phaseVisits
                   .filter( visit => visit.exitedVersion != null )
                   .sort( ( a, b ) => b.enteredAt - a.enteredAt )[0];
    }

    /**
     * Squashes phase visits to only keep the most recent one from each phase
     * This is used to find out which card version to show in a phase column
     * when using the history option.
     *
     * @returns {Array}
     */
    get squashedPhaseVisits() {
        if ( !this.phaseVisits ) {
            return [];
        }

        // Keep a list of the visited phases for filtering
        // Current phase is already considered visited
        let visitedPhases = [this.phase];

        // Sort and filter the list
        return this.phaseVisits
                   .sort( ( a, b ) => b.enteredAt - a.enteredAt )
                   .filter( visit => {
                       // Skip current phase
                       if ( visit.exitedVersion == null || visitedPhases.indexOf( visit.phase ) != -1 ) {
                           return false;
                       }
                       visitedPhases.push( visit.phase );
                       return true;
                   } );
    }

    /**
     * Get data for the provided phase id
     *
     * @param {String} phaseId
     * @returns {PhaseDataSchema|Object|null}
     */
    getPhaseData( phaseId ) {
        return this.phaseData ? this.phaseData[phaseId] : null;//this.phaseData.find( pd => pd.phase == phaseId ) : null;
    }

    /**
     * Find author cursor for this card
     *
     * @returns {*}
     */
    findAuthor() {
        return UserCollection.find( { _id: this.author }, {
            fields: {
                _id:                 1,
                points:              1,
                'profile.firstName': 1,
                'profile.lastName':  1,
                'profile.avatarUrl': 1
            }
        } );
    }

    /**
     * Get the author for this card
     *
     * @returns {Meteor.User}
     */
    getAuthor() {
        return UserCollection.findOne( { _id: this.author }, {
            fields: {
                _id:                 1,
                points:              1,
                'profile.firstName': 1,
                'profile.lastName':  1,
                'profile.avatarUrl': 1
            }
        } );
    }

    /**
     * Find project cursor for this card
     *
     * @param {Object} options
     * @returns {*}
     */
    findProject( options = {} ) {
        return ProjectCollection.find( { _id: this.project }, options );
    }

    /**
     * Get the project for this card
     *
     * @param {Object} options
     * @returns {Project}
     */
    getProject( options = {} ) {
        return ProjectCollection.findOne( { _id: this.project }, options );
    }

    /**
     * Find image cursor for this card
     *
     * @param options
     * @returns {*}
     */
    findImage( options = {} ) {
        return CardImagesCollection.find( {
            'owners.id':          this.id,
            'owners.versionFrom': { $lte: this.version },
            $or:                  [
                { 'owners.versionTo': { $exists: false } },
                { 'owners.versionTo': { $gte: this.version } }
            ]
        }, options );
    }

    /**
     * Get the image for this card
     *
     * @param options
     * @returns {*}
     */
    getImage( options = {} ) {
        return CardImagesCollection.findOne( {
            'owners.id':          this.id,
            'owners.versionFrom': { $lte: this.version },
            $or:                  [
                { 'owners.versionTo': { $exists: false } },
                { 'owners.versionTo': { $gte: this.version } }
            ]
        }, options );
    }

    /**
     * Find attachments cursor for this card
     *
     * @param options
     * @returns {*}
     */
    findAttachments( options = {} ) {
        return CardAttachmentsCollection.find( {
            'owners.id':          this.id,
            'owners.versionFrom': { $lte: this.version },
            $or:                  [
                { 'owners.versionTo': { $exists: false } },
                { 'owners.versionTo': { $gte: this.version } }
            ]
        }, options );
    }

    /**
     * Get the attachments for this card
     *
     * @param options
     * @returns {*}
     */
    getAttachments( options = {} ) {
        return this.findAttachments( options ).fetch();
    }

    get linkQuery() {
        if ( this.version == null ) {
            log.error( 'findLinks - version unavailable on card' );
            //throw new Meteor.Error( "version unavailable while trying to find links for a card" );
            return null;
        }

        const query = {
            $or: [
                {
                    'source.id':          this.id,
                    'source.versionFrom': { $lte: this.version },
                    $or:                       [
                        { 'source.versionTo': { $exists: false } },
                        { 'source.versionTo': { $gte: this.version } }
                    ]
                },
                {
                    'destination.id':          this.id,
                    'destination.versionFrom': { $lte: this.version },
                    $or:                       [
                        { 'destination.versionTo': { $exists: false } },
                        { 'destination.versionTo': { $gte: this.version } }
                    ]
                }
            ]
        };

        return query;
    }

    /**
     * Find links for this card
     *
     * @returns {Cursor}
     */
    findLinks( options = {} ) {
        const query = this.linkQuery;
        if ( !query ) {
            return null;
        }

        return CardLinksCollection.find( query, options );
    }

    /**
     * Get the links for this card
     *
     * @param {Object} options
     * @returns {*}
     */
    getLinks( options = {} ) {
        const cursor = this.findLinks( options );
        return cursor ? cursor.fetch() : [];
    }

    /**
     * Find the linked cards for this card
     *
     * @param {Object} options
     * @returns {Cursor|null}
     */
    findLinked( options = {} ) {
        const links = this.getLinks();
        if ( !links.length ) {
            return null;
        }

        return CardCollection.find( { id: { $in: links.map( link => link.source.id ) }, current: true }, options );
    }

    /**
     * Get the linked cards for this card
     * @returns {Array}
     */
    getLinked() {
        const cursor = this.findLinked();
        return cursor ? cursor.fetch() : [];
    }

    _linkExists( query ) {
        const linkQuery = this.linkQuery;
        if ( !linkQuery ) {
            return false;
        }

        const link = CardLinksCollection.findOne(
            _.extend( linkQuery, query ),
            { fields: { _id: 1 }, transform: null }
        );

        return link != null;
    }

    _sourceLinkExists( destination = null, type ) {
        if ( this.id == null ) {
            log.error( '_sourceLinkExists - id unavailable on source card' );
            return false;
        }

        if ( this.version == null ) {
            log.error( '_sourceLinkExists - version unavailable on source card' );
            return false;
        }

        if ( destination && destination.id == null ) {
            log.error( '_sourceLinkExists - id unavailable on destination card' );
            return false;
        }

        if ( destination && destination.version == null ) {
            log.error( '_sourceLinkExists - version unavailable on destination card' );
            return false;
        }

        const sourceQuery = {
            'source.id':          this.id,
            'source.versionFrom': { $lte: this.version },
            $or:                  [
                { 'source.versionTo': { $exists: false } },
                { 'source.versionTo': { $gte: this.version } }
            ]
        };

        let query = {
            type: type
        };

        if ( destination ) {
            _.extend( query, {
                $and: [
                    sourceQuery,
                    {
                        'destination.id':          destination.id,
                        'destination.versionFrom': { $lte: destination.version },
                        $or:                       [
                            { 'destination.versionTo': { $exists: false } },
                            { 'destination.versionTo': { $gte: destination.version } }
                        ]
                    }
                ]
            } );
        } else {
            _.extend( query, sourceQuery )
        }

        const link = CardLinksCollection.findOne(
            query,
            {
                fields:    {
                    _id: 1
                },
                transform: null
            }
        );

        return link != null;
    }

    _destinationLinkExists( source = null, type ) {
        if ( this.id == null ) {
            log.error( '_destinationLinkExists - id unavailable on destination card' );
            return false;
        }

        if ( this.version == null ) {
            log.error( '_destinationLinkExists - version unavailable on destination card' );
            return false;
        }

        if ( source && source.id == null ) {
            log.error( '_destinationLinkExists - id unavailable on source card' );
            return false;
        }

        if ( source && source.version == null ) {
            log.error( '_destinationLinkExists - version unavailable on source card' );
            return false;
        }

        const destinationQuery = {
            'destination.id':          this.id,
            'destination.versionFrom': { $lte: this.version },
            $or:                       [
                { 'destination.versionTo': { $exists: false } },
                { 'destination.versionTo': { $gte: this.version } }
            ]
        };

        let query = {
            type: type
        };

        if ( source ) {
            _.extend( query, {
                $and: [
                    destinationQuery,
                    {
                        'source.id':          source.id,
                        'source.versionFrom': { $lte: source.version },
                        $or:                  [
                            { 'source.versionTo': { $exists: false } },
                            { 'source.versionTo': { $gte: source.version } }
                        ]
                    }
                ]
            } );
        } else {
            _.extend( query, destinationQuery )
        }

        const link = CardLinksCollection.findOne(
            query,
            {
                fields:    {
                    _id: 1
                },
                transform: null
            }
        );

        return link != null;
    }

    /**
     * Verify if this card is linked in any way to another one
     *
     * @param {Object} source
     * @returns {boolean}
     */
    isLinkedTo( source ) {
        if ( this.id == null ) {
            log.error( 'isLinkedTo - id unavailable on destination card' );
            return false;
        }

        if ( this.version == null ) {
            log.error( 'isLinkedTo - version unavailable on destination card' );
            return false;
        }

        if ( source && source.id == null ) {
            log.error( 'isLinkedTo - id unavailable on source card' );
            return false;
        }

        if ( source && source.version == null ) {
            log.error( 'isLinkedTo - version unavailable on source card' );
            return false;
        }

        const link = CardLinksCollection.findOne(
            {
                $and: [
                    {
                        'source.id':          source.id,
                        'source.versionFrom': { $lte: source.version },
                        $or:                  [
                            { 'source.versionTo': { $exists: false } },
                            { 'source.versionTo': { $gte: source.version } }
                        ]
                    },
                    {
                        'destination.id':          this.id,
                        'destination.versionFrom': { $lte: this.version },
                        $or:                       [
                            { 'destination.versionTo': { $exists: false } },
                            { 'destination.versionTo': { $gte: this.version } }
                        ]
                    }
                ]
            },
            {
                fields:    {
                    _id: 1
                },
                transform: null
            }
        );

        return link != null;
    }

    get isReference() {
        return this._sourceLinkExists( null, CardLinkTypes.references.id );
    }

    isReferenceFor( card ) {
        return this._sourceLinkExists( card, CardLinkTypes.references.id );
    }

    get hasReference() {
        return this._destinationLinkExists( null, CardLinkTypes.references.id );
    }

    hasReferenceTo( card ) {
        return this._destinationLinkExists( card, CardLinkTypes.references.id );
    }

    get isOriginal() {
        return this._sourceLinkExists( null, CardLinkTypes.derives.id );
    }

    isOriginalOf( card ) {
        return this._sourceLinkExists( card, CardLinkTypes.derives.id );
    }

    get isDerived() {
        return this._destinationLinkExists( null, CardLinkTypes.derives.id );
    }

    isDerivedFrom( card ) {
        return this._destinationLinkExists( card, CardLinkTypes.derives.id );
    }

    get isComplemented() {
        return this._sourceLinkExists( null, CardLinkTypes.complements.id );
    }

    isComplementedBy( card ) {
        return this._sourceLinkExists( card, CardLinkTypes.complements.id );
    }

    get isComplement() {
        return this._destinationLinkExists( null, CardLinkTypes.complements.id );
    }

    complements( card ) {
        return this._destinationLinkExists( card, CardLinkTypes.complements.id );
    }

    /**
     * Card temperature
     * @type {Number}
     */
    get temperature() {
        if ( this.lastTemperature == null || this.heatedAt == null ) {
            throw new Meteor.Error( 'Missing fields lastTemperature and heatedAt to calculate card temperature' );
        }

        const now = new Date();
        return Analytics.getCurrentTemperature( this.lastTemperature, this.heatedAt, now, Settings.shared.analytics.card.cooldown );
    }

    /**
     * Get object ancestors
     *
     * @returns {*[]}
     */
    get objectAncestors() {
        if ( this.id == null || this.project == null ) {
            throw new Meteor.Error( 'Missing fields id and project to get object ancestors' );
        }

        return [
            {
                id:   this.id,
                type: CardCollection.entityName
            },
            {
                id:   this.project,
                type: ProjectCollection.entityName
            }
        ];
    }
}
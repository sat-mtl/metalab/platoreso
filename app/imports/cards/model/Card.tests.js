"use strict";

import { chai, assert, expect } from 'meteor/practicalmeteor:chai';
import { sinon, spies, stubs } from 'meteor/practicalmeteor:sinon';

import CardCollection from "/imports/cards/CardCollection"; // Must be there before otherwise babel screams at us because circular dependencies
import Card from "/imports/cards/model/Card";
import Settings from "/imports/settings/Settings";

describe( 'cards/CardModel', function () {

    let sandbox;

    beforeEach( () => {
        sandbox = sinon.sandbox.create();
    } );

    afterEach( () => {
        sandbox.restore();
    } );

    describe( 'get squashedPhaseVisits', () => {

        it( 'will return an empty array of there is are no phase visits', () => {
            const card = new Card( {} );
            expect( card.squashedPhaseVisits ).to.eql( [] );
        } );

        it( 'will ignore the non-exited from phases', () => {
            const card = new Card( {
                phaseVisits: [
                    {
                        phase:         'phase1',
                        exitedVersion: 1
                    },
                    {
                        phase: 'phase2'
                    }
                ]
            } );
            expect( card.squashedPhaseVisits ).to.eql( [
                {
                    phase:         'phase1',
                    exitedVersion: 1
                }
            ] );
        } );

        it( 'will ignore the current phases', () => {
            const card = new Card( {
                phase:       'phase2',
                phaseVisits: [
                    {
                        phase:         'phase1',
                        exitedVersion: 1
                    },
                    {
                        phase:         'phase2',
                        exitedVersion: 2
                    }
                ]
            } );
            expect( card.squashedPhaseVisits ).to.eql( [
                {
                    phase:         'phase1',
                    exitedVersion: 1
                }
            ] );
        } );

        it( 'will keep the most recent visit from the same phase if multiple - A', () => {
            const lastDate = new Date( (new Date()).getTime() + 60000 );
            const card     = new Card( {
                phaseVisits: [
                    {
                        phase:         'phase1',
                        enteredAt:     new Date( (new Date()).getTime() - 60000 ),
                        exitedVersion: 1
                    },
                    {
                        phase:         'phase1',
                        enteredAt:     new Date(),
                        exitedVersion: 2
                    },
                    {
                        phase:         'phase1',
                        enteredAt:     lastDate,
                        exitedVersion: 3
                    }
                ]
            } );
            expect( card.squashedPhaseVisits ).to.eql( [
                {
                    phase:         'phase1',
                    enteredAt:     lastDate,
                    exitedVersion: 3
                }
            ] );
        } );

        it( 'will keep the most recent visit from the same phase if multiple - B', () => {
            const lastDate = new Date( (new Date()).getTime() + 60000 );
            const card     = new Card( {
                phaseVisits: [
                    {
                        phase:         'phase1',
                        enteredAt:     lastDate,
                        exitedVersion: 1
                    },
                    {
                        phase:         'phase1',
                        enteredAt:     new Date(),
                        exitedVersion: 2
                    },
                    {
                        phase:         'phase1',
                        enteredAt:     new Date( (new Date()).getTime() - 60000 ),
                        exitedVersion: 3
                    }
                ]
            } );
            expect( card.squashedPhaseVisits ).to.eql( [
                {
                    phase:         'phase1',
                    enteredAt:     lastDate,
                    exitedVersion: 1
                }
            ] );
        } );

    } );

    describe( 'get temperature', () => {

        let now;
        let clock;

        beforeEach( () => {
            now             = new Date();
            clock           = sinon.useFakeTimers( now.getTime() );
            Settings.shared = {
                analytics: {
                    card: { cooldown: 1 }
                }
            };
        } );

        afterEach( () => {
            clock.restore();
        } );

        it( 'should fail without the right fields (none)', () => {
            const card = new Card();
            expect( () => card.temperature ).to.throw( /Missing fields/ );
        } );

        it( 'should fail without the right fields (no lastTemperature)', () => {
            const card = new Card( { heatedAt: new Date() } );
            expect( () => card.temperature ).to.throw( /Missing fields/ );
        } );

        it( 'should fail without the right fields (no heatedAt)', () => {
            const card = new Card( { lastTemperature: 1 } );
            expect( () => card.temperature ).to.throw( /Missing fields/ );
        } );

        it( 'should return the current temperature if set "now"', () => {
            const card = new Card( { lastTemperature: 1, heatedAt: now } );
            expect( card.temperature ).to.equal( 1 );
        } );

        it( 'should return the current temperature if set an hour ago (1 point per hour)', () => {
            Settings.shared.analytics.card.cooldown = 1;
            const card = new Card( { lastTemperature: 1, heatedAt: now - (1000*60*60) } );
            expect( card.temperature ).to.equal( 0 );
        } );

        it( 'should return the current temperature if set an hour ago (0.5 point per hour)', () => {
            Settings.shared.analytics.card.cooldown = 0.5;
            const card = new Card( { lastTemperature: 1, heatedAt: now - (1000*60*60) } );
            expect( card.temperature ).to.equal( 0.5 );
        } );

        it( 'should return the current temperature if set an hour ago (0.1 point per hour)', () => {
            Settings.shared.analytics.card.cooldown = 0.1;
            const card = new Card( { lastTemperature: 1, heatedAt: now - (1000*60*60) } );
            expect( card.temperature ).to.equal( 0.9 );
        } );

        it( 'should return a temperature lower than 0', () => {
            Settings.shared.analytics.card.cooldown = 100;
            const card = new Card( { lastTemperature: 1, heatedAt: now - (1000*60*60) } );
            expect( card.temperature ).to.equal( -99 );
        } );

    } );
} );
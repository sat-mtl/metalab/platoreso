"use strict";

import { chai, assert, expect } from 'meteor/practicalmeteor:chai';
import { sinon, spies, stubs } from 'meteor/practicalmeteor:sinon';

import CardCollection from "/imports/cards/CardCollection"; // Must be there before otherwise babel screams at us because circular dependencies
import CardFilesHelpers from "/imports/cards/files/CardFilesHelpers";

describe( 'cards/files/CardsFilesHelpers', function () {

    let sandbox;

    beforeEach( () => {
        sandbox = sinon.sandbox.create();
    } );

    afterEach( () => {
        sandbox.restore();
    } );

    describe( "commitPending", () => {

        let collection;

        beforeEach( ()=> {
            sandbox.stub( CardCollection, "snapshotAndUpdate" );
            sandbox.stub( CardFilesHelpers, "commitLastVersion" );
            collection = {
                name:         "stub",
                update:       sinon.stub(),
                fetchPending: sinon.stub(),
                config:       { replacePrevious: false }
            };
        } );

        function expectEarlyReturn() {
            expect( collection.fetchPending.called ).to.be.false;
            expectNoPendingFiles();
        }

        function expectNoPendingFiles() {
            expect( CardFilesHelpers.commitLastVersion.called ).to.be.false;
            expect( CardCollection.snapshotAndUpdate.called ).to.be.false;
            expect( collection.update.called ).to.be.false;
        }

        it( "Should throw if missing collection", () => {
            expect( ()=>CardFilesHelpers.commitPending() ).to.throw( /Match error/ );
            expectEarlyReturn();
        } );

        it( "Should throw if missing userId", () => {
            expect( ()=>CardFilesHelpers.commitPending( collection ) ).to.throw( /Match error/ );
            expectEarlyReturn();
        } );

        it( "Should throw if missing card", () => {
            expect( ()=>CardFilesHelpers.commitPending( collection, "userId" ) ).to.throw( /Match error/ );
            expectEarlyReturn();
        } );

        it( "Should throw if missing card id", () => {
            const card = {};
            expect( ()=>CardFilesHelpers.commitPending( collection, 'userId', card ) ).to.throw( /Match error/ );
            expectEarlyReturn();
        } );

        it( "Should throw if missing card version", () => {
            const card = { id: "cardId" };
            expect( ()=>CardFilesHelpers.commitPending( collection, 'userId', card ) ).to.throw( /Match error/ );
            expectEarlyReturn();
        } );

        it( "Should return early if no pending files found (empty array)", () => {
            collection.fetchPending.returns( [] );

            const card = { id: "cardId", version: 0 };
            expect( ()=>CardFilesHelpers.commitPending( collection, 'userId', card ) ).not.to.throw();

            assert( collection.fetchPending.calledWith( "cardId" ) );
            expectNoPendingFiles();
        } );

        it( "Should return early if no pending files found (null)", () => {
            collection.fetchPending.returns( null );

            const card = { id: "cardId", version: 0 };
            expect( ()=>CardFilesHelpers.commitPending( collection, 'userId', card ) ).not.to.throw();

            assert( collection.fetchPending.calledWith( "cardId" ) );
            expectNoPendingFiles();
        } );

        describe( "when pending files are available", () => {

            beforeEach( () => {
                collection.fetchPending.returns( [
                    {
                        _id:    "fileId",
                        owners: [
                            {
                                id: "cardId"
                            }
                        ]
                    }
                ] );
            } );

            it( "Should make a snapshot if told to do so (no version)", () => {
                const card = { id: "cardId", version: 1 };
                CardFilesHelpers.commitPending( collection, 'userId', card );
                assert( CardCollection.snapshotAndUpdate.called );
                assert( CardCollection.snapshotAndUpdate.calledWith( "userId", "cardId" ) );
            } );

            it( "Should make a snapshot if told to do so (null version)", () => {
                const card = { id: "cardId", version: 1 };
                CardFilesHelpers.commitPending( collection, 'userId', card, null );
                assert( CardCollection.snapshotAndUpdate.called );
                assert( CardCollection.snapshotAndUpdate.calledWith( "userId", "cardId" ) );
            } );

            it( "Should not make a snapshot if told to not do so", () => {
                const card = { id: "cardId", version: 1 };
                CardFilesHelpers.commitPending( collection, 'userId', card, 1 );
                expect( CardCollection.snapshotAndUpdate.called ).to.be.false;
            } );

            it( "Should not commit the last version of the card file by default", () => {
                const card = { id: "cardId", version: 1 };
                CardFilesHelpers.commitPending( collection, 'userId', card );
                assert.isFalse( CardFilesHelpers.commitLastVersion.called );
            } );

            it( "Should commit the last version of the card file to the current version", () => {
                collection.config.replacePrevious = true;
                const card                        = { id: "cardId", version: 1 };
                CardFilesHelpers.commitPending( collection, 'userId', card );
                assert( CardFilesHelpers.commitLastVersion.calledWith( collection, card ) );
            } );

            it( "Should update the pending files to the correct version with no snapshot", () => {
                const card = { id: "cardId", version: 1 };

                CardFilesHelpers.commitPending( collection, 'userId', card, 3 );

                expect( collection.update.callCount ).to.equal( 1 );
                assert( collection.update.calledWith( {
                    _id: "fileId"
                }, {
                    $set: {
                        owners:  [
                            {
                                id:          "cardId",
                                versionFrom: 3
                            }
                        ],
                        pending: false
                    }
                } ) );
            } );

            it( "Should update the pending files to the correct version with snapshot previously made", () => {
                const card = { id: "cardId", version: 1 };
                CardCollection.snapshotAndUpdate.returns( 2 );

                CardFilesHelpers.commitPending( collection, 'userId', card );

                expect( collection.update.callCount ).to.equal( 1 );
                assert( collection.update.calledWith( {
                    _id: "fileId"
                }, {
                    $set: {
                        owners:  [
                            {
                                id:          "cardId",
                                versionFrom: 2
                            }
                        ],
                        pending: false
                    }
                } ) );
            } );

        } )
    } );

    describe( "commitLastVersion", () => {

        let collection;
        let fetch;

        beforeEach( () => {
            fetch      = sinon.stub();
            collection = { name: "stub", find: sinon.stub().returns( { fetch } ), update: sinon.stub() };
        } );

        it( "should throw if no collection", () => {
            expect( ()=> CardFilesHelpers.commitLastVersion() ).to.throw( /Match error/ );
            assert.isFalse( collection.find.called );
            assert.isFalse( collection.update.called );
        } );

        it( "should throw if no card", () => {
            expect( ()=> CardFilesHelpers.commitLastVersion( collection ) ).to.throw( /Match error/ );
            assert.isFalse( collection.find.called );
            assert.isFalse( collection.update.called );
        } );

        it( "should do nothing if no previous file found", () => {
            fetch.returns( [] );
            CardFilesHelpers.commitLastVersion( collection, { id: "cardId", version: 0 } );
            assert( collection.find.calledWith( {
                    'pending':            false,
                    'owners.id':          "cardId",
                    'owners.versionFrom': { $lte: 0 },
                    'owners.versionTo':   { $exists: false }
                }, { fields: { _id: 1 }, transform: null }
            ) );
            assert.isFalse( collection.update.called );
        } );

        it( "should update the previous file with the current card version", () => {
            fetch.returns( [{ _id: "fileId" }] );
            CardFilesHelpers.commitLastVersion( collection, { id: "cardId", version: 4 } );
            assert( collection.find.calledWith( {
                    'pending':            false,
                    'owners.id':          "cardId",
                    'owners.versionFrom': { $lte: 4 },
                    'owners.versionTo':   { $exists: false }
                }, { fields: { _id: 1 }, transform: null }
            ) );
            assert( collection.update.calledWith( {
                _id:         { $in: ["fileId"] },
                'owners.id': "cardId"
            }, {
                $set: {
                    'owners.$.versionTo': 4
                }
            }, { multi: true } ) );
        } );

        it( "should update the previous files with the current card version", () => {
            fetch.returns( [{ _id: "fileId1" }, { _id: "fileId2" }] );
            CardFilesHelpers.commitLastVersion( collection, { id: "cardId", version: 4 } );
            assert( collection.find.calledWith( {
                    'pending':            false,
                    'owners.id':          "cardId",
                    'owners.versionFrom': { $lte: 4 },
                    'owners.versionTo':   { $exists: false }
                }, { fields: { _id: 1 }, transform: null }
            ) );
            assert( collection.update.calledWith( {
                _id:         { $in: ["fileId1", "fileId2"] },
                'owners.id': "cardId"
            }, {
                $set: {
                    'owners.$.versionTo': 4
                }
            }, { multi: true } ) );
        } );

        it( "should support having the ids of files to update passed as parameters", () => {
            fetch.returns( [{ _id: "fileId" }] );
            CardFilesHelpers.commitLastVersion( collection, { id: "cardId", version: 4 }, ['file1', 'file2'] );
            assert( collection.find.calledWith( {
                    '_id':                { $in: ['file1', 'file2'] },
                    'pending':            false,
                    'owners.id':          "cardId",
                    'owners.versionFrom': { $lte: 4 },
                    'owners.versionTo':   { $exists: false }
                }, { fields: { _id: 1 }, transform: null }
            ) );
            assert( collection.update.calledWith( {
                _id:         { $in: ["fileId"] },
                'owners.id': "cardId"
            }, {
                $set: {
                    'owners.$.versionTo': 4
                }
            } ) );
        } );

    } );

} );
"use strict";

import Logger from "meteor/metalab:logger/Logger";
import CardCollection from "../CardCollection";

const log = Logger( 'card-files-helpers' );

/**
 * Helper Class for dealing with Card Files
 * Since CollectionFS already uses a transform we can't put
 * these methods directly on the file file class
 */
export default class CardFilesHelpers {

    /**
     * Commit pending files
     * This will create a card snapshot beforehand if snapshot parameter is true.
     *
     * @param {Mongo.Collection|Array<Mongo.Collection>} collections
     * @param {String} userId
     * @param {Card} card
     * @param {Number} currentVersion Current version of the card to attach files to,
     *                                If omitted, a snapshot of the card will be taken before attaching
     */
    static commitPending( collections, userId, card, currentVersion = null ) {
        check( collections, Match.OneOf( Match.Any, [Match.Any] ) ); //Actually it should be a Mongo.Collection
        check( userId, String );
        check( card, Match.ObjectIncluding( {
            id:     String,
            version: Number
        } ) );
        check( currentVersion, Match.OneOf( null, undefined, Number ) );

        if ( !_.isArray( collections ) ) {
            collections = [collections];
        }

        collections.forEach( collection => {
            const pendingFiles = collection.fetchPending( card.id );
            if ( !pendingFiles || !pendingFiles.length ) {
                log.verbose( `commitPending - No pending ${collection.name} found for card ${card.id}` );
                return;
            }

            if ( collection.config.replacePrevious ) {
                log.verbose( `commitPending - Freezing last version of previous ${collection.name}` );
                CardFilesHelpers.commitLastVersion( collection, card );
            }

            // If currentVersion is null make a snapshot to increment version number
            if ( currentVersion == null ) {
                log.verbose( `commitPending - Making a snapshot before committing ${collection.name}` );
                currentVersion = CardCollection.snapshotAndUpdate( userId, card.id );
            }

            // This is set up as a forEach
            // but will actually only handle one file at the time in the current implementation
            // Pending files are new so initialize with versionFrom
            log.verbose( `commitPending - Committing pending ${collection.name} for card ${card.id}` );
            pendingFiles.forEach( file => {
                const owners = file.owners.map( owner => ({
                    id:          card.id,
                    versionFrom: currentVersion
                }) );

                log.debug( `commitPending - Updating ${collection.name} owners and pending state`, owners );
                collection.update( {_id: file._id}, {$set: {owners: owners, pending: false}} );
            } );
        } );
        
        return currentVersion;
    }

    /**
     * Sets the last version a file was attached to its owner on.
     * Since we version cards, we never remove files but merely
     * keep a range of versions from and to which they were attached
     * to the card.
     *
     * @param {Mongo.Collection|Array<Mongo.Collection>} collections
     * @param {Card} card
     * @param {String|Array<String>} [fileIds] When we want to target a specific file
     * @returns {Boolean} Is a snapshot of the card required after this commit?
     */
    static commitLastVersion( collections, card, fileIds = null ) {
        check( collections, Match.OneOf( Match.Any, [Match.Any] )); //Actually it should be a Mongo.Collection
        check( card, Match.ObjectIncluding( {
            id:     String,
            version: Number
        } ) );
        check( fileIds, Match.OneOf( null, String, [String] ) );

        if ( !_.isArray( collections ) ) {
            collections = [collections];
        }

        let needSnapshot = false;

        collections.forEach( collection => {
            let query = {
                'pending':            false,
                'owners.id':          card.id,
                'owners.versionFrom': {$lte: card.version},
                'owners.versionTo':   {$exists: false}
            };

            if ( fileIds ) {
                query._id = _.isArray(fileIds) ? { $in: fileIds } : fileIds;
            }

            // Check for a previous file
            const previousFiles = collection.find( query, {fields: {_id: 1}, transform: null} ).fetch();

            // If we have a previous file, set its owner's final version
            if ( previousFiles.length ) {
                const previousFileIds = previousFiles.map( file => file._id );
                log.debug( `commitLastVersion - Previous ${collection.name} found, updating owner's last version to ${card.version}`, previousFileIds );
                collection.update( {
                    _id:         {$in: previousFileIds},
                    'owners.id': card.id
                }, {
                    $set: {
                        'owners.$.versionTo': card.version
                    }
                }, {multi: true} );

                needSnapshot = true;
            }
        } );

        return needSnapshot;
    }

    /**
     * Before insert helper
     * Should be called bound to the collection it has to manipulate
     * Example: CardFilesHelpers.beforeInsert.apply(CardImagesCollection)
     *
     * @param userId
     * @param file
     * @returns {boolean}
     */
    static beforeInsert( userId, file ) {
        check( userId, String );
        check( file, Match.ObjectIncluding( {
            card: String
        } ) );

        if ( !file.card ) {
            log.warn( `Trying to insert a ${this.name} without an associated card`, file );
            return false;
        }

        const card = CardCollection.findOne( {id: file.card, current: true}, {fields: {id: 1, version: 1}} );
        if ( !card ) {
            log.warn( `Card not found while trying to insert a ${this.name}`, file );
            return false;
        }

        // This does not go into the db, it was only there for us
        delete file.card;

        const newCard = file.newCard;
        delete file.newCard;

        if ( newCard ) {
            // The card is new, and since on new cards we upload after creating the card
            // we can directly assign the file to the card here
            log.silly( `Associating ${this.name} to new card version 0` );
            file.pending = false;
            file.owners  = [{
                id:          card.id,
                versionFrom: card.version
            }];
        } else {
            // This is a new file for an existing card, set as pending because we can't know for sure
            // The old & new card versions from here
            log.silly( `Card already exists, setting ${this.name} as pending, will process versioning when updating the card` );
            file.pending = true;
            file.owners  = [{id: card.id}];
        }

        log.verbose( `Adding ${this.name} to card ${card.id}`, file );
        return true;
    }
}

"use strict";

import ProjectCollection from "../../projects/ProjectCollection";
import ProjectSecurity from "../../projects/ProjectSecurity";
import CardCollection from "../CardCollection";
import FeedCollection from "../../feed/FeedCollection";

/**
 * Card (Public)
 */
Meteor.publishComposite( "feed/card/story", function ( storyId, includeRelated ) {
    check( storyId, String );
    check( includeRelated, Boolean );

    const story = FeedCollection.findOne( { _id: storyId }, { fields: { objects: 1 } } );
    if ( !story ) {
        return this.ready();
    }

    // Check the project directly, it skips a call to DB
    if ( !ProjectSecurity.canRead( this.userId, story.getObjectIdByType( ProjectCollection.entityName ) ) ) {
        return this.ready();
    }

    let cardChildren = [
        {
            find( card ){
                return ProjectCollection.find( { _id: card.project }, {
                    fields: {
                        _id:           1,
                        slug:          1,
                        name:          1,
                        'phases._id':  1,
                        'phases.name': 1
                    }
                } );
            }
        }
    ];

    if ( includeRelated ) {
        cardChildren.push( {
            find( card ) {
                return card.findAuthor();
            }
        } );
        cardChildren.push( {
            find( card ) {
                return card.findImage();
            }
        } );
    }

    function findBasic( cards, current ) {
        return CardCollection.find( { id: { $in: cards }, current }, {
            fields: _.extend( {
                id:     1,
                slug:    1,
                name:    1,
                version: 1
            } )
        } );
    }

    function findRelated( cards, current ) {
        return CardCollection.find( { id: { $in: cards }, current }, {
            fields: _.extend( {
                id:       1,
                slug:      1,
                project:   1,
                author:    1,
                name:      1,
                content:   1,
                createdAt: 1,
                version:   1
            } )
        } );
    }

    return {
        find() {
            return FeedCollection.find( { _id: storyId }, { fields: { objects: 1 } } );
        },
        children: [
            {
                find( story ) {
                    // Regular cards
                    const cards = story.getObjectIdsByType( CardCollection.entityName, null, false );
                    if ( includeRelated ) {
                        return findRelated( cards, true );
                    } else {
                        return findBasic( cards, true );
                    }
                },
                children: cardChildren
            },
            /*{
                find( story ) {
                    // Archived cards
                    const cards = story.getObjectIdsByType( CardCollection.entityName, null, true );
                    if ( includeRelated ) {
                        return findRelated( cards, false );
                    } else {
                        return findBasic( cards, false );
                    }
                },
                children: cardChildren
            }*/
        ]
    };
} );


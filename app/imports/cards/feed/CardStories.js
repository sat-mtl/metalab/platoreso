"use strict";

/**
 * Card Story Type Constants
 * @type {{created: string, updated: string, liked: string, unliked: string, linked: string, moved: string, copied: string}}
 */
export const StoryTypes = {
    created:  'created',
    updated:  'updated',
    liked:    'liked',
    unliked:  'unliked',
    linked:   'linked',
    unlinked: 'unlinked',
    moved:    'moved',
    copied:   'copied',
    pinned:   'pinned',
    unpinned: 'unpinned',
};
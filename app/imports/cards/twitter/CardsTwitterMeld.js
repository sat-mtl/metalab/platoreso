"use strict";

import UserCollection from "/imports/accounts/UserCollection";
import AccountsMeldHelper from "/imports/accounts/server/AccountsMeldHelper";
import CardsTwitterHelpers from "./CardsTwitterHelpers";

/**
 * Service added callback
 * Track cards that were created from tweets to reassign them to their original author.
 */
AccountsMeldHelper.onServiceAdded( ( user_id, service_name ) => {
    if ( service_name != 'twitter' ) {
        return;
    }

    const user = UserCollection.findOne( { _id: user_id }, { fields: { _id: 1, 'services.twitter.id': 1 } } );
    if ( !user || !user.services || !user.services.twitter || !user.services.twitter.id ) {
        return;
    }

    CardsTwitterHelpers.relink(user.services.twitter.id, user._id);
} );
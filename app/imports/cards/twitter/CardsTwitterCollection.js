"use strict";

import CardCollection from "../CardCollection";
import TweetSchema from "../../twitter/model/schema/TweetSchema";

// Add a tweet field to the card schema
CardCollection.attachSchema( {
   tweet: {
       type:     TweetSchema,
       optional: true
   }
} );
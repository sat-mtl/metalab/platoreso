"use strict";

import CardCollection from "../CardCollection";

export default class CardsTwitterHelpers
{
    /**
     * Relink a card made from a tweet to a new user
     * Normally used to link cards to their original authors if the user signs up after the fact.
     *
     * @param twitterId
     * @param userId
     */
    static relink( twitterId, userId ) {
        CardCollection.update(
            {'tweet.user.id_str': twitterId, 'author': {$ne: userId}},
            {$set: {'author': userId}},
            {multi: true}
        );
    };
}
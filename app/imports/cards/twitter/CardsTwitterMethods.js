"use strict";

import Logger from "meteor/metalab:logger/Logger";

import UserCollection from "/imports/accounts/UserCollection";
import Audit from "../../audit/Audit";
import TweetCollection from "../../twitter/TweetCollection"
import ProjectCollection from "../../projects/ProjectCollection";
import CardCollection from "../CardCollection";
import CardMethods from "../CardMethods";
import CardSecurity from "../CardSecurity";
import { CardAuditMessages } from "../CardAudit";

const log = Logger( "card-twitter-methods" );

/**
 * Create card from tweet
 *
 * @params {String} tweetId
 * @params {String} projectId
 * @params {String} phaseId
 */
CardMethods.createFromTweet = new ValidatedMethod( {
    name:     'pr/card/createFromTweet',
    validate: new SimpleSchema( {
        tweetId:   { type: String },
        projectId: { type: String },
        phaseId:   { type: String }
    } ).validator(),
    run( { tweetId, projectId, phaseId } ) {

        // PREREQUISITES

        // We don't want transforms as we are injecting into card and simple schema refuses constructors
        const tweet = TweetCollection.findOne( { _id: tweetId }, { transform: null } );
        if ( !tweet ) {
            throw new Meteor.Error( 'tweet not found' );
        }

        const user = UserCollection.findOne( { 'services.twitter.id': tweet.user.id_str }, { fields: { _id: 1 } } );
        
        let tweetCard = {
            author:  user ? user._id : this.userId,
            project: projectId,
            phase:   phaseId,
            image:   null, //TODO: Get/Upload tweet image
            tweet:   tweet,
            name:    tweet.text,
            content: `[@${tweet.user.screen_name}](https://twitter.com/${tweet.user.screen_name})`
        };

        // SECURITY

        if ( !CardSecurity.canCreate( this.userId, tweetCard ) ) {
            log.warn( `create - ${this.userId} is not authorized to create card from tweet`, tweetCard );
            throw new Meteor.Error( 'unauthorized' );
        }

        // UPDATE

        const project = ProjectCollection.findOne( { _id: tweetCard.project }, { fields: { _id: 1, 'phases._id': 1 } } );
        if ( !project ) {
            throw new Meteor.Error( 'project not found' );
        }

        const phase = project.phases.find( phase => phase._id == tweetCard.phase );
        if ( !phase ) {
            throw new Meteor.Error( 'phase not found' );
        }

        // Insert
        log.verbose( "create - Creating card", tweetCard );

        // Generate an Id, stolen from Meteor mongo package
        // Using Random.id() created two different cards on server and client
        // So the ui flickered while it was syncing
        const cardId = CardCollection._makeNewID();
        tweetCard.id   = cardId;
        CardCollection.insert( tweetCard );

        // AUDIT

        Audit.next( {
            domain:  CardCollection.entityName,
            message: CardAuditMessages.added,
            userId:  this.userId,
            cardId:  cardId
        } );

        return cardId;
    }
} );
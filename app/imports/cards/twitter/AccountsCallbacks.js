"use strict";

import CardsTwitterHelpers from "./CardsTwitterHelpers";

/**
 * If the new user has a twitter profile, find cards made from one of their tweet and take ownership.
 */
Accounts.validateNewUser( user => {
    if ( user.services.twitter && user.services.twitter.id) {
        CardsTwitterHelpers.relink(user.services.twitter.id, user._id);
    }
    return true;
} );
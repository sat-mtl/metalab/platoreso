"use strict";

export const CardAuditMessages = {
    added: "added",
    updated: "updated",
    archived: "archived",
    removed: "removed",
    liked: "liked",
    unliked: "unliked",
    linked: "linked",
    unlinked: "unlinked",
    moved: "moved",
    copied: "copied",
    pinned: "pinned",
    unpinned: "unpinned"
};
"use strict";

import ProjectCollection from "../../../projects/ProjectCollection";
import ProjectSecurity from "../../../projects/ProjectSecurity";
import CardCollection from "../../CardCollection";
import CommentCollection from "../../../comments/CommentCollection";
import FeedCollection from "../../../feed/FeedCollection";

/**
 * Card Comment Feed Story
 */
Meteor.publishComposite( "card/comment/feed/story", function ( storyId, includeRelated = false ) {
    check( storyId, String );
    check( includeRelated, Boolean );

    const story = FeedCollection.findOne( { _id: storyId }, { fields: { objects: 1, data: 1 } } );
    if ( !story ) {
        return this.ready();
    }

    // Check the project directly, it skips a call to DB
    if ( !ProjectSecurity.canRead( this.userId, story.getObjectIdByType( ProjectCollection.entityName ) ) ) {
        return this.ready();
    }

    let cardChildren = [
        {
            find( card ){
                return ProjectCollection.find( { _id: card.project }, {
                    fields: {
                        _id:  1,
                        slug: 1,
                        name: 1
                    }
                } );
            }
        }
    ];

    if ( includeRelated ) {
        cardChildren.push( {
            find( card ) {
                return card.findAuthor();
            }
        } );
        cardChildren.push( {
            find( card ) {
                return card.findImage()
            }
        } );
    }

    return {
        find() {
            return CommentCollection.find( { _id: story.data.comment }, {
                fields: {
                    _id:     1,
                    objects: 1,
                    post:    1,
                    author:  1
                }
            } );
        },
        children: [
            {
                find( comment ) {
                    const card = story.getObjectByType( CardCollection.entityName, null );
                    return CardCollection.find( { id: comment.entityId, current: !card.archived }, {
                        fields: includeRelated
                                    ? {
                            _id:       1,
                            id:        1,
                            slug:      1,
                            project:   1,
                            author:    1,
                            name:      1,
                            content:   1,
                            createdAt: 1,
                            current:   1,
                            version:   1
                        }
                                    : {
                            _id:     1,
                            id:      1,
                            slug:    1,
                            name:    1,
                            current: 1,
                            version: 1
                        },
                        sort:   { version: -1 },
                        limit:  1
                    } )
                },
                children: cardChildren
            }
        ]
    }
} );


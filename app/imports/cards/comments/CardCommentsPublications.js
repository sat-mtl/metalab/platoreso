"use strict";

import UserCollection from "/imports/accounts/UserCollection";
import CardSecurity from "../CardSecurity";
import CardCollection from "../CardCollection";
import CommentCollection from "../../comments/CommentCollection";
import CommentPublicationHelpers from "../../comments/CommentPublicationHelpers";

/**
 * Card Comments
 *
 * NOTE: Do not search for a subscription in the code, the name is computed dynamically
 */
Meteor.publishComposite( 'card/comments', function ( cardId, queryParams = {} ) {
    check( cardId, String );
    check( queryParams, Match.OneOf( null, Match.ObjectIncluding( {
        maxDate: Match.Optional( Date )
    } ) ) );

    if ( !CardSecurity.canRead( this.userId, cardId ) ) {
        return this.ready();
    }

    return {
        find() {
            let query = {
                'objects.0.type': CardCollection.entityName,
                'objects.0.id':   cardId,
                //moderated:       false // We still have to show them as moderated
            };

            if ( queryParams && queryParams.maxDate ) {
                query.createdAt = { $lte: queryParams.maxDate };
            }

            return CommentCollection.find( query, {
                fields: CommentPublicationHelpers.getCommentListFields( this.userId )
            } );
        },
        children: [
            {
                find( comment ) {
                    return UserCollection.find( { _id: comment.author }, {
                        fields: {
                            _id:                 1,
                            points:              1,
                            'profile.firstName': 1,
                            'profile.lastName':  1,
                            'profile.avatarUrl': 1
                        }
                    } );
                }
            }
        ]
    };
} );
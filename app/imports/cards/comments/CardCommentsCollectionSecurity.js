"use strict";

import ProjectHelpers from "../../projects/ProjectHelpers";
import ProjectSecurity from "../../projects/ProjectSecurity";
import CommentCollection from "../../comments/CommentCollection";
import CardCollection from "../CardCollection";
import CardSecurity from "../CardSecurity";

/**
 * Security Rules
 *
 * The comments package should not know about the concrete entities it allows commenting on and
 * those entities don't have to know that that can be commented on. So here we define the explicit rules
 * for each entity type this umbrella package knows about
 */

/**
 * Combines both the canContribute and phaseIsActive helpers to know if the user has access to the card
 */
Security.defineMethod( "ifHasAccessToCard", {
    fetch: [],
    //transform: null, // We want transform for entityType and entityId getters
    deny:  function ( type, arg, userId, doc, fields, modifier ) {
        const card = CardCollection.findOne( { id: doc.entityId, current: true }, { fields: { project: 1, phase: 1 } } );
        if ( !card ) {
            return true; // Deny
        }
        const canContribute = ProjectSecurity.canContribute( userId, card.project );
        const phaseIsActive = ProjectHelpers.phaseIsActive( card.project, card.phase );
        return !( canContribute && phaseIsActive );
    }
} );

// COMMENTING

CommentCollection.permit( ['insert'] )
                 .onlyProps( ['objects', 'parent', 'author', 'post'] )
                 .ifLoggedIn()
                 .ifEntityTypeIs( 'card' )
                 .ifHasAccessToCard();

// LIKING

CommentCollection.permit( ['update'] )
                 .onlyProps( ['likes', 'likeCount' ] )
                 .ifLoggedIn()
                 .ifEntityTypeIs( 'card' )
                 .ifHasAccessToCard();

// MODERATION

Security.defineMethod( "ifCanModerateCardComment", {
    fetch: ['objects'],
    transform: null,
    allow: function ( type, arg, userId, comment, fields, modifier ) {
        return comment && CardSecurity.canModerate( userId, comment.objects[0].id );
    }
} );

CommentCollection
    .permit( ['update'] )
    .onlyProps( ['post', 'moderated', 'reportCount', 'reportedBy'] )
    .ifLoggedIn()
    .ifEntityTypeIs( 'card' )
    .ifCanModerateCardComment();

CommentCollection
    .permit( ['remove'] )
    .ifLoggedIn()
    .ifEntityTypeIs( 'card' )
    .ifCanModerateCardComment();
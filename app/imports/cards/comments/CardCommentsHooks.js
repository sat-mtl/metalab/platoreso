"use strict";

import Logger from "meteor/metalab:logger/Logger";
import {Job} from "meteor/vsivsi:job-collection";
import JobCollection from "/imports/jobs/JobCollection";
import NotificationCollection from "/imports/notifications/NotificationCollection";
import UserCollection from "../../accounts/UserCollection";
import UserHelpers from "../../accounts/UserHelpers";
import CardCollection from "../CardCollection";
import CardAnalytics from "../analytics/CardAnalytics";
import Gamification from "../../gamification/Gamification";
import CommentCollection from "../../comments/CommentCollection";
import {CardCommentNotificationTypes} from "./notifications/CardCommentNotifications";
import {StoryTypes} from "../../comments/feed/CommentStories";
import FeedCollection from "/imports/feed/FeedCollection";
import FeedHelpers from "../../feed/FeedHelpers";
import Settings from "../../settings/Settings";

const log = Logger( "card-comments-hooks" );

/**
 * Card Collection Hooks
 */

CardCollection.after.remove( function ( userId, card ) {
    // Defer, so that methods can return without waiting
    Meteor.defer( () => {
        log.debug( `Card "${card.name}" (${card._id}) removed, cleaning up relations` );

        // Remove Comments
        CommentCollection.removeAllForObject( card.id, CardCollection.entityName );

    } );
} );

/**
 * Comment Collection Hooks
 * Watches card comments being added or modified to create feed stories
 */

/**
 * Get the card with the required information for the feed
 * @param {String} id Card Id
 * @returns {Card}
 */
function getCard( id ) {
    return CardCollection.findOne( { id: id, current: true }, {
        fields: {
            id:      1,
            phase:   1,
            project: 1
        }
    } );
}

/**
 * Get the objects array for the passed comment
 *
 * @param {Comment} comment
 * @returns {Array}
 */
function getObjects( comment ) {
    const card = getCard( comment.entityId );

    return [
        {
            id:   card.id,
            type: 'card'
        },
        {
            id:   card.phase,
            type: 'phase'
        },
        {
            id:   card.project,
            type: 'project'
        }
    ];
}

/**
 * Comment after insert hook
 * Checks if the comment was for a card and
 * creates feed stories from the comments
 */
CommentCollection.after.insert( function ( userId, comment ) {
    Meteor.defer( () => {

        comment = this.transform();

        // If there is no user id (server insert)
        // or the entity commented on is not a card
        // then return and don't generate stories
        if ( !userId || comment.entityType != CardCollection.entityName ) {
            return;
        }

        log.verbose( `Comment ${comment._id} inserted` );

        // FEED

        FeedCollection.recordStory( {
            internal: false,
            subject:  userId,
            action:   comment.parent ? StoryTypes.replied : StoryTypes.commented,
            objects:  getObjects( comment ),
            data:     {
                comment: comment._id
            }
        } );

        // We need information from the card commented on...
        const card = CardCollection.findOne( { id: comment.entityId, current: true }, {
            fields:    {
                id:              1,
                slug:            1,
                name:            1,
                author:          1,
                phase:           1,
                project:         1,
                likes:           1,
                lastTemperature: 1,
                heatedAt:        1
            },
            transform: null
        } );
        if ( !card ) {
            log.warn( `Could not find card ${comment.entityId} for card comment after insert hook.` );
            return;
        }

        // CARD HEAT + REPUTATION POINTS (NOT GIVING POINTS FOR SELF COMMENTS)
        if ( userId != card.author ) {
            CardAnalytics.heat( card, Settings.shared.analytics.card.heat.comment );
            Gamification.awardPoints( userId, Settings.shared.gamification.points.card.comment, card.project );
        }

        // NOTIFICATIONS

        // Setup the base notification
        const notification = {
            subject: userId,
            objects: [
                {
                    id:   comment._id,
                    type: 'comment'
                },
                {
                    id:   card.id,
                    type: 'card'
                },
                {
                    id:   card.phase,
                    type: 'phase'
                },
                {
                    id:   card.project,
                    type: 'project'
                }
            ]
        };

        // If the comment was a reply, keep track of the comment replied to
        // in order to be able to later quote that comment
        if ( comment.parent ) {
            notification.data = {
                repliedTo: comment.parent
            };
        }

        // Keep a list of every user notified at each step, so that some notifications can have precendence
        // over others and not generate multiple notifications for the same user.
        // Assume the commenter knows he/she commented ;)
        let alreadyNotified = [userId];

        const commenter = UserCollection.findOne( { _id: comment.author }, { fields: { profile: 1 } } );

        // User being replied to
        // It has precedence on the card author notification since it is more specific
        // and could potentially exclude the card author if he/she is the person replied to
        if ( comment.parent ) {
            const parentComment = CommentCollection.findOne( { _id: comment.parent }, { fields: { author: 1 } } );
            if ( alreadyNotified.indexOf( parentComment.author ) == -1 ) {

                const parentCommentAuthor = UserCollection.findOne( { _id: parentComment.author }, {
                    fields: {
                        _id:                                           1,
                        unsubscribeToken:                              1,
                        emails:                                        1,
                        'profile.firstName':                           1,
                        'profile.lastName':                            1,
                        'profile.language':                            1,
                        'profile.allowEmailContact':                   1,
                        'profile.emailNotifications.cardCommentReply': 1
                    }
                } );

                if ( parentCommentAuthor ) {
                    log.silly( `    - Notifiying replied-to comment author ${parentComment.author}` );

                    alreadyNotified.push( parentComment.author );

                    // UI NOTIFICATION
                    NotificationCollection.notify( _.defaults( {
                        user:   parentComment.author,
                        action: CardCommentNotificationTypes.repliedToYou
                    }, notification ) );

                    // MAIL
                    if ( parentCommentAuthor.profile.allowEmailContact
                        && parentCommentAuthor.profile.emailNotifications.cardCommentReply
                        && parentCommentAuthor.emails
                        && parentCommentAuthor.emails[0]
                        && parentCommentAuthor.emails[0].verified ) {
                        new Job( JobCollection, 'mail', {
                            to:        `${UserHelpers.getDisplayName( parentCommentAuthor )} <${parentCommentAuthor.emails[0].address}>`,
                            template:  'card-comment-reply',
                            user:      _.pick( parentCommentAuthor, ['unsubscribeToken', 'emails', 'profile'] ),
                            commenter: _.pick( commenter, ['profile'] ),
                            card:      _.pick( card, ['name'] ),
                            comment:   _.pick( comment, ['post'] )
                        } ).save();
                    }

                } else {
                    log.warn( `    - Replied-to comment author ${parentComment.author} not found!` );
                }

            } else {
                log.silly( `    - Replied-to comment author ${parentComment.author} doesn't need to be notified` );
            }
        }

        // Card Author
        // Only if not the commenter or already notified of a reply
        if ( alreadyNotified.indexOf( card.author ) == -1 ) {

            const author = UserCollection.findOne( { _id: card.author }, {
                fields: {
                    _id:                                      1,
                    unsubscribeToken:                         1,
                    emails:                                   1,
                    'profile.firstName':                      1,
                    'profile.lastName':                       1,
                    'profile.language':                       1,
                    'profile.allowEmailContact':              1,
                    'profile.emailNotifications.cardComment': 1
                }
            } );

            if ( author ) {
                log.silly( `    - Notifiying card author ${card.author}` );

                alreadyNotified.push( card.author );

                // UI NOTIFICATION
                NotificationCollection.notify( _.defaults( {
                    user:   card.author,
                    action: comment.parent ? CardCommentNotificationTypes.replied : CardCommentNotificationTypes.commented
                }, notification ) );

                // MAIL
                if ( author.profile.allowEmailContact
                    && author.profile.emailNotifications.cardComment
                    && author.emails
                    && author.emails[0]
                    && author.emails[0].verified ) {
                    new Job( JobCollection, 'mail', {
                        to:        `${UserHelpers.getDisplayName( author )} <${author.emails[0].address}>`,
                        template:  'card-comment',
                        user:      _.pick( author, ['unsubscribeToken', 'emails', 'profile'] ),
                        commenter: _.pick( commenter, ['profile'] ),
                        card:      _.pick( card, ['id', 'name'] ),
                        comment:   _.pick( comment, ['post'] )
                    } ).save();
                }
            } else {
                log.warn( `    - Card author ${card.author} not found!` );
            }
        } else {
            log.silly( `    - Card author ${card.author} doesn't need to be notified` );
        }

        // Other Commenters on the card
        const commenters = CommentCollection.distinct( 'author', {
            'objects.0.entity': card.id,
            author:             { $nin: alreadyNotified }
        } );
        if ( commenters.length ) {
            log.silly( "    - Notifiying other commenters", commenters );
        }

        commenters.forEach( commenter => {
            //TODO: Test if user wants that notification
            alreadyNotified.push( commenter );

            NotificationCollection.notify( _.defaults( {
                user:   commenter,
                action: comment.parent ? CardCommentNotificationTypes.alsoReplied : CardCommentNotificationTypes.alsoCommented
            }, notification ) );
        } );

        // Likers of the card
        const likers = _.difference( card.likes, alreadyNotified );
        if ( likers.length ) {
            log.silly( "    - Notifiying card likers", likers );
        }

        likers.forEach( liker => {
            //TODO: Test if user wants that notification
            alreadyNotified.push( liker );

            NotificationCollection.notify( _.defaults( {
                user:   liker,
                action: comment.parent ? CardCommentNotificationTypes.repliedToLiked : CardCommentNotificationTypes.commentedOnLiked
            }, notification ) );
        } );

    } );
} );

/**
 * Comment after update hook
 * Check if the comment was for a card and
 * creates feed stories from the changes
 */
CommentCollection.after.update( function ( userId, comment ) {
    Meteor.defer( () => {

        comment = this.transform();

        // If there is no user id (server insert)
        // or the entity commented on is not a card
        // then return and don't generate stories
        if ( !userId || comment.entityType != CardCollection.entityName ) {
            return;
        }

        log.verbose( `Comment ${comment._id} updated, checking if update is worth a feed story` );

        // Retrieve changes we might be interested in
        const changes = FeedHelpers.getDiff( comment, this.previous, ['post'] );
        if ( _.isEmpty( changes ) ) {
            log.verbose( "    - Post was not edited, no story will be created" );
            return;
        }

        log.verbose( "    - Post was edited, creating a feed story" );

        FeedCollection.recordStory( {
            internal: true,
            subject:  userId,
            action:   comment.parent ? StoryTypes.editedReply : StoryTypes.editedComment,
            objects:  getObjects( comment ),
            data:     {
                comment: comment._id,
                changes: FeedHelpers.getChanges( comment, this.previous, ['post'] )
            }
        } );

    } );
} );

/**
 * Comment after remove hook
 */
CommentCollection.after.remove( function ( userId, comment ) {
    Meteor.defer( () => {

        comment = this.transform();

        // If there is no user id (server remove)
        // or the entity commented on is not a card
        // then return and don't do anything
        if ( !userId || comment.entityType != CardCollection.entityName ) {
            return;
        }

        log.verbose( `Comment ${comment._id} removed` );

        // We need information from the card commented on...
        const card = CardCollection.findOne( { id: comment.entityId, current: true }, {
            fields:    {
                author:          1,
                project:         1,
                lastTemperature: 1,
                heatedAt:        1
            },
            transform: null
        } );
        if ( !card ) {
            log.warn( `Could not find card ${comment.entityId} for card comment after remove hook.` );
            return;
        }

        // CARD HEAT + REPUTATION POINTS (NOT REMOVING POINTS FROM SELF COMMENTS)
        if ( userId != card.author ) {
            CardAnalytics.cool( card, comment.createdAt, Settings.shared.analytics.card.heat.comment );
            Gamification.awardPoints( userId, -Settings.shared.gamification.points.card.comment, card.project );
        }

    } );
} );
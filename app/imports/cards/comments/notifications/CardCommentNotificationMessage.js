"use strict";

import {CardCommentNotificationTypes} from "/imports/cards/comments/notifications/CardCommentNotifications";
import NotificationMessageHelpers from "/imports/notifications/NotificationMessageHelpers";

export default class CardCommentNotificationMessage {

    static getLink( notification, subject, { card } ) {
        // No uri used here because card might not be transformed
        return card ? `/card/${card.slug || card.id}` : null;
    }

    static getMessage( notification, subject, { card, comment, project }, html ) {
        if ( !subject || !card || !project ) {
            return null;
        }

        const i18nData = {
            user:    NotificationMessageHelpers.subjectSpan( subject, html ),
            card:    NotificationMessageHelpers.cardSpan( card, html ),
            project: NotificationMessageHelpers.projectSpan( project, html ),
            comment: comment ? ( html ? `<span class="comment excerpt">${comment.excerptMarkdown( 30 )}</span>` : comment.excerptMarkdown( 30 ) ) : null
        };
        let message    = null;

        switch ( notification.action ) {
            case CardCommentNotificationTypes.commented:
                message = __( '__user__ commented on your card __card__ in __project__', i18nData );
                break;
            case CardCommentNotificationTypes.replied:
                if ( comment ) {
                    message = __( '__user__ replied to a comment "__comment__" on your card __card__ in __project__', i18nData );
                } else {
                    message = __( '__user__ replied to a comment on your card __card__ in __project__', i18nData );
                }
                break;
            case CardCommentNotificationTypes.alsoCommented:
                message = __( '__user__ also commented on card __card__ in __project__', i18nData );
                break;
            case CardCommentNotificationTypes.alsoReplied:
                if ( comment ) {
                    message = __( '__user__ replied to a comment "__comment__" on card __card__ in __project__', i18nData );
                } else {
                    message = __( '__user__ replied to a comment on card __card__ in __project__', i18nData );
                }
                break;
            case CardCommentNotificationTypes.repliedToYou:
                if ( comment ) {
                    message = __( '__user__ replied to your comment "__comment__" on card __card__ in __project__', i18nData );
                } else {
                    message = __( '__user__ replied to your comment on card __card__ in __project__', i18nData );
                }
                break;
            case CardCommentNotificationTypes.commentedOnLiked:
                message = __( '__user__ commented on a card you liked __card__ in __project__', i18nData );
                break;
            case CardCommentNotificationTypes.repliedOnLiked:
                if ( comment ) {
                    message = __( '__user__ replied to a comment "__comment__" on a card you liked __card__ in __project__', i18nData );
                } else {
                    message = __( '__user__ replied to a comment on a card you liked __card__ in __project__', i18nData );
                }
                break;
            case CardCommentNotificationTypes.liked:
                if ( comment ) {
                    message = __( '__user__ liked your comment "__comment__" on card __card__ in __project__', i18nData );
                } else {
                    message = __( '__user__ liked your comment on card __card__ in __project__', i18nData );
                }
                break;
        }

        return message;
    }

}
"use strict";

import {ValidatedMethod} from "meteor/mdg:validated-method";
import NotificationCollection from "/imports/notifications/NotificationCollection";
import ProjectCollection from "../../../projects/ProjectCollection";
import ProjectSecurity from "../../../projects/ProjectSecurity";
import CardCollection from "../../CardCollection";
import CommentCollection from "../../../comments/CommentCollection";

const CardCommentNotificationMethods = {

    fetch: new ValidatedMethod( {
            name:     'pr/notification/card/comment',
            validate: new SimpleSchema( {
                notificationId: {
                    type: String
                }
            } ).validator( { clean: true } ),
            run( { notificationId } ) {

                // NOTIFICATION

                // Can only read own notifications, so also search by userId
                const notification = NotificationCollection.findOne( { _id: notificationId, user: this.userId }, {
                    fields: {
                        objects: 1,
                        data:    1
                    }
                } );
                if ( !notification ) {
                    throw new Meteor.Error('notification not found');
                }

                // PROJECT

                const projectId = notification.getObjectIdByType( ProjectCollection.entityName );
                // Check the project directly, it skips a call to DB
                if ( !ProjectSecurity.canRead( this.userId, projectId ) ) {
                    throw new Meteor.Error('unauthorized');
                }
                const project = ProjectCollection.findOne( { _id: projectId }, {
                    fields: {
                        _id:  1,
                        slug: 1,
                        name: 1
                    }
                } );

                // CARD

                const cardId = notification.getObjectIdByType( CardCollection.entityName );
                const card = CardCollection.findOne( { id: cardId, current: true }, {
                    fields: {
                        _id:     1,
                        id:      1,
                        slug:    1,
                        name:    1,
                        version: 1,
                        current: 1
                    }
                } );

                // CARD IMAGE

                const cardImage = card ? card.getImage() : null;

                // COMMENT

                let comment;
                if ( notification.data && notification.data.repliedTo ) {
                    comment = CommentCollection.getObjectIdByType( { _id: notification.data.repliedTo }, {
                        fields: {
                            _id:  1,
                            post: 1
                        }
                    } );
                }

                return {
                    project,
                    card,
                    cardImage,
                    comment
                };
            }
        }
    )
};

export default CardCommentNotificationMethods
"use strict";

import { NotificationTypes as CommentNotificationTypes } from "/imports/comments/notifications/CommentNotifications";

export const CardCommentNotificationTypes = _.extend( {
    commentedOnLiked: 'commented-on-liked',
    repliedOnLiked:   'replied-on-liked'
}, CommentNotificationTypes );

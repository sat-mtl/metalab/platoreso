"use strict";

import UserCollection from "/imports/accounts/UserCollection";
import ProjectCollection from "../../../projects/ProjectCollection";
import ProjectSecurity from "../../../projects/ProjectSecurity";
import CardCollection from "../../CardCollection";
import CardPublicationHelpers from "../CardPublicationHelpers";

/**
 * Project Cards Per Phase (Public)
 */
Meteor.publishComposite( "project/card/list", function ( projectId, phaseId = null, options = { showCardHistory: false, isSearch: false } ) {
    check( projectId, String );
    check( phaseId, Match.OneOf( String, null ) );
    check( options, Match.ObjectIncluding( {
        showCardHistory: Match.Optional( Boolean ),
        linked:          Match.Optional( Boolean )
    } ) );

    if ( !ProjectSecurity.canRead( this.userId, projectId ) ) {
        return this.ready();
    }

    const cardFieldList   = CardPublicationHelpers.getCardListFields( this.userId );
    const relatedChildren = [
        {
            find: card => card.findImage()
        },
        {
            find: card => UserCollection.find( { _id: card.author }, {
                fields: {
                    _id:                 1,
                    'profile.firstName': 1,
                    'profile.lastName':  1,
                    'profile.avatarUrl': 1
                }
            } )
        }
    ];

    if ( options.linked ) {
        relatedChildren.push( {
            find: card => card.findLinks()
        } );
    }

    let children = relatedChildren.slice();

    if ( options.showCardHistory ) {
        children.push( {
            find( card, project ) {
                // Get last version from all phase visits
                // This was only using the last visit from each phase before but it wouldn't
                // really paint a representative picture of the card's history.
                const versions = card.phaseVisits
                                     // Keep "exited" visits only, current visit doesn't interest us here
                                     // And also only keep visits to phases that are still active in the project
                                     .filter( visit => visit.exitedVersion != null && project.phases.find( phase => phase._id == visit.phase && !phase.removed ) )
                                     .map( visit => visit.exitedVersion );
                return CardCollection.find(
                    {
                        id:        card.id,
                        current:   false,
                        moderated: false,
                        version:   { $in: versions }
                    },
                    {
                        fields: _.extend( { versionedAt: 1, phaseVisits: 1 }, cardFieldList )
                    }
                );
            },
            children: relatedChildren
        } );
    }

    return {
        find() {
            const query   = { _id: projectId };
            const options = { fields: { _id: 1, phases: 1 } };
            if ( phaseId ) {
                query['phases._id'] = phaseId;
            }
            return ProjectCollection.find( query, options );
        },
        children: [
            {
                find( project ) {
                    let query = { project: project._id, current: true, moderated: false };
                    if ( phaseId ) {
                        query.phase = phaseId;
                    }

                    return CardCollection.find( query, {
                        fields: _.extend( options.showCardHistory ? { phaseVisits: 1 } : {}, cardFieldList )
                    } );
                },
                children: children
            }
        ]

    }
} );
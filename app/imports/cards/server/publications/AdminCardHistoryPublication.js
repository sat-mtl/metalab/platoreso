"use strict";

import slugify from "underscore.string/slugify";
import { Counts } from "meteor/tmeasday:publish-counts";
import UserHelpers from "/imports/accounts/UserHelpers";
import CardCollection from "/imports/cards/CardCollection";

/**
 * Card List (Administration)
 *
 * @params {Object} [options] Options for the find operation (ex: sort)
 */
Meteor.publishComposite( "admin/card/history", function ( query = null, options = null ) {

    // Check query for only the allowed fields
    check( query, Match.ObjectIncluding( {
        id:  String,
        name: Match.Optional( Match.ObjectIncluding( { $regex: String } ) )
    } ) );

    // Check options for only the allowed fields
    check( options, Match.OneOf( null, {
        sort:  Match.Optional( Object ),
        limit: Match.Optional( Number ),
        skip:  Match.Optional( Number )
    } ) );

    if ( !UserHelpers.isSuper( this.userId ) ) {
        return this.ready();
    }

    // Setup query
    query = query || {};

    // Setup options
    options = _.extend( options || {}, {
        fields: {
            id:          1,
            current:     1,
            version:     1,
            slug:        1,
            name:        1,
            name_sort:   1,
            project:     1,
            updatedAt:   1,
            updatedBy:   1,
            versionedAt: 1,
            versionedBy: 1
        }
    } );

    let children = [
        {
            find: card => card.findImage()
        }
    ];

    return {
        find() {
            // Slugify the json query as a unique identifier for the count
            Counts.publish( this,
                'card/history/count/' + slugify( JSON.stringify( query ) ),
                CardCollection.find( query, { fields: { _id: 1 } } ),
                { noReady: true }
            );
            return CardCollection.find( query, options )
        },
        children: children
    };
} );
"use strict";

import slugify from "underscore.string/slugify";
import { Counts } from "meteor/tmeasday:publish-counts";
import UserCollection from "/imports/accounts/UserCollection";
import UserHelpers from "/imports/accounts/UserHelpers";
import ProjectCollection from "../../../projects/ProjectCollection";
import CardCollection from "../../CardCollection";
import CardImagesCollection from "../../images/CardImagesCollection";

/**
 * Card List (Administration)
 *
 * @params {Object} [options] Options for the find operation (ex: sort)
 */
Meteor.publishComposite( "admin/card/list", function ( query = null, options = null ) {
    // Check query for only the allowed fields
    check( query, Match.OneOf( null, {
        name:    Match.Optional( Match.ObjectIncluding( { $regex: String } ) ),
        author:  Match.Optional( String ),
        project: Match.Optional( String )
    } ) );

    // Check options for only the allowed fields
    check( options, Match.OneOf( null, {
        sort:  Match.Optional( Object ),
        limit: Match.Optional( Number ),
        skip:  Match.Optional( Number )
    } ) );

    if ( !UserHelpers.isSuper( this.userId ) ) {
        return this.ready();
    }

    // Setup query
    query = query || {};

    // !important
    query.current = true;

    // Setup options
    options = _.extend( options || {}, {
        fields: {
            id:       1,
            slug:      1,
            author:    1,
            name:      1,
            name_sort: 1,
            project:   1,
            current:   1,
            createdAt: 1,
            createdBy: 1,
            updatedAt: 1,
            updatedBy: 1
        }
    } );

    let children = [
        {
            find( card ) {
                return CardImagesCollection.find( { owners: card.id } );
            }
        }
    ];

    // If no project was passed, we'll need the card's projects in the publication
    if ( !query.project ) {
        children.push( {
            find( card ) {
                return ProjectCollection.find( { _id: card.project }, { fields: { name: 1 } } );
            }
        } );
    }

    // If no author was passed, we'll need the card's author in the publication
    if ( !query.author ) {
        children.push( {
            find( card ) {
                return UserCollection.find( { _id: card.author }, {
                    fields: {
                        'profile.firstName': 1, 'profile.lastName': 1
                    }
                } );
            }
        } );
    }


    return {
        find() {
            // Slugify the json query as a unique identifier for the count
            Counts.publish( this,
                'card/count/' + slugify( JSON.stringify( query ) ),
                CardCollection.find( query, { fields: { _id: 1 } } ),
                { noReady: true }
            );
            return CardCollection.find( query, options )
        },
        children: children
    };
} );
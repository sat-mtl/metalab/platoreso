"use strict";

import Logger from "meteor/metalab:logger/Logger";
import CardCollection from "../../CardCollection";
import CardSecurity from "../../CardSecurity";
import CardPublicationHelpers from "../CardPublicationHelpers";

const log = Logger( "card-publication" );

/**
 * Card (Public)
 */
Meteor.publishComposite( "card", function ( cardId, options = {} ) {
    check( cardId, String );
    check( options, Match.ObjectIncluding( {
        cardVersion: Match.Optional( Number ),
        linked:      Match.Optional( Boolean )
    } ) );

    let publication = {
        find() {
            const query = {
                moderated: false,
                $or:       [
                    { id: cardId },
                    { slug: cardId }
                ]
            };

            if ( options.version == null ) {
                query.current = true;
            } else {
                query.version = parseInt( options.version );
            }

            const card = CardCollection.findOne( query,
                {
                    fields: {
                        id: 1
                    }
                }
            );

            if ( !card ) {
                log.verbose( `Card ${cardId} not found`, options );
                return null;
            }

            if ( !CardSecurity.canRead( this.userId, card.id ) ) {
                log.debug( `User ${this.userId} cannot read card ${card.id}` );
                return null;
            }

            return CardCollection.find( query, {
                fields: CardPublicationHelpers.getCardDetailedFields( this.userId )
            } );
        },
        children: [
            { find: card => card.findAuthor() },
            { find: card => card.findImage() },
            { find: card => card.findAttachments() }
        ]
    };

    // Also fetch basic linked cards if the linked options is set
    if ( options.linked ) {
        publication.children.push( {
            find:     card => card.findLinks(),
            children: [
                {
                    find:     link => link.findSourceLinkedCard( {
                        fields: CardPublicationHelpers.getCardListFields( this.userId )
                    } ),
                    children: [
                        { find: card => card.findAuthor() },
                        { find: card => card.findImage() }
                    ]
                }
            ]
        } );
    }

    return publication;
} );
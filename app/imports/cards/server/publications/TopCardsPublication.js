"use strict";

import ProjectSecurity from "../../../projects/ProjectSecurity";
import CardCollection from "../../../cards/CardCollection";
import CardPublicationHelpers from "../CardPublicationHelpers";
import UserCollection from "/imports/accounts/UserCollection";

/**
 * Project (Public)
 */
Meteor.publishComposite( "project/card/top", function ( projectId, limit = 5 ) {
    check( projectId, String );
    check( limit, Number );

    // Check with the project instead of the _id to prevent another fetch from the db
    if ( !ProjectSecurity.canRead( this.userId, projectId ) ) {
        return this.ready();
    }

    return {
        find() {
            return CardCollection.find(
                {
                    project:          projectId,
                    current:          true,
                    moderated:        false,
                    totalTemperature: { $gt: 0 }
                },
                {
                    fields: _.extend( { totalTemperature: 1 }, CardPublicationHelpers.getCardListFields( this.userId ) ),
                    $sort:  { totalTemperature: -1 },
                    $limit: limit
                } );
        },
        children: [
            {
                find( card ) {
                    return card.findImage();
                }
            },
            {
                find( card ) {
                    return UserCollection.find( { _id: card.author }, {
                        fields: {
                            _id:                 1,
                            'profile.firstName': 1,
                            'profile.lastName':  1,
                            'profile.avatarUrl': 1
                        }
                    } );
                }
            }
        ]
    };
} );
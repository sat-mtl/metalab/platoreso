"use strict";

import { Counts } from "meteor/tmeasday:publish-counts";
import ProjectSecurity from "../../../projects/ProjectSecurity";
import CardCollection from "../../CardCollection";

/**
 * Project Card Count (Public)
 */
Meteor.publish( "project/card/count", function ( projectId ) {
    check( projectId, String );

    if ( !ProjectSecurity.canRead( this.userId, projectId ) ) {
        return this.ready();
    }

    //FIXME: This can get nasty ram-wise
    Counts.publish( this,
        'project/card/count/' + projectId,
        CardCollection.find( { project: projectId, current: true, moderated: false }, { fields: { _id: 1 } } ) );
} );
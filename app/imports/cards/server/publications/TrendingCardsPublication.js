"use strict";

import { MS_PER_HOUR } from "../../../utils/TimeUtils";

import ProjectSecurity from "../../../projects/ProjectSecurity";
import CardCollection from "../../CardCollection";
import Settings from "../../../settings/Settings";

Meteor.publish( "project/card/trending", function ( projectId, limit = 5 ) {
    check( projectId, String );
    check( limit, Number );

    if ( !ProjectSecurity.canRead( this.userId, projectId ) ) {
        return this.ready();
    }

    const self       = this;
    let initializing = true;
    let ids          = {};
    let iteration    = 0;

    function pipeline() {
        return [
            {
                $match: {
                    project:   projectId,
                    current:   true,
                    moderated: false
                }
            },
            {
                $project: {
                    temp:      {
                        $subtract: [
                            '$lastTemperature',
                            {
                                $multiply: [
                                    {
                                        $divide: [
                                            {
                                                $subtract: [
                                                    new Date(),
                                                    '$heatedAt'
                                                ]
                                            },
                                            MS_PER_HOUR
                                        ]
                                    },
                                    Settings.shared.analytics.card.cooldown
                                ]
                            }
                        ]
                    },
                    _id:       1,
                    id:        1,
                    project:   1,
                    current:   1,
                    moderated: 1
                }
            },
            { $sort: { temp: -1 } },
            { $limit: limit }
        ];
    }

    function update() {
        if ( initializing ) {
            return;
        }

        // add and update documents on the client
        CardCollection.aggregate( pipeline() ).forEach( doc => {
            if ( !ids[doc._id] ) {
                self.added( "cards", doc._id, doc );
            } else {
                self.changed( "cards", doc._id, doc );
            }
            ids[doc._id] = iteration;
        } );
        // remove documents not in the result anymore
        _.forEach( ids, ( v, k ) => {
            if ( v != iteration ) {
                delete ids[k];
                self.removed( "cards", k );
            }
        } );

        iteration++;
    }

    // track any changes on the collection used for the aggregation
    var query  = CardCollection.find( { project: projectId, current: true, moderated: false } );
    var handle = query.observeChanges( {
        added:   update,
        changed: update,
        removed: update,
        error:   function ( err ) {
            throw err;
        }
    } );

    initializing = false;
    update();
    this.ready();
    this.onStop( () => handle.stop() );
} );
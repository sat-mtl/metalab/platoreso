"use strict";

import UserCollection from "/imports/accounts/UserCollection";
import UserHelpers from "/imports/accounts/UserHelpers";
import CardImagesCollection from "../../images/CardImagesCollection";
import ProjectCollection from "../../../projects/ProjectCollection";
import CardCollection from "../../CardCollection";

/**
 * Card Edit (Administration)
 */
Meteor.publish( "admin/card/edit", function ( id ) {
    check( id, String );
    if ( UserHelpers.isSuper( this.userId ) ) {
        return [
            CardCollection.find( { id: id, current: true } ),
            CardImagesCollection.find( { owners: id } ),
            ProjectCollection.find( {}, { fields: { _id: 1, name: 1, 'phases._id': 1, 'phases.name': 1 } } ),
            UserCollection.find( {}, { fields: { 'profile.firstName': 1, 'profile.lastName': 1 } } )
        ];
    } else {
        this.ready();
    }
} );
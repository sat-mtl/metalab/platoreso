"use strict";

import { Counts } from "meteor/tmeasday:publish-counts";
import CardCollection from "../../CardCollection";

/**
 * Card Count (Public)
 */
Meteor.publish( "card/count", function ( ) {
    //FIXME: This can get nasty ram-wise
    Counts.publish( this, 'card/count', CardCollection.find( { current: true, moderated: false }, { fields: { _id: 1 } } ) );
} );
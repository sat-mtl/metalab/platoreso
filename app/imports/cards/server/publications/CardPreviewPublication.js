"use strict";

import CardCollection from "../../CardCollection";
import CardSecurity from "../../CardSecurity";
import CardPublicationHelpers from "../CardPublicationHelpers";

/**
 * Card (Public)
 */
Meteor.publishComposite( "card/preview", function ( cardId ) {
    check( cardId, String );

    return {
        find() {
            if ( !CardSecurity.canRead( this.userId, cardId ) ) {
                return this.ready();
            }

            // Extra fields we might need depending on our options
            // They get added to the query fields if not already provided
            // This is just a safety against future modifications of the default fields in the event someone decides a
            // potential option-required field is not to be part of the default set anymore.
            let extraFields = {};

            // We want the regular card
            return CardCollection.find( { id: cardId, current: true, moderated: false }, {
                fields: _.defaults( CardPublicationHelpers.getCardListFields( this.userId ), extraFields )
            } );
        },
        children: [
            { find: card => card.findAuthor() },
            { find: card => card.findImage() }
        ]
    };
} );
"use strict";

import { Security } from 'meteor/ongoworks:security';
import { RoleGroups } from "/imports/accounts/Roles";
import CardCollection from "../CardCollection";
import CardImagesCollection from "../images/CardImagesCollection";
import CardAttachmentsCollection from "../attachments/CardAttachmentsCollection";
import CardSecurity from "../CardSecurity";

/**
 * Deny if user cannot edit the image owner's group
 * At this stage the file is uploaded, so its owner is identified by the owners property
 */
Security.defineMethod( "ifCanReadOwnerCard", {
    fetch:     [],
    transform: null,
    deny:      function ( type, arg, userId, doc, fields, modifier ) {
        return !_.some( doc.owners, owner => CardSecurity.canRead( userId, owner.id ) );
    }
} );

/**
 * Deny if user cannot edit the image owner's card
 * When uploading, file owner is identified by a card property,
 * it changes later but at upload time it is to be linked to a single card only.
 */
Security.defineMethod( "ifCanEditOwnerCard", {
    fetch:     [],
    transform: null,
    deny:      function ( type, arg, userId, doc, fields, modifier ) {
        if ( doc.card && !doc.owners ) {
            // Before being inserted
            return !CardSecurity.canEdit( userId, doc.card );
        } else {
            // After being inserted
            return !_.some( doc.owners, owner => CardSecurity.canEdit( userId, owner.id ) );
        }
    }
} );

/**
 * Security Rules
 */

// Allow anyone with access to card to download files
// NOTE: Not combined with following rule because download rule is broken with more than one collection
Security.permit( ['download'] )
        .collections( [CardAttachmentsCollection] )
        .ifCanReadOwnerCard()
        .allowInClientCode();

// Allow anyone with access to card to download images
// NOTE: Not combined with previous rule because download rule is broken with more than one collection
Security.permit( ['download'] )
        .collections( [CardImagesCollection] )
        .ifCanReadOwnerCard()
        .allowInClientCode();

Security.permit( ['insert'] )
        .collections( [CardImagesCollection, CardAttachmentsCollection] )
        .ifLoggedIn()
        .ifHasRole( RoleGroups.experts )
        .allowInClientCode();

Security.permit( ['insert'] )
        .collections( [CardImagesCollection, CardAttachmentsCollection] )
        .ifLoggedIn()
        .ifCanEditOwnerCard()
        .allowInClientCode();

// MODERATION

Security.defineMethod( "ifCanModerateCard", {
    fetch:     ['author', 'project', 'phase'],
    transform: null,
    allow:     function ( type, arg, userId, doc, fields, modifier ) {
        return CardSecurity.canModerate( userId, doc );
    }
} );

CardCollection
    .permit( ['update'] )
    .onlyProps( ['moderated', 'reportCount', 'reportedBy'] )
    .ifLoggedIn()
    .ifCanModerateCard();
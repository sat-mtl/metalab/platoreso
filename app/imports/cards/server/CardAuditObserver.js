"use strict";

import "rxjs/add/operator/filter";
import Logger from "meteor/metalab:logger/Logger";
import NotificationCollection from "/imports/notifications/NotificationCollection";
import FeedCollection from "/imports/feed/FeedCollection";
import Audit from "../../audit/Audit";
import ProjectCollection from "../../projects/ProjectCollection";
import CardCollection from "../CardCollection";
import { StoryTypes } from "../feed/CardStories";
import { NotificationTypes } from "../notifications/CardNotifications";
import { CardAuditMessages } from "../CardAudit";

const log = Logger( "card-audit-observer" );

export default class CardAuditObserver {

    constructor( subject ) {
        // Added
        subject
            .filter( audit => audit.message == CardAuditMessages.added )
            .subscribe( CardAuditObserver.added );

        // Updated
        subject
            .filter( audit => audit.message == CardAuditMessages.updated )
            .subscribe( CardAuditObserver.updated );

        // Archived
        subject
            .filter( audit => audit.message == CardAuditMessages.archived )
            .subscribe( CardAuditObserver.archived );

        // Liked
        subject
            .filter( audit => audit.message == CardAuditMessages.liked )
            .subscribe( CardAuditObserver.liked );

        // Unliked
        subject
            .filter( audit => audit.message == CardAuditMessages.unliked )
            .subscribe( CardAuditObserver.unliked );

        // Linked
        subject
            .filter( audit => audit.message == CardAuditMessages.linked )
            .subscribe( CardAuditObserver.linked );

        // Unlinked
        subject
            .filter( audit => audit.message == CardAuditMessages.unlinked )
            .subscribe( CardAuditObserver.unlinked );

        // Moved
        subject
            .filter( audit => audit.message == CardAuditMessages.moved )
            .subscribe( CardAuditObserver.moved );

        // Copied
        subject
            .filter( audit => audit.message == CardAuditMessages.copied )
            .subscribe( CardAuditObserver.copied );

        // Pinned
        subject
            .filter( audit => audit.message == CardAuditMessages.pinned )
            .subscribe( CardAuditObserver.pinned );

        // Unpinned
        subject
            .filter( audit => audit.message == CardAuditMessages.unpinned )
            .subscribe( CardAuditObserver.unpinned );
    }

    /**
     * Card added
     *
     * @param userId
     * @param cardId
     */
    static added( { userId, cardId } ) {
        log.verbose( `added - Card ${cardId} added by ${userId}` );

        const card = CardCollection.findOne(
            { id: cardId, current: true },
            { fields: { id: 1, project: 1, phase: 1 } }
        );
        if ( !card ) {
            log.warn( `added - Card ${cardId} not found` );
            return;
        }

        // Post feed story
        FeedCollection.recordStory( {
            internal: false,
            subject:  userId,
            action:   StoryTypes.created,
            objects:  card.objectAncestors,
            data:     {
                phase: card.phase
            }
        } );
    }

    /**
     * Card updated
     *
     * @param userId
     * @param cardId
     */
    static updated( { userId, cardId } ) {
        log.verbose( `updated - Card ${cardId} updated by ${userId}` );

        const card = CardCollection.findOne(
            { id: cardId, current: true },
            { fields: { id: 1, project: 1, phase: 1 } }
        );
        if ( !card ) {
            log.warn( `updated - Card ${cardId} not found` );
            return;
        }

        //TODO: story.data = FeedHelpers.getChanges(card, this.previous, ['name', 'content']);

        FeedCollection.recordStory( {
            internal: false,//true,
            subject:  userId,
            action:   StoryTypes.updated,
            objects:  card.objectAncestors,
            data:     {
                phase: card.phase
            }
        } );
    }

    /**
     * Card archived
     *
     * @param userId
     * @param cardId
     */
    static archived( { userId, cardId } ) {
        log.verbose( `archived - Card ${cardId} archived by ${userId}` );

        // Remove feed stories
        FeedCollection.archiveAllForObject( cardId, CardCollection.entityName );

        // Remove notifications
        NotificationCollection.unnotifyAllForObject( cardId, CardCollection.entityName );
    }

    static liked( { userId, cardId } ) {
        log.verbose( `liked - Card ${cardId} liked by ${userId}` );

        const card = CardCollection.findOne( { id: cardId, current: true }, {
            fields: {
                id:      1,
                author:  1,
                phase:   1,
                project: 1
            }
        } );
        if ( !card ) {
            log.warn( `liked - Card ${cardId} not found` );
            return;
        }

        FeedCollection.recordStory( {
            internal: false,
            subject:  userId,
            action:   StoryTypes.liked,
            objects:  card.objectAncestors,
            data:     {
                phase: card.phase
            }
        } );

        if ( userId != card.author ) {
            // Notify the card author
            NotificationCollection.notify( {
                user:    card.author,
                subject: userId,
                action:  NotificationTypes.liked,
                objects: card.objectAncestors
            } );
        }
    }

    static unliked( { userId, cardId } ) {
        log.verbose( `unliked - Card ${cardId} unliked by ${userId}` );

        const card = CardCollection.findOne( { id: cardId, current: true }, {
            fields: {
                id:      1,
                author:  1,
                project: 1,
                phase:   1
            }
        } );
        if ( !card ) {
            log.warn( `unliked - Card ${cardId} not found` );
            return;
        }

        // Record internally
        FeedCollection.recordStory( {
            internal: true,
            subject:  userId,
            action:   StoryTypes.unliked,
            objects:  card.objectAncestors,
            data:     {
                phase: card.phase
            }
        } );

        // Hide from general public
        FeedCollection.makeInternal( {
            subject:        userId,
            action:         StoryTypes.liked,
            'objects.0.id': card.id
        } );

        // Unnotify
        NotificationCollection.unnotify( card.author, userId, NotificationTypes.liked, card.id, CardCollection.entityName );
    }

    static pinned( { userId, cardId } ) {
        log.verbose( `pinned - Card ${cardId} pinned by ${userId}` );

        const card = CardCollection.findOne( { id: cardId, current: true }, {
            fields: {
                id:      1,
                author:  1,
                project: 1
            }
        } );
        if ( !card ) {
            log.warn( `pinned - Card ${cardId} not found` );
            return;
        }

        FeedCollection.recordStory( {
            internal: false,
            subject:  userId,
            action:   StoryTypes.pinned,
            objects:  card.objectAncestors
        } );

        if ( userId != card.author ) {
            // Notify the card author
            NotificationCollection.notify( {
                user:    card.author,
                subject: userId,
                action:  NotificationTypes.pinned,
                objects: card.objectAncestors
            } );
        }
    }

    static unpinned( { userId, cardId } ) {
        log.verbose( `unpinned - Card ${cardId} unpinned by ${userId}` );

        const card = CardCollection.findOne( { id: cardId, current: true }, {
            fields: {
                id:      1,
                author:  1,
                project: 1
            }
        } );
        if ( !card ) {
            log.warn( `unpinned - Card ${cardId} not found` );
            return;
        }

        // Record internally
        FeedCollection.recordStory( {
            internal: true,
            subject:  userId,
            action:   StoryTypes.unpinned,
            objects:  card.objectAncestors
        } );

        // Hide from general public
        FeedCollection.makeInternal( {
            subject:        userId,
            action:         StoryTypes.pinned,
            'objects.0.id': card.id
        } );

        // Unnotify
        NotificationCollection.unnotify( card.author, userId, NotificationTypes.pinned, card.id, CardCollection.entityName );
    }

    static linked( { userId, sourceCardId, destinationCardId, linkType } ) {
        log.verbose( `linked - Card ${sourceCardId} linked (${linkType}) to card ${destinationCardId} by ${userId}` );

        const sourceCard = CardCollection.findOne( { id: sourceCardId, current: true }, {
            fields: {
                id:      1,
                author:  1,
                phase:   1,
                project: 1
            }
        } );
        if ( !sourceCard ) {
            log.warn( `linked - Card ${sourceCardId} not found` );
            return;
        }

        const destinationCard = CardCollection.findOne( { id: destinationCardId, current: true }, {
            fields: {
                id:      1,
                author:  1,
                phase:   1,
                project: 1
            }
        } );
        if ( !destinationCard ) {
            log.warn( `linked - Card ${destinationCardId} not found` );
            return;
        }

        const objects = [
            {
                id:   sourceCard.id,
                type: CardCollection.entityName,
                role: 'source'
            },
            {
                id:   destinationCard.id,
                type: CardCollection.entityName,
                role: 'destination'
            },
            {
                id:   sourceCard.project,
                type: ProjectCollection.entityName
            }
        ];

        const data = {
            linkType:  linkType,
            fromPhase: sourceCard.phase,
            toPhase:   destinationCard.phase
        };

        FeedCollection.recordStory( {
            internal: false,
            subject:  userId,
            action:   StoryTypes.linked,
            objects:  objects,
            data:     data
        } );

        // Notifications

        if ( sourceCard.author == destinationCard.author ) {

            // They're the same don't test for both
            if ( userId != sourceCard.author ) {
                // Notify the card author
                NotificationCollection.notify( {
                    user:    sourceCard.author,
                    subject: userId,
                    action:  NotificationTypes.linkedBoth,
                    objects: objects,
                    data:    data
                } );
            }

        } else {

            // Source Card
            if ( userId != sourceCard.author ) {
                // Notify the card author
                NotificationCollection.notify( {
                    user:    sourceCard.author,
                    subject: userId,
                    action:  NotificationTypes.linked,
                    objects: objects,
                    data:    data
                } );
            }

            // Destination Card
            if ( userId != destinationCard.author ) {
                // Notify the card author
                NotificationCollection.notify( {
                    user:    destinationCard.author,
                    subject: userId,
                    action:  NotificationTypes.linkedTo,
                    objects: objects,
                    data:    data
                } );
            }
        }
    }

    static unlinked( { userId, sourceCardId, destinationCardId, linkType } ) {
        log.verbose( `unlinked - Card ${sourceCardId} unlinked (${linkType}) from card ${destinationCardId} by ${userId}` );

        const sourceCard = CardCollection.findOne( { id: sourceCardId, current: true }, {
            fields: {
                id:      1,
                author:  1,
                phase:   1,
                project: 1
            }
        } );
        if ( !sourceCard ) {
            log.warn( `unlinked - Card ${sourceCardId} not found` );
            return;
        }

        const destinationCard = CardCollection.findOne( { id: destinationCardId, current: true }, {
            fields: {
                id:      1,
                author:  1,
                phase:   1,
                project: 1
            }
        } );
        if ( !destinationCard ) {
            log.warn( `unlinked - Card ${destinationCard} not found` );
            return;
        }

        const objects = [
            {
                id:   sourceCard.id,
                type: CardCollection.entityName,
                role: 'source'
            },
            {
                id:   destinationCard.id,
                type: CardCollection.entityName,
                role: 'destination'
            },
            {
                id:   sourceCard.project,
                type: ProjectCollection.entityName
            }
        ];

        const data = {
            linkType:  linkType,
            toPhase:   sourceCard.phase,
            fromPhase: destinationCard.phase
        };

        FeedCollection.recordStory( {
            internal: false, //true,
            subject:  userId,
            action:   StoryTypes.unlinked,
            objects:  objects,
            data:     data
        } );

        // Hide from general public
        /*FeedCollection.makeInternal( {
         subject:        userId,
         action:         StoryTypes.linked,
         'objects.0.id': card.id,
         'objects.1.id': fromCard.id
         } );*/

        // Unnotify linked to card author
        NotificationCollection.unnotify(
            sourceCard.author,
            null, // The person unlinking is not the person who linked
            NotificationTypes.linked,
            sourceCard.id,
            CardCollection.entityName,
            { 'objects.1.id': destinationCard.id }
        );

        // Unnotify linked from card author
        NotificationCollection.unnotify(
            destinationCard.author,
            null, // The person unlinking is not the person who linked
            NotificationTypes.linkedTo,
            sourceCard.id,
            CardCollection.entityName,
            { 'objects.1.id': destinationCard.id }
        );

        // Unnotify linked both from card author
        NotificationCollection.unnotify(
            destinationCard.author,
            null, // The person unlinking is not the person who linked
            NotificationTypes.linkedBoth,
            sourceCard.id,
            CardCollection.entityName,
            { 'objects.1.id': destinationCard.id }
        );
    }

    static moved( { userId, cardId, fromPhaseId, toPhaseId } ) {
        log.verbose( `moved - Card ${cardId} was moved from phase ${fromPhaseId} to phase ${toPhaseId} by ${userId}` );

        const card = CardCollection.findOne( { id: cardId, current: true }, {
            fields: {
                id:      1,
                author:  1,
                phase:   1,
                project: 1
            }
        } );
        if ( !card ) {
            log.warn( `moved - Card ${cardId} not found` );
            return;
        }

        const objects = [
            {
                id:   card.id,
                type: CardCollection.entityName
            },
            {
                id:   card.project,
                type: ProjectCollection.entityName
            }
        ];

        const data = {
            fromPhase: fromPhaseId,
            toPhase:   toPhaseId
        };

        FeedCollection.recordStory( {
            internal: false,
            subject:  userId,
            action:   StoryTypes.moved,
            objects:  objects,
            data:     data
        } );

        // Notifications
        if ( userId != card.author ) {
            // Notify the card author
            NotificationCollection.notify( {
                user:    card.author,
                subject: userId,
                action:  NotificationTypes.moved,
                objects: objects,
                data:    data
            } );
        }
    }

    static copied( { userId, cardId, newCardId } ) {
        log.verbose( `copied - Card ${cardId} was copied as ${newCardId} by ${userId}` );

        const card = CardCollection.findOne( { id: cardId, current: true }, {
            fields: {
                id:      1,
                author:  1,
                phase:   1,
                project: 1
            }
        } );

        if ( !card ) {
            log.warn( `copied - Card ${cardId} not found` );
            return;
        }

        const newCard = CardCollection.findOne( { id: newCardId, current: true }, {
            fields: {
                id:      1,
                author:  1,
                phase:   1,
                project: 1
            }
        } );
        if ( !newCard ) {
            log.warn( `copied - Card ${cardId} not found` );
            return;
        }

        const objects = [
            {
                id:   card.id,
                type: CardCollection.entityName,
                role: 'original'
            },
            {
                id:   newCard.id,
                type: CardCollection.entityName,
                role: 'copy'
            },
            {
                id:   card.project,
                type: ProjectCollection.entityName
            }
        ];

        const data = {
            fromPhase: card.phase,
            toPhase:   newCard.phase
        };

        FeedCollection.recordStory( {
            internal: false,
            subject:  userId,
            action:   StoryTypes.copied,
            objects:  objects,
            data:     data
        } );

        // Notifications
        if ( userId != card.author ) {
            // Notify the card author
            NotificationCollection.notify( {
                user:    card.author,
                subject: userId,
                action:  NotificationTypes.copied,
                objects: objects,
                data:    data
            } );
        }
    }

}

export const observer = new CardAuditObserver( Audit.subject.filter( audit => audit.domain == CardCollection.entityName ) );
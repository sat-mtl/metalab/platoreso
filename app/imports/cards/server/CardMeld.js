"use strict";

import Logger from "meteor/metalab:logger/Logger";
import AccountsMeldHelper from "../../accounts/server/AccountsMeldHelper";
import CardCollection from "../CardCollection";
import CardLinksCollection from "../links/CardLinksCollection";

const log = Logger("card-meld");

/**
 * Card Melding
 */
AccountsMeldHelper.onMeld( ( src_user_id, dst_user_id ) => {
    // Cards
    log.debug(`Melding cards for user ${src_user_id} into ${dst_user_id}`);

    CardCollection.update( { 'author': src_user_id }, { $set: { 'author': dst_user_id } }, { multi: true } );
    CardCollection.update( { 'createdBy': src_user_id }, { $set: { 'createdBy': dst_user_id } }, { multi: true } );
    CardCollection.update( { 'updatedBy': src_user_id }, { $set: { 'updatedBy': dst_user_id } }, { multi: true } );
    CardCollection.update( { 'versionedBy': src_user_id }, { $set: { 'versionedBy': dst_user_id } }, { multi: true } );
    CardCollection.update( { 'likes': src_user_id }, { $set: { 'likes.$': dst_user_id } }, { multi: true } );

    // Links
    log.debug(`Melding card links for user ${src_user_id} into ${dst_user_id}`);

    CardLinksCollection.update( { 'linkedBy': src_user_id }, { $set: { 'linkedBy': dst_user_id } }, { multi: true } );
    CardLinksCollection.update( { 'updatedBy': src_user_id }, { $set: { 'updatedBy': dst_user_id } }, { multi: true } );
    CardLinksCollection.update( { 'unlinkedBy': src_user_id }, { $set: { 'unlinkedBy': dst_user_id } }, { multi: true } );
} );
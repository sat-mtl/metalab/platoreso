"use strict";

import Logger from "meteor/metalab:logger/Logger";
import NotificationCollection from "/imports/notifications/NotificationCollection";
import FeedCollection from "/imports/feed/FeedCollection";
import UserCollection from "/imports/accounts/UserCollection";
import UserHelpers from "/imports/accounts/UserHelpers";
import Settings from "/imports/settings/Settings";
import { MS_PER_HOUR } from "/imports/utils/TimeUtils";
import ProjectCollection from "../../projects/ProjectCollection";
import CardCollection from "../CardCollection";
import CardImagesCollection from "../images/CardImagesCollection";
import CardAttachmentsCollection from "../attachments/CardAttachmentsCollection";
import CardLinksCollection from "../links/CardLinksCollection";

const log = Logger( "card-hooks" );

/**
 * User Removed
 */
UserCollection.after.remove( function ( userId, user ) {
    // Defer, so that methods can return without waiting
    Meteor.defer( () => {
        log.debug( `User "${UserHelpers.getDisplayName( user )}" (${user._id}) removed, cleaning up relations` );

        // Remove all user cards
        CardCollection.remove( { author: user._id } );

    } );
} );

/**
 * Project Removed
 */
ProjectCollection.after.remove( function ( userId, project ) {
    // Defer, so that methods can return without waiting
    Meteor.defer( () => {
        log.debug( `Project "${project.name}" (${project._id}) removed, cleaning up relations` );

        // Remove all project cards
        CardCollection.remove( { project: project._id } );

    } );
} );

/**
 * After card update
 * Update the project temperature with an aggregate of all the card's temperatures in the project
 */
CardCollection.after.update( function ( userId, card, fieldNames, modifier, options ) {
    // Defer, so that methods can return without waiting
    Meteor.defer( () => {
        const now         = new Date();
        const temperature = CardCollection.aggregate(
            [
                {
                    $match: {
                        project:   card.project,
                        current:   true,
                        moderated: false
                    }
                },
                {
                    $project: {
                        temp:             {
                            $max: [
                                0,
                                {
                                    $subtract: [
                                        '$lastTemperature',
                                        {
                                            $multiply: [
                                                {
                                                    $divide: [
                                                        {
                                                            $subtract: [
                                                                now,
                                                                '$heatedAt'
                                                            ]
                                                        },
                                                        MS_PER_HOUR
                                                    ]
                                                },
                                                Settings.shared.analytics.card.cooldown
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        totalTemperature: 1
                    }
                },
                {
                    $group: {
                        _id:   "projectTemperature",
                        last:  { $sum: "$temp" },
                        total: { $sum: "$totalTemperature" }
                    }
                }
            ]
        );

        if ( !temperature.length ) {
            // Something went wrong
            log.warn( `Could not get project ${card.project} temperature.` );
            return;
        }

        ProjectCollection.direct.update( { _id: card.project }, {
            $set: {
                lastTemperature:  temperature[0].last,
                totalTemperature: temperature[0].total,
                heatedAt:         now
            }
        } );
    } );
}, { fetchPrevious: false } );

/**
 * Card Removed
 */
CardCollection.after.remove( function ( userId, card ) {
    // Defer, so that methods can return without waiting
    Meteor.defer( () => {
        log.debug( `Card "${card.name}" (${card._id}) removed, cleaning up relations` );

        // Remove Links
        CardLinksCollection.unlinkRemovedCard( card.id );

        // Unlink images
        CardImagesCollection.unlinkVersionedOwner( card.id );

        // Unlink attachments
        CardAttachmentsCollection.unlinkVersionedOwner( card.id );

        // Remove notifications
        NotificationCollection.unnotifyAllForObject( card.id, CardCollection.entityName );

        // Remove Stories
        FeedCollection.removeAllForObject( card.id, CardCollection.entityName );

    } );
} );
"use strict";

// These needs to be evaluated on startup

/** CARD - GENERAL **/
import "./CardMeld";
import "./CardCollectionSecurity";
import "./CardHooks"
import "./CardAuditObserver";

/** CARD - IMAGES **/

/** CARD - ATTACHMENTS **/

/** CARD - FEED **/
import "../feed/CardFeedPublications";

/** CARD - COMMENTS **/
import "../comments/CardCommentsCollectionSecurity";
import "../comments/CardCommentsPublications";
import "../comments/CardCommentsHooks";

/** CARD - COMMENTS - FEED **/
import "../comments/feed/CardCommentsFeedPublications";

/** CARD - MODERATION **/
import "../moderation/CardModerationReportListPublication";

/** CARD - TWITTER **/
import "../twitter/CardsTwitterMeld";
import "../twitter/AccountsCallbacks";

/** CARD - PUBLICATIONS **/
import "./publications/AdminCardEditPublication";
import "./publications/AdminCardListPublication";
import "./publications/AdminCardHistoryPublication";
import "./publications/CardPublication";
import "./publications/CardPreviewPublication";
import "./publications/ProjectCardListPublication";
import "./publications/ProjectCardCountPublication";
import "./publications/CardCountPublication";
import "./publications/TrendingCardsPublication";
import "./publications/TopCardsPublication";
"use strict";

import CardCollection from "./../CardCollection";
import CollectionHelpers from "/imports/collections/CollectionHelpers";

export default class CardPublicationHelpers {

    /**
     * Get card list fields
     *
     * @param userId
     */
    static getCardListFields( userId ) {
        return _.defaults(
            {
                likes: { $elemMatch: { $eq: userId } } // Only show user's likes so that we can know to like/dislike
            },
            CardPublicationHelpers.cardListFields,
            CollectionHelpers.getDefaultFields( CardCollection, userId )
        );
    };

    /**
     * Get card detailed fields
     *
     * @param userId
     */
    static getCardDetailedFields( userId ) {
        return _.defaults(
            {
                likes: { $elemMatch: { $eq: userId } } // Only show user's likes so that we can know to like/dislike
            },
            CardPublicationHelpers.cardDetailedFields,
            CollectionHelpers.getDefaultFields( CardCollection, userId )
        );
    };
}

CardPublicationHelpers.defaultFields = {
    id:               1,
    slug:             1,
    author:           1,
    project:          1,
    phase:            1,
    name:             1,
    name_sort:        1,
    tags:             1,
    viewCount:        1,
    likeCount:        1,
    attachmentCount:  1,
    totalTemperature: 1,
    lastTemperature:  1,
    heatedAt:         1,
    createdAt:        1,
    updatedAt:        1,
    current:          1,
    version:          1,
    versionedAt:      1
};

CardPublicationHelpers.cardListFields = _.extend( {
    excerpt: 1,
    pinned:  1
}, CardPublicationHelpers.defaultFields );

CardPublicationHelpers.cardDetailedFields = _.extend( {
    content:   1,
    data:      1,
    phaseData: 1
}, CardPublicationHelpers.defaultFields );
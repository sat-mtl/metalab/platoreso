"use strict";

import { chai, assert, expect } from 'meteor/practicalmeteor:chai';
import { sinon, spies, stubs } from 'meteor/practicalmeteor:sinon';
import { Factory } from "meteor/dburles:factory";
import "../fixtures/factories";
import CardCollection from "./CardCollection";

describe( 'cards/CardCollection', function () {

    let sandbox;

    beforeEach( () => {
        sandbox = sinon.sandbox.create();
    } );

    afterEach( () => {
        sandbox.restore();
    } );

    describe( "snapshot", () => {

        beforeEach( () => {
            sandbox.stub( CardCollection, "findOne" );
            sandbox.stub( CardCollection.direct, "insert" );
        } );

        function expectEarlyReturn() {
            assert.isFalse( CardCollection.direct.insert.called );
        }

        it( "Should throw if no userId passed", () => {
            expect( ()=>CardCollection.snapshot() ).to.throw( /Match error/ );
            expectEarlyReturn();
        } );

        it( "Should throw if no card passed", () => {
            expect( ()=>CardCollection.snapshot( "userId" ) ).to.throw( /Match error/ );
            expectEarlyReturn();
        } );

        it( "Should throw if card is invalid", () => {
            expect( ()=>CardCollection.snapshot( "userId", { not: "a card" } ) ).to.throw( /Match error/ );
            expectEarlyReturn();
        } );

        it( "Should throw if card not found", () => {
            CardCollection.findOne.returns( null );
            expect( ()=>CardCollection.snapshot( "userId", "cardId" ) ).to.throw( /snapshot card not found/ );
            expectEarlyReturn();
        } );

        it( "Should search for the card if passed an id", () => {
            CardCollection.findOne.returns( { id: "cardId" } );
            CardCollection.snapshot( "userId", "cardId" );
            assert( CardCollection.findOne.calledWith( { id: "cardId", current: true }, { transform: null } ) );
        } );

        it( "Should not search for the card if passed a card", () => {
            const card = Factory.build( 'minimal-card' );
            CardCollection.snapshot( "userId", card );
            assert.isFalse( CardCollection.findOne.called );
        } );

        it( "Should not search for the card if passed a card", () => {
            const card = Factory.build( 'minimal-card' );
            CardCollection.snapshot( "userId", card );
            assert.isFalse( CardCollection.findOne.called );
        } );

        it( "Should make a snapshot", () => {
            const card = { id: "cardId", version: 1 };
            expect( CardCollection.snapshot( "userId", card ) ).to.equal( 2 );
            assert( CardCollection.direct.insert.calledWith( {
                id:          "cardId",
                current:     false,
                versionedAt: sinon.match.date,
                versionedBy: "userId",
                version:     1
            }, { bypassCollection2: true } ) );
        } );

        it( "Should retry if inserting fails on duplicate key", () => {
            const card = { id: "cardId", version: 1 };
            CardCollection.direct.insert.onFirstCall().throws( { code: 11000 } );
            expect( CardCollection.snapshot( "userId", card ) ).to.equal( 3 );
            assert( CardCollection.direct.insert.calledWith( {
                id:          "cardId",
                current:     false,
                versionedAt: sinon.match.date,
                versionedBy: "userId",
                version:     2
            }, { bypassCollection2: true } ) );
        } );

        it( "Should trow if inserting fails on duplicate key more than 10 times", () => {
            const card = { id: "cardId", version: 1 };
            CardCollection.direct.insert.throws( { code: 11000 } );
            expect( () => CardCollection.snapshot( "userId", card ) ).to.throw( /snapshot error/ );
        } );

        it( "Should re-trow if inserting fails for any reason other than duplicate key", () => {
            const card  = { id: "cardId", version: 1 };
            const error = { some: "error" };
            CardCollection.direct.insert.throws( error );
            expect( () => CardCollection.snapshot( "userId", card ) ).to.throw( error );
        } );
    } );

    describe( "snapshotAndUpdate", () => {

        beforeEach( () => {
            sandbox.stub( CardCollection, "snapshot" );
            sandbox.stub( CardCollection, "update" );
        } );

        function expectEarlyReturn() {
            assert.isFalse( CardCollection.snapshot.called );
            assert.isFalse( CardCollection.update.called );
        }

        it( "Should throw if no userId passed", () => {
            expect( ()=>CardCollection.snapshotAndUpdate() ).to.throw( /Match error/ );
            expectEarlyReturn();
        } );

        it( "Should throw if no card passed", () => {
            expect( ()=>CardCollection.snapshotAndUpdate( "userId" ) ).to.throw( /Match error/ );
            expectEarlyReturn();
        } );

        it( "Should throw if card is invalid", () => {
            expect( ()=>CardCollection.snapshotAndUpdate( "userId", { not: "a card" } ) ).to.throw( /Match error/ );
            expectEarlyReturn();
        } );

        it( "Should make a snapshot and update from a card id", () => {
            CardCollection.snapshot.returns( 2 );
            expect( CardCollection.snapshotAndUpdate( "userId", "cardId" ) ).to.equal( 2 );
            assert( CardCollection.snapshot.calledWith( "userId", "cardId" ) );
            assert( CardCollection.update.calledWith( { id: "cardId", current: true }, { $set: { version: 2 } } ) );
        } );

        it( "Should make a snapshot and update from a card object", () => {
            const card = { id: "cardId", version: 1 };
            CardCollection.snapshot.returns( 2 );
            expect( CardCollection.snapshotAndUpdate( "userId", card ) ).to.equal( 2 );
            assert( CardCollection.snapshot.calledWith( "userId", card ) );
            assert( CardCollection.update.calledWith( { id: "cardId", current: true }, { $set: { version: 2 } } ) );
        } );

    } );

} );
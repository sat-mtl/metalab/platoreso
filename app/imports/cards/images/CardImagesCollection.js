"use strict";

import ImageCollection from "/imports/images/ImageCollection";
import CardFilesHelpers from"../files/CardFilesHelpers";

/**
 * Card Images Collection
 */
class CardImagesCollection extends ImageCollection {
    constructor( name, options = {}, config = {} ) {
        _.extend( config, { replacePrevious: true } );
        super( name, options, config );

        /**
         * Indexes
         */

        if ( Meteor.isServer ) {
            this.files._ensureIndex( {
                'owners.id': 1,
                'owners.versionFrom': 1,
                'owners.versionTo': 1
             }, { name: 'owners_id_versionFrom_versionTo' } );
        }

        /**
         * Hooks
         */

         if ( Meteor.isServer ) {
             this.files.before.insert( CardFilesHelpers.beforeInsert.bind( this ) );
         }
    }
}

export default new CardImagesCollection( 'card_images' );
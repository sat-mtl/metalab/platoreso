"use strict";

import { chai, assert, expect } from "meteor/practicalmeteor:chai";
import { sinon, spies, stubs } from "meteor/practicalmeteor:sinon";
import ProjectCollection from "/imports/projects/ProjectCollection";
import CardMethods, { commitImages } from "/imports/cards/CardMethods";
import CardCollection from "/imports/cards/CardCollection";
import CardLinksCollection from "/imports/cards/links/CardLinksCollection";
import CardLinkTypes from "/imports/cards/links/CardLinkTypes";
import CardFilesHelpers from "/imports/cards/files/CardFilesHelpers";
import CardAttachmentsCollection from "/imports/cards/attachments/CardAttachmentsCollection";
import CardImagesCollection from "/imports/cards/images/CardImagesCollection";
import CardSecurity from "/imports/cards/CardSecurity";
import CardAnalytics from "/imports/cards/analytics/CardAnalytics";
import { CardAuditMessages } from "/imports/cards/CardAudit";
import Gamification from "/imports/gamification/Gamification";
import Settings from "/imports/settings/Settings";
import Audit from "/imports/audit/Audit";

describe( 'cards/Methods', () => {

    let sandbox;

    beforeEach( () => {
        sandbox = sinon.sandbox.create();
        sandbox.stub( Audit, 'next' );

        Settings.shared = {
            analytics:    {
                card: {
                    cooldown: 0,
                    heat:     {
                        creation:        1,
                        update:          2,
                        view:            3,
                        like:            4,
                        comment:         5,
                        linkSource:      6,
                        linkDestination: 7,
                        move:            8
                    }
                }
            },
            gamification: {
                points: {
                    card: {
                        creation: 1,
                        like:     2,
                        comment:  3,
                        link:     4,
                        move:     5
                    }
                }
            }
        };
    } );

    afterEach( () => {
        sandbox.restore();
    } );

    describe( 'create', () => {

        let context;

        beforeEach( () => {
            context = { userId: 'userId' };
            sandbox.stub( CardSecurity, 'canCreate' );
            sandbox.stub( CardCollection, 'insert' );
            sandbox.spy( CardAnalytics, 'initialHeatData' );
            sandbox.stub( Gamification, 'awardPoints' );
        } );

        describe( "Happy scenarios", () => {

            beforeEach( () => {
                CardSecurity.canCreate.returns( true );
            } );

            it( "should create card", () => {
                const newCard          = {
                    project: 'projectId',
                    phase:   'phaseId',
                    name:    'card name',
                    content: 'card content'
                };
                const intermediateCard = _.extend( { author: context.userId }, newCard );
                const finalCard        = {
                    id:               sinon.match.string,
                    author:           context.userId,
                    project:          'projectId',
                    phase:            'phaseId',
                    name:             'card name',
                    content:          'card content',
                    totalTemperature: 1,
                    lastTemperature:  1,
                    heatedAt:         sinon.match.date,
                    phaseVisits:      [
                        {
                            type:           'created',
                            phase:          'phaseId',
                            enteredVersion: 0,
                            enteredAt:      sinon.match.date
                        }
                    ]
                };
                CardCollection.insert.returns( 'cardId' );
                CardMethods.create._execute( context, _.clone( newCard ) );
                assert( CardSecurity.canCreate.calledWith( context.userId, intermediateCard ) );
                assert( CardAnalytics.initialHeatData.calledWith( Settings.shared.analytics.card.heat.creation, finalCard ) );
                assert( Gamification.awardPoints.calledWith( 'userId', Settings.shared.gamification.points.card.creation, 'projectId' ) );
                assert( CardCollection.insert.calledWith( finalCard ) );
                assert( Audit.next.calledWith( {
                    domain:  CardCollection.entityName,
                    message: CardAuditMessages.added,
                    userId:  context.userId,
                    cardId:  sinon.match.string //'cardId' => Id is generated now!
                } ) )
            } );

        } );

        describe( "Failing scenarios", () => {

            it( "Should throw if not allowed", () => {
                const newCard = {
                    project: 'projectId',
                    phase:   'phaseId',
                    name:    'card name',
                    content: 'card content'
                };
                CardSecurity.canCreate.returns( false );
                expect( () => CardMethods.create._execute( context, newCard ) ).to.throw( /unauthorized/ );
                expect( CardAnalytics.initialHeatData.called ).to.be.false;
                expect( Gamification.awardPoints.called ).to.be.false;
                expect( CardCollection.insert.called ).to.be.false;
                expect( Audit.next.called ).to.be.false;
            } );

        } );

    } );

    describe( 'archive', () => {

        let context;

        beforeEach( () => {
            context = { userId: 'userId' };

            sandbox.stub( CardSecurity, 'canArchive' );
            sandbox.stub( CardCollection, 'findOne' );
            sandbox.stub( CardCollection.direct, 'remove' );
            sandbox.stub( CardCollection, 'snapshot' );
            sandbox.stub( CardCollection, 'snapshotAndUpdate' );
            sandbox.stub( CardCollection.direct, 'update' );
            sandbox.stub( CardFilesHelpers, 'commitLastVersion' );
            sandbox.stub( CardLinksCollection, 'unlinkArchivedCard' );
            sandbox.stub( Gamification, 'awardPoints' );
        } );

        describe( "Happy scenarios", () => {

            beforeEach( () => {
                CardSecurity.canArchive.returns( true );
            } );

            it( "should remove card", () => {
                CardCollection.findOne.returns( {
                    _id:     'mongoCardId',
                    id:      'cardId',
                    author:  'authorId',
                    project: 'projectId'
                } );
                CardCollection.snapshot.returns( 123 );

                CardMethods.archive._execute( context, { cardId: 'cardId' } );

                assert( CardSecurity.canArchive.calledWith( context.userId, 'cardId' ) );
                assert( CardCollection.findOne.calledWith( {
                    id:      'cardId',
                    current: true
                }, { transform: null } ) );

                if ( Meteor.isClient ) {
                    assert( CardCollection.direct.remove.calledWith( { id: 'cardId', current: true } ) );
                }

                if ( Meteor.isServer ) {
                    assert( CardCollection.snapshot.calledWith( 'userId', {
                        _id:     'mongoCardId',
                        id:      'cardId',
                        current: false,
                        author:  'authorId',
                        project: 'projectId'
                    } ) );
                    assert( CardCollection.direct.update.calledWith( {
                        _id: 'mongoCardId'
                    }, {
                        $set: {
                            current: false,
                            deleted: true,
                            version: 123
                        }
                    } ) );
                    assert( CardFilesHelpers.commitLastVersion.calledWith( [
                        CardImagesCollection,
                        CardAttachmentsCollection
                    ], {
                        id:      'cardId',
                        version: 123
                    } ) );
                    assert( CardLinksCollection.unlinkArchivedCard.calledWith( 'userId', {
                        _id:     'mongoCardId',
                        id:      'cardId',
                        current: false,
                        author:  'authorId',
                        project: 'projectId',
                        version: 123
                    } ) );
                    assert.isFalse( Gamification.awardPoints.called );
                    assert( Audit.next.calledWith( {
                        domain:  CardCollection.entityName,
                        message: CardAuditMessages.archived,
                        userId:  context.userId,
                        cardId:  'cardId'
                    } ) );
                }
            } );

            if ( Meteor.isServer ) {

                it( "should remove reputation points when removing your own card", () => {
                    CardCollection.findOne.returns( { author: 'userId', project: 'projectId' } );
                    CardMethods.archive._execute( context, { cardId: 'cardId' } );
                    assert( Gamification.awardPoints.calledWith( 'userId', -Settings.shared.gamification.points.card.creation, 'projectId' ) );
                } );

            }

        } );

        describe( "Failing scenarios", () => {

            it( "Should throw if not allowed", () => {
                CardSecurity.canArchive.returns( false );

                expect( () => CardMethods.archive._execute( context, { cardId: 'cardId' } ) ).to.throw( /unauthorized/ );

                expect( CardCollection.findOne.called ).to.be.false;
                expect( CardCollection.direct.remove.called ).to.be.false;
                expect( CardCollection.snapshot.called ).to.be.false;
                expect( CardCollection.direct.update.called ).to.be.false;
                expect( CardFilesHelpers.commitLastVersion.called ).to.be.false;
                expect( CardLinksCollection.unlinkArchivedCard.called ).to.be.false;
                expect( Gamification.awardPoints.called ).to.be.false;
                expect( Audit.next.called ).to.be.false;
            } );

            it( "Should throw if card not found", () => {
                CardSecurity.canArchive.returns( true );
                CardCollection.findOne.returns( null );

                expect( () => CardMethods.archive._execute( context, { cardId: 'cardId' } ) ).to.throw( /card not found/ );

                assert( CardCollection.findOne.calledWith(
                    {
                        id:      'cardId',
                        current: true
                    },
                    {
                        transform: null
                    }
                ) );
                expect( CardCollection.direct.remove.called ).to.be.false;
                expect( CardCollection.snapshot.called ).to.be.false;
                expect( CardCollection.direct.update.called ).to.be.false;
                expect( CardFilesHelpers.commitLastVersion.called ).to.be.false;
                expect( CardLinksCollection.unlinkArchivedCard.called ).to.be.false;
                expect( Gamification.awardPoints.called ).to.be.false;
                expect( Audit.next.called ).to.be.false;
            } );

        } );

    } );

    describe( 'remove', () => {

        let context;

        beforeEach( () => {
            context = { userId: 'userId' };

            sandbox.stub( CardSecurity, 'canRemove' );
            sandbox.stub( CardCollection, 'findOne' );
            sandbox.stub( CardCollection, 'remove' );
            sandbox.stub( Gamification, 'awardPoints' );
        } );

        describe( "Happy scenarios", () => {

            beforeEach( () => {
                CardSecurity.canRemove.returns( true );
            } );

            it( "should remove card", () => {
                CardCollection.findOne.returns( {
                    _id:     'mongoCardId',
                    id:      'cardId',
                    author:  'authorId',
                    project: 'projectId'
                } );

                CardMethods.remove._execute( context, { cardId: 'cardId' } );

                assert( CardSecurity.canRemove.calledWith( context.userId, 'cardId' ) );
                assert( CardCollection.findOne.calledWith( {
                    id:      'cardId',
                    current: true
                }, { fields: { author: 1 }, transform: null } ) );

                assert( CardCollection.remove.calledWith( { id: 'cardId' } ) );

                assert.isFalse( Gamification.awardPoints.called );
                assert( Audit.next.calledWith( {
                    domain:  CardCollection.entityName,
                    message: CardAuditMessages.removed,
                    userId:  context.userId,
                    cardId:  'cardId'
                } ) );
            } );

            it( "should remove reputation points when removing your own card", () => {
                CardCollection.findOne.returns( { author: 'userId', project: 'projectId' } );
                CardMethods.remove._execute( context, { cardId: 'cardId' } );
                assert( Gamification.awardPoints.calledWith( 'userId', -Settings.shared.gamification.points.card.creation, 'projectId' ) );
            } );

        } );

        describe( "Failing scenarios", () => {

            it( "Should throw if not allowed", () => {
                CardSecurity.canRemove.returns( false );

                expect( () => CardMethods.remove._execute( context, { cardId: 'cardId' } ) ).to.throw( /unauthorized/ );

                expect( CardCollection.findOne.called ).to.be.false;
                expect( CardCollection.remove.called ).to.be.false;
                expect( Gamification.awardPoints.called ).to.be.false;
                expect( Audit.next.called ).to.be.false;
            } );

            it( "Should throw if card not found", () => {
                CardSecurity.canRemove.returns( true );
                CardCollection.findOne.returns( null );

                expect( () => CardMethods.remove._execute( context, { cardId: 'cardId' } ) ).to.throw( /card not found/ );

                assert( CardCollection.findOne.calledWith(
                    {
                        id:      'cardId',
                        current: true
                    },
                    {
                        fields:    { author: 1 },
                        transform: null
                    }
                ) );
                expect( CardCollection.remove.called ).to.be.false;
                expect( Gamification.awardPoints.called ).to.be.false;
                expect( Audit.next.called ).to.be.false;
            } );

        } );

    } );

    describe( 'update', () => {

        let context;

        beforeEach( () => {
            context = { userId: 'userId' };
            sandbox.stub( CardSecurity, 'canUpdate' );
            sandbox.stub( CardCollection, 'findOne' );
            sandbox.stub( ProjectCollection, 'findOne' );
            sandbox.stub( CardCollection, 'snapshot' );
            sandbox.stub( CardCollection, 'snapshotAndUpdate' );
            sandbox.stub( CardCollection, 'update' );
            sandbox.spy( CardAnalytics, 'heatModifier' );
            sandbox.stub( CardFilesHelpers, 'commitPending' );
            sandbox.stub( CardFilesHelpers, 'commitLastVersion' );
            sandbox.stub( CardAttachmentsCollection, 'updateAttachmentCount' );
        } );

        describe( "Happy scenarios", () => {

            let card;

            beforeEach( () => {
                card = {
                    id:              "cardId",
                    version:         1,
                    project:         "projectId",
                    phase:           "phaseId",
                    lastTemperature: 1,
                    heatedAt:        new Date()
                };
                CardSecurity.canUpdate.returns( true );
                CardCollection.findOne.returns( card );
                ProjectCollection.findOne.returns( {
                    phases: [
                        {
                            _id: "phaseId"
                        }
                    ]
                } );
            } );

            it( "should query the right card fields", () => {
                CardMethods.update._execute( context, { id: "cardId", changes: { name: "Test" } } );
                expect( CardCollection.findOne.calledWith( { id: "cardId", current: true }, {
                    fields:    {
                        id:              1,
                        version:         1,
                        project:         1,
                        phase:           1,
                        lastTemperature: 1,
                        heatedAt:        1
                    },
                    transform: null
                } ) );
            } );

            it( "should not snapshot if changes are empty", () => {
                CardMethods.update._execute( context, { id: "cardId" } );
                assert.isFalse( CardCollection.snapshot.called );
            } );

            it( "should not update if changes are empty", () => {
                CardMethods.update._execute( context, { id: "cardId" } );
                assert.isFalse( CardCollection.update.called );
            } );

            it( "should snapshot if there are changes", () => {
                CardMethods.update._execute( context, { id: "cardId", changes: { name: "Test" } } );
                assert( CardCollection.snapshot.called );
                assert( CardCollection.snapshot.calledWith( context.userId, "cardId" ) );
            } );

            it( "should update if there are changes", () => {
                CardCollection.snapshot.returns( 123 );
                CardMethods.update._execute( context, { id: "cardId", changes: { name: "Test" } } );
                assert( CardAnalytics.heatModifier.calledWith(
                    card,
                    Settings.shared.analytics.card.heat.update,
                    sinon.match( {
                        $set: sinon.match( {
                            name:    "Test",
                            version: 123
                        } )
                    } )
                ) );
                assert( CardCollection.update.called );
                assert( CardCollection.update.calledWith( {
                    id:      "cardId",
                    current: true
                }, {
                    $inc: {
                        totalTemperature: Settings.shared.analytics.card.heat.update
                    },
                    $set: {
                        name:            "Test",
                        version:         123,
                        lastTemperature: 1 + Settings.shared.analytics.card.heat.update,
                        heatedAt:        sinon.match.date
                    }
                } ) );
            } );

            it( "changing picture should trigger a commitPending", () => {
                CardMethods.update._execute( context, { id: "cardId", changedPicture: true } );
                assert( CardFilesHelpers.commitPending.calledWith(
                    CardImagesCollection,
                    "userId",
                    card,
                    null
                ) );
            } );

            it( "not changing picture (false) should not trigger a commitPending", () => {
                CardMethods.update._execute( context, { id: "cardId", addedFiles: false } );
                assert.isFalse( CardFilesHelpers.commitPending.called );
            } );

            it( "not changing picture (undefined) should not trigger a commitPending", () => {
                CardMethods.update._execute( context, { id: "cardId" } );
                assert.isFalse( CardFilesHelpers.commitPending.called );
            } );

            it( "removing picture should trigger a commitLastVersion", () => {
                CardMethods.update._execute( context, { id: "cardId", removedPicture: true } );
                assert( CardFilesHelpers.commitLastVersion.calledWith(
                    CardImagesCollection,
                    card
                ) );
            } );

            it( "removing picture (false) should not trigger a commitLastVersion", () => {
                CardMethods.update._execute( context, { id: "cardId", removedPicture: false } );
                assert.isFalse( CardFilesHelpers.commitLastVersion.called );
            } );

            it( "removing picture (undefined) should not trigger a commitLastVersion", () => {
                CardMethods.update._execute( context, { id: "cardId" } );
                assert.isFalse( CardFilesHelpers.commitLastVersion.called );
            } );

            it( "adding files should trigger a commitPending", () => {
                CardMethods.update._execute( context, { id: "cardId", addedFiles: true } );
                assert( CardFilesHelpers.commitPending.calledWith(
                    CardAttachmentsCollection,
                    "userId",
                    card,
                    null
                ) );
            } );

            it( "not adding files (false) should not trigger a commitPending", () => {
                CardMethods.update._execute( context, { id: "cardId", addedFiles: false } );
                assert.isFalse( CardFilesHelpers.commitPending.called );
            } );

            it( "not adding files (undefined) should not trigger a commitPending", () => {
                CardMethods.update._execute( context, { id: "cardId" } );
                assert.isFalse( CardFilesHelpers.commitPending.called );
            } );

            it( "adding files should trigger an attachment recount", () => {
                CardMethods.update._execute( context, { id: "cardId", addedFiles: true } );
                assert( CardAttachmentsCollection.updateAttachmentCount.calledWith(
                    "cardId",
                    1
                ) );
            } );

            it( "not adding files (false) should not trigger an attachment recount", () => {
                CardMethods.update._execute( context, { id: "cardId", addedFiles: false } );
                assert.isFalse( CardAttachmentsCollection.updateAttachmentCount.called );
            } );

            it( "not adding files (undefined) should not trigger an attachment recount", () => {
                CardMethods.update._execute( context, { id: "cardId" } );
                assert.isFalse( CardAttachmentsCollection.updateAttachmentCount.called );
            } );

            it( "removed files alone should not trigger an update", () => {
                CardMethods.update._execute( context, { id: "cardId", removedFiles: ['file1', 'file2'] } );
                assert.isFalse( CardCollection.update.called );
            } );

            it( "removed files should be committed to the last version", () => {
                CardMethods.update._execute( context, { id: "cardId", removedFiles: ['file1', 'file2'] } );
                assert( CardFilesHelpers.commitLastVersion.called );
                assert( CardFilesHelpers.commitLastVersion.calledWith( CardAttachmentsCollection, card, ['file1', 'file2'] ) );
                assert.isFalse( CardCollection.snapshotAndUpdate.called );
            } );

            it( "removed files should not trigger a snapshot if if did nothing", () => {
                CardFilesHelpers.commitLastVersion.returns( false );
                CardMethods.update._execute( context, { id: "cardId", removedFiles: ['file1', 'file2'] } );
                assert.isFalse( CardCollection.snapshotAndUpdate.called );
            } );

            it( "removed files should trigger a snapshot if it removed something and not already happened", () => {
                CardFilesHelpers.commitLastVersion.returns( true );
                CardMethods.update._execute( context, { id: "cardId", removedFiles: ['file1', 'file2'] } );
                assert( CardCollection.snapshotAndUpdate.called );
                assert( CardCollection.snapshotAndUpdate.calledWith( "userId", "cardId" ) );
            } );

            it( "removed files should not trigger a snapshot if it already happened", () => {
                CardCollection.snapshot.returns( 123 );
                CardFilesHelpers.commitLastVersion.returns( true );
                CardMethods.update._execute( context, {
                    id:           "cardId",
                    changes:      { name: "Test" },
                    removedFiles: ['file1', 'file2']
                } );
                assert.isFalse( CardCollection.snapshotAndUpdate.called );
            } );

            it( "removing files should trigger an attachment recount", () => {
                CardMethods.update._execute( context, { id: "cardId", removedFiles: ['file1', 'file2'] } );
                assert( CardAttachmentsCollection.updateAttachmentCount.calledWith(
                    "cardId",
                    1
                ) );
            } );

            it( "not removing files (empty) should not trigger an attachment recount", () => {
                CardMethods.update._execute( context, { id: "cardId", removedFiles: [] } );
                assert.isFalse( CardAttachmentsCollection.updateAttachmentCount.called );
            } );

            it( "not removing files (undefined) should not trigger an attachment recount", () => {
                CardMethods.update._execute( context, { id: "cardId" } );
                assert.isFalse( CardAttachmentsCollection.updateAttachmentCount.called );
            } );

        } );

        describe( "Failing scenarios", () => {

            it( "Should throw if not allowed", () => {
                CardSecurity.canUpdate.returns( false );
                expect( () => CardMethods.update._execute( context, { id: "cardId" } ) ).to.throw( /unauthorized/ );
            } );

            it( "Should throw if card is not found", () => {
                CardSecurity.canUpdate.returns( true );
                CardCollection.findOne.returns( null );
                expect( () => CardMethods.update._execute( context, { id: "cardId" } ) ).to.throw( /card not found/ );
            } );

            it( "Should throw if project is not found", () => {
                CardSecurity.canUpdate.returns( true );
                CardCollection.findOne.returns( { _id: "cardId", version: 1, project: "projectId", phase: "phaseId" } );
                ProjectCollection.findOne.returns( null );
                expect( () => CardMethods.update._execute( context, { id: "cardId" } ) ).to.throw( /project not found/ );
            } );

            it( "Should throw if phase is not found (no phases in project)", () => {
                CardSecurity.canUpdate.returns( true );
                CardCollection.findOne.returns( { _id: "cardId", version: 1, project: "projectId", phase: "phaseId" } );
                ProjectCollection.findOne.returns( { _id: "projectId" } );
                expect( () => CardMethods.update._execute( context, { id: "cardId" } ) ).to.throw( /phase not found/ );
            } );

            it( "Should throw if phase is not found", () => {
                CardSecurity.canUpdate.returns( true );
                CardCollection.findOne.returns( { _id: "cardId", version: 1, project: "projectId", phase: "phaseId" } );
                ProjectCollection.findOne.returns( { _id: "projectId", phases: [{ _id: "otherPhaseId" }] } );
                expect( () => CardMethods.update._execute( context, { id: "cardId" } ) ).to.throw( /phase not found/ );
            } );

            //TODO: Phase Types
        } );
    } );

    describe( 'view', () => {

        let context;
        let card;

        beforeEach( () => {
            context = { userId: 'userId' };
            sandbox.stub( CardSecurity, 'canRead' );
            sandbox.stub( CardCollection, 'findOne' );
            sandbox.stub( CardCollection, 'update' );
            sandbox.spy( CardAnalytics, 'heatModifier' );
        } );

        afterEach( () => {
            card = null;
        } );

        if ( Meteor.isServer ) {

            it( 'should record a card view', () => {
                // SETUP
                card = {
                    id:              'cardId',
                    author:          'authorId',
                    lastTemperature: 1,
                    heatedAt:        new Date()
                };
                CardSecurity.canRead.returns( true );
                CardCollection.findOne.returns( card );

                // ACTION
                expect( () => CardMethods.view._execute( context, { cardId: 'cardId' } ) ).to.not.throw();

                // TESTS
                assert( CardSecurity.canRead.calledWith( 'userId', 'cardId' ) );
                assert( CardCollection.findOne.calledWith( { id: 'cardId', current: true }, {
                    fields:    { id: 1, author: 1, lastTemperature: 1, heatedAt: 1 },
                    transform: null
                } ) );
                assert( CardAnalytics.heatModifier.calledWith(
                    card,
                    Settings.shared.analytics.card.heat.view,
                    sinon.match.object
                ) );
                assert( CardCollection.update.calledWith( { id: 'cardId', current: true }, {
                    $inc: { viewCount: 1, totalTemperature: Settings.shared.analytics.card.heat.view },
                    $set: { lastTemperature: 1 + Settings.shared.analytics.card.heat.view, heatedAt: sinon.match.date }
                } ) );
            } );

            it( 'should not record a card view from the card author', () => {
                // SETUP
                card = {
                    id:     'cardId',
                    author: 'userId'
                };
                CardSecurity.canRead.returns( true );
                CardCollection.findOne.returns( card );

                // ACTION
                expect( () => CardMethods.view._execute( context, { cardId: 'cardId' } ) ).to.not.throw();

                // TESTS
                assert( CardSecurity.canRead.calledWith( 'userId', 'cardId' ) );
                assert( CardCollection.findOne.calledWith( { id: 'cardId', current: true }, {
                    fields:    { id: 1, author: 1, lastTemperature: 1, heatedAt: 1 },
                    transform: null
                } ) );
                assert.isFalse( CardAnalytics.heatModifier.called );
                assert.isFalse( CardCollection.update.called );
            } );

            it( 'should throw if not allowed', () => {
                // SETUP
                CardSecurity.canRead.returns( false );

                // ACTION
                expect( () => CardMethods.view._execute( context, { cardId: 'cardId' } ) ).to.throw( /unauthorized/ );

                // TESTS
                assert( CardSecurity.canRead.calledWith( 'userId', 'cardId' ) );
                assert.isFalse( CardCollection.findOne.called );
                assert.isFalse( CardAnalytics.heatModifier.called );
                assert.isFalse( CardCollection.update.called );
            } );

        }

        function expectCheckFailed() {
            assert.isFalse( CardSecurity.canRead.called );
            assert.isFalse( CardCollection.findOne.called );
            assert.isFalse( CardAnalytics.heatModifier.called );
            assert.isFalse( CardCollection.update.called );
        }

        it( 'should fail on extra parameter', () => {
            expect( () => CardMethods.view._execute( context, {
                cardId: 123,
                extra:  'parameter'
            } ) ).to.throw( /validation-error/ );
            expectCheckFailed();
        } );

        it( 'should fail on invalid cardId (number)', () => {
            expect( () => CardMethods.view._execute( context, { cardId: 123 } ) ).to.throw( /validation-error/ );
            expectCheckFailed();
        } );

        it( 'should fail on invalid cardId (null)', () => {
            expect( () => CardMethods.view._execute( context, { cardId: null } ) ).to.throw( /validation-error/ );
            expectCheckFailed();
        } );

        it( 'should fail on missing parameters', () => {
            expect( () => CardMethods.view._execute( context ) ).to.throw( /must be an object/ );
            expectCheckFailed();
        } );

    } );

    describe( 'like', () => {

        let context;
        let card;

        beforeEach( () => {
            context = { userId: 'userId' };
            sandbox.stub( CardSecurity, 'canLike' );
            sandbox.stub( CardCollection, 'findOne' );
            sandbox.stub( CardCollection, 'update' );
            sandbox.spy( CardAnalytics, 'heatModifier' );
            sandbox.spy( Gamification, 'awardPoints' );
        } );

        afterEach( () => {
            card = null;
        } );

        it( 'should like a card', () => {
            // SETUP
            card = {
                id:              'cardId',
                author:          'authorId',
                project:         'projectId',
                lastTemperature: 1,
                heatedAt:        new Date(),
                likes:           [
                    'likedUserId'
                ]
            };
            CardSecurity.canLike.returns( true );
            CardCollection.findOne.returns( card );

            // ACTION
            expect( () => CardMethods.like._execute( context, { cardId: 'cardId' } ) ).to.not.throw();

            // TESTS
            assert( CardSecurity.canLike.calledWith( 'userId', 'cardId' ) );
            assert( CardCollection.findOne.calledWith(
                {
                    id:      'cardId',
                    current: true
                },
                {
                    fields:    {
                        author:          1,
                        project:         1,
                        likes:           1,
                        lastTemperature: 1,
                        heatedAt:        1
                    },
                    transform: null
                }
            ) );
            assert( CardAnalytics.heatModifier.calledWith(
                card,
                Settings.shared.analytics.card.heat.like,
                sinon.match.object
            ) );
            assert( Gamification.awardPoints.calledWith( 'userId', Settings.shared.gamification.points.card.like, 'projectId' ) );
            if ( Meteor.isServer ) {
                assert( CardCollection.update.calledWith( { id: 'cardId', current: true }, {
                    $addToSet: { likes: 'userId' },
                    $inc:      { totalTemperature: Settings.shared.analytics.card.heat.like },
                    $set:      {
                        likeCount:       2,
                        lastTemperature: 1 + Settings.shared.analytics.card.heat.like,
                        heatedAt:        sinon.match.date
                    }
                } ) );
            } else {
                assert( CardCollection.update.calledWith( { id: 'cardId', current: true }, {
                    $addToSet: { likes: 'userId' },
                    $inc:      { likeCount: 1, totalTemperature: Settings.shared.analytics.card.heat.like },
                    $set:      {
                        lastTemperature: 1 + Settings.shared.analytics.card.heat.like,
                        heatedAt:        sinon.match.date
                    }
                } ) );
            }
            assert( Audit.next.calledWith( {
                domain:  'card',
                message: 'liked',
                userId:  'userId',
                cardId:  'cardId'
            } ) );
        } );

        it( 'should not heat card nor get points if self like', () => {
            // SETUP
            card = {
                id:              'cardId',
                author:          'userId',
                lastTemperature: 1,
                heatedAt:        new Date(),
                likes:           [
                    'likedUserId'
                ]
            };
            CardSecurity.canLike.returns( true );
            CardCollection.findOne.returns( card );

            // ACTION
            expect( () => CardMethods.like._execute( context, { cardId: 'cardId' } ) ).to.not.throw();

            // TESTS
            assert.isFalse( CardAnalytics.heatModifier.called );
            assert.isFalse( Gamification.awardPoints.called );
        } );

        it( 'should unlike a card', () => {
            // SETUP
            card = {
                id:              'cardId',
                author:          'authorId',
                project:         'projectId',
                lastTemperature: 2,
                heatedAt:        new Date(),
                likes:           [
                    'userId'
                ]
            };
            CardSecurity.canLike.returns( true );
            CardCollection.findOne.returns( card );

            // ACTION
            expect( () => CardMethods.like._execute( context, { cardId: 'cardId' } ) ).to.not.throw();

            // TESTS
            assert( CardSecurity.canLike.calledWith( 'userId', 'cardId' ) );
            assert( CardCollection.findOne.calledWith(
                {
                    id:      'cardId',
                    current: true
                },
                {
                    fields:    {
                        author:          1,
                        project:         1,
                        likes:           1,
                        lastTemperature: 1,
                        heatedAt:        1
                    },
                    transform: null
                }
            ) );
            if ( Meteor.isServer ) {
                assert( CardCollection.update.calledWith( { id: 'cardId', current: true }, {
                    $pull: { likes: 'userId' },
                    $inc:  { totalTemperature: -Settings.shared.analytics.card.heat.like },
                    $set:  {
                        likeCount:       0,
                        lastTemperature: Math.max( 0, 2 - Settings.shared.analytics.card.heat.like ),
                        heatedAt:        sinon.match.date
                    }
                } ) );
            } else {
                assert( CardCollection.update.calledWith( { id: 'cardId', current: true }, {
                    $pull: { likes: 'userId' },
                    $inc:  { likeCount: -1, totalTemperature: -Settings.shared.analytics.card.heat.like },
                    $set:  {
                        lastTemperature: Math.max( 0, 2 - Settings.shared.analytics.card.heat.like ),
                        heatedAt:        sinon.match.date
                    }
                } ) );
            }

            assert( Gamification.awardPoints.calledWith( 'userId', -Settings.shared.gamification.points.card.like, 'projectId' ) );

            assert( Audit.next.calledWith( {
                domain:  'card',
                message: 'unliked',
                userId:  'userId',
                cardId:  'cardId'
            } ) );
        } );

        it( 'should not cool card nor remove points if self unlike', () => {
            // SETUP
            card = {
                id:              'cardId',
                author:          'userId',
                lastTemperature: 1,
                heatedAt:        new Date(),
                likes:           [
                    'userId'
                ]
            };
            CardSecurity.canLike.returns( true );
            CardCollection.findOne.returns( card );

            // ACTION
            expect( () => CardMethods.like._execute( context, { cardId: 'cardId' } ) ).to.not.throw();

            // TESTS
            assert.isFalse( CardAnalytics.heatModifier.called );
            assert.isFalse( Gamification.awardPoints.called );
        } );

        it( 'should throw if not allowed', () => {
            // SETUP
            CardSecurity.canLike.returns( false );

            // ACTION
            expect( () => CardMethods.like._execute( context, { cardId: 'cardId' } ) ).to.throw( /unauthorized/ );

            // TESTS
            assert( CardSecurity.canLike.calledWith( 'userId', 'cardId' ) );
            assert.isFalse( CardCollection.findOne.called );
            assert.isFalse( CardCollection.update.called );
            assert.isFalse( Audit.next.called );
        } );

        function expectCheckFailed() {
            assert.isFalse( CardCollection.findOne.called );
            assert.isFalse( CardSecurity.canLike.called );
            assert.isFalse( CardAnalytics.heatModifier.called );
            assert.isFalse( Gamification.awardPoints.called );
            assert.isFalse( Audit.next.called );
        }

        it( 'should fail on extra parameters', () => {
            expect( () => CardMethods.like._execute( context, {
                cardId: 'cardId',
                extra:  'parameter'
            } ) ).to.throw( /validation-error/ );
            expectCheckFailed();
        } );

        it( 'should fail on invalid cardId (number)', () => {
            expect( () => CardMethods.like._execute( context, { cardId: 123 } ) ).to.throw( /validation-error/ );
            expectCheckFailed();
        } );

        it( 'should fail on invalid cardId (null)', () => {
            expect( () => CardMethods.like._execute( context, { cardId: null } ) ).to.throw( /validation-error/ );
            expectCheckFailed();
        } );

        it( 'should fail on missing parameters', () => {
            expect( () => CardMethods.like._execute( context ) ).to.throw( /must be an object/ );
            expectCheckFailed();
        } );

    } );

    describe( 'link', () => {

        let context;

        beforeEach( () => {
            context = { userId: 'userId' };
            sandbox.stub( CardSecurity, 'canLink' );
            sandbox.stub( CardCollection, 'findOne' );
            sandbox.stub( CardLinksCollection, 'findOne' );
            sandbox.stub( CardCollection, 'snapshotAndUpdate' );
            sandbox.stub( CardLinksCollection, 'insert' );
            sandbox.stub( CardLinksCollection, 'update' );
            sandbox.stub( CardAnalytics, 'heat' );
            sandbox.stub( Gamification, 'awardPoints' );
        } );

        it( 'should link cards', () => {
            // SETUP
            const sourceCard      = { id: 'sourceCardId', project: 'projectId', version: 1 };
            const destinationCard = { id: 'destinationCardId', project: 'projectId', version: 2 };
            CardCollection.findOne.onFirstCall().returns( _.clone( sourceCard ) );
            CardCollection.findOne.onSecondCall().returns( _.clone( destinationCard ) );
            CardSecurity.canLink.returns( true );
            CardLinksCollection.findOne.returns( null );
            CardCollection.snapshotAndUpdate.onFirstCall().returns( 11 );
            CardCollection.snapshotAndUpdate.onSecondCall().returns( 22 );

            // ACTION
            expect( () => CardMethods.linkCards._execute( context, {
                sourceCardId:      'sourceCardId',
                linkType:          CardLinkTypes.references.id,
                destinationCardId: 'destinationCardId'
            } ) ).to.not.throw();

            // TEST
            expect( CardCollection.findOne.callCount ).to.eql( 2 );
            assert( CardCollection.findOne.getCall( 0 ).calledWith(
                { id: 'sourceCardId', current: true },
                {
                    transform: null
                }
            ) );
            assert( CardCollection.findOne.getCall( 1 ).calledWith(
                { id: 'destinationCardId', current: true },
                {
                    transform: null
                }
            ) );

            assert( CardSecurity.canLink.calledWith( 'userId',
                sourceCard,
                destinationCard
            ) );

            assert( CardLinksCollection.findOne.calledOnce );
            assert( CardLinksCollection.findOne.calledWith( {
                'source.id':               'sourceCardId',
                'source.versionFrom':      { $lte: 1 },
                'source.versionTo':        { $exists: false },
                'destination.id':          'destinationCardId',
                'destination.versionFrom': { $lte: 2 },
                'destination.versionTo':   { $exists: false }
            } ) );
            expect( CardCollection.snapshotAndUpdate.callCount ).to.eql( 2 );
            assert( CardCollection.snapshotAndUpdate.getCall( 0 ).calledWith( 'userId', sourceCard ) );
            assert( CardCollection.snapshotAndUpdate.getCall( 1 ).calledWith( 'userId', destinationCard ) );
            assert( CardLinksCollection.insert.calledWith( {
                source:      {
                    id:          'sourceCardId',
                    versionFrom: 11
                },
                destination: {
                    id:          'destinationCardId',
                    versionFrom: 22
                },
                type:        CardLinkTypes.references.id,
                linkedAt:    sinon.match.date,
                linkedBy:    'userId'
            } ) );
            expect( CardAnalytics.heat.callCount ).to.equal( 2 );
            assert( CardAnalytics.heat.getCall( 0 ).calledWith(
                sourceCard,
                Settings.shared.analytics.card.heat.linkSource
            ) );
            assert( CardAnalytics.heat.getCall( 1 ).calledWith(
                destinationCard,
                Settings.shared.analytics.card.heat.linkDestination
            ) );
            assert( Gamification.awardPoints.calledWith(
                'userId',
                Settings.shared.gamification.points.card.link,
                'projectId'
            ) );
            assert.isFalse( CardLinksCollection.update.called );

            assert( Audit.next.calledWith( {
                domain:            'card',
                message:           'linked',
                userId:            'userId',
                sourceCardId:      'sourceCardId',
                destinationCardId: 'destinationCardId',
                linkType:          CardLinkTypes.references.id
            } ) );
        } );

        it( 'should not heat source card if linked by the author', () => {
            // SETUP
            const sourceCard      = { id: 'sourceCardId', project: 'projectId', author: 'userId', version: 1 };
            const destinationCard = { id: 'destinationCardId', project: 'projectId', author: 'authorId', version: 2 };
            CardCollection.findOne.onFirstCall().returns( _.clone( sourceCard ) );
            CardCollection.findOne.onSecondCall().returns( _.clone( destinationCard ) );
            CardSecurity.canLink.returns( true );
            CardLinksCollection.findOne.returns( null );

            // ACTION
            expect( () => CardMethods.linkCards._execute( context, {
                sourceCardId:      'sourceCardId',
                linkType:          CardLinkTypes.references.id,
                destinationCardId: 'destinationCardId'
            } ) ).to.not.throw();

            // TEST
            expect( CardAnalytics.heat.callCount ).to.equal( 1 );
            assert( CardAnalytics.heat.calledWith(
                destinationCard,
                Settings.shared.analytics.card.heat.linkDestination
            ) );
            assert( Gamification.awardPoints.calledWith(
                'userId',
                Settings.shared.gamification.points.card.link,
                'projectId'
            ) );
        } );

        it( 'should not heat destination card if linked by the author', () => {
            // SETUP
            const sourceCard      = { id: 'sourceCardId', project: 'projectId', author: 'authorId', version: 1 };
            const destinationCard = { id: 'destinationCardId', project: 'projectId', author: 'userId', version: 2 };
            CardCollection.findOne.onFirstCall().returns( _.clone( sourceCard ) );
            CardCollection.findOne.onSecondCall().returns( _.clone( destinationCard ) );
            CardSecurity.canLink.returns( true );
            CardLinksCollection.findOne.returns( null );

            // ACTION
            expect( () => CardMethods.linkCards._execute( context, {
                sourceCardId:      'sourceCardId',
                linkType:          CardLinkTypes.references.id,
                destinationCardId: 'destinationCardId'
            } ) ).to.not.throw();

            // TEST
            expect( CardAnalytics.heat.callCount ).to.equal( 1 );
            assert( CardAnalytics.heat.calledWith(
                sourceCard,
                Settings.shared.analytics.card.heat.linkSource
            ) );
            assert( Gamification.awardPoints.calledWith(
                'userId',
                Settings.shared.gamification.points.card.link,
                'projectId'
            ) );
        } );

        it( 'should not get reputation points from linking two of your own cards', () => {
            // SETUP
            const sourceCard      = { id: 'sourceCardId', project: 'projectId', author: 'userId', version: 1 };
            const destinationCard = { id: 'destinationCardId', project: 'projectId', author: 'userId', version: 2 };
            CardCollection.findOne.onFirstCall().returns( _.clone( sourceCard ) );
            CardCollection.findOne.onSecondCall().returns( _.clone( destinationCard ) );
            CardSecurity.canLink.returns( true );
            CardLinksCollection.findOne.returns( null );

            // ACTION
            expect( () => CardMethods.linkCards._execute( context, {
                sourceCardId:      'sourceCardId',
                linkType:          CardLinkTypes.references.id,
                destinationCardId: 'destinationCardId'
            } ) ).to.not.throw();

            // TEST
            assert.isFalse( CardAnalytics.heat.called );
            assert.isFalse( Gamification.awardPoints.called );
        } );

        it( 'should update a card link', () => {
            // SETUP
            const sourceCard      = { id: 'sourceCardId', project: 'projectId', version: 1 };
            const destinationCard = { id: 'destinationCardId', project: 'projectId', version: 2 };
            CardCollection.findOne.onFirstCall().returns( _.clone( sourceCard ) );
            CardCollection.findOne.onSecondCall().returns( _.clone( destinationCard ) );
            CardSecurity.canLink.returns( true );
            CardLinksCollection.findOne.returns( { _id: 'linkId' } );

            // ACTION
            expect( () => CardMethods.linkCards._execute( context, {
                sourceCardId:      'sourceCardId',
                linkType:          CardLinkTypes.references.id,
                destinationCardId: 'destinationCardId'
            } ) ).to.not.throw();

            // TEST
            expect( CardCollection.findOne.callCount ).to.eql( 2 );
            assert( CardCollection.findOne.getCall( 0 ).calledWith(
                { id: 'sourceCardId', current: true },
                {
                    transform: null
                }
            ) );
            assert( CardCollection.findOne.getCall( 1 ).calledWith(
                { id: 'destinationCardId', current: true },
                {
                    transform: null
                }
            ) );

            assert( CardSecurity.canLink.calledWith( 'userId',
                sourceCard,
                destinationCard
            ) );

            assert( CardLinksCollection.findOne.calledOnce );
            assert( CardLinksCollection.findOne.calledWith( {
                'source.id':               'sourceCardId',
                'source.versionFrom':      { $lte: 1 },
                'source.versionTo':        { $exists: false },
                'destination.id':          'destinationCardId',
                'destination.versionFrom': { $lte: 2 },
                'destination.versionTo':   { $exists: false }
            } ) );
            assert( CardLinksCollection.update.calledWith(
                {
                    _id: 'linkId',
                }, {
                    $set: {
                        type:      CardLinkTypes.references.id,
                        updatedAt: sinon.match.date,
                        updatedBy: 'userId'
                    }
                }
            ) );
            assert.isFalse( CardAnalytics.heat.called );
            assert.isFalse( Gamification.awardPoints.called );
            assert.isFalse( CardCollection.snapshotAndUpdate.called );
            assert.isFalse( CardLinksCollection.insert.called );
        } );

        it( 'should throw if not allowed', () => {
            // SETUP
            CardCollection.findOne.onFirstCall().returns( { id: 'sourceCardId', project: 'projectId' } );
            CardCollection.findOne.onSecondCall().returns( { id: 'destinationCardId', project: 'projectId' } );
            CardSecurity.canLink.returns( false );

            // ACTION
            expect( () => CardMethods.linkCards._execute( context, {
                sourceCardId:      'sourceCardId',
                destinationCardId: 'destinationCardId',
                linkType:          CardLinkTypes.references.id
            } ) ).to.throw( /unauthorized/ );

            // TESTS
            expect( CardCollection.findOne.callCount ).to.eql( 2 );
            assert( CardCollection.findOne.getCall( 0 ).calledWith(
                { id: 'sourceCardId', current: true },
                {
                    transform: null
                }
            ) );
            assert( CardCollection.findOne.getCall( 1 ).calledWith(
                { id: 'destinationCardId', current: true },
                {
                    transform: null
                }
            ) );
            assert( CardSecurity.canLink.calledWith( 'userId',
                { id: 'sourceCardId', project: 'projectId' },
                { id: 'destinationCardId', project: 'projectId' }
            ) );

            assert.isFalse( CardCollection.snapshotAndUpdate.called );
            assert.isFalse( CardLinksCollection.findOne.called );
            assert.isFalse( CardLinksCollection.insert.called );
            assert.isFalse( CardLinksCollection.update.called );
            assert.isFalse( CardAnalytics.heat.called );
            assert.isFalse( Gamification.awardPoints.called );
            assert.isFalse( Audit.next.called );
        } );

        it( 'should throw if sourceCard is not found', () => {
            // SETUP
            CardCollection.findOne.returns( null );

            // ACTION
            expect( () => CardMethods.linkCards._execute( context, {
                sourceCardId:      'sourceCardId',
                destinationCardId: 'destinationCardId',
                linkType:          CardLinkTypes.references.id
            } ) ).to.throw( /card not found/ );

            // TESTS
            expect( CardCollection.findOne.callCount ).to.eql( 1 );
            assert( CardCollection.findOne.getCall( 0 ).calledWith(
                { id: 'sourceCardId', current: true },
                {
                    transform: null
                }
            ) );
            assert.isFalse( CardSecurity.canLink.called );
            assert.isFalse( CardCollection.snapshotAndUpdate.called );
            assert.isFalse( CardLinksCollection.findOne.called );
            assert.isFalse( CardLinksCollection.insert.called );
            assert.isFalse( CardLinksCollection.update.called );
            assert.isFalse( CardAnalytics.heat.called );
            assert.isFalse( Gamification.awardPoints.called );
            assert.isFalse( Audit.next.called );
        } );

        it( 'should throw if destinationCard is not found', () => {
            // SETUP
            CardCollection.findOne.onFirstCall().returns( { id: 'sourceCardId' } );
            CardCollection.findOne.onSecondCall().returns( null );

            // ACTION
            expect( () => CardMethods.linkCards._execute( context, {
                sourceCardId:      'sourceCardId',
                destinationCardId: 'destinationCardId',
                linkType:          CardLinkTypes.references.id
            } ) ).to.throw( /card not found/ );

            // TEST
            expect( CardCollection.findOne.callCount ).to.eql( 2 );
            assert( CardCollection.findOne.getCall( 0 ).calledWith(
                { id: 'sourceCardId', current: true },
                {
                    transform: null
                }
            ) );
            assert( CardCollection.findOne.getCall( 1 ).calledWith(
                { id: 'destinationCardId', current: true },
                {
                    transform: null
                }
            ) );
            assert.isFalse( CardSecurity.canLink.called );
            assert.isFalse( CardCollection.snapshotAndUpdate.called );
            assert.isFalse( CardLinksCollection.findOne.called );
            assert.isFalse( CardLinksCollection.insert.called );
            assert.isFalse( CardLinksCollection.update.called );
            assert.isFalse( CardAnalytics.heat.called );
            assert.isFalse( Gamification.awardPoints.called );
            assert.isFalse( Audit.next.called );
        } );

        it( 'should throw if cards are the same', () => {
            // ACTION
            expect( () => CardMethods.linkCards._execute( context, {
                sourceCardId:      'cardId',
                destinationCardId: 'cardId',
                linkType:          CardLinkTypes.references.id
            } ) ).to.throw( /cannot link card to itself/ );

            // TEST
            assert.isFalse( CardCollection.findOne.called );
            assert.isFalse( CardSecurity.canLink.called );
            assert.isFalse( CardCollection.snapshotAndUpdate.called );
            assert.isFalse( CardLinksCollection.findOne.called );
            assert.isFalse( CardLinksCollection.insert.called );
            assert.isFalse( CardLinksCollection.update.called );
            assert.isFalse( CardAnalytics.heat.called );
            assert.isFalse( Gamification.awardPoints.called );
            assert.isFalse( Audit.next.called );
        } );

        it( 'should throw if cards are from different projects', () => {
            // SETUP
            CardCollection.findOne.onFirstCall().returns( { id: 'sourceCardId', project: 'project1' } );
            CardCollection.findOne.onSecondCall().returns( { id: 'destinationCardId', project: 'project2' } );

            // ACTION
            expect( () => CardMethods.linkCards._execute( context, {
                sourceCardId:      'sourceCardId',
                destinationCardId: 'destinationCardId',
                linkType:          CardLinkTypes.references.id
            } ) ).to.throw( /cards not from same project/ );

            // TEST
            expect( CardCollection.findOne.callCount ).to.eql( 2 );
            assert( CardCollection.findOne.getCall( 0 ).calledWith(
                { id: 'sourceCardId', current: true },
                {
                    transform: null
                }
            ) );
            assert( CardCollection.findOne.getCall( 1 ).calledWith(
                { id: 'destinationCardId', current: true },
                {
                    transform: null
                }
            ) );
            assert.isFalse( CardSecurity.canLink.called );
            assert.isFalse( CardCollection.snapshotAndUpdate.called );
            assert.isFalse( CardLinksCollection.findOne.called );
            assert.isFalse( CardLinksCollection.insert.called );
            assert.isFalse( CardLinksCollection.update.called );
            assert.isFalse( CardAnalytics.heat.called );
            assert.isFalse( Gamification.awardPoints.called );
            assert.isFalse( Audit.next.called );
        } );

        function expectCheckFailed() {
            assert.isFalse( CardCollection.findOne.called );
            assert.isFalse( CardSecurity.canLink.called );
            assert.isFalse( CardCollection.snapshotAndUpdate.called );
            assert.isFalse( CardLinksCollection.findOne.called );
            assert.isFalse( CardLinksCollection.insert.called );
            assert.isFalse( CardLinksCollection.update.called );
            assert.isFalse( CardAnalytics.heat.called );
            assert.isFalse( Gamification.awardPoints.called );
            assert.isFalse( Audit.next.called );
        }

        it( 'should fail on invalid sourceCardId (null)', () => {
            expect( () => CardMethods.linkCards._execute( context, {
                sourceCardId:      null,
                destinationCardId: 'destinationCardId',
                linkType:          CardLinkTypes.references.id
            } ) ).to.throw( /validation-error/ );
            expectCheckFailed();
        } );

        it( 'should fail on invalid sourceCardId (undefined)', () => {
            expect( () => CardMethods.linkCards._execute( context, {
                destinationCardId: 'destinationCardId',
                linkType:          CardLinkTypes.references.id
            } ) ).to.throw( /validation-error/ );
            expectCheckFailed();
        } );

        it( 'should fail on invalid toCard (null)', () => {
            expect( () => CardMethods.linkCards._execute( context, {
                sourceCardId:      'sourceCardId',
                destinationCardId: null,
                linkType:          CardLinkTypes.references.id
            } ) ).to.throw( /validation-error/ );
            expectCheckFailed();
        } );

        it( 'should fail on invalid toCard (undefined)', () => {
            expect( () => CardMethods.linkCards._execute( context, {
                sourceCardId: 'sourceCardId',
                linkType:     CardLinkTypes.references.id
            } ) ).to.throw( /validation-error/ );
            expectCheckFailed();
        } );

        it( 'should fail on invalid linkType (invalid value)', () => {
            expect( () => CardMethods.linkCards._execute( context, {
                sourceCardId:      'sourceCardId',
                destinationCardId: 'destinationCardId',
                linkType:          'pouet'
            } ) ).to.throw( /validation-error/ );
            expectCheckFailed();
        } );

        it( 'should fail on invalid linkType (null)', () => {
            expect( () => CardMethods.linkCards._execute( context, {
                sourceCardId:      'sourceCardId',
                destinationCardId: 'destinationCardId',
                linkType:          null
            } ) ).to.throw( /validation-error/ );
            expectCheckFailed();
        } );

        it( 'should fail on invalid linkType (undefined)', () => {
            expect( () => CardMethods.linkCards._execute( context, {
                sourceCardId:      'sourceCardId',
                destinationCardId: 'destinationCardId'
            } ) ).to.throw( /validation-error/ );
            expectCheckFailed();
        } );

        it( 'should fail on missing parameters', () => {
            expect( () => CardMethods.linkCards._execute( context, {} ) ).to.throw( /validation-error/ );
            expectCheckFailed();
        } );

    } );

    describe( 'unlink', () => {

        let context;

        beforeEach( () => {
            context = { userId: "userId" };
            sandbox.stub( CardSecurity, 'canUnlink' );
            sandbox.stub( CardCollection, 'findOne' );
            sandbox.stub( CardCollection, 'snapshotAndUpdate' );
            sandbox.stub( CardLinksCollection, 'findOne' );
            sandbox.stub( CardLinksCollection, 'update' );
            sandbox.stub( CardAnalytics, 'cool' );
            sandbox.stub( Gamification, 'awardPoints' );
        } );

        it( "should unlink two cards", () => {
            const link        = {
                _id:         'linkId',
                source:      { id: 'sourceCardId' },
                destination: { id: 'destinationCardId' },
                type:        'linkType',
                linkedBy:    'linkerId',
                linkedAt:    new Date()
            };
            const source      = { id: 'sourceCardId', version: 3 };
            const destination = { id: 'destinationCardId', version: 4 };
            CardLinksCollection.findOne.returns( link );
            CardCollection.findOne.onFirstCall().returns( _.clone( source ) );
            CardCollection.findOne.onSecondCall().returns( _.clone( destination ) );
            CardSecurity.canUnlink.returns( true );

            expect( () => CardMethods.unlink._execute( context, { linkId: 'linkId' } ) ).not.to.throw();

            assert( CardLinksCollection.findOne.calledWith(
                { _id: 'linkId' },
                { fields: { _id: 1, source: 1, destination: 1, type: 1, linkedBy: 1, linkedAt: 1 } }
            ) );
            expect( CardCollection.findOne.callCount ).to.eql( 2 );
            assert( CardCollection.findOne.getCall( 0 ).calledWith( { id: 'sourceCardId', current: true }, {
                transform: null
            } ) );
            assert( CardCollection.findOne.getCall( 1 ).calledWith( { id: 'destinationCardId', current: true }, {
                transform: null
            } ) );
            assert( CardSecurity.canUnlink.calledWith( "userId", source, destination ) );
            assert( CardLinksCollection.update.calledWith(
                {
                    _id: link._id
                },
                {
                    $set: {
                        'source.versionTo':      3,
                        'destination.versionTo': 4,
                        unlinkedAt:              sinon.match.date,
                        unlinkedBy:              'userId'
                    }
                }
            ) );
            expect( CardCollection.snapshotAndUpdate.callCount ).to.eql( 2 );
            assert( CardCollection.snapshotAndUpdate.getCall( 0 ).calledWith( 'userId', source ) );
            assert( CardCollection.snapshotAndUpdate.getCall( 1 ).calledWith( 'userId', destination ) );
            expect( CardAnalytics.cool.callCount ).to.equal( 2 );
            assert( CardAnalytics.cool.getCall( 0 ).calledWith( source, link.linkedAt, Settings.shared.analytics.card.heat.linkSource ) );
            assert( CardAnalytics.cool.getCall( 1 ).calledWith( destination, link.linkedAt, Settings.shared.analytics.card.heat.linkDestination ) );
            assert( Gamification.awardPoints.calledWith( 'linkerId', -Settings.shared.gamification.points.card.link ) );
            assert( Audit.next.calledWith( {
                domain:            "card",
                message:           "unlinked",
                userId:            "userId",
                sourceCardId:      'sourceCardId',
                destinationCardId: 'destinationCardId',
                linkType:          'linkType'
            } ) );
        } );

        it( "link creator should not lose reputation points if unlinking a link with two of their own cards", () => {
            const link        = {
                _id:         'linkId',
                source:      'sourceCardId',
                destination: 'destinationCardId',
                linkedBy:    'linkerId',
                linkedAt:    new Date()
            };
            const source      = { id: 'sourceCardId', author: 'linkerId' };
            const destination = { id: 'destinationCardId', author: 'linkerId' };
            CardLinksCollection.findOne.returns( link );
            CardCollection.findOne.onFirstCall().returns( source );
            CardCollection.findOne.onSecondCall().returns( destination );
            CardSecurity.canUnlink.returns( true );

            expect( () => CardMethods.unlink._execute( context, { linkId: 'linkId' } ) ).not.to.throw();

            assert.isFalse( Gamification.awardPoints.called );
        } );

        it( "link creator should lose reputation points NOT the person unlinking", () => {
            const link        = {
                _id:         'linkId',
                source:      'sourceCardId',
                destination: 'destinationCardId',
                linkedBy:    'linkerId',
                linkedAt:    new Date()
            };
            const source      = { id: 'sourceCardId', author: 'userId' };
            const destination = { id: 'destinationCardId', author: 'authorId' };
            CardLinksCollection.findOne.returns( link );
            CardCollection.findOne.onFirstCall().returns( source );
            CardCollection.findOne.onSecondCall().returns( destination );
            CardSecurity.canUnlink.returns( true );

            expect( () => CardMethods.unlink._execute( context, { linkId: 'linkId' } ) ).not.to.throw();

            assert( Gamification.awardPoints.calledWith( 'linkerId', -Settings.shared.gamification.points.card.link ) );
        } );

        it( "link creator should lose reputation points if unlinking a link created with someone else's card as a source", () => {
            const link        = {
                _id:         'linkId',
                source:      'sourceCardId',
                destination: 'destinationCardId',
                linkedBy:    'userId',
                linkedAt:    new Date()
            };
            const source      = { id: 'sourceCardId', author: 'userId' };
            const destination = { id: 'destinationCardId', author: 'authorId' };
            CardLinksCollection.findOne.returns( link );
            CardCollection.findOne.onFirstCall().returns( source );
            CardCollection.findOne.onSecondCall().returns( destination );
            CardSecurity.canUnlink.returns( true );

            expect( () => CardMethods.unlink._execute( context, { linkId: 'linkId' } ) ).not.to.throw();

            assert( Gamification.awardPoints.calledWith( 'userId', -Settings.shared.gamification.points.card.link ) );
        } );

        it( "link creator should lose reputation points if unlinking a link created with someone else's card as a destination", () => {
            const link        = {
                _id:         'linkId',
                source:      'sourceCardId',
                destination: 'destinationCardId',
                linkedBy:    'userId',
                linkedAt:    new Date()
            };
            const source      = { id: 'sourceCardId', author: 'authorId' };
            const destination = { id: 'destinationCardId', author: 'userId' };
            CardLinksCollection.findOne.returns( link );
            CardCollection.findOne.onFirstCall().returns( source );
            CardCollection.findOne.onSecondCall().returns( destination );
            CardSecurity.canUnlink.returns( true );

            expect( () => CardMethods.unlink._execute( context, { linkId: 'linkId' } ) ).not.to.throw();

            assert( Gamification.awardPoints.calledWith( 'userId', -Settings.shared.gamification.points.card.link ) );
        } );

        it( "should not heat source card if unlinked by card author", () => {
            const link        = {
                _id:         'linkId',
                source:      'sourceCardId',
                destination: 'destinationCardId',
                linkedBy:    'linkerId',
                linkedAt:    new Date()
            };
            const source      = { id: 'sourceCardId', author: 'userId' };
            const destination = { id: 'destinationCardId', author: 'authorId' };
            CardLinksCollection.findOne.returns( link );
            CardCollection.findOne.onFirstCall().returns( source );
            CardCollection.findOne.onSecondCall().returns( destination );
            CardSecurity.canUnlink.returns( true );

            expect( () => CardMethods.unlink._execute( context, { linkId: 'linkId' } ) ).not.to.throw();

            expect( CardAnalytics.cool.callCount ).to.equal( 1 );
            assert( CardAnalytics.cool.calledWith( {
                id:     'destinationCardId',
                author: 'authorId'
            }, link.linkedAt, Settings.shared.analytics.card.heat.linkDestination ) );
            assert( Gamification.awardPoints.calledWith( 'linkerId', -Settings.shared.gamification.points.card.link ) );
        } );

        it( "should not heat destination card if unlinked by card author", () => {
            const link        = {
                _id:         'linkId',
                source:      'sourceCardId',
                destination: 'destinationCardId',
                linkedBy:    'linkerId',
                linkedAt:    new Date()
            };
            const source      = { id: 'sourceCardId', author: 'authorId' };
            const destination = { id: 'destinationCardId', author: 'userId' };
            CardLinksCollection.findOne.returns( link );
            CardCollection.findOne.onFirstCall().returns( source );
            CardCollection.findOne.onSecondCall().returns( destination );
            CardSecurity.canUnlink.returns( true );

            expect( () => CardMethods.unlink._execute( context, { linkId: 'linkId' } ) ).not.to.throw();

            expect( CardAnalytics.cool.callCount ).to.equal( 1 );
            assert( CardAnalytics.cool.calledWith( {
                id:     'sourceCardId',
                author: 'authorId'
            }, link.linkedAt, Settings.shared.analytics.card.heat.linkSource ) );
            assert( Gamification.awardPoints.calledWith( 'linkerId', -Settings.shared.gamification.points.card.link ) );
        } );

        it( "should throw if not allowed", () => {
            const link        = {
                _id:         'linkId',
                source:      { id: 'sourceCardId' },
                destination: { id: 'destinationCardId' },
                type:        'linkType',
                linkedBy:    'linkerId',
                linkedAt:    new Date()
            };
            const source      = { id: 'sourceCardId', author: 'authorId' };
            const destination = { id: 'destinationCardId', author: 'authorId' };
            CardLinksCollection.findOne.returns( link );
            CardCollection.findOne.onFirstCall().returns( source );
            CardCollection.findOne.onSecondCall().returns( destination );
            CardSecurity.canUnlink.returns( false );

            expect( ()=> CardMethods.unlink._execute( context, { linkId: 'linkId' } ) ).to.throw( /unauthorized/ );

            assert( CardLinksCollection.findOne.calledWith(
                { _id: 'linkId' },
                { fields: { _id: 1, source: 1, destination: 1, type: 1, linkedBy: 1, linkedAt: 1 } }
            ) );
            expect( CardCollection.findOne.callCount ).to.eql( 2 );
            assert( CardCollection.findOne.getCall( 0 ).calledWith( { id: 'sourceCardId', current: true }, {
                transform: null
            } ) );
            assert( CardCollection.findOne.getCall( 1 ).calledWith( { id: 'destinationCardId', current: true }, {
                transform: null
            } ) );
            assert( CardSecurity.canUnlink.calledWith( "userId", source, destination ) );
            assert.isFalse( CardCollection.snapshotAndUpdate.called );
            assert.isFalse( CardLinksCollection.update.called );
            assert.isFalse( CardAnalytics.cool.called );
            assert.isFalse( Gamification.awardPoints.called );
            assert.isFalse( Audit.next.called );
        } );

        it( "should throw if link is not found", () => {
            CardLinksCollection.findOne.returns( null );

            expect( ()=> CardMethods.unlink._execute( context, { linkId: 'linkId' } ) ).to.throw( /link not found/ );

            assert( CardLinksCollection.findOne.calledWith(
                { _id: 'linkId' },
                { fields: { _id: 1, source: 1, destination: 1, type: 1, linkedBy: 1, linkedAt: 1 } }
            ) );
            assert.isFalse( CardCollection.findOne.called );
            assert.isFalse( CardSecurity.canUnlink.called );
            assert.isFalse( CardCollection.snapshotAndUpdate.called );
            assert.isFalse( CardLinksCollection.update.called );
            assert.isFalse( CardAnalytics.cool.called );
            assert.isFalse( Gamification.awardPoints.called );
            assert.isFalse( Audit.next.called );
        } );

        it( "should throw if source card is not found", () => {
            const link = {
                _id:         'linkId',
                source:      { id: 'sourceCardId' },
                destination: { id: 'destinationCardId' },
                type:        'linkType',
                linkedBy:    'linkerId',
                linkedAt:    new Date()
            };
            CardLinksCollection.findOne.returns( link );
            CardCollection.findOne.onFirstCall().returns( null );

            expect( ()=> CardMethods.unlink._execute( context, { linkId: 'linkId' } ) ).to.throw( /card not found/ );

            assert( CardLinksCollection.findOne.calledWith(
                { _id: 'linkId' },
                { fields: { _id: 1, source: 1, destination: 1, type: 1, linkedBy: 1, linkedAt: 1 } }
            ) );
            expect( CardCollection.findOne.calledOnce );
            assert( CardCollection.findOne.calledWith( { id: "sourceCardId", current: true }, {
                transform: null
            } ) );
            assert.isFalse( CardSecurity.canUnlink.called );
            assert.isFalse( CardCollection.snapshotAndUpdate.called );
            assert.isFalse( CardLinksCollection.update.called );
            assert.isFalse( CardAnalytics.cool.called );
            assert.isFalse( Gamification.awardPoints.called );
            assert.isFalse( Audit.next.called );
        } );

        it( "should throw if destination card is not found", () => {
            const link   = {
                _id:         'linkId',
                source:      { id: 'sourceCardId' },
                destination: { id: 'destinationCardId' },
                type:        'linkType',
                linkedBy:    'linkerId',
                linkedAt:    new Date()
            };
            const source = { id: 'sourceCardId', author: 'authorId' };
            CardLinksCollection.findOne.returns( link );
            CardCollection.findOne.onFirstCall().returns( source );
            CardCollection.findOne.onSecondCall().returns( null );

            expect( ()=> CardMethods.unlink._execute( context, { linkId: 'linkId' } ) ).to.throw( /card not found/ );

            assert( CardLinksCollection.findOne.calledWith(
                { _id: 'linkId' },
                { fields: { _id: 1, source: 1, destination: 1, type: 1, linkedBy: 1, linkedAt: 1 } }
            ) );
            expect( CardCollection.findOne.callCount ).to.eql( 2 );
            assert( CardCollection.findOne.getCall( 0 ).calledWith( { id: 'sourceCardId', current: true }, {
                transform: null
            } ) );
            assert( CardCollection.findOne.getCall( 1 ).calledWith( { id: 'destinationCardId', current: true }, {
                transform: null
            } ) );
            assert.isFalse( CardSecurity.canUnlink.called );
            assert.isFalse( CardCollection.snapshotAndUpdate.called );
            assert.isFalse( CardLinksCollection.update.called );
            assert.isFalse( CardAnalytics.cool.called );
            assert.isFalse( Gamification.awardPoints.called );
            assert.isFalse( Audit.next.called );
        } );

        function expectCheckFailed() {
            assert.isFalse( CardLinksCollection.findOne.called );
            assert.isFalse( CardCollection.findOne.called );
            assert.isFalse( CardSecurity.canUnlink.called );
            assert.isFalse( CardCollection.snapshotAndUpdate.called );
            assert.isFalse( CardLinksCollection.update.called );
            assert.isFalse( CardAnalytics.cool.called );
            assert.isFalse( Gamification.awardPoints.called );
            assert.isFalse( Audit.next.called );
        }

        it( 'should fail on missing linkId', () => {
            expect( () => CardMethods.linkCards._execute( context, {} ) ).to.throw( /validation-error/ );
            expectCheckFailed();
        } );

        it( 'should fail on null linkId', () => {
            expect( () => CardMethods.linkCards._execute( context, { linkId: null } ) ).to.throw( /validation-error/ );
            expectCheckFailed();
        } );

        it( 'should fail on invalid linkId', () => {
            expect( () => CardMethods.linkCards._execute( context, { linkId: 123 } ) ).to.throw( /validation-error/ );
            expectCheckFailed();
        } );

    } );

    describe( 'moveToPhase', () => {

        let context;
        let card;

        beforeEach( () => {
            context = { userId: 'userId' };
            sandbox.stub( CardSecurity, 'canMove' );
            sandbox.stub( CardCollection, 'findOne' );
            sandbox.stub( ProjectCollection, 'findOne' );
            sandbox.stub( CardCollection, 'update' );
            sandbox.stub( CardCollection, 'snapshot' );
            sandbox.spy( CardAnalytics, 'heatModifier' );
        } );

        afterEach( () => {
            card = null;
        } );

        it( 'should move card', () => {
            // SETUP
            card = {
                id:              'cardId',
                author:          'authorId',
                project:         'projectId',
                phase:           'phaseId',
                version:         1,
                lastTemperature: 1,
                heatedAt:        new Date()
            };
            CardSecurity.canMove.returns( true );
            CardCollection.findOne.returns( card );
            ProjectCollection.findOne.returns( { _id: 'projectId' } );
            CardCollection.snapshot.returns( 123 );

            // ACTION
            expect( () => CardMethods.moveToPhase._execute( context, {
                cardId:  'cardId',
                phaseId: 'newPhaseId'
            } ) ).to.not.throw();

            // TESTS
            assert( CardSecurity.canMove.calledWith( 'userId', 'cardId', 'newPhaseId' ) );
            assert( CardCollection.findOne.calledWith(
                { id: 'cardId', current: true },
                {
                    fields:    { id: 1, author: 1, project: 1, phase: 1, version: 1, lastTemperature: 1, heatedAt: 1 },
                    transform: null
                }
            ) );
            assert( ProjectCollection.findOne.calledWith( {
                _id:          'projectId',
                'phases._id': 'newPhaseId'
            }, { fields: { _id: 1 }, transform: null } ) );

            expect( CardCollection.update.callCount ).to.eql( 2 );

            assert( CardCollection.update.getCall( 0 ).calledWith(
                {
                    id:          'cardId',
                    current:     true,
                    phaseVisits: { $elemMatch: { exitedVersion: { $exists: false } } }
                }, {
                    $set: {
                        'phaseVisits.$.exitedVersion': card.version,
                        'phaseVisits.$.exitedAt':      sinon.match.date,
                        'phaseVisits.$.exitedTo':      'newPhaseId'
                    }
                }
            ), "Card not pre-updated correctly" );

            expect( CardCollection.snapshot.calledWith( "userId", "cardId" ), "Card not snapshot correctly" );

            assert( CardAnalytics.heatModifier.calledWith(
                card,
                Settings.shared.analytics.card.heat.move,
                sinon.match.object
            ) );

            assert( CardCollection.update.getCall( 1 ).calledWith(
                { id: 'cardId', current: true },
                {
                    $inc:  {
                        totalTemperature: Settings.shared.analytics.card.heat.move
                    },
                    $set:  {
                        phase:           'newPhaseId',
                        version:         123,
                        lastTemperature: 1 + Settings.shared.analytics.card.heat.move,
                        heatedAt:        sinon.match.date
                    },
                    $push: {
                        phaseVisits: {
                            phase:          "newPhaseId",
                            type:           "moved",
                            enteredVersion: 123,
                            enteredAt:      sinon.match.date,
                            enteredFrom:    "phaseId"
                        }
                    }
                }
            ), "Card not updated correctly" );

            assert( Audit.next.calledWith( {
                domain:      'card',
                message:     'moved',
                userId:      'userId',
                cardId:      'cardId',
                fromPhaseId: 'phaseId',
                toPhaseId:   'newPhaseId'
            } ) );
        } );

        it( 'should heat card if not moved by author', () => {
            // SETUP
            card = {
                id:              'cardId',
                author:          'authorId',
                project:         'projectId',
                phase:           'phaseId',
                version:         1,
                lastTemperature: 1,
                heatedAt:        new Date()
            };
            CardSecurity.canMove.returns( true );
            CardCollection.findOne.returns( card );
            ProjectCollection.findOne.returns( { _id: 'projectId' } );
            CardCollection.snapshot.returns( 123 );

            // ACTION
            expect( () => CardMethods.moveToPhase._execute( context, {
                cardId:  'cardId',
                phaseId: 'newPhaseId'
            } ) ).to.not.throw();

            // TESTS
            assert( CardAnalytics.heatModifier.calledWith(
                card,
                Settings.shared.analytics.card.heat.move,
                sinon.match.object
            ) );
        } );

        it( 'should not heat card if moved by author', () => {
            // SETUP
            card = {
                id:              'cardId',
                author:          'userId',
                project:         'projectId',
                phase:           'phaseId',
                version:         1,
                lastTemperature: 1,
                heatedAt:        new Date()
            };
            CardSecurity.canMove.returns( true );
            CardCollection.findOne.returns( card );
            ProjectCollection.findOne.returns( { _id: 'projectId' } );
            CardCollection.snapshot.returns( 123 );

            // ACTION
            expect( () => CardMethods.moveToPhase._execute( context, {
                cardId:  'cardId',
                phaseId: 'newPhaseId'
            } ) ).to.not.throw();

            // TESTS
            assert.isFalse( CardAnalytics.heatModifier.called );
        } );

        it( 'should throw if not allowed', () => {
            // SETUP
            CardSecurity.canMove.returns( false );

            // ACTION
            expect( () => CardMethods.moveToPhase._execute( context, {
                cardId:  'cardId',
                phaseId: 'newPhaseId'
            } ) ).to.throw( /unauthorized/ );

            // TESTS
            assert( CardSecurity.canMove.calledWith( 'userId', 'cardId', 'newPhaseId' ) );
            assert.isFalse( CardCollection.findOne.called );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( CardCollection.update.called );
            assert.isFalse( CardCollection.snapshot.called );
            assert.isFalse( CardAnalytics.heatModifier.called );
            assert.isFalse( Audit.next.called );
        } );

        it( 'should throw if card is not found', () => {
            // SETUP
            CardSecurity.canMove.returns( true );
            CardCollection.findOne.returns( null );

            // ACTION
            expect( () => CardMethods.moveToPhase._execute( context, {
                cardId:  'cardId',
                phaseId: 'newPhaseId'
            } ) ).to.throw( /card not found/ );

            // TESTS
            assert( CardSecurity.canMove.calledWith( 'userId', 'cardId', 'newPhaseId' ) );
            assert( CardCollection.findOne.calledWith( { id: 'cardId', current: true }, {
                fields:    { id: 1, author: 1, project: 1, phase: 1, version: 1, lastTemperature: 1, heatedAt: 1 },
                transform: null
            } ) );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( CardCollection.update.called );
            assert.isFalse( CardCollection.snapshot.called );
            assert.isFalse( CardAnalytics.heatModifier.called );
            assert.isFalse( Audit.next.called );
        } );

        it( 'should throw if project/phase is not found', () => {
            // SETUP
            CardSecurity.canMove.returns( true );
            CardCollection.findOne.returns( {
                id:      'cardId',
                author:  'authorId',
                project: 'projectId',
                phase:   'phaseId'
            } );
            ProjectCollection.findOne.returns( null );

            // ACTION
            expect( () => CardMethods.moveToPhase._execute( context, {
                cardId:  'cardId',
                phaseId: 'newPhaseId'
            } ) ).to.throw( /phase not found/ );

            // TEST
            assert( CardSecurity.canMove.calledWith( 'userId', 'cardId', 'newPhaseId' ) );
            assert( CardCollection.findOne.calledWith( { id: 'cardId', current: true }, {
                fields:    { id: 1, author: 1, project: 1, phase: 1, version: 1, lastTemperature: 1, heatedAt: 1 },
                transform: null
            } ) );
            assert( ProjectCollection.findOne.calledWith( {
                _id:          'projectId',
                'phases._id': 'newPhaseId'
            }, { fields: { _id: 1 }, transform: null } ) );
            assert.isFalse( CardCollection.update.called );
            assert.isFalse( CardCollection.snapshot.called );
            assert.isFalse( CardAnalytics.heatModifier.called );
            assert.isFalse( Audit.next.called );
        } );

        function expectCheckFailed() {
            assert.isFalse( CardCollection.findOne.called );
            assert.isFalse( ProjectCollection.findOne.called );
            assert.isFalse( CardSecurity.canMove.called );
            assert.isFalse( CardCollection.update.called );
            assert.isFalse( CardCollection.snapshot.called );
            assert.isFalse( CardAnalytics.heatModifier.called );
            assert.isFalse( Audit.next.called );
        }

        it( 'should fail on extra parameter', () => {
            expect( () => CardMethods.moveToPhase._execute( context, {
                cardId:  'cardId',
                phaseId: 'phaseId',
                extra:   'parameter'
            } ) ).to.throw( /validation-error/ );
            expectCheckFailed();
        } );

        it( 'should fail on invalid cardId (number)', () => {
            expect( () => CardMethods.moveToPhase._execute( context, {
                cardId:  123,
                phaseId: 'phaseId'
            } ) ).to.throw( /validation-error/ );
            expectCheckFailed();
        } );

        it( 'should fail on invalid cardId (null)', () => {
            expect( () => CardMethods.moveToPhase._execute( context, {
                cardId:  null,
                phaseId: 'phaseId'
            } ) ).to.throw( /validation-error/ );
            expectCheckFailed();
        } );

        it( 'should fail on invalid phaseId (number)', () => {
            expect( () => CardMethods.moveToPhase._execute( context, {
                cardId:  'cardId',
                phaseId: 123
            } ) ).to.throw( /validation-error/ );
            expectCheckFailed();
        } );

        it( 'should fail on invalid phaseId (null)', () => {
            expect( () => CardMethods.moveToPhase._execute( context, {
                cardId:  'cardId',
                phaseId: null
            } ) ).to.throw( /validation-error/ );
            expectCheckFailed();
        } );

        it( 'should fail on invalid phaseId (undefined)', () => {
            expect( () => CardMethods.moveToPhase._execute( context, { cardId: 'cardId' } ) ).to.throw( /validation-error/ );
            expectCheckFailed();
        } );

        it( 'should fail on missing parameters', () => {
            expect( () => CardMethods.moveToPhase._execute( context ) ).to.throw( /must be an object/ );
            expectCheckFailed();
        } );

    } );

    describe( 'pin', () => {

        let context;
        let card;

        beforeEach( () => {
            context = { userId: 'userId' };
            sandbox.stub( CardSecurity, 'canPin' );
            sandbox.stub( CardCollection, 'findOne' );
            sandbox.stub( CardCollection, 'snapshot' );
            sandbox.stub( CardCollection, 'update' );
        } );

        afterEach( () => {
            card = null;
        } );

        if ( Meteor.isServer ) {

            it( 'should pin a card', () => {
                // SETUP
                card = { id: 'cardId', project: 'projectId', pinned: false };
                CardSecurity.canPin.returns( true );
                CardCollection.findOne.returns( _.clone( card ) );
                CardCollection.snapshot.returns( 123 );

                // ACTION
                expect( () => CardMethods.pin._execute( context, { cardId: 'cardId' } ) ).to.not.throw();

                // TESTS
                assert( CardSecurity.canPin.calledWith( 'userId', _.clone( card ) ) );
                assert( CardCollection.findOne.calledWith( { id: 'cardId', current: true }, {
                    fields:    { id: 1, project: 1, pinned: 1 },
                    transform: null
                } ) );
                assert( CardCollection.update.calledWith( { id: 'cardId', current: true }, {
                    $set: { pinned: true, version: 123 }
                } ) );
                assert( Audit.next.calledWith( {
                    domain:  'card',
                    message: 'pinned',
                    userId:  'userId',
                    cardId:  'cardId'
                } ) );
            } );

            it( 'should not pin a pinned card', () => {
                // SETUP
                card = { id: 'cardId', project: 'projectId', pinned: true };
                CardSecurity.canPin.returns( true );
                CardCollection.findOne.returns( _.clone( card ) );

                // ACTION
                expect( () => CardMethods.pin._execute( context, { cardId: 'cardId' } ) ).to.not.throw();

                // TESTS
                assert.isFalse( CardSecurity.canPin.called );
                assert( CardCollection.findOne.calledWith( { id: 'cardId', current: true }, {
                    fields:    { id: 1, project: 1, pinned: 1 },
                    transform: null
                } ) );
                assert.isFalse( CardCollection.update.called );
                assert.isFalse( Audit.next.called );
            } );

            it( 'should throw if not allowed', () => {
                // SETUP
                card = { id: 'cardId', project: 'projectId', pinned: false };
                CardSecurity.canPin.returns( false );
                CardCollection.findOne.returns( _.clone( card ) );

                // ACTION
                expect( () => CardMethods.pin._execute( context, { cardId: 'cardId' } ) ).to.throw( /unauthorized/ );

                // TESTS
                assert( CardSecurity.canPin.calledWith( 'userId', _.clone( card ) ) );
                assert( CardCollection.findOne.calledWith( { id: 'cardId', current: true }, {
                    fields:    { id: 1, project: 1, pinned: 1 },
                    transform: null
                } ) );
                assert.isFalse( CardCollection.update.called );
                assert.isFalse( Audit.next.called );
            } );

        }

        function expectCheckFailed() {
            assert.isFalse( CardSecurity.canPin.called );
            assert.isFalse( CardCollection.findOne.called );
            assert.isFalse( CardCollection.update.called );
            assert.isFalse( Audit.next.called );
        }

        it( 'should fail on extra parameter', () => {
            expect( () => CardMethods.pin._execute( context, {
                cardId: 123,
                extra:  'parameter'
            } ) ).to.throw( /validation-error/ );
            expectCheckFailed();
        } );

        it( 'should fail on invalid cardId (number)', () => {
            expect( () => CardMethods.pin._execute( context, { cardId: 123 } ) ).to.throw( /validation-error/ );
            expectCheckFailed();
        } );

        it( 'should fail on invalid cardId (null)', () => {
            expect( () => CardMethods.pin._execute( context, { cardId: null } ) ).to.throw( /validation-error/ );
            expectCheckFailed();
        } );

        it( 'should fail on missing parameters', () => {
            expect( () => CardMethods.pin._execute( context ) ).to.throw( /must be an object/ );
            expectCheckFailed();
        } );

    } );

    describe( 'unpin', () => {

        let context;
        let card;

        beforeEach( () => {
            context = { userId: 'userId' };
            sandbox.stub( CardSecurity, 'canPin' );
            sandbox.stub( CardCollection, 'findOne' );
            sandbox.stub( CardCollection, 'snapshot' );
            sandbox.stub( CardCollection, 'update' );
        } );

        afterEach( () => {
            card = null;
        } );

        if ( Meteor.isServer ) {

            it( 'should unpin a card', () => {
                // SETUP
                card = { id: 'cardId', project: 'projectId', pinned: true };
                CardSecurity.canPin.returns( true );
                CardCollection.findOne.returns( _.clone( card ) );
                CardCollection.snapshot.returns( 123 );

                // ACTION
                expect( () => CardMethods.unpin._execute( context, { cardId: 'cardId' } ) ).to.not.throw();

                // TESTS
                assert( CardSecurity.canPin.calledWith( 'userId', _.clone( card ) ) );
                assert( CardCollection.findOne.calledWith( { id: 'cardId', current: true }, {
                    fields:    { id: 1, project: 1, pinned: 1 },
                    transform: null
                } ) );
                assert( CardCollection.update.calledWith( { id: 'cardId', current: true }, {
                    $set: { pinned: false, version: 123 }
                } ) );
                assert( Audit.next.calledWith( {
                    domain:  'card',
                    message: 'unpinned',
                    userId:  'userId',
                    cardId:  'cardId'
                } ) );
            } );

            it( 'should not unpin an unpinned card', () => {
                // SETUP
                card = { id: 'cardId', project: 'projectId', pinned: false };
                CardSecurity.canPin.returns( true );
                CardCollection.findOne.returns( _.clone( card ) );

                // ACTION
                expect( () => CardMethods.unpin._execute( context, { cardId: 'cardId' } ) ).to.not.throw();

                // TESTS
                assert.isFalse( CardSecurity.canPin.called );
                assert( CardCollection.findOne.calledWith( { id: 'cardId', current: true }, {
                    fields:    { id: 1, project: 1, pinned: 1 },
                    transform: null
                } ) );
                assert.isFalse( CardCollection.update.called );
                assert.isFalse( Audit.next.called );
            } );

            it( 'should throw if not allowed', () => {
                // SETUP
                card = { id: 'cardId', project: 'projectId', pinned: true };
                CardSecurity.canPin.returns( false );
                CardCollection.findOne.returns( _.clone( card ) );

                // ACTION
                expect( () => CardMethods.unpin._execute( context, { cardId: 'cardId' } ) ).to.throw( /unauthorized/ );

                // TESTS
                assert( CardSecurity.canPin.calledWith( 'userId', _.clone( card ) ) );
                assert( CardCollection.findOne.calledWith( { id: 'cardId', current: true }, {
                    fields:    { id: 1, project: 1, pinned: 1 },
                    transform: null
                } ) );
                assert.isFalse( CardCollection.update.called );
                assert.isFalse( Audit.next.called );
            } );

        }

        function expectCheckFailed() {
            assert.isFalse( CardSecurity.canPin.called );
            assert.isFalse( CardCollection.findOne.called );
            assert.isFalse( CardCollection.update.called );
            assert.isFalse( Audit.next.called );
        }

        it( 'should fail on extra parameter', () => {
            expect( () => CardMethods.unpin._execute( context, {
                cardId: 123,
                extra:  'parameter'
            } ) ).to.throw( /validation-error/ );
            expectCheckFailed();
        } );

        it( 'should fail on invalid cardId (number)', () => {
            expect( () => CardMethods.unpin._execute( context, { cardId: 123 } ) ).to.throw( /validation-error/ );
            expectCheckFailed();
        } );

        it( 'should fail on invalid cardId (null)', () => {
            expect( () => CardMethods.unpin._execute( context, { cardId: null } ) ).to.throw( /validation-error/ );
            expectCheckFailed();
        } );

        it( 'should fail on missing parameters', () => {
            expect( () => CardMethods.unpin._execute( context ) ).to.throw( /must be an object/ );
            expectCheckFailed();
        } );

    } );

    /*describe( 'copyToPhase', () => {

     let context;
     let cardFindOptions;
     let cardCopy;

     beforeEach( () => {
     context = { userId: 'userId' };

     cardFindOptions = {
     fields:    {
     author:       0, // We change the author when copying
     commentCount: 0, // We don't keep the comments when copying
     viewCount:    0, // No stealing view counts from a popular card
     likes:        0, // No stealing likes from a popular card
     likeCount:    0, // No Stealing like counts from a popular card
     version:      0, // Version restarts to 0 when copying
     updatedAt:    0, // Can't be set on insert
     updatedBy:    0
     },
     transform: null
     };

     sandbox.stub( CardSecurity, 'canCopy' );
     sandbox.stub( CardCollection, 'findOne' );
     sandbox.stub( ProjectCollection, 'findOne' );
     sandbox.stub( CardCollection, 'insert' ).returns( 'newCardId' );
     sandbox.stub( CardImagesCollection, 'update' );
     } );

     afterEach( () => {
     cardCopy = null;
     } );

     it( 'should copy card', () => {
     // SETUP
     cardCopy = {
     author:       'userId',
     project:      'projectId',
     phase:        'newPhaseId',
     phaseVisits:  [{ phase: 'newPhaseId', type: 'copied', enteredVersion: 0, enteredAt: sinon.match.date }],
     viewCount:    0,
     commentCount: 0,
     likeCount:    0,
     likes:        [],
     version:      0
     };

     CardSecurity.canCopy.returns( true );
     CardCollection.findOne.returns( {
     _id:          'cardId',
     author:       'authorId',
     project:      'projectId',
     phase:        'phaseId',
     phaseVisits:  [{ phase: 'somePhaseId', type: 'created', enteredVersion: 15, enteredAt: new Date() }],
     commentCount: 5,
     viewCount:    10,
     likeCount:    20,
     likes:        ['like1', 'like2'],
     version:      30
     } );
     ProjectCollection.findOne.returns( { _id: 'projectId' } );

     // ACTION
     expect( () => CardMethods.copyToPhase._execute( context, {
     cardId:  'cardId',
     phaseId: 'newPhaseId'
     } ) ).to.not.throw();

     // TEST
     assert( CardSecurity.canCopy.calledWith( 'userId', 'cardId', 'newPhaseId' ) );
     assert( CardCollection.findOne.calledWith( { _id: 'cardId' }, cardFindOptions ) );
     assert( ProjectCollection.findOne.calledWith( {
     _id:          'projectId',
     'phases._id': 'newPhaseId'
     }, { fields: { _id: 1 }, transform: null } ) );
     assert( CardCollection.insert.calledWith( cardCopy ) );
     assert( CardImagesCollection.update.calledWith( { owners: 'cardId' }, { $addToSet: { owners: 'newCardId' } } ) );
     assert( Audit.next.calledWith( {
     domain:    'card',
     message:   'copied',
     userId:    'userId',
     cardId:    'cardId',
     newCardId: 'newCardId'
     } ) );
     } );

     it( 'should throw if not allowed', () => {
     // SETUP
     CardSecurity.canCopy.returns( false );

     // ACTION
     expect( () => CardMethods.copyToPhase._execute( context, {
     cardId:  'cardId',
     phaseId: 'newPhaseId'
     } ) ).to.throw( /unauthorized/ );

     // TESTS
     assert( CardSecurity.canCopy.calledWith( 'userId', 'cardId', 'newPhaseId' ) );
     assert.isFalse( CardCollection.findOne.calledWith() );
     assert.isFalse( ProjectCollection.findOne.called );
     assert.isFalse( CardImagesCollection.update.called );
     assert.isFalse( Audit.next.called );
     } );

     it( 'should throw if card is not found', () => {
     // SETUP
     CardSecurity.canCopy.returns( true );
     CardCollection.findOne.returns( null );

     // ACTION
     expect( () => CardMethods.copyToPhase._execute( context, {
     cardId:  'cardId',
     phaseId: 'newPhaseId'
     } ) ).to.throw( /card not found/ );

     // TESTS
     assert( CardSecurity.canCopy.calledWith( 'userId', 'cardId', 'newPhaseId' ) );
     assert( CardCollection.findOne.calledWith( { _id: 'cardId' }, cardFindOptions ) );
     assert.isFalse( ProjectCollection.findOne.called );
     assert.isFalse( CardImagesCollection.update.called );
     assert.isFalse( Audit.next.called );
     } );

     it( 'should throw if project/phase is not found', () => {
     // SETUP
     CardSecurity.canCopy.returns( true );
     CardCollection.findOne.returns( {
     _id:     'cardId',
     author:  'authorId',
     project: 'projectId',
     phase:   'phaseId'
     } );
     ProjectCollection.findOne.returns( null );

     // ACTION
     expect( () => CardMethods.copyToPhase._execute( context, {
     cardId:  'cardId',
     phaseId: 'newPhaseId'
     } ) ).to.throw( /phase not found/ );

     // TEST
     assert( CardSecurity.canCopy.calledWith( 'userId', 'cardId', 'newPhaseId' ) );
     assert( CardCollection.findOne.calledWith( { _id: 'cardId' }, cardFindOptions ) );
     assert( ProjectCollection.findOne.calledWith( {
     _id:          'projectId',
     'phases._id': 'newPhaseId'
     }, { fields: { _id: 1 }, transform: null } ) );
     assert.isFalse( CardImagesCollection.update.called );
     assert.isFalse( Audit.next.called );
     } );

     function expectCheckFailed() {
     assert.isFalse( CardCollection.findOne.called );
     assert.isFalse( ProjectCollection.findOne.called );
     assert.isFalse( CardSecurity.canCopy.called );
     assert.isFalse( CardImagesCollection.update.called );
     assert.isFalse( Audit.next.called );
     }

     it( 'should fail on extra parameter', () => {
     expect( () => CardMethods.copyToPhase._execute( context, {
     cardId:  123,
     phaseId: 'phaseId',
     extra:   'parameter'
     } ) ).to.throw( /validation-error/ );
     expectCheckFailed();
     } );

     it( 'should fail on invalid cardId (number)', () => {
     expect( () => CardMethods.copyToPhase._execute( context, {
     cardId:  123,
     phaseId: 'phaseId'
     } ) ).to.throw( /validation-error/ );
     expectCheckFailed();
     } );

     it( 'should fail on invalid cardId (null)', () => {
     expect( () => CardMethods.copyToPhase._execute( context, {
     cardId:  null,
     phaseId: 'phaseId'
     } ) ).to.throw( /validation-error/ );
     expectCheckFailed();
     } );

     it( 'should fail on invalid phaseId (number)', () => {
     expect( () => CardMethods.copyToPhase._execute( context, {
     cardId:  'cardId',
     phaseId: 123
     } ) ).to.throw( /validation-error/ );
     expectCheckFailed();
     } );

     it( 'should fail on invalid phaseId (null)', () => {
     expect( () => CardMethods.copyToPhase._execute( context, {
     cardId:  'cardId',
     phaseId: null
     } ) ).to.throw( /validation-error/ );
     expectCheckFailed();
     } );

     it( 'should fail on invalid phaseId (undefined)', () => {
     expect( () => CardMethods.copyToPhase._execute( context, { cardId: 'cardId' } ) ).to.throw( /validation-error/ );
     expectCheckFailed();
     } );

     it( 'should fail on missing parameters', () => {
     expect( () => CardMethods.copyToPhase._execute( context ) ).to.throw( /must be an object/ );
     expectCheckFailed();
     } );

     } );*/

} );
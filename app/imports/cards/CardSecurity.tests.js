"use strict";

import { chai, assert, expect } from 'meteor/practicalmeteor:chai';
import { sinon, spies, stubs } from 'meteor/practicalmeteor:sinon';

import ProjectHelpers from "/imports/projects/ProjectHelpers";
import ProjectSecurity from "/imports/projects/ProjectSecurity";
import CardCollection from "/imports/cards/CardCollection";
import CardSecurity from "/imports/cards/CardSecurity";
import UserHelpers from "/imports/accounts/UserHelpers";
import { RoleList } from "/imports/accounts/Roles";

describe( 'cards/CardSecurity', function () {

    let sandbox;

    beforeEach( () => {
        sandbox = sinon.sandbox.create();
    } );

    afterEach( () => {
        sandbox.restore();
    } );

    describe( 'canRead', function () {

        beforeEach( function () {
            sandbox.stub( UserHelpers, 'isMember' );
            sandbox.stub( CardCollection, 'findOne' );
            sandbox.stub( ProjectSecurity, 'canRead' );
        } );

        it( 'should allow a global member to read the card', () => {
            UserHelpers.isMember.returns( true );
            expect( CardSecurity.canRead( 'userId', 'cardId' ) ).to.be.true;
            assert( UserHelpers.isMember.calledWith( 'userId' ) );
            assert.isFalse( CardCollection.findOne.called );
            assert.isFalse( ProjectSecurity.canRead.called );
        } );

        it( 'should allow using a card id', () => {
            UserHelpers.isMember.returns( false );
            CardCollection.findOne.returns( { project: 'projectId' } );
            ProjectSecurity.canRead.returns( true );
            expect( CardSecurity.canRead( 'userId', 'cardId' ) ).to.be.true;
            assert( UserHelpers.isMember.calledWith( 'userId' ) );
            assert( CardCollection.findOne.calledWith( { id: 'cardId', current: true }, { fields: { project: 1 }, transform: null } ) );
            assert( ProjectSecurity.canRead.calledWith( 'userId', 'projectId' ) );
        } );

        it( 'should work without a user', () => {
            UserHelpers.isMember.returns( false );
            CardCollection.findOne.returns( { project: 'projectId' } );
            ProjectSecurity.canRead.returns( true );
            expect( CardSecurity.canRead( null, 'cardId' ) ).to.be.true;
            assert( UserHelpers.isMember.calledWith( null ) );
            assert( CardCollection.findOne.calledWith( { id: 'cardId', current: true }, { fields: { project: 1 }, transform: null } ) );
            assert( ProjectSecurity.canRead.calledWith( null, 'projectId' ) );
        } );

        it( 'should allow using a card with id', () => {
            UserHelpers.isMember.returns( false );
            CardCollection.findOne.returns( { project: 'projectId' } );
            ProjectSecurity.canRead.returns( true );
            expect( CardSecurity.canRead( 'userId', { id: 'cardId' } ) ).to.be.true;
            assert( UserHelpers.isMember.calledWith( 'userId' ) );
            assert( CardCollection.findOne.calledWith( { id: 'cardId', current: true }, { fields: { project: 1 }, transform: null } ) );
            assert( ProjectSecurity.canRead.calledWith( 'userId', 'projectId' ) );
        } );

        it( 'should allow using a card with project', () => {
            UserHelpers.isMember.returns( false );
            CardCollection.findOne.returns( { project: 'projectId' } );
            ProjectSecurity.canRead.returns( true );
            expect( CardSecurity.canRead( 'userId', { project: 'projectId' } ) ).to.be.true;
            assert( UserHelpers.isMember.calledWith( 'userId' ) );
            assert.isFalse( CardCollection.findOne.called );
            assert( ProjectSecurity.canRead.calledWith( 'userId', 'projectId' ) );
        } );

        it( 'should allow using a card with id and project', () => {
            UserHelpers.isMember.returns( false );
            CardCollection.findOne.returns( { project: 'projectId' } );
            ProjectSecurity.canRead.returns( true );
            expect( CardSecurity.canRead( 'userId', { id: 'cardId', project: 'projectId' } ) ).to.be.true;
            assert( UserHelpers.isMember.calledWith( 'userId' ) );
            assert.isFalse( CardCollection.findOne.called );
            assert( ProjectSecurity.canRead.calledWith( 'userId', 'projectId' ) );
        } );

        it( 'should not allow if the card is not found', () => {
            UserHelpers.isMember.returns( false );
            CardCollection.findOne.returns( null );
            expect( CardSecurity.canRead( 'userId', 'cardId' ) ).to.be.false;
            assert( UserHelpers.isMember.calledWith( 'userId' ) );
            assert( CardCollection.findOne.calledWith( { id: 'cardId', current: true }, { fields: { project: 1 }, transform: null } ) );
            assert.isFalse( ProjectSecurity.canRead.called );
        } );

        it( 'should not allow if the card is not linked to a project (weird)', () => {
            UserHelpers.isMember.returns( false );
            CardCollection.findOne.returns( { id: 'cardId' } );
            expect( CardSecurity.canRead( 'userId', 'cardId' ) ).to.be.false;
            assert( UserHelpers.isMember.calledWith( 'userId' ) );
            assert( CardCollection.findOne.calledWith( { id: 'cardId', current: true }, { fields: { project: 1 }, transform: null } ) );
            assert.isFalse( ProjectSecurity.canRead.called );
        } );

        function assertCheckFailed() {
            assert.isFalse( UserHelpers.isMember.called );
            assert.isFalse( CardCollection.findOne.called );
            assert.isFalse( ProjectSecurity.canRead.called );
        }

        it( 'should throw if card is invalid (number)', () => {
            expect( () => CardSecurity.canRead( 'userId', 123 ) ).to.throw( /Match error/ );
            assertCheckFailed();
        } );

        it( 'should throw if card is invalid (null)', () => {
            expect( () => CardSecurity.canRead( 'userId', null ) ).to.throw( /Match error/ );
            assertCheckFailed();
        } );

        it( 'should throw if card is invalid (no project or id)', () => {
            expect( () => CardSecurity.canRead( 'userId', { name: 'card' } ) ).to.throw( /Match error/ );
            assertCheckFailed();
        } );

        it( 'should throw if user is invalid (number)', () => {
            expect( () => CardSecurity.canRead( 123, 'cardId' ) ).to.throw( /Match error/ );
            assertCheckFailed();
        } );

    } );

    describe( 'canEdit', function () {

        beforeEach( function () {
            sandbox.stub( UserHelpers, 'isModerator' );
            sandbox.stub( CardCollection, 'findOne' );
            sandbox.stub( ProjectHelpers, 'phaseIsActive' );
            sandbox.stub( ProjectSecurity, 'canEdit' );
        } );

        it( 'should allow a global moderator to edit the card', ()=> {
            UserHelpers.isModerator.returns( true );
            expect( CardSecurity.canEdit( 'userId', 'cardId' ) ).to.be.true;
            assert( UserHelpers.isModerator.calledWith( 'userId' ) );
            assert.isFalse( CardCollection.findOne.called );
            assert.isFalse( ProjectSecurity.canEdit.called );
            assert.isFalse( ProjectHelpers.phaseIsActive.called );
        } );

        it( 'should not allow an anonymous user to edit the card', ()=> {
            UserHelpers.isModerator.returns( false );
            expect( CardSecurity.canEdit( null, 'cardId' ) ).to.be.false;
            assert.isFalse( UserHelpers.isModerator.called );
            assert.isFalse( CardCollection.findOne.called );
            assert.isFalse( ProjectSecurity.canEdit.called );
            assert.isFalse( ProjectHelpers.phaseIsActive.called );
        } );

        it( 'should not allow if card is not found', ()=> {
            UserHelpers.isModerator.returns( false );
            CardCollection.findOne.returns( null );
            expect( CardSecurity.canEdit( 'userId', 'cardId' ) ).to.be.false;
            assert( UserHelpers.isModerator.calledWith( 'userId' ) );
            assert( CardCollection.findOne.calledWith( { id: 'cardId', current: true }, { fields: { author: 1, project: 1, phase: 1 }, transform: null } ) );
            assert.isFalse( ProjectSecurity.canEdit.called );
            assert.isFalse( ProjectHelpers.phaseIsActive.called );
        } );

        it( 'should allow a moderator (or someone with project access) to edit the card', ()=> {
            UserHelpers.isModerator.returns( false );
            CardCollection.findOne.returns( { project: 'projectId' } );
            ProjectSecurity.canEdit.returns( true );
            expect( CardSecurity.canEdit( 'userId', 'cardId' ) ).to.be.true;
            assert( UserHelpers.isModerator.calledWith( 'userId' ) );
            assert( CardCollection.findOne.calledWith( { id: 'cardId', current: true }, { fields: { author: 1, project: 1, phase: 1 }, transform: null } ) );
            assert( ProjectSecurity.canEdit.calledWith( 'userId', 'projectId' ) );
            assert.isFalse( ProjectHelpers.phaseIsActive.called );
        } );

        it( 'should deny if not the card author', ()=> {
            UserHelpers.isModerator.returns( false );
            CardCollection.findOne.returns( { project: 'projectId', author: 'anotherUser' } );
            ProjectSecurity.canEdit.returns( false );
            expect( CardSecurity.canEdit( 'userId', 'cardId' ) ).to.be.false;
            assert( UserHelpers.isModerator.calledWith( 'userId' ) );
            assert( CardCollection.findOne.calledWith( { id: 'cardId', current: true }, { fields: { author: 1, project: 1, phase: 1 }, transform: null } ) );
            assert( ProjectSecurity.canEdit.calledWith( 'userId', 'projectId' ) );
            assert.isFalse( ProjectHelpers.phaseIsActive.called );
        } );

        it( 'should allow if the card author and phase is active', ()=> {
            UserHelpers.isModerator.returns( false );
            CardCollection.findOne.returns( { author: 'userId', project: 'projectId', phase: 'phaseId' } );
            ProjectSecurity.canEdit.returns( false );
            ProjectHelpers.phaseIsActive.returns( true );
            expect( CardSecurity.canEdit( 'userId', 'cardId' ) ).to.be.true;
            assert( UserHelpers.isModerator.calledWith( 'userId' ) );
            assert( CardCollection.findOne.calledWith( { id: 'cardId', current: true }, { fields: { author: 1, project: 1, phase: 1 }, transform: null } ) );
            assert( ProjectSecurity.canEdit.calledWith( 'userId', 'projectId' ) );
            assert( ProjectHelpers.phaseIsActive.calledWith( 'projectId', 'phaseId' ) );
        } );

        it( 'should deny if the card author and phase is not active', ()=> {
            UserHelpers.isModerator.returns( false );
            CardCollection.findOne.returns( { author: 'userId', project: 'projectId', phase: 'phaseId' } );
            ProjectSecurity.canEdit.returns( false );
            ProjectHelpers.phaseIsActive.returns( false );
            expect( CardSecurity.canEdit( 'userId', 'cardId' ) ).to.be.false;
            assert( UserHelpers.isModerator.calledWith( 'userId' ) );
            assert( CardCollection.findOne.calledWith( { id: 'cardId', current: true }, { fields: { author: 1, project: 1, phase: 1 }, transform: null } ) );
            assert( ProjectSecurity.canEdit.calledWith( 'userId', 'projectId' ) );
            assert( ProjectHelpers.phaseIsActive.calledWith( 'projectId', 'phaseId' ) );
        } );

    } );

    describe( "canUpdate", () => {

        it( "should wrap CardSecurity.canEdit (true)", () => {
            sandbox.stub( CardSecurity, 'canEdit' ).returns( true );
            expect( CardSecurity.canUpdate( 'userId', 'cardId' ) ).to.be.true;
            assert( CardSecurity.canEdit.calledWith( 'userId', 'cardId' ) );
        } );

        it( "should wrap CardSecurity.canEdit (false)", () => {
            sandbox.stub( CardSecurity, 'canEdit' ).returns( false );
            expect( CardSecurity.canUpdate( 'userId', 'cardId' ) ).to.be.false;
            assert( CardSecurity.canEdit.calledWith( 'userId', 'cardId' ) );
        } );

    } );

    describe( "canModerate", () => {

        it( "should wrap CardSecurity.canEdit (true)", () => {
            sandbox.stub( CardSecurity, 'canEdit' ).returns( true );
            expect( CardSecurity.canModerate( 'userId', 'cardId' ) ).to.be.true;
            assert( CardSecurity.canEdit.calledWith( 'userId', 'cardId' ) );
        } );

        it( "should wrap CardSecurity.canEdit (false)", () => {
            sandbox.stub( CardSecurity, 'canEdit' ).returns( false );
            expect( CardSecurity.canModerate( 'userId', 'cardId' ) ).to.be.false;
            assert( CardSecurity.canEdit.calledWith( 'userId', 'cardId' ) );
        } );

    } );

    describe( "canCopy", () => {

        it( "should wrap CardSecurity.canEdit (true)", () => {
            sandbox.stub( CardSecurity, 'canEdit' ).returns( true );
            expect( CardSecurity.canCopy( 'userId', 'cardId', 'phaseId' ) ).to.be.true;
            assert( CardSecurity.canEdit.calledWith( 'userId', 'cardId' ) );
        } );

        it( "should wrap CardSecurity.canEdit (false)", () => {
            sandbox.stub( CardSecurity, 'canEdit' ).returns( false );
            expect( CardSecurity.canCopy( 'userId', 'cardId', 'phaseId' ) ).to.be.false;
            assert( CardSecurity.canEdit.calledWith( 'userId', 'cardId' ) );
        } );

    } );

    describe( "canMove", () => {

        beforeEach( function () {
            sandbox.stub( CardCollection, 'findOne' );
            sandbox.stub( ProjectSecurity, 'canContribute' );
        } );

        it( "should allow if can contribute to project", () => {
            CardCollection.findOne.returns( { project: "projectId" } );
            ProjectSecurity.canContribute.returns( true );

            expect( CardSecurity.canMove( 'userId', 'cardId', 'phaseId' ) ).to.be.true;

            assert( CardCollection.findOne.calledWith( { id: "cardId", current: true }, { fields: { project: 1 }, transform: null } ) );
            assert( ProjectSecurity.canContribute.calledWith( 'userId', 'projectId' ) );
        } );

        it( "should bypass card fetch if already have project", () => {
            CardCollection.findOne.returns( { project: "projectId" } );
            ProjectSecurity.canContribute.returns( true );

            expect( CardSecurity.canMove( 'userId', { project: 'projectId' }, 'phaseId' ) ).to.be.true;

            assert.isFalse( CardCollection.findOne.called );
            assert( ProjectSecurity.canContribute.calledWith( 'userId', 'projectId' ) );
        } );

        it( "should return false if card is not found", () => {
            CardCollection.findOne.returns( null );

            expect( CardSecurity.canMove( 'userId', 'cardId', 'phaseId' ) ).to.be.false;

            assert( CardCollection.findOne.calledWith( { id: "cardId", current: true }, { fields: { project: 1 }, transform: null } ) );
            assert.isFalse( ProjectSecurity.canContribute.called );
        } );

        it( "should deny if can't contribute to project", () => {
            CardCollection.findOne.returns( { project: "projectId" } );
            ProjectSecurity.canContribute.returns( false );

            expect( CardSecurity.canMove( 'userId', 'cardId', 'phaseId' ) ).to.be.false;

            assert( CardCollection.findOne.calledWith( { id: "cardId", current: true }, { fields: { project: 1 }, transform: null } ) );
            assert( ProjectSecurity.canContribute.calledWith( 'userId', 'projectId' ) );
        } );

    } );

    describe( "canLink", () => {

        beforeEach( ()=> {
            sandbox.stub( UserHelpers, 'isModerator' );
            sandbox.stub( CardCollection, 'findOne' );
            sandbox.stub( ProjectSecurity, 'canContribute' );
        } );

        it( "should pass with card ids", () => {
            UserHelpers.isModerator.returns( false );
            CardCollection.findOne.onFirstCall().returns( { id: 'sourceCardId', project: 'projectId' } );
            CardCollection.findOne.onSecondCall().returns( { id: 'destinationCardId', project: 'projectId' } );
            ProjectSecurity.canContribute.returns( true );

            expect( CardSecurity.canLink( 'userId', 'sourceCardId', 'destinationCardId' ) ).to.be.true;

            assert( UserHelpers.isModerator.calledWith( 'userId' ) );
            expect( CardCollection.findOne.callCount ).to.equal( 2 );
            assert( CardCollection.findOne.getCall( 0 ).calledWith( { id: 'sourceCardId', current: true }, { fields: { id: 1, project: 1 }, transform: null } ) );
            assert( CardCollection.findOne.getCall( 1 ).calledWith( { id: 'destinationCardId', current: true }, { fields: { id: 1, project: 1 }, transform: null } ) );
            assert( ProjectSecurity.canContribute.calledWith( 'userId', 'projectId' ) );
        } );

        it( "should return false if destination card not found", () => {
            UserHelpers.isModerator.returns( false );
            CardCollection.findOne.returns( null );

            expect( CardSecurity.canLink( 'userId', 'sourceCardId', 'destinationCardId' ) ).to.be.false;

            assert( UserHelpers.isModerator.calledWith( 'userId' ) );
            expect( CardCollection.findOne.callCount ).to.equal( 1 );
            assert( CardCollection.findOne.getCall( 0 ).calledWith( { id: 'sourceCardId', current: true }, { fields: { id: 1, project: 1 }, transform: null } ) );
            assert.isFalse( ProjectSecurity.canContribute.called );
        } );

        it( "should return false if destination card not found", () => {
            UserHelpers.isModerator.returns( false );
            CardCollection.findOne.onFirstCall().returns( { id: 'sourceCardId', project: 'projectId' } );
            CardCollection.findOne.onSecondCall().returns( null );

            expect( CardSecurity.canLink( 'userId', 'sourceCardId', 'destinationCardId' ) ).to.be.false;

            assert( UserHelpers.isModerator.calledWith( 'userId' ) );
            expect( CardCollection.findOne.callCount ).to.equal( 2 );
            assert( CardCollection.findOne.getCall( 0 ).calledWith( { id: 'sourceCardId', current: true }, { fields: { id: 1, project: 1 }, transform: null } ) );
            assert( CardCollection.findOne.getCall( 1 ).calledWith( { id: 'destinationCardId', current: true }, { fields: { id: 1, project: 1 }, transform: null } ) );
            assert.isFalse( ProjectSecurity.canContribute.called );
        } );

        it( "should pass with cards", () => {
            UserHelpers.isModerator.returns( false );
            ProjectSecurity.canContribute.returns( true );

            expect( CardSecurity.canLink(
                'userId',
                { id: 'sourceCardId', project: 'projectId' },
                { id: 'destinationCardId', project: 'projectId' }
            ) ).to.be.true;

            assert( UserHelpers.isModerator.calledWith( 'userId' ) );
            assert.isFalse( CardCollection.findOne.called );
            assert( ProjectSecurity.canContribute.calledWith( 'userId', 'projectId' ) );
        } );

        it( "should fetch cards if passed cards don't have a project attribute", () => {
            UserHelpers.isModerator.returns( false );
            CardCollection.findOne.onFirstCall().returns( { id: 'sourceCardId', project: 'projectId' } );
            CardCollection.findOne.onSecondCall().returns( { id: 'destinationCardId', project: 'projectId' } );
            ProjectSecurity.canContribute.returns( true );

            expect( CardSecurity.canLink(
                'userId',
                { id: 'sourceCardId' },
                { id: 'destinationCardId' }
            ) ).to.be.true;

            assert( UserHelpers.isModerator.calledWith( 'userId' ) );
            expect( CardCollection.findOne.callCount ).to.equal( 2 );
            assert( CardCollection.findOne.getCall( 0 ).calledWith( { id: 'sourceCardId', current: true }, { fields: { id: 1, project: 1 }, transform: null } ) );
            assert( CardCollection.findOne.getCall( 1 ).calledWith( { id: 'destinationCardId', current: true }, { fields: { id: 1, project: 1 }, transform: null } ) );
            assert( ProjectSecurity.canContribute.calledWith( 'userId', 'projectId' ) );
        } );

        it( "should throw without a source card", () => {
            UserHelpers.isModerator.returns( false );
            ProjectSecurity.canContribute.returns( true );

            expect( () => CardSecurity.canLink(
                'userId',
                null,
                { id: 'destinationCardId', project: 'projectId' }
            ) ).to.throw( /Match error/ );

            assert.isFalse( UserHelpers.isModerator.called );
            assert.isFalse( CardCollection.findOne.called );
            assert.isFalse( ProjectSecurity.canContribute.called );
        } );

        it( "should throw without a destination card", () => {
            UserHelpers.isModerator.returns( false );
            ProjectSecurity.canContribute.returns( true );

            expect( () => CardSecurity.canLink(
                'userId',
                { id: 'sourceCardId', project: 'projectId' },
                null
            ) ).to.throw( /Match error/ );

            assert.isFalse( UserHelpers.isModerator.called );
            assert.isFalse( CardCollection.findOne.called );
            assert.isFalse( ProjectSecurity.canContribute.called );
        } );

        it( "should return false if same card", () => {
            UserHelpers.isModerator.returns( false );
            ProjectSecurity.canContribute.returns( true );

            expect( CardSecurity.canLink(
                'userId',
                { id: 'cardId', project: 'projectId' },
                { id: 'cardId', project: 'projectId' }
            ) ).to.be.false;

            assert( UserHelpers.isModerator.calledWith( 'userId' ) );
            assert.isFalse( CardCollection.findOne.called );
            assert.isFalse( ProjectSecurity.canContribute.called );
        } );

        it( "should return false if not same project", () => {
            UserHelpers.isModerator.returns( false );
            ProjectSecurity.canContribute.returns( true );

            expect( CardSecurity.canLink(
                'userId',
                { id: 'sourceCardId', project: 'project1Id' },
                { id: 'destinationCardId', project: 'project2Id' }
            ) ).to.be.false;

            assert( UserHelpers.isModerator.calledWith( 'userId' ) );
            assert.isFalse( CardCollection.findOne.called );
            assert.isFalse( ProjectSecurity.canContribute.called );
        } );

        it( "should skip if no userId", () => {
            expect( CardSecurity.canLink( null, 'sourceCardId', 'destinationCardId' ) ).to.be.false;

            assert.isFalse( UserHelpers.isModerator.called );
            assert.isFalse( CardCollection.findOne.called );
            assert.isFalse( ProjectSecurity.canContribute.called );
        } );

        it( "should skip & pass if global moderator", () => {
            UserHelpers.isModerator.returns( true );

            expect( CardSecurity.canLink( 'userId', 'sourceCardId', 'destinationCardId' ) ).to.be.true;

            assert( UserHelpers.isModerator.calledWith( 'userId' ) );
            assert.isFalse( CardCollection.findOne.called );
            assert.isFalse( ProjectSecurity.canContribute.called );
        } );

    } );

    describe( "canUnlink", () => {

        beforeEach( ()=> {
            sandbox.stub( CardSecurity, 'canLink' );
        } );

        it( "should forward canLink success", () => {
            CardSecurity.canLink.returns( true );
            expect( CardSecurity.canUnlink( "userId", "sourceCardId", "destinationCardId" ) ).to.be.true;
            assert( CardSecurity.canLink.calledWith( "userId", "sourceCardId", "destinationCardId" ) );
        } );

        it( "should forward canLink failure", () => {
            CardSecurity.canLink.returns( false );
            expect( CardSecurity.canUnlink( "userId", "sourceCardId", "destinationCardId" ) ).to.be.false;
            assert( CardSecurity.canLink.calledWith( "userId", "sourceCardId", "destinationCardId" ) );
        } );

    } );

    describe( "canLike", () => {

        beforeEach( ()=> {
            sandbox.stub( UserHelpers, 'isModerator' );
            sandbox.stub( CardCollection, 'findOne' );
            sandbox.stub( ProjectSecurity, 'canRead' );
        } );

        it( 'should return false if no user', () => {
            // SETUP

            // EXECUTION
            expect( CardSecurity.canLike( null, 'cardId' ) ).to.be.false;

            // ASSERTIONS
            assert.isFalse( UserHelpers.isModerator.called );
            assert.isFalse( CardCollection.findOne.called );
            assert.isFalse( ProjectSecurity.canRead.called );
        } );

        it( 'should return true if user is a global moderator', () => {
            // SETUP
            UserHelpers.isModerator.returns( true );

            // EXECUTION
            expect( CardSecurity.canLike( 'userId', 'cardId' ) ).to.be.true;

            // ASSERTIONS
            assert( UserHelpers.isModerator.calledWith( 'userId' ) );
            assert.isFalse( CardCollection.findOne.called );
            assert.isFalse( ProjectSecurity.canRead.called );
        } );

        it( 'should look for the card if passed an id', () => {
            // SETUP
            UserHelpers.isModerator.returns( false );
            CardCollection.findOne.returns( { project: 'projectId' } );
            ProjectSecurity.canRead.returns( true );

            // EXECUTION
            expect( CardSecurity.canLike( 'userId', 'cardId' ) ).to.be.true;

            // ASSERTIONS
            assert( UserHelpers.isModerator.calledWith( 'userId' ) );
            assert( CardCollection.findOne.calledWith(
                { id: 'cardId', current: true },
                { fields: { project: 1 }, transform: null }
            ) );
            assert( ProjectSecurity.canRead.calledWith( 'userId', 'projectId' ) );
        } );

        it( 'should return false if the card is not found', () => {
            // SETUP
            UserHelpers.isModerator.returns( false );
            CardCollection.findOne.returns( null );

            // EXECUTION
            expect( CardSecurity.canLike( 'userId', 'cardId' ) ).to.be.false;

            // ASSERTIONS
            assert( UserHelpers.isModerator.calledWith( 'userId' ) );
            assert( CardCollection.findOne.calledWith(
                { id: 'cardId', current: true },
                { fields: { project: 1 }, transform: null }
            ) );
            assert.isFalse( ProjectSecurity.canRead.called );
        } );

        it( 'should look for the card if it doesn\'t have a project key', () => {
            // SETUP
            UserHelpers.isModerator.returns( false );
            CardCollection.findOne.returns( { project: 'projectId' } );
            ProjectSecurity.canRead.returns( true );

            // EXECUTION
            expect( CardSecurity.canLike( 'userId', { id: 'cardId' } ) ).to.be.true;

            // ASSERTIONS
            assert( UserHelpers.isModerator.calledWith( 'userId' ) );
            assert( CardCollection.findOne.calledWith(
                { id: 'cardId', current: true },
                { fields: { project: 1 }, transform: null }
            ) );
            assert( ProjectSecurity.canRead.calledWith( 'userId', 'projectId' ) );
        } );

        it( 'should skip card lookup if it has a project key', () => {
            // SETUP
            UserHelpers.isModerator.returns( false );
            ProjectSecurity.canRead.returns( true );

            // EXECUTION
            expect( CardSecurity.canLike( 'userId', { project: 'projectId' } ) ).to.be.true;

            // ASSERTIONS
            assert( UserHelpers.isModerator.calledWith( 'userId' ) );
            assert.isFalse( CardCollection.findOne.called );
            assert( ProjectSecurity.canRead.calledWith( 'userId', 'projectId' ) );
        } );

        describe( 'check failed', () => {

            function expectCheckFailed() {
                assert.isFalse( UserHelpers.isModerator.called );
                assert.isFalse( CardCollection.findOne.called );
                assert.isFalse( ProjectSecurity.canRead.called );
            }

            it( 'should throw if using an invalid userId', () => {
                expect( ()=>CardSecurity.canLike( 123, 'cardId' ) ).to.throw( /Match error/ );
                expectCheckFailed();
            } );

            it( 'should throw if using an invalid card (number)', () => {
                expect( ()=>CardSecurity.canLike( 'userId', 123 ) ).to.throw( /Match error/ );
                expectCheckFailed();
            } );

            it( 'should throw if using an invalid card (no id or project)', () => {
                expect( ()=>CardSecurity.canLike( 'userId', { slug: 'whatever' } ) ).to.throw( /Match error/ );
                expectCheckFailed();
            } );

            it( 'should throw if using an invalid card (null)', () => {
                expect( ()=>CardSecurity.canLike( 'userId', null ) ).to.throw( /Match error/ );
                expectCheckFailed();
            } );

            it( 'should throw if using an invalid card (undefined)', () => {
                expect( ()=>CardSecurity.canLike( 'userId' ) ).to.throw( /Match error/ );
                expectCheckFailed();
            } );

        } );

    } );

    describe( "canPin", () => {

        beforeEach( ()=> {
            sandbox.stub( UserHelpers, 'isModerator' );
            sandbox.stub( CardCollection, 'findOne' );
            sandbox.stub( ProjectSecurity, 'canEdit' );
        } );

        it( 'should return false if no user', () => {
            // SETUP

            // EXECUTION
            expect( CardSecurity.canPin( null, 'cardId' ) ).to.be.false;

            // ASSERTIONS
            assert.isFalse( UserHelpers.isModerator.called );
            assert.isFalse( CardCollection.findOne.called );
            assert.isFalse( ProjectSecurity.canEdit.called );
        } );

        it( 'should return true if user is a global moderator', () => {
            // SETUP
            UserHelpers.isModerator.returns( true );

            // EXECUTION
            expect( CardSecurity.canPin( 'userId', 'cardId' ) ).to.be.true;

            // ASSERTIONS
            assert( UserHelpers.isModerator.calledWith( 'userId' ) );
            assert.isFalse( CardCollection.findOne.called );
            assert.isFalse( ProjectSecurity.canEdit.called );
        } );

        it( 'should look for the card if passed an id', () => {
            // SETUP
            UserHelpers.isModerator.returns( false );
            CardCollection.findOne.returns( { project: 'projectId' } );
            ProjectSecurity.canEdit.returns( true );

            // EXECUTION
            expect( CardSecurity.canPin( 'userId', 'cardId' ) ).to.be.true;

            // ASSERTIONS
            assert( UserHelpers.isModerator.calledWith( 'userId' ) );
            assert( CardCollection.findOne.calledWith(
                { id: 'cardId', current: true },
                { fields: { project: 1 }, transform: null }
            ) );
            assert( ProjectSecurity.canEdit.calledWith( 'userId', 'projectId' ) );
        } );

        it( 'should return false if the card is not found', () => {
            // SETUP
            UserHelpers.isModerator.returns( false );
            CardCollection.findOne.returns( null );

            // EXECUTION
            expect( CardSecurity.canPin( 'userId', 'cardId' ) ).to.be.false;

            // ASSERTIONS
            assert( UserHelpers.isModerator.calledWith( 'userId' ) );
            assert( CardCollection.findOne.calledWith(
                { id: 'cardId', current: true },
                { fields: { project: 1 }, transform: null }
            ) );
            assert.isFalse( ProjectSecurity.canEdit.called );
        } );

        it( 'should look for the card if it doesn\'t have a project key', () => {
            // SETUP
            UserHelpers.isModerator.returns( false );
            CardCollection.findOne.returns( { project: 'projectId' } );
            ProjectSecurity.canEdit.returns( true );

            // EXECUTION
            expect( CardSecurity.canPin( 'userId', { id: 'cardId' } ) ).to.be.true;

            // ASSERTIONS
            assert( UserHelpers.isModerator.calledWith( 'userId' ) );
            assert( CardCollection.findOne.calledWith(
                { id: 'cardId', current: true },
                { fields: { project: 1 }, transform: null }
            ) );
            assert( ProjectSecurity.canEdit.calledWith( 'userId', 'projectId' ) );
        } );

        it( 'should skip card lookup if it has a project key', () => {
            // SETUP
            UserHelpers.isModerator.returns( false );
            ProjectSecurity.canEdit.returns( true );

            // EXECUTION
            expect( CardSecurity.canPin( 'userId', { project: 'projectId' } ) ).to.be.true;

            // ASSERTIONS
            assert( UserHelpers.isModerator.calledWith( 'userId' ) );
            assert.isFalse( CardCollection.findOne.called );
            assert( ProjectSecurity.canEdit.calledWith( 'userId', 'projectId' ) );
        } );

        describe( 'check failed', () => {

            function expectCheckFailed() {
                assert.isFalse( UserHelpers.isModerator.called );
                assert.isFalse( CardCollection.findOne.called );
                assert.isFalse( ProjectSecurity.canEdit.called );
            }

            it( 'should throw if using an invalid userId', () => {
                expect( ()=>CardSecurity.canPin( 123, 'cardId' ) ).to.throw( /Match error/ );
                expectCheckFailed();
            } );

            it( 'should throw if using an invalid card (number)', () => {
                expect( ()=>CardSecurity.canPin( 'userId', 123 ) ).to.throw( /Match error/ );
                expectCheckFailed();
            } );

            it( 'should throw if using an invalid card (no id or project)', () => {
                expect( ()=>CardSecurity.canPin( 'userId', { slug: 'whatever' } ) ).to.throw( /Match error/ );
                expectCheckFailed();
            } );

            it( 'should throw if using an invalid card (null)', () => {
                expect( ()=>CardSecurity.canPin( 'userId', null ) ).to.throw( /Match error/ );
                expectCheckFailed();
            } );

            it( 'should throw if using an invalid card (undefined)', () => {
                expect( ()=>CardSecurity.canPin( 'userId' ) ).to.throw( /Match error/ );
                expectCheckFailed();
            } );

        } );

    } );
} );
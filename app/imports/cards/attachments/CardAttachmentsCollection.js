"use strict";

import AttachmentCollection from "../../attachments/AttachmentsCollection";
import CardFilesHelpers from"../files/CardFilesHelpers";
import CardCollection from '../CardCollection';

/**
 * Card Files Collection
 */
class CardAttachmentsCollection extends AttachmentCollection {
    constructor( name ) {
        super( name );

        /**
         * Indexes
         */

        if ( Meteor.isServer ) {
            this.files._ensureIndex( {
                'owners.id':          1,
                'owners.versionFrom': 1,
                'owners.versionTo':   1
            }, { name: 'owners_id_versionFrom_versionTo' } );
        }

        /**
         * Hooks
         *
         * Because of the way collectionFS works
         * this is the closest we have to methods to handle the uploaded file.
         *
         * This handles linking the uploads to the right card version.
         */

        if ( Meteor.isServer ) {
            this.files.before.insert( CardFilesHelpers.beforeInsert.bind( this ) );
            this.files.after.insert( ( userId, file ) => {
                // If the file is not pending, update the attachment count on the card
                // This only is not pending when uploading attachments to a new card
                if ( !file.pending ) {
                    this.updateAttachmentCount( file.owners[0].id, file.owners[0].versionFrom );
                }
            } );
        }
    }

    updateAttachmentCount( cardId, version ) {
        check( cardId, String );
        check( version, Number );
        const count = this.find( {
            'pending':            false,
            'owners.id':          cardId,
            'owners.versionFrom': { $lte: version },
            'owners.versionTo':   { $exists: false }
        } ).count();
        CardCollection.update( { id: cardId, current: true }, {
            $set: {
                attachmentCount: count
            }
        } );
    }
}
export default new CardAttachmentsCollection( "card_attachments" );
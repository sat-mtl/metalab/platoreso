"use strict";

import { chai, assert, expect } from 'meteor/practicalmeteor:chai';
import { sinon, spies, stubs } from 'meteor/practicalmeteor:sinon';
import CardCollection from "../CardCollection";
import Analytics from "/imports/analytics/Analytics";
import CardAnalytics from "./CardAnalytics";
import Settings from "../../settings/Settings";

describe( 'cards/CardAnalytics', function () {

    let sandbox;
    let card;
    let now;
    let clock;

    beforeEach( () => {
        now             = new Date();
        clock           = sinon.useFakeTimers( now.getTime() );
        sandbox         = sinon.sandbox.create();
        Settings.shared = {
            analytics: {
                card: {
                    cooldown: 0
                }
            }
        };
        card            = {
            id:             'cardId',
            lastTemperature: 1,
            heatedAt:        new Date()
        }
    } );

    afterEach( () => {
        sandbox.restore();
        clock.restore();
    } );

    describe( "heatData", () => {

        it( 'should return new heat data', () => {
            const data = CardAnalytics.heatData( null, 1 );
            expect( data ).to.exist;
            expect( data.lastTemperature ).to.equal( 1 );
            expect( data.heatedAt ).instanceof( Date );
        } );

        it( 'should return new heat data from card', () => {
            const data = CardAnalytics.heatData( card, 1 );
            expect( data ).to.exist;
            expect( data.lastTemperature ).to.equal( 2 );
            expect( data.heatedAt ).instanceof( Date );
        } );

        it( 'should return modified data', () => {
            const data = CardAnalytics.heatData( null, 1, { stay: 'here' } );
            expect( data ).to.exist;
            expect( data.stay ).to.eql( 'here' );
            expect( data.lastTemperature ).to.equal( 1 );
            expect( data.heatedAt ).instanceof( Date );
        } );

        it( 'should return modified data from card', () => {
            const data = CardAnalytics.heatData( card, 1, { stay: 'here' } );
            expect( data ).to.exist;
            expect( data.stay ).to.eql( 'here' );
            expect( data.lastTemperature ).to.equal( 2 );
            expect( data.heatedAt ).instanceof( Date );
        } );

        it( 'should modify data in place', () => {
            const modifier = { stay: 'here' };
            CardAnalytics.heatData( null, 1, modifier );
            expect( modifier.stay ).to.eql( 'here' );
            expect( modifier.lastTemperature ).to.equal( 1 );
            expect( modifier.heatedAt ).instanceof( Date );
        } );

        it( 'should modify data in place from card', () => {
            const modifier = { stay: 'here' };
            CardAnalytics.heatData( card, 1, modifier );
            expect( modifier.stay ).to.eql( 'here' );
            expect( modifier.lastTemperature ).to.equal( 2 );
            expect( modifier.heatedAt ).instanceof( Date );
        } );

        it( 'should not go below 0', () => {
            const modifier = CardAnalytics.heatData( card, -10 );
            expect( modifier.lastTemperature ).to.equal( 0 );
            expect( modifier.heatedAt ).instanceof( Date );
        } );

        it( 'should throw if missing card fields', () => {
            expect( () => CardAnalytics.heatData( { id: 'cardId' }, 1 ) ).to.throw( /Missing fields/ );
        } );
    } );

    describe( 'initialHeatData', () => {

        it('should forward and extend properly', () => {
            const data = CardAnalytics.initialHeatData(10);
            expect( data ).to.exist;
            expect( data.totalTemperature ).to.equal( 10 );
            expect( data.lastTemperature ).to.equal( 10 );
            expect( data.heatedAt ).instanceof( Date );
        });

    });

    describe( "heatModifier", () => {

        it( 'should return new modifier', () => {
            const modifier = CardAnalytics.heatModifier( null, 1 );
                expect( modifier ).to.exist;
                expect( modifier.$inc ).to.exist;
                expect( modifier.$inc.totalTemperature ).to.equal(1);
            expect( modifier.$set ).to.exist;
            expect( modifier.$set.lastTemperature ).to.equal( 1 );
            expect( modifier.$set.heatedAt ).instanceof( Date );
        } );

        it( 'should return new modifier from card', () => {
            const modifier = CardAnalytics.heatModifier( card, 1 );
            expect( modifier ).to.exist;
            expect( modifier.$inc ).to.exist;
            expect( modifier.$inc.totalTemperature ).to.equal(1);
            expect( modifier.$set ).to.exist;
            expect( modifier.$set.lastTemperature ).to.equal( 2 );
            expect( modifier.$set.heatedAt ).instanceof( Date );
        } );

        it( 'should return augmented modifier', () => {
            const modifier = CardAnalytics.heatModifier( null, 1, { $set: { something: 'else' } } );
            expect( modifier ).to.exist;
            expect( modifier.$inc ).to.exist;
            expect( modifier.$inc.totalTemperature ).to.equal(1);
            expect( modifier.$set ).to.exist;
            expect( modifier.$set.something ).to.eql( 'else' );
            expect( modifier.$set.lastTemperature ).to.equal( 1 );
            expect( modifier.$set.heatedAt ).instanceof( Date );
        } );

        it( 'should return augmented modifier from card', () => {
            const modifier = CardAnalytics.heatModifier( card, 1, { $set: { something: 'else' } } );
            expect( modifier ).to.exist;
            expect( modifier.$inc ).to.exist;
            expect( modifier.$inc.totalTemperature ).to.equal(1);
            expect( modifier.$set ).to.exist;
            expect( modifier.$set.something ).to.eql( 'else' );
            expect( modifier.$set.lastTemperature ).to.equal( 2 );
            expect( modifier.$set.heatedAt ).instanceof( Date );
        } );

        it( 'should augment modifier in place', () => {
            const modifier = { $set: { something: 'else' } };
            CardAnalytics.heatModifier( null, 1, modifier );
            expect( modifier ).to.exist;
            expect( modifier.$inc ).to.exist;
            expect( modifier.$inc.totalTemperature ).to.equal(1);
            expect( modifier.$set ).to.exist;
            expect( modifier.$set.something ).to.eql( 'else' );
            expect( modifier.$set.lastTemperature ).to.equal( 1 );
            expect( modifier.$set.heatedAt ).instanceof( Date );
        } );

        it( 'should augment modifier in place from card', () => {
            const modifier = { $set: { something: 'else' } };
            CardAnalytics.heatModifier( card, 1, modifier );
            expect( modifier ).to.exist;
            expect( modifier.$inc ).to.exist;
            expect( modifier.$inc.totalTemperature ).to.equal(1);
            expect( modifier.$set ).to.exist;
            expect( modifier.$set.something ).to.eql( 'else' );
            expect( modifier.$set.lastTemperature ).to.equal( 2 );
            expect( modifier.$set.heatedAt ).instanceof( Date );
        } );

    } );

    describe( "heat", () => {

        beforeEach( () => {
            sandbox.stub( CardCollection.direct, 'update' );
        } );

        it( "should heat a card", () => {
            CardAnalytics.heat( card, 1 );
            assert( CardCollection.direct.update.calledWith(
                {
                    id: card.id,
                    current: true
                },
                {
                    $set: {
                        lastTemperature: 2,
                        heatedAt:        sinon.match.date
                    },
                    $inc: {
                        totalTemperature: 1
                    }
                }
            ) );
        } );

        it( "should throw without a card", () => {
            expect( () => CardAnalytics.heat( null, 1 ) ).to.throw( /Card is required/ );
            expect( CardCollection.direct.update.called ).to.be.false;
        } );

        it( "should throw without a card id", () => {
            expect( () => CardAnalytics.heat( { something: 'else' }, 1 ) ).to.throw( /Card id is required/ );
            expect( CardCollection.direct.update.called ).to.be.false;
        } );

    } );

    describe( "cool", () => {

        beforeEach( () => {
            sandbox.stub( CardAnalytics, 'heat' );
        } );

        describe( 'unit', () => {

            beforeEach( () => {
                sandbox.stub( Analytics, 'getCurrentTemperature' );
            } );

            it( "should cool a card", () => {
                const actionTime                        = now - (1000 * 60 * 60);
                Settings.shared.analytics.card.cooldown = 0.5;
                Analytics.getCurrentTemperature.returns( 0.5 );

                CardAnalytics.cool( card, actionTime, 1 );

                assert( Analytics.getCurrentTemperature.calledWith( 1, actionTime, now, Settings.shared.analytics.card.cooldown ) );
                assert( CardAnalytics.heat.calledWith( card, -0.5 ) );
            } );

            it( "should not cool a card if heat to be removed is 0", () => {
                const actionTime                        = now - (1000 * 60 * 60);
                Settings.shared.analytics.card.cooldown = 0.5;
                Analytics.getCurrentTemperature.returns( 0 );

                CardAnalytics.cool( card, actionTime, 1 );

                assert( Analytics.getCurrentTemperature.calledWith( 1, actionTime, now, Settings.shared.analytics.card.cooldown ) );
                assert.isFalse( CardAnalytics.heat.called );
            } );

        } );

        describe( 'integration', () => {

            beforeEach( () => {
                sandbox.spy( Analytics, 'getCurrentTemperature' );
            } );

            it( "should cool a card", () => {
                const actionTime                        = now - (1000 * 60 * 60);
                Settings.shared.analytics.card.cooldown = 0.5;

                CardAnalytics.cool( card, actionTime, 1 );

                assert( Analytics.getCurrentTemperature.calledWith( 1, actionTime, now, Settings.shared.analytics.card.cooldown ) );
                assert( CardAnalytics.heat.calledWith( card, -0.5 ) );
            } );

            it( "should not cool a card if heat to be removed is 0", () => {
                const actionTime                        = now - (1000 * 60 * 60);
                Settings.shared.analytics.card.cooldown = 1;

                CardAnalytics.cool( card, actionTime, 1 );

                assert( Analytics.getCurrentTemperature.calledWith( 1, actionTime, now, Settings.shared.analytics.card.cooldown ) );
                assert.isFalse( CardAnalytics.heat.called );
            } );
        } );

        describe( 'validation', () => {
            it( "should throw without a card", () => {
                expect( () => CardAnalytics.cool( null, now, 1 ) ).to.throw( /Card is required/ );
                expect( CardAnalytics.heat.called ).to.be.false;
            } );

            it( "should throw without a card id", () => {
                expect( () => CardAnalytics.cool( { something: 'else' }, now, 1 ) ).to.throw( /Card id is required/ );
                expect( CardAnalytics.heat.called ).to.be.false;
            } );
        } );
    } );

} );
"use strict";

import { _ } from "meteor/underscore";
import Logger from "meteor/metalab:logger/Logger";
import Analytics from "/imports/analytics/Analytics";
import Settings from "/imports/settings/Settings";
import Card from "../model/Card";
import CardCollection from "../CardCollection";

const log = Logger( "card-analytics" );

/**
 * Card Analytics
 */
export default class CardAnalytics {

    /**
     * "Cools" a card.
     * Use this method to remove temperature that was set at a specific date.
     * Instead of heating directly with a negative value this method will compute how much
     * heat is left to remove from the original action.
     *
     * @param {Card} card
     * @param {Date} date
     * @param {Number} [heat]
     */
    static cool( card, date, heat = 0 ) {
        if ( !card ) {
            throw new Meteor.Error( "Card is required" );
        }

        if ( !card.id ) {
            throw new Meteor.Error( "Card id is required" );
        }

        log.debug( `Cooling card ${card.id} with ${heat} points from ${date}` );

        const cold = Analytics.getCurrentTemperature( heat, date, new Date(), Settings.shared.analytics.card.cooldown );
        if ( cold != 0 ) {
            log.verbose( `    - Removing ${cold} points` );
            CardAnalytics.heat( card, -cold );
        } else {
            log.verbose( `    - Card already cooled from the action, not removing heat` )
        }
    }

    /**
     * Apply "heat" to a card.
     * Use this method to directly update the card with the new temperature.
     * Useful for cases where the card was not already created/updated but we want to add heat.
     *
     * @param {Card} card
     * @param {Number} [heat]
     */
    static heat( card, heat = 0 ) {
        if ( !card ) {
            throw new Meteor.Error( "Card is required" );
        }

        if ( !card.id ) {
            throw new Meteor.Error( "Card id is required" );
        }

        log.debug( `Heating card ${card.id} with ${heat} points` );

        CardCollection.direct.update( { id: card.id, current: true }, CardAnalytics.heatModifier( card, heat ) );
    }

    /**
     * Apply "heat" to a card modifier.
     * Use this method to add updated temperature data to an existing or new modifier.
     * Useful to enhance existing updates.
     *
     * @param {Card} card
     * @param {Number} [heat]
     * @param {Object} [modifier]
     */
    static heatModifier( card, heat = 0, modifier = {} ) {
        if ( !modifier.$set ) {
            modifier.$set = {};
        }
        _.extend( modifier.$set, CardAnalytics.heatData( card, heat ) );

        if ( !modifier.$inc ) {
            modifier.$inc = {};
        }

        if ( modifier.$inc.totalTemperature != null ) {
            modifier.$inc.totalTemperature += heat;
        } else {
            modifier.$inc.totalTemperature = heat;
        }

        return modifier;
    }

    /**
     * Get card "heat" data
     * Use this method to only get temperature data.
     * Useful for inserts.
     *
     * @param {Card} card
     * @param {Number} [heat]
     * @param {Object} [data] Object to add data to, if null, the method will create one and return it.
     * @returns {{lastTemperature: Number, heatedAt: Date}}
     */
    static heatData( card, heat = 0, data = {} ) {
        if ( card && ( card.lastTemperature == null || card.heatedAt == null ) ) {
            throw new Meteor.Error( 'Missing fields lastTemperature and heatedAt to calculate card temperature' );
        }

        const now  = new Date();
        const temp = card ? Math.max( 0, Analytics.getCurrentTemperature( card.lastTemperature, card.heatedAt, now, Settings.shared.analytics.card.cooldown ) ): 0;

        return _.extend( data, {
            lastTemperature: Math.max( 0, temp + heat ), // Never write below zero, we only allow below zero when reading
            heatedAt:        now
        } );
    }

    static initialHeatData( heat = 0, data = {} ) {
        return _.extend( CardAnalytics.heatData( null, heat, data ), {
            totalTemperature: heat
        } );
    }

}
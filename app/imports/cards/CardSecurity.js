"use strict";

import ProjectHelpers from "../projects/ProjectHelpers";
import ProjectSecurity from "../projects/ProjectSecurity";
import CardCollection from "./CardCollection";

import UserHelpers from "/imports/accounts/UserHelpers";

/**
 * Card Security
 *
 * For every method in Card Methods, a rule should exist here to allow/deny the action.
 * Collection-based security will be deprecated in favor of per-method security, thus limiting the potential
 * risk of allowing unwanted modifications to the collection.
 */
export default class CardSecurity {

    /**
     * Can Read Card
     *
     * @param userId
     * @param card
     * @returns {Boolean}
     */
    static canRead( userId = null, card ) {
        check( card, Match.OneOf(
            Match.ObjectIncluding( { project: String } ),
            Match.ObjectIncluding( { id: String } ),
            String
        ) );
        check( userId, Match.OneOf( String, null ) );

        if ( UserHelpers.isMember( userId ) ) {
            // Global Member, all access
            return true;
        }

        // Logged-in or anonymous, find the card if we don't already have the project key
        const cardId = _.isString( card ) ? card : card.id;
        if ( cardId && !card.project ) {
            card = CardCollection.findOne( { id: cardId, current: true }, { fields: { project: 1 }, transform: null } );
        }

        // At this point if the card is either not found or does not contain the project key
        // we return negatively, as there's nothing to be read or it's un unfortunate orphan
        if ( !card || !card.project ) {
            return false;
        }

        // Cards all have the same access rights, dictated by the project
        return ProjectSecurity.canRead( userId, card.project );
    }

    static canEdit( userId = null, card ) {
        check( card, Match.OneOf(
            Match.ObjectIncluding( { author: String, project: String, phase: String } ),
            Match.ObjectIncluding( { id: String } ),
            Match.ObjectIncluding( { _id: String } ),
            String
        ) );
        check( userId, Match.OneOf( String, null ) );

        if ( !userId ) {
            // Not logged in, scram
            return false;
        }

        if ( UserHelpers.isModerator( userId ) ) {
            // Global Moderator, all access
            return true;
        }

        // Logged-in, find the card if we don't already have the project key
        let query = null;

        if ( _.isString( card ) ) {
            query = { id: card, current: true };
        } else if ( !card.author || !card.project || !card.phase ) {
            if ( card._id ) {
                card = CardCollection.findOne( { _id: card._id }, { fields: { id: 1 }, transform: null } );
                if ( !card ) {
                    return false;
                }
            }
            query = { id: card.id, current: true };
        }

        if ( query ) {
            card = CardCollection.findOne(
                query,
                { fields: { author: 1, project: 1, phase: 1 }, transform: null }
            );
        }

        if ( !card ) {
            // No card, too bad...
            return false;
        }

        // At this point, we really need the project, whatever happens
        // TODO: Fetch it preemptively to save on future database calls
        // Requirements:
        //     - ProjectSecurity.canEdit: groups
        //     - ProjectHelpers.phaseIsActive: phases._id, phases.startDate, phases.endDate
        // Don't forget: Minimongo doesn't support operators in projections yet
        /*const project = ProjectCollection.findOne(
         { _id: card.project, 'phases._id': card.phase },
         { fields: { groups: 1, 'phases._id': 1, 'phases.startDate': 1, 'phases.endDate': 1}
         });*/

        if ( ProjectSecurity.canEdit( userId, card.project ) ) {
            // If the user can edit the project, it means he/she is a moderator (at least)
            // which are not constrained by phase active status
            return true;

        } else {
            // Otherwise, only allow if the user is the original's card author and the phase is active
            return ( card.author == userId && ProjectHelpers.phaseIsActive( card.project, card.phase ) );
        }
    }

    /**
     * Can Create Card
     * Simple wrapper for now
     *
     * @param userId
     * @param card
     */
    static canCreate( userId = null, card ) {
        return CardSecurity.canEdit( userId, card );
    }

    /**
     * Can Update Card
     * Simple wrapper for now
     *
     * @param userId
     * @param card
     */
    static canUpdate( userId = null, card ) {
        return CardSecurity.canEdit( userId, card );
    }

    /**
     * Can Moderate Card
     * Simple wrapper for now
     *
     * @param userId
     * @param card
     */
    static canModerate( userId = null, card ) {
        return CardSecurity.canEdit( userId, card );
    }

    /**
     * Can Archive Card
     * Simple wrapper for now
     *
     * @param userId
     * @param card
     */
    static canArchive( userId = null, card ) {
        return CardSecurity.canEdit( userId, card );
    }

    /**
     * Can Remove Card
     * Simple wrapper for now
     *
     * @param userId
     * @param card
     */
    static canRemove( userId = null, card ) {
        return CardSecurity.canEdit( userId, card );
    }

    /**
     * Can Copy card
     * Simple wrapper for now
     *
     * @param userId
     * @param card
     * @param toPhase
     * @returns {Boolean}
     */
    static canCopy( userId = null, card, toPhase ) {
        return CardSecurity.canUpdate( userId, card );
    }

    /**
     * Can Move Card
     *
     * TODO: Should check phase dates
     *
     * @param userId
     * @param card
     * @param toPhase
     * @returns {Boolean}
     */
    static canMove( userId = null, card, toPhase ) {
        const cardId = _.isString( card ) ? card : card.id;
        if ( cardId && !card.project ) {
            card = CardCollection.findOne(
                { id: cardId, current: true },
                { fields: { project: 1 }, transform: null }
            );
        }
        if ( !card ) {
            // No card, too bad...
            return false;
        }
        // If the user can read the project, it is enough to let them link cards it
        return ProjectSecurity.canContribute( userId, card.project );
    }

    /**
     * Checks if a card can be linked
     *
     * @param {String} userId
     * @param {String|Object} sourceCard
     * @param {String|Object} destinationCard
     * @returns {Boolean}
     */
    static canLink( userId = null, sourceCard, destinationCard ) {
        check( userId, Match.OneOf( String, null ) );
        check( sourceCard, Match.OneOf(
            Match.ObjectIncluding( { id: String, project: String } ),
            Match.ObjectIncluding( { id: String } ),
            String
        ) );
        check( destinationCard, Match.OneOf(
            Match.ObjectIncluding( { id: String, project: String } ),
            Match.ObjectIncluding( { id: String } ),
            String
        ) );

        if ( !userId ) {
            // Not logged in, scram
            return false;
        }

        if ( UserHelpers.isModerator( userId ) ) {
            // Global Moderator, all access
            return true;
        }

        // Logged-in, find the cards if we don't already have the project key
        const sourceCardId = _.isString( sourceCard ) ? sourceCard : sourceCard.id;
        if ( sourceCardId && !sourceCard.project ) {
            sourceCard = CardCollection.findOne( { id: sourceCardId, current: true }, { fields: { id: 1, project: 1 }, transform: null } );
        }
        if ( !sourceCard ) {
            // No card, too bad...
            return false;
        }

        const destinationCardId = _.isString( destinationCard ) ? destinationCard : destinationCard.id;
        if ( destinationCardId && !destinationCard.project ) {
            destinationCard = CardCollection.findOne( { id: destinationCardId, current: true }, { fields: { id: 1, project: 1 }, transform: null } );
        }
        if ( !destinationCard ) {
            // No card, too bad...
            return false;
        }

        // Same card, forget it
        if ( sourceCard.id == destinationCard.id ) {
            return false;
        }

        // Not from the same project, forget it
        if ( sourceCard.project != destinationCard.project ) {
            return false;
        }

        // If the user can read the project, it is enough to let them link cards it
        return ProjectSecurity.canContribute( userId, sourceCard.project );
    }

    /**
     * Check if a card can be unlinked by the user
     * For now just a wrapper for canLink
     *
     * @param {String} userId
     * @param {String|Object} sourceCard
     * @param {String|Object} destinationCard
     * @returns {Boolean}
     */
    static canUnlink( userId = null, sourceCard, destinationCard ) {
        return CardSecurity.canLink( userId, sourceCard, destinationCard );
    }

    /**
     * Can Like Card
     *
     * @param userId
     * @param card
     * @returns {*}
     */
    static canLike( userId = null, card ) {
        check( userId, Match.OneOf( String, null ) );
        check( card, Match.OneOf(
            Match.ObjectIncluding( { project: String } ),
            Match.ObjectIncluding( { id: String } ),
            String
        ) );

        if ( !userId ) {
            // Not logged in, scram
            return false;
        }

        if ( UserHelpers.isModerator( userId ) ) {
            // Global Moderator, all access
            return true;
        }

        // Logged-in, find the card if we don't already have the project key
        const cardId = _.isString( card ) ? card : card.id;
        if ( cardId && !card.project ) {
            card = CardCollection.findOne( { id: cardId, current: true }, { fields: { project: 1 }, transform: null } );
        }

        if ( !card ) {
            // No card, too bad...
            return false;
        }

        // If the user can read the project, it is enough to let them like it
        return ProjectSecurity.canRead( userId, card.project );
    }

    /**
     * Can Pin/Unpin Card
     * Simple wrapper for now
     *
     * @param userId
     * @param card
     * @returns {Boolean}
     */
    static canPin( userId = null, card ) {
        check( userId, Match.OneOf( String, null ) );
        check( card, Match.OneOf(
            Match.ObjectIncluding( { project: String } ),
            Match.ObjectIncluding( { id: String } ),
            String
        ) );

        if ( !userId ) {
            // Not logged in, scram
            return false;
        }

        if ( UserHelpers.isModerator( userId ) ) {
            // Global Moderator, all access
            return true;
        }

        // Logged-in, find the card if we don't already have the project key
        const cardId = _.isString( card ) ? card : card.id;
        if ( cardId && !card.project ) {
            card = CardCollection.findOne( { id: cardId, current: true }, { fields: { project: 1 }, transform: null } );
        }

        if ( !card ) {
            // No card, too bad...
            return false;
        }

        return ProjectSecurity.canEdit( userId, card.project );
    }
}
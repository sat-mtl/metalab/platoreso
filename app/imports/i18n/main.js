"use strict";

// METHODS

import "./I18nMethods";

// MAIN

import { TAPi18next, TAPi18n } from "meteor/tap:i18n";

// We do want to fallback when the string wasn't translated
TAPi18next.options.fallbackOnEmpty = true;

// We use it gettext-style, so no key separator is needed
// The default was a dot and was failing for sentences
TAPi18next.options.keyseparator = null;
// Same thing here, we want colons
//TAPi18next.options.nsseparator = null;

// We don't use that postProcessor, it breaks variable interpolation
TAPi18next.options.postProcess = null;

// For an obscure reason the way TAPi18n handles missing keys (so any english key)
// was breaking variable interpolation, removing the TAPi18n added "project:" namespace here resolved the issue
TAPi18next.options.parseMissingKey = function(key) {
    return key.substr(key.indexOf(':') + 1);
};

// Make it global
global.__ = TAPi18n.__;
"use strict";

import Logger from "meteor/metalab:logger/Logger";
import {ValidatedMethod} from "meteor/mdg:validated-method";

const log = Logger( "i18n-methods" );

const validFile = new RegExp('^[a-zA-Z0-9\-\_]+$');

const I18nMethods = {

    /**
     * Create a group
     *
     * @param  {object} info User update information
     * @return {string} Group Id
     */
    getDocument: new ValidatedMethod( {
        name:     'pr/i18n/doc',
        validate: new SimpleSchema( {
            name: {
                type: String,
                custom: function() {
                    if ( this.value.match(validFile) == null ) {
                        return "notAllowed";
                    }
                }
            },
            lang: {
                type: String,
                optional: true,
                defaultValue: 'en'
            }
        } ).validator( { clean: true } ),
        run( { name, lang } ) {
            if ( Meteor.isServer ) {
                return Assets.getText( `i18n/${name}.${lang}.md` );
            }
        }
    } )

};

export default I18nMethods;
"use strict";

import logger from "meteor/metalab:logger/Logger";
import {TAPi18n} from "meteor/tap:i18n";
import {StoredVar} from "meteor/dispatch:stored-var";
import moment from "moment";
import "moment/locale/fr";

// Moment js locales

const log = logger( "i18n" );

// Stored variable holding the last selected language
export const language = new StoredVar( 'i18n.language' );

const i18n = {

    getLanguage:  TAPi18n.getLanguage.bind( TAPi18n ),
    getLanguages: TAPi18n.getLanguages.bind( TAPi18n ),

    setLanguage( lang, manual = false ) {
        if ( !_.contains( _.keys( TAPi18n.getLanguages() ), lang ) ) {
            log.warn( `Invalid language: ${lang}` );
            return;
        }

        /*if ( language == TAPi18n.getLanguage() ) {
         d && d('same language as before');
         return;
         }*/

        log.info( `Setting language: ${lang}` );

        Session.set( 'i18n.loading', true );

        moment.locale( lang );

        TAPi18n.setLanguage( lang )
            .done( () => {
                log.verbose( "Language loaded" );

                // Save to profile when manual
                if ( manual ) {
                    const user = Meteor.users ? Meteor.user() : null;
                    if ( user ) {
                        //TODO: Use a method, we don't want to expose the DB like this anymore
                        Meteor.users.update( { _id: user._id }, { $set: { 'profile.language': lang } } );
                    }
                }

                // Always save to local storage, we want to track the last used language
                // in order for a logging out user to continue seeing the application it its language.
                language.set( lang );

                Session.set( 'i18n.loading', false );
                Session.set( 'i18n.language', lang );
            } )
            .fail( ( error ) => {
                log.error( "Error loading language", error );
                Session.set( 'i18n.loading', false );
            } );

    }
};

export default i18n;
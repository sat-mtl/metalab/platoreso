"use strict";

import logger from "meteor/metalab:logger/Logger";
import { TAPi18next } from "meteor/tap:i18n";
import i18n, { language } from "../i18n";

/*

// We do want to fallback when the string wasn't translated
TAPi18next.options.fallbackOnEmpty = true;

// We use it gettext-style, so no key separator is needed
// The default was a dot and was failing for sentences
TAPi18next.options.keyseparator = null;
// Same thing here, we want colons
//TAPi18next.options.nsseparator = null;

// We don't use that postProcessor, it breaks variable interpolation
TAPi18next.options.postProcess = null;

// For an obscure reason the way TAPi18n handles missing keys (so any english key)
// was breaking variable interpolation, removing the TAPi18n added "project:" namespace here resolved the issue
TAPi18next.options.parseMissingKey = function(key) {
    return key.substr(key.indexOf(':') + 1);
};

*/

Meteor.startup(() => {
    const log = logger("i18n");

    log.info("Setting up i18n");

    // Default language is null, while we wait for detection
    Session.set('i18n.language', null);

    let lastSetLanguage = null;

    /**
     * Initialize the default loaded language and track user to change language when it changes
     */
    Tracker.autorun(() => {
        let lang;

        // User has priority over whatever is stored
        const user = Meteor.users ? Meteor.user() : null;
        if ( user && user.profile && user.profile.language ) {
            lang = user.profile.language;

            if ( lang ) {
                log.verbose("Using language from the user profile", lang);
            } else {
                log.verbose("No language found in the user profile");
            }
        }

        // Then we retrieve what is stored as the last language used
        if ( !lang ) {
            Tracker.nonreactive(() => {
                lang = language.get();
            });

            if ( lang ) {
                log.verbose("Using language from local storage", lang);
            } else {
                log.verbose("No language found in local storage");
            }
        }

        // Only then we try to auto-detect what we should use
        if ( !lang && window && window.navigator ) {
            lang = window.navigator.userLanguage || window.navigator.language;

            if ( lang ) {
                log.verbose("Using language from browser", lang);
            } else {
                log.verbose("No language found in browser");
            }
        }

        // Last, we set the default
        if ( !lang ) {
            lang = 'en';
            log.verbose ("Using default language", lang);
        }

        // Keep only the first part
        // https://github.com/TAPevents/tap-i18n/issues/102
        // https://github.com/TAPevents/tap-i18n/compare/master...ubald:patch-1
        lang = lang.split('-')[0];

        // This method uses reactive sources so set as non reactive
        if ( lang != lastSetLanguage ) {
            log.info('Requesting language: ' + lang);
            i18n.setLanguage( lang );
            lastSetLanguage = lang;
        }
    });

});
"use strict";

import faker from "faker";
import { Factory } from "meteor/dburles:factory";
import UserCollection from "/imports/accounts/UserCollection";
import CardCollection from "/imports/cards/CardCollection";

Factory.define( "user", UserCollection, {
    profile: {
        firstName: () => faker.name.firstName(),
        lastName:  () => faker.name.lastName(),
        biography: () => faker.lorem.sentence( faker.random.number( 300 ) ),
        avatarUrl: () => faker.image.avatar()
    },
    emails:  [
        {
            address:  faker.internet.email(),
            verified: faker.random.arrayElement( [true, true, true, true, false] )
        }
    ]
} );

Factory.define( "project", null, {} );

Factory.define( "phase", null, {} );

Factory.define( "minimal-card", CardCollection, {
    id:          Random.id(),
    name:        () => faker.random.boolean() ? faker.company.catchPhrase() : faker.company.bs(),
    name_sort:   () => (faker.random.boolean() ? faker.company.catchPhrase() : faker.company.bs()).toLowerCase(),
    slug:        () => faker.helpers.slugify( faker.random.boolean() ? faker.company.catchPhrase() : faker.company.bs() ),
    content:     () => faker.lorem.sentence( faker.random.number( 100 ) ),
    version:     () => faker.random.number( 12 ),
    author:      Factory.get( "user" ),
    project:     Factory.get( "project" ),
    phase:       Factory.get( "phase" ),
    phaseVisits: []
} );

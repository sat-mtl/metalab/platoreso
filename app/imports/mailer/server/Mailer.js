"use strict";

import path from "path";
import {EmailTemplate} from "email-templates"

import {Meteor} from "meteor/meteor";
import {Email} from "meteor/email";

import Logger from "meteor/metalab:logger/Logger";

const log = Logger( "Mailer" );

// "assets/app" is were :private: files are moved when app is built
export const templatesPath = "assets/app/mail-templates/";


// MAILER
export default class Mailer {

    static send( data, cb ) {
        const template = new EmailTemplate( path.join( templatesPath, data.template ) );
        template.render( data, Meteor.bindEnvironment( ( err, result ) => {
            if ( err ) {
                log.error( err );
                return cb( err );
            }

            try {
                Email.send( {
                    from:    Meteor.settings.email.from,
                    to:      data.to,
                    subject: result.subject,
                    text:    result.text,
                    html:    result.html
                } );
            } catch ( e ) {
                log.error( err );
                return cb( err );
            }

            cb();

        } ) );
    }

}
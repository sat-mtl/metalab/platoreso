"use strict";

import "./jobs/MailerJob";

// MAILER INITIALIZATION
import Handlebars from "handlebars";
import {TAPi18n} from "meteor/tap:i18n";
import UserHelpers from "/imports/accounts/UserHelpers";

Handlebars.registerHelper( 't', ( key, options ) => new Handlebars.SafeString( TAPi18n.__( key, _.extend( options.hash, { lng: options.data.root.user.profile.language || 'en' } ) ) ) );
Handlebars.registerHelper( 'concat', ( ...params ) => params.slice( 0, -1 ).join( '' ) );
Handlebars.registerHelper( 'displayName', user => UserHelpers.getDisplayName( user ) );
Handlebars.registerHelper( 'absoluteUrl', uri => Meteor.absoluteUrl( uri ) );
Handlebars.registerPartial( 'header', Assets.getText( 'mail-templates/header.hbs' ) );
Handlebars.registerPartial( 'footer', Assets.getText( 'mail-templates/footer.hbs' ) );

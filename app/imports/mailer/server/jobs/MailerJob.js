"use strict";

import {Meteor}  from "meteor/meteor";
import {Email} from "meteor/email";
import Logger from "meteor/metalab:logger/Logger";
import JobCollection from "/imports/jobs/JobCollection";
import Mailer from "../Mailer";

const log = Logger( "MailerJob" );

/**
 * Basic mailer job that handles sending emails "asynchronously"
 */
const mailQueue = JobCollection.processJobs( 'mail', { pollInterval: false, workTimeout: 60 * 1000 }, ( job, cb ) => {
    log.verbose("Processing mail queue");
    
    Mailer.send( job.data, err => {
        if ( err ) {
            log.error(err);
            job.fail( err );
            return cb();
        }
        
        log.verbose("Mail sent");
        job.done();
        cb();
    } );
} );

JobCollection.find( { type: 'mail', status: 'ready' } ).observeChanges( {
    added: () => mailQueue.trigger()
} );
"use strict";

import logger from "meteor/metalab:logger/Logger";
const log = logger("Router");

let routeLoaders = [];

export function registerRouteLoader(routeLoader) {
    routeLoaders.push(routeLoader);
}

export function loadRoutes(location, cb) {
    log.info('Loading dynamic routes');

    const length = routeLoaders.length;
    let i = 0;
    let routes = [];

    function next() {
        if ( i < length ) {
            routeLoaders[i](location, loadedRoutes => {
                routes = routes.concat(loadedRoutes);
                i++;
                next();
            } );
        } else {
            cb(null, routes);
        }
    }
    next();
}
"use strict";

import Colors from "colors/safe";

// Enable colors in custom messages
Colors.enabled = true;

console.log("\n");
console.log(Colors.cyan("┌─┐┬  ┌─┐┌┬┐┌─┐┬─┐┌─┐┌─┐┌─┐"));
console.log(Colors.cyan("├─┘│  ├─┤ │ │ │├┬┘├┤ └─┐│ │"));
console.log(Colors.cyan("┴  ┴─┘┴ ┴ ┴ └─┘┴└─└─┘└─┘└─┘"));
console.log(`${Colors.cyan.bold("PlatoReso")} ${Colors.gray("-")} Version: ${Colors.bold(version)}` );
console.log("\n");
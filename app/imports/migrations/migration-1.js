"use strict";

import { Migrations } from "meteor/percolate:migrations"
import AvatarCollection from "../../imports/accounts/AvatarCollection";
import GroupImagesCollection from "../../imports/groups/GroupImagesCollection";
import ProjectImagesCollection from "../../imports/projects/ProjectImagesCollection";
import CardImagesCollection from "../../imports/cards/images/CardImagesCollection";

const collections = [AvatarCollection, GroupImagesCollection, ProjectImagesCollection, CardImagesCollection];

Migrations.add( {
    version: 1,
    name:    'Change upload owner to array for multiple references to the same file',
    up() {
        collections.forEach( collection => {
            collection.find( {owners: {$exists: false}}, {
                fields: {
                    _id:   1,
                    owner: 1
                }
            }, {transform: null} ).forEach( image => {
                collection.update( {_id: image._id}, {
                    $unset: {owner: null},
                    $set:   {owners: [image.owner]}
                }, {multi: true} );
            } );
        } );
    },
    down() {
        collections.forEach( collection => {
            collection.find( {owner: {$exists: false}}, {
                fields: {
                    _id:    1,
                    owners: 1
                }
            }, {transform: null} ).forEach( image => {
                collection.update( {_id: image._id}, {
                    $unset: {owners: null},
                    $set:   {owner: image.owners[0]}
                }, {multi: true} );
            } );
        } );
    }
} );
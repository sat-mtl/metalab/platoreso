"use strict";

import {Migrations} from "meteor/percolate:migrations";
import ProjectCollection from "../projects/ProjectCollection";

Migrations.add( {
    version: 14,
    name:    'Set all phases as not removed',
    up() {
        ProjectCollection.find( {}, { fields: { _id: 1, phases: 1 }, transform: null } ).forEach( project => {
            const phases = project.phases || [];
            phases.forEach( phase => phase.removed = false );
            ProjectCollection.direct.update( { _id: project._id }, { $set: { phases: phases } } );
        } );
    },
    down() {
        ProjectCollection.find( {}, { fields: { _id: 1, phases: 1 }, transform: null } ).forEach( project => {
            const phases = project.phases || [];
            phases.forEach( phase => delete phase.removed );
            ProjectCollection.direct.update( { _id: project._id }, { $set: { phases: phases } } );
        } );
    }
} );
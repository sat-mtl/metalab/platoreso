"use strict";

import { Migrations } from "meteor/percolate:migrations";
import CardCollection from "../../imports/cards/CardCollection";
import CardLinksCollection from "../../imports/cards/links/CardLinksCollection";
import FeedCollection from "../../imports/feed/FeedCollection";
import NotificationCollection from "../../imports/notifications/NotificationCollection";

Migrations.add( {
    version: 11,
    name:    'Feed + Notifications adjustments for link types',
    up() {
        [ FeedCollection, NotificationCollection ].forEach( collection => {
            collection.update(
                {
                    $or:            [
                        { action: 'linked' },
                        { action: 'linked-to' },
                        { action: 'unlinked' }
                    ],
                    'objects.role': 'from'
                },
                {
                    $set: {
                        'objects.$.role': 'source'
                    }
                },
                {
                    multi: true
                }
            );
            collection.update(
                {
                    $or:            [
                        { action: 'linked' },
                        { action: 'linked-to' },
                        { action: 'unlinked' }
                    ],
                    'objects.role': 'to'
                },
                {
                    $set: {
                        'objects.$.role': 'destination'
                    }
                },
                {
                    multi: true
                }
            );
            collection.find(
                {
                    $or: [
                        { action: 'linked' },
                        { action: 'linked-to' },
                        { action: 'unlinked' }
                    ]
                }
            ).forEach( story => {
                const link = CardLinksCollection.findOne( {
                    source:      story.getObjectIdByType( CardCollection.entityName, 'source' ),
                    destination: story.getObjectIdByType( CardCollection.entityName, 'destination' )
                } );
                if ( link ) {
                    collection.update( { _id: story._id }, { $set: { 'data.linkType': link.type } } );
                }
            } );
        });
    },
    down() {
        [ FeedCollection, NotificationCollection ].forEach( collection => {
            collection.update(
                {
                    $or:            [
                        { action: 'linked' },
                        { action: 'linked-to' },
                        { action: 'linked-both' },
                        { action: 'unlinked' }
                    ],
                    'objects.role': 'source'
                },
                {
                    $set: {
                        'objects.$.role': 'from'
                    }
                },
                {
                    multi: true
                }
            );
            collection.update(
                {
                    $or:            [
                        { action: 'linked' },
                        { action: 'linked-to' },
                        { action: 'linked-both' },
                        { action: 'unlinked' }
                    ],
                    'objects.role': 'destination'
                },
                {
                    $set: {
                        'objects.$.role': 'to'
                    }
                },
                {
                    multi: true
                }
            );
            collection.update(
                {
                    $or: [
                        { action: 'linked' },
                        { action: 'linked-to' },
                        { action: 'linked-both' },
                        { action: 'unlinked' }
                    ]
                },
                {
                    $unset: {
                        'data.linkType': null
                    }
                },
                {
                    multi: true
                }
            );
        });
    }
} )
;
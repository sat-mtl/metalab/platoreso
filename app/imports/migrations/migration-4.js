"use strict";

import { Migrations } from "meteor/percolate:migrations"
import CardCollection from "../cards/CardCollection";
import PhaseVisits from "../cards/phases/PhaseVisits";

Migrations.add( {
    version: 4,
    name:    'Add a phaseVisit to each card for its current phase',
    up() {
        CardCollection.find(
            {
                phaseVisits: {$exists: false}
            },
            {
                fields:    {_id: 1, phase: 1},
                transform: null
            }
        ).forEach( card => CardCollection.direct.update(
            {
                _id: card._id
            },
            {
                $set: {
                    phaseVisits: [ PhaseVisits.createVisit( card.phase, PhaseVisits.visitTypes.created ) ]
                }
            },
            {
                bypassCollection2: true
            }
            )
        );
    },
    down() {
        CardCollection.direct.update( {phaseVisits: {$exists: true}}, {$unset: {phaseVisits: true}}, {
            multi:             true,
            bypassCollection2: true
        } );
    }
} );
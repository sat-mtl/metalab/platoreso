"use strict";

import { Migrations } from "meteor/percolate:migrations"
import ProjectCollection from "../projects/ProjectCollection";

Migrations.add( {
    version: 3,
    name:    'Remove allowImageUpload from phases',
    up() {
        ProjectCollection.find( { 'phases.allowImageUpload': { $exists: true } }, { fields: { _id: 1, phases: 1 }, transform: null } ).forEach( project => {
            const phases = project.phases.map( phase => {
                delete phase.allowImageUpload;
                return phase;
            } );
            ProjectCollection.direct.update( { _id: project._id }, { $set: { phases: phases } }, { bypassCollection2: true } );
        } );
    },
    down() {
        ProjectCollection.find( {}, { fields: { _id: 1, phases: 1 }, transform: null } ).forEach( project => {
            const phases = project.phases.map( phase => {
                phase.allowImageUpload = true;
                return phase;
            } );
            ProjectCollection.direct.update( { _id: project._id }, { $set: { phases: phases } }, { bypassCollection2: true } );
        } );
    }
} );
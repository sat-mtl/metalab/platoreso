"use strict";

import { Migrations } from "meteor/percolate:migrations";
import UserCollection from "/imports/accounts/UserCollection";

Migrations.add( {
    version: 20,
    name:    'Use https for profile images + set everyone as not having accepted terms',
    up() {
        // Update facebook/twitter avatar url
        UserCollection.find( {} ).forEach( user => {
            if (
                user.profile.avatarUrl
                && user.profile.avatarUrl.indexOf( 'http://graph.facebook.com/' ) != -1
                && user.services.facebook
                && user.services.facebook.id
            ) {
                UserCollection.update( { _id: user._id }, {
                    $set: {
                        'profile.avatarUrl': 'https://graph.facebook.com/' + user.services.facebook.id + '/picture/?type=large'
                    }
                } );
            }

            if (
                user.profile.avatarUrl
                && user.profile.avatarUrl.indexOf( 'twimg.com' ) != -1
                && user.services.twitter
                && user.services.twitter.profile_image_url_https
            ) {
                UserCollection.update( { _id: user._id }, {
                    $set: {
                        'profile.avatarUrl': user.services.twitter.profile_image_url_https
                    }
                } );
            }

            UserCollection.update( { _id: user._id }, {
                $set: {
                    unsubscribeToken: Random.id()
                }
            } );

        } );

        // Set everyone as not accepted
        UserCollection.update( {}, {
            $set: {
                acceptedTerms:               false,
                'profile.allowEmailContact': false
            }
        }, { multi: true } );
    },
    down() {

    }
} );
"use strict";

import { Migrations } from "meteor/percolate:migrations";
import CardCollection from "/imports/cards/CardCollection";
import CommentCollection from "/imports/comments/CommentCollection";

Migrations.add( {
    version: 17,
    name:    'Change the way comments reference their parent entity',
    up() {
        // Remove old index
        try {
            CommentCollection._dropIndex( 'entityType_entity_moderated_createdAt' );
        } catch ( e ) {
        }

        // For now they are all card comments so this is simple
        CommentCollection.find( {}, { fields: { entity: 1 }, transform: null } ).forEach( comment => {
            const card = CardCollection.findOne( { id: comment.entity, $or: [{ current: true }, { deleted: true }] }, { fields: { id: 1, project: 1 } } );
            if ( !card ) {
                console.warn( 'card not found', comment );
            }

            CommentCollection.direct.update(
                { _id: comment._id },
                {
                    $set:   {
                        objects: card.objectAncestors
                    },
                    $unset: {
                        entityType: null,
                        entity:     null
                    }
                },
                {
                    bypassCollection2: true
                }
            );
        } );

        // Recount all comments
        CardCollection.find( {}, { fields: { _id: 1, id: 1, current: 1, versionedAt: 1 }, transform: null } ).forEach( card => {
            const query = {
                'objects.0.type': CardCollection.entityName,
                'objects.0.id':   card.id
            };
            if ( !card.current ) {
                query.createdAt = { $lte: card.versionedAt };
            }
            const commentCount = CommentCollection.find( query, { fields: { _id: 1 } } ).count();
            CardCollection.direct.update( { _id: card._id }, { $set: { commentCount: commentCount } } );
        } );
    },
    down() {
        // For now they are all card comments so this is simple
        CommentCollection.find( {}, { fields: { objects: 1 }, transform: null } ).forEach( comment => {
            CommentCollection.direct.update(
                { _id: comment._id },
                {
                    $set:   {
                        entityName: comment.objects[0].type,
                        entity:     comment.objects[0].id
                    },
                    $unset: {
                        objects: null
                    }
                },
                {
                    bypassCollection2: true
                }
            );
        } );
    }
} );
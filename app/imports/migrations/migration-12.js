"use strict";

import { Migrations } from "meteor/percolate:migrations"
import UserCollection from "../../imports/accounts/UserCollection";
import CardCollection from "../../imports/cards/CardCollection";
import CardHistoryCollection from "./legacy/CardHistoryCollection";

Migrations.add( {
    version: 12,
    name:    'Fix for emailNotifications field not being added to created users and linkedCards still being in the document',
    up() {
        UserCollection.direct.update( {
            'profile.emailNotifications': { $exists: false }
        }, {
            $set: {
                'profile.emailNotifications': {
                    cardComment:      true,
                    cardCommentReply: true
                }
            }
        }, { multi: true, bypassCollection2: true } );


        // Remove the fields from the collections
        CardCollection.direct.update( {}, { $unset: { linkedCards: null } }, { multi: true, bypassCollection2: true } );
        CardHistoryCollection.direct.update( {}, { $unset: { linkedCards: null } }, { multi: true, bypassCollection2: true } );
    },
    down() {
        // nothing to revert, we're just fixing a bug
    }
} );
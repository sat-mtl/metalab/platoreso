"use strict";

import { Migrations } from "meteor/percolate:migrations"
import FeedCollection from "../../imports/feed/FeedCollection";

Migrations.add( {
    version: 2,
    name:    'Change user to subject, subjects to objects, object to data and rename actions',
    up() {
        // Rename fields
        FeedCollection.direct.update( {}, {
            $rename: {
                'user':     'subject',
                'subjects': 'objects',
                'object':   'data'
            }
        }, { multi: true, bypassCollection2: true } );

        // Rename actions
        FeedCollection.direct.update( { action: 'create' }, { $set: { action: 'created' } }, { multi: true } );
        FeedCollection.direct.update( { action: 'update' }, { $set: { action: 'updated' } }, { multi: true } );
        FeedCollection.direct.update( { action: 'like' }, { $set: { action: 'liked' } }, { multi: true } );
        FeedCollection.direct.update( { action: 'unlike' }, { $set: { action: 'unliked' } }, { multi: true } );

        FeedCollection.direct.update( { action: 'comment' }, { $set: { action: 'commented' } }, { multi: true } );
        FeedCollection.direct.update( { action: 'comment-update' }, { $set: { action: 'edited-comment' } }, { multi: true } );
        FeedCollection.direct.update( { action: 'reply' }, { $set: { action: 'replied' } }, { multi: true } );
        FeedCollection.direct.update( { action: 'reply-update' }, { $set: { action: 'edited-reply' } }, { multi: true } );
    },
    down() {

        // Rename actions
        FeedCollection.direct.update( { action: 'created' }, { $set: { action: 'create' } }, { multi: true } );
        FeedCollection.direct.update( { action: 'updated' }, { $set: { action: 'update' } }, { multi: true } );
        FeedCollection.direct.update( { action: 'liked' }, { $set: { action: 'like' } }, { multi: true } );
        FeedCollection.direct.update( { action: 'unliked' }, { $set: { action: 'unlike' } }, { multi: true } );

        FeedCollection.direct.update( { action: 'commented' }, { $set: { action: 'comment' } }, { multi: true } );
        FeedCollection.direct.update( { action: 'edited-comment' }, { $set: { action: 'comment-update' } }, { multi: true } );
        FeedCollection.direct.update( { action: 'replied' }, { $set: { action: 'reply' } }, { multi: true } );
        FeedCollection.direct.update( { action: 'edited-reply' }, { $set: { action: 'reply-update' } }, { multi: true } );

        // Rename fields
        FeedCollection.direct.update( {}, {
            $rename: {
                'subject': 'user',
                'objects': 'subjects',
                'data':    'object'
            }
        }, { multi: true, bypassCollection2: true } );
    }
} );
"use strict";

import prune from "underscore.string/prune";
import { Migrations } from "meteor/percolate:migrations";
import { MS_PER_HOUR } from "/imports/utils/TimeUtils";
import CardCollection from "/imports/cards/CardCollection";
import SettingsCollection from "/imports/settings/SettingsCollection";
import ProjectCollection from "/imports/projects/ProjectCollection";

Migrations.add( {
    version: 16,
    name:    'Add card excerpt to all cards + add project heat',
    up() {
        // Excerpt
        CardCollection.find( {}, { fields: { _id: 1, content: 1 }, transform: null } ).forEach( card => {
            CardCollection.direct.update( { _id: card._id }, { $set: { excerpt: prune( card.content || '', 512, '' ) } }, { bypassCollection2: true } );
        } );

        // Analytics
        const now = new Date();
        const cooldown = 0.1;

        SettingsCollection.direct.update( {}, {
            $set: {
                'shared.analytics.project': {
                    cooldown: cooldown
                }
            }
        }, {
            multi:             true,
            bypassCollection2: true
        } );

        ProjectCollection.find( {}, { fields: { _id: 1 }, transform: null } ).forEach( project => {
            const temperature = CardCollection.aggregate(
                [
                    {
                        $match: {
                            project:   project._id,
                            current:   true,
                            moderated: false
                        }
                    },
                    {
                        $project: {
                            temp:             {
                                $max: [
                                    0,
                                    {
                                        $subtract: [
                                            '$lastTemperature',
                                            {
                                                $multiply: [
                                                    {
                                                        $divide: [
                                                            {
                                                                $subtract: [
                                                                    now,
                                                                    '$heatedAt'
                                                                ]
                                                            },
                                                            MS_PER_HOUR
                                                        ]
                                                    },
                                                    cooldown
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            },
                            totalTemperature: 1
                        }
                    },
                    {
                        $group: {
                            _id:   "projectTemperature",
                            last:  { $sum: "$temp" },
                            total: { $sum: "$totalTemperature" }
                        }
                    }
                ]
            );

            ProjectCollection.direct.update( { _id: project._id }, {
                $set: {
                    lastTemperature:  temperature.length ? temperature[0].last : 0,
                    totalTemperature: temperature.length ? temperature[0].total : 0,
                    heatedAt:         now
                }
            }, {
                bypassCollection2: true
            } );
        } );
    },
    down() {
        // Analytics
        SettingsCollection.direct.update( {}, {
            $unset: { 'shared.analytics.project': null }
        }, {
            multi:             true,
            bypassCollection2: true
        } );

        ProjectCollection.direct.update( {}, {
            $unset: {
                totalTemperature: null,
                lastTemperature:  null,
                heatedAt:         null
            }
        }, {
            multi:             true,
            bypassCollection2: true
        } );

        // Excerpt
        CardCollection.direct.update( {}, { $unset: { excerpt: null } }, { multi: true, bypassCollection2: true } );
    }
} );
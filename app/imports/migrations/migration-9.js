"use strict";

import { Migrations } from "meteor/percolate:migrations";
import CardCollection from "../../imports/cards/CardCollection";
import CardLinksCollection from "../../imports/cards/links/CardLinksCollection";
import CardHistoryCollection from "./legacy/CardHistoryCollection";
import SettingsCollection from "../../imports/settings/SettingsCollection";
import CommentCollection from "../../imports/comments/CommentCollection";

Migrations.add( {
    version: 9,
    name:    'Adding analytics configuration',
    up() {
        const now = new Date();

        const analyticsSettings = {
            card: {
                heat:     {
                    creation:        1,
                    update:          0.25,
                    view:            0.75,
                    like:            1,
                    comment:         1.25,
                    linkSource:      0.75,
                    linkDestination: 0.75,
                    move:            0.5
                },
                cooldown: 0.1
            }
        };

        SettingsCollection.direct.update( {}, {
            $set: {
                'shared.analytics': analyticsSettings
            }
        }, {
            multi:             true,
            bypassCollection2: true
        } );

        CardCollection.direct.update( {}, {
            $set: {
                totalTemperature: 0,
                lastTemperature:  0,
                heatedAt:         now
            }
        }, {
            multi:             true,
            bypassCollection2: true
        } );

        CardHistoryCollection.direct.update( {}, {
            $set: {
                totalTemperature: 0,
                lastTemperature:  0,
                heatedAt:         now
            }
        }, {
            multi:             true,
            bypassCollection2: true
        } );

        // Estimate a total temperature for each card
        CardCollection.find( {} ).forEach( card => {
            let temp = analyticsSettings.card.heat.creation; // It is there so it was created
            temp += card.version * analyticsSettings.card.heat.update;
            temp += card.viewCount * analyticsSettings.card.heat.view;
            temp += card.likeCount * analyticsSettings.card.heat.like;
            temp += CommentCollection.find( { entityType: CardCollection.entityName, entity: card._id } ).count() * analyticsSettings.card.heat.comment;
            temp += CardLinksCollection.find( { source: card._id } ).count() * analyticsSettings.card.heat.linkSource;
            temp += CardLinksCollection.find( { destination: card._id } ).count() * analyticsSettings.card.heat.linkDestination;
            temp += card.phaseVisits.length * analyticsSettings.card.heat.move;

            CardCollection.direct.update( { _id: card._id }, { $set: { totalTemperature: temp } }, { bypassCollection2: true } );
        } );
    },
    down() {
        SettingsCollection.direct.update( {}, {
            $unset: { 'shared.analytics': null }
        }, {
            multi:             true,
            bypassCollection2: true
        } );

        CardCollection.direct.update( {}, {
            $unset: {
                totalTemperature: null,
                lastTemperature:  null,
                heatedAt:         null
            }
        }, {
            multi:             true,
            bypassCollection2: true
        } );

        CardHistoryCollection.direct.update( {}, {
            $unset: {
                totalTemperature: null,
                lastTemperature:  null,
                heatedAt:         null
            }
        }, {
            multi:             true,
            bypassCollection2: true
        } );
    }
} )
;
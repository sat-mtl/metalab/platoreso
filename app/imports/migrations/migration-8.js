"use strict";

import { Migrations } from "meteor/percolate:migrations"
import CardLinkTypes from "../../imports/cards/links/CardLinkTypes";
import CardCollection from "../../imports/cards/CardCollection";
import CardHistoryCollection from "./legacy/CardHistoryCollection";
import CardLinksCollection from "../../imports/cards/links/CardLinksCollection";

Migrations.add( {
    version: 8,
    name:    'Converted linkedCards to support type and history',
    up() {
        // Only keep the links on active cards, it's too complicated to try and recover old links from card snapshots
        // Besides, not a lot of people archived cards and/or used links extensively so it's not that bad.
        CardCollection
            .find( {}, { fields: { _id: 1, linkedCards: 1, createdAt: 1, createdBy: 1 }, transform: null } )
            .forEach( card => {
                // Nothing, so don't try creating links
                if ( typeof(card.linkedCards) == 'undefined' || card.linkedCards == null ) {
                    return;
                }

                card.linkedCards.forEach( linkedCardId => {
                    // Only create the link if we can find the card, this will clean possible corrupted links
                    const linkedCard = CardCollection.findOne( { _id: linkedCardId }, { fields: { _id: 1, createdAt: 1 }, transform: null } );
                    if ( !linkedCard ) {
                        return;
                    }

                    // Check if we already created a link for this connection, this will clean up possible duplicates
                    const link = CardLinksCollection.findOne( {
                        source:      linkedCard._id,
                        destination: card._id,
                        type:        CardLinkTypes.references.id,
                    }, { fields: { _id: 1 }, transform: null } );

                    if ( link ) {
                        return;
                    }

                    CardLinksCollection.direct.insert( {
                        source:      linkedCard._id,
                        destination: card._id,
                        type:        CardLinkTypes.references.id,
                        createdAt:   new Date( Math.max( card.createdAt, linkedCard.createdAt ) )
                    } );
                } );
            } );

        // Remove the fields from the collections
        CardCollection.direct.update( {}, { $unset: { linkedCards: null } }, { multi: true, bypassCollection2: true } );
        CardHistoryCollection.direct.update( {}, { $unset: { linkedCards: null } }, { multi: true, bypassCollection2: true } );
    },
    down() {
        CardLinksCollection.find( {} ).forEach( link => {
            const collection  = link.removed ? CardHistoryCollection : CardCollection;
            const idAttribute = link.removed ? 'ref' : '_id';

            collection.direct.update( { [idAttribute]: link.destination }, {
                $addToSet: {
                    linkedCards: link.source
                }
            }, {
                multi:             true,
                bypassCollection2: true
            } );
        } );
    }
} )
;
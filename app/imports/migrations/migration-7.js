"use strict";

import {Migrations} from "meteor/percolate:migrations"
import UserCollection from "/imports/accounts/UserCollection";
import CardCollection from "/imports/cards/CardCollection";
import CardAttachmentsCollection from "/imports/cards/attachments/CardAttachmentsCollection";

Migrations.add( {
    version: 7,
    name:    'Added emailNotifications to user profile, attachmentCount to cards',
    up() {
        UserCollection.direct.update( {}, {
            $set: {
                'profile.emailNotifications': {
                    cardComment:      true,
                    cardCommentReply: true
                }
            }
        }, { multi: true } );
        CardCollection.find( {}, { fields: { _id: 1, version: 1 } } ).forEach( card => CardAttachmentsCollection.updateAttachmentCount( card._id, card.version ) );
    },
    down() {
        UserCollection.direct.update( {}, { $unset: { 'profile.emailNotifications': null } }, { multi: true } );
        CardCollection.direct.update( {}, { $unset: { attachmentCount: null } }, { multi: true } );
    }
} );
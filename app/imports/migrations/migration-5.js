"use strict";

import { Migrations } from "meteor/percolate:migrations"
import FeedCollection from "../../imports/feed/FeedCollection";

Migrations.add( {
    version: 5,
    name:    'Removed comment from feed objects',
    up() {
        FeedCollection.find({ action: { $in: ['commented', 'edited-comment', 'replied', 'edited-reply']}} ).forEach( story => {
            const objects = story.objects.slice(1);
            let data = {
                comment: story.objects[0].id
            };
            if ( story.data ) {
                data.changes = story.data;
            }
            FeedCollection.direct.update({_id: story._id}, { $set: { objects: objects, data: data }});
        });
    },
    down() {
        FeedCollection.find({ action: { $in: ['commented', 'edited-comment', 'replied', 'edited-reply']}} ).forEach( story => {
            const comment = story.data.comment;
            story.objects.unshift({
                id: comment,
                type: 'comment'
            });
            FeedCollection.direct.update({_id: story._id}, { $set: { objects: story.objects, data: story.data.changes || {} } } );
        });
    }
} );
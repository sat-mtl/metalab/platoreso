"use strict";

import { Migrations } from "meteor/percolate:migrations";
import SettingsCollection from "../../imports/settings/SettingsCollection";

Migrations.add( {
    version: 18,
    name:    'Adding gamification to comments',
    up() {
        SettingsCollection.direct.update( {}, {
            $set: {
                'shared.gamification.points.comment': {
                    like: 1
                }
            }
        }, {
            multi:             true,
            bypassCollection2: true
        } );
    },
    down() {
        SettingsCollection.direct.update( {}, {
            $unset: { 'shared.gamification.points.comment': null }
        }, {
            multi:             true,
            bypassCollection2: true
        } );
    }
} )
;
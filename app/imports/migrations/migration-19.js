"use strict";

import {Migrations} from "meteor/percolate:migrations";
import ConversationCollection from "/imports/messages/ConversationCollection";
import UserCollection from "/imports/accounts/UserCollection";
import GroupCollection from "/imports/groups/GroupCollection";
import {GroupRoles, GROUP_PREFIX} from "/imports/groups/GroupRoles";

Migrations.add( {
    version: 19,
    name:    'Adding conversations for groups',
    up() {
        const now = new Date();
        ConversationCollection.remove( { type: 'group' } );
        GroupCollection.find( {}, { fields: { _id: 1, slug: 1, name: 1 }, transform: null } ).forEach( group => {
            const participants = UserCollection.find( {
                completed:                                   true,
                ['roles.' + GROUP_PREFIX + '_' + group._id]: { $in: GroupRoles }
            }, {
                fields: {
                    _id: 1
                }
            } ).map( user => ({ id: user._id, unreadCount: 0, readAt: now }) );

            ConversationCollection.insert( {
                type:   'group',
                entity: group._id,
                slug:   group.slug || group._id,
                name:   group.name,
                topic:  group.description,
                        participants
            } );
        } );


    },
    down() {
        ConversationCollection.remove( { type: 'group' } );
    }
} );
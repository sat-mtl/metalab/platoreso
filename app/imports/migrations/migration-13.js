"use strict";

import { Migrations } from "meteor/percolate:migrations"
import CardCollection from "../cards/CardCollection";
import CardLinksCollection from "../cards/links/CardLinksCollection";

function getCardHistoryCollection() {
    const module = require( './legacy/CardHistoryCollection' );
    if ( module && module.default ) {
        return module.default;
    } else {
        return new Mongo.Collection( 'card_history' );
    }
}

Migrations.add( {
    version: 13,
    name:    'Link versioning',
    up() {
        const CardHistoryCollection = getCardHistoryCollection();

        // Remove unused index
        try {
            CardCollection._dropIndex( 'c2_slug' );
        } catch ( e ) {
            console.warn( 'Index to be removed "c2_slug", not found!' )
        }

        // Copy card _id to id and add the current field
        CardCollection.find( {}, { field: { _id: 1 }, transform: null } ).forEach( card => {
            CardCollection.direct.update( { _id: card._id }, { $set: { id: card._id, current: true } } );
        } );

        // Copy snapshots back to cards collection
        CardHistoryCollection.find( {}, { transform: null } ).forEach( snapshot => {
            snapshot.id = snapshot.ref;
            delete snapshot.ref;
            snapshot.current = false;
            CardCollection.direct.insert( snapshot, { bypassCollection2: true } );
        } );

        // Drop the now useless collection
        CardHistoryCollection.rawCollection().drop();

        // Update links, forget removed links, we're still not at a point were non-active links matter
        CardLinksCollection.find( {} ).forEach( link => {
            let sourceFrom = CardCollection.findOne(
                { id: link.source, current: false, versionedAt: { $lte: link.createdAt } },
                { sort: { versionedAt: -1 }, fields: { version: 1 }, transform: null }
            );
            if ( !sourceFrom ) {
                sourceFrom = CardCollection.findOne(
                    { id: link.source, current: true },
                    { fields: { version: 1 }, transform: null }
                );
            }
            if ( !sourceFrom ) {
                console.warn( 'Forgetting previously deleted link', link );
                CardLinksCollection.direct.remove( { _id: link._id } );
                return
            }

            let destinationFrom = CardCollection.findOne(
                { id: link.destination, current: false, versionedAt: { $lte: link.createdAt } },
                { sort: { versionedAt: -1 }, fields: { version: 1 }, transform: null }
            );
            if ( !destinationFrom ) {
                destinationFrom = CardCollection.findOne(
                    { id: link.destination, current: true },
                    { fields: { version: 1 }, transform: null }
                );
            }
            if ( !destinationFrom ) {
                console.warn( 'Forgetting previously deleted link', link );
                CardLinksCollection.direct.remove( { _id: link._id } );
                return
            }

            CardLinksCollection.direct.update( { _id: link._id }, {
                $set:   {
                    source:      {
                        id:          link.source,
                        versionFrom: sourceFrom.version
                    },
                    destination: {
                        id:          link.destination,
                        versionFrom: destinationFrom.version
                    },
                    linkedAt:    link.createdAt
                },
                $unset: {
                    createdAt: null,
                    createdBy: null,
                    removedAt: null,
                    removedBy: null,
                }
            }, { bypassCollection2: true } );
        } );
    },
    down() {
        const CardHistoryCollection = getCardHistoryCollection();

        // Move snapshots back to its own collection
        CardCollection.find( { current: false }, { transform: null } ).forEach( card => {
            card.ref = card.id;
            delete card.id;
            delete card.current;
            CardHistoryCollection.direct.insert( card, { bypassCollection2: true } );
            CardCollection.direct.remove( { _id: card._id } );
        } );

        // Move id back to _id and remove current
        CardCollection.find( {}, { transform: null } ).forEach( card => {
            CardCollection.direct.update( { _id: card._id }, { $set: { _id: card.id }, $unset: { id: null, current: null } }, { bypassCollection2: true } );
        } );
    }
} );
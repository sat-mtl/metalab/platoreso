"use strict";

import { Migrations } from "meteor/percolate:migrations";
import UserCollection from "../../imports/accounts/UserCollection";
import ProjectCollection from "../../imports/projects/ProjectCollection";
import CardCollection from "../../imports/cards/CardCollection";
import CommentCollection from "../../imports/comments/CommentCollection";
import SettingsCollection from "../../imports/settings/SettingsCollection";

Migrations.add( {
    version: 10,
    name:    'Adding gamification configuration',
    up() {
        const now = new Date();

        const gamificationSettings = {
            points: {
                card: {
                    creation: 10,
                    like:     2,
                    comment:  5,
                    link:     5,
                    move:     5
                }
            }
        };

        SettingsCollection.direct.update( {}, {
            $set: {
                'shared.gamification': gamificationSettings
            }
        }, {
            multi:             true,
            bypassCollection2: true
        } );

        // Calculate every user's points from what we can gather at the moment
        let projects = [];
        let cardIds  = {};
        ProjectCollection.find( {}, { fields: { _id: 1 } } ).forEach( project => {
            projects.push( project._id );
            cardIds[project._id] = CardCollection.find( { project: project._id }, { fields: { _id: 1 } } ).map( card => card._id );
        } );

        UserCollection.find( {}, { fields: { _id: 1 } } ).forEach( user => {
            let points           = 0;
            let pointsPerProject = {};

            points += gamificationSettings.points.card.creation * CardCollection.find( { author: user._id } ).count();
            points += gamificationSettings.points.card.like * CardCollection.find( { likes: user._id } ).count();
            points += gamificationSettings.points.card.comment * CommentCollection.find( { author: user._id, entityType: CardCollection.entityName } ).count();

            projects.forEach( projectId => {
                let projectPoints = 0;
                projectPoints += gamificationSettings.points.card.creation * CardCollection.find( { author: user._id, project: projectId } ).count();
                projectPoints += gamificationSettings.points.card.like * CardCollection.find( { likes: user._id, project: projectId } ).count();
                projectPoints += gamificationSettings.points.card.comment * CommentCollection.find( { author: user._id, entityType: CardCollection.entityName, entity: { $in: cardIds[projectId] } } ).count();
                if ( projectPoints > 0 ) {
                    pointsPerProject[projectId] = projectPoints;
                }
            } );

            UserCollection.direct.update( { _id: user._id }, { $set: { points: points, pointsPerProject: pointsPerProject } }, { bypassCollection2: true } );
        } );
    },
    down() {
        SettingsCollection.direct.update( {}, {
            $unset: { 'shared.gamification': null }
        }, {
            multi:             true,
            bypassCollection2: true
        } );

        UserCollection.direct.update( {}, {
            $unset: {
                points: null,
                pointsPerProject: null
            }
        }, {
            multi:             true,
            bypassCollection2: true
        } );
    }
} )
;
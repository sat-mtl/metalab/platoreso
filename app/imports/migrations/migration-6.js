"use strict";

import { Migrations } from "meteor/percolate:migrations"
import CardCollection from "/imports/cards/CardCollection";
import CardImagesCollection from "/imports/cards/images/CardImagesCollection";
import CardAttachmentsCollection from "/imports/cards/attachments/CardAttachmentsCollection";
import FeedCollection from "/imports/feed/FeedCollection";
import UserCollection from "/imports/accounts/UserCollection";

Migrations.add( {
    version: 6,
    name:    'Added card versioning',
    up() {
        CardCollection.direct.update( { version: { $exists: false } }, { $set: { version: 0 } }, {
            multi:             true,
            bypassCollection2: true
        } );

        CardCollection.find( {}, { fields: { _id: 1, version: 1, phaseVisits: 1 } } ).forEach( card => {
            if ( card.phaseVisits ) {
                let lastDate         = null;
                let lastPhase        = null;
                const newPhaseVisits = card.phaseVisits
                                           // Sort by descending date for our modifications
                                           .sort( ( a, b ) => b.date - a.date )
                                           // Modify each phase visit
                                           .map( ( phaseVisit, index ) => {
                                               // Assume they all just entered with their current version
                                               phaseVisit.enteredVersion = card.version;
                                               phaseVisit.enteredAt      = phaseVisit.date;
                                               if ( index != 0 ) {
                                                   // Assume they exited at their current version too, we don't have the history yet
                                                   phaseVisit.exitedVersion = card.version;
                                                   phaseVisit.exitedAt      = lastDate;
                                                   phaseVisit.exitedTo      = lastPhase;
                                               } else if ( index < card.phaseVisits.length - 1 ) {
                                                   phaseVisit.enteredFrom = card.phaseVisits[index + 1].phase;
                                               }
                                               // Keep this visit's date for the next's existed date
                                               lastDate  = phaseVisit.date;
                                               lastPhase = phaseVisit.phase;
                                               delete phaseVisit.date;
                                               return phaseVisit;
                                           } )
                                           // Resort in chronological order
                                           .sort( ( a, b ) => a.enteredAt - b.enteredAt );

                // Update the phase visits
                CardCollection.direct.update( { _id: card._id }, { $set: { phaseVisits: newPhaseVisits } }, { bypassCollection2: true } );
            }
        } );

        CardImagesCollection.find( {} ).forEach( image => {

            let copies;
            if ( image.copies ) {
                copies                          = {
                    card_images_original: image.copies.card_original,
                    card_images_large:    image.copies.card_large,
                    card_images_medium:   image.copies.card_medium,
                    card_images_small:    image.copies.card_small,
                    card_images_thumb:    image.copies.card_thumb
                };
                copies.card_images_original.key = copies.card_images_original.key.replace( "card-", "card_images-" );
                copies.card_images_large.key    = copies.card_images_large.key.replace( "card-", "card_images-" );
                copies.card_images_medium.key   = copies.card_images_medium.key.replace( "card-", "card_images-" );
                copies.card_images_small.key    = copies.card_images_small.key.replace( "card-", "card_images-" );
                copies.card_images_thumb.key    = copies.card_images_thumb.key.replace( "card-", "card_images-" );
            } else {
                console.error( 'Image has no copies, removing', image );
                CardImagesCollection.files.direct.remove( { _id: image._id } );
            }

            const owners = image.owners.map( owner => {
                return {
                    id:          owner,
                    versionFrom: 0
                }
            } );

            let modifier = { pending: false, owners };
            if ( copies ) {
                modifier.copies = copies;
            }

            CardImagesCollection.files.direct.update( { _id: image._id }, { $set: modifier } );
        } );

        CardAttachmentsCollection.find( {} ).forEach( file => {

            let copies;
            if ( file.copies ) {
                copies                      = {
                    card_attachments: file.copies.card_files
                };
                copies.card_attachments.key = copies.card_attachments.key.replace( "card_files-", "card_attachments-" );
            } else {
                console.error( 'File has no copies, removing', file );
                CardAttachmentsCollection.files.direct.remove( { _id: file._id } );
            }

            const owners = file.owners.map( owner => {
                return {
                    id:          owner,
                    versionFrom: 0
                }
            } );

            let modifier = { pending: false, owners };
            if ( copies ) {
                modifier.copies = copies;
            }

            CardAttachmentsCollection.files.direct.update( { _id: file._id }, { $set: modifier } );
        } );

        FeedCollection.find( {}, { fields: { _id: 1, objects: 1 } } ).forEach( story => {
            FeedCollection.update( { _id: story._id }, {
                $set: {
                    archived: false,
                    objects:  story.objects.map( object => {
                        object.archived = false;
                        return object;
                    } )
                }
            }, {
                bypassCollection2: true
            } );
        } );

        // Adjust avatar urls
        UserCollection.find( {}, { fields: { _id: 1, 'profile.avatarUrl': 1 } } ).forEach( user => {
            if ( user.profile.avatarUrl ) {
                UserCollection.update( { _id: user._id }, { $set: { 'profile.avatarUrl': user.profile.avatarUrl.replace( "/cfs", "" ) } } )
            }
        } );

    },
    down() {
        CardCollection.direct.update( { version: { $exists: true } }, { $unset: { version: null } }, {
            multi:             true,
            bypassCollection2: true
        } );

        CardCollection.find( {}, { fields: { _id: 1, phaseVisits: 1 } } ).forEach( card => {
            if ( card.phaseVisits ) {
                const newPhaseVisits = card.phaseVisits.map( ( phaseVisit, index ) => {
                                               // Assume they all just entered with their current version
                                               phaseVisit.date = phaseVisit.enteredAt || new Date();
                                               delete phaseVisit.enteredVersion;
                                               delete phaseVisit.enteredAt;
                                               delete phaseVisit.exitedVersion;
                                               delete phaseVisit.exitedAt;
                                               delete phaseVisit.enteredFrom;
                                               delete phaseVisit.exitedTo;
                                               return phaseVisit;
                                           } )
                                           // Sort in chronological order
                                           .sort( ( a, b ) => b.date - a.date );
                // Update the phase visits
                CardCollection.direct.update( { _id: card._id }, { $set: { phaseVisits: newPhaseVisits } }, { bypassCollection2: true } );
            }
        } );

        //CardImagesCollection.files.rawDatabase().collection( 'cfs.card_images.filerecord' ).rename( "cfs.card.filerecord", {dropTarget: true} );
        CardImagesCollection.find( {} ).forEach( image => {
            let remainingOwners = [];
            image.owners.forEach( owner => {
                if ( owner.versionTo == undefined ) {
                    remainingOwners.push( owner.id );
                }
            } );
            if ( remainingOwners.length > 0 ) {

                let copies;
                if ( image.copies ) {
                    copies                   = {
                        card_original: image.copies.card_images_original,
                        card_large:    image.copies.card_images_large,
                        card_medium:   image.copies.card_images_medium,
                        card_small:    image.copies.card_images_small,
                        card_thumb:    image.copies.card_images_thumb
                    };
                    copies.card_original.key = copies.card_original.key.replace( "card_images-", "card-" );
                    copies.card_large.key    = copies.card_large.key.replace( "card_images-", "card-" );
                    copies.card_medium.key   = copies.card_medium.key.replace( "card_images-", "card-" );
                    copies.card_small.key    = copies.card_small.key.replace( "card_images-", "card-" );
                    copies.card_thumb.key    = copies.card_thumb.key.replace( "card_images-", "card-" );
                } else {
                    console.error( 'Image has no copies', image );
                }

                let modifier = { owners: remainingOwners };
                if ( copies ) {
                    modifier.copies = copies;
                }

                CardImagesCollection.files.direct.update( { _id: image._id }, { $unset: { pending: null }, $set: modifier } );
            } else {
                CardImagesCollection.files.direct.remove( { _id: image._id } );
            }
        } );

        CardAttachmentsCollection.find( {} ).forEach( file => {
            let remainingOwners = [];
            file.owners.forEach( owner => {
                if ( owner.versionTo == undefined ) {
                    remainingOwners.push( owner.id );
                }
            } );
            if ( remainingOwners.length > 0 ) {
                let copies;
                if ( file.copies ) {
                    copies                = {
                        card_files: file.copies.card_attachments
                    };
                    copies.card_files.key = copies.card_files.key.replace( "card_attachments-", "card_files-" );
                } else {
                    console.error( 'File has no copies', file );
                }

                let modifier = { owners: remainingOwners };
                if ( copies ) {
                    modifier.copies = copies;
                }

                CardAttachmentsCollection.files.direct.update( { _id: file._id }, { $unset: { pending: null }, $set: modifier } );
            } else {
                CardAttachmentsCollection.files.direct.remove( { _id: file._id } );
            }
        } );

        FeedCollection.remove( { archived: true } );
        FeedCollection.find( {}, { fields: { _id: 1, objects: 1 } } ).forEach( story => {
            FeedCollection.update( { _id: story._id }, {
                $unset: { archived: null },
                $set:   {
                    objects: story.objects.map( object => {
                        delete object.archived;
                        return object;
                    } )
                }
            }, {
                bypassCollection2: true
            } );
        } );

        // Adjust avatar urls
        UserCollection.find( {}, { fields: { _id: 1, 'profile.avatarUrl': 1 } } ).forEach( user => {
            if ( user.profile.avatarUrl ) {
                UserCollection.update( { _id: user._id }, { $set: { 'profile.avatarUrl': "/cfs" + user.profile.avatarUrl } } )
            }
        } );
    }
} );
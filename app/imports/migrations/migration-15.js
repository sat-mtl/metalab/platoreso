"use strict";

import { Migrations } from "meteor/percolate:migrations";
import TweetCollection from "../twitter/TweetCollection";
import UserCollection from "../accounts/UserCollection";

Migrations.add( {
    version: 15,
    name:    'Update tweets hashtags to support indexing + add user recentProject',
    up() {
        // Tweets
        TweetCollection.find( {}, { fields: { _id: 1, entities: 1 }, transform: null } ).forEach( tweet => {
            const hashtags = tweet.entities.hashtags.map( hashtag => hashtag.text.toLowerCase() );
            TweetCollection.direct.update( { _id: tweet._id }, { $set: { hashtags: hashtags }, $unset: { entities: null } }, { bypassCollection2: true } );
        } );

        // Users
        UserCollection.direct.update( {}, { $set: { recentProjects: [] } }, { multi: true, bypassCollection2: true } );
    },
    down() {
        // Tweets, not vital, only tweets

        // Users
        UserCollection.direct.update( {}, { $unset: { recentProjects: null } }, { multi: true, bypassCollection2: true } );
    }
} );
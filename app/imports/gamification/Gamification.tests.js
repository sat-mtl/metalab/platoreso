"use strict";

import { chai, assert, expect } from 'meteor/practicalmeteor:chai';
import { sinon, spies, stubs } from 'meteor/practicalmeteor:sinon';

import Gamification from "./Gamification";
import UserCollection from "../accounts/UserCollection";

describe( 'gamification/Gamification', () => {

    let sandbox;

    beforeEach( () => {
        sandbox = sinon.sandbox.create();
        sandbox.stub( UserCollection, 'update' );
    } );

    afterEach( () => {
        sandbox.restore();
    } );

    it( 'should award points', () => {
        Gamification.awardPoints( 'userId', 10 );
        expect( UserCollection.update.calledWith( { _id: 'userId' }, { $inc: { points: 10 } } ) );
    } );

    it( 'should award points to a project', () => {
        Gamification.awardPoints( 'userId', 10, 'projectId' );
        expect( UserCollection.update.calledWith( { _id: 'userId' }, { $inc: { points: 10, [`pointsPerProject.projectId`]: 10 } } ) );
    } );

    it( 'should skip update if giving 0 points', () => {
        Gamification.awardPoints( 'userId', 0 );
        expect( UserCollection.update.called ).to.be.false;
    } );

    it( 'should throw if missing userId', () => {
        expect( () => Gamification.awardPoints( null, 0 ) ).to.throw(/missing userId/);
        expect( UserCollection.update.called ).to.be.false;
    } );

    it( 'should throw if points of bad type', () => {
        expect( () => Gamification.awardPoints( 'userId', "points" ) ).to.throw(/invalid points type/);
        expect( UserCollection.update.called ).to.be.false;
    } );

    it( 'should throw if points is NaN', () => {
        expect( () => Gamification.awardPoints( 'userId', NaN ) ).to.throw(/points not a number/);
        expect( UserCollection.update.called ).to.be.false;
    } );

} );
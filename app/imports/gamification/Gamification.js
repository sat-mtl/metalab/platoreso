"use strict";

import Logger from "meteor/metalab:logger/Logger";
import UserCollection from "../accounts/UserCollection";

const log = Logger( "gamification" );

export default class Gamification {

    /**
     * Give points to the specified userId
     *
     * @param {String} userId
     * @param {Number} points
     * @param {String} [projectId]
     */
    static awardPoints( userId, points, projectId ) {
        if ( !userId ) {
            throw new Meteor.Error('missing userId');
        }

        if ( typeof(points) != 'number' ) {
            throw new Meteor.Error('invalid points type');
        }

        if ( isNaN(points) ) {
            throw new Meteor.Error('points not a number');
        }

        if ( points == 0 ) {
            return;
        }

        log.debug( `Awarding ${points} reputation points to user ${userId} in project ${projectId}`);

        // User points
        const modifier = { $inc: { points: points } };

        if ( projectId ) {
            modifier.$inc[`pointsPerProject.${projectId}`] = points;
        }

        UserCollection.update( { _id: userId }, modifier );
    }

}
"use strict";

import TweetSchema from "./model/schema/TweetSchema";
import Tweet from "./model/Tweet";

class TweetCollection extends Mongo.Collection {
    constructor( name, options = {} ) {
        // Change transform before calling super, this is the only way to set it
        // Also, this won't suffer from module loading as it is a callback
        options.transform = tweet => new Tweet(tweet);

        super(name, options);

        /**
         * Schema
         */

        this.attachSchema(TweetSchema);

        /**
         * Indexes
         */

        if ( Meteor.isServer ) {
            // Hashtags
            this._ensureIndex( { hashtags: 1, timestamp_ms: -1 }, { name: 'hashtag_timestamp' } );
        }

        /**
         * Security
         */

        this.deny({
            insert: () => true,
            update: () => true,
            remove: () => true
        });
    }
}

export default new TweetCollection('tweets');
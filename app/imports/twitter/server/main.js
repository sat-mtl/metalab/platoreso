"use strict";

import logger from "meteor/metalab:logger/Logger";
import "./TweetsPublication";
import TwitterStream from "./TwitterStream";
import ProjectCollection from "../../projects/ProjectCollection";
const log = logger( "Twitter" );

// Implied

// Startup

Meteor.startup( () => {

    // Configuration
    const twitterSettings = Meteor.settings.twitter;
    if ( !twitterSettings ) {
        log.warn( "Twitter API not configured." );
        return;
    }

    const stream = new TwitterStream( Meteor.settings.twitter );

    function updateHashtags() {
        const hashtags = _.uniq( ProjectCollection.find({ hashtag: { $exists: true, $ne: '' } } ).map( project => project.hashtag ) );
        stream.watchHashtags( hashtags );
    }

    // Get hashtags from projects
    ProjectCollection.find( { hashtag: { $ne: null } }, { fields: { hashtag: 1 } } ).observeChanges( {
        added( id, fields ) {
            log.verbose( `Adding hashtag ${fields.hashtag} for project ${id}` );
            updateHashtags();
        },
        changed( id, fields ) {
            log.verbose( `Changing hashtag ${fields.hashtag} for project ${id}` );
            updateHashtags();
        },
        removed( id, fields ) {
            log.verbose( `Removing hashtag for project ${id}` );
            updateHashtags();
        }
    } );
} );
"use strict";

import moment from "moment";
import Twit from "twit";
import he from "he"

import logger from "meteor/metalab:logger/Logger";
import ProjectCollection from "../../projects/ProjectCollection";
import TweetCollection from "../TweetCollection";

const log = logger("Twitter");
const TWEETS_TTL    = 7 * 24 * 60 * 60 * 1000; // 1 week

export default class TwitterStream {

    constructor(config) {
        log.info( "Configuring Twitter stream" );

        this.hashtags = [];

        // Twitter API
        this.twitter = new Twit( {
            consumer_key:        config.consumerKey,
            consumer_secret:     config.consumerSecret,
            access_token:        config.accessToken,
            access_token_secret: config.accessTokenSecret
        } );

        // Cleanup cron
        SyncedCron.add( {
            name:     'Prune old tweets',
            schedule: function ( parser ) {
                return parser.text( 'every 1 hour' );
            },
            job:      Meteor.bindEnvironment( () => {
                const oldest = (moment().unix() * 1000) - TWEETS_TTL;
                TweetCollection.remove( {timestamp_ms: {$lt: oldest}} );
            } )
        } );

        if ( !SyncedCron.running ) {
            SyncedCron.start();
        }
    }

    /**
     * Request an update to the stream.
     * Delays by 1 second so that multiple updates in a row don't try to reconnect all at once.
     */
    _requestUpdateStream() {
        log.verbose("Requesting a stream update");

        if ( this.timeout ) {
            Meteor.clearTimeout( this.timeout );
        }
        this.timeout = Meteor.setTimeout( this._updateStream.bind(this), 1000 );
    }

    /**
     * Update the twitter stream subscription
     */
    _updateStream() {
        log.verbose("Updating stream");

        // Stop previous stream, if any
        if ( this.stream ) {
            this.stream.stop();
        }

        if ( this.hashtags.length == 0 ) {
            return;
        }

        log.debug( 'Watching hashtags', this.hashtags );

        // Setup stream
        this.stream = this.twitter.stream(
            'statuses/filter',
            {
                track: this.hashtags.map( hashtag => '#' + hashtag ).join( ',' )
            }
        );

        this.stream.on( 'tweet', Meteor.bindEnvironment( tweet => {
            // Skip retweets
            if ( tweet.retweet || tweet.retweeted_status ) {
                return;
            }

            log.silly( '[Tweet]', tweet.text );

            // Decode html entities
            tweet.text = he.decode( tweet.text );

            // Keep timestamp as int
            tweet.timestamp_ms = parseInt( tweet.timestamp_ms );

            // Keep indexed lowercase hashtags
            tweet.hashtags = tweet.entities.hashtags.map( hashtag => hashtag.text.toLowerCase() );

            try {
                TweetCollection.insert( tweet );
            } catch ( e ) {
                // We don't really care if a tweet fails to insert...
                log.warn( e );
            }

        } ) );

        this.stream.on( 'error', error => {
            log.error( "[Twitter]", error.message );
        } );

        this.stream.on( 'warning', warning => {
            log.warn( "[Twitter]", warning );
        } );

        this.stream.on( 'disconnect', disconnectMessage => {
            log.warn( "[Twitter]", disconnectMessage );
        } );
    }

    /**
     * Set the list of hashtags to watch for
     *
     * @param {Array} hashtags
     */
    watchHashtags(hashtags) {
        this.hashtags = hashtags;
        this._requestUpdateStream();
    }

}
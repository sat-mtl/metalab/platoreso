"use strict";

import logger from "meteor/metalab:logger/Logger";
import TweetCollection from "../TweetCollection";

const log = logger("Twitter");

log.verbose( "Setting up tweets publication" );

/**
 * Tweet List (per hashtag)
 */
Meteor.publish( "tweets", function ( hashtag ) {
    check( hashtag, String );

    log.silly( "[Publication] [tweets]", hashtag );

    return TweetCollection.find( {
        hashtags: hashtag.toLowerCase()
    },
    {
        limit: 100,
        sort: {
            timestamp_ms: -1
        }
    } );
} );
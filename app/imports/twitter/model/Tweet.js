"use strict";

import linkifyIt from "linkify-it";

const linkify = linkifyIt();
linkify.add( '@', {
    validate:  function ( text, pos, self ) {
        var tail = text.slice( pos );

        if ( !self.re.twitter ) {
            self.re.twitter = new RegExp(
                '^([a-zA-Z0-9_]){1,15}(?!_)(?=$|' + self.re.src_ZPCc + ')'
            );
        }
        if ( self.re.twitter.test( tail ) ) {
            // Linkifier allows punctuation chars before prefix,
            // but we additionally disable `@` ("@@mention" is invalid)
            if ( pos >= 2 && tail[pos - 2] === '@' ) {
                return false;
            }
            return tail.match( self.re.twitter )[0].length;
        }
        return 0;
    },
    normalize: function ( match ) {
        match.url = 'https://twitter.com/' + match.url.replace( /^@/, '' );
    }
} );

export default class Tweet {

    constructor( doc ) {
        _.extend( this, doc );
    }

    /**
     * Get the tweet with urls autolinked
     *
     * @returns {String}
     */
    get htmlText() {
        if ( !this._htmlText ) {
            const matches = linkify.match( this.text );
            if ( matches ) {
                let last = 0;
                let html = "";
                matches.forEach( match => {
                    if ( last < match.index ) {
                        html += this.text.slice( last, match.index ).replace( /\r?\n/g, '<br>' );
                    }
                    html += `<a target="_blank" href="${match.url}"">${match.text}</a>`;
                    last = match.lastIndex;
                } );
                if ( last < this.text.length ) {
                    html += this.text.slice( last ).replace( /\r?\n/g, '<br>' );
                }
                this._htmlText = html;
            } else {
                this._htmlText = this.text;
            }
        }
        return this._htmlText;
    }
}
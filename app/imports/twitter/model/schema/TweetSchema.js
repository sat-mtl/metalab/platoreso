"use strict";

import { SimpleSchema } from "meteor/aldeed:simple-schema";

export const TwitterUserSchema = new SimpleSchema({
    id_str: {
        type: String
    },
    screen_name: {
        type: String
    },
    name: {
        type: String,
        optional: true
    },
    location: {
        type: String,
        optional: true
    },
    url: {
        type: String,
        optional: true
    },
    description: {
        type: String,
        optional: true
    },
    profile_image_url: {
        type: String,
        optional: true
    }
    //TODO: Other fields if needed
});

export const TweetSchema = new SimpleSchema({
    id_str: {
        type: String
    },
    text: {
        type: String
    },
    user: {
        type: TwitterUserSchema
    },
    timestamp_ms: {
        type: Number
    },
    hashtags: {
        type: [String]
    },
    /*entities: {
        type: Object,
        optional: true,
        blackbox: true
    }*/
    //TODO: Other fields if needed
});

export default TweetSchema;
"use strict";

import Logger from "meteor/metalab:logger/Logger";
import { ValidatedMethod } from "meteor/mdg:validated-method";
import CollectionHelpers from "/imports/collections/CollectionHelpers";

const log = Logger( "moderation-methods" );

const ModerationMethods = {

    /**
     * Report an entity
     **
     * @param entityTypeName
     * @param entityId
     */
    report: new ValidatedMethod( {
        name:     "pr/moderation/report",
        validate: new SimpleSchema( {
            entityTypeName: {
                type: String
            },
            entityId:       {
                type: String
            }
        } ).validator( { clean: true } ),
        run( { entityTypeName, entityId } ) {

            const collection = CollectionHelpers.get( entityTypeName );
            if ( !collection ) {
                throw new Meteor.Error( 'wrong entity type' );
            }

            const entity = collection.findOne(
                collection.moderationEntityQuery( entityId ),
                {
                    fields: {
                        moderated:   1,
                        reportCount: 1,
                        reportedBy:  1
                    }
                }
            );
            if ( !entity ) {
                return;
            }

            let modifier          = {};
            const alreadyReported = entity.reportedBy && entity.reportedBy.indexOf( this.userId ) != -1;
            if ( alreadyReported ) {
                modifier.$pull = { reportedBy: this.userId };
            } else {
                modifier.$addToSet = { reportedBy: this.userId };
            }

            // Check the security with only the "reportedBy" being modified, user can't modify the report count so we add it later
            Security.can( this.userId ).update( entity._id, modifier ).for( collection ).throw();

            // Clone modifier, this is only useful for the unit tests, so that it is not the same
            // reference as the one used for the call to Security...
            modifier = _.clone( modifier );

            // Update report count
            const reportCount = entity.reportedBy ? entity.reportedBy.length : 0;
            modifier.$set     = { reportCount: reportCount + ( alreadyReported ? -1 : 1 ) };

            collection.update( { _id: entity._id }, modifier );
        }
    } ),

    /**
     * Approve an entity
     *
     * @param entityTypeName
     * @param entityId
     */
    approve: new ValidatedMethod( {
        name:     "pr/moderation/approve",
        validate: new SimpleSchema( {
            entityTypeName: {
                type: String
            },
            entityId:       {
                type: String
            }
        } ).validator( { clean: true } ),
        run( { entityTypeName, entityId } ) {

            const collection = CollectionHelpers.get( entityTypeName );
            if ( !collection ) {
                throw new Meteor.Error( 'wrong entity type' );
            }

            const entity = collection.findOne(
                collection.moderationEntityQuery( entityId ),
                {
                    fields: {
                        _id: 1
                    }
                }
            );
            if ( !entity ) {
                return;
            }

            const modifier = { $set: { moderated: false, reportCount: 0, reportedBy: [] } };

            Security.can( this.userId ).update( entity._id, modifier ).for( collection ).throw();

            collection.update( { _id: entity._id }, modifier );
        }
    } ),

    /**
     * Moderate an entity
     *
     * @param entityTypeName
     * @param entityId
     */
    moderate: new ValidatedMethod( {
        name:     "pr/moderation/moderate",
        validate: new SimpleSchema( {
            entityTypeName: {
                type: String
            },
            entityId:       {
                type: String
            }
        } ).validator( { clean: true } ),
        run( { entityTypeName, entityId } ) {

            const collection = CollectionHelpers.get( entityTypeName );
            if ( !collection ) {
                throw new Meteor.Error( 'wrong entity type' );
            }

            const entity = collection.findOne(
                collection.moderationEntityQuery( entityId ),
                {
                    fields: {
                        _id: 1
                    }
                }
            );
            if ( !entity ) {
                return;
            }

            let modifier = { $set: { moderated: true } };
            Security.can( this.userId ).update( entity._id, modifier ).for( collection ).throw();
            collection.update( { _id: entity._id }, modifier );
        }
    } )
};

export default ModerationMethods
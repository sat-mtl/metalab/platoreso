"use strict";

import ModerationSchema from "./model/ModerationSchema";

export default class ModerationHelpers {

    /**
     * Setup moderation
     *
     * @param {Object} collection
     * @param {Constructor} [model]
     * @param {Function} [entityQuery] Function used to get the query for the entity
     */
    static setup( collection, model = null, entityQuery = null ) {
        // Attach the moderation schema to the entity's collection
        collection.attachSchema( ModerationSchema );

        // Augment the model
        if ( model ) {

            /**
             * Method on the model to check if the entity was reported by the passed user.
             * Will only ever work when checking for the current user as it is the only reporter
             * we publish for privacy reasons.
             *
             * @param userId
             * @returns {Boolean}
             */
            model.prototype.isReportedBy = function ( userId ) {
                return !!this.reportedBy && this.reportedBy.indexOf( userId ) != -1;
            };
        }

        collection.moderationEntityQuery = entityQuery || (entityId => ({ _id: entityId }));

        // Attach a helper method to the entity's collection find reported content
        collection.findReported = ( query, options ) => {
            if ( !query ) {
                query = {};
            }

            // First combine any $or that might occur in query with our inside an $and if necessary
            /*if ( query.$or ) {
             query.$and = [{$or: [{moderated: false}, {moderated: {$exists: false}}]}, {$or: query.$or}];
             delete query.$or;
             } else {
             query.$or = [{moderated: false}, {moderated: {$exists: false}}]
             }*/

            // Then extend the query with our defaults
            if ( !query.moderated ) {
                query = _.extend( {
                    reportCount: { $gt: 0 }
                }, query );
            }

            return collection.find( query, options );
        };

        if ( Meteor.isServer ) {
            // Continue setup on the server
            const ModerationHelpersServer = require( "/imports/moderation/server/ModerationHelpers" ).default;
            ModerationHelpersServer.setup( collection, model );
        }
    }

}
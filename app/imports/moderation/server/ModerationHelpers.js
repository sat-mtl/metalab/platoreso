"use strict";

import Logger from "meteor/metalab:logger/Logger";
import { Security } from "meteor/ongoworks:security";
import AccountsMeldHelper from "/imports/accounts/server/AccountsMeldHelper";

const log = Logger( "moderation-helper" );

/**
 * Checks that you can only report as yourself and that it is either
 * a report done with an $addToSet or
 * a cancelled report done with a $pull
 * this way the report-related fields can only be edited correctly.
 */
Security.defineMethod( "canOnlyReportAsYourself", {
    fetch:     [],
    transform: null,
    deny:      function ( type, arg, userId, doc, fields, modifier ) {
        const reported   = ( modifier.$addToSet && modifier.$addToSet.reportedBy && modifier.$addToSet.reportedBy == userId );
        const unreported = ( modifier.$pull && modifier.$pull.reportedBy && modifier.$pull.reportedBy == userId );
        return reported == unreported; // reported XOR unreported, you can only have one true, otherwise, deny
    }
} );

export default class ModerationHelpersServer {

    /**
     * Attach the moderation security to the collection
     *
     * @param collection
     * @private
     */
    static _attachSecurity( collection ) {
        collection.permit( ['update'] )
                  .onlyProps( ['reportedBy'] )
                  .ifLoggedIn()
                  .canOnlyReportAsYourself();
    }

    /**
     * Setup moderation
     *
     * @param {Object} collection
     * @param {Constructor} [model]
     */
    static setup( collection, model = null ) {
        // Add reported to default publication fields
        collection.defaultFields.push( 'moderated' );
        // Add own report to default publication fields
        collection.defaultFields.push( userId => ({ reportedBy: { $elemMatch: { $eq: userId } } }) );

        // Security for reporting
        ModerationHelpersServer._attachSecurity( collection );

        // Account Melding
        AccountsMeldHelper.onMeld( ( src_user_id, dst_user_id ) => {
            log.debug( `Melding moderations for user ${src_user_id} into ${dst_user_id}` );
            collection.update( { 'reportedBy': src_user_id }, { $set: { 'reportedBy.$': dst_user_id } }, { multi: true } );
        } );
    }

}
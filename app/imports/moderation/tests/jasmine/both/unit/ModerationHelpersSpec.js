"use strict";

import CollectionHelpers from "/imports/collections/CollectionHelpers";
import ModerationHelpers from "/imports/moderation/ModerationHelpers";
import ModerationHelpersServer from "/imports/moderation/service/ModerationHelpers";
import ModerationSchema from "/imports/moderation/model/ModerationSchema";

describe( 'pr-moderation/both/unit/ModerationHelpers', function () {

    it( 'helper should exist', function () {
        expect( ModerationHelpers ).toBeDefined();
    } );

    describe( 'setup', function () {

        let registration;

        beforeEach( function () {
            registration = {
                collection: {
                    attachSchema: jasmine.createSpy( 'attachSchema' ),
                    find:         jasmine.createSpy( 'find' ).and.returnValue( 'find cursor' )
                },
                model:      { prototype: {} }
            };
            spyOn( CollectionHelpers, 'get' ).and.returnValue( registration );
            if ( Meteor.isServer ) {
                spyOn( ModerationHelpersServer, 'setup' );
            }
        } );

        it( 'should setup a collection for moderation', function () {
            expect( () => ModerationHelpers.setup( 'entity' ) ).not.toThrow();
            expect( CollectionHelpers.get ).toHaveBeenCalledWith( 'entity' );
            expect( registration.collection.attachSchema ).toHaveBeenCalledWith( ModerationSchema );
            expect( registration.model.prototype.isReportedBy ).toBeDefined();
            expect( registration.collection.findReported ).toBeDefined();
            if ( Meteor.isServer ) {
                expect( ModerationHelpersServer.setup ).toHaveBeenCalledWith( 'entity' );
            }
        } );

        it( 'should fail if collection is not found', function () {
            CollectionHelpers.get.and.returnValue( null );
            expect( () => ModerationHelpers.setup( 'not found' ) ).toThrowError( /collection not found/ );
            expect( CollectionHelpers.get ).toHaveBeenCalledWith( 'not found' );
            if ( Meteor.isServer ) {
                expect( ModerationHelpersServer.setup ).not.toHaveBeenCalled();
            }
        } );

        it( 'should add a working \'isReportedBy\' to the model', function () {
            expect( () => ModerationHelpers.setup( 'entity' ) ).not.toThrow();
            expect( registration.model.prototype.isReportedBy.call( { reportedBy: ['reporterId'] }, 'notReporterId' ) ).toBe( false );
            expect( registration.model.prototype.isReportedBy.call( { reportedBy: ['reporterId'] }, 'reporterId' ) ).toBe( true );
        } );

        it( 'should add a working \'findReported\' to the collection (with query)', function () {
            expect( () => ModerationHelpers.setup( 'entity' ) ).not.toThrow();
            expect( registration.collection.findReported( { name: 'something' }, { sort: { name: 1 } } ) ).toEqual( 'find cursor' );
            expect( registration.collection.find ).toHaveBeenCalledWith( {
                name:        'something',
                $or:         [{ moderated: false }, { moderated: { $exists: false } }],
                reportCount: { $gt: 0 }
            }, { sort: { name: 1 } } );
        } );

        it( 'should add a working \'findReported\' to the collection (without query)', function () {
            expect( () => ModerationHelpers.setup( 'entity' ) ).not.toThrow();
            expect( registration.collection.findReported() ).toEqual( 'find cursor' );
            expect( registration.collection.find ).toHaveBeenCalledWith( {
                $or:         [{ moderated: false }, { moderated: { $exists: false } }],
                reportCount: { $gt: 0 }
            }, undefined );
        } );

        it( 'should combine additional $or query arguments in \'findReported\'', function () {
            expect( () => ModerationHelpers.setup( 'entity' ) ).not.toThrow();
            expect( registration.collection.findReported( { name: 'something', $or: [{ something: 'or-ed' }] }, { sort: { name: 1 } } ) ).toEqual( 'find cursor' );
            expect( registration.collection.find ).toHaveBeenCalledWith( {
                name:        'something',
                $and:        [{ $or: [{ moderated: false }, { moderated: { $exists: false } }] }, { $or: [{ something: 'or-ed' }] }],
                reportCount: { $gt: 0 }
            }, { sort: { name: 1 } } );
        } );
    } );

} );
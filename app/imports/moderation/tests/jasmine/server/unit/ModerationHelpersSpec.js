"use strict";

import CollectionHelpers from "/imports/collections/CollectionHelpers";
const { ModerationHelpersServer } = PR.Moderation;

describe( 'pr-moderation/server/unit/ModerationHelpers', function () {

    it( 'helper should exist', function () {
        expect( PR.Moderation.ModerationHelpersServer ).toBeDefined();
    } );

    describe( 'setup', function () {

        let registration;

        beforeEach( function () {
            registration = {
                collection: { name: 'some collection' },
                defaultFields: {},
                defaultFieldsGetters: { push: jasmine.createSpy('push')}
            };
            spyOn( CollectionHelpers, 'get' ).and.returnValue( registration );
            spyOn( ModerationHelpersServer, '_attachSecurity');
        } );

        it( 'should setup a collection for moderation', function () {
            expect( () => ModerationHelpersServer.setup( 'entity' ) ).not.toThrow();
            expect( CollectionHelpers.get ).toHaveBeenCalledWith( 'entity' );
            expect( registration.defaultFields ).toEqual({ moderated: 1 });
            expect( registration.defaultFieldsGetters.push ).toHaveBeenCalledWith( ModerationHelpersServer._getModerationDefaultFieldsForUser );
            expect( ModerationHelpersServer._attachSecurity ).toHaveBeenCalledWith( registration.collection );
        } );

    } );

} );
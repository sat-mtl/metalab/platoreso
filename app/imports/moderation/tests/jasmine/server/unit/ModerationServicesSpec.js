"use strict";

import CollectionHelpers from "/imports/collections/CollectionHelpers";
const { ModerationServicesServer } = PR.Moderation;

describe( 'pr-moderation/server/unit/ModerationServices', function () {

    it( 'services should exist', function () {
        expect( PR.Moderation.ModerationServicesServer ).toBeDefined();
    } );

    describe( 'report', function () {

        let context;
        let entity;
        let registration;
        let security_update;
        let security_for;
        let security_throw;

        beforeEach( function () {
            context      = {
                userId: '420'
            };
            entity       = {};
            registration = {
                collection: {
                    findOne: jasmine.createSpy( 'findOne' ).and.returnValue( entity ),
                    update:  jasmine.createSpy( 'update' )
                }
            };
            spyOn( CollectionHelpers, 'get' ).and.returnValue( registration );

            security_throw  = jasmine.createSpy( 'throw' );
            security_for    = jasmine.createSpy( 'for' ).and.returnValue( { throw: security_throw } );
            security_update = jasmine.createSpy( 'update' ).and.returnValue( { for: security_for } );
            spyOn( Security, 'can' ).and.returnValue( { update: security_update } );
        } );

        it( 'should report an entity for moderation', function () {
            const security_modifier = { $addToSet: { reportedBy: context.userId } };
            const modifier          = { $addToSet: { reportedBy: context.userId }, $set: { reportCount: 1 } };
            expect( ()=>ModerationServicesServer.report.call( context, 'entityName', 'entityId' ) ).not.toThrow();
            expect( CollectionHelpers.get ).toHaveBeenCalledWith( 'entityName' );
            expect( registration.collection.findOne ).toHaveBeenCalledWith( { _id: 'entityId' }, {
                fields: {
                    moderated:   1,
                    reportCount: 1,
                    reportedBy:  1
                }
            } );
            expect( Security.can ).toHaveBeenCalledWith( context.userId );
            expect( security_update ).toHaveBeenCalledWith( entity, security_modifier );
            expect( security_for ).toHaveBeenCalledWith( registration.collection );
            expect( security_throw ).toHaveBeenCalled();
            expect( registration.collection.update ).toHaveBeenCalledWith( { _id: entity._id }, modifier );
        } );

        it( 'should unreport an entity for moderation', function () {
            entity.reportedBy       = [context.userId];
            const security_modifier = { $pull: { reportedBy: context.userId } };
            const modifier          = { $pull: { reportedBy: context.userId }, $set: { reportCount: 0 } };
            expect( ()=>ModerationServicesServer.report.call( context, 'entityName', 'entityId' ) ).not.toThrow();
            expect( CollectionHelpers.get ).toHaveBeenCalledWith( 'entityName' );
            expect( registration.collection.findOne ).toHaveBeenCalledWith( { _id: 'entityId' }, {
                fields: {
                    moderated:   1,
                    reportCount: 1,
                    reportedBy:  1
                }
            } );
            expect( Security.can ).toHaveBeenCalledWith( context.userId );
            expect( security_update ).toHaveBeenCalledWith( entity, security_modifier );
            expect( security_for ).toHaveBeenCalledWith( registration.collection );
            expect( security_throw ).toHaveBeenCalled();
            expect( registration.collection.update ).toHaveBeenCalledWith( { _id: entity._id }, modifier );
        } );

        it( 'should increment reportCount', function () {
            entity.reportedBy = ['666'];
            const modifier    = { $addToSet: { reportedBy: context.userId }, $set: { reportCount: 2 } };
            expect( ()=>ModerationServicesServer.report.call( context, 'entityName', 'entityId' ) ).not.toThrow();
            expect( registration.collection.update ).toHaveBeenCalledWith( { _id: entity._id }, modifier );
        } );

        it( 'should decrement reportCount', function () {
            entity.reportedBy = [context.userId, '666'];
            const modifier    = { $pull: { reportedBy: context.userId }, $set: { reportCount: 1 } };
            expect( ()=>ModerationServicesServer.report.call( context, 'entityName', 'entityId' ) ).not.toThrow();
            expect( CollectionHelpers.get ).toHaveBeenCalledWith( 'entityName' );
            expect( registration.collection.update ).toHaveBeenCalledWith( { _id: entity._id }, modifier );
        } );

        it( 'should silently fail if entity is not found', function () {
            registration.collection.findOne.and.returnValue( null );
            expect( ()=>ModerationServicesServer.report.call( context, 'entityName', 'entityId' ) ).not.toThrow();
            expect( CollectionHelpers.get ).toHaveBeenCalledWith( 'entityName' );
            expect( registration.collection.findOne ).toHaveBeenCalledWith( { _id: 'entityId' }, {
                fields: {
                    moderated:   1,
                    reportCount: 1,
                    reportedBy:  1
                }
            } );
            expect( Security.can ).not.toHaveBeenCalled();
            expect( security_update ).not.toHaveBeenCalled();
            expect( security_for ).not.toHaveBeenCalled();
            expect( security_throw ).not.toHaveBeenCalled();
            expect( registration.collection.update ).not.toHaveBeenCalled();
        } );

        it( 'should throw if collection is not found', function () {
            CollectionHelpers.get.and.returnValue( null );
            expect( ()=>ModerationServicesServer.report.call( context, 'entityName', 'entityId' ) ).toThrowError( /wrong entity type/ );
            expect( CollectionHelpers.get ).toHaveBeenCalledWith( 'entityName' );
            expect( registration.collection.findOne ).not.toHaveBeenCalled();
            expect( Security.can ).not.toHaveBeenCalled();
            expect( security_update ).not.toHaveBeenCalled();
            expect( security_for ).not.toHaveBeenCalled();
            expect( security_throw ).not.toHaveBeenCalled();
            expect( registration.collection.update ).not.toHaveBeenCalled();
        } );

        it( 'should throw if not allowed', function () {
            const security_modifier = { $addToSet: { reportedBy: context.userId } };
            security_throw.and.throwError('unauthorized');
            expect( ()=>ModerationServicesServer.report.call( context, 'entityName', 'entityId' ) ).toThrowError( /unauthorized/ );
            expect( CollectionHelpers.get ).toHaveBeenCalledWith( 'entityName' );
            expect( registration.collection.findOne ).toHaveBeenCalledWith( { _id: 'entityId' }, {
                fields: {
                    moderated:   1,
                    reportCount: 1,
                    reportedBy:  1
                }
            } );
            expect( Security.can ).toHaveBeenCalledWith( context.userId );
            expect( security_update ).toHaveBeenCalledWith( entity, security_modifier );
            expect( security_for ).toHaveBeenCalledWith( registration.collection );
            expect( security_throw ).toHaveBeenCalled();
            expect( registration.collection.update ).not.toHaveBeenCalled();
        } );

        function assertCheckFailed() {
            expect( CollectionHelpers.get ).not.toHaveBeenCalled();
            expect( registration.collection.findOne ).not.toHaveBeenCalled();
            expect( Security.can ).not.toHaveBeenCalled();
            expect( security_update ).not.toHaveBeenCalled();
            expect( security_for ).not.toHaveBeenCalled();
            expect( security_throw ).not.toHaveBeenCalled();
            expect( registration.collection.update ).not.toHaveBeenCalled();
        }

        it( 'should throw on invalid entity name parameter', function () {
            expect( ()=>ModerationServicesServer.report.call( context, 123, 'entityId' ) ).toThrowError( /Match error/ );
            assertCheckFailed();
        } );

        it( 'should throw on null entity name parameter', function () {
            expect( ()=>ModerationServicesServer.report.call( context, null, 'entityId' ) ).toThrowError( /Match error/ );
            assertCheckFailed();
        } );

        it( 'should throw on invalid entity id parameter', function () {
            expect( ()=>ModerationServicesServer.report.call( context, 'entityName', 123 ) ).toThrowError( /Match error/ );
            assertCheckFailed();
        } );

        it( 'should throw on null entity id parameter', function () {
            expect( ()=>ModerationServicesServer.report.call( context, 'entityName', null ) ).toThrowError( /Match error/ );
            assertCheckFailed();
        } );

    } );

    describe( 'moderate', function () {

        let context;
        let entity;
        let registration;
        let security_update;
        let security_for;
        let security_throw;

        beforeEach( function () {
            context      = {
                userId: '420'
            };
            entity       = {};
            registration = {
                collection: {
                    findOne: jasmine.createSpy( 'findOne' ).and.returnValue( entity ),
                    update:  jasmine.createSpy( 'update' )
                }
            };
            spyOn( CollectionHelpers, 'get' ).and.returnValue( registration );

            security_throw  = jasmine.createSpy( 'throw' );
            security_for    = jasmine.createSpy( 'for' ).and.returnValue( { throw: security_throw } );
            security_update = jasmine.createSpy( 'update' ).and.returnValue( { for: security_for } );
            spyOn( Security, 'can' ).and.returnValue( { update: security_update } );
        } );

        it( 'should set the entity as moderated', function () {
            const modifier          = { $set: { moderated: true } };
            expect( ()=>ModerationServicesServer.moderate.call( context, 'entityName', 'entityId' ) ).not.toThrow();
            expect( CollectionHelpers.get ).toHaveBeenCalledWith( 'entityName' );
            expect( registration.collection.findOne ).toHaveBeenCalledWith( { _id: 'entityId' }, { fields: { _id: 1 } } );
            expect( Security.can ).toHaveBeenCalledWith( context.userId );
            expect( security_update ).toHaveBeenCalledWith( entity, modifier );
            expect( security_for ).toHaveBeenCalledWith( registration.collection );
            expect( security_throw ).toHaveBeenCalled();
            expect( registration.collection.update ).toHaveBeenCalledWith( { _id: entity._id }, modifier );
        } );

        it( 'should throw if collection not found', function () {
            CollectionHelpers.get.and.returnValue(null);
            expect( ()=>ModerationServicesServer.moderate.call( context, 'entityName', 'entityId' ) ).toThrowError(/wrong entity type/);
            expect( CollectionHelpers.get ).toHaveBeenCalledWith( 'entityName' );
            expect( registration.collection.findOne ).not.toHaveBeenCalled();
            expect( Security.can ).not.toHaveBeenCalled();
            expect( security_update ).not.toHaveBeenCalled();
            expect( security_for ).not.toHaveBeenCalled();
            expect( security_throw ).not.toHaveBeenCalled();
            expect( registration.collection.update ).not.toHaveBeenCalled();
        } );

        it( 'should silently fail if entity is not found', function () {
            registration.collection.findOne.and.returnValue( null );
            expect( ()=>ModerationServicesServer.moderate.call( context, 'entityName', 'entityId' ) ).not.toThrow();
            expect( CollectionHelpers.get ).toHaveBeenCalledWith( 'entityName' );
            expect( registration.collection.findOne ).toHaveBeenCalledWith( { _id: 'entityId' }, { fields: { _id: 1 } } );
            expect( Security.can ).not.toHaveBeenCalled();
            expect( security_update ).not.toHaveBeenCalled();
            expect( security_for ).not.toHaveBeenCalled();
            expect( security_throw ).not.toHaveBeenCalled();
            expect( registration.collection.update ).not.toHaveBeenCalled();
        } );

        function assertCheckFailed() {
            expect( CollectionHelpers.get ).not.toHaveBeenCalled();
            expect( registration.collection.findOne ).not.toHaveBeenCalled();
            expect( Security.can ).not.toHaveBeenCalled();
            expect( security_update ).not.toHaveBeenCalled();
            expect( security_for ).not.toHaveBeenCalled();
            expect( security_throw ).not.toHaveBeenCalled();
            expect( registration.collection.update ).not.toHaveBeenCalled();
        }

        it( 'should throw on invalid entity name parameter', function () {
            expect( ()=>ModerationServicesServer.moderate.call( context, 123, 'entityId' ) ).toThrowError( /Match error/ );
            assertCheckFailed();
        } );

        it( 'should throw on null entity name parameter', function () {
            expect( ()=>ModerationServicesServer.moderate.call( context, null, 'entityId' ) ).toThrowError( /Match error/ );
            assertCheckFailed();
        } );

        it( 'should throw on invalid entity id parameter', function () {
            expect( ()=>ModerationServicesServer.moderate.call( context, 'entityName', 123 ) ).toThrowError( /Match error/ );
            assertCheckFailed();
        } );

        it( 'should throw on null entity id parameter', function () {
            expect( ()=>ModerationServicesServer.moderate.call( context, 'entityName', null ) ).toThrowError( /Match error/ );
            assertCheckFailed();
        } );
    } );

} );
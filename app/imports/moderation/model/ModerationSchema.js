"use strict";

import { SimpleSchema } from "meteor/aldeed:simple-schema";

const ModerationSchema = new SimpleSchema( {
    moderated:   {
        type:         Boolean,
        optional:     true,
        defaultValue: false,
        index: true
    },
    reportCount: {
        type:         Number,
        defaultValue: 0
    },
    reportedBy:  {
        type:         [String],
        defaultValue: []
    }
} );

export default ModerationSchema;
"use strict";

import slugify from "underscore.string/slugify";
import GroupHelpers from "/imports/groups/GroupHelpers";
import GroupSecurity from "/imports/groups/GroupSecurity";
import GroupCollection from "../../groups/GroupCollection";
import ProjectCollection from "../../projects/ProjectCollection";
import CardCollection from "../../cards/CardCollection";
import CommentCollection from "../CommentCollection";

import UserCollection from "/imports/accounts/UserCollection";
import UserHelpers from "/imports/accounts/UserHelpers";
import { RoleGroups } from "/imports/accounts/Roles";

import { Counts } from "meteor/tmeasday:publish-counts";

/**
 * Moderation Report List (Administration)
 *
 * @params {Object} [options] Options for the find operation (ex: sort)
 */
Meteor.publishComposite( "comment/moderation/report/list", function ( query = null, options = null ) {

    // Check query for only the allowed fields
    check( query, Match.OneOf( null, {
        post:      Match.Optional( Match.ObjectIncluding( { $regex: String } ) ),
        moderated: Match.Optional( Boolean )
    } ) );

    // Check options for only the allowed fields
    check( options, Match.OneOf( null, {
        sort:  Match.Optional( Object ),
        limit: Match.Optional( Number ),
        skip:  Match.Optional( Number )
    } ) );

    // Only allow top-level moderators if the groups package isn't available
    const topLevelModerator = UserHelpers.isModerator( this.userId );
    if ( !topLevelModerator && !GroupSecurity.canModerateGroups( this.userId ) ) {
        return this.ready();
    }

    query = query || {};

    // Keep the original (unmodified by permissions, to publish counts to a unique URI)
    const originalQuery = _.clone( query );

    options = _.extend( options || {}, {
        fields: {
            id:          1,
            post:        1,
            author:      1,
            moderated:   1,
            reportCount: 1,
            objects:     1
        }
    } );

    return {
        find() {
            if ( !topLevelModerator ) {
                //TODO: This query has the potential to become *very* ugly after a while (lots of projects)
                //      Should be monitored in order to refactor if it becomes a problem

                // Let's $or whatever the user has access to
                let $or = [];

                // User is not a top-level moderator so filter comments by groups/projects/cards he/she has access to as a group moderator
                // If we are here it is already assumed we have the groups package

                // GROUPS
                const userGroups = GroupHelpers.getGroupsForUser( this.userId, RoleGroups.moderators );
                $or.push( { 'objects.0.type': GroupCollection.entityName, 'objects.0.id': { $in: userGroups } } );

                // PROJECTS
                const projects   = ProjectCollection.find( { groups: { $in: userGroups } }, { fields: { _id: 1 } } );
                const projectIds = projects.map( project => project._id );
                $or.push( {
                    objects: {
                        $elemMatch: {
                            'type': ProjectCollection.entityName,
                            'id':   { $in: projectIds }
                        }
                    }
                } );

                // CARDS
                //const cards = CardCollection.find( { project: { $in: projectIds }, current: true }, { fields: { id: 1 } } );
                //$or.push( { 'objects.1.type': ProjectCollection.entityName, 'objects.1.type': { $in: cards.map( card => card.id ) } } );

                if ( query.$or ) {
                    query.$or = query.$or.concat( $or );
                } else {
                    query.$or = $or;
                }
            }

            // Slugify the json query as a unique identifier for the count
            Counts.publish( this,
                'comment/moderation/report/count/' + slugify( JSON.stringify( originalQuery ) ),
                CommentCollection.findReported( query, { fields: { _id: 1 } } ),
                { noReady: true } // Important otherwise the cursor is considered ready before running the next query
            );

            return CommentCollection.findReported( query, options )
        },
        children: [
            {
                find( comment ) {
                    return UserCollection.find( { _id: comment.author }, {
                        fields: {
                            _id:     1,
                            profile: 1
                        }
                    } );
                }
            }
        ]
    };
} );
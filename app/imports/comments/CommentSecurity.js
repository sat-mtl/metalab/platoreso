"use strict";

import UserHelpers from "/imports/accounts/UserHelpers";
import CommentCollection from "./CommentCollection";

export default class CommentSecurity {

    /**
     * Checks if a comment can be edited
     *
     * @param {String|Object} comment
     * @param {String} user
     * @returns {Boolean}
     */
    static canEdit( comment, user ) {
        //TODO: check() doesn't like when the object is a constructor...
        check( comment, Match.OneOf( Match.Where( comment=>comment.author ), String ) );
        check( user, Match.OneOf( Match.ObjectIncluding( { _id: String } ), String, null ) );

        if ( UserHelpers.isModerator( user ) ) {
            // Moderator, all access
            return true;
        } else if ( user ) {
            if ( 'object' === typeof comment ) {
                return comment.author == user;
            } else {
                const commentEntity = CommentCollection.findOne( { _id: comment }, { fields: { author: 1 } } );
                if ( !commentEntity ) {
                    return false;
                }
                if ( 'object' === typeof user ) {
                    return commentEntity.author == user._id;
                } else {
                    return commentEntity.author == user;
                }
            }
        } else {
            return false;
        }
    };

}
"use strict";

import flatten from 'flat';
import Logger from "meteor/metalab:logger/Logger";
import { ValidatedMethod } from "meteor/mdg:validated-method";
import Settings from "/imports/settings/Settings";
import Gamification from "/imports/gamification/Gamification";
import Audit from "/imports/audit/Audit";
import { CommentAuditMessages } from "./CommentAudit";
import CommentSchema from "./model/CommentSchema";
import CommentCollection from "./CommentCollection";

const log = Logger( 'comments' );

const CommentMethods = {

    add: new ValidatedMethod( {
        name:     'pr/comment/add',
        validate: CommentSchema.pick( [
            //'objects',
            //'objects.$',
            'objects.$.id',
            'objects.$.type',
            'parent',
            'post'
        ] ).validator( { clean: true } ),
        run( info ) {
            info.author = this.userId; // Assign the card author

            Security.can( this.userId ).insert( info ).for( CommentCollection ).throw();

            if ( info.parent ) {
                const parent = CommentCollection.findOne( { _id: info.parent }, { fields: { _id: 1, ancestors: 1 } } );
                if ( !parent ) {
                    throw new Meteor.Error( 'parent comment not found' );
                }
                info.ancestors = parent.ancestors ? parent.ancestors.concat( [parent._id] ) : [parent._id];
            }

            log.verbose( "Adding comment", info );

            return CommentCollection.insert( info );
        }
    } ),

    update: new ValidatedMethod( {
        name:     'pr/comment/update',
        validate: new SimpleSchema( {
            commentId: { type: String },
            post:      { type: String }
        } ).validator(),
        run( { commentId, post } ) {

            const modifier = { $set: { post } };

            Security.can( this.userId ).update( commentId, modifier ).for( CommentCollection ).throw();

            log.verbose( `Updating comment ${commentId}`, modifier );

            CommentCollection.update( { _id: commentId }, modifier );
        }
    } ),

    remove: new ValidatedMethod( {
        name:     'pr/comment/remove',
        validate: new SimpleSchema( {
            commentId: { type: String }
        } ).validator(),
        run( { commentId } ) {
            Security.can( this.userId ).remove( commentId ).for( CommentCollection ).throw();

            log.verbose( `Removing comment ${commentId}` );

            CommentCollection.remove( { _id: commentId } );
        }
    } ),

    /**
     * Like a comment
     *
     * @param {string} commentd Comment Id
     */
    like: new ValidatedMethod( {
        name:     'pr/comment/like',
        validate: new SimpleSchema( {
            commentId: { type: String }
        } ).validator(),
        run( { commentId } ) {

            // PRECONDITIONS

            const comment = CommentCollection.findOne(
                {
                    _id: commentId
                },
                {
                    fields:    {
                        author: 1,
                        likes:  1
                    },
                    transform: null
                }
            );
            if ( !comment ) {
                throw new Meteor.Error( 'comment not found' );
            }

            // MODIFIER

            let modifier       = {};
            const alreadyLiked = comment.likes && comment.likes.indexOf( this.userId ) != -1;
            if ( alreadyLiked ) {
                // Unlike
                modifier.$pull = { likes: this.userId };
            } else {
                // Like
                modifier.$addToSet = { likes: this.userId };
            }

            // Set the like count after checking the permissions, as the user can never change those directly
            if ( Meteor.isServer ) {
                // On the server calculate the real like count since we have access to likes
                const likeCount = comment.likes ? comment.likes.length : 0;
                modifier.$set   = { likeCount: likeCount + ( alreadyLiked ? -1 : 1 ) };

            } else {
                // On the client just inc/dec temporarily
                modifier.$inc = { likeCount: alreadyLiked ? -1 : 1 };
            }

            // SECURITY

            Security.can( this.userId ).update( commentId, modifier ).for( CommentCollection ).throw();

            // UPDATE

            // Update card
            CommentCollection.update( { _id: commentId }, modifier );

            // Give the user points only if not liking/unliking its own comment
            //TODO: Find a way to increment project points too
            if ( this.userId != comment.author ) {
                Gamification.awardPoints( this.userId, ( alreadyLiked ? -1 : 1 ) * Settings.shared.gamification.points.comment.like );
            }

            // AUDIT

            Audit.next( {
                domain:    CommentCollection.entityName,
                message:   alreadyLiked ? CommentAuditMessages.unliked : CommentAuditMessages.liked,
                userId:    this.userId,
                commentId: commentId
            } );

        }
    } ),
};

export default CommentMethods;
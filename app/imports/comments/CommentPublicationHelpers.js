"use strict";

import CommentCollection from "./CommentCollection";
import CollectionHelpers from "/imports/collections/CollectionHelpers";

export default class CommentPublicationHelpers {

    /**
     * Get comment list fields
     *
     * @param userId
     */
    static getCommentListFields( userId ) {
        return _.defaults(
            {
                likes: { $elemMatch: { $eq: userId } } // Only show user's likes so that we can know to like/dislike
            },
            CommentPublicationHelpers.defaultFields,
            CommentPublicationHelpers.commentListFields,
            CollectionHelpers.getDefaultFields( CommentCollection, userId )
        );
    };

    /**
     * Get comment detailed fields
     *
     * @param userId
     */
    static getCommentDetailedFields( userId ) {
        return _.defaults(
            {
                likes: { $elemMatch: { $eq: userId } } // Only show user's likes so that we can know to like/dislike
            },
            CommentPublicationHelpers.defaultFields,
            CommentPublicationHelpers.commentDetailedFields,
            CollectionHelpers.getDefaultFields( CommentCollection, userId )
        );
    };
}

CommentPublicationHelpers.defaultFields = {
    _id:       1,
    objects:   1,
    ancestors: 1,
    parent:    1,
    author:    1,
    post:      1,
    likeCount: 1,
    createdAt: 1,
    createdBy: 1,
    updatedAt: 1,
    updatedBy: 1
};

CommentPublicationHelpers.commentListFields = {};

CommentPublicationHelpers.commentDetailedFields = {};
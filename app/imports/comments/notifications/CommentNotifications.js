"use strict";

/**
 * Comment Notification Type Constants
 * @type {{commented: string, replied: string, alsoCommented: string, alsoReplied: string, repliedToYou: string}}
 */
export const NotificationTypes = {
    commented:     'commented',
    replied:       'replied',
    alsoCommented: 'also-commented',
    alsoReplied:   'also-replied',
    repliedToYou:  'replied-to-you',
    liked:         'liked',
    unliked:       'unliked'
};
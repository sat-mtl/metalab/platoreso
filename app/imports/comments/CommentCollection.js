"use strict";

import CollectionHelpers from "/imports/collections/CollectionHelpers";
import ModerationHelpers from "/imports/moderation/ModerationHelpers";
import Comment from "./model/Comment";
import CommentSchema from "./model/CommentSchema";

/**
 * Class
 */
class CommentCollection extends Mongo.Collection {
    constructor( name, options = {} ) {
        // Change transform before calling super, this is the only way to set it
        // Also, this won't suffer from module loading as it is a callback
        options.transform = comment => new Comment( comment );

        super( name, options );

        this.entityName = "comment";

        /**
         * Schema
         */

        this.attachSchema( CommentSchema );

        /**
         * Indexes
         */

        if ( Meteor.isServer ) {
            // Comment list
            this._ensureIndex( {
                'objects.0.type': 1,
                'objects.0.id':   1,
                moderated:        1,
                createdAt:        1
            }, { name: 'type_id_moderated_createdAt' } );
        }

        /**
         * Security
         */

        this.deny( {
            insert: () => true,
            update: () => true,
            remove: () => true
        } );

        /**
         * Behaviors
         */

        this.attachBehaviour( 'timestampable' );

        // The rest goes into a Meteor.startup because since we create the collection instance
        // right away when exporting from this module, we end up executing this constructor
        // before the imported modules are available (when there are circular dependencies).
        // This way we are sure the app is loaded when we execute the following code.
        Meteor.startup( () => {

            /**
             * Registration
             * TODO: Get rid of that system
             */

            CollectionHelpers.registerCollection( this.entityName, this, Comment );

            /**
             * Helpers
             */

            ModerationHelpers.setup( this, Comment );
        } );
    }

    /**
     * Recount comments and set count on the entity's document.
     *
     * @param collection
     * @param entityId
     */
    recountComments( collection, entityId ) {
        const commentCount = this.find( {
            'objects.0.type': collection.entityName,
            'objects.0.id':   entityId
        }, { fields: { _id: 1 } } ).count();
        // Direct, no hooks should run on that
        collection.direct.update( collection.commentEntityQuery( entityId ), { $set: { commentCount: commentCount } } );
    }

    /**
     * Setup a collection to receive comments
     *
     * @param {Mongo.Collection} collection
     * @param {Function} [entityQuery] Function used to get the query for the entity
     */
    setupCollection( collection, entityQuery = null ) {
        // Add comment count to default fields
        collection.defaultFields.push( 'commentCount' );

        // Attach the schema
        collection.attachSchema( {
            commentCount: {
                type:         Number,
                optional:     true,
                defaultValue: 0
            }
        } );

        collection.commentEntityQuery = entityQuery || (entityId => ({ _id: entityId }));

        if ( Meteor.isServer ) {
            // Setup the hooks only if we are the server

            this.after.insert( ( userId, comment ) => {
                if ( !comment.objects || !comment.objects[0] || comment.objects[0].type != collection.entityName ) {
                    return;
                }
                this.recountComments( collection, comment.objects[0].id );
            } );

            this.after.remove( ( userId, comment ) => {
                if ( !comment.objects || !comment.objects[0] || comment.objects[0].type != collection.entityName ) {
                    return;
                }
                this.recountComments( collection, comment.objects[0].id );
            } );
        }

    }

    /**
     * Remove all comments for the passed object.
     *
     * @param {String} id Entity ID
     * @param {String} entityType Entity type name
     */
    removeAllForObject( id, entityType ) {
        this.remove( {
            'objects.0.id':   id,
            'objects.0.type': entityType
        } );
    }
}

export default new CommentCollection( "comments" );
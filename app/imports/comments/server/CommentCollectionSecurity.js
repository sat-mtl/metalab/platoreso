"use strict";

import { RoleList } from "/imports/accounts/Roles";
import CommentCollection from "../CommentCollection";

/**
 * Security Rules
 *
 * Only the most restrictive basic security rules are enforced here.
 * The reason behind this is that the comments package should not know about
 * the concrete entities it allows commenting on and those entities don't have to know
 * that that can be commented on. So here is just the basics and an umbrella package
 * should define the security for each entity type individually.
 */

/**
 * Deny if comment is not for the provided entity type
 */
Security.defineMethod( "ifEntityTypeIs", {
    fetch: ['object'],
    transform: null,
    allow:  function ( type, arg, userId, comment, fields, modifier ) {
        return comment && comment.objects[0].type === arg;
    }
} );

/**
 * Deny if current user is not the comment author
 */
Security.defineMethod( "ifIsCommentAuthor", {
    fetch: ['author'],
    transform: null,
    allow:  function ( type, arg, userId, comment, fields, modifier ) {
        return comment && userId === comment.author;
    }
} );

CommentCollection.permit( ['insert', 'update', 'remove'] )
                 .ifLoggedIn()
                 .ifHasRole( RoleList.admin );

CommentCollection.permit( ['update'] )
                 .onlyProps( ['post'] )
                 .ifLoggedIn()
                 .ifIsCommentAuthor();

CommentCollection.permit( ['remove'] )
                 .ifLoggedIn()
                 .ifIsCommentAuthor();
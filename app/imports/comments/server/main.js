"use strict";

// GENERAL
import "./CommentMeld";
import "./CommentHooks";
import "./CommentAuditObserver";
import "./CommentCollectionSecurity";

// PUBLICATIONS
import "./publications/AdminCommentEditPublication";
import "./publications/AdminCommentListPublication";
import "./publications/CommentPublication";
import "../moderation/CommentModerationReportListPublication";
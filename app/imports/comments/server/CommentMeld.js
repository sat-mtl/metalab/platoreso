"use strict";

import Logger from "meteor/metalab:logger/Logger";
import AccountsMeldHelper from "/imports/accounts/server/AccountsMeldHelper";
import CommentCollection from  "../CommentCollection";

const log = Logger("comment-meld");

/**
 * Card Melding
 */
AccountsMeldHelper.onMeld( ( src_user_id, dst_user_id ) => {
    log.debug(`Melding comments for user ${src_user_id} into ${dst_user_id}`);

    CommentCollection.update( { 'author': src_user_id }, { $set: { 'author': dst_user_id } }, { multi: true } );
    CommentCollection.update( { 'createdBy': src_user_id }, { $set: { 'createdBy': dst_user_id } }, { multi: true } );
    CommentCollection.update( { 'updatedBy': src_user_id }, { $set: { 'updatedBy': dst_user_id } }, { multi: true } );
} );
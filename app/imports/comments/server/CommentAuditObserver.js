"use strict";

import "rxjs/add/operator/filter";
import Logger from "meteor/metalab:logger/Logger";
import Audit from "/imports/audit/Audit";
import FeedCollection from "/imports/feed/FeedCollection";
import NotificationCollection from "/imports/notifications/NotificationCollection";
import CommentCollection from "/imports/comments/CommentCollection";
import { StoryTypes } from "/imports/comments/feed/CommentStories";
import { CommentAuditMessages } from "/imports/comments/CommentAudit";
import { NotificationTypes } from "/imports/comments/notifications/CommentNotifications";

const log = Logger( "comments-audit-observer" );

// LIKED
Audit.subject
     .filter( audit => audit.message == CommentAuditMessages.liked )
     .subscribe( ( { userId, commentId } ) => {
         log.verbose( `liked - Comment ${commentId} liked by ${userId}` );

         const comment = CommentCollection.findOne( { _id: commentId }, {
             fields: {
                 author:  1,
                 objects: 1
             }
         } );
         if ( !comment ) {
             log.warn( `liked - Comment ${commentId} not found` );
             return;
         }

         FeedCollection.recordStory( {
             internal: false,
             subject:  userId,
             action:   StoryTypes.liked,
             objects:  comment.objects,
             data:     {
                 comment: comment._id
             }
         } );

         if ( userId != comment.author ) {
             // Notify the card author
             NotificationCollection.notify( {
                 user:    comment.author,
                 subject: userId,
                 action:  NotificationTypes.liked,
                 objects: [{ id: comment._id, type: CommentCollection.entityName }].concat( comment.objects ),
             } );
         }
     } );


// UNLIKED
Audit.subject
     .filter( audit => audit.message == CommentAuditMessages.unliked )
     .subscribe( ( { userId, commentId } ) => {
         log.verbose( `unliked - Comment ${commentId} unliked by ${userId}` );

         const comment = CommentCollection.findOne( { _id: commentId }, {
             fields: {
                 author:  1,
                 objects: 1
             }
         } );
         if ( !comment ) {
             log.warn( `unliked - Comment ${commentId} not found` );
             return;
         }

         // Record internally
         FeedCollection.recordStory( {
             internal: true,
             subject:  userId,
             action:   StoryTypes.unliked,
             objects:  comment.objects
         } );

         // Hide from general public
         FeedCollection.makeInternal( {
             subject:        userId,
             action:         StoryTypes.liked,
             'data.comment': comment._id
         } );

         // Unnotify
         NotificationCollection.unnotify( comment.author, userId, NotificationTypes.liked, comment._id, CommentCollection.entityName );
     } );
"use strict";

import Logger from "meteor/metalab:logger/Logger";
import FeedCollection from "/imports/feed/FeedCollection";
import NotificationCollection from "/imports/notifications/NotificationCollection";
import CommentCollection from "../CommentCollection";

const log = Logger( "comments" );

/**
 * Comment Removed
 */
CommentCollection.after.remove( function ( userId, comment ) {
    // Defer, so that methods can return without waiting
    Meteor.defer( () => {
        log.debug( `Comment ${comment._id} removed` );

        // Remove replies
        log.silly( "    - removing replies" );
        CommentCollection.remove( { ancestors: comment._id } );

        // Remove stories
        log.silly( "    - removing stories" );
        FeedCollection.removeAllForObject( comment._id, CommentCollection.entityName );
        FeedCollection.removeStories( { 'data.comment': comment._id } );

        // Remove Notifications
        log.silly( "    - removing notifications" );
        NotificationCollection.unnotifyAllForObject( comment._id, CommentCollection.entityName );
    } );
} );
"use strict";

import { RoleGroups } from "/imports/accounts/Roles";
import GroupCollection from "/imports/groups/GroupCollection";
import GroupHelpers from "/imports/groups/GroupHelpers";
import ProjectCollection from "/imports/projects/ProjectCollection";
import CommentCollection from "../../CommentCollection";

Meteor.publish( 'comment', function ( commentId ) {
    check( commentId, String );

    if ( !this.userId ) {
        return this.ready();
    }

    // Let's $or whatever the user has access to
    let query = {
        _id: commentId,
        $or: []
    };

    // GROUPS
    const userGroups = GroupHelpers.getGroupsForUser( this.userId, RoleGroups.members );
    query.$or.push( { 'objects.0.type': GroupCollection.entityName, 'objects.0.id': { $in: userGroups } } );

    // PROJECTS
    const projects   = ProjectCollection.find( { groups: { $in: userGroups } }, { fields: { _id: 1 } } );
    const projectIds = projects.map( project => project._id );
    query.$or.push( {
        objects: {
            $elemMatch: {
                'type': ProjectCollection.entityName,
                'id':   { $in: projectIds }
            }
        }
    } );

    return CommentCollection.find( query );
} );
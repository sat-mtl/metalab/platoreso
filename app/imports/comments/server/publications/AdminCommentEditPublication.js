"use strict";

import UserHelpers from "/imports/accounts/UserHelpers";
import CommentCollection from "../../CommentCollection";

/**
 * Comment Edit (Administration)
 */
Meteor.publish( "admin/comment/edit", function ( id ) {
    check( id, String );

    if ( !UserHelpers.isSuper( this.userId ) ) {
        return this.ready();
    }

    return [
        CommentCollection.find( { _id: id } )
    ];
} );
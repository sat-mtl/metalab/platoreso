"use strict";

import slugify from "underscore.string/slugify";
import UserCollection from "/imports/accounts/UserCollection";
import UserHelpers from "/imports/accounts/UserHelpers";
import CommentCollection from "../../CommentCollection";

/**
 * Comments List (Administration)
 *
 * @params {Object} [options] Options for the find operation (ex: sort)
 */
Meteor.publishComposite( "admin/comment/list", function ( query = null, options = null ) {

    // Check query for only the allowed fields
    check( query, Match.OneOf( null, {
        post:   Match.Optional( Match.ObjectIncluding( { $regex: String } ) ),
        author: Match.Optional( String )
    } ) );

    // Check options for only the allowed fields
    check( options, Match.OneOf( null, {
        sort:  Match.Optional( Object ),
        limit: Match.Optional( Number ),
        skip:  Match.Optional( Number )
    } ) );

    if ( !UserHelpers.isSuper( this.userId ) ) {
        return this.ready();
    }

    query   = query || {};
    options = _.extend( options || {}, {
        fields: {
            _id:       1,
            slug:      1,
            author:    1,
            objects:   1,
            post:      1,
            createdAt: 1,
            createdBy: 1,
            updatedAt: 1,
            updatedBy: 1
        }
    } );

    let children = [];

    if ( !query.author ) {
        children.push( {
            find( card ) {
                return UserCollection.find( { _id: card.author }, {
                    fields: {
                        'profile.firstName': 1,
                        'profile.lastName':  1
                    }
                } );
            }
        } );
    }

    return {
        find() {
            // Slugify the json query as a unique identifier for the count
            Counts.publish( this,
                'comment/count/' + slugify( JSON.stringify( query ) ),
                CommentCollection.find( query, { fields: { _id: 1 } } ),
                { noReady: true }
            );

            return CommentCollection.find( query, options )
        },
        children: children
    };
} );
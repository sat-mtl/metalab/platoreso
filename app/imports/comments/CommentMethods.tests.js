
import { chai, assert, expect } from "meteor/practicalmeteor:chai";
import { sinon, spies, stubs } from "meteor/practicalmeteor:sinon";
import CommentMethods, { commitImages } from "/imports/comments/CommentMethods";
import CommentCollection from "/imports/comments/CommentCollection";
import CommentSecurity from "/imports/comments/CommentSecurity";
import { CommentAuditMessages } from "/imports/comments/CommentAudit";
import Gamification from "/imports/gamification/Gamification";
import Settings from "/imports/settings/Settings";
import Audit from "/imports/audit/Audit";

describe( 'comments/Methods', () => {

    let sandbox;

    beforeEach( () => {
        sandbox = sinon.sandbox.create();
        sandbox.stub( Audit, 'next' );

        Settings.shared = {
            gamification: {
                points: {
                    comment: {
                        like:     1
                    }
                }
            }
        };
    } );

    afterEach( () => {
        sandbox.restore();
    } );

});
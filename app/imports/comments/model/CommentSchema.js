"use strict";

import { SimpleSchema } from "meteor/aldeed:simple-schema";

export const CommentObjectSchema = new SimpleSchema( {
    id:   {
        type: String
    },
    type: {
        type: String
    }
} );

const CommentSchema = new SimpleSchema( {
    objects:   {
        type: [CommentObjectSchema]
    },
    ancestors: {
        type:     [String],
        optional: true
    },
    parent:    {
        type:     String,
        optional: true
    },
    author:    {
        type: String
    },
    post:      {
        type: String
    },
    likes:     {
        type:         [String],
        optional:     true,
        defaultValue: []
    },

    likeCount: {
        type:         Number,
        optional:     true,
        defaultValue: 0
    },
} );

export default CommentSchema;
"use strict";

import prune from "underscore.string/prune";
import md, { mdInline } from "/imports/utils/markdown";
import UserCollection from "/imports/accounts/UserCollection";

/**
 * Comment Model
 *
 * @param doc
 * @constructor
 */
export default class Comment {

    constructor( doc ) {
        _.extend( this, doc );
    }

    /**
     * Get an excerpt as rendered markdown
     * We have to prune BEFORE markdown, otherwise we run the risk of having broken markup.
     *
     * @parms {Number} length Number of characters in the excerpt
     * @returns {String}
     */
    excerptMarkdown( length = 140 ) {
        if ( !this._excerptMarkdown ) {
            this._excerptMarkdown = {};
        }
        if ( !this._excerptMarkdown[length] ) {
            this._excerptMarkdown[length] = mdInline( prune( this.post || '', length ) );
        }
        return this._excerptMarkdown[length];
    }

    /**
     * Get post as rendered markdown
     *
     * @getter
     * @returns {String}
     */
    get postMarkdown() {
        if ( !this._postMarkdown ) {
            this._postMarkdown = md( this.post || '' );
        }
        return this._postMarkdown;
    }

    /**
     * Get the entity type commented on
     * @returns {String}
     */
    get entityType() {
        if ( !this.objects || !this.objects.length ) {
            throw new Meteor.Error( 'Missing field objects in comment to get entityType' );
        }
        return this.objects[0].type;
    }

    /**
     * Get the entity id commented on
     * @returns {String}
     */
    get entityId() {
        if ( !this.objects || !this.objects.length ) {
            throw new Meteor.Error( 'Missing field objects in comment to get entity' );
        }
        return this.objects[0].id;
    }

    /**
     * Get the entity id commented on
     * @deprecated Use entityId instead
     * @returns {String}
     */
    get entity() {
        return this.entityId;
    }

    /**
     * Find author cursor for this card
     *
     * @returns {*}
     */
    findAuthor() {
        return UserCollection.find( { _id: this.author }, {
            fields: {
                _id:                 1,
                points:              1,
                'profile.firstName': 1,
                'profile.lastName':  1,
                'profile.avatarUrl': 1
            }
        } );
    }

    /**
     * Get the author for this card
     *
     * @returns {Meteor.User}
     */
    getAuthor() {
        return UserCollection.findOne( { _id: this.author }, {
            fields: {
                _id:                 1,
                points:              1,
                'profile.firstName': 1,
                'profile.lastName':  1,
                'profile.avatarUrl': 1
            }
        } );
    }
};
"use strict";

// Collection
import "./CommentCollection";

// Methods
import "./CommentMethods";
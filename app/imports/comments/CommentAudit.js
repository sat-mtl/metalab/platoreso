"use strict";

export const CommentAuditMessages = {
    liked: "liked",
    unliked: "unliked"
};
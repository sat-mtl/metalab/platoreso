"use strict";

/**
 * Comment Story Type Constants
 * @type {{commented: string, editedComment: string, replied: string, editedReply: string}}
 */
export const StoryTypes = {
    commented:     'commented',
    editedComment: 'edited-comment',
    replied:       'replied',
    editedReply:   'edited-reply',
    liked:         'liked',
    unliked:       'unliked'
};

export const StoryNames = _.values( StoryTypes );
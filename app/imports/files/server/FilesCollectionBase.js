"use strict";

import Logger from "meteor/metalab:logger/Logger";
import { FS } from "meteor/cfs:base-package";
import { Security } from "meteor/ongoworks:security";
import AccountsMeldHelper from "/imports/accounts/server/AccountsMeldHelper";

const log = Logger( "files-collection-server" );

/**
 * Deny if user is not the uploader
 */
Security.defineMethod( "ifIsUploader", {
    fetch:     [],
    transform: null,
    deny:      function ( type, arg, userId, doc, fields, modifier ) {
        return userId != doc.uploader;
    }
} );

export default class FilesCollectionBase extends FS.Collection {

    constructor( name, options = {} ) {
        super( name, options );

        /**
         * Because of the way collectionFS works
         * this is the closest we have to methods to handle the uploaded file.
         * This handles linking the uploads to the right uploader.
         */
        this.files.before.insert( this._beforeInsert );

        /**
         * We have to allow the uploader to update, since that's how files are uploaded
         */
        Security.permit( ['update'] )
            .collections( [ this ] )
            .ifLoggedIn()
            .ifIsUploader()
            .allowInClientCode();

        // Account Melding
        AccountsMeldHelper.onMeld( ( src_user_id, dst_user_id ) => {
            log.debug( `Melding uploads for user ${src_user_id} into ${dst_user_id}` );
            this.update( { 'uploader': src_user_id }, { $set: { 'uploader': dst_user_id } }, { multi: true } );
        } );
    }

    _beforeInsert( userId, file ) {
        file.uploader = userId;
    }

    /**
     * Remove or update file when an owner is deleted
     *
     * @param {String|Array<String>} ownerId Owner entity that was deleted
     * @param {String|Array<String>} excludedFileIds Files to ignore (possibly because they are the one being updated/replaced)
     */
    unlinkOwner( ownerId, excludedFileIds = null ) {
        // We support multiple owners in the query
        if ( !_.isArray( ownerId ) ) {
            ownerId = [ownerId];
        }

        let removeQuery = {owners: {$in: ownerId, $size: 1}};
        let updateQuery = {owners: {$in: ownerId}};

        // Ignore the exlcuded ids
        if ( excludedFileIds ) {
            if ( !_.isArray( excludedFileIds ) ) {
                excludedFileIds = [excludedFileIds];
            }

            removeQuery._id = {$nin: excludedFileIds};
            updateQuery._id = {$nin: excludedFileIds}
        }

        // Remove all matching single-owner files
        // If a file only has one owner then it is automatically removed when the owner is deleted
        this.remove( removeQuery );

        // Update all matching multi-owner files
        // If a file has more than one owner, then we don't remove it, we just remove the deleted owner from the
        // current file owners.
        this.update( updateQuery, {$pullAll: {owners: ownerId}}, {multi: true} );
    }

    /**
     * Remove or update file when an owner is deleted
     * This is for the entities that use an object array for the owners (like the cards, because of the version support)
     *
     * @param {String|Array<String>} ownerId Owner entity that was deleted
     */
    unlinkVersionedOwner( ownerId ) {
        // We support multiple owners in the query
        if ( !_.isArray( ownerId ) ) {
            ownerId = [ownerId];
        }

        let removeQuery = {'owners.id': {$in: ownerId}, owners: {$size: 1}};
        let updateQuery = {'owners.id': {$in: ownerId}};

        // Remove all matching single-owner files
        // If a file only has one owner then it is automatically removed when the owner is deleted
        this.remove( removeQuery );

        // Update all matching multi-owner files
        // If a file has more than one owner, then we don't remove it, we just remove the deleted owner from the
        // current image owners.
        this.update( updateQuery, {$pull: {owners: {id: ownerId}}}, {multi: true} );
    };
}
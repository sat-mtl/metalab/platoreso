"use strict";

import { chai, assert, expect } from 'meteor/practicalmeteor:chai';
import { sinon, spies, stubs } from 'meteor/practicalmeteor:sinon';

import { FS } from "meteor/cfs:base-package";
import FilesCollection from "/imports/files/FilesCollection";


describe( 'FilesCollection', function () {

    let sandbox;
    let name;

    beforeEach( () => {
        sandbox = sinon.sandbox.create();

        name = Random.id();

        sandbox.spy( FS.Store, 'FileSystem' );
        sandbox.spy( FS, 'Collection' );
    } );

    afterEach( () => {
        sandbox.restore();
    } );

    describe( 'findPending', () => {

        let collection;

        beforeEach( ()=> {
            collection = new FilesCollection( name, {stores: [new FS.Store.FileSystem( name )]} );
            sandbox.stub( collection, 'find' ).returns( 'cursor' );
        } );

        it( "should find pending files", () => {
            expect( collection.findPending( 'ownerId' ) ).to.equal( 'cursor' );
            assert( collection.find.calledWith( {
                pending:     true,
                'owners.id': 'ownerId'
            }, {
                fields: {
                    _id:    1,
                    owners: 1
                }
            } ) );
        } );

    });

    describe( "fetchPending", () => {
        let collection;

        beforeEach( ()=> {
            collection = new FilesCollection( name, {stores: [new FS.Store.FileSystem( name )]} );
            sandbox.stub( collection, 'find' ).returns( 'cursor' );
        } );

        it( "should fetch pending files", () => {
            const fetch = sandbox.stub().returns( 'pending' );
            sandbox.stub( collection, 'findPending' ).returns( {fetch} );
            expect( collection.fetchPending( "ownerId" ) ).to.equal( "pending" );
            assert( collection.findPending.calledWith( "ownerId" ) );
        } );

    } );

    if ( Meteor.isServer ) {

        describe( 'unlinkOwner', () => {

            let collection;

            beforeEach( ()=> {
                collection = new FilesCollection( name, {stores: [new FS.Store.FileSystem( name )]} );
                sandbox.stub( collection, 'remove' );
                sandbox.stub( collection, 'update' );
            } );

            it( 'should unlink one unique owner', () => {
                collection.unlinkOwner( 'ownerId' );
                assert( collection.remove.calledWith( {owners: {$in: ['ownerId'], $size: 1}} ) );
                assert( collection.update.calledWith( {owners: {$in: ['ownerId']}}, {$pullAll: {owners: ['ownerId']}}, {multi: true} ) );
            } );

            it( 'should unlink one unique owner and ignore a single id', () => {
                collection.unlinkOwner( 'ownerId', 'ignoredId' );
                assert( collection.remove.calledWith( {
                    _id:    {$nin: ['ignoredId']},
                    owners: {$in: ['ownerId'], $size: 1}
                } ) );
                assert( collection.update.calledWith( {
                    _id:    {$nin: ['ignoredId']},
                    owners: {$in: ['ownerId']}
                }, {$pullAll: {owners: ['ownerId']}}, {multi: true} ) );
            } );

            it( 'should unlink multiple owners', () => {
                collection.unlinkOwner( ['ownerId1', 'ownerId2'] );
                assert( collection.remove.calledWith( {owners: {$in: ['ownerId1', 'ownerId2'], $size: 1}} ) );
                assert( collection.update.calledWith( {owners: {$in: ['ownerId1', 'ownerId2']}}, {$pullAll: {owners: ['ownerId1', 'ownerId2']}}, {multi: true} ) );
            } );

            it( 'should unlink one unique owner and ignore multiple ids', () => {
                collection.unlinkOwner( 'ownerId', ['ignoredId1', 'ignoredId2'] );
                assert( collection.remove.calledWith( {
                    _id:    {$nin: ['ignoredId1', 'ignoredId2']},
                    owners: {$in: ['ownerId'], $size: 1}
                } ) );
                assert( collection.update.calledWith( {
                    _id:    {$nin: ['ignoredId1', 'ignoredId2']},
                    owners: {$in: ['ownerId']}
                }, {$pullAll: {owners: ['ownerId']}}, {multi: true} ) );
            } );

            it( 'should unlink multiple owners and ignore multiple ids', () => {
                collection.unlinkOwner( ['ownerId1', 'ownerId2'], ['ignoredId1', 'ignoredId2'] );
                assert( collection.remove.calledWith( {
                    _id:    {$nin: ['ignoredId1', 'ignoredId2']},
                    owners: {$in: ['ownerId1', 'ownerId2'], $size: 1}
                } ) );
                assert( collection.update.calledWith( {
                    _id:    {$nin: ['ignoredId1', 'ignoredId2']},
                    owners: {$in: ['ownerId1', 'ownerId2']}
                }, {$pullAll: {owners: ['ownerId1', 'ownerId2']}}, {multi: true} ) );
            } );

        } );
    }

} );
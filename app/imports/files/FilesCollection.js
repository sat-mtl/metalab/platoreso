"use strict";

import { FS } from "meteor/cfs:base-package";

let FilesCollectionBase;
if ( Meteor.isServer ) {
    FilesCollectionBase = require( "./server/FilesCollectionBase" ).default;
} else {
    FilesCollectionBase = require( "./client/FilesCollectionBase" ).default;
}

export default class FilesCollection extends FilesCollectionBase {

    constructor( name, options = {}, config = {} ) {
        super( name, options );
        _.defaults( config, {replacePrevious: false} );
        this.config = config;
    }

    /**
     * Find files marked as pending for a specified owner
     *
     * @param ownerId
     * @returns {*}
     */
    findPending( ownerId ) {
        return this.find( {
            pending:     true,
            'owners.id': ownerId
        }, {
            fields:    {
                _id:    1,
                owners: 1
            }
        } );
    }

    /**
     * Fetch files marked as pending for a specified owner
     *
     * @param ownerId
     * @returns {*}
     */
    fetchPending( ownerId ) {
        return this.findPending( ownerId ).fetch();
    }

}
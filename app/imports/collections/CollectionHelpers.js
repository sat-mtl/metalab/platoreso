"use strict";

export default class CollectionHelpers {

    static registerCollection( entityType, collection, model = null ) {
        collection.entityName = entityType;
        collection.model = model;
        collection.defaultFields = collection.defaultFields || [];
        CollectionHelpers.map[entityType] = collection;
    }

    /**
     * Get the entry for an entity
     *
     * @param {String} entityName
     * @returns {Object}
     */
    static get(entityName) {
        return CollectionHelpers.map[entityName];
    }

    static getDefaultFields( collection, userId, customFields = null ) {
        let fields             = _.defaults( customFields || {}, { _id: 1 } );
        let collectionDefaults = {};
        collection.defaultFields.forEach( field => {
            if ( "function" == typeof field ) {
                return _.defaults( collectionDefaults, field( userId ) );
            } else {
                return _.defaults( collectionDefaults, { [field]: 1 } );
            }
        } );
        _.defaults( fields, collectionDefaults );
        return fields;
    }
}

CollectionHelpers.map = {};
"use strict";

/**
 * Aggregate
 *
 * Exposes the rawCollection's aggregate function.
 * This can only be used on the server and is not reactive.
 *
 * Based off meteorhacks:meteor-aggregate
 * @see https://github.com/meteorhacks/meteor-aggregate
 *
 * @param pipelines
 * @param options
 * @returns {*}
 */
Mongo.Collection.prototype.aggregate = function ( pipelines, options ) {
    const rawCollection = this.rawCollection();
    return Meteor.wrapAsync( rawCollection.aggregate.bind( rawCollection ) )( pipelines, options );
};

/**
 * Distinct
 *
 * Exposes the rawCollection's distinct function.
 * This can only be used on the server and is not reactive.
 *
 * Based off meteorhacks:meteor-aggregate
 * @see https://github.com/meteorhacks/meteor-aggregate
 *
 * @param field
 * @param query
 * @returns {*}
 */
Mongo.Collection.prototype.distinct = function ( field, query ) {
    const rawCollection = this.rawCollection();
    return Meteor.wrapAsync( rawCollection.distinct.bind( rawCollection ) )( field, query );
};
"use strict";

import { chai, assert, expect } from 'meteor/practicalmeteor:chai';
import { sinon, spies, stubs } from 'meteor/practicalmeteor:sinon';

import CollectionHelpers from "/imports/collections/CollectionHelpers";

describe( 'Collections/CollectionHelpers', function () {

    let sandbox;

    beforeEach( () => {
        sandbox = sinon.sandbox.create();
    } );

    afterEach( () => {
        sandbox.restore();
    } );

    describe( 'registerCollection', function () {

        beforeEach( function () {
            CollectionHelpers.map = {};
        } );

        it( 'should register a collection', function () {
            const collection = {};
            expect( CollectionHelpers.map ).to.eql( {} );
            CollectionHelpers.registerCollection( 'entity', collection, 'model' );
            expect( CollectionHelpers.map['entity'] ).not.to.be.undefined;
            expect( CollectionHelpers.map['entity'] ).to.eql( { entityName: 'entity', model: 'model', defaultFields: [] } );
        } );

    } );

    describe( 'get', function () {

        it( 'should get the entry from the map', function () {
            CollectionHelpers.map['test'] = { test: 'entry' };
            expect( CollectionHelpers.get( 'test' ) ).to.eql( { test: 'entry' } );
        } );

    } );

    describe( 'getDefaultFields', function () {

        it( 'should get the default fields form the collection', function () {
            const collection = {
                defaultFields: ['_id']
            };
            const result     = {
                _id: 1
            };
            expect( CollectionHelpers.getDefaultFields( collection, 'userId', {} ) ).to.eql( result );
        } );

        it( 'should add custom fields', function () {
            const collection = {
                defaultFields: ['_id']
            };
            const result     = {
                _id:  1,
                name: 1
            };
            expect( CollectionHelpers.getDefaultFields( collection, 'userId', { name: 1 } ) ).to.eql( result );
        } );

        it( 'should overwrite the default fields if needed', function () {
            const collection = {
                defaultFields: ['_id']
            };
            const result     = {
                _id: 2 // The 2 means nothing for mongo, it is just to test overwrite
            };
            expect( CollectionHelpers.getDefaultFields( collection, 'userId', { _id: 2 } ) ).to.eql( result );
        } );

        it( 'should call the getters', function () {
            const getter     = sinon.stub();
            getter.returns( { name: 1 } );
            const collection = {
                defaultFields: ['_id', getter]
            };
            const result     = {
                _id:  1,
                name: 1
            };
            expect( CollectionHelpers.getDefaultFields( collection, 'userId', {} ) ).to.eql( result );
            assert( getter.calledWith( 'userId' ) );
        } );

    } );

} );
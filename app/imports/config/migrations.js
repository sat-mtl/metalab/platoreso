"use strict";

import { Migrations } from "meteor/percolate:migrations"
import logger from "meteor/metalab:logger/Logger";
const log = logger('Migrations');

/**
 * Current Migration version
 * Please document each migration in order to keep track of what is going on
 *
 * 1  -> multiple owners
 * 2  -> changed field + action names
 * 3  -> remove allowImageUpload from phases
 * 4  -> phaseVisits initial support
 * 5  -> removed comment from feed objects
 * 6  -> card versioning support
 * 7  -> email notifications support
 * 8  -> linked cards types
 * 9  -> Card analytics
 * 10 -> Gamification
 * 11 -> Linked cards in feed & notifications
 * 12 -> Fix for emailNotifications field not being added to created users
 * 13 -> Combined CardCollection & CardHistoryCollection
 * 14 -> Set all phases as not removed for new soft removable phases
 * 15 -> Move all tweets hashtags as lowercase string array to be indexed + add user recentProjects
 * 16 -> Card excerpts + project heat
 * 17 -> Comment object ancestors for moderation filtering
 * 18 -> Added comment like gamification
 * 19 -> Added conversations for groups
 * 20 -> HTTPS for profile pictures + terms of service
 *
 * @type {number}
 */
export const migration = 20;

log.info("Configuring migrations");

/**
 * Migrations Configuration
 * This is a "global" configuration that is going to be shared by any package using migrations
 */
Migrations.config( {
    log:            true,
    logger:         ( { level, message, tag } ) => log.log( level, '[' + tag + '] ' + message ),
    logIfLatest:    true,
    collectionName: "migrations"
} );

/**
 * Load migrations
 */
import "../migrations/index";

/**
 * Perform migration to "latest" on Meteor.startup,
 * thus letting the time to modules to define their migration strategies beforehand
 */
Meteor.startup( () => {
    if ( !migration ) {
        log.error( 'Migration version undefined!' );
    } else {
        log.info( "Running migrations" );
        Migrations.migrateTo( migration );
    }
} );
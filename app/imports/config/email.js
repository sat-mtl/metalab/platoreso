"use strict";

import path from "path";
import {EmailTemplate} from "email-templates"
import {Accounts} from "meteor/accounts-base";
import logger from "meteor/metalab:logger/Logger";
import {templatesPath} from "/imports/mailer/server/Mailer";

const log = logger( 'Email' );

const { email: emailConfig } = Meteor.settings;

if ( emailConfig ) {
    log.info( "Configuring email" );

    if ( !process.env.MAIL_URL && emailConfig.username && emailConfig.password && emailConfig.server && emailConfig.port ) {
        process.env.MAIL_URL = `smtp://${encodeURIComponent( emailConfig.username )}:${emailConfig.password}@${emailConfig.server}:${emailConfig.port}`;
    }

    Accounts.emailTemplates.siteName = emailConfig.siteName;
    Accounts.emailTemplates.from     = emailConfig.from;

} else {
    log.warn( "No email configuration" )
}

function subjectTemplate(templateName) {
    return user => {
        const template = new EmailTemplate( path.join( templatesPath, templateName ) );
        return Meteor.wrapAsync( template.render, template )( { user } ).subject;
    }
}

function htmlTemplate(templateName) {
    return ( user, url ) => {
        const template = new EmailTemplate( path.join( templatesPath, templateName ) );
        return Meteor.wrapAsync( template.render, template )( { user, url } ).html;
    }
}

// RESET PASSWORD
Accounts.emailTemplates.resetPassword.subject = subjectTemplate('reset-password');
Accounts.emailTemplates.resetPassword.html = htmlTemplate('reset-password');
Accounts.emailTemplates.resetPassword.text = null;

// ENROLL ACCOUNT
Accounts.emailTemplates.enrollAccount.subject = subjectTemplate('enroll-account');
Accounts.emailTemplates.enrollAccount.html = htmlTemplate('enroll-account');
Accounts.emailTemplates.enrollAccount.text = null;

// VERIFY EMAIL
Accounts.emailTemplates.verifyEmail.subject = subjectTemplate('verify-email');
Accounts.emailTemplates.verifyEmail.html = htmlTemplate('verify-email');
Accounts.emailTemplates.verifyEmail.text = null;
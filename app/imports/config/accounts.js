"use strict";

import logger from "meteor/metalab:logger/Logger";
const log = logger('Accounts');

// Sign in services
import "./accounts/facebook";
import "./accounts/google";
import "./accounts/linkedIn";
import "./accounts/twitter";

log.info("Configuring Accounts");

/**
 * Accounts Configuration
 */
Accounts.config( {
    sendVerificationEmail: true
} );

/**
 * Configures "reset password account" email link
 */
Accounts.urls.resetPassword = function ( token ) {
    return Meteor.absoluteUrl( "reset-password/" + token );
};

/**
 * Configures "enroll account" email link
 */
Accounts.urls.enrollAccount = function ( token ) {
    return Meteor.absoluteUrl( "enroll-account/" + token );
};

/**
 * Configures "verify email" email link
 */
Accounts.urls.verifyEmail = function ( token ) {
    return Meteor.absoluteUrl( "verify-email/" + token );
};

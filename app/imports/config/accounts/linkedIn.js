"use strict";

import { ServiceConfiguration } from "meteor/service-configuration";

if( Meteor.settings.linkedIn ) {
    ServiceConfiguration.configurations.upsert(
        { service: "linkedin" },
        {
            $set: {
                clientId: Meteor.settings.linkedIn.clientId,
                secret: Meteor.settings.linkedIn.secret
            }
        }
    );
}
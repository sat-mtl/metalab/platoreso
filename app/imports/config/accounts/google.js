"use strict";

import { ServiceConfiguration } from "meteor/service-configuration";

if( Meteor.settings.google ) {
    ServiceConfiguration.configurations.upsert(
        { service: "google" },
        {
            $set: {
                clientId: Meteor.settings.google.clientId,
                secret: Meteor.settings.google.secret
            }
        }
    );
}
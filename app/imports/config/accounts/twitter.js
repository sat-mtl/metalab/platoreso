"use strict";

import { ServiceConfiguration } from "meteor/service-configuration";

if(Meteor.settings.twitter) {
    ServiceConfiguration.configurations.upsert(
        { service: "twitter" },
        {
            $set: {
                consumerKey: Meteor.settings.twitter.consumerKey,
                secret: Meteor.settings.twitter.consumerSecret
            }
        }
    );
}
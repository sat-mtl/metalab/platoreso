"use strict";

import { ServiceConfiguration } from "meteor/service-configuration";

if( Meteor.settings.facebook ) {
    ServiceConfiguration.configurations.upsert(
        { service: "facebook" },
        {
            $set: {
                appId: Meteor.settings.facebook.appId,
                secret: Meteor.settings.facebook.secret
            }
        }
    );
}
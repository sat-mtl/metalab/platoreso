"use strict";

import { SyncedCron } from 'meteor/percolate:synced-cron';
import logger from "meteor/metalab:logger/Logger";
const log = logger('Cron');

log.info("Configuring Cron");
SyncedCron.config({
    log: true,
    logger: ({ level, message, tag }) => log.log( level, '[' + tag + '] ' + message)
});
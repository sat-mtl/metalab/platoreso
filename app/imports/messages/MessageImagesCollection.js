"use strict";

import ImageCollection from "/imports/images/ImageCollection";

/**
 * Message Images Collection
 */

const MessageImagesCollection = new ImageCollection('message_images', null, {
    overwritePrevious: true
});
export default MessageImagesCollection;
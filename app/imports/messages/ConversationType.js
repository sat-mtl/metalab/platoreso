"use strict";

const ConversationType = {
    group: 'group'
};

export default ConversationType;

export const ConversationTypes = _.values( ConversationType );
"use strict";

import CollectionHelpers from "/imports/collections/CollectionHelpers";
import ModerationHelpers from "/imports/moderation/ModerationHelpers";
import Message from "./model/Message";
import MessageSchema from "./model/MessageSchema";

class MessageCollection extends Mongo.Collection {
    constructor( name, options = {} ) {
        // Change transform before calling super, this is the only way to set it
        // Also, this won't suffer from module loading as it is a callback
        options.transform = message => new Message( message );

        super( name, options );

        this.entityName = "message";

        /**
         * Schema
         */

        this.attachSchema( MessageSchema );

        /**
         * Indexes
         */

        if ( Meteor.isServer ) {

        }

        /**
         * Security
         */

        this.deny( {
            insert: () => true,
            update: () => true,
            remove: () => true
        } );

        /**
         * Behaviors
         */

        this.attachBehaviour( 'timestampable' );

        // The rest goes into a Meteor.startup because since we create the collection instance
        // right away when exporting from this module, we end up executing this constructor
        // before the imported modules are available (when there are circular dependencies).
        // This way we are sure the app is loaded when we execute the following code.
        Meteor.startup( () => {
            /**
             * Registration
             * TODO: Get rid of that system
             */

            CollectionHelpers.registerCollection( this.entityName, this, Message );

            /**
             * Helpers
             */
            ModerationHelpers.setup( this, Message );

        } );
    }

    /**
     * Get message default fields
     *
     * @param userId
     */
    getDefaultFields( userId ) {
        return _.defaults(
            {}, // Important, otherwise we write into default fields
            MessageCollection.defaultFields,
            CollectionHelpers.getDefaultFields( this, userId )
        );
    };

}

MessageCollection.defaultFields = {
    conversation: 1,
    author:       1,
    content:      1,
    hasImage:     1,
    createdAt:    1
};

export default new MessageCollection( "messages" );
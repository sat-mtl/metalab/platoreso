"use strict";

import UserHelpers from "/imports/accounts/UserHelpers";
import ConversationCollection from "./ConversationCollection";
import ConversationType from "/imports/messages/ConversationType";
import {GROUP_PREFIX} from "/imports/groups/GroupRoles";
import {RoleGroups} from "/imports/accounts/Roles";

export default class ConversationHelpers {

    static canModerate( userId, conversationId ) {
        if ( UserHelpers.isModerator( userId ) ) {
            return true;
        } else {
            const conversation = ConversationCollection.findOne(
                { _id: conversationId },
                { fields: { type: 1, entity: 1 } }
            );
            if ( !conversation ) {
                return false;
            }
            if ( conversation.type == ConversationType.group ) {
                return UserHelpers.userIsInRole( userId, RoleGroups.moderators, GROUP_PREFIX + '_' + conversation.entity );
            }
        }

        return false;
    }

}
"use strict";

import slugify from "underscore.string/slugify";
import {Counts} from "meteor/tmeasday:publish-counts";
import UserHelpers from "/imports/accounts/UserHelpers";
import {RoleGroups} from "/imports/accounts/Roles";
import GroupHelpers from "/imports/groups/GroupHelpers";
import ConversationType from "../ConversationType";
import ConversationCollection from "../ConversationCollection";
import MessageCollection from "../MessageCollection";

/**
 * Moderation Report List (Administration)
 *
 * @params {Object} [options] Options for the find operation (ex: sort)
 */
Meteor.publishComposite( "message/moderation/report/list", function ( query = null, options = null ) {

    // Check query for only the allowed fields
    check( query, Match.OneOf(
        null, {
            name:      Match.Optional( Match.ObjectIncluding( { $regex: String } ) ),
            moderated: Match.Optional( Boolean )
        }
    ) );

    // Check options for only the allowed fields
    check( options, Match.OneOf( null, {
        sort:  Match.Optional( Object ),
        limit: Match.Optional( Number ),
        skip:  Match.Optional( Number )
    } ) );


    const moderationQuery = {};

    if ( query && query.moderated != null ) {
        moderationQuery.moderated = query.moderated;
    }

    if ( query && query.name ) {
        moderationQuery.content = query.name;
    }

    if ( !UserHelpers.isModerator( this.userId ) ) {
        const userGroups             = GroupHelpers.getGroupsForUser( this.userId, RoleGroups.moderators );
        const conversations          = ConversationCollection.find( {
                type:   ConversationType.group,
                entity: { $in: userGroups }
            },
            {
                fields:    {
                    _id: 1
                },
                transform: null
            } ).map( conversation => conversation._id );
        moderationQuery.conversation = { $in: conversations };
    }

    // Keep the original (unmodified by permissions, to publish counts to a unique URI)
    const originalQuery = _.clone( moderationQuery );

    options = _.extend( options || {}, {
        fields: {
            _id:          1,
            author:       1,
            conversation: 1,
            content:      1,
            moderated:    1,
            reportCount:  1
        }
    } );

    let children = [
        {
            find: message => message.findConversation( {
                fields: {
                    _id:    1,
                    type:   1,
                    entity: 1,
                    slug:   1,
                    name:   1,
                    topic:  1
                }
            } )
        },
        { find: message => message.findAuthor() }
    ];

    return {
        find() {
            // Slugify the json query as a unique identifier for the count
            Counts.publish( this,
                'message/moderation/report/count/' + slugify( JSON.stringify( originalQuery ) ),
                MessageCollection.findReported( moderationQuery, { fields: { _id: 1 } } ),
                { noReady: true }
            );

            return MessageCollection.findReported( moderationQuery, options )
        },
        children: children
    };
} );
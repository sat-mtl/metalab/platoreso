"use strict";

import Logger from "meteor/metalab:logger/Logger";
import {ValidatedMethod} from "meteor/mdg:validated-method";
import ConversationCollection from "./ConversationCollection";

const log = Logger( "conversation-methods" );

const ConversationMethods = {

    /**
     * Ping a conversation
     *
     * @param {String} conversationId
     */
    ping: new ValidatedMethod( {
        name:     'pr/conversation/ping',
        validate: new SimpleSchema( {
            conversationId: {
                type: String
            }
        } ).validator( { clean: true } ),
        run( { conversationId } ) {

            // UPDATE
            // No need for security here since we're filtering by participant in the query

            log.silly( "ping - Pinging conversation", conversationId );
            return ConversationCollection.direct.update(
                {
                    _id:               conversationId,
                    'participants.id': this.userId
                },
                {
                    $set: {
                        'participants.$.readAt':    new Date(),
                        'participants.$.unreadCount': 0
                    }
                }
            );
        }
    } )
};

export default ConversationMethods;
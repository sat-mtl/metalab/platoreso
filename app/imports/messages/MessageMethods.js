"use strict";

import Logger from "meteor/metalab:logger/Logger";
import { ValidatedMethod } from "meteor/mdg:validated-method";
import MessageSchema from "./model/MessageSchema";
import MessageCollection from "./MessageCollection";

const log = Logger( "message-methods" );

const MessageMethods = {

    /**
     * Send a message
     *
     * @param {String} conversationId
     * @param {String} message
     * @returns {String} messageId
     */
    send: new ValidatedMethod( {
        name:     'pr/message/send',
        validate: MessageSchema.pick( [
            'conversation',
            'content',
            'hasImage'
        ] ).validator( { clean: true } ),
        run( { conversation, content, hasImage } ) {

            // MESSAGE
            const msg = {
                author:    this.userId,
                createdAt: new Date(),
                           conversation,
                           content,
                           hasImage
            };

            // SECURITY

            Security.can( this.userId ).insert( msg ).for( MessageCollection ).throw();

            // UPDATE

            log.silly( "send - Sending message", this.userId, content );
            return MessageCollection.insert( msg );
        }
    } ),

    /**
     * Remove a message
     *
     * @param {String} conversationId
     * @param {String} message
     * @returns {String} messageId
     */
    remove: new ValidatedMethod( {
        name:     'pr/message/remove',
        validate: new SimpleSchema( {
            messageId: {
                type: String
            }
        } ).validator( { clean: true } ),
        run( { messageId } ) {
console.log(messageId);
            // SECURITY

            Security.can( this.userId ).remove( messageId ).for( MessageCollection ).throw();

            // UPDATE

            log.silly( "remove - Removing message", this.userId, messageId );
            return MessageCollection.remove( {_id: messageId } );
        }
    } ),
};

export default MessageMethods;
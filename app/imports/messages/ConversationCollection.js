"use strict";

import CollectionHelpers from "/imports/collections/CollectionHelpers";
import Conversation from "./model/Conversation";
import ConversationSchema from "./model/ConversationSchema";

class ConversationCollection extends Mongo.Collection {
    constructor( name, options = {} ) {
        // Change transform before calling super, this is the only way to set it
        // Also, this won't suffer from module loading as it is a callback
        options.transform = conversation => new Conversation( conversation );

        super( name, options );

        this.entityName = "conversation";

        /**
         * Schema
         */

        this.attachSchema( ConversationSchema );

        /**
         * Indexes
         */

        if ( Meteor.isServer ) {

        }

        /**
         * Security
         */

        this.deny( {
            insert: () => true,
            update: () => true,
            remove: () => true
        } );

        /**
         * Behaviors
         */

        this.attachBehaviour( 'timestampable' );

        // The rest goes into a Meteor.startup because since we create the collection instance
        // right away when exporting from this module, we end up executing this constructor
        // before the imported modules are available (when there are circular dependencies).
        // This way we are sure the app is loaded when we execute the following code.
        Meteor.startup( () => {
            /**
             * Registration
             * TODO: Get rid of that system
             */

            CollectionHelpers.registerCollection( this.entityName, this, Conversation );

        } );
    }

    addParticipant( query = {}, userId ) {
        query = _.defaults( {
            'participants.id': { $nin: [userId] }
        }, query );
        this.update( query, { $push: { participants: { id: userId, unreadCount: 0, readAt: new Date() } } }, { multi: true } );
    }

    removeParticipant( query = {}, userId ) {
        query = _.defaults( {
            'participants.id': { $in: [userId] }
        }, query );
        this.update( query, { $pull: { participants: { id: userId } } }, { multi: true } );
    }

    /**
     * Get conversation default fields
     *
     * @param userId
     */
    getDefaultFields( userId ) {
        return _.defaults(
            {}, // Important, otherwise we write into default fields
            ConversationCollection.defaultFields,
            CollectionHelpers.getDefaultFields( this, userId )
        );
    };

}

ConversationCollection.defaultFields = {
    type:         1,
    entity:       1,
    slug:         1,
    name:         1,
    topic:        1,
    participants: 1
};

export default new ConversationCollection( "conversations" );
"use strict";

import prune from "underscore.string/prune";
import logger from "meteor/metalab:logger/Logger";
import notify, {canNotify} from "/imports/utils/notify";
import UserHelpers from "/imports/accounts/UserHelpers";
import ConversationType from "/imports/messages/ConversationType";
import MessageCollection from "/imports/messages/MessageCollection";

const log = logger( "messages-browser-notifications" );

Meteor.startup( () => {
    // Automatic subscription to notifications on the client-side
    log.info( 'Subscribing to last messages' );
    Meteor.subscribe( 'messages/unread', () => {
        if ( !canNotify() ) {
            return;
        }
        // Make it autorun in a tracker so that we can reset when user logs in
        Tracker.autorun( () => {
            let initializing          = true;

            // Meteor.userId() is outside the observer because of the Tracker
            const userId              = Meteor.userId();
            const focusedConversation = Session.get( 'conversation.focused' );

            MessageCollection.find( {}, { sort: { createdAt: -1 } } ).observe( {
                addedAt( message, atIndex ) {
                    if ( initializing || atIndex != 0 || message.author == userId ) {
                        return;
                    }

                    const conversation = message.getConversation();
                    if ( conversation._id == focusedConversation ) {
                        // Don't notify for current conversation
                        return;
                    }

                    let title, body;
                    const author = message.getAuthor();
                    switch ( conversation.type ) {
                        case ConversationType.group:
                            title = __( "New message in __conversation__", {
                                conversation: `#${conversation.slug}`
                            } );
                            body  = __( "__user__: __message__", {
                                user:    UserHelpers.getDisplayName( author ),
                                message: prune( message.content, 140 )
                            } );
                            break;
                        default:
                            return;
                            break;
                    }

                    notify( title, `/messages/${conversation._id}`, {
                        body: body,
                        tag:  conversation._id,
                        icon: "/images/platform-avatar.png"
                    } );
                }
            } );
            initializing = false;
        } );
    } );
} );
"use strict";

import CollectionHelpers from "/imports/collections/CollectionHelpers";
import ConversationCollection from "../../ConversationCollection";
import MessageCollection from "../../MessageCollection";

/**
 * Messages
 */
Meteor.publishComposite( "messages/unread", function () {
    return {
        find() {
            return ConversationCollection.find( { 'participants.id': this.userId }, {
                fields: ConversationCollection.getDefaultFields( this.userId )
            } );
        },
        children: [
            {
                // Last message
                find( conversation ) {
                    return MessageCollection.find(
                        {
                            conversation: conversation._id
                        },
                        {
                            fields: MessageCollection.getDefaultFields( this.userId ),
                            sort:   { createdAt: -1 },
                            limit:  1
                        }
                    );
                },
                children: [
                    { find: message => message.findAuthor() }
                ]
            }
        ]
    }
} );
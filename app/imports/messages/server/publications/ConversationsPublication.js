"use strict";

import {Counts} from "meteor/tmeasday:publish-counts";
import ConversationCollection from "../../ConversationCollection";
import ConversationType from "../../ConversationType";
import GroupImagesCollection from "/imports/groups/GroupImagesCollection";

/**
 * Conversations
 */
Meteor.publishComposite( "conversations", function () {
    return {
        find() {
            return ConversationCollection.find(
                {
                    'participants.id': this.userId
                },
                {
                    fields: ConversationCollection.getDefaultFields( this.userId )
                } );
        },
        children: [
            {
                find( conversation ) {
                    switch ( conversation.type ) {
                        case ConversationType.group:
                            return GroupImagesCollection.find( { owners: conversation.entity } );
                            break;
                    }
                }
            },
            { find: conversation => conversation.findParticipants() }
        ]
    };
} );
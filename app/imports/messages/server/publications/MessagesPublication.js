"use strict";

import ConversationCollection from "../../ConversationCollection";
import MessageCollection from "../../MessageCollection";

/**
 * Messages
 */
Meteor.publishComposite( "messages", function ( conversationId, startDate = null ) {
    check( conversationId, String );
    check( startDate, Match.OneOf( Date, null ) );

    startDate = startDate || new Date();

    return {
        find() {
            return ConversationCollection.find( { _id: conversationId, 'participants.id': this.userId }, {
                fields: ConversationCollection.getDefaultFields( this.userId )
            } );
        },
        children: [
            {
                // The rest
                find( conversation ) {
                    return MessageCollection.find(
                        {
                            conversation: conversation._id,
                            createdAt:    { $gte: startDate }
                        },
                        {
                            fields: MessageCollection.getDefaultFields( this.userId ),
                            sort:   { createdAt: -1 }
                        }
                    );
                },
                children: [
                    { find: message => message.findAuthor() },
                    { find: message => message.hasImage && message.findImage() }
                ]
            },
            {
                // Last 10 messages
                find( conversation ) {
                    return MessageCollection.find(
                        {
                            conversation: conversation._id,
                            createdAt:    { $lte: startDate }
                        },
                        {
                            fields: MessageCollection.getDefaultFields( this.userId ),
                            sort:   { createdAt: -1 },
                            limit:  20
                        }
                    );
                },
                children: [
                    { find: message => message.findAuthor() },
                    { find: message => message.hasImage && message.findImage() }
                ]
            }
        ]
    }
} );
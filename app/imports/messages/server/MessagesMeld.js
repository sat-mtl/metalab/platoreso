"use strict";

import Logger from "meteor/metalab:logger/Logger";
import AccountsMeldHelper from "/imports/accounts/server/AccountsMeldHelper";
import ConversationCollection from "../ConversationCollection";
import MessageCollection from "../MessageCollection";

const log = Logger( "messages-meld" );

/**
 * Message Melding
 */
AccountsMeldHelper.onMeld( ( src_user_id, dst_user_id ) => {
    log.debug( `Melding messages for user ${src_user_id} into ${dst_user_id}` );

    ConversationCollection.update( { 'participants.id': src_user_id }, { $set: { 'participants.$.id': dst_user_id } }, { multi: true } );

    MessageCollection.update( { 'author': src_user_id }, { $set: { 'author': dst_user_id } }, { multi: true } );
    MessageCollection.update( { 'createdBy': src_user_id }, { $set: { 'createdBy': dst_user_id } }, { multi: true } );
    MessageCollection.update( { 'updatedBy': src_user_id }, { $set: { 'updatedBy': dst_user_id } }, { multi: true } );
} );
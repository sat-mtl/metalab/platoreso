"use strict";

import Logger from "meteor/metalab:logger/Logger";
import UserCollection from "/imports/accounts/UserCollection";
import UserHelpers from "/imports/accounts/UserHelpers";
import ConversationCollection from "/imports/messages/ConversationCollection";
import MessageCollection from "../MessageCollection";
import MessageImagesCollection from "../MessageImagesCollection";

const log = Logger( "messages-hooks" );

/**
 * Conversation Removed
 */
ConversationCollection.after.remove( function ( userId, conversation ) {
    // Defer, so that methods can return without waiting
    Meteor.defer( () => {
        log.debug( `Conversation (${conversation._id}) removed, cleaning up relations` );

        // Remove associated messages
        MessageCollection.remove( { conversation: conversation._id } );

    } );
} );

/**
 * Message Added
 */
MessageCollection.after.insert( function ( userId, message ) {
    // Defer, so that methods can return without waiting
    Meteor.defer( () => {
        log.silly( `Message (${message._id}) added, counting unread messages` );

        const conversation = ConversationCollection.findOne(
            {
                _id: message.conversation
            },
            {
                fields: {
                    _id:          1,
                    participants: 1
                }
            }
        );

        conversation.participants.forEach( participant => {
            let modifier;
            if ( participant.id == userId ) {
                // Updating the message author, assume they are reading while typing and change its readAt also
                modifier = {
                    $set: {
                        'participants.$.readAt': new Date(),
                        'participants.$.unreadCount': 0
                    }
                }
            } else {
                // Updating someone else, recalculate
                const unread = MessageCollection.find( {
                    conversation: conversation._id,
                    createdAt:    { $gt: participant.readAt }
                }, { fields: { _id: 1 } } ).count();
                modifier = {
                    $set: {
                        'participants.$.unreadCount': unread
                    }
                };
            }

            ConversationCollection.direct.update(
                {
                    _id:               conversation._id,
                    'participants.id': participant.id
                },
                modifier
            );
        } );

    } );
} );

/**
 * Message removed
 */
MessageCollection.after.remove( function( userId, message ) {
    // Defer, so that methods can return without waiting
    Meteor.defer( () => {
        log.debug( `Message (${message._id}) removed, cleaning up relations` );

        // Unlink images
        MessageImagesCollection.unlinkOwner( message._id );
    });
});

/**
 * User Removed
 */
UserCollection.after.remove( function ( userId, user ) {
    // Defer, so that methods can return without waiting
    Meteor.defer( () => {
        log.debug( `User "${UserHelpers.getDisplayName( user )}" (${user._id}) removed, cleaning up relations` );

        ConversationCollection.removeParticipant( null, user._id );

        //TODO: What do we do with messages
    } );
} );
"use strict";

import UserHelpers from "/imports/accounts/UserHelpers";
import {RoleGroups} from "/imports/accounts/Roles";
import ConversationCollection from "../ConversationCollection";
import MessageCollection from "../MessageCollection";
import MessageImagesCollection from "../MessageImagesCollection";

/**
 * Deny if comment is not for the provided entity type
 */
Security.defineMethod( "ifMessageAuthorIsParticipant", {
    fetch:     [],
    transform: null,
    allow:     function ( type, arg, userId, message, fields, modifier ) {
        return ConversationCollection.findOne( { _id: message.conversation, 'participants.id': userId } ) != null;
    }
} );

MessageCollection.permit( ['insert'] )
    .ifLoggedIn()
    .ifMessageAuthorIsParticipant();

// IMAGES

/**
 * Deny if user cannot read the message
 */
Security.defineMethod( "ifCanAccessOwnerMessageConversation", {
    fetch:     [],
    transform: null,
    allow:     function ( type, arg, userId, doc, fields, modifier ) {
        return _.some( doc.owners, owner => {
            const message = MessageCollection.findOne( { _id: owner }, {
                fields:    { conversation: 1 },
                transform: null
            } );
            if ( !message ) {
                return false;
            }
            const conversation = ConversationCollection.findOne( {
                _id:               message.conversation,
                'participants.id': userId
            }, { fields: { _id: 1 }, transform: null } );
            return conversation != null;
        } );
    }
} );

/**
 * Security Rules
 */

// Allow anyone with access to group to download images
Security.permit( ['download'] )
    .collections( [MessageImagesCollection] )
    .ifCanAccessOwnerMessageConversation()
    .allowInClientCode();

Security.permit( ['insert'] )
    .collections( [MessageImagesCollection] )
    .ifLoggedIn()
    .ifCanAccessOwnerMessageConversation()
    .allowInClientCode();


// MODERATION

MessageCollection
    .permit( ['update'] )
    .onlyProps( ['moderated', 'reportCount', 'reportedBy'] )
    .ifLoggedIn()
    .ifHasRole( RoleGroups.moderators );

MessageCollection
    .permit( ['remove'] )
    .ifLoggedIn()
    .ifHasRole( RoleGroups.moderators );
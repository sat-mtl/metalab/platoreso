"use strict";

import { SimpleSchema } from "meteor/aldeed:simple-schema";

/**
 * Message Schema Base
 * @type {SimpleSchema}
 */
const MessageSchema = new SimpleSchema( {
    conversation: {
        type:  String,
        index: true
    },
    author:       {
        type: String
    },
    content:      {
        type:     String,
        optional: true,
        custom:   function () {
            var shouldBeRequired = this.field( 'hasImage' ).value == false;

            if ( shouldBeRequired ) {
                // inserts
                if ( !this.operator ) {
                    if ( !this.isSet || this.value === null || this.value === "" ) {
                        return "required";
                    }
                }

                // updates
                else if ( this.isSet ) {
                    if ( this.operator === "$set" && this.value === null || this.value === "" ) {
                        return "required";
                    }
                    if ( this.operator === "$unset" ) {
                        return "required";
                    }
                    if ( this.operator === "$rename" ) {
                        return "required";
                    }
                }
            }
        }
    },
    hasImage:     {
        type:         Boolean,
        optional:     true,
        defaultValue: false
    }
} );

export default MessageSchema;
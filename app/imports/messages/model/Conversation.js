"use strict";

import md, { mdInline } from "/imports/utils/markdown";
import UserCollection from "/imports/accounts/UserCollection";
import MessageCollection from "../MessageCollection";
import ConversationCollection from "../ConversationCollection";

/**
 * Conversation Model
 * @constructor
 */
export default class Conversation {

    /**
     * @constructor
     * @param doc
     */
    constructor( doc ) {
        _.extend( this, doc );
    }

    /**
     * Get message as rendered markdown
     *
     * @getter
     * @returns {String}
     */
    get topicMarkdown() {
        if ( !this._topicMarkdown ) {
            this._topicMarkdown = mdInline( this.topic || '' );
        }
        return this._topicMarkdown;
    }

    /**
     * Find participants cursor for this conversation
     *
     * @returns {Mongo.Cursor}
     */
    findParticipants() {
        return UserCollection.find( { _id: { $in: this.participants.map( participant => participant.id ) } }, {
            fields: {
                _id:                 1,
                points:              1,
                'profile.firstName': 1,
                'profile.lastName':  1,
                'profile.avatarUrl': 1
            }
        } );
    }

    /**
     * Get the participants for this conversation
     *
     * @returns {Array<Meteor.User>}
     */
    getParticipants() {
        return this.findParticipants().fetch();
    }

    get participant() {
        return this.participants.find( participant => participant.id == Meteor.userId() );
    }

    get messageCount() {
        return MessageCollection.find( { conversation: this._id } ).count();
    }

    findMessages( options = {} ) {
        return MessageCollection.find( { conversation: this._id }, options );
    }

    getMessages( options = {} ) {
        return this.findMessages( options ).fetch()
    }

    findUnreadMessages( options = {} ) {
        return MessageCollection.find( { conversation: this._id, createdAt: { $gt: this.participant.reatAt } }, options );
    }

    getUnreadMessages( options = {} ) {
        return this.findUnreadMessages( options ).fetch()
    }

    clear() {
        this.participants.forEach( participant => {
            ConversationCollection.direct.update(
                {
                    _id:               this._id,
                    'participants.id': participant.id
                },
                {
                    $set: {
                        'participants.$.unreadCount': 0
                    }
                }
            );
        } );

        MessageCollection.remove( { conversation: this._id } );
    }

};
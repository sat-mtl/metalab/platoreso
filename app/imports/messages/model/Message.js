"use strict";

import prune from "underscore.string/prune";
import md, { mdInline } from "/imports/utils/markdown";
import UserCollection from "/imports/accounts/UserCollection";
import ConversationCollection from "../ConversationCollection";
import MessageImagesCollection from "../MessageImagesCollection";

/**
 * Message Model
 * @constructor
 */
export default class Message {

    /**
     * @constructor
     * @param doc
     */
    constructor( doc ) {
        _.extend( this, doc );
    }

    /**
     * Get message as rendered markdown
     *
     * @getter
     * @returns {String}
     */
    get contentMarkdown() {
        if ( !this._contentMarkdown ) {
            this._contentMarkdown = md( this.content || '' );
        }
        return this._contentMarkdown;
    }

    /**
     * Get an excerpt as rendered markdown
     * We have to prune BEFORE markdown, otherwise we run the risk of having broken markup.
     *
     * @parms {Number} length Number of characters in the excerpt
     * @returns {String}
     */
    excerptMarkdown( length = 140 ) {
        if ( !this._excerptMarkdown ) {
            this._excerptMarkdown = {};
        }
        if ( !this._excerptMarkdown[length] ) {
            this._excerptMarkdown[length] = mdInline( prune( this.content || '', length ) );
        }
        return this._excerptMarkdown[length];
    }

    /**
     * Find conversation cursor for this message
     *
     * @param {Object} options
     * @returns {Mongo.Cursor}
     */
    findConversation( options = {} ) {
        return ConversationCollection.find( { _id: this.conversation }, options );
    }

    /**
     * Get the conversation for this message
     *
     * @param {Object} options
     * @returns {Conversation}
     */
    getConversation( options = {} ) {
        return ConversationCollection.findOne( { _id: this.conversation }, options );
    }

    /**
     * Find author cursor for this message
     *
     * @returns {Mongo.Cursor}
     */
    findAuthor() {
        return UserCollection.find( { _id: this.author }, {
            fields: {
                _id:                 1,
                points:              1,
                'profile.firstName': 1,
                'profile.lastName':  1,
                'profile.avatarUrl': 1
            }
        } );
    }

    /**
     * Get the author for this message
     *
     * @returns {Meteor.User}
     */
    getAuthor() {
        return UserCollection.findOne( { _id: this.author }, {
            fields: {
                _id:                 1,
                points:              1,
                'profile.firstName': 1,
                'profile.lastName':  1,
                'profile.avatarUrl': 1
            }
        } );
    }

    /**
     * Find image cursor for this message
     *
     * @param options
     * @returns {*}
     */
    findImage( options = {} ) {
        return MessageImagesCollection.find( {
            'owners': this._id
        }, options );
    }

    /**
     * Find the image for this image
     *
     * @param options
     * @returns {*}
     */
    getImage( options = {} ) {
        return MessageImagesCollection.findOne( {
            'owners': this._id
        }, options );
    }

};
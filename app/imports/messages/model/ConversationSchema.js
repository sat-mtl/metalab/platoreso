"use strict";

import {SimpleSchema} from "meteor/aldeed:simple-schema";
import {ConversationTypes} from "../ConversationType";

/**
 * Participant Schema
 * @type {SimpleSchema}
 */
const ParticipantSchema = new SimpleSchema( {
    id:          {
        type: String
    },
    readAt:    {
        type:     Date,
        optional: true
    },
    unreadCount: {
        type:         Number,
        optional:     true,
        defaultValue: 0
    }
} );

/**
 * Conversation Schema
 * @type {SimpleSchema}
 */
const ConversationSchema = new SimpleSchema( {

    /**
     * Type of conversation
     */
    type: {
        type:          String,
        allowedValues: ConversationTypes
    },

    /**
     * Entity Id, if attached to an entity
     * Type should be used to determine the entity type
     */
    entity: {
        type:     String,
        optional: true
    },

    /**
     * Conversation slug
     */
    slug: {
        type: String
    },

    /**
     * Conversation name
     */
    name: {
        type: String
    },

    /**
     * Conversation name
     */
    topic: {
        type:     String,
        optional: true
    },

    /**
     * List of participants in the conversation
     */
    participants: {
        type:         [ParticipantSchema],
        optional:     true,
        defaultValue: []
    }
} );

export default ConversationSchema;
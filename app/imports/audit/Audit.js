"use strict";

import { Subject } from "rxjs/Subject";

/**
 * Audit messaging system
 *
 * It is build around rx.js and is wrapped in this class mainly to support
 * Meteor.defer() being used automatically instead of having to use it
 * in every instance when we want to send an audit message.
 */
export class Audit {

    constructor() {
        this._subject = new Subject();
    }

    next( message ) {
        if ( Meteor.isServer ) {
            Meteor.defer( () => this._subject.next( message ) );
        } else {
            this._subject.next( message );
        }
    }

    get subject() {
        return this._subject;
    }

}

export default new Audit();
"use strict";

import UserCollection from "../UserCollection";
import UserHelpers from "../UserHelpers";

/**
 * Service class used by Meteor methods
 * in order to be able to unit test the methods in isolation.
 *
 * Even though the methods are static Meteor will bind "this" when calling the method.
 * Unit tests should be able to deal with that on a case-by-case basis.
 */
export default class UserServicesServer {

    /**
     * Send an email validation message to either the logged in user or the passed user if allowed
     *
     * @param {String} [sendToUserId]
     */
    static sendValidationEmail( sendToUserId = null ) {
        check( sendToUserId, Match.OneOf( String, null ) );
        if ( !this.userId || ( sendToUserId && this.userId != sendToUserId && !UserHelpers.isSuper( this.userId ) ) ) {
            throw new Meteor.Error( 'unauthorized' );
        }
        Accounts.sendVerificationEmail( sendToUserId ? sendToUserId : this.userId );
    }

    /**
     * Set password
     * Can only be done for the logged in user
     *
     * @param {String} password
     */
    static setPassword( password ) {
        check( password, String );

        if ( !this.userId ) {
            throw new Meteor.Error( 401, "Must be logged in" );
        }

        const user = UserCollection.findOne( { _id: this.userId }, { fields: { services: 1 } } );
        if ( !user ) {
            throw new Meteor.Error( 403, "User not found" );
        }

        if ( user.services && user.services.password &&
            (user.services.password.bcrypt || user.services.password.srp) ) {
            throw new Meteor.Error( 403, "User already has a password set" );
        }

        Accounts.setPassword( this.userId, password, { logout: false } );
    }
}
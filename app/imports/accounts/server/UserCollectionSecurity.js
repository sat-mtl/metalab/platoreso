"use strict";

import UserHelpers from "../UserHelpers";

/**
 * Helper Methods
 */

Security.defineMethod("ifEnabled", {
    fetch: [],
    transform: null,
    deny: function (type, arg, userId, doc) {
        return !Meteor.user().enabled;
    }
});

Security.defineMethod("ifVerified", {
    fetch: [],
    transform: null,
    deny: function (type, arg, userId, doc) {
        return !UserHelpers.hasVerifiedEmail(Meteor.user());
    }
});

Security.defineMethod("ifIsCurrentUser", {
    fetch: [],
    transform: null,
    deny: function (type, arg, userId, doc) {
        return userId !== doc._id;
    }
});

Security.defineMethod("ifIsNotCurrentUser", {
    fetch: [],
    transform: null,
    deny: function (type, arg, userId, doc) {
        return userId === doc._id;
    }
});

/*Security.defineMethod("ifAtLeastOneAdminRemains", {
    fetch: [],
    transform: null,
    deny: function (type, arg, userId, doc) {
        return Meteor.users.find({'roles':'admin'}).count() > 1;
    }
});*/

/* Negative version of the built-in ifHasRole */
/*if (Package && Package["alanning:roles"]) {
    var Roles = Package["alanning:roles"].Roles;
    Security.defineMethod("ifNotHasRole", {
        fetch: [],
        transform: null,
        deny: function (type, arg, userId) {
            if (!arg) {
                throw new Error('ifHasRole security rule method requires an argument');
            }
            if (arg.role) {
                return Roles.userIsInRole(userId, arg.role, arg.group);
            } else {
                return Roles.userIsInRole(userId, arg);
            }
        }
    });
}*/

/**
 * Security Rules
 */

// Only allow users to modify their profiles... nothing else,
// otherwise they could potentially change their role
// NOTE: This is only used for the language as of now
Security.permit(['update'])
    .collections([ Meteor.users ])
    .ifLoggedIn()
    .ifEnabled()
    .ifIsCurrentUser()
    .onlyProps(['profile'])
    .allowInClientCode();

// Non-admin removing themselves
/*Security.permit(['remove'])
    .collections([ Meteor.users ])
    .ifLoggedIn()
    .ifEnabled()
    .ifNotHasRole('admin')
    .ifIsCurrentUser()
    .apply();*/

// Admins can modify everything
/*Security.permit(['update'])
    .collections([ Meteor.users ])
    .ifLoggedIn()
    .ifEnabled()
    .ifHasRole(RoleList.admin)
    .apply();*/

// Admins can only remove themselves if another admin remains
/*
Security.permit(['remove'])
    .collections([ Meteor.users ])
    .ifLoggedIn()
    .ifEnabled()
    .ifHasRole(RoleList.admin)
    .ifIsNotCurrentUser()
    .apply();*/

"use strict";

import UserCollection from "../UserCollection";

/**
 * Watch for users getting disabled and log them out
 * This will auto-clean up on startup too.
 */
UserCollection.find( { enabled: false }, { fields: { _id: 1 } } ).observeChanges( {
    added( id, user ) {
        UserCollection.update( { _id: id }, { $set: { "services.resume.loginTokens": [] } } );
    }
} );
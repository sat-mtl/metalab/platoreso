"use strict";

import DefaultProfile from "./DefaultProfile";
import UserCollection from "../UserCollection";
import UserHelpers from "../UserHelpers";
import { RoleList } from "../Roles";

/**
 * Account Callbacks
 * Defined as static methods here as to be able to unit test them
 */
export default class AccountsCallbacks {

    /**
     * Create User Callback
     *
     * Builds a profile based on the information returned by the different
     * services providers.
     */
    static onCreateUser( options, user ) {

        // Start with the default behavior
        user.profile = _.defaults( options.profile || {}, DefaultProfile );

        // Per-service initialization
        if ( user.services ) {
            if ( user.services.password ) {
                // Password users need to fill in a profile
                user.completed = false;

            } else if ( user.services.facebook ) {
                // We already have a profile & email
                user.completed         = true;
                user.emails            = [
                    {
                        address:  user.services.facebook.email,
                        verified: true
                    }
                ];
                user.profile.firstName = user.services.facebook.first_name || user.profile.name;
                delete user.profile.name;
                user.profile.lastName  = user.services.facebook.last_name || '';
                user.profile.avatarUrl = 'https://graph.facebook.com/' + user.services.facebook.id + '/picture/?type=large';
                user.profile.language  = user.services.facebook.locale.split( '_' )[0];

            } else if ( user.services.twitter ) {
                // Skip email validation as it slows the process of registering with twitter
                user.completed         = true;
                user.profile.firstName = user.profile.name || user.services.twitter.screenName;
                delete user.profile.name;
                user.profile.lastName  = '';
                user.profile.avatarUrl = user.services.twitter.profile_image_url_https;
                user.profile.language  = user.services.twitter.lang;

            } else if ( user.services.google ) {
                // We already have a profile & email
                user.completed         = true;
                user.emails            = [
                    {
                        address:  user.services.google.email,
                        verified: user.services.google.verified_email
                    }
                ];
                user.profile.firstName = user.services.google.given_name || user.profile.name;
                delete user.profile.name;
                user.profile.lastName  = user.services.google.family_name || '';
                user.profile.avatarUrl = user.services.google.picture;
                user.profile.language  = user.services.google.locale ? user.services.google.locale.split( '-' )[0] : 'en';

            } else if ( user.services.linkedin ) {
                // We already have a profile & email
                user.completed         = true;
                user.emails            = [
                    {
                        address:  user.services.linkedin.emailAddress,
                        verified: true
                    }
                ];
                user.profile.firstName = user.services.linkedin.firstName || user.profile.name;
                delete user.profile.name;
                user.profile.lastName  = user.services.linkedin.lastName || '';
                user.profile.avatarUrl = user.services.linkedin.pictureUrl;
                user.profile.biography = user.services.linkedin.headline;
            }
        } else {
            user.completed = UserHelpers.isProfileCompleted( user );
        }

        // Start enabled, they didn't do anything wrong (yet)
        user.enabled = true;
        user.unsubscribeToken = Random.id();

        // Check if we are the first user
        const firstUser = UserCollection.findOne() == null;

        // Set roles
        user.roles = {
            [Roles.GLOBAL_GROUP]: [firstUser ? RoleList.admin : RoleList.user]
        };

        return user;
    }

    /**
     * Login Attempt Validation
     * This is where whe check if the user has the right to login (is not disabled)
     */
    static validateLoginAttempt( attempt ) {

        // Don't bother if it was already rejected before
        if ( !attempt.allowed ) {
            return false;
        }

        // Reject disabled users
        if ( !attempt.user.enabled ) {
            throw new Meteor.Error( "account disabled" );
        } else {
            return true;
        }
    }
}

Accounts.onCreateUser( AccountsCallbacks.onCreateUser );
Accounts.validateLoginAttempt( AccountsCallbacks.validateLoginAttempt );
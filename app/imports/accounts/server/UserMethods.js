"use strict";

import wrapServiceMethod from "/imports/utils/wrapServiceMethod";
import UserServicesServer from "./UserServices";
import UserMethods from "../UserMethods";

// Server only methods
UserMethods.sendValidationEmail = wrapServiceMethod('pr/accounts/sendValidationEmail', UserServicesServer.sendValidationEmail );
UserMethods.setPassword = wrapServiceMethod('pr/accounts/setPassword', UserServicesServer.setPassword );
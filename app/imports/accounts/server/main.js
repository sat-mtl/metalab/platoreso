"use strict";

// General
import "./AccountsCallbacks";
import "./AccountsMeldHelper";

// User
import "./UserCollectionSecurity";
import "./UserObservers";
import "./UserHooks";

// Avatar
import "./AvatarMeld";
import "./AvatarCollectionSecurity";
import "./AvatarUpdater";

// Publications
import "./publications/AdminAvatarEdit";
import "./publications/AdminUserEdit";
import "./publications/AdminUserList";
import "./publications/AvatarEdit";
import "./publications/UserList";
import "./publications/UserCurrent";
import "./publications/UserEdit";
import "./publications/UserPublic";
import "./moderation/UserModerationReportListPublication";

// Methods
import "./UserMethods";
"use strict";

import UserCollection from "../UserCollection";
import AvatarCollection from "../AvatarCollection";

/**
 * Avatar Thumb Store Stored handler
 * Update the owner's avatar url when the thumbnail is available
 */
AvatarCollection.storesLookup.avatar_thumb.on("stored", Meteor.bindEnvironment(function(store, file) {
    UserCollection.update( {
        _id: file.owners[0]
    }, {
        $set: {
            'profile.avatarUrl': file.url( {
                store: 'avatar_thumb',
                // Return a broken URL if the image is not stored yet
                // It should be stored since we are in a stored callback
                // But it kept returning null here unless I used this option
                brokenIsFine: true
            } )
        }
    } );
}));


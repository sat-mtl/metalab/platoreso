"use strict";

import UserCollection from "../UserCollection";
import AvatarCollection from "../AvatarCollection";
import { RoleList, RoleGroups } from "../Roles";

/**
 * Helper Methods
 */

Security.defineMethod( "ifIsCurrentUserPicture", {
    fetch:     [],
    transform: null,
    deny:      ( type, arg, userId, doc ) => {
        return userId !== doc.owners[0];
    }
} );

/**
 * Security Rules
 */

Security.permit( ['download'] )
        .collections( [AvatarCollection] )
        .allowInClientCode();

Security.permit( ['insert', 'update', 'remove'] )
        .collections( [AvatarCollection] )
        .ifLoggedIn()
        .ifIsCurrentUserPicture()
        .allowInClientCode();

Security.permit( ['insert', 'update', 'remove'] )
        .collections( [AvatarCollection] )
        .ifLoggedIn()
        .ifHasRole( RoleList.admin )
        .allowInClientCode();


// MODERATION

UserCollection
    .permit( ['update'] )
    .onlyProps( ['moderated', 'reportCount', 'reportedBy'] )
    .ifLoggedIn()
    .ifHasRole( RoleGroups.moderators );
"use strict";

import { chai, assert, expect } from 'meteor/practicalmeteor:chai';
import { sinon, spies, stubs } from 'meteor/practicalmeteor:sinon';

import AccountsMeldHelper from "./AccountsMeldHelper";

if ( Meteor.isServer ) {
    describe( 'accounts/AccountsMeldHelper', function () {

        let sandbox;

        beforeEach( () => {
            sandbox = sinon.sandbox.create();
        } );

        afterEach( () => {
            sandbox.restore();
        } );

        describe( '_meldUserCallback', function () {

            describe( 'gamification points', () => {

                it( 'should meld gamification points', () => {
                    const src = { points: 1 };
                    const dst = { points: 2 };

                    expect( AccountsMeldHelper._meldUserCallback( src, dst ) ).to.eql( {
                        points:           3,
                        pointsPerProject: {}
                    } );
                } );

                it( 'should meld gamification points (no source points)', () => {
                    const src = {};
                    const dst = {
                        points: 2
                    };

                    expect( AccountsMeldHelper._meldUserCallback( src, dst ) ).to.eql( {
                        points:           2,
                        pointsPerProject: {}
                    } );
                } );

                it( 'should meld gamification points (no destination points)', () => {
                    const src = {
                        points: 1
                    };
                    const dst = {};

                    expect( AccountsMeldHelper._meldUserCallback( src, dst ) ).to.eql( {
                        points:           1,
                        pointsPerProject: {}
                    } );
                } );

                it( 'should meld gamification points (no points)', () => {
                    const src = {};
                    const dst = {};

                    expect( AccountsMeldHelper._meldUserCallback( src, dst ) ).to.eql( {
                        points:           0,
                        pointsPerProject: {}
                    } );
                } );

                it( 'should meld project points', () => {
                    const src = {
                        pointsPerProject: {
                            'project1': 1,
                            'project2': 2,
                        }
                    };
                    const dst = {
                        pointsPerProject: {
                            'project1': 3,
                            'project3': 5
                        }
                    };

                    expect( AccountsMeldHelper._meldUserCallback( src, dst ) ).to.eql( {
                        points:           0,
                        pointsPerProject: {
                            'project1': 4,
                            'project2': 2,
                            'project3': 5
                        }
                    } );
                } );

                it( 'should meld project points (no source project)', () => {
                    const src = {};
                    const dst = {
                        pointsPerProject: {
                            'project1': 3,
                            'project3': 4
                        }
                    };

                    expect( AccountsMeldHelper._meldUserCallback( src, dst ) ).to.eql( {
                        points:           0,
                        pointsPerProject: {
                            'project1': 3,
                            'project3': 4
                        }
                    } );
                } );

                it( 'should meld project points (no destination project)', () => {
                    const src = {
                        pointsPerProject: {
                            'project1': 1,
                            'project2': 2,
                        }
                    };
                    const dst = {};

                    expect( AccountsMeldHelper._meldUserCallback( src, dst ) ).to.eql( {
                        points:           0,
                        pointsPerProject: {
                            'project1': 1,
                            'project2': 2
                        }
                    } );
                } );

            } );
        } );
    } );
}

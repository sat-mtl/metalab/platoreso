"use strict";

import AvatarCollection from "../AvatarCollection";
import AccountsMeldHelper from "./AccountsMeldHelper";

/**
 * Avatar Melding
 */
AccountsMeldHelper.onMeld( ( src_user_id, dst_user_id ) => {
    AvatarCollection.update( { owners: src_user_id }, { $set: { owners: [dst_user_id] } }, { multi: true } );
} );
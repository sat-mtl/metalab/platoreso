"use strict";

import UserHelpers from "../../UserHelpers";
import UserCollection from "../../UserCollection";
import {RoleGroups} from "../../Roles";
import {GroupRoles} from "../../../groups/GroupRoles";

/**
 * User List (General)
 * This provides a list of users for display/search with emails and basic profile.
 * It is only working for global experts and group experts.
 * In the case of group experts, it will only return users which are in the groups where
 * the requesting user is an expert.
 */
Meteor.publish( "user/list", function () {

    if ( !this.userId ) {
        return this.ready();
    }

    let query = {};

    if ( !UserHelpers.isModerator( this.userId ) ) {
        // Not a global expert, so filter by group permissions
        const groups = UserHelpers.getGroupsForUser( this.userId, RoleGroups.moderators );
        if ( !groups || !groups.length ) {
            return this.ready();
        }
        
        query.$or = groups.map( group => ({ [`roles.${group}`]: { $in: GroupRoles } }) );
    }

    return UserCollection.find( query, {
        fields: {
            '_id':               1,
            'roles':             1, // Only allowed to return all roles as this is for a group manager
            'emails':            1,
            'profile.firstName': 1,
            'profile.lastName':  1,
            'profile.avatarUrl': 1
        }
    } );

} );
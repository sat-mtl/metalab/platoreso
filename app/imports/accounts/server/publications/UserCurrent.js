"use strict";

import UserCollection from "../../UserCollection";

/**
 * Current User
 * Provides more information than Meteor gives us by default
 */
Meteor.publish( "user/current", function () {
    if ( !this.userId ) {
        return this.ready();
    }

    return UserCollection.find( { _id: this.userId }, {
        fields: {
            enabled:        1,
            completed:      1,
            acceptedTerms:  1,
            points:         1,
            recentProjects: 1
        }
    } );
} );
"use strict";

import UserCollection from "../../UserCollection";

/**
 * User Edit (Self)
 */
Meteor.publish( "user/edit", function () {
    if ( !this.userId ) {
        return this.ready();
    }

    return UserCollection.find( {_id: this.userId}, {
        fields: {
            'services.password._':  1, // This won't return anything, but it'll put the password key if it exists
            'services.facebook.id': 1,
            'services.twitter.id':  1,
            'services.google.id':   1,
            'services.linkedin.id': 1
        }
    } );
} );

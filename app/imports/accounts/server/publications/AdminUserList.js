"use strict";

import slugify from "underscore.string/slugify";

import UserHelpers from "../../UserHelpers";
import UserCollection from "../../UserCollection";
import AvatarCollection from "../../AvatarCollection";

/**
 * User List (Administration)
 *
 * @params {Object} [options] Options for the find operation (ex: sort)
 */
Meteor.publishComposite( "admin/user/list", function ( query = null, options = null ) {

    // Check query for only the allowed fields
    check( query, Match.OneOf( null, {
        $or: Match.Optional( [Match.OneOf(
            Match.ObjectIncluding( { 'profile.firstName': { $regex: String, $options: String } } ),
            Match.ObjectIncluding( { 'profile.lastName': { $regex: String, $options: String } } ),
            Match.ObjectIncluding( { 'emails.address': { $regex: String, $options: String } } )
        )] )
    } ) );

    // Check options for only the allowed fields
    check( options, Match.OneOf( null, {
        sort:  Match.Optional( Object ),
        limit: Match.Optional( Number ),
        skip:  Match.Optional( Number )
    } ) );

    if ( !UserHelpers.isSuper( this.userId ) ) {
        return this.ready();
    }

    // Add our required fields in options
    query = query || {};

    // Setup options
    options = _.extend( options || {}, {
        fields: {
            enabled:                1,
            completed:              1,
            moderated:              1,
            emails:                 1,
            profile:                1,
            points:                 1,
            roles:                  1,
            'services.password._':  1, // This won't return anything, but it'll put the password key if it exists
            'services.facebook.id': 1,
            'services.twitter.id':  1,
            'services.google.id':   1,
            'services.linkedin.id': 1,
            createdAt:              1,
            createdBy:              1,
            updatedAt:              1,
            updatedBy:              1
        }
    } );

    return {
        find() {
            // Slugify the json query as a unique identifier for the count
            Counts.publish( this,
                'user/count/' + slugify( JSON.stringify( query ) ),
                UserCollection.find( query, { fields: { _id: 1 } } ),
                { noReady: true }
            );

            return UserCollection.find( query, options )
        },
        children: [
            {
                find( user ) {
                    return AvatarCollection.find( { owners: user._id } );
                }
            }
        ]
    };
} );
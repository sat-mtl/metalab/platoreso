"use strict";

import UserHelpers from "../../UserHelpers";
import UserCollection from "../../UserCollection";

/**
 * User Edit (Administration)
 */
Meteor.publish( "admin/user/edit", function ( id ) {
    check( id, String );

    if ( !UserHelpers.isAdmin( this.userId ) ) {
        return this.ready();
    }

    return UserCollection.find({_id: id});
} );
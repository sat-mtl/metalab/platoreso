"use strict";

import UserHelpers from "../../UserHelpers";
import AvatarCollection from "../../AvatarCollection";

/**
 * Avatar Edit (Administration)
 */
Meteor.publish( "admin/avatar/edit", function ( id ) {
    check( id, String );

    if ( !UserHelpers.isAdmin(this.userId) ) {
        return this.ready();
    }

    return AvatarCollection.find({owners:id});
} );
"use strict";

import UserCollection from "../../UserCollection";
import AvatarCollection from "../../AvatarCollection";
import CollectionHelpers from "../../../collections/CollectionHelpers";

/**
 * Public User Subscription
 * Will only send the publicly available data
 */
Meteor.publish( "user/public", function ( id ) {
    check( id, String );

    if ( !this.userId ) {
        return this.ready();
    }

    return [
        UserCollection.find( {
            _id:       id,
            moderated: false,
            enabled:   true,
            completed: true
        }, {
            fields: CollectionHelpers.getDefaultFields(
                UserCollection,
                this.userId,
                {
                    'profile': 1,
                    'roles':   1,
                    'points':  1
                }
            )
        } ),
        AvatarCollection.find( { owners: id } )
    ];
} );

"use strict";

import AvatarCollection from "../../AvatarCollection";

/**
 * Avatar Edit (Self)
 */
Meteor.publish( "avatar/edit", function () {
    if ( !this.userId ) {
        return this.ready();
    }

    return AvatarCollection.find( {owners: this.userId} );
} );

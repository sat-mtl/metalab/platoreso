"use strict";

const DefaultProfile = {
    allowEmailContact:  false,
    emailNotifications: {
        cardComment:      true,
        cardCommentReply: true
    }
};

export default DefaultProfile;
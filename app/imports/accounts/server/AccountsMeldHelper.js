"use strict";

import { AccountsMeld } from "meteor/splendido:accounts-meld";

let onMeldCallbacks         = [];
let onServiceAddedCallbacks = [];

export default class AccountsMeldHelper {

    /**
     * Resets all registered callbacks, useful mostly for testing
     * @private
     */
    static _reset() {
        onMeldCallbacks         = [];
        onServiceAddedCallbacks = [];
    }

    /**
     * Register a callback for meldDBCallback
     * @param callback
     */
    static onMeld( callback ) {
        onMeldCallbacks.push( callback );
    }

    /**
     * Register a callback for serviceAddedCallback
     * @param callback
     */
    static onServiceAdded( callback ) {
        onServiceAddedCallbacks.push( callback );
    }

    /**
     * Calls all the registered meldDBCallbacks
     *
     * @param src_user_id
     * @param dst_user_id
     * @private
     */
    static _meldDBCallback( src_user_id, dst_user_id ) {
        onMeldCallbacks.forEach( callback => callback( src_user_id, dst_user_id ) );
    }

    /**
     * Calls all the registered serviceAddedCallbaks
     *
     * @param user_id
     * @param service_name
     * @private
     */
    static _serviceAddedCallback( user_id, service_name ) {
        onServiceAddedCallbacks.forEach( callback => callback( user_id, service_name ) );
    }

    /**
     * Meld user callback
     * Combines profiles from two users into one
     *
     * @param src_user
     * @param dst_user
     * @returns {*}
     * @private
     */
    static _meldUserCallback( src_user, dst_user ) {
        // Dates
        if ( src_user.createdAt < dst_user.createdAt ) {
            dst_user.createdAt = src_user.createdAt;
        }

        // Profile
        if ( src_user.profile ) {
            let profile = {};
            _.defaults( profile, dst_user.profile || {} );
            _.defaults( profile, src_user.profile || {} );
            if ( !_.isEmpty( profile ) ) {
                dst_user.profile = profile;
            }
        }

        // Gamification points
        const srcPoints = src_user.points;
        const dstPoints = dst_user.points;
        dst_user.points = ( isNaN(srcPoints) ? 0 : srcPoints ) + ( isNaN(dstPoints) ? 0 : dstPoints );
        if ( !dst_user.pointsPerProject ) {
            dst_user.pointsPerProject = {};
        }
        if ( src_user.pointsPerProject ) {
            _.each( src_user.pointsPerProject, ( points, project ) => {
                const srcPoints = src_user.pointsPerProject[project];
                const dstPoints = dst_user.pointsPerProject[project];
                dst_user.pointsPerProject[project] = ( isNaN(srcPoints) ? 0 : srcPoints ) + ( isNaN(dstPoints) ? 0 : dstPoints );
            } );
        }

        // Status
        if ( src_user.enabled != undefined ) {
            dst_user.enabled = src_user.enabled || dst_user.enabled;
        }

        // Completed, if one of the two is, then the combination must be
        if ( src_user.completed != undefined ) {
            dst_user.completed = src_user.completed || dst_user.completed;
        }

        // Roles
        if ( src_user.roles ) {
            let roles = {};
            _.defaults( roles, dst_user.roles || {} );
            _.each( src_user.roles, ( value, key ) => {
                if ( !roles[key] ) {
                    roles[key] = value;
                } else {
                    roles[key] = _.union( value, roles[key] );
                }
            } );
            if ( !_.isEmpty( roles ) ) {
                dst_user.roles = roles;
            }
        }

        return dst_user;
    }

}

AccountsMeld.configure( {
    askBeforeMeld:        false,
    meldDBCallback:       AccountsMeldHelper._meldDBCallback,
    meldUserCallback:     AccountsMeldHelper._meldUserCallback,
    serviceAddedCallback: AccountsMeldHelper._serviceAddedCallback
} );
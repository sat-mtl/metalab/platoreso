"use strict";

import Logger from "meteor/metalab:logger/Logger";
import UserCollection from "../UserCollection";
import UserHelpers from "../UserHelpers";
import AvatarCollection from "../AvatarCollection";

const log = Logger( "user-hooks" );

/**
 * User Removed
 */
UserCollection.after.remove( function ( userId, user ) {
    // Defer, so that methods can return without waiting
    Meteor.defer( () => {
        log.debug( `User "${UserHelpers.getDisplayName( user )}" (${user._id}) removed, cleaning up relations` );

        // Remove associated image(s)
        AvatarCollection.unlinkOwner( user._id );
    } );
} );
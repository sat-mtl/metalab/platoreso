"use strict";

import slugify from "underscore.string/slugify";
import { Counts } from "meteor/tmeasday:publish-counts";

import UserHelpers from "../../UserHelpers";
import UserCollection from "../../UserCollection";
import AvatarCollection from "../../AvatarCollection";

/**
 * Moderation Report List (Administration)
 *
 * @params {Object} [options] Options for the find operation (ex: sort)
 */
Meteor.publishComposite( "user/moderation/report/list", function ( query = null, options = null ) {

    // Check query for only the allowed fields
    check( query, Match.OneOf( null, {
        $or:       Match.Optional( [Match.OneOf(
            Match.ObjectIncluding( { 'profile.firstName': { $regex: String, $options: String } } ),
            Match.ObjectIncluding( { 'profile.lastName': { $regex: String, $options: String } } ),
            Match.ObjectIncluding( { 'emails.address': { $regex: String, $options: String } } )
        )] ),
        moderated: Match.Optional( Boolean )
    } ) );

    // Check options for only the allowed fields
    check( options, Match.OneOf( null, {
        sort:  Match.Optional( Object ),
        limit: Match.Optional( Number ),
        skip:  Match.Optional( Number )
    } ) );

    const topLevelModerator = UserHelpers.isModerator( this.userId );
    if ( !topLevelModerator ) {
        return this.ready();
    }

    query = query || {};

    // Keep the original (unmodified by permissions, to publish counts to a unique URI)
    const originalQuery = _.clone( query );

    options = _.extend( options || {}, {
        fields: {
            _id:         1,
            profile:     1,
            moderated:   1,
            reportCount: 1
        }
    } );

    let children = [
        {
            find( user ) {
                return AvatarCollection.find( { owners: user._id } );
            }
        }
    ];

    return {
        find() {
            // Slugify the json query as a unique identifier for the count
            Counts.publish( this,
                'user/moderation/report/count/' + slugify( JSON.stringify( originalQuery ) ),
                UserCollection.findReported( query, { fields: { _id: 1 } } ),
                { noReady: true }
            );


            return UserCollection.findReported( query, options )
        },
        children: children
    };
} );
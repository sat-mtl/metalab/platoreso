"use strict";

export const RoleList = {
    admin:     'admin',
    super:     'super',
    expert:    'expert',
    moderator: 'moderator',
    member:    'member',
    user:      'user'
};

/**
 * Role Levels
 * Used to sort priority for roles, as when deciding the maximum role a super can assign to a user.
 *
 * @type {Object<Number>}
 */
export const RoleLevels = {
    admin:     500,
    super:     400,
    expert:    300,
    moderator: 200,
    member:    150,
    user:      100
};

/**
 * Role groups
 * Defines a hierarchy of permissions
 * Requiring for example that something might be accessed only by moderatorS mean
 * that moderators, experts, supers and admins will have access to it.
 *
 * @type {Object<Array<String>>}
 */
export const RoleGroups = {};

RoleGroups.admins     = [RoleList.admin];
RoleGroups.supers     = RoleGroups.admins.concat( [RoleList.super] );
RoleGroups.experts    = RoleGroups.supers.concat( [RoleList.expert] );
RoleGroups.moderators = RoleGroups.experts.concat( [RoleList.moderator] );
RoleGroups.members    = RoleGroups.moderators.concat( [RoleList.member] );
RoleGroups.users      = RoleGroups.members.concat( [RoleList.user] );
"use strict";

import CollectionHelpers from "/imports/collections/CollectionHelpers";
import ModerationHelpers from "/imports/moderation/ModerationHelpers";
import UserSchema from "./model/schema/UserSchema";

// Attach schema to the users collection
Meteor.users.attachSchema( UserSchema );

const UserCollection = Meteor.users;
const entityName = "user";
CollectionHelpers.registerCollection( entityName, Meteor.users );
ModerationHelpers.setup( UserCollection );
export default UserCollection;
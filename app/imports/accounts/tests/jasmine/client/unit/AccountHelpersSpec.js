describe( 'AccountHelpers', function () {

    describe('basic namespaced meteor methods', function() {
        it('should expose some meteor method', function() {
            expect(PR.Accounts.verifyEmail).toBeDefined();
            expect(PR.Accounts.verifyEmail).toBe(Accounts.verifyEmail);
            expect(PR.Accounts.forgotPassword).toBeDefined();
            expect(PR.Accounts.forgotPassword).toBe(Accounts.forgotPassword);
            expect(PR.Accounts.resetPassword).toBeDefined();
            expect(PR.Accounts.resetPassword).toBe(Accounts.resetPassword);
            expect(PR.Accounts.changePassword).toBeDefined();
            expect(PR.Accounts.changePassword).toBe(Accounts.changePassword);
        });
    });

    describe( 'register', function () {

        it( 'should expose a register method', function () {
            expect( PR.Accounts.register ).toBeDefined();
        } );

        it( 'should register', function () {
            const info = {email: 'me@you.com', password: '123456'};
            const cb   = jasmine.createSpy();
            spyOn( Accounts, 'createUser' );
            PR.Accounts.register( info, cb );
            expect( Accounts.createUser ).toHaveBeenCalledWith( info, cb );
        } );

        it( 'should remove superfluous fields', function () {
            const info  = {email: 'me@you.com', password: '123456'};
            const extra = _.extend( {extra: 'param'}, info );
            const cb    = jasmine.createSpy();
            spyOn( Accounts, 'createUser' );
            PR.Accounts.register( extra, cb );
            expect( Accounts.createUser ).toHaveBeenCalledWith( info, cb );
        } );

        it( 'should callback with an error when missing password', function ( done ) {
            const info = {email: 'me@you.com'};
            spyOn( Accounts, 'createUser' );
            PR.Accounts.register( info, err => {
                expect( err ).toBeDefined();
                expect( Accounts.createUser ).not.toHaveBeenCalled();
                done();
            } );
        } );

        it( 'should callback with an error when missing email', function ( done ) {
            const info = {password: '123456'};
            spyOn( Accounts, 'createUser' );
            PR.Accounts.register( info, err => {
                expect( err ).toBeDefined();
                expect( Accounts.createUser ).not.toHaveBeenCalled();
                done();
            } );
        } );

        it( 'should callback with an error when missing both', function ( done ) {
            const info = {};
            spyOn( Accounts, 'createUser' );
            PR.Accounts.register( info, err => {
                expect( err ).toBeDefined();
                expect( Accounts.createUser ).not.toHaveBeenCalled();
                done();
            } );
        } );

        it( 'should callback with an error when invalid email', function ( done ) {
            const info = {email: 'not an email', password: '123456'};
            spyOn( Accounts, 'createUser' );
            PR.Accounts.register( info, err => {
                expect( err ).toBeDefined();
                expect( Accounts.createUser ).not.toHaveBeenCalled();
                done();
            } );
        } );

    } );

    describe( 'login', function () {

        it( 'should expose a login method', function () {
            expect( PR.Accounts.login ).toBeDefined();
        } );

        describe( 'general', function () {

            it( 'should callback with an error when service is not recognized', function ( done ) {
                const credentials = {service: 'someObscureService'};
                PR.Accounts.login( credentials, err => {
                    expect( err ).toBeDefined();
                    done();
                } );
            } );

            it( 'should callback with an error when service is missing', function ( done ) {
                const credentials = {};
                PR.Accounts.login( credentials, err => {
                    expect( err ).toBeDefined();
                    done();
                } );
            } );

            it( 'should callback with an error when credentials are null', function ( done ) {
                const credentials = null;
                PR.Accounts.login( credentials, err => {
                    expect( err ).toBeDefined();
                    done();
                } );
            } );

        } );

        describe( 'password', function () {

            it( 'should login', function () {
                const credentials = {service: 'password', email: 'me@you.com', password: '123456'};
                const cb          = jasmine.createSpy();
                spyOn( Meteor, 'loginWithPassword' );
                PR.Accounts.login( credentials, cb );
                expect( Meteor.loginWithPassword ).toHaveBeenCalledWith( credentials.email, credentials.password, cb );
            } );

            it( 'should remove superfluous fields', function () {
                const credentials = {service: 'password', email: 'me@you.com', password: '123456'};
                const extra       = _.extend( {extra: 'param'}, credentials );
                const cb          = jasmine.createSpy();
                spyOn( Meteor, 'loginWithPassword' );
                PR.Accounts.login( extra, cb );
                expect( Meteor.loginWithPassword ).toHaveBeenCalledWith( credentials.email, credentials.password, cb );
            } );

            it( 'should callback with an error when missing password', function ( done ) {
                const credentials = {service: 'password', email: 'me@you.com'};
                spyOn( Meteor, 'loginWithPassword' );
                PR.Accounts.login( credentials, err => {
                    expect( err ).toBeDefined();
                    expect( Meteor.loginWithPassword ).not.toHaveBeenCalled();
                    done();
                } );
            } );

            it( 'should callback with an error when missing email', function ( done ) {
                const credentials = {service: 'password', password: '123456'};
                spyOn( Meteor, 'loginWithPassword' );
                PR.Accounts.login( credentials, err => {
                    expect( err ).toBeDefined();
                    expect( Meteor.loginWithPassword ).not.toHaveBeenCalled();
                    done();
                } );
            } );

            it( 'should callback with an error when invalid email', function ( done ) {
                const credentials = {service: 'password', email: 'not an email', password: '123456'};
                spyOn( Meteor, 'loginWithPassword' );
                PR.Accounts.login( credentials, err => {
                    expect( err ).toBeDefined();
                    expect( Meteor.loginWithPassword ).not.toHaveBeenCalled();
                    done();
                } );
            } );

        } );

        describe( 'facebook', function () {

            it( 'should login', function () {
                const credentials = {service: 'facebook'};
                const cb          = jasmine.createSpy();
                spyOn( Meteor, 'loginWithFacebook' );
                PR.Accounts.login( credentials, cb );
                expect( Meteor.loginWithFacebook ).toHaveBeenCalledWith( {requestPermissions: ['email']}, cb );
            } );

        } );

        describe( 'twitter', function () {

            it( 'should login', function () {
                const credentials = {service: 'twitter'};
                const cb          = jasmine.createSpy();
                spyOn( Meteor, 'loginWithTwitter' );
                PR.Accounts.login( credentials, cb );
                expect( Meteor.loginWithTwitter ).toHaveBeenCalledWith( {}, cb );
            } );

        } );

        describe( 'google', function () {

            it( 'should login', function () {
                const credentials = {service: 'google'};
                const cb          = jasmine.createSpy();
                spyOn( Meteor, 'loginWithGoogle' );
                PR.Accounts.login( credentials, cb );
                expect( Meteor.loginWithGoogle ).toHaveBeenCalledWith( {requestPermissions: ['openid', 'email']}, cb );
            } );

        } );

        describe( 'linkedIn', function () {

            it( 'should login', function () {
                const credentials = {service: 'linkedin'};
                const cb          = jasmine.createSpy();
                spyOn( Meteor, 'loginWithLinkedIn' );
                PR.Accounts.login( credentials, cb );
                expect( Meteor.loginWithLinkedIn ).toHaveBeenCalledWith( {requestPermissions: ['r_emailaddress']}, cb );
            } );

        } );

    } );

    describe( 'logout', function () {

        it( 'should expose a login method', function () {
            expect( PR.Accounts.logout ).toBeDefined();
        } );

        it( 'should logout', function () {
            spyOn( Meteor, 'logout' );
            PR.Accounts.logout();
            expect( Meteor.logout ).toHaveBeenCalled();
        } );
    } );
} );
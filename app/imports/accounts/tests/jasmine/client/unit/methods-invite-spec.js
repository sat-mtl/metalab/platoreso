const { UserServices, UserHelpers } = PR.Accounts;

describe( 'pr-accounts/client/unit/method/invite', function () {

    // Previous tests already done in common unit code

    it( 'should invite', function () {
        spyOn( UserHelpers, 'isExpert' ).and.returnValue( true );
        const info = { email: 'me@you.com', 'profile': { firstName: 'Test', lastName: 'User' } };
        expect( () => UserServices.invite( info ) ).not.toThrow();
    } );

} );

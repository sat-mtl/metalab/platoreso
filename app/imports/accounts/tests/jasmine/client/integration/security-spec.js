"use strict";

const { methods: AccountMethods, RoleList } = PR.Accounts;

/**
 * Security Tests
 * The custom subscription makes this not exactly the environment in which
 * the app is run but it is required that we have more information than normally
 * available in order to try and exploit it ;)
 */
describe('pr-accounts/client/integration/security', function() {

    beforeEach(function(done) {
        Meteor.call('fixtures/users/reset', function() {
            Meteor.subscribe( 'fixtures/users/list', function () {
                done();
            } );
        });
    });

    afterEach( function(done) {
        // Logout
        Meteor.logout(function() {
            done();
        });
    });

    describe('an admin user', function() {

        beforeEach( function ( done ) {
            Meteor.call('fixtures/users/be/admin', function ( err ) {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

        it('can create a user through accounts', function(done) {
            Accounts.createUser( {
                email: 'test_admin@test.com',
                password: 'test1234',
                profile: {
                    name: 'Test User'
                }
            }, function(err /*, user*/) {
                expect(err).toBeUndefined();
                done();
            });
        });

        it('cannot directly create a user', function() {
            Meteor.users.insert({ createdAt: new Date(), enabled: true, profile: { firstName: 'Pouet', lastName: 'Poupou'} }, function( err ) {
                expect(err).toBeDefined();
                expect(err.error).toBe(403);
            } );
        });

        it('can update another user', function(done) {
            var user = Meteor.users.findOne({'emails.address':'normal@test.com'});
            AccountMethods.update({_id:user._id, profile: {firstName:'Pouet Premier'}}, function(err) {
                expect(err).toBeUndefined();
                var user = Meteor.users.findOne({'emails.address':'normal@test.com'});
                expect(user.profile.firstName).toBe('Pouet Premier');
                //expect(user.roles).toEqual({'group':['admin']});
                done();
            });
        });

        it('cannot directly delete another user', function(done) {
            var user = Meteor.users.findOne({'emails.address':'normal@test.com'});
            Meteor.users.remove({_id:user._id}, function(err) {
                expect(err).not.toBeUndefined();
                done();
            });
        });

        it('can delete another user', function(done) {
            var user = Meteor.users.findOne({'emails.address':'normal@test.com'});
            AccountMethods.remove(user._id, function(err) {
                expect(err).toBeUndefined();
                done();
            });
        });

        it('cannot delete another admin directly', function(done) {
            var user = Meteor.users.findOne({'emails.address':'admin2@test.com'});
            Meteor.users.remove({_id:user._id}, function(err) {
                expect(err).not.toBeUndefined();
                done();
            });
        });

        it('can delete another admin', function(done) {
            var user = Meteor.users.findOne({'emails.address':'admin2@test.com'});
            AccountMethods.remove(user._id, function(err) {
                expect(err).toBeUndefined();
                done();
            });
        });

        // There is a method for this since it's complicated
        it('cannot delete itself', function(done) {
            Meteor.users.remove({_id:Meteor.userId()}, function(err) {
                expect(err).not.toBeUndefined();
                expect(err.error).toBe(403);
                done();
            });
        });

    });

    describe('a normal user', function() {

        beforeEach( function ( done ) {
            Meteor.call('fixtures/users/be/normal', function ( err ) {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

        it('can create a user through accounts', function(done) {
            Accounts.createUser( {
                email:    'test_normal@test.com',
                password: 'test1234',
                profile:  {

                }
            }, function(err) {
                expect(err).toBeUndefined();
                done();
            });
        });

        it('cannot directly create a user', function() {
            Meteor.users.insert({ createdAt: new Date(), enabled: true, profile: { firstName: 'Pouet', lastName: 'Poupou' } }, function( err ) {
                expect(err).not.toBeUndefined();
                expect(err.error).toBe(403);
            } );
        });

        it('cannot create an admin user', function(done) {
            Accounts.createUser( {
                email:    'test_normal@test.com',
                password: 'test1234',
                roles: { [Roles.GLOBAL_GROUP]: ['admin'] },
                profile:  {
                    name: 'Test User'
                }
            }, function(err) {
                expect(err).toBeUndefined();
                var user = Meteor.users.findOne({'emails.address':'test_normal@test.com'});
                expect(user.roles).toEqual({[Roles.GLOBAL_GROUP]:[RoleList.user]});
                done();
            });
        });

        // This was changed because users collection is denied any direct changes from the collection (OR NOT???)
        it('can directly update its profile', function(done) {
            Meteor.users.update({_id:Meteor.userId()}, { $set: {'profile.firstName':'Pouet Premier'}}, function(err) {
                //expect(err).not.toBeUndefined();
                //expect(err.error).toBe(403);
                expect(err).toBeUndefined();
                var user = Meteor.users.findOne({'emails.address':'normal@test.com'});
                console.log(user);
                expect(user.profile.firstName).toBe('Pouet Premier');
                done();
            });
        });

        it('cannot update its roles', function(done) {
            Meteor.users.update({_id:Meteor.userId()}, { $set: {'roles':{'group':['admin']}}}, function(err) {
                expect(err).not.toBeUndefined();
                expect(err.error).toBe(403);
                done();
            });
        });

        it('cannot update another user\'s profile', function(done) {
            var user = Meteor.users.findOne({'emails.address':'other@test.com'});
            Meteor.users.update({_id:user._id}, { $set: {'profile.firstName':'Pouet Premier'}}, function(err) {
                expect(err).not.toBeUndefined();
                expect(err.error).toBe(403);
                done();
            });
        });

        it('cannot update another user\'s roles', function(done) {
            var user = Meteor.users.findOne({'emails.address':'other@test.com'});
            Meteor.users.update({_id:user._id}, { $set: {'roles':{'group':['admin']}}}, function(err) {
                expect(err).not.toBeUndefined();
                expect(err.error).toBe(403);
                done();
            });
        });

        it('cannot delete another user', function(done) {
            var user = Meteor.users.findOne({'emails.address':'normal@test.com'});
            Meteor.users.remove({_id:user._id}, function(err) {
                expect(err).not.toBeUndefined();
                expect(err.error).toBe(403);
                done();
            });
        });

        // There is a method for this since it's complicated
        it('cannot delete itself', function(done) {
            Meteor.users.remove({_id:Meteor.userId()}, function(err) {
                expect(err).not.toBeUndefined();
                expect(err.error).toBe(403);
                done();
            });
        });
    });

    describe('an anonymous user', function() {

        it('can register', function(done) {
            Accounts.createUser( {
                email: 'test_anonymous@test.com',
                password: 'test1234',
                profile: {
                    name: 'Test User'
                }
            }, function(err) {
                expect(err).toBeUndefined();
                done();
            });
        });

        it('cannot update another user\'s profile', function(done) {
            var user = Meteor.users.findOne({'emails.address':'other@test.com'});
            Meteor.users.update({_id:user._id}, { $set: {'profile.firstName':'Pouet Premier'}}, function(err) {
                expect(err).not.toBeUndefined();
                expect(err.error).toBe(403);
                done();
            });
        });

        it('cannot update another user\'s roles', function(done) {
            var user = Meteor.users.findOne({'emails.address':'other@test.com'});
            Meteor.users.update({_id:user._id}, { $set: {'roles':{'group':['admin']}}}, function(err) {
                expect(err).not.toBeUndefined();
                expect(err.error).toBe(403);
                done();
            });
        });

        it('cannot delete another user', function(done) {
            var user = Meteor.users.findOne({'emails.address':'other@test.com'});
            Meteor.users.remove({_id:user._id}, function(err) {
                expect(err).not.toBeUndefined();
                expect(err.error).toBe(403);
                done();
            });
        });
    });
});
const { methods: AccountMethods, RoleList } = PR.Accounts;


function fullTest(done) {
    var user = Meteor.users.findOne( {'emails.address': 'normal@test.com'} );
    const info = { _id: user._id, email: 'new@email.com', profile: { firstName: 'New First Name', lastName: 'New Last Name' } };
    AccountMethods.update( info, ( err ) => {
        expect( err ).toBeUndefined();
        var u = Meteor.users.findOne( { _id: user._id } );
        expect( u ).toBeDefined();
        expect( u.profile ).toBeDefined();
        expect( u.profile.firstName ).toEqual( info.profile.firstName );
        expect( u.profile.firstName_sort ).toEqual( info.profile.firstName.toLowerCase() );
        expect( u.profile.lastName ).toEqual( info.profile.lastName );
        expect( u.profile.lastName_sort ).toEqual( info.profile.lastName.toLocaleLowerCase() );
        expect( u.emails[0].address ).toEqual( info.email );
        expect( u.emails[0].verified ).toBe(true);
        done();
    } );
}

function selfTest(email, verified, done) {
    var user = Meteor.users.findOne( {'emails.address': email} );
    const info = { _id: user._id, email: 'new@email.com', profile: { firstName: 'New First Name', lastName: 'New Last Name' } };
    AccountMethods.update( info, ( err ) => {
        expect( err ).toBeUndefined();
        var u = Meteor.users.findOne( { _id: user._id } );
        expect( u ).toBeDefined();
        expect( u.profile ).toBeDefined();
        expect( u.profile.firstName ).toEqual( info.profile.firstName );
        expect( u.profile.firstName_sort ).toEqual( info.profile.firstName.toLowerCase() );
        expect( u.profile.lastName ).toEqual( info.profile.lastName );
        expect( u.profile.lastName_sort ).toEqual( info.profile.lastName.toLocaleLowerCase() );
        expect( u.emails[0].address ).toEqual( info.email );
        expect( u.emails[0].verified ).toBe(verified);
        done();
    } );
}

function failTest(done) {
    var user = Meteor.users.findOne( {'emails.address': 'other@test.com'} );
    const info = { _id: user._id, email: 'new@email.com', profile: { firstName: 'New First Name', lastName: 'New Last Name' } };
    AccountMethods.update( info, ( err ) => {
        expect( err ).toBeDefined();
        expect( err.error ).toMatch(/unauthorized/);
        var u = Meteor.users.findOne( { _id: user._id } );
        expect( u ).toBeDefined();
        expect( u.profile ).toBeDefined();
        expect( u.profile.firstName ).toEqual( user.profile.firstName );
        expect( u.profile.firstName_sort ).toEqual( user.profile.firstName.toLowerCase() );
        expect( u.profile.lastName ).toEqual( user.profile.lastName );
        expect( u.profile.lastName_sort ).toEqual( user.profile.lastName.toLocaleLowerCase() );
        expect( u.emails[0].address ).toEqual( user.emails[0].address );
        expect( u.emails[0].verified ).toBe( user.emails[0].verified );
        done();
    } );
}

describe( 'pr-accounts/client/integration/methods/update', function () {

    beforeEach( function ( done ) {
        Meteor.call( 'fixtures/users/reset', function () {
            Meteor.subscribe( 'fixtures/users/list', function () {
                done();
            } );
        } );
    } );

    afterEach( function ( done ) {
        // Logout
        Meteor.logout( function () {
            done();
        } );
    } );

    describe( 'general', function () {

        beforeEach( function ( done ) {
            Meteor.call('fixtures/users/be/admin', function ( err ) {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

        it( 'should return error on missing _id', function ( done ) {
            const info = { whatever: 'value' };
            AccountMethods.update( info, ( err ) => {
                expect( err ).toBeDefined();
                expect( err.message ).toMatch( /Match failed/ );
                done();
            } );
        } );

        it( 'should return error on user not found', function ( done ) {
            const info = {_id: 'will not exist' };
            AccountMethods.update( info, ( err ) => {
                expect( err ).toBeDefined();
                expect( err.message ).toMatch( /user not found/ );
                done();
            } );
        } );

        it( 'can add completed status', function ( done ) {
            var user = Meteor.users.findOne( {'emails.address': 'normal@test.com'} );
            const info = { _id: user._id, profile: { firstName: 'First', lastName: '' } };
            AccountMethods.update( info, ( err ) => {
                expect( err ).toBeUndefined();
                var user = Meteor.users.findOne( {'emails.address': 'normal@test.com'} );
                expect( user.completed ).toBe(false);
                const info = { _id: user._id, profile: { lastName: 'Last' } };
                AccountMethods.update( info, ( err ) => {
                    expect( err ).toBeUndefined();
                    var user = Meteor.users.findOne( {'emails.address': 'normal@test.com'} );
                    expect( user ).toBeDefined();
                    expect( user.completed ).toBe(true);
                    done();
                } );
            } );
        } );

        it( 'can remove completed status', function ( done ) {
            var user = Meteor.users.findOne( {'emails.address': 'normal@test.com'} );
            const info = { _id: user._id, profile: { firstName: 'First', lastName: 'Last' } };
            AccountMethods.update( info, ( err ) => {
                expect( err ).toBeUndefined();
                var user = Meteor.users.findOne( {'emails.address': 'normal@test.com'} );
                expect( user.completed ).toBe(true);
                const info = { _id: user._id, profile: { lastName: '' } };
                AccountMethods.update( info, ( err ) => {
                    expect( err ).toBeUndefined();
                    var user = Meteor.users.findOne( {'emails.address': 'normal@test.com'} );
                    expect( user ).toBeDefined();
                    expect( user.completed ).toBe(false);
                    done();
                } );
            } );
        } );

    } );

    describe( 'an admin user', function () {

        beforeEach( function ( done ) {
            Meteor.call('fixtures/users/be/admin', function ( err ) {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

        it( 'can update any user', function ( done ) {
            fullTest(done);
        } );

        it('can update itself', function(done) {
            selfTest("admin1@test.com", true, done);
        });

    } );

    describe( 'a super user', function () {

        beforeEach( function ( done ) {
            Meteor.call('fixtures/users/be/superuser', function ( err ) {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

        it( 'can update any user', function ( done ) {
            fullTest(done);
        } );

        it('can update itself', function(done) {
            selfTest("super@test.com", true, done);
        });

    } );


    describe( 'an expert user', function () {

        beforeEach( function ( done ) {
            Meteor.call('fixtures/users/be/expert', function ( err ) {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

        it('can update itself', function(done) {
            selfTest("expert@test.com", false, done);
        });

        it('can\'t update another user', function(done) {
            failTest(done);
        });

    } );

    describe( 'a moderator user', function () {

        beforeEach( function ( done ) {
            Meteor.call('fixtures/users/be/moderator', function ( err ) {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

        it('can update itself', function(done) {
            selfTest("moderator@test.com", false, done);
        });

        it('can\'t update another user', function(done) {
            failTest(done);
        });

    } );

    describe( 'a normal user', function () {

        beforeEach( function ( done ) {
            Meteor.call('fixtures/users/be/normal', function ( err ) {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

        it('can update itself', function(done) {
            selfTest("normal@test.com", false, done);
        });

        it('can\'t update another user', function(done) {
            failTest(done);
        });

    } );

    describe( 'an anonymous user', function () {

        it('can\'t update another user', function(done) {
            failTest(done);
        });

    } );

} );
"use strict";

const { methods: AccountMethods } = PR.Accounts;

describe( 'pr-accounts/client/integration/method/sendValidationEmail', function () {

    beforeEach( function ( done ) {
        Meteor.call( 'fixtures/users/reset', function () {
            Meteor.subscribe( 'fixtures/users/list', function () {
                done();
            } );
        } );
    } );

    afterEach( function ( done ) {
        // Logout
        Meteor.logout( function () {
            done();
        } );
    } );

    describe( 'a user without a password', function () {

        let id;

        beforeEach( function ( done ) {
            Meteor.call( '__create_passwordless_user_and_login__', function (err, _id) {
                expect( err ).toBeUndefined();
                expect( _id ).toBeDefined();
                id = _id;
                done();
            } );
        } );

        it( 'can set a password if it doesn\'t already has one', function ( done ) {
            var user = Meteor.users.findOne( { _id: id } );
            expect( user ).toBeDefined();
            AccountMethods.setPassword( 'secret', ( err ) => {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

    } );

    describe( 'a normal user', function () {

        beforeEach( function ( done ) {
            Meteor.call('fixtures/users/be/normal', function ( err ) {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

        it( 'can\'t set a password if it already has one', function ( done ) {
            var user = Meteor.users.findOne( { 'emails.address': 'normal@test.com' } );
            expect( user ).toBeDefined();
            AccountMethods.setPassword( 'secret', ( err ) => {
                expect( err ).toBeDefined();
                expect( err.message ).toMatch( /already has a password/ );
                done();
            } );
        } );

    } );

    describe( 'an anonymous user', function () {

        it( 'can\'t set a password', function ( done ) {
            AccountMethods.setPassword( 'secret', ( err ) => {
                expect( err ).toBeDefined();
                expect( err.message ).toMatch( /Must be logged in/ );
                done();
            } );
        } );

    } );


} );
"use strict";

const { methods: AccountMethods, UserHelpers } = PR.Accounts;

describe('pr-accounts/client/integration/registration', function() {

    beforeEach(function(done) {
        Meteor.call('fixtures/users/reset', function() {
            done();
        });
    });

    afterEach( function(done) {
        // Logout
        Meteor.logout(function() {
            done();
        });
    });

    it('should be refused without an email (builtin)', function(done) {
        Accounts.createUser( {
            password: 'test1234',
            profile: {

            }
        }, function(err) {
            expect(err).not.toBeUndefined();
            expect(err.error).toBe(400);
            done();
        });
    } );

    it('should be refused without an email (invite method)', function(done) {
        AccountMethods.invite( {
            profile: {
                firstName: 'Test',
                lastName:  'User'
            }
        }, function(err) {
            expect(err).not.toBeUndefined();
            expect(err.error).toBe(400);
            done();
        });
    } );

    it('should be refused without a password (builtin)', function() {
        expect(
            Accounts.createUser.bind( {
                email: 'test_anonymous@test.com',
                profile: {

                }
            })
        ).toThrow();
    } );

    it('should be refused without a first name (invite method)', function(done) {
        AccountMethods.invite( {
            email: 'test_anonymous@test.com',
            profile: {
                lastName: 'User'
            }
        }, (err) => {
            expect(err).not.toBeUndefined();
            expect(err.error).toBe(400);
            done();
        });
    } );

    it('should be refused without a last name (invite method)', function(done) {
        AccountMethods.invite( {
            email: 'test_anonymous@test.com',
            profile: {
                firstName: 'Test'
            }
        }, (err) => {
            expect(err).not.toBeUndefined();
            expect(err.error).toBe(400);
            done();
        });
    } );

    it('should be enabled by default (builtin) but without roles', function(done) {
        Accounts.createUser( {
            email: 'test_anonymous@test.com',
            password: 'test1234',
            profile: {

            }
        }, function(err) {
            expect(err).toBeUndefined();
            expect(Meteor.user().enabled).toBe(true);
            expect( Roles.userIsInRole('user') ).toBe(false);
            done();
        });
    });
});
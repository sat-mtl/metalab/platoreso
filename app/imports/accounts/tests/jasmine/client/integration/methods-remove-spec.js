const { methods: AccountMethods } = PR.Accounts;

describe( 'pr-accounts/client/integration/method/remove', function () {

    beforeEach( function ( done ) {
        Meteor.call( 'fixtures/users/reset', function () {
            Meteor.subscribe( 'fixtures/users/list', function () {
                done();
            } );
        } );
    } );

    afterEach( function ( done ) {
        // Logout
        Meteor.logout( function () {
            done();
        } );
    } );

    describe( 'an admin user', function () {

        beforeEach( function ( done ) {
            Meteor.call('fixtures/users/be/admin', function ( err ) {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

        it( 'can\'t remove itself', function ( done ) {
            var user = Meteor.users.findOne( {'emails.address': 'admin1@test.com'} );
            expect( user ).toBeDefined();
            AccountMethods.remove( user._id, ( err ) => {
                expect( err ).toBeDefined();
                expect( err.error ).toMatch( /cannot remove self/ );
                var u = Meteor.users.findOne( user._id );
                expect( u ).toBeDefined();
                done();
            } );
        } );

        it( 'can remove another user', function ( done ) {
            var user = Meteor.users.findOne( {'emails.address': 'normal@test.com'} );
            expect( user ).toBeDefined();
            AccountMethods.remove( user._id, ( err ) => {
                expect( err ).toBeUndefined();
                var u = Meteor.users.findOne( user._id );
                expect( u ).toBeUndefined();
                done();
            } );
        } );

    } );

    describe( 'a super user', function () {

        beforeEach( function ( done ) {
            Meteor.call('fixtures/users/be/superuser', function ( err ) {
                expect( err ).toBeUndefined();
                done();
            } );
        } );
        it( 'can\'t remove itself', function ( done ) {
            var user = Meteor.users.findOne( {'emails.address': 'super@test.com'} );
            expect( user ).toBeDefined();
            AccountMethods.remove( user._id, ( err ) => {
                expect( err ).toBeDefined();
                expect( err.error ).toMatch( /cannot remove self/ );
                var u = Meteor.users.findOne( user._id );
                expect( u ).toBeDefined();
                done();
            } );
        } );

        it( 'can\'t remove an admin', function ( done ) {
            var user = Meteor.users.findOne( {'emails.address': 'admin1@test.com'} );
            expect( user ).toBeDefined();
            AccountMethods.remove( user._id, ( err ) => {
                expect( err ).toBeDefined();
                expect( err.error ).toMatch( /unauthorized/ );
                var u = Meteor.users.findOne( user._id );
                expect( u ).toBeDefined();
                done();
            } );
        } );

        it( 'can remove another user', function ( done ) {
            var user = Meteor.users.findOne( {'emails.address': 'normal@test.com'} );
            expect( user ).toBeDefined();
            AccountMethods.remove( user._id, ( err ) => {
                expect( err ).toBeUndefined();
                var u = Meteor.users.findOne( user._id );
                expect( u ).toBeUndefined();
                done();
            } );
        } );

    } );

    describe( 'an expert', function () {

        beforeEach( function ( done ) {
            Meteor.call('fixtures/users/be/expert', function ( err ) {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

        it( 'can\'t remove another user', function ( done ) {
            var user = Meteor.users.findOne( {'emails.address': 'normal@test.com'} );
            expect( user ).toBeDefined();
            AccountMethods.remove( user._id, ( err ) => {
                expect( err ).toBeDefined();
                expect( err.error ).toMatch( /unauthorized/ );
                var u = Meteor.users.findOne( user._id );
                expect( u ).toBeDefined();
                done();
            } );
        } );

    } );

    describe( 'a moderator', function () {

        beforeEach( function ( done ) {
            Meteor.call('fixtures/users/be/moderator', function ( err ) {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

        it( 'can\'t remove another user', function ( done ) {
            var user = Meteor.users.findOne( {'emails.address': 'normal@test.com'} );
            expect( user ).toBeDefined();
            AccountMethods.remove( user._id, ( err ) => {
                expect( err ).toBeDefined();
                expect( err.error ).toMatch( /unauthorized/ );
                var u = Meteor.users.findOne( user._id );
                expect( u ).toBeDefined();
                done();
            } );
        } );

    } );

    describe( 'a member', function () {

        beforeEach( function ( done ) {
            Meteor.call('fixtures/users/be/member', function ( err ) {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

        it( 'can\'t remove another user', function ( done ) {
            var user = Meteor.users.findOne( {'emails.address': 'normal@test.com'} );
            expect( user ).toBeDefined();
            AccountMethods.remove( user._id, ( err ) => {
                expect( err ).toBeDefined();
                expect( err.error ).toMatch( /unauthorized/ );
                var u = Meteor.users.findOne( user._id );
                expect( u ).toBeDefined();
                done();
            } );
        } );

    } );

    describe( 'a normal user', function () {

        beforeEach( function ( done ) {
            Meteor.call('fixtures/users/be/normal', function ( err ) {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

        it( 'can\'t remove slef (for now)', function ( done ) {
            var user = Meteor.users.findOne( {'emails.address': 'normal@test.com'} );
            expect( user ).toBeDefined();
            AccountMethods.remove( user._id, ( err ) => {
                expect( err ).toBeDefined();
                expect( err.error ).toMatch( /cannot remove self/ );
                var u = Meteor.users.findOne( user._id );
                expect( u ).toBeDefined();
                done();
            } );
        } );

        it( 'can\'t remove another user', function ( done ) {
            var user = Meteor.users.findOne( {'emails.address': 'member@test.com'} );
            expect( user ).toBeDefined();
            AccountMethods.remove( user._id, ( err ) => {
                expect( err ).toBeDefined();
                expect( err.error ).toMatch( /unauthorized/ );
                var u = Meteor.users.findOne( user._id );
                expect( u ).toBeDefined();
                done();
            } );
        } );

    } );

} );
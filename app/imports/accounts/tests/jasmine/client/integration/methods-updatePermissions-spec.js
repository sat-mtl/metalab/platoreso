const { methods: AccountMethods, RoleList } = PR.Accounts;

describe( 'pr-accounts/client/integration/method/updatePermissions', function () {

    beforeEach( function ( done ) {
        Meteor.call( 'fixtures/users/reset', function () {
            Meteor.subscribe( 'fixtures/users/list', function () {
                done();
            } );
        } );
    } );

    afterEach( function ( done ) {
        // Logout
        Meteor.logout( function () {
            done();
        } );
    } );

    describe( 'an admin user (or general cases)', function () {

        beforeEach( function ( done ) {
            Meteor.call('fixtures/users/be/admin', function ( err ) {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

        describe( 'on itself', function () {

            it( 'can change its own roles', function ( done ) {
                var user = Meteor.users.findOne( {'emails.address': 'admin1@test.com'} );
                expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user, RoleList.admin] );
                AccountMethods.updatePermissions( {_id: user._id, roles: [RoleList.moderator]}, ( err ) => {
                    expect( err ).toBeUndefined();
                    expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.admin, RoleList.moderator] );
                    done();
                } );
            } );

            it( 'can\'t remove its admin privileges (empty array)', function ( done ) {
                var user = Meteor.users.findOne( {'emails.address': 'admin1@test.com'} );
                expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user, RoleList.admin] );
                AccountMethods.updatePermissions( {_id: user._id, roles: []}, ( err ) => {
                    expect( err ).toBeUndefined();
                    expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.admin] );
                    done();
                } );
            } );

            it( 'can\'t remove its admin privileges (null)', function ( done ) {
                var user = Meteor.users.findOne( {'emails.address': 'admin1@test.com'} );
                expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user, RoleList.admin] );
                AccountMethods.updatePermissions( {_id: user._id, roles: null}, ( err ) => {
                    expect( err ).toBeUndefined();
                    expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user, RoleList.admin] );
                    done();
                } );
            } );

            it( 'can\'t remove its admin privileges (nothing)', function ( done ) {
                var user = Meteor.users.findOne( {'emails.address': 'admin1@test.com'} );
                expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user, RoleList.admin] );
                AccountMethods.updatePermissions( {_id: user._id}, ( err ) => {
                    expect( err ).toBeUndefined();
                    expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user, RoleList.admin] );
                    done();
                } );
            } );

            it( 'can\'t set custom roles', function ( done ) {
                var user = Meteor.users.findOne( {'emails.address': 'admin1@test.com'} );
                expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user, RoleList.admin] );
                AccountMethods.updatePermissions( {_id: user._id, roles: ['custom role']}, ( err ) => {
                    expect( err ).toBeUndefined();
                    expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user, RoleList.admin] );
                    done();
                } );
            } );

            it( 'can\'t set custom roles but will still add/remove the rest', function ( done ) {
                var user = Meteor.users.findOne( {'emails.address': 'admin1@test.com'} );
                expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user, RoleList.admin] );
                AccountMethods.updatePermissions( {_id: user._id, roles: ['custom role', RoleList.moderator]}, ( err ) => {
                    expect( err ).toBeUndefined();
                    expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.admin, RoleList.moderator] );
                    done();
                } );
            } );

        } );

        describe( 'on another admin', function () {

            it( 'can remove another admin\'s privileges (empty array)', function ( done ) {
                var user = Meteor.users.findOne( {'emails.address': 'admin2@test.com'} );
                expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user, RoleList.admin] );
                AccountMethods.updatePermissions( {_id: user._id, roles: []}, ( err ) => {
                    expect( err ).toBeUndefined();
                    expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [] );
                    done();
                } );
            } );

            it( 'does not remove permission on null roles key', function ( done ) {
                var user = Meteor.users.findOne( {'emails.address': 'admin2@test.com'} );
                expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user, RoleList.admin] );
                AccountMethods.updatePermissions( {_id: user._id, roles: null}, ( err ) => {
                    expect( err ).toBeUndefined();
                    expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user, RoleList.admin] );
                    done();
                } );
            } );

            it( 'does not remove permission on omitted roles key', function ( done ) {
                var user = Meteor.users.findOne( {'emails.address': 'admin2@test.com'} );
                expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user, RoleList.admin] );
                AccountMethods.updatePermissions( {_id: user._id}, ( err ) => {
                    expect( err ).toBeUndefined();
                    expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user, RoleList.admin] );
                    done();
                } );
            } );

            it( 'can\'t set custom roles', function ( done ) {
                var user = Meteor.users.findOne( {'emails.address': 'admin2@test.com'} );
                expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user, RoleList.admin] );
                AccountMethods.updatePermissions( {_id: user._id, roles: ['custom role']}, ( err ) => {
                    expect( err ).toBeUndefined();
                    expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user, RoleList.admin] );
                    done();
                } );
            } );


            it( 'can\'t set custom roles but will still add/remove the rest', function ( done ) {
                var user = Meteor.users.findOne( {'emails.address': 'admin2@test.com'} );
                expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user, RoleList.admin] );
                AccountMethods.updatePermissions( {_id: user._id, roles: ['custom role', RoleList.moderator]}, ( err ) => {
                    expect( err ).toBeUndefined();
                    expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.moderator] );
                    done();
                } );
            } );
        } );

        describe( 'on another user', function () {

            it( 'can change another user\'s roles (replacing)', function ( done ) {
                var user = Meteor.users.findOne( {'emails.address': 'normal@test.com'} );
                expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user] );
                AccountMethods.updatePermissions( {_id: user._id, roles: [RoleList.member]}, ( err ) => {
                    expect( err ).toBeUndefined();
                    expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.member] );
                    done();
                } );
            } );

            it( 'can change another user\'s roles (removing)', function ( done ) {
                var user = Meteor.users.findOne( {'emails.address': 'normal@test.com'} );
                expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user] );
                AccountMethods.updatePermissions( {_id: user._id, roles: []}, ( err ) => {
                    expect( err ).toBeUndefined();
                    expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [] );
                    done();
                } );
            } );


            it( 'does not remove permission on omitted roles key', function ( done ) {
                var user = Meteor.users.findOne( {'emails.address': 'normal@test.com'} );
                expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user] );
                AccountMethods.updatePermissions( {_id: user._id}, ( err ) => {
                    expect( err ).toBeUndefined();
                    expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user] );
                    done();
                } );
            } );


            it( 'does not remove permission on null roles key', function ( done ) {
                var user = Meteor.users.findOne( {'emails.address': 'normal@test.com'} );
                expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user] );
                AccountMethods.updatePermissions( {_id: user._id, roles: null}, ( err ) => {
                    expect( err ).toBeUndefined();
                    expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user] );
                    done();
                } );
            } );

            it( 'can\'t set custom roles', function ( done ) {
                var user = Meteor.users.findOne( {'emails.address': 'normal@test.com'} );
                expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user] );
                AccountMethods.updatePermissions( {_id: user._id, roles: ['custom role']}, ( err ) => {
                    expect( err ).toBeUndefined();
                    expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user] );
                    done();
                } );
            } );


            it( 'can\'t set custom roles but will still add/remove the rest', function ( done ) {
                var user = Meteor.users.findOne( {'emails.address': 'normal@test.com'} );
                expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user] );
                AccountMethods.updatePermissions( {_id: user._id, roles: ['custom role', RoleList.moderator]}, ( err ) => {
                    expect( err ).toBeUndefined();
                    expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.moderator] );
                    done();
                } );
            } );

            // GROUPS

            it( 'can set user roles for a group', function ( done ) {
                var user = Meteor.users.findOne( {'emails.address': 'normal@test.com'} );
                expect( Roles.getRolesForUser( user._id, 'test_group' ) ).toEqual( [RoleList.user] ); //Includes global group
                AccountMethods.updatePermissions( {_id: user._id, groups: {test_group: ['test']}}, ( err ) => {
                    expect( err ).toBeUndefined();
                    expect( Roles.getRolesForUser( user._id, 'test_group' ) ).toEqual( ['test', RoleList.user ] ); //Includes global group
                    done();
                } );
            } );

            it( 'can remove user roles for a group', function ( done ) {
                var user = Meteor.users.findOne( {'emails.address': 'normal@test.com'} );
                expect( Roles.getRolesForUser( user._id, 'test_group' ) ).toEqual( [RoleList.user] ); //Includes global group
                AccountMethods.updatePermissions( {_id: user._id, groups: {test_group: ['test']}}, ( err ) => {
                    expect( err ).toBeUndefined();
                    expect( Roles.getRolesForUser( user._id, 'test_group' ) ).toEqual( ['test', RoleList.user] );
                    // Now the deleting part of the test
                    AccountMethods.updatePermissions( {
                        _id:                user._id,
                        removedPermissions: ['test_group']
                    }, ( err ) => {
                        expect( err ).toBeUndefined();
                        expect( Roles.getRolesForUser( user._id, 'test_group' ) ).toEqual( [RoleList.user] );
                        done();
                    } );
                } );
            } );
        } );

    } );

    describe( 'a super user', function () {

        let targetUser;

        beforeEach( function ( done ) {
            Meteor.call('fixtures/users/be/superuser', function ( err ) {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

        describe( 'on itself', function () {

            it( 'can change its own roles (and loose it\'s super role)', function ( done ) {
                var user = Meteor.users.findOne( {'emails.address': 'super@test.com'} );
                expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user, RoleList.super] );
                AccountMethods.updatePermissions( {_id: user._id, roles: [RoleList.expert]}, ( err ) => {
                    expect( err ).toBeUndefined();
                    expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.expert] );
                    done();
                } );
            } );

            it( 'can\'t give itself admin rights', function ( done ) {
                var user = Meteor.users.findOne( {'emails.address': 'super@test.com'} );
                expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user, RoleList.super] );
                AccountMethods.updatePermissions( {_id: user._id, roles: [RoleList.admin]}, ( err ) => {
                    expect( err ).toBeUndefined();
                    expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user, RoleList.super] );
                    done();
                } );
            } );

        } );

        describe( 'on another admin', function () {

            it( 'can\'t remove an admin\'s privileges', function ( done ) {
                var user = Meteor.users.findOne( {'emails.address': 'admin1@test.com'} );
                expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user, RoleList.admin] );
                AccountMethods.updatePermissions( {_id: user._id, roles: []}, ( err ) => {
                    expect( err ).toBeUndefined();
                    expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.admin] );
                    done();
                } );
            } );
        } );

        describe( 'on another user', function () {

            it( 'can\'t give another user\'s admin privileges', function ( done ) {
                var user = Meteor.users.findOne( {'emails.address': 'normal@test.com'} );
                expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user] );
                AccountMethods.updatePermissions( {_id: user._id, roles: [RoleList.admin]}, ( err ) => {
                    expect( err ).toBeUndefined();
                    expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user] );
                    done();
                } );
            } );
        } );

    } );

    describe( 'a normal user', function () {

        beforeEach( function ( done ) {
            Meteor.call('fixtures/users/be/normal', function ( err ) {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

        it( 'can\'t change roles', function ( done ) {
            var user = Meteor.users.findOne( {'emails.address': 'normal@test.com'} );
            expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user] );
            AccountMethods.updatePermissions( {_id: user._id, roles: ['test']}, ( err ) => {
                expect( err ).not.toBeUndefined();
                expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user] );
                done();
            } );
        } );

        it( 'can\'t set user roles for a group', function ( done ) {
            var user = Meteor.users.findOne( {'emails.address': 'normal@test.com'} );
            expect( Roles.getRolesForUser( user._id, 'test_group' ) ).toEqual( [RoleList.user] ); //Includes global group
            AccountMethods.updatePermissions( {_id: user._id, groups: {test_group: ['test']}}, ( err ) => {
                expect( err ).not.toBeUndefined();
                expect( Roles.getRolesForUser( user._id, 'test_group' ) ).toEqual( [RoleList.user] );
                done();
            } );
        } );

    } );

    describe( 'an anonymous user', function () {

        it( 'can\'t change roles', function ( done ) {
            var user = Meteor.users.findOne( {'emails.address': 'normal@test.com'} );
            expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user] );
            AccountMethods.updatePermissions( {_id: user._id, roles: ['test']}, ( err ) => {
                expect( err ).not.toBeUndefined();
                expect( Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP ) ).toEqual( [RoleList.user] );
                done();
            } );
        } );

        it( 'can\'t set user roles for a group', function ( done ) {
            var user = Meteor.users.findOne( {'emails.address': 'normal@test.com'} );
            expect( Roles.getRolesForUser( user._id, 'test_group' ) ).toEqual( [RoleList.user] ); //Includes global group
            AccountMethods.updatePermissions( {_id: user._id, groups: {test_group: ['test']}}, ( err ) => {
                expect( err ).not.toBeUndefined();
                expect( Roles.getRolesForUser( user._id, 'test_group' ) ).toEqual( [RoleList.user] );
                done();
            } );
        } );

    } );

} );
"use strict";

const { methods: AccountMethods } = PR.Accounts;

describe( 'pr-accounts/client/integration/method/sendValidationEmail', function () {

    beforeEach( function ( done ) {
        Meteor.call( 'fixtures/users/reset', function () {
            Meteor.subscribe( 'fixtures/users/list', function () {
                done();
            } );
        } );
    } );

    afterEach( function ( done ) {
        // Logout
        Meteor.logout( function () {
            done();
        } );
    } );

    describe( 'an admin user', function () {

        beforeEach( function ( done ) {
            Meteor.call('fixtures/users/be/admin', function ( err ) {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

        it( 'can send validation email for another user (if unverified)', function ( done ) {
            var user = Meteor.users.findOne( { 'emails.address': 'unverified@test.com' } );
            expect( user ).toBeDefined();
            AccountMethods.sendValidationEmail( user._id, ( err ) => {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

        it( 'can\'t send validation email for another user (if verified)', function ( done ) {
            var user = Meteor.users.findOne( { 'emails.address': 'normal@test.com' } );
            expect( user ).toBeDefined();
            AccountMethods.sendValidationEmail( user._id, ( err ) => {
                expect( err ).toBeDefined();
                done();
            } );
        } );
    } );

    describe( 'a super user', function () {

        beforeEach( function ( done ) {
            Meteor.call('fixtures/users/be/superuser', function ( err ) {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

        it( 'can send validation email for another user (if unverified)', function ( done ) {
            var user = Meteor.users.findOne( { 'emails.address': 'unverified@test.com' } );
            expect( user ).toBeDefined();
            AccountMethods.sendValidationEmail( user._id, ( err ) => {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

        it( 'can\'t send validation email for another user (if verified)', function ( done ) {
            var user = Meteor.users.findOne( { 'emails.address': 'normal@test.com' } );
            expect( user ).toBeDefined();
            AccountMethods.sendValidationEmail( user._id, ( err ) => {
                expect( err ).toBeDefined();
                done();
            } );

        } );
    } );

    describe( 'a normal user', function () {

        beforeEach( function ( done ) {
            Meteor.call('fixtures/users/be/normal', function ( err ) {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

        it( 'can\'t send validation email for another user (if unverified)', function ( done ) {
            var user = Meteor.users.findOne( { 'emails.address': 'unverified@test.com' } );
            expect( user ).toBeDefined();
            AccountMethods.sendValidationEmail( user._id, ( err ) => {
                expect( err ).toBeDefined();
                done();
            } );
        } );

        it( 'can\'t send validation email for another user (if verified)', function ( done ) {
            var user = Meteor.users.findOne( { 'emails.address': 'normal@test.com' } );
            expect( user ).toBeDefined();
            AccountMethods.sendValidationEmail( user._id, ( err ) => {
                expect( err ).toBeDefined();
                done();
            } );
        } );
    } );

    describe( 'a normal (unverified) user', function () {

        beforeEach( function ( done ) {
            Meteor.call('fixtures/users/be/unverified', function ( err ) {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

        it( 'can send validation email for itself', function ( done ) {
            var user = Meteor.users.findOne( { 'emails.address': 'unverified@test.com' } );
            expect( user ).toBeDefined();
            AccountMethods.sendValidationEmail( user._id, ( err ) => {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

    } );

    describe( 'an anonymous user', function () {

        it( 'can\'t send validation email for another user', function ( done ) {
            var user = Meteor.users.findOne( { 'emails.address': 'member@test.com' } );
            expect( user ).toBeDefined();
            AccountMethods.sendValidationEmail( user._id, ( err ) => {
                expect( err ).toBeDefined();
                expect( err.error ).toMatch( /unauthorized/ );
                done();
            } );
        } );

    } );

} );
const { methods: AccountMethods, RoleList } = PR.Accounts;

describe( 'pr-accounts/client/integration/methods/invite', function () {

    beforeEach( function ( done ) {
        Meteor.call('fixtures/users/reset', function () {
            Meteor.subscribe( 'fixtures/users/list', function () {
                done();
            } );
        } );
    } );

    afterEach( function ( done ) {
        // Logout
        Meteor.logout( function () {
            done();
        } );
    } );

    describe( 'general', function () {

        beforeEach( function ( done ) {
            Meteor.call('fixtures/users/be/admin', function ( err ) {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

        it( 'should return error on missing email', function ( done ) {
            const invited = { profile: { firstName: 'First', lastName: 'Last' } };
            AccountMethods.invite( invited, ( err ) => {
                expect( err ).toBeDefined();
                expect( err.message ).toMatch( /Match failed/ );
                var user = Meteor.users.findOne( { 'emails.address': invited.email } );
                expect( user ).toBeUndefined();
                done();
            } );
        } );

        it( 'should return error on missing firstName', function ( done ) {
            const invited = { email: 'invited@email.com', profile: { lastName: 'Last' } };
            AccountMethods.invite( invited, ( err ) => {
                expect( err ).toBeDefined();
                expect( err.message ).toMatch( /Match failed/ );
                var user = Meteor.users.findOne( { 'emails.address': invited.email } );
                expect( user ).toBeUndefined();
                done();
            } );
        } );

        it( 'should return error on missing lastName', function ( done ) {
            const invited = { email: 'invited@email.com', profile: { firstName: 'First' } };
            AccountMethods.invite( invited, ( err ) => {
                expect( err ).toBeDefined();
                expect( err.message ).toMatch( /Match failed/ );
                var user = Meteor.users.findOne( { 'emails.address': invited.email } );
                expect( user ).toBeUndefined();
                done();
            } );
        } );

        it( 'should return error on missing profile', function ( done ) {
            const invited = { email: 'invited@email.com' };
            AccountMethods.invite( invited, ( err ) => {
                expect( err ).toBeDefined();
                expect( err.message ).toMatch( /Match failed/ );
                var user = Meteor.users.findOne( { 'emails.address': invited.email } );
                expect( user ).toBeUndefined();
                done();
            } );
        } );

    } );

    describe( 'an admin user', function () {

        beforeEach( function ( done ) {
            Meteor.call('fixtures/users/be/admin', function ( err ) {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

        it( 'can invite a user', function ( done ) {
            const invited = { email: 'invited@email.com', profile: { firstName: 'First', lastName: 'Last' } };
            AccountMethods.invite( invited, ( err ) => {
                expect( err ).toBeUndefined();
                var user = Meteor.users.findOne( { 'emails.address': invited.email } );
                expect( user ).toBeDefined();
                expect( user.profile ).toBeDefined();
                expect( user.profile.firstName ).toEqual( invited.profile.firstName );
                expect( user.profile.lastName ).toEqual( invited.profile.lastName );
                expect( user.emails ).toBeDefined();
                expect( user.emails[0] ).toBeDefined();
                expect( user.emails[0].address ).toEqual( invited.email );
                expect( user.roles ).toEqual( { [Roles.GLOBAL_GROUP]: [RoleList.user] } );
                done();
            } );
        } );

        it( 'can invite a user with roles', function ( done ) {
            const invited = { email: 'invited@email.com', profile: { firstName: 'First', lastName: 'Last' }, roles: [RoleList.expert] };
            AccountMethods.invite( invited, ( err ) => {
                expect( err ).toBeUndefined();
                var user = Meteor.users.findOne( { 'emails.address': invited.email } );
                expect( user ).toBeDefined();
                expect( user.roles ).toEqual( { [Roles.GLOBAL_GROUP]: [RoleList.user, RoleList.expert] } );
                done();
            } );
        } );

        it( 'can invite a user with group permissions', function ( done ) {
            const invited = {
                email:   'invited@email.com',
                profile: { firstName: 'First', lastName: 'Last' },
                groups:  {
                    group1: ['role1', 'role2']
                }
            };
            AccountMethods.invite( invited, ( err ) => {
                expect( err ).toBeUndefined();
                var user = Meteor.users.findOne( { 'emails.address': invited.email } );
                expect( user ).toBeDefined();
                expect( user.roles ).toEqual( {
                    [Roles.GLOBAL_GROUP]: [RoleList.user],
                    group1:               ['role1', 'role2']
                } );
                done();
            } );
        } );

    } );

    describe( 'a super user', function () {

        beforeEach( function ( done ) {
            Meteor.call('fixtures/users/be/superuser', function ( err ) {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

        it( 'can invite a user', function ( done ) {
            const invited = { email: 'invited@email.com', profile: { firstName: 'First', lastName: 'Last' } };
            AccountMethods.invite( invited, ( err ) => {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

        it( 'can invite a user with roles', function ( done ) {
            const invited = { email: 'invited@email.com', profile: { firstName: 'First', lastName: 'Last' }, roles: [RoleList.expert] };
            AccountMethods.invite( invited, ( err ) => {
                expect( err ).toBeUndefined();
                var user = Meteor.users.findOne( { 'emails.address': invited.email } );
                expect( user ).toBeDefined();
                expect( user.roles ).toEqual( { [Roles.GLOBAL_GROUP]: [RoleList.user, RoleList.expert] } );
                done();
            } );
        } );

        it( 'cannot invite an admin', function ( done ) {
            const invited = { email: 'invited@email.com', profile: { firstName: 'First', lastName: 'Last' }, roles: [RoleList.admin] };
            AccountMethods.invite( invited, ( err ) => {
                expect( err ).toBeUndefined();
                var user = Meteor.users.findOne( { 'emails.address': invited.email } );
                expect( user ).toBeDefined();
                expect( user.roles ).toEqual( { [Roles.GLOBAL_GROUP]: [RoleList.user] } );
                done();
            } );
        } );

    } );


    describe( 'an expert user', function () {

        beforeEach( function ( done ) {
            Meteor.call('fixtures/users/be/expert', function ( err ) {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

        it( 'can invite a user', function ( done ) {
            const invited = { email: 'invited@email.com', profile: { firstName: 'First', lastName: 'Last' } };
            AccountMethods.invite( invited, ( err ) => {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

    } );

    describe( 'a moderator user', function () {

        beforeEach( function ( done ) {
            Meteor.call('fixtures/users/be/moderator', function ( err ) {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

        it( 'cannot invite a user', function ( done ) {
            const invited = { email: 'invited@email.com', profile: { firstName: 'First', lastName: 'Last' } };
            AccountMethods.invite( invited, ( err ) => {
                expect( err ).toBeDefined();
                expect( err.error ).toMatch( /unauthorized/ );
                done();
            } );
        } );

    } );

    describe( 'a normal user', function () {

        beforeEach( function ( done ) {
            Meteor.call('fixtures/users/be/normal', function ( err ) {
                expect( err ).toBeUndefined();
                done();
            } );
        } );

        it( 'cannot invite a user', function ( done ) {
            const invited = { email: 'invited@email.com', profile: { firstName: 'First', lastName: 'Last' } };
            AccountMethods.invite( invited, ( err ) => {
                expect( err ).toBeDefined();
                expect( err.error ).toMatch( /unauthorized/ );
                done();
            } );
        } );

    } );

    describe( 'an anonymous user', function () {

        it( 'cannot invite a user', function ( done ) {
            const invited = { email: 'invited@email.com', profile: { firstName: 'First', lastName: 'Last' } };
            AccountMethods.invite( invited, ( err ) => {
                expect( err ).toBeDefined();
                expect( err.error ).toMatch( /unauthorized/ );
                done();
            } );
        } );

    } );

} );
"use strict";

const { UserServices, UserHelpers, UserCollection, methods: AccountMethods } = PR.Accounts;

describe( 'pr-accounts/both/unit/method/remove', function () {

    beforeEach( function () {
        spyOn( UserHelpers, 'isSuper' );
        spyOn( UserHelpers, 'isAdmin' );
        spyOn( UserCollection, 'findOne' );
        spyOn( UserCollection, 'remove' );
    } );

    it( 'the method wrapper should be exposed in the namespace', function () {
        expect( AccountMethods.remove ).toBeDefined();
    } );

    it( 'should throw on missing userId', function () {
        expect( () => UserServices.remove() ).toThrow();
        expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
        expect( UserCollection.findOne ).not.toHaveBeenCalled();
        expect( UserHelpers.isAdmin ).not.toHaveBeenCalled();
        expect( UserCollection.remove ).not.toHaveBeenCalled();
    } );

    it( 'should throw if not logged in (or not bound to a Meteor method)', function () {
        expect( () => UserServices.remove( '420' ) ).toThrowError( /unauthorized/ );
        expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
        expect( UserCollection.findOne ).not.toHaveBeenCalled();
        expect( UserHelpers.isAdmin ).not.toHaveBeenCalled();
        expect( UserCollection.remove ).not.toHaveBeenCalled();
    } );

    it( 'should throw if not allowed', function () {
        UserHelpers.isSuper.and.returnValue( false );
        expect( () => UserServices.remove.call( {userId: '666'}, '420' ) ).toThrowError( /unauthorized/ );
        expect( UserHelpers.isSuper ).toHaveBeenCalledWith( '666' );
        expect( UserCollection.findOne ).not.toHaveBeenCalled();
        expect( UserHelpers.isAdmin ).not.toHaveBeenCalled();
        expect( UserCollection.remove ).not.toHaveBeenCalled();
    } );

    it( 'should throw if trying to remove self', function () {
        expect( () => UserServices.remove.call( {userId: '420'}, '420' ) ).toThrowError( /cannot\ remove\ self/ );
        expect( UserHelpers.isSuper ).not.toHaveBeenCalled(); // Will not be called if trying to remove self because self removal will be implemented later
        expect( UserCollection.findOne ).not.toHaveBeenCalled();
        expect( UserHelpers.isAdmin ).not.toHaveBeenCalled();
        expect( UserCollection.remove ).not.toHaveBeenCalled();
    } );

    it( 'should throw if trying to remove self, especially when an admin', function () {
        expect( () => UserServices.remove.call( {userId: '420'}, '420' ) ).toThrowError( /cannot\ remove\ self/ );
        expect( UserHelpers.isSuper ).not.toHaveBeenCalled(); // Will not be called if trying to remove self because self removal will be implemented later
        expect( UserCollection.findOne ).not.toHaveBeenCalled();
        expect( UserHelpers.isAdmin ).not.toHaveBeenCalled();
        expect( UserCollection.remove ).not.toHaveBeenCalled();
    } );

    it( 'should throw if user id not found', function () {
        UserHelpers.isSuper.and.returnValue( true );
        UserCollection.findOne.and.returnValue( null );
        expect( () => UserServices.remove.call( {userId: '666'}, '420' ) ).toThrowError( /user not found/ );
        expect( UserHelpers.isSuper ).toHaveBeenCalledWith( '666' );
        expect( UserCollection.findOne ).toHaveBeenCalledWith( {_id: '420'}, {fields:{_id:1}} );
        expect( UserHelpers.isAdmin ).not.toHaveBeenCalled();
        expect( UserCollection.remove ).not.toHaveBeenCalled();
    } );

    it( 'should remove when authorized', function () {
        UserHelpers.isSuper.and.returnValue( true );
        UserHelpers.isAdmin.and.returnValues( true, false ); // admin removing whatever
        UserCollection.findOne.and.returnValue( {_id: '666'} );
        expect( () => UserServices.remove.call( {userId: '666'}, '420' ) ).not.toThrow();
        expect( UserHelpers.isSuper ).toHaveBeenCalledWith( '666' );
        expect( UserCollection.findOne ).toHaveBeenCalledWith( {_id: '420'}, {fields:{_id:1}} );
        expect( UserHelpers.isAdmin.calls.count() ).toEqual( 2 );
        expect( UserHelpers.isAdmin.calls.argsFor( 0 ) ).toEqual( ['666'] );
        expect( UserHelpers.isAdmin.calls.argsFor( 1 ) ).toEqual( ['420'] );
        expect( UserCollection.remove ).toHaveBeenCalledWith( {_id: '420'} );
    } );

    it( 'should not remove an admin when super', function () {
        UserHelpers.isSuper.and.returnValue( true );
        UserHelpers.isAdmin.and.returnValues( false, true ); //super removing admin
        UserCollection.findOne.and.returnValue( {_id: '666'} );
        expect( () => UserServices.remove.call( {userId: '666'}, '420' ) ).toThrowError( /unauthorized/ );
        expect( UserHelpers.isSuper ).toHaveBeenCalledWith( '666' );
        expect( UserCollection.findOne ).toHaveBeenCalledWith( {_id: '420'}, {fields:{_id:1}} );
        expect( UserHelpers.isAdmin.calls.count() ).toEqual( 2 );
        expect( UserHelpers.isAdmin.calls.argsFor( 0 ) ).toEqual( ['666'] );
        expect( UserHelpers.isAdmin.calls.argsFor( 1 ) ).toEqual( ['420'] );
        expect( UserCollection.remove ).not.toHaveBeenCalled();
    } );

} );

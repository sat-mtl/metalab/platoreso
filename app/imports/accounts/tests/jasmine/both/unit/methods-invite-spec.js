const { UserServices, UserHelpers } = PR.Accounts;

describe( 'pr-accounts/both/unit/method/invite', function () {

    it( 'service should exist', function () {
        expect( UserServices ).toBeDefined();
    } );

    it( 'methods should be exposed', function () {
        expect( PR.Accounts.methods ).toBeDefined();
    } );

    it( 'should exist in the service', function () {
        expect( UserServices.invite ).toBeDefined();
    } );

    it( 'the method wrapper should be exposed in the namespace', function () {
        expect( PR.Accounts.methods.invite ).toBeDefined();
    } );

    it( 'should throw on missing email', function () {
        const info = { profile: { firstName: 'Test', lastName: 'User' } };
        expect( () => UserServices.invite( info ) ).toThrowError( /Match error/ );
    } );

    it( 'should throw on missing first name', function () {
        const info = { email: 'me@you.com', profile: { lastName: 'User' } };
        expect( () => UserServices.invite( info ) ).toThrowError( /Match error/ );
    } );

    it( 'should throw on missing last name', function () {
        const info = { email: 'me@you.com', profile: { firstName: 'Test' } };
        expect( () => UserServices.invite( info ) ).toThrowError( /Match error/ );
    } );

    it( 'should throw if not allowed', function () {
        spyOn( UserHelpers, 'isExpert' ).and.returnValue( false );
        const info = { email: 'me@you.com', profile: { firstName: 'Test', lastName: 'User' } };
        expect( () => UserServices.invite.call( { userId: '420' }, info ) ).toThrowError( /unauthorized/ );
        expect( UserHelpers.isExpert ).toHaveBeenCalledWith( '420' );
    } );


    // The rest is client-only or server-only

} );

const { UserHelpers, UserCollection, UserServices, RoleList } = PR.Accounts;

describe( 'pr-accounts/both/unit/method/updatePermissions', function () {

    describe( 'updateRoles', function () {

        let updateInfo;
        let loggedUser;

        beforeEach( function () {
            loggedUser = '420';
            updateInfo = { _id: '666', roles: [] };

            spyOn( UserHelpers, 'isSuper' );
            spyOn( UserHelpers, 'isAdmin' );
            spyOn( UserCollection, 'findOne' );
            spyOn( Roles, 'getRolesForUser' );
            spyOn( Roles, 'removeUsersFromRoles' );
            spyOn( Roles, 'addUsersToRoles' );
        } );

        it( 'should exist in the service', function () {
            expect( UserServices._updateRoles ).toBeDefined();
        } );

        it( 'the method wrapper should not be exposed in the namespace', function () {
            expect( PR.Accounts.methods._updateRoles ).toBeUndefined();
        } );

        describe( 'when allowed', function () {

            let loggedUserRoles;
            let currentRoles;

            function _preExpectRoles() {
                // We expect everything to go well so return the user we asked for
                UserCollection.findOne.and.returnValue( { _id: updateInfo._id } );
                Roles.getRolesForUser.and.returnValues( currentRoles, loggedUserRoles, loggedUserRoles );
                // Call
                expect( ()=>UserServices._updateRoles.call( { userId: loggedUser }, updateInfo ) ).not.toThrow();
                // Poutine
                expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
                expect( UserCollection.findOne ).toHaveBeenCalledWith( { _id: updateInfo._id }, { fields: { _id: 1 } } );
                expect( Roles.getRolesForUser.calls.count() ).toEqual( 3 );
                expect( Roles.getRolesForUser.calls.argsFor( 0 ) ).toEqual( [updateInfo._id, Roles.GLOBAL_GROUP] );
                expect( Roles.getRolesForUser.calls.argsFor( 1 ) ).toEqual( [loggedUser, Roles.GLOBAL_GROUP] );
                expect( Roles.getRolesForUser.calls.argsFor( 2 ) ).toEqual( [loggedUser, Roles.GLOBAL_GROUP] );
            }

            function expectRoles( added, removed ) {
                _preExpectRoles();
                // Actual add/remove
                expect( Roles.removeUsersFromRoles ).toHaveBeenCalledWith( updateInfo._id, removed, Roles.GLOBAL_GROUP );
                expect( Roles.addUsersToRoles ).toHaveBeenCalledWith( updateInfo._id, added, Roles.GLOBAL_GROUP );
            }

            function expectAbortedRoles( ) {
                _preExpectRoles();
                // Actual add/remove
                expect( Roles.removeUsersFromRoles ).not.toHaveBeenCalled( );
                expect( Roles.addUsersToRoles ).not.toHaveBeenCalled( );
            }

            describe( 'as an admin (or general cases)', function () {

                beforeEach( function () {
                    loggedUserRoles = [RoleList.admin];
                    UserHelpers.isSuper.and.returnValue( true );
                    UserHelpers.isAdmin.and.returnValue( true );
                } );

                it( 'should update roles', function () {
                    // What is
                    currentRoles = [RoleList.expert];
                    // What we want
                    updateInfo.roles = [RoleList.member, RoleList.moderator];
                    // What happened (added, removed)
                    expectRoles( [RoleList.member, RoleList.moderator], [RoleList.expert] );
                } );

                it( 'should give admin rights', function () {
                    currentRoles = [RoleList.expert];
                    updateInfo.roles = [RoleList.admin];
                    expectRoles( [RoleList.admin], [RoleList.expert] );
                } );

                it( 'should remove admin rights', function () {
                    currentRoles = [RoleList.admin];
                    updateInfo.roles = [RoleList.super];
                    expectRoles( [RoleList.super], [RoleList.admin] );
                } );

                it( 'should not remove self admin rights (replacing)', function () {
                    currentRoles = [RoleList.admin];
                    updateInfo._id = loggedUser;
                    updateInfo.roles = [RoleList.super];
                    expectRoles( [RoleList.super], [] );
                } );

                it( 'should not remove self admin rights (removing)', function () {
                    currentRoles = [RoleList.admin];
                    updateInfo._id = loggedUser;
                    updateInfo.roles = [];
                    expectRoles( [], [] );
                } );

                it( 'should not allow custom roles', function () {
                    currentRoles = [RoleList.admin];
                    updateInfo._id = loggedUser;
                    updateInfo.roles = ['fake role'];
                    expectAbortedRoles();
                } );
            } );

            describe( 'as a super', function () {

                beforeEach( function () {
                    loggedUserRoles = [RoleList.super];
                    UserHelpers.isSuper.and.returnValue( true );
                    UserHelpers.isAdmin.and.returnValue( false );
                } );

                it( 'should update roles', function () {
                    // What is
                    currentRoles = [RoleList.expert];
                    // What we want
                    updateInfo.roles = [RoleList.member, RoleList.moderator];
                    // What happened (added, removed)
                    expectRoles( [RoleList.member, RoleList.moderator], [RoleList.expert] );
                } );

                it( 'should give super rights', function () {
                    currentRoles = [RoleList.expert];
                    updateInfo.roles = [RoleList.super];
                    expectRoles( [RoleList.super], [RoleList.expert] );
                } );

                it( 'should remove super rights', function () {
                    currentRoles = [RoleList.super];
                    updateInfo.roles = [RoleList.expert];
                    expectRoles( [RoleList.expert], [RoleList.super] );
                } );

                it( 'should remove self super rights (replacing)', function () {
                    currentRoles = [RoleList.super];
                    updateInfo._id = loggedUser;
                    updateInfo.roles = [RoleList.expert];
                    expectRoles( [RoleList.expert], [RoleList.super] );
                } );

                it( 'should remove self super rights (removing)', function () {
                    currentRoles = [RoleList.super];
                    updateInfo._id = loggedUser;
                    updateInfo.roles = [];
                    expectRoles( [], [RoleList.super] );
                } );

                it( 'should not give admin rights', function () {
                    currentRoles = [RoleList.expert];
                    updateInfo.roles = [RoleList.admin, RoleList.moderator];
                    expectRoles( [RoleList.moderator], [RoleList.expert] );
                } );
            } );

        } );

        describe('when not allowed/unsuccessful', function() {

            function expectFailedRoles() {
                expect( Roles.getRolesForUser ).not.toHaveBeenCalled();
                expect( Roles.removeUsersFromRoles ).not.toHaveBeenCalled();
                expect( Roles.addUsersToRoles ).not.toHaveBeenCalled();
            }

            it( 'should not update roles if not allowed', function () {
                UserHelpers.isSuper.and.returnValue( false );
                expect( ()=>UserServices._updateRoles.call( { userId: loggedUser }, updateInfo ) ).toThrowError( /unauthorized/ );
                expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
                expect( UserCollection.findOne ).not.toHaveBeenCalled();
                expectFailedRoles();
            } );

            it( 'should not update roles if not logged in (or bound to a Meteor method)', function () {
                expect( ()=>UserServices._updateRoles( updateInfo ) ).toThrowError( /unauthorized/ );
                expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
                expect( UserCollection.findOne ).not.toHaveBeenCalled();
                expectFailedRoles();
            } );

            it( 'should not update roles if user is not found', function () {
                UserHelpers.isSuper.and.returnValue( true );
                UserCollection.findOne.and.returnValue( null );
                expect( ()=>UserServices._updateRoles.call( { userId: loggedUser }, updateInfo ) ).toThrowError( /user\ not\ found/ );
                expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
                expect( UserCollection.findOne ).toHaveBeenCalledWith( { _id: updateInfo._id }, { fields: { _id: 1 } } );
                expectFailedRoles();
            } );

            it( 'should not update roles missing user id', function () {
                delete updateInfo._id;
                expect( ()=>UserServices._updateRoles.call( { userId: loggedUser }, updateInfo ) ).toThrowError( /Match\ error/ );
                expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
                expect( UserCollection.findOne ).not.toHaveBeenCalled();
                expectFailedRoles();
            } );

            it( 'should not update roles missing roles array', function () {
                delete updateInfo.roles;
                expect( ()=>UserServices._updateRoles.call( { userId: loggedUser }, updateInfo ) ).toThrowError( /Match\ error/ );
                expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
                expect( UserCollection.findOne ).not.toHaveBeenCalled();
                expectFailedRoles();
            } );
        });
    } );

    describe( 'removeGroupPermissions', function () {

        beforeEach( function () {
            spyOn( UserHelpers, 'isSuper' );
            spyOn( UserCollection, 'findOne' );
            spyOn( UserCollection, 'update' );
        } );

        it( 'should exist in the service', function () {
            expect( UserServices._removeGroupPermissions ).toBeDefined();
        } );

        it( 'the method wrapper should not be exposed in the namespace', function () {
            expect( PR.Accounts.methods._removeGroupPermissions ).toBeUndefined();
        } );

        it( 'should remove group permissions', function () {
            // Info
            const loggedUser = '420';
            const info       = { _id: '666', removedPermissions: ['trolls', 'social_justice_warriors'] };
            const user       = { _id: '666' };
            // Spies
            UserHelpers.isSuper.and.returnValue( true );
            UserCollection.findOne.and.returnValue( user );
            // Call
            expect( ()=>UserServices._removeGroupPermissions.call( { userId: loggedUser }, info ) ).not.toThrow();
            // Expectations
            expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
            expect( UserCollection.findOne ).toHaveBeenCalledWith( { _id: info._id }, { fields: { _id: 1 } } );
            expect( UserCollection.update ).toHaveBeenCalledWith( { _id: info._id }, { $unset: { 'roles.trolls': null, 'roles.social_justice_warriors': null } } );
        } );

        it( 'should throw if missing _id', function () {
            // Info
            const loggedUser = '420';
            const info       = { removedPermissions: ['trolls', 'social_justice_warriors'] };
            // Call
            expect( ()=>UserServices._removeGroupPermissions.call( { userId: loggedUser }, info ) ).toThrowError( /Match\ error/ );
            // Expectations
            expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
            expect( UserCollection.findOne ).not.toHaveBeenCalled();
            expect( UserCollection.update ).not.toHaveBeenCalled();
        } );

        it( 'should throw if missing removedPermissions', function () {
            // Info
            const loggedUser = '420';
            const info       = { _id: '666' };
            // Call
            expect( ()=>UserServices._removeGroupPermissions.call( { userId: loggedUser }, info ) ).toThrowError( /Match\ error/ );
            // Expectations
            expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
            expect( UserCollection.findOne ).not.toHaveBeenCalled();
            expect( UserCollection.update ).not.toHaveBeenCalled();
        } );

        it( 'should throw if not logged in (or bound to a Meteor method)', function () {
            // Info
            const info = { _id: '666', removedPermissions: ['trolls', 'social_justice_warriors'] };
            // Call
            expect( ()=>UserServices._removeGroupPermissions( info ) ).toThrowError( /unauthorized/ );
            // Expectations
            expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
            expect( UserCollection.findOne ).not.toHaveBeenCalled();
            expect( UserCollection.update ).not.toHaveBeenCalled();
        } );

        it( 'should throw if not an admin', function () {
            // Info
            const loggedUser = '420';
            const info       = { _id: '666', removedPermissions: ['trolls', 'social_justice_warriors'] };
            // Spies
            UserHelpers.isSuper.and.returnValue( false );
            // Call
            expect( ()=>UserServices._removeGroupPermissions.call( { userId: loggedUser }, info ) ).toThrowError( /unauthorized/ );
            // Expectations
            expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
            expect( UserCollection.findOne ).not.toHaveBeenCalled();
            expect( UserCollection.update ).not.toHaveBeenCalled();
        } );

        it( 'should throw if user is not found', function () {
            // Info
            const loggedUser = '420';
            const info       = { _id: '666', removedPermissions: ['trolls', 'social_justice_warriors'] };
            // Spies
            UserHelpers.isSuper.and.returnValue( true );
            UserCollection.findOne.and.returnValue( null );
            // Call
            expect( ()=>UserServices._removeGroupPermissions.call( { userId: loggedUser }, info ) ).toThrowError( /user\ not\ found/ );
            // Expectations
            expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
            expect( UserCollection.findOne ).toHaveBeenCalledWith( { _id: info._id }, { fields: { _id: 1 } } );
            expect( UserCollection.update ).not.toHaveBeenCalled();
        } );

    } );

    describe( 'setGroupPermissions', function () {

        beforeEach( function () {
            spyOn( UserHelpers, 'isSuper' );
            spyOn( UserCollection, 'findOne' );
            spyOn( Roles, 'setUserRoles' );
        } );

        it( 'should exist in the service', function () {
            expect( UserServices._setGroupPermissions ).toBeDefined();
        } );

        it( 'the method wrapper should not be exposed in the namespace', function () {
            expect( PR.Accounts.methods._setGroupPermissions ).toBeUndefined();
        } );

        it( 'should set group permissions', function () {
            // Info
            const loggedUser = '420';
            const info       = { _id: '666', groups: { 'commenters': ['troll', 'social_justice_warrior', 'white knight'], 'empty': null } };
            const user       = { _id: '666' };
            // Spies
            UserHelpers.isSuper.and.returnValue( true );
            UserCollection.findOne.and.returnValue( user );
            // Call
            expect( ()=>UserServices._setGroupPermissions.call( { userId: loggedUser }, info ) ).not.toThrow();
            // Expectations
            expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
            expect( UserCollection.findOne ).toHaveBeenCalledWith( { _id: info._id }, { fields: { _id: 1 } } );
            expect( Roles.setUserRoles.calls.count() ).toEqual( 2 );
            expect( Roles.setUserRoles.calls.argsFor( 0 ) ).toEqual( [info._id, info.groups.commenters, 'commenters'] );
            expect( Roles.setUserRoles.calls.argsFor( 1 ) ).toEqual( [info._id, [], 'empty'] );
        } );

        it( 'should throw if missing _id', function () {
            // Info
            const loggedUser = '420';
            const info       = { groups: { 'commenters': ['troll', 'social_justice_warrior', 'white knight'] } };
            // Call
            expect( ()=>UserServices._setGroupPermissions.call( { userId: loggedUser }, info ) ).toThrowError( /Match\ error/ );
            // Expectations
            expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
            expect( UserCollection.findOne ).not.toHaveBeenCalled();
            expect( Roles.setUserRoles ).not.toHaveBeenCalled();
        } );

        it( 'should throw if missing groups', function () {
            // Info
            const loggedUser = '420';
            const info       = { _id: '666' };
            // Call
            expect( ()=>UserServices._setGroupPermissions.call( { userId: loggedUser }, info ) ).toThrowError( /Match\ error/ );
            // Expectations
            expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
            expect( UserCollection.findOne ).not.toHaveBeenCalled();
            expect( Roles.setUserRoles ).not.toHaveBeenCalled();
        } );

        it( 'should throw if not logged in (or bound to a Meteor method)', function () {
            // Info
            const info = { _id: '666', groups: { 'commenters': ['troll', 'social_justice_warrior', 'white knight'] } };
            // Call
            expect( ()=>UserServices._setGroupPermissions( info ) ).toThrowError( /unauthorized/ );
            // Expectations
            expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
            expect( UserCollection.findOne ).not.toHaveBeenCalled();
            expect( Roles.setUserRoles ).not.toHaveBeenCalled();
        } );

        it( 'should throw if not an admin', function () {
            // Info
            const loggedUser = '420';
            const info       = { _id: '666', groups: { 'commenters': ['troll', 'social_justice_warrior', 'white knight'] } };
            // Spies
            UserHelpers.isSuper.and.returnValue( false );
            // Call
            expect( ()=>UserServices._setGroupPermissions.call( { userId: loggedUser }, info ) ).toThrowError( /unauthorized/ );
            // Expectations
            expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
            expect( UserCollection.findOne ).not.toHaveBeenCalled();
            expect( Roles.setUserRoles ).not.toHaveBeenCalled();
        } );

        it( 'should throw if user is not found', function () {
            // Info
            const loggedUser = '420';
            const info       = { _id: '666', groups: { 'commenters': ['troll', 'social_justice_warrior', 'white knight'] } };
            // Spies
            UserHelpers.isSuper.and.returnValue( true );
            UserCollection.findOne.and.returnValue( null );
            // Call
            expect( ()=>UserServices._setGroupPermissions.call( { userId: loggedUser }, info ) ).toThrowError( /user\ not\ found/ );
            // Expectations
            expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
            expect( UserCollection.findOne ).toHaveBeenCalledWith( { _id: info._id }, { fields: { _id: 1 } } );
            expect( Roles.setUserRoles ).not.toHaveBeenCalled();
        } );

    } );

    describe( 'updatePermissions', function () {

        beforeEach( function () {
            spyOn( UserHelpers, 'isSuper' );
            spyOn( UserCollection, 'findOne' );
            spyOn( UserServices, '_updateRoles' );
            spyOn( UserServices, '_removeGroupPermissions' );
            spyOn( UserServices, '_setGroupPermissions' );
        } );

        it( 'should exist in the service', function () {
            expect( UserServices.updatePermissions ).toBeDefined();
        } );

        it( 'the method wrapper should be exposed in the namespace', function () {
            expect( PR.Accounts.methods.updatePermissions ).toBeDefined();
        } );

        it( 'update permissions', function () {
            // Info
            const loggedUser = '420';
            const info       = { _id: '666', roles: ['god'], removedPermissions: ['exists'], groups: { deities: ['invention'] } };
            const user       = { _id: '666' };
            // Spies
            UserHelpers.isSuper.and.returnValue( true );
            UserCollection.findOne.and.returnValue( user );
            // Call
            expect( ()=>UserServices.updatePermissions.call( { userId: loggedUser }, info ) ).not.toThrow();
            // Expectations
            expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
            expect( UserCollection.findOne ).toHaveBeenCalledWith( { _id: info._id }, { fields: { _id: 1 } } );
            expect( UserServices._updateRoles ).toHaveBeenCalledWith( info );
            expect( UserServices._removeGroupPermissions ).toHaveBeenCalledWith( info );
            expect( UserServices._setGroupPermissions ).toHaveBeenCalledWith( info );
        } );

        it( 'update permissions, skip roles', function () {
            // Info
            const loggedUser = '420';
            const info       = { _id: '666', removedPermissions: ['exists'], groups: { deities: ['invention'] } };
            const user       = { _id: '666' };
            // Spies
            UserHelpers.isSuper.and.returnValue( true );
            UserCollection.findOne.and.returnValue( user );
            // Call
            expect( ()=>UserServices.updatePermissions.call( { userId: loggedUser }, info ) ).not.toThrow();
            // Expectations
            expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
            expect( UserCollection.findOne ).toHaveBeenCalledWith( { _id: info._id }, { fields: { _id: 1 } } );
            expect( UserServices._updateRoles ).not.toHaveBeenCalled();
            expect( UserServices._removeGroupPermissions ).toHaveBeenCalledWith( info );
            expect( UserServices._setGroupPermissions ).toHaveBeenCalledWith( info );
        } );

        it( 'update permissions, skip removedPermissions', function () {
            // Info
            const loggedUser = '420';
            const info       = { _id: '666', roles: ['god'], groups: { deities: ['invention'] } };
            const user       = { _id: '666' };
            // Spies
            UserHelpers.isSuper.and.returnValue( true );
            UserCollection.findOne.and.returnValue( user );
            // Call
            expect( ()=>UserServices.updatePermissions.call( { userId: loggedUser }, info ) ).not.toThrow();
            // Expectations
            expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
            expect( UserCollection.findOne ).toHaveBeenCalledWith( { _id: info._id }, { fields: { _id: 1 } } );
            expect( UserServices._updateRoles ).toHaveBeenCalledWith( info );
            expect( UserServices._removeGroupPermissions ).not.toHaveBeenCalled();
            expect( UserServices._setGroupPermissions ).toHaveBeenCalledWith( info );
        } );

        it( 'update permissions, skip groups', function () {
            // Info
            const loggedUser = '420';
            const info       = { _id: '666', roles: ['god'], removedPermissions: ['exists'] };
            const user       = { _id: '666' };
            // Spies
            UserHelpers.isSuper.and.returnValue( true );
            UserCollection.findOne.and.returnValue( user );
            // Call
            expect( ()=>UserServices.updatePermissions.call( { userId: loggedUser }, info ) ).not.toThrow();
            // Expectations
            expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
            expect( UserCollection.findOne ).toHaveBeenCalledWith( { _id: info._id }, { fields: { _id: 1 } } );
            expect( UserServices._updateRoles ).toHaveBeenCalledWith( info );
            expect( UserServices._removeGroupPermissions ).toHaveBeenCalledWith( info );
            expect( UserServices._setGroupPermissions ).not.toHaveBeenCalled();
        } );

        it( 'throw if missing _id', function () {
            // Info
            const loggedUser = '420';
            const info       = { roles: ['god'], removedPermissions: ['exists'], groups: { deities: ['invention'] } };
            // Call
            expect( ()=>UserServices.updatePermissions.call( { userId: loggedUser }, info ) ).toThrowError( /Match\ error/ );
            // Expectations
            expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
            expect( UserCollection.findOne ).not.toHaveBeenCalled();
            expect( UserServices._updateRoles ).not.toHaveBeenCalled();
            expect( UserServices._removeGroupPermissions ).not.toHaveBeenCalled();
            expect( UserServices._setGroupPermissions ).not.toHaveBeenCalled();
        } );

        it( 'throw if not logged in (or bound to a Meteor method)', function () {
            // Info
            const info = { _id: '666', roles: ['god'], removedPermissions: ['exists'], groups: { deities: ['invention'] } };
            // Call
            expect( ()=>UserServices.updatePermissions( info ) ).toThrowError( /unauthorized/ );
            // Expectations
            expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
            expect( UserCollection.findOne ).not.toHaveBeenCalled();
            expect( UserServices._updateRoles ).not.toHaveBeenCalled();
            expect( UserServices._removeGroupPermissions ).not.toHaveBeenCalled();
            expect( UserServices._setGroupPermissions ).not.toHaveBeenCalled();
        } );

        it( 'throw if not an admin', function () {
            // Info
            const loggedUser = '420';
            const info       = { _id: '666', roles: ['god'], removedPermissions: ['exists'], groups: { deities: ['invention'] } };
            // Spies
            UserHelpers.isSuper.and.returnValue( false );
            // Call
            expect( ()=>UserServices.updatePermissions.call( { userId: loggedUser }, info ) ).toThrowError( /unauthorized/ );
            // Expectations
            expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
            expect( UserCollection.findOne ).not.toHaveBeenCalled();
            expect( UserServices._updateRoles ).not.toHaveBeenCalled();
            expect( UserServices._removeGroupPermissions ).not.toHaveBeenCalled();
            expect( UserServices._setGroupPermissions ).not.toHaveBeenCalled();
        } );

        it( 'throw if user not found', function () {
            // Info
            const loggedUser = '420';
            const info       = { _id: '666', roles: ['god'], removedPermissions: ['exists'], groups: { deities: ['invention'] } };
            // Spies
            UserHelpers.isSuper.and.returnValue( true );
            UserCollection.findOne.and.returnValue( null );
            // Call
            expect( ()=>UserServices.updatePermissions.call( { userId: loggedUser }, info ) ).toThrowError( /user\ not\ found/ );
            // Expectations
            expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
            expect( UserCollection.findOne ).toHaveBeenCalledWith( { _id: info._id }, { fields: { _id: 1 } } );
            expect( UserServices._updateRoles ).not.toHaveBeenCalled();
            expect( UserServices._removeGroupPermissions ).not.toHaveBeenCalled();
            expect( UserServices._setGroupPermissions ).not.toHaveBeenCalled();
        } );
    } );
} );
const { UserHelpers, UserServices, UserCollection } = PR.Accounts;

describe( 'pr-accounts', function () {

    describe( 'methods', function () {

        it( 'service should exist', function () {
            expect( UserServices ).toBeDefined();
        } );

        it( 'methods should be exposed', function () {
            expect( PR.Accounts.methods ).toBeDefined();
        } );

        describe( 'updateEmail', function () {

            beforeEach( function () {
                spyOn( UserHelpers, 'isSuper' );
                spyOn( UserCollection, 'findOne' );
                spyOn( UserCollection, 'update' );
                if ( Meteor.isServer ) {
                    spyOn( Accounts, 'sendVerificationEmail' );
                }
            } );

            it( 'should exist in the service', function () {
                expect( UserServices._updateEmail ).toBeDefined();
            } );

            it( 'the method wrapper should not be exposed in the namespace', function () {
                expect( PR.Accounts.methods._updateEmail ).not.toBeDefined();
            } );

            it( 'should update email if the user had a previous email address', function () {
                const loggedUser = '666';
                const info       = {_id: '420', email: 'after@email.com'};
                const user       = {_id: '420', emails: [{address: 'before@email.com', verified: true}]};
                const updateInfo = {'emails.0.address': info.email, 'emails.0.verified': true};

                //console.log(PR.Accounts, PR.Accounts.UserHelpers, PR.Accounts.UserHelpers.isSuper);
                UserHelpers.isSuper.and.returnValue( true );
                UserCollection.findOne.and.returnValue( user );

                expect( () => UserServices._updateEmail.call( {userId: loggedUser}, info ) ).not.toThrow();

                expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
                expect( UserCollection.findOne ).toHaveBeenCalledWith( {_id: info._id}, {fields: {emails: 1}} );
                expect( UserCollection.update ).toHaveBeenCalledWith( {_id: info._id}, {$set: updateInfo} );
                if ( Meteor.isServer ) {
                    // We test as an admin here
                    expect( Accounts.sendVerificationEmail ).not.toHaveBeenCalled();
                }
            } );

            it( 'should initialize the emails field if the user never had an email', function () {
                const loggedUser = '666';
                const info       = {_id: '420', email: 'after@email.com'};
                const user       = {_id: '420'};
                const updateInfo = {emails: [{address: info.email, verified: true}]};

                UserHelpers.isSuper.and.returnValue( true );
                UserCollection.findOne.and.returnValue( user );

                expect( () => UserServices._updateEmail.call( {userId: loggedUser}, info ) ).not.toThrow();

                expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
                expect( UserCollection.findOne ).toHaveBeenCalledWith( {_id: info._id}, {fields: {emails: 1}} );
                expect( UserCollection.update ).toHaveBeenCalledWith( {_id: info._id}, {$set: updateInfo} );
                if ( Meteor.isServer ) {
                    // We test as an admin here
                    expect( Accounts.sendVerificationEmail ).not.toHaveBeenCalled();
                }
            } );

            it( 'should require verification if changed', function () {
                const loggedUser = '420';
                const info       = {_id: '420', email: 'after@email.com'};
                const user       = {_id: '420', emails: [{address: 'before@email.com', verified: true}]};
                const updateInfo = {'emails.0.address': info.email, 'emails.0.verified': false};

                UserHelpers.isSuper.and.returnValue( false );
                UserCollection.findOne.and.returnValue( user );

                expect( () => UserServices._updateEmail.call( {userId: loggedUser}, info ) ).not.toThrow();

                expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
                expect( UserCollection.findOne ).toHaveBeenCalledWith( {_id: info._id}, {fields: {emails: 1}} );
                expect( UserCollection.update ).toHaveBeenCalledWith( {_id: info._id}, {$set: updateInfo} );
                if ( Meteor.isServer ) {
                    expect( Accounts.sendVerificationEmail ).toHaveBeenCalledWith( info._id );
                }
            } );

            it( 'should not be able to edit someone else\'s email if not an admin', function () {
                const loggedUser = '666';
                const info       = {_id: '420', email: 'same@email.com'};
                const user       = {_id: '420', emails: [{address: 'same@email.com', verified: true}]};

                UserHelpers.isSuper.and.returnValue( false );
                UserCollection.findOne.and.returnValue( user );

                expect( () => UserServices._updateEmail.call( {userId: loggedUser}, info ) ).toThrowError( /unauthorized/ );

                expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
                expect( UserCollection.findOne ).not.toHaveBeenCalled();
                expect( UserCollection.update ).not.toHaveBeenCalled();
                if ( Meteor.isServer ) {
                    expect( Accounts.sendVerificationEmail ).not.toHaveBeenCalled();
                }
            } );

            it( 'should throw if not logged in (or not bound to Meteor method)', function () {
                // Info
                const info = {_id: '420', email: 'same@email.com'};
                // Call
                expect( () => UserServices._updateEmail( _.clone( info ) ) ).toThrowError( /unauthorized/ );
                // Expectations
                expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
                expect( UserCollection.findOne ).not.toHaveBeenCalled();
                expect( UserCollection.update ).not.toHaveBeenCalled();
                if ( Meteor.isServer ) {
                    expect( Accounts.sendVerificationEmail ).not.toHaveBeenCalled();
                }
            } );

            it( 'should throw if user is not found', function () {
                const loggedUser = '420';
                const info       = {_id: '420', email: 'same@email.com'};

                UserHelpers.isSuper.and.returnValue( false );
                UserCollection.findOne.and.returnValue( null );

                expect( () => UserServices._updateEmail.call( {userId: loggedUser}, info ) ).toThrowError( /user\ not\ found/ );

                expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
                expect( UserCollection.findOne ).toHaveBeenCalledWith( {_id: info._id}, {fields: {emails: 1}} );
                expect( UserCollection.update ).not.toHaveBeenCalled();
                if ( Meteor.isServer ) {
                    expect( Accounts.sendVerificationEmail ).not.toHaveBeenCalled();
                }
            } );

            it( 'should not email if it hasn\'t changed', function () {
                const loggedUser = '420';
                const info       = {_id: '420', email: 'same@email.com'};
                const user       = {_id: '420', emails: [{address: 'same@email.com', verified: true}]};

                UserHelpers.isSuper.and.returnValue( false );
                UserCollection.findOne.and.returnValue( user );

                expect( () => UserServices._updateEmail.call( {userId: loggedUser}, info ) ).not.toThrow();

                expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
                expect( UserCollection.findOne ).toHaveBeenCalledWith( {_id: info._id}, {fields: {emails: 1}} );
                expect( UserCollection.update ).not.toHaveBeenCalled();
                if ( Meteor.isServer ) {
                    expect( Accounts.sendVerificationEmail ).not.toHaveBeenCalled();
                }
            } );

        } );

        describe( 'updateProfile', function () {

            beforeEach( function () {
                spyOn( UserHelpers, 'isSuper' );
                spyOn( UserCollection, 'update' );
            } );

            it( 'should exist in the service', function () {
                expect( UserServices._updateProfile ).toBeDefined();
            } );

            it( 'the method wrapper should not be exposed in the namespace', function () {
                expect( PR.Accounts.methods._updateEmail ).not.toBeDefined();
            } );

            it( 'should update profile', function () {
                // Info
                const loggedUser = '666';
                const info       = {_id: '420', profile: {firstName: 'Test', lastName: 'User'}};
                const updateInfo = {
                    'profile.firstName':      info.profile.firstName,
                    'profile.firstName_sort': info.profile.firstName.toLowerCase(),
                    'profile.lastName':       info.profile.lastName,
                    'profile.lastName_sort':  info.profile.lastName.toLocaleLowerCase()
                };
                // Spies
                UserHelpers.isSuper.and.returnValue( true );
                // Call
                expect( () => UserServices._updateProfile.call( {userId: loggedUser}, _.clone( info ) ) ).not.toThrow();
                // Expectations
                expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
                expect( UserCollection.update ).toHaveBeenCalledWith( {_id: info._id}, {$set: updateInfo} );
            } );

            it( 'should do nothing if no profile info', function () {
                // Info
                const loggedUser = '666';
                const info       = {_id: '420', profile: {}};
                // Spies
                UserHelpers.isSuper.and.returnValue( true );
                // Call
                expect( () => UserServices._updateProfile.call( {userId: loggedUser}, _.clone( info ) ) ).not.toThrow();
                // Expectations
                expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
                expect( UserCollection.update ).not.toHaveBeenCalled();
            } );

            it( 'should throw on missing _id', function () {
                // Info
                const loggedUser = '666';
                const info       = {profile: {}};
                // Call
                expect( () => UserServices._updateProfile.call( {userId: loggedUser}, _.clone( info ) ) ).toThrowError( /Match\ error/ );
                // Expectations
                expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
                expect( UserCollection.update ).not.toHaveBeenCalled();
            } );

            it( 'should throw on missing profile', function () {
                // Info
                const loggedUser = '666';
                const info       = {_id: '420'};
                // Call
                expect( () => UserServices._updateProfile.call( {userId: loggedUser}, _.clone( info ) ) ).toThrowError( /Match\ error/ );
                // Expectations
                expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
                expect( UserCollection.update ).not.toHaveBeenCalled();
            } );

            it( 'should throw if not logged in (or not bound to Meteor method)', function () {
                // Info
                const info = {_id: '420', profile: {}};
                // Call
                expect( () => UserServices._updateProfile( _.clone( info ) ) ).toThrowError( /unauthorized/ );
                // Expectations
                expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
                expect( UserCollection.update ).not.toHaveBeenCalled();
            } );

            it( 'should throw if not editing self and not an admin', function () {
                // Info
                const loggedUser = '666';
                const info       = {_id: '420', profile: {}};
                // Spies
                UserHelpers.isSuper.and.returnValue( false );
                // Call
                expect( () => UserServices._updateProfile.call( {userId: loggedUser}, _.clone( info ) ) ).toThrowError( /unauthorized/ );
                // Expectations
                expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
                expect( UserCollection.update ).not.toHaveBeenCalled();
            } );

            it( 'should be able to edit self if not an admin', function () {
                // Info
                const loggedUser = '420';
                const info       = {_id: '420', profile: {firstName: 'Test', lastName: 'User'}};
                // Spies
                UserHelpers.isSuper.and.returnValue( false );
                // Call
                expect( () => UserServices._updateProfile.call( {userId: loggedUser}, _.clone( info ) ) ).not.toThrow();
                // Expectations
                expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
                expect( UserCollection.update ).toHaveBeenCalled(); // Don't check params this has already been tested
            } );

        } );

        describe( 'updateProfileCompletion', function () {

            beforeEach( function () {
                spyOn( UserHelpers, 'isSuper' );
                spyOn( UserHelpers, 'isProfileCompleted' );
                spyOn( UserCollection, 'findOne' );
                spyOn( UserCollection, 'update' );
            } );

            it( 'should exist in the service', function () {
                expect( UserServices._updateProfileCompletion ).toBeDefined();
            } );

            it( 'the method wrapper should not be exposed in the namespace', function () {
                expect( PR.Accounts.methods._updateProfileCompletion ).not.toBeDefined();
            } );

            it( 'should make a user pass from not completed to completed', function () {
                // Info
                const loggedUser = '666';
                const updateUser = '420';
                const user       = {emails: [], profile: {}, completed: false}; // Actually, we don't really care since it's a unit test
                // Spies
                UserHelpers.isSuper.and.returnValue( true );
                UserCollection.findOne.and.returnValue( user );
                UserHelpers.isProfileCompleted.and.returnValue( true );
                // Call
                expect( ()=>UserServices._updateProfileCompletion.call( {userId: loggedUser}, updateUser ) ).not.toThrow();
                // Expectations
                expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
                expect( UserCollection.findOne ).toHaveBeenCalledWith( {_id: updateUser}, {
                    fields: {
                        emails:    1,
                        profile:   1,
                        completed: 1
                    }
                } );
                expect( UserHelpers.isProfileCompleted ).toHaveBeenCalledWith( user );
                expect( UserCollection.update ).toHaveBeenCalledWith( {_id: updateUser}, {$set: {completed: true}} );
            } );

            it( 'should make a user pass from completed to not completed', function () {
                // Info
                const loggedUser = '666';
                const updateUser = '420';
                const user       = {emails: [], profile: {}, completed: true}; // Actually, we don't really care since it's a unit test
                // Spies
                UserHelpers.isSuper.and.returnValue( true );
                UserCollection.findOne.and.returnValue( user );
                UserHelpers.isProfileCompleted.and.returnValue( false );
                // Call
                expect( ()=>UserServices._updateProfileCompletion.call( {userId: loggedUser}, updateUser ) ).not.toThrow();
                // Expectations
                expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
                expect( UserCollection.findOne ).toHaveBeenCalledWith( {_id: updateUser}, {
                    fields: {
                        emails:    1,
                        profile:   1,
                        completed: 1
                    }
                } );
                expect( UserHelpers.isProfileCompleted ).toHaveBeenCalledWith( user );
                expect( UserCollection.update ).toHaveBeenCalledWith( {_id: updateUser}, {$set: {completed: false}} );
            } );

            it( 'should allow updating self when not an admin', function () {
                // Info
                const loggedUser = '420';
                const updateUser = '420';
                const user       = {emails: [], profile: {}, completed: true}; // Actually, we don't really care since it's a unit test
                // Spies
                UserCollection.findOne.and.returnValue( user );
                UserHelpers.isProfileCompleted.and.returnValue( true );
                // Call
                expect( ()=>UserServices._updateProfileCompletion.call( {userId: loggedUser}, updateUser ) ).not.toThrow();
                // Expectations
                expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
                expect( UserCollection.findOne ).toHaveBeenCalledWith( {_id: updateUser}, {
                    fields: {
                        emails:    1,
                        profile:   1,
                        completed: 1
                    }
                } );
                expect( UserHelpers.isProfileCompleted ).toHaveBeenCalledWith( user );
                expect( UserCollection.update ).toHaveBeenCalledWith( {_id: updateUser}, {$set: {completed: true}} );
            } );

            it( 'should throw if missing parameter', function () {
                // Info
                const loggedUser = '666';
                // Call
                expect( ()=>UserServices._updateProfileCompletion.call( {userId: loggedUser}, null ) ).toThrowError( /Match\ error/ );
                // Expectations
                expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
                expect( UserCollection.findOne ).not.toHaveBeenCalled();
                expect( UserHelpers.isProfileCompleted ).not.toHaveBeenCalled();
                expect( UserCollection.update ).not.toHaveBeenCalled();
            } );

            it( 'should throw if not updating self and not an admin', function () {
                // Info
                const loggedUser = '666';
                const updateUser = '420';
                // Spies
                UserHelpers.isSuper.and.returnValue( false );
                // Call
                expect( ()=>UserServices._updateProfileCompletion.call( {userId: loggedUser}, updateUser ) ).toThrowError( /unauthorized/ );
                // Expectations
                expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
                expect( UserCollection.findOne ).not.toHaveBeenCalled();
                expect( UserHelpers.isProfileCompleted ).not.toHaveBeenCalled();
                expect( UserCollection.update ).not.toHaveBeenCalled();
            } );

            it( 'should throw if not logged in (or not bound to a Meteor method)', function () {
                // Call
                expect( ()=>UserServices._updateProfileCompletion( '420' ) ).toThrowError( /unauthorized/ );
                // Expectations
                expect( UserHelpers.isSuper ).not.toHaveBeenCalled();
                expect( UserCollection.findOne ).not.toHaveBeenCalled();
                expect( UserHelpers.isProfileCompleted ).not.toHaveBeenCalled();
                expect( UserCollection.update ).not.toHaveBeenCalled();
            } );

        } );

        describe( 'update', function () {

            beforeEach( function () {
                spyOn( UserHelpers, 'isSuper' );
                spyOn( UserCollection, 'findOne' );
                spyOn( UserServices, '_updateProfile' );
                spyOn( UserServices, '_updateProfileCompletion' );
                spyOn( UserServices, '_updateEmail' );
                spyOn( UserCollection, 'update' );
            } );

            it( 'should exist in the service', function () {
                expect( UserServices.update ).toBeDefined();
            } );

            it( 'the method wrapper should be exposed in the namespace', function () {
                expect( PR.Accounts.methods.update ).toBeDefined();
            } );

            it( 'should update', function () {
                // Info
                const loggedUser = '666';
                const info       = {
                    _id:     '420',
                    email:   'after@email.com',
                    profile: {firstName: 'Test', lastName: 'User'}
                };
                const user       = {
                    _id:       info._id,
                    emails:    [{address: 'before@email.com', verified: true}],
                    completed: false
                };
                // Spies
                UserHelpers.isSuper.and.returnValue( true );
                UserCollection.findOne.and.returnValue( user );
                // Call
                expect( () => UserServices.update.call( {userId: loggedUser}, _.clone( info ) ) ).not.toThrow();
                // Expectations
                expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
                expect( UserCollection.findOne ).toHaveBeenCalledWith( {_id: info._id}, {fields: {completed: 1}} );
                expect( UserServices._updateProfile ).toHaveBeenCalledWith( info );
                expect( UserServices._updateProfileCompletion ).toHaveBeenCalledWith( info._id );
                expect( UserServices._updateEmail ).toHaveBeenCalledWith( info );
            } );

            it( 'should update self when not an admin', function () {
                // Info
                const loggedUser = '420';
                const info       = {
                    _id:     '420',
                    email:   'after@email.com',
                    profile: {firstName: 'Test', lastName: 'User'}
                };
                const user       = {
                    _id:       info._id,
                    emails:    [{address: 'before@email.com', verified: true}],
                    completed: false
                };
                // Spies
                UserHelpers.isSuper.and.returnValue( true );
                UserCollection.findOne.and.returnValue( user );
                // Call
                expect( () => UserServices.update.call( {userId: loggedUser}, _.clone( info ) ) ).not.toThrow();
                // Expectations
                expect( UserHelpers.isSuper ).not.toHaveBeenCalled( );
                expect( UserCollection.findOne ).toHaveBeenCalledWith( {_id: info._id}, {fields: {completed: 1}} );
                expect( UserServices._updateProfile ).toHaveBeenCalledWith( info );
                expect( UserServices._updateProfileCompletion ).toHaveBeenCalledWith( info._id );
                expect( UserServices._updateEmail ).toHaveBeenCalledWith( info );
            } );

            it( 'should throw if not updating self and not an admin', function () {
                // Info
                const loggedUser = '666';
                const info       = {
                    _id:     '420',
                    email:   'after@email.com',
                    profile: {firstName: 'Test', lastName: 'User'}
                };
                // Spies
                UserHelpers.isSuper.and.returnValue( false );
                // Call
                expect( () => UserServices.update.call( {userId: loggedUser}, _.clone( info ) ) ).toThrowError(/unauthorized/);
                // Expectations
                expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
                expect( UserCollection.findOne ).not.toHaveBeenCalled();
                expect( UserServices._updateProfile ).not.toHaveBeenCalled();
                expect( UserServices._updateProfileCompletion ).not.toHaveBeenCalled();
                expect( UserServices._updateEmail ).not.toHaveBeenCalled();
            } );

            it( 'should throw if user is not found', function () {
                // Info
                const loggedUser = '666';
                const info       = {
                    _id:     '420',
                    email:   'after@email.com',
                    profile: {firstName: 'Test', lastName: 'User'}
                };
                // Spies
                UserHelpers.isSuper.and.returnValue( true );
                UserCollection.findOne.and.returnValue( null );
                // Call
                expect( () => UserServices.update.call( {userId: loggedUser}, _.clone( info ) ) ).toThrowError(/user\ not\ found/);
                // Expectations
                expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
                expect( UserCollection.findOne ).toHaveBeenCalledWith( {_id: info._id}, {fields: {completed: 1}} );
                expect( UserServices._updateProfile ).not.toHaveBeenCalled();
                expect( UserServices._updateProfileCompletion ).not.toHaveBeenCalled();
                expect( UserServices._updateEmail ).not.toHaveBeenCalled();
            } );

            it( 'should not update profile if it hasn\'t been provided', function () {
                // Info
                const loggedUser = '666';
                const info       = {
                    _id:     '420',
                    email:   'after@email.com'
                };
                const user       = {
                    _id:       info._id,
                    emails:    [{address: 'before@email.com', verified: true}],
                    completed: false
                };
                // Spies
                UserHelpers.isSuper.and.returnValue( true );
                UserCollection.findOne.and.returnValue( user );
                // Call
                expect( () => UserServices.update.call( {userId: loggedUser}, _.clone( info ) ) ).not.toThrow();
                // Expectations
                expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
                expect( UserCollection.findOne ).toHaveBeenCalledWith( {_id: info._id}, {fields: {completed: 1}} );
                expect( UserServices._updateProfile ).not.toHaveBeenCalled();
                expect( UserServices._updateProfileCompletion ).toHaveBeenCalledWith( info._id );
                expect( UserServices._updateEmail ).toHaveBeenCalledWith( info );
            } );

            it( 'should not update email if it hasn\'t been provided', function () {
                // Info
                const loggedUser = '666';
                const info       = {
                    _id:     '420',
                    profile: {firstName: 'Test', lastName: 'User'}
                };
                const user       = {
                    _id:       info._id,
                    emails:    [{address: 'before@email.com', verified: true}],
                    completed: false
                };
                // Spies
                UserHelpers.isSuper.and.returnValue( true );
                UserCollection.findOne.and.returnValue( user );
                // Call
                expect( () => UserServices.update.call( {userId: loggedUser}, _.clone( info ) ) ).not.toThrow();
                // Expectations
                expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
                expect( UserCollection.findOne ).toHaveBeenCalledWith( {_id: info._id}, {fields: {completed: 1}} );
                expect( UserServices._updateProfile ).toHaveBeenCalledWith( info );
                expect( UserServices._updateProfileCompletion ).toHaveBeenCalledWith( info._id );
                expect( UserServices._updateEmail ).not.toHaveBeenCalled();
            } );

            it( 'should not update completion if neither email nor profile has been provided', function () {
                // Info
                const loggedUser = '666';
                const info       = {
                    _id:     '420'
                };
                const user       = {
                    _id:       info._id,
                    emails:    [{address: 'before@email.com', verified: true}],
                    completed: false
                };
                // Spies
                UserHelpers.isSuper.and.returnValue( true );
                UserCollection.findOne.and.returnValue( user );
                // Call
                expect( () => UserServices.update.call( {userId: loggedUser}, _.clone( info ) ) ).not.toThrow();
                // Expectations
                expect( UserHelpers.isSuper ).toHaveBeenCalledWith( loggedUser );
                expect( UserCollection.findOne ).toHaveBeenCalledWith( {_id: info._id}, {fields: {completed: 1}} );
                expect( UserServices._updateProfile ).not.toHaveBeenCalled();
                expect( UserServices._updateProfileCompletion ).not.toHaveBeenCalled();
                expect( UserServices._updateEmail ).not.toHaveBeenCalled();
            } );


            // The rest is client-only or server-only

        } );
    } );
} );

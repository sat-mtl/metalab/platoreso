describe( 'pr-accounts', function () {

    describe( 'methods', function () {

        it( 'shared service should exist', function () {
            expect( PR.Accounts.UserServices ).toBeDefined();
        } );

        if ( Meteor.isClient ) {
            it( 'server service should not exist', function () {
                expect( PR.Accounts.UserServicesServer ).toBeUndefined();
            } );
        }

        it( 'methods should be exposed', function () {
            expect( PR.Accounts.methods ).toBeDefined();
        } );

        describe( 'setPassword', function () {

            if ( Meteor.isServer ) {
                it( 'should exist in the server service', function () {
                    expect( PR.Accounts.UserServicesServer.setPassword ).toBeDefined();
                } );
            }

            it( 'should not exist in the shared service', function () {
                expect( PR.Accounts.UserServices.setPassword ).toBeUndefined();
            } );

            it( 'the method wrapper should be exposed in the namespace', function () {
                expect( PR.Accounts.methods.setPassword ).toBeDefined();
            } );

        } );
    } );
} );

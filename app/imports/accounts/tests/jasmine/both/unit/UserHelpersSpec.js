"use strict";

const { UserHelpers, RoleGroups, RoleList } = PR.Accounts;

describe( 'pr-accounts/both/unit/UserHelpers', function () {

    it( 'should exist', function () {
        expect( PR.Accounts.UserHelpers ).toBeDefined();
    } );

    describe( 'hasEmail', function () {

        it( 'should tell if a user has an email', function () {
            const user = { emails: [{ address: 'test@emails.com' }] };
            expect( UserHelpers.hasEmail( user ) ).toBe( true );
        } );

        it( 'should tell if a user has at least one email', function () {
            const user = { emails: [{ something: 'else' }, { address: 'test@emails.com' }] };
            expect( UserHelpers.hasEmail( user ) ).toBe( true );
        } );

        it( 'should tell if a user does not have an email', function () {
            const user = { emails: [{ something: 'else' }] };
            expect( UserHelpers.hasEmail( user ) ).toBe( false );
        } );

        it( 'should tell if a user does not have any emails', function () {
            const user = { something: 'else' };
            expect( UserHelpers.hasEmail( user ) ).toBe( false );
        } );

        it( 'return false when no user is provided', function () {
            expect( UserHelpers.hasEmail( null ) ).toBe( false );
        } );
    } );

    describe( 'hasVerifiedEmail', function () {

        it( 'should tell if a user has a verified email', function () {
            const user = { emails: [{ address: 'test@emails.com', verified: true }] };
            expect( UserHelpers.hasVerifiedEmail( user ) ).toBe( true );
        } );

        it( 'should tell if a user does not have a verified email 1', function () {
            const user = { emails: [{ address: 'test@emails.com', verified: false }] };
            expect( UserHelpers.hasVerifiedEmail( user ) ).toBe( false );
        } );

        it( 'should tell if a user does not have a verified email 2', function () {
            const user = { emails: [{ address: 'test@emails.com' }] };
            expect( UserHelpers.hasVerifiedEmail( user ) ).toBe( false );
        } );

        it( 'should tell if a user does not have a verified email 3', function () {
            const user = { emails: [{ something: 'else' }, { address: 'test@emails.com' }] };
            expect( UserHelpers.hasVerifiedEmail( user ) ).toBe( false );
        } );

        it( 'should tell if a user does not have a verified email 4', function () {
            const user = { something: 'else' };
            expect( UserHelpers.hasVerifiedEmail( user ) ).toBe( false );
        } );

        it( 'return false when no user is provided', function () {
            expect( UserHelpers.hasVerifiedEmail( null ) ).toBe( false );
        } );

    } );

    describe( 'isProfileCompleted', function () {

        it( 'should tell if a profile is completed', function () {
            const user = {
                emails:  [{ address: 'test@email.com', verified: true }],
                profile: { firstName: 'First', lastName: 'Last' }
            };
            expect( UserHelpers.isProfileCompleted( user ) ).toBe( true );
        } );

        it( 'should tell if a profile is not completed (not verified)', function () {
            const user = {
                emails:  [{ address: 'test@email.com', verified: false }],
                profile: { firstName: 'First', lastName: 'Last' }
            };
            expect( UserHelpers.isProfileCompleted( user ) ).toBe( false );
        } );

        it( 'should tell if a profile is not completed (no first name)', function () {
            const user = {
                emails:  [{ address: 'test@email.com', verified: true }],
                profile: { lastName: 'Last' }
            };
            expect( UserHelpers.isProfileCompleted( user ) ).toBe( false );
        } );

        it( 'should tell if a profile is not completed (no last name)', function () {
            const user = {
                emails:  [{ address: 'test@email.com', verified: true }],
                profile: { firstName: 'First' }
            };
            expect( UserHelpers.isProfileCompleted( user ) ).toBe( false );
        } );

        it( 'should tell if a profile is not completed (no profile)', function () {
            const user = {
                emails: [{ address: 'test@email.com', verified: true }]
            };
            expect( UserHelpers.isProfileCompleted( user ) ).toBe( false );
        } );

        it( 'should tell if a profile is not completed (no emails)', function () {
            const user = {
                profile: { firstName: 'First', lastName: 'Last' }
            };
            expect( UserHelpers.isProfileCompleted( user ) ).toBe( false );
        } );

        it( 'should tell if a profile is not completed (no user)', function () {
            expect( UserHelpers.isProfileCompleted( null ) ).toBe( false );
        } );

    } );

    describe( 'isAdmin', function () {

        it( 'should relay the info to roles (no group provided, not explicit)', function () {
            const user = '420';
            spyOn( Roles, 'userIsInRole' );
            UserHelpers.isAdmin( user );
            expect( Roles.userIsInRole ).toHaveBeenCalledWith( user, RoleGroups.admins, null );
        } );

        it( 'should relay the info to roles (group provided, not explicit)', function () {
            const user  = '420';
            const group = 'janitors';
            spyOn( Roles, 'userIsInRole' );
            UserHelpers.isAdmin( user, group );
            expect( Roles.userIsInRole ).toHaveBeenCalledWith( user, RoleGroups.admins, group );
        } );

        it( 'should relay the info to roles (group provided, explicit)', function () {
            const user  = '420';
            const group = 'janitors';
            spyOn( Roles, 'userIsInRole' );
            UserHelpers.isAdmin( user, group, true );
            expect( Roles.userIsInRole ).toHaveBeenCalledWith( user, RoleList.admin, group );
        } );

        it( 'should relay the info to roles (no group provided, explicit)', function () {
            const user = '420';
            spyOn( Roles, 'userIsInRole' );
            UserHelpers.isAdmin( user, null, true );
            expect( Roles.userIsInRole ).toHaveBeenCalledWith( user, RoleList.admin, null );
        } );

        it( 'should return false if no user', function () {
            spyOn( Roles, 'userIsInRole' );
            expect( UserHelpers.isAdmin( null, null, true ) ).toBe( false );
            expect( Roles.userIsInRole ).not.toHaveBeenCalled()
        } );

    } );

    describe( 'isSuper', function () {

        it( 'should relay the info to roles (no group provided, not explicit)', function () {
            const user = '420';
            spyOn( Roles, 'userIsInRole' );
            UserHelpers.isSuper( user );
            expect( Roles.userIsInRole ).toHaveBeenCalledWith( user, RoleGroups.supers, null );
        } );

        it( 'should relay the info to roles (group provided, not explicit)', function () {
            const user  = '420';
            const group = 'janitors';
            spyOn( Roles, 'userIsInRole' );
            UserHelpers.isSuper( user, group );
            expect( Roles.userIsInRole ).toHaveBeenCalledWith( user, RoleGroups.supers, group );
        } );

        it( 'should relay the info to roles (group provided, explicit)', function () {
            const user  = '420';
            const group = 'janitors';
            spyOn( Roles, 'userIsInRole' );
            UserHelpers.isSuper( user, group, true );
            expect( Roles.userIsInRole ).toHaveBeenCalledWith( user, RoleList.super, group );
        } );

        it( 'should relay the info to roles (no group provided, explicit)', function () {
            const user = '420';
            spyOn( Roles, 'userIsInRole' );
            UserHelpers.isSuper( user, null, true );
            expect( Roles.userIsInRole ).toHaveBeenCalledWith( user, RoleList.super, null );
        } );

        it( 'should return false if no user', function () {
            spyOn( Roles, 'userIsInRole' );
            expect( UserHelpers.isSuper( null, null, true ) ).toBe( false );
            expect( Roles.userIsInRole ).not.toHaveBeenCalled()
        } );

    } );

    describe( 'isExpert', function () {

        it( 'should relay the info to roles (no group provided, not explicit)', function () {
            const user = '420';
            spyOn( Roles, 'userIsInRole' );
            UserHelpers.isExpert( user );
            expect( Roles.userIsInRole ).toHaveBeenCalledWith( user, RoleGroups.experts, null );
        } );

        it( 'should relay the info to roles (group provided, not explicit)', function () {
            const user  = '420';
            const group = 'janitors';
            spyOn( Roles, 'userIsInRole' );
            UserHelpers.isExpert( user, group );
            expect( Roles.userIsInRole ).toHaveBeenCalledWith( user, RoleGroups.experts, group );
        } );

        it( 'should relay the info to roles (group provided, explicit)', function () {
            const user  = '420';
            const group = 'janitors';
            spyOn( Roles, 'userIsInRole' );
            UserHelpers.isExpert( user, group, true );
            expect( Roles.userIsInRole ).toHaveBeenCalledWith( user, RoleList.expert, group );
        } );

        it( 'should relay the info to roles (no group provided, explicit)', function () {
            const user = '420';
            spyOn( Roles, 'userIsInRole' );
            UserHelpers.isExpert( user, null, true );
            expect( Roles.userIsInRole ).toHaveBeenCalledWith( user, RoleList.expert, null );
        } );

        it( 'should return false if no user', function () {
            spyOn( Roles, 'userIsInRole' );
            expect( UserHelpers.isExpert( null, null, true ) ).toBe( false );
            expect( Roles.userIsInRole ).not.toHaveBeenCalled()
        } );

    } );

    describe( 'isModerator', function () {

        it( 'should relay the info to roles (no group provided, not explicit)', function () {
            const user = '420';
            spyOn( Roles, 'userIsInRole' );
            UserHelpers.isModerator( user );
            expect( Roles.userIsInRole ).toHaveBeenCalledWith( user, RoleGroups.moderators, null );
        } );

        it( 'should relay the info to roles (group provided, not explicit)', function () {
            const user  = '420';
            const group = 'janitors';
            spyOn( Roles, 'userIsInRole' );
            UserHelpers.isModerator( user, group );
            expect( Roles.userIsInRole ).toHaveBeenCalledWith( user, RoleGroups.moderators, group );
        } );

        it( 'should relay the info to roles (group provided, explicit)', function () {
            const user  = '420';
            const group = 'janitors';
            spyOn( Roles, 'userIsInRole' );
            UserHelpers.isModerator( user, group, true );
            expect( Roles.userIsInRole ).toHaveBeenCalledWith( user, RoleList.moderator, group );
        } );

        it( 'should relay the info to roles (no group provided, explicit)', function () {
            const user = '420';
            spyOn( Roles, 'userIsInRole' );
            UserHelpers.isModerator( user, null, true );
            expect( Roles.userIsInRole ).toHaveBeenCalledWith( user, RoleList.moderator, null );
        } );

        it( 'should return false if no user', function () {
            spyOn( Roles, 'userIsInRole' );
            expect( UserHelpers.isModerator( null, null, true ) ).toBe( false );
            expect( Roles.userIsInRole ).not.toHaveBeenCalled()
        } );

    } );

    describe( 'isMember', function () {

        it( 'should relay the info to roles (no group provided, not explicit)', function () {
            const user = '420';
            spyOn( Roles, 'userIsInRole' );
            UserHelpers.isMember( user );
            expect( Roles.userIsInRole ).toHaveBeenCalledWith( user, RoleGroups.members, null );
        } );

        it( 'should relay the info to roles (group provided, not explicit)', function () {
            const user  = '420';
            const group = 'janitors';
            spyOn( Roles, 'userIsInRole' );
            UserHelpers.isMember( user, group );
            expect( Roles.userIsInRole ).toHaveBeenCalledWith( user, RoleGroups.members, group );
        } );

        it( 'should relay the info to roles (group provided, explicit)', function () {
            const user  = '420';
            const group = 'janitors';
            spyOn( Roles, 'userIsInRole' );
            UserHelpers.isMember( user, group, true );
            expect( Roles.userIsInRole ).toHaveBeenCalledWith( user, RoleList.member, group );
        } );

        it( 'should relay the info to roles (no group provided, explicit)', function () {
            const user = '420';
            spyOn( Roles, 'userIsInRole' );
            UserHelpers.isMember( user, null, true );
            expect( Roles.userIsInRole ).toHaveBeenCalledWith( user, RoleList.member, null );
        } );

        it( 'should return false if no user', function () {
            spyOn( Roles, 'userIsInRole' );
            expect( UserHelpers.isMember( null, null, true ) ).toBe( false );
            expect( Roles.userIsInRole ).not.toHaveBeenCalled()
        } );

    } );

    describe( 'isUser', function () {

        it( 'should relay the info to roles (no group provided, not explicit)', function () {
            const user = '420';
            spyOn( Roles, 'userIsInRole' );
            UserHelpers.isUser( user );
            expect( Roles.userIsInRole ).toHaveBeenCalledWith( user, RoleGroups.users, null );
        } );

        it( 'should relay the info to roles (group provided, not explicit)', function () {
            const user  = '420';
            const group = 'janitors';
            spyOn( Roles, 'userIsInRole' );
            UserHelpers.isUser( user, group );
            expect( Roles.userIsInRole ).toHaveBeenCalledWith( user, RoleGroups.users, group );
        } );

        it( 'should relay the info to roles (group provided, explicit)', function () {
            const user  = '420';
            const group = 'janitors';
            spyOn( Roles, 'userIsInRole' );
            UserHelpers.isUser( user, group, true );
            expect( Roles.userIsInRole ).toHaveBeenCalledWith( user, RoleList.user, group );
        } );

        it( 'should relay the info to roles (no group provided, explicit)', function () {
            const user = '420';
            spyOn( Roles, 'userIsInRole' );
            UserHelpers.isUser( user, null, true );
            expect( Roles.userIsInRole ).toHaveBeenCalledWith( user, RoleList.user, null );
        } );

        it( 'should return false if no user', function () {
            spyOn( Roles, 'userIsInRole' );
            expect( UserHelpers.isUser( null, null, true ) ).toBe( false );
            expect( Roles.userIsInRole ).not.toHaveBeenCalled()
        } );

    } );

    describe( 'getRolesForUser', function () {
        it( 'is just a wrapper for Roles.getRolesForUser()', function () {
            spyOn( Roles, 'getRolesForUser' );
            UserHelpers.getRolesForUser( '123' );
            expect( Roles.getRolesForUser ).toHaveBeenCalledWith( '123' );
        } )
    } );

    describe( 'getGroupsForUser', function () {
        it( 'is just a wrapper for Roles.getGroupsForUser()', function () {
            spyOn( Roles, 'getGroupsForUser' );
            UserHelpers.getGroupsForUser( '123' );
            expect( Roles.getGroupsForUser ).toHaveBeenCalledWith( '123' );
        } )
    } );

    /**
     * Here we have to test using the app's roles
     * Otherwise the helper method will not be able to find the role levels it needs
     */
    describe( 'filterRoles', function () {

        let roles;
        let userId;

        beforeEach( function () {
            roles  = ['admin', 'super', 'expert', 'moderator', 'member', 'user'];
            userId = '420';
            spyOn( Roles, 'getRolesForUser' );
        } );

        it( 'should get roles for the passed user', function () {
            UserHelpers.filterRoles( roles, userId );
            expect( Roles.getRolesForUser ).toHaveBeenCalledWith( userId, Roles.GLOBAL_GROUP );
        } );

        it( 'should return nothing if no roles for the passed user', function () {
            Roles.getRolesForUser.and.returnValue( [] );
            expect( UserHelpers.filterRoles( roles, userId ) ).toEqual( [] );
        } );

        it( 'should return nothing if user is not found', function () {
            Roles.getRolesForUser.and.returnValue( null );
            expect( UserHelpers.filterRoles( roles, userId ) ).toEqual( [] );
        } );

        it( 'should let an admin specify any role', function () {
            Roles.getRolesForUser.and.returnValue( ['admin'] );
            expect( UserHelpers.filterRoles( roles, userId ) ).toEqual( ['admin', 'super', 'expert', 'moderator', 'member', 'user'] );
        } );

        it( 'should remove roles higher than super', function () {
            Roles.getRolesForUser.and.returnValue( ['super'] );
            expect( UserHelpers.filterRoles( roles, userId ) ).toEqual( ['super', 'expert', 'moderator', 'member', 'user'] );
        } );

        it( 'should remove roles higher than expert', function () {
            Roles.getRolesForUser.and.returnValue( ['expert'] );
            expect( UserHelpers.filterRoles( roles, userId ) ).toEqual( ['expert', 'moderator', 'member', 'user'] );
        } );

        it( 'should remove roles higher than expert', function () {
            Roles.getRolesForUser.and.returnValue( ['expert'] );
            expect( UserHelpers.filterRoles( roles, userId ) ).toEqual( ['expert', 'moderator', 'member', 'user'] );
        } );

        it( 'should remove roles higher than moderator', function () {
            Roles.getRolesForUser.and.returnValue( ['moderator'] );
            expect( UserHelpers.filterRoles( roles, userId ) ).toEqual( ['moderator', 'member', 'user'] );
        } );

        it( 'should remove roles higher than member', function () {
            Roles.getRolesForUser.and.returnValue( ['member'] );
            expect( UserHelpers.filterRoles( roles, userId ) ).toEqual( ['member', 'user'] );
        } );

        it( 'should remove roles higher than user', function () {
            Roles.getRolesForUser.and.returnValue( ['user'] );
            expect( UserHelpers.filterRoles( roles, userId ) ).toEqual( ['user'] );
        } );

        it( 'should remove unknown roles', function () {
            Roles.getRolesForUser.and.returnValue( ['admin'] );
            expect( UserHelpers.filterRoles( ['admin', 'super', 'some', 'fake', 'roles'], userId ) ).toEqual( ['admin', 'super'] );
        } );

    } );

    describe( 'getDisplayName', function () {
        it( 'should return a display name', function () {
            expect( UserHelpers.getDisplayName( {
                profile: {
                    firstName: 'First',
                    lastName:  'Last'
                }
            } ) ).toEqual( 'First Last' );
        } );
        it( 'should return a display name (only first name)', function () {
            expect( UserHelpers.getDisplayName( { profile: { firstName: 'First' } } ) ).toEqual( 'First' );
        } );
        it( 'should return a display name (only last name)', function () {
            expect( UserHelpers.getDisplayName( { profile: { lastName: 'Last' } } ) ).toEqual( 'Last' );
        } );
        it( 'should return a display name (email)', function () {
            expect( UserHelpers.getDisplayName( { emails: [{ address: 'test@email.com' }] } ) ).toEqual( 'test@email.com' );
        } );
        it( 'should return a display name (_id)', function () {
            expect( UserHelpers.getDisplayName( { _id: '420' } ) ).toEqual( '420' );
        } );
        it( 'should return a display name (nothing)', function () {
            expect( UserHelpers.getDisplayName( {} ) ).toEqual( '' );
        } );
    } );

} );
const userPublic = Meteor.server.publish_handlers['user/public'];
import UserCollection from "/imports/accounts/UserCollection";

describe( 'user/public publication', function () {

    let context;

    beforeEach( function () {
        context = {
            ready:  jasmine.createSpy( 'ready' )
        };
        spyOn( UserCollection, 'find' );
    } );

    describe( 'check passed', function () {

        describe( 'logged in', function () {

            beforeEach( function () {
                context.userId = '420';
            } );

            afterEach( function () {
                expect( context.ready ).not.toHaveBeenCalled();
            } );

            it( 'should publish', function () {
                userPublic.call( context, '666' );
                expect( UserCollection.find ).toHaveBeenCalledWith({_id: '666'}, jasmine.any(Object));
            } );
            
        } );

        describe( 'not logged in', function () {

            afterEach( function () {
                expect( UserCollection.find ).not.toHaveBeenCalled();
            } );

            it( 'should not publish', function () {
                userPublic.call( context, '666' );
            } );

        } );

    } );

    describe( 'check failed', function () {

        afterEach( function () {
            expect( context.ready ).not.toHaveBeenCalled();
            expect( UserCollection.find ).not.toHaveBeenCalled();
        } );

        it( 'should throw on bad id (undefined)', function () {
            expect( ()=>userPublic.call( context ) ).toThrowError( /Match\ error/ );
        } );

        it( 'should throw on bad id (number)', function () {
            expect( ()=>userPublic.call( context, 1234 ) ).toThrowError( /Match\ error/ );
        } );

        it( 'should throw on bad id (object)', function () {
            expect( ()=>userPublic.call( context, {not: 'a string'} ) ).toThrowError( /Match\ error/ );
        } );

    } );

} );
describe( 'pr-accounts', function () {

    describe( 'methods', function () {

        it( 'shared service should exist', function () {
            expect( PR.Accounts.UserServices ).toBeDefined();
        } );

        it( 'server service should exist', function () {
            expect( PR.Accounts.UserServicesServer ).toBeDefined();
        } );

        it( 'methods should be exposed', function () {
            expect( PR.Accounts.methods ).toBeDefined();
        } );

        describe( 'sendValidationEmail', function () {

            it( 'should exist in the service', function () {
                expect( PR.Accounts.UserServicesServer.sendValidationEmail ).toBeDefined();
            } );

            it( 'the method wrapper should be exposed in the namespace', function () {
                expect( PR.Accounts.methods.sendValidationEmail ).toBeDefined();
            } );

            it('should send a validation email to self if not provided', function() {
                const loggedUser = '420';
                spyOn(PR.Accounts.UserHelpers, 'isSuper');
                spyOn(Accounts, 'sendVerificationEmail');
                expect(()=>PR.Accounts.UserServicesServer.sendValidationEmail.call({userId: loggedUser})).not.toThrow();
                expect(PR.Accounts.UserHelpers.isSuper).not.toHaveBeenCalled();
                expect(Accounts.sendVerificationEmail).toHaveBeenCalledWith(loggedUser)
            });

            it('should send a validation email to self', function() {
                const loggedUser = '420';
                const validateUser = '420';
                spyOn(PR.Accounts.UserHelpers, 'isSuper');
                spyOn(Accounts, 'sendVerificationEmail');
                expect(()=>PR.Accounts.UserServicesServer.sendValidationEmail.call({userId: loggedUser}, validateUser)).not.toThrow();
                expect(PR.Accounts.UserHelpers.isSuper).not.toHaveBeenCalled();
                expect(Accounts.sendVerificationEmail).toHaveBeenCalledWith(validateUser)
            });

            it('should send a validation email to someone else if allowed', function() {
                const loggedUser = '420';
                const validateUser = '666';
                spyOn(PR.Accounts.UserHelpers, 'isSuper').and.returnValue(true);
                spyOn(Accounts, 'sendVerificationEmail');
                expect(()=>PR.Accounts.UserServicesServer.sendValidationEmail.call({userId: loggedUser}, validateUser)).not.toThrow();
                expect(PR.Accounts.UserHelpers.isSuper).toHaveBeenCalledWith(loggedUser);
                expect(Accounts.sendVerificationEmail).toHaveBeenCalledWith(validateUser)
            });

            it('should not send a validation email to someone else if not allowed', function() {
                const loggedUser = '420';
                const validateUser = '666';
                spyOn(PR.Accounts.UserHelpers, 'isSuper').and.returnValue(false);
                spyOn(Accounts, 'sendVerificationEmail');
                expect(()=>PR.Accounts.UserServicesServer.sendValidationEmail.call({userId: loggedUser}, validateUser)).toThrowError(/unauthorized/);
                expect(PR.Accounts.UserHelpers.isSuper).toHaveBeenCalledWith(loggedUser);
                expect(Accounts.sendVerificationEmail).not.toHaveBeenCalled()
            });

            it('should fail if not logged in (or not bound to a Meteor method))', function() {
                const validateUser = '666';
                spyOn(PR.Accounts.UserHelpers, 'isSuper');
                spyOn(Accounts, 'sendVerificationEmail');
                expect(()=>PR.Accounts.UserServicesServer.sendValidationEmail(validateUser)).toThrowError(/unauthorized/);
                expect(PR.Accounts.UserHelpers.isSuper).not.toHaveBeenCalled();
                expect(Accounts.sendVerificationEmail).not.toHaveBeenCalled()
            });
        } );
    } );
} );

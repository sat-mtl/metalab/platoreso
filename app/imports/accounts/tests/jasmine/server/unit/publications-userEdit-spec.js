const userEdit = Meteor.server.publish_handlers['user/edit'];
import UserCollection from "/imports/accounts/UserCollection";

describe( 'user/edit publication', function () {

    let context;

    beforeEach( function () {
        context = {
            ready:  jasmine.createSpy( 'ready' )
        };
        spyOn( UserCollection, 'find' );
    } );

    describe( 'logged in', function () {

        beforeEach( function () {
            context.userId = '420';
        } );

        afterEach( function() {
            expect( context.ready ).not.toHaveBeenCalled();
        });

        it( 'should publish', function () {
            userEdit.call( context );
            expect( UserCollection.find ).toHaveBeenCalledWith({_id: context.userId}, jasmine.any(Object));
        } );

    } );

    describe( 'not logged in', function () {

        afterEach( function () {
            expect( UserCollection.find ).not.toHaveBeenCalled();
        } );

        it( 'should not publish', function () {
            userEdit.call( context );
        } );

    } );

} );
const {AccountsCallbacks} = PR.Accounts;

describe('Accounts Callbacks', function() {

    it('should be exposed in the namespace', function() {
        expect(PR.Accounts.AccountsCallbacks).toBeDefined();
    });

    describe('validate login attempt', function() {

        it('should be defined', function() {
            expect(AccountsCallbacks.validateLoginAttempt).toBeDefined();
        });

        it('should allow an enabled user to login', function(){
            expect(AccountsCallbacks.validateLoginAttempt({allowed: true, user:{enabled: true}})).toBe(true);
        });

        it('should respect previously denied attempts', function(){
            expect(AccountsCallbacks.validateLoginAttempt({allowed: false})).toBe(false);
        });

        it('should not allow a disabled user to log in', function(){
            expect(()=>AccountsCallbacks.validateLoginAttempt({allowed: true, user:{enabled: false}})).toThrowError(/account\ disabled/);
        });

        it('should not allow a non explicitely enabled user to log in', function(){
            expect(()=>AccountsCallbacks.validateLoginAttempt({allowed: true, user:{}})).toThrowError(/account\ disabled/);
        });

    });

});
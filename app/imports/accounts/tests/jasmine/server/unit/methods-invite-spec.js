"use strict";

const { UserServices, UserHelpers, RoleList } = PR.Accounts;

describe( 'pr-accounts/server/unit/method/invite', function () {

    // Previous tests already done in common unit code
    // So this only tests the server part of the method
    // Any validation and role checking is assumed to already have been tested
    // @see /tests/jasmine/both/unit/methods-invite-spec.js

    let loggedUser;
    let userId;

    beforeEach( function () {
        loggedUser = '420';
        userId     = '666';
        spyOn( UserHelpers, 'isExpert' ).and.returnValue( true );
        spyOn( UserHelpers, 'filterRoles' ).and.callFake( ( roles, userId )=>roles ); // Just return everything, we test this helper somewhere else
        spyOn( Accounts, 'createUser' ).and.returnValue( userId );
        spyOn( Accounts, 'sendEnrollmentEmail' );
        spyOn( Roles, 'getRolesForUser' );
        spyOn( Roles, 'addUsersToRoles' );
        spyOn( Roles, 'setUserRoles' );
    } );

    afterEach( function () {

    } );

    it( 'should invite', function () {
        const info = { email: 'me@you.com', profile: { firstName: 'First', lastName: 'Last' } };
        expect( UserServices.invite.call( { userId: loggedUser }, info ) ).toEqual( userId );
        expect( Accounts.createUser ).toHaveBeenCalledWith( {
            email:   'me@you.com',
            profile: { firstName: 'First', lastName: 'Last' }
        } );
        expect( UserHelpers.filterRoles ).toHaveBeenCalledWith( [], loggedUser );
        expect( Roles.addUsersToRoles ).toHaveBeenCalledWith( userId, [RoleList.user], Roles.GLOBAL_GROUP );
        expect( Roles.setUserRoles ).not.toHaveBeenCalled();
        expect( Accounts.sendEnrollmentEmail ).toHaveBeenCalledWith( userId );
    } );

    it( 'should invite with custom roles', function () {
        const roles = ['garbage', 'collector'];
        const info  = {
            email:   'me@you.com',
            profile: {
                firstName: 'Test',
                lastName:  'User'
            },
            roles:   roles
        };
        expect( UserServices.invite.call( { userId: loggedUser }, info ) ).toEqual( userId );
        expect( Accounts.createUser ).toHaveBeenCalledWith( {
            email:   'me@you.com',
            profile: { firstName: 'Test', lastName: 'User' }
        } );
        expect( UserHelpers.filterRoles ).toHaveBeenCalledWith( roles, loggedUser );
        expect( Roles.addUsersToRoles ).toHaveBeenCalledWith( userId, roles.concat([RoleList.user]), Roles.GLOBAL_GROUP );
        expect( Roles.setUserRoles ).not.toHaveBeenCalled();
        expect( Accounts.sendEnrollmentEmail ).toHaveBeenCalledWith( userId );
    } );

    it( 'should invite with group permissions', function () {
        const groups = {
            commenters: ['captain obvious', 'white knight', 'troll'],
            other:      ['lack', 'of', 'imagination']
        };
        const info   = {
            email:   'me@you.com',
            profile: {
                firstName: 'Test',
                lastName:  'User'
            },
            groups:  groups
        };
        expect( UserServices.invite.call( { userId: loggedUser }, info ) ).toEqual( userId );
        expect( Accounts.createUser ).toHaveBeenCalledWith( {
            email:   'me@you.com',
            profile: { firstName: 'Test', lastName: 'User' }
        } );
        expect( UserHelpers.filterRoles ).toHaveBeenCalledWith( [], loggedUser );
        expect( Roles.addUsersToRoles ).toHaveBeenCalledWith( userId, [RoleList.user], Roles.GLOBAL_GROUP );
        expect( Roles.setUserRoles.calls.count() ).toEqual( 2 );
        expect( Roles.setUserRoles.calls.argsFor( 0 ) ).toEqual( [userId, groups.commenters, 'commenters'] );
        expect( Roles.setUserRoles.calls.argsFor( 1 ) ).toEqual( [userId, groups.other, 'other'] );
        expect( Accounts.sendEnrollmentEmail ).toHaveBeenCalledWith( userId );
    } );

    it( 'should invite with custom roles and group permissions', function () {
        const roles  = ['garbage', 'collector'];
        const groups = {
            commenters: ['captain obvious', 'white knight', 'troll'],
            other:      ['lack', 'of', 'imagination']
        };
        const info   = {
            email:   'me@you.com',
            profile: {
                firstName: 'Test',
                lastName:  'User'
            },
            roles:   roles,
            groups:  groups
        };
        expect( UserServices.invite.call( { userId: loggedUser }, info ) ).toEqual( userId );
        expect( Accounts.createUser ).toHaveBeenCalledWith( {
            email:   'me@you.com',
            profile: { firstName: 'Test', lastName: 'User' }
        } );
        expect( UserHelpers.filterRoles ).toHaveBeenCalledWith( roles, loggedUser );
        expect( Roles.addUsersToRoles ).toHaveBeenCalledWith( userId, roles.concat([RoleList.user]), Roles.GLOBAL_GROUP );
        expect( Roles.setUserRoles.calls.count() ).toEqual( 2 );
        expect( Roles.setUserRoles.calls.argsFor( 0 ) ).toEqual( [userId, groups.commenters, 'commenters'] );
        expect( Roles.setUserRoles.calls.argsFor( 1 ) ).toEqual( [userId, groups.other, 'other'] );
        expect( Accounts.sendEnrollmentEmail ).toHaveBeenCalledWith( userId );
    } );

    it( 'should not choke on invalid groups', function () {
        const info = {
            email:  'me@you.com', profile: { firstName: 'First', lastName: 'Last' },
            groups: 'not a groups object'
        };
        expect( UserServices.invite.call( { userId: loggedUser }, info ) ).toEqual( userId );
        expect( Accounts.createUser ).toHaveBeenCalledWith( {
            email:   'me@you.com',
            profile: { firstName: 'First', lastName: 'Last' }
        } );
        expect( UserHelpers.filterRoles ).toHaveBeenCalledWith( [], loggedUser );
        expect( Roles.addUsersToRoles ).toHaveBeenCalledWith( userId, [RoleList.user], Roles.GLOBAL_GROUP );
        expect( Roles.setUserRoles ).not.toHaveBeenCalled();
        expect( Accounts.sendEnrollmentEmail ).toHaveBeenCalledWith( userId );
    } );

    it( 'should not choke on invalid groups', function () {
        const info = {
            email:  'me@you.com', profile: { firstName: 'First', lastName: 'Last' },
            groups: { test: 'not a roles array' }
        };
        expect( UserServices.invite.call( { userId: loggedUser }, info ) ).toEqual( userId );
        expect( Accounts.createUser ).toHaveBeenCalledWith( {
            email:   'me@you.com',
            profile: { firstName: 'First', lastName: 'Last' }
        } );
        expect( UserHelpers.filterRoles ).toHaveBeenCalledWith( [], loggedUser );
        expect( Roles.addUsersToRoles ).toHaveBeenCalledWith( userId, [RoleList.user], Roles.GLOBAL_GROUP );
        expect( Roles.setUserRoles ).not.toHaveBeenCalled();
        expect( Accounts.sendEnrollmentEmail ).toHaveBeenCalledWith( userId );
    } );

    // From here, more concise tests, the rest has been tested before

    /**
     * filterRoles is tested somewhere else, just make sure we use what it returns
     */
    it( 'should respect the value returned by filter roles', function () {
        UserHelpers.filterRoles.and.returnValue( ['something', 'else'] );
        const info = { email: 'me@you.com', profile: { firstName: 'First', lastName: 'Last' }, roles: ['some', 'roles'] };
        expect( UserServices.invite.call( { userId: loggedUser }, info ) ).toEqual( userId );
        expect( UserHelpers.filterRoles ).toHaveBeenCalledWith( ['some', 'roles'], loggedUser );
        expect( Roles.addUsersToRoles ).toHaveBeenCalledWith( userId, ['something', 'else', RoleList.user], Roles.GLOBAL_GROUP );
    } );

    it( 'should not add user to any passed roles if none returned by filterRoles', function () {
        UserHelpers.filterRoles.and.returnValue( [] );
        const info = { email: 'me@you.com', profile: { firstName: 'First', lastName: 'Last' }, roles: ['some', 'roles'] };
        expect( UserServices.invite.call( { userId: loggedUser }, info ) ).toEqual( userId );
        expect( UserHelpers.filterRoles ).toHaveBeenCalledWith( ['some', 'roles'], loggedUser );
        expect( Roles.addUsersToRoles ).toHaveBeenCalledWith( userId, [RoleList.user], Roles.GLOBAL_GROUP );
    } );
} );
const { AccountsMeldHelper } = PR.Accounts;

describe( 'Account Melding Helpers', function () {

    beforeEach(()=>{
        // Reset all callbacks
        // Since we are not running in a real isolated unit test environment
        // Other packages might already have registered callbacks that will
        // disrupt our tests here
        AccountsMeldHelper._reset();
    });

    it( 'should be exposed in namespace', function () {
        expect( PR.Accounts.AccountsMeldHelper ).toBeDefined();
    } );

    it( 'should register meld callbacks', function () {
        const cb = jasmine.createSpy( 'meldCallback' );
        AccountsMeldHelper.onMeld( cb );
        AccountsMeldHelper._meldDBCallback( '420', '666' );
        expect( cb ).toHaveBeenCalledWith( '420', '666' );
    } );

    it( 'should register multiple meld callbacks', function () {
        const cb1 = jasmine.createSpy( 'meldCallback1' );
        const cb2 = jasmine.createSpy( 'meldCallback2' );
        AccountsMeldHelper.onMeld( cb1 );
        AccountsMeldHelper.onMeld( cb2 );
        AccountsMeldHelper._meldDBCallback( '420', '666' );
        expect( cb1 ).toHaveBeenCalledWith( '420', '666' );
        expect( cb2 ).toHaveBeenCalledWith( '420', '666' );
    } );

    it( 'should register serviceAdded callbacks', function () {
        const cb = jasmine.createSpy( 'serviceAddedCallback' );
        AccountsMeldHelper.onServiceAdded( cb );
        AccountsMeldHelper._serviceAddedCallback( '420', '666' );
        expect( cb ).toHaveBeenCalledWith( '420', '666' );
    } );

    it( 'should register multiple serviceAdded callbacks', function () {
        const cb1 = jasmine.createSpy( 'serviceAddedCallback1' );
        const cb2 = jasmine.createSpy( 'serviceAddedCallback2' );
        AccountsMeldHelper.onServiceAdded( cb1 );
        AccountsMeldHelper.onServiceAdded( cb2 );
        AccountsMeldHelper._serviceAddedCallback( '420', 'service name' );
        expect( cb1 ).toHaveBeenCalledWith( '420', 'service name' );
        expect( cb2 ).toHaveBeenCalledWith( '420', 'service name' );
    } );

    it( 'should keep oldest creation date (from src)', function () {
        const src_user    = {
            createdAt: new Date( (new Date()) - 1000 )
        };
        const dst_user    = {
            createdAt: new Date()
        };
        const merged_user = {
            createdAt: new Date( src_user.createdAt )
        };
        expect( AccountsMeldHelper._meldUserCallback( src_user, dst_user ) ).toEqual( merged_user );
    } );

    it( 'should keep oldest creation date (from dst)', function () {
        const src_user    = {
            createdAt: new Date()
        };
        const dst_user    = {
            createdAt: new Date( (new Date()) - 1000 )
        };
        const merged_user = {
            createdAt: new Date( dst_user.createdAt )
        };
        expect( AccountsMeldHelper._meldUserCallback( src_user, dst_user ) ).toEqual( merged_user );
    } );

    it( 'should merge profiles with dst having priority', function () {
        const src_user    = {
            profile: {
                firstName: 'First',
                lastName:  'Last'
            }
        };
        const dst_user    = {
            profile: {
                firstName: 'New',
                extra:     'Prop'
            }
        };
        const merged_user = {
            profile: {
                firstName: 'New',
                lastName:  'Last',
                extra:     'Prop'
            }
        };
        expect( AccountsMeldHelper._meldUserCallback( src_user, dst_user ) ).toEqual( merged_user );
    } );

    it( 'should merge profiles when dst doesn\'t have one', function () {
        const src_user    = {
            profile: {
                firstName: 'First',
                lastName:  'Last'
            }
        };
        const dst_user    = {};
        const merged_user = {
            profile: {
                firstName: 'First',
                lastName:  'Last'
            }
        };
        expect( AccountsMeldHelper._meldUserCallback( src_user, dst_user ) ).toEqual( merged_user );
    } );

    it( 'should OR the enabled setting 1', function () {
        const src_user    = {
            enabled: false
        };
        const dst_user    = {
            enabled: true
        };
        const merged_user = {
            enabled: true
        };
        expect( AccountsMeldHelper._meldUserCallback( src_user, dst_user ) ).toEqual( merged_user );
    } );

    it( 'should OR the enabled setting 2', function () {
        const src_user    = {
            enabled: false
        };
        const dst_user    = {
            enabled: true
        };
        const merged_user = {
            enabled: true
        };
        expect( AccountsMeldHelper._meldUserCallback( src_user, dst_user ) ).toEqual( merged_user );
    } );

    it( 'should OR the enabled setting 3', function () {
        const src_user    = {
            enabled: false
        };
        const dst_user    = {
            enabled: false
        };
        const merged_user = {
            enabled: false
        };
        expect( AccountsMeldHelper._meldUserCallback( src_user, dst_user ) ).toEqual( merged_user );
    } );

    it( 'should OR the enabled setting 4', function () {
        const src_user    = {};
        const dst_user    = {
            enabled: true
        };
        const merged_user = {
            enabled: true
        };
        expect( AccountsMeldHelper._meldUserCallback( src_user, dst_user ) ).toEqual( merged_user );
    } );

    it( 'should OR the completed setting 1', function () {
        const src_user    = {
            completed: false
        };
        const dst_user    = {
            completed: true
        };
        const merged_user = {
            completed: true
        };
        expect( AccountsMeldHelper._meldUserCallback( src_user, dst_user ) ).toEqual( merged_user );
    } );

    it( 'should OR the completed setting 2', function () {
        const src_user    = {
            completed: false
        };
        const dst_user    = {
            completed: true
        };
        const merged_user = {
            completed: true
        };
        expect( AccountsMeldHelper._meldUserCallback( src_user, dst_user ) ).toEqual( merged_user );
    } );

    it( 'should OR the completed setting 3', function () {
        const src_user    = {
            completed: false
        };
        const dst_user    = {
            completed: false
        };
        const merged_user = {
            completed: false
        };
        expect( AccountsMeldHelper._meldUserCallback( src_user, dst_user ) ).toEqual( merged_user );
    } );

    it( 'should OR the completed setting 4', function () {
        const src_user    = {};
        const dst_user    = {
            completed: true
        };
        const merged_user = {
            completed: true
        };
        expect( AccountsMeldHelper._meldUserCallback( src_user, dst_user ) ).toEqual( merged_user );
    } );

    it( 'should combine roles 1', function () {
        const src_user    = {
            roles: {
                group1: ['role1', 'role2'],
                group2: ['role1']
            }
        };
        const dst_user    = {
            roles: {
                group2: ['role2'],
                group3: ['role1']
            }
        };
        const merged_user = {
            roles: {
                group1: ['role1', 'role2'],
                group2: ['role1', 'role2'],
                group3: ['role1']
            }
        };
        expect( AccountsMeldHelper._meldUserCallback( src_user, dst_user ) ).toEqual( merged_user );
    } );

} );
"use strict";

const adminUserlist = Meteor.server.publish_handlers['admin/user/list'];

import slugify from "underscore.string/slugify";

 import { Counts } from "meteor/tmeasday:publish-counts";
const { UserHelpers, UserCollection, AvatarCollection } = PR.Accounts;

describe( 'admin/user/list publication', function () {

    let context;
    let query;
    let options;

    beforeEach( function () {
        context = {
            ready:  jasmine.createSpy( 'ready' ),
            onStop: jasmine.createSpy( 'onStop' ),
            added:  jasmine.createSpy( 'added' )
        };

        query   = {};
        options = {};

        spyOn( Counts, 'publish' );

        // First one for the Counts, second null so that publishComposite doesn't throw
        spyOn( UserCollection, 'find' ).and.returnValues( 'cursor', null );
    } );

    describe( 'check passed', function () {

        beforeEach( function () {
            spyOn( UserHelpers, 'isSuper' );
        } );

        afterEach( function () {
            expect( context.ready ).toHaveBeenCalled();
        } );

        describe( 'logged in', function () {

            beforeEach( function () {
                context.userId = '420';
            } );

            afterEach( function () {
                expect( UserHelpers.isSuper ).toHaveBeenCalledWith( context.userId );
            } );

            describe( 'as admin', function () {

                beforeEach( function () {
                    UserHelpers.isSuper.and.returnValue( true );
                } );

                it( 'should publish', function () {
                    adminUserlist.call( context, query, options );

                    expect( UserCollection.find.calls.count() ).toEqual( 2 );
                    expect( UserCollection.find.calls.argsFor( 0 ) ).toEqual( [query, { fields: { _id: 1 } }] );
                    expect( UserCollection.find.calls.argsFor( 1 ) ).toEqual( [query, _.extend( options, { fields: jasmine.any( Object ) } )] );
                    expect( Counts.publish ).toHaveBeenCalledWith( context, 'user/count/' + slugify( JSON.stringify(query) ), 'cursor', { noReady: true } );

                    expect( UserCollection.find ).toHaveBeenCalled();
                } );
            } );

            describe( 'not as admin', function () {

                beforeEach( function () {
                    UserHelpers.isSuper.and.returnValue( false );
                } );

                afterEach( function () {
                    expect( UserHelpers.isSuper ).toHaveBeenCalledWith( context.userId );
                    expect( UserCollection.find ).not.toHaveBeenCalled();
                    expect( Counts.publish ).not.toHaveBeenCalled();
                } );

                it( 'should not publish', function () {
                    adminUserlist.call( context );
                } );

            } );

        } );

        describe( 'not logged in', function () {

            afterEach( function () {
                expect( UserHelpers.isSuper ).toHaveBeenCalledWith( undefined );
                expect( UserCollection.find ).not.toHaveBeenCalled();
                expect( Counts.publish ).not.toHaveBeenCalled();
            } );

            it( 'should not publish', function () {
                adminUserlist.call( context );
            } );

        } );

    } );

    describe( 'check failed', function () {

        afterEach( function () {
            expect( context.ready ).not.toHaveBeenCalled();
            expect( UserCollection.find ).not.toHaveBeenCalled();
            expect( Counts.publish ).not.toHaveBeenCalled();
        } );

        it( 'should throw on bad query', function () {
            expect( ()=>adminUserlist.call( context, {bad: 'query'} ) ).toThrowError( /Match\ error/ );
        } );

        it( 'should throw on bad options', function () {
            expect( ()=>adminUserlist.call( context, null, {bad: 'options'} ) ).toThrowError( /Match\ error/ );
        } );

    } );

} );
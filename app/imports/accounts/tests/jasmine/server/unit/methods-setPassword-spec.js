describe( 'pr-accounts', function () {

    describe( 'methods', function () {

        it( 'shared service should exist', function () {
            expect( PR.Accounts.UserServices ).toBeDefined();
        } );

        it( 'server service should exist', function () {
            expect( PR.Accounts.UserServicesServer ).toBeDefined();
        } );

        it( 'methods should be exposed', function () {
            expect( PR.Accounts.methods ).toBeDefined();
        } );

        describe( 'setPassword', function () {

            it( 'should exist in the service', function () {
                expect( PR.Accounts.UserServicesServer.setPassword ).toBeDefined();
            } );

            it( 'the method wrapper should be exposed in the namespace', function () {
                expect( PR.Accounts.methods.setPassword ).toBeDefined();
            } );

            it('should set password', function() {
                const loggedUser = '420';
                const password = 'secret';
                const user = {};
                spyOn(PR.Accounts.UserCollection, 'findOne').and.returnValue(user);
                spyOn(Accounts, 'setPassword');
                expect(()=>PR.Accounts.UserServicesServer.setPassword.call({userId: loggedUser}, password)).not.toThrow();
                expect(PR.Accounts.UserCollection.findOne).toHaveBeenCalledWith({_id: loggedUser},{fields:{services:1}});
                expect(Accounts.setPassword).toHaveBeenCalledWith(loggedUser, password, {logout: false});
            });

            it('should not set password if missing parameter', function() {
                const loggedUser = '420';
                spyOn(PR.Accounts.UserCollection, 'findOne');
                spyOn(Accounts, 'setPassword');
                expect(()=>PR.Accounts.UserServicesServer.setPassword.call({userId: loggedUser}, null)).toThrowError(/Match\ error/);
                expect(PR.Accounts.UserCollection.findOne).not.toHaveBeenCalled();
                expect(Accounts.setPassword).not.toHaveBeenCalled();
            });

            it('should not set password if not logged in (or not bound to a Meteor method)', function() {
                const password = 'secret';
                spyOn(PR.Accounts.UserCollection, 'findOne');
                spyOn(Accounts, 'setPassword');
                expect(()=>PR.Accounts.UserServicesServer.setPassword(password)).toThrowError(/Must\ be\ logged\ in/);
                expect(PR.Accounts.UserCollection.findOne).not.toHaveBeenCalled();
                expect(Accounts.setPassword).not.toHaveBeenCalled();
            });

            it('should not set password if user is not found', function() {
                const loggedUser = '420';
                const password = 'secret';
                spyOn(PR.Accounts.UserCollection, 'findOne').and.returnValue(null);
                spyOn(Accounts, 'setPassword');
                expect(()=>PR.Accounts.UserServicesServer.setPassword.call({userId: loggedUser}, password)).toThrowError(/User\ not\ found/);
                expect(PR.Accounts.UserCollection.findOne).toHaveBeenCalledWith({_id: loggedUser},{fields:{services:1}});
                expect(Accounts.setPassword).not.toHaveBeenCalled();
            });

            it('should not set password if user already has one (bcrypt)', function() {
                const loggedUser = '420';
                const password = 'secret';
                const user = { services: { password: { bcrypt: 'secret' }}};
                spyOn(PR.Accounts.UserCollection, 'findOne').and.returnValue(user);
                spyOn(Accounts, 'setPassword');
                expect(()=>PR.Accounts.UserServicesServer.setPassword.call({userId: loggedUser}, password)).toThrowError(/User\ already\ has\ a\ password\ set/);
                expect(PR.Accounts.UserCollection.findOne).toHaveBeenCalledWith({_id: loggedUser},{fields:{services:1}});
                expect(Accounts.setPassword).not.toHaveBeenCalled();
            });

            it('should not set password if user already has one (srp)', function() {
                const loggedUser = '420';
                const password = 'secret';
                const user = { services: { password: { srp: 'secret' }}};
                spyOn(PR.Accounts.UserCollection, 'findOne').and.returnValue(user);
                spyOn(Accounts, 'setPassword');
                expect(()=>PR.Accounts.UserServicesServer.setPassword.call({userId: loggedUser}, password)).toThrowError(/User\ already\ has\ a\ password\ set/);
                expect(PR.Accounts.UserCollection.findOne).toHaveBeenCalledWith({_id: loggedUser},{fields:{services:1}});
                expect(Accounts.setPassword).not.toHaveBeenCalled();
            });
        } );
    } );
} );

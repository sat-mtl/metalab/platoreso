const adminAvatarEdit = Meteor.server.publish_handlers['admin/avatar/edit'];
const { UserHelpers, AvatarCollection } = PR.Accounts;

describe( 'admin/avatar/edit publication', function () {

    let context;

    beforeEach( function () {
        context = {
            ready:  jasmine.createSpy( 'ready' ),
            //onStop: jasmine.createSpy( 'onStop' ),
            //added:  jasmine.createSpy( 'added' )
        };
        spyOn( AvatarCollection, 'find' );
    } );

    describe( 'check passed', function () {

        beforeEach( function () {
            spyOn( UserHelpers, 'isAdmin' );
        } );

        afterEach( function () {
            //expect( context.ready ).toHaveBeenCalled();
        } );

        describe( 'logged in', function () {

            beforeEach( function () {
                context.userId = '420';
            } );

            afterEach( function () {
                expect( UserHelpers.isAdmin ).toHaveBeenCalledWith( context.userId );
            } );

            describe( 'as admin', function () {

                beforeEach( function () {
                    UserHelpers.isAdmin.and.returnValue( true );
                } );

                afterEach( function() {
                    expect( context.ready ).not.toHaveBeenCalled();
                });

                it( 'should publish', function () {
                    adminAvatarEdit.call( context, '666' );
                    expect( AvatarCollection.find ).toHaveBeenCalledWith({owners: '666'});
                } );
            } );

            describe( 'not as admin', function () {

                beforeEach( function () {
                    UserHelpers.isAdmin.and.returnValue( false );
                } );

                afterEach( function () {
                    expect( UserHelpers.isAdmin ).toHaveBeenCalledWith( context.userId );
                    expect( context.ready ).toHaveBeenCalled();
                    expect( AvatarCollection.find ).not.toHaveBeenCalled();
                } );

                it( 'should not publish', function () {
                    adminAvatarEdit.call( context, '666' );
                } );

            } );

        } );

        describe( 'not logged in', function () {

            afterEach( function () {
                expect( UserHelpers.isAdmin ).toHaveBeenCalledWith( undefined );
                expect( AvatarCollection.find ).not.toHaveBeenCalled();
            } );

            it( 'should not publish', function () {
                adminAvatarEdit.call( context, '666' );
            } );

        } );

    } );

    describe( 'check failed', function () {

        afterEach( function () {
            expect( context.ready ).not.toHaveBeenCalled();
            expect( AvatarCollection.find ).not.toHaveBeenCalled();
        } );

        it( 'should throw on bad if (undefined)', function () {
            expect( ()=>adminAvatarEdit.call( context ) ).toThrowError( /Match\ error/ );
        } );

        it( 'should throw on bad if (number)', function () {
            expect( ()=>adminAvatarEdit.call( context, 1234 ) ).toThrowError( /Match\ error/ );
        } );

        it( 'should throw on bad id (object)', function () {
            expect( ()=>adminAvatarEdit.call( context, {not: 'a string'} ) ).toThrowError( /Match\ error/ );
        } );

    } );

} );
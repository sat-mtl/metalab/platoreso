const {AccountsCallbacks, UserCollection, RoleList} = PR.Accounts;

describe( 'Accounts Callbacks', function () {

    it( 'should be exposed in the namespace', function () {
        expect( PR.Accounts.AccountsCallbacks ).toBeDefined();
    } );

    describe( 'on create user', function () {

        let options;
        let user;
        let returned;

        beforeEach( function () {
            options  = {};
            user     = {};
            returned = {enabled: true, completed: false, profile: {}};
            spyOn( UserCollection, 'findOne' );
        } );

        it( 'should be defined', function () {
            expect( AccountsCallbacks.onCreateUser ).toBeDefined();
        } );

        describe( 'basics (common to all services)', function () {

            it( 'should create a user', function () {
                returned.roles = {[Roles.GLOBAL_GROUP]: [RoleList.user]};
                UserCollection.findOne.and.returnValue( {} ); // Whatever non-null
                expect( AccountsCallbacks.onCreateUser( options, user ) ).toEqual( returned );
            } );

            it( 'should create an admin if it is the first user to register', function () {
                returned.roles = {[Roles.GLOBAL_GROUP]: [RoleList.admin]};
                UserCollection.findOne.and.returnValue( null );
                expect( AccountsCallbacks.onCreateUser( options, user ) ).toEqual( returned );
            } );
        } );

        describe('per service', function() {

            beforeEach(function() {
                user.services = {};
                returned.services = {};
                returned.roles = {[Roles.GLOBAL_GROUP]: [RoleList.user]};
                UserCollection.findOne.and.returnValue( {} ); // Whatever non-null
            });

            describe('password', function() {

                beforeEach(function() {
                    user.services.password = {};
                    returned.services.password = {};
                });

                it('should initialize as non completed', function() {
                    returned.completed = false;
                    expect( AccountsCallbacks.onCreateUser( options, user ) ).toEqual( returned );
                });
            });

            describe('verified services', function() {

                beforeEach(function() {
                    returned.completed = true;
                });

                describe( 'facebook', function () {

                    beforeEach(function() {
                        user.services.facebook = {};
                    });

                    it('should build a profile', function() {
                        const facebook = {
                            id: '420',
                            email: 'test@email.com',
                            first_name: 'First',
                            last_name: 'Last',
                            locale: 'en_US'
                        };
                        user.services.facebook = _.clone(facebook);
                        returned.services.facebook = _.clone(facebook);
                        returned.emails = [
                            {
                                address: 'test@email.com',
                                verified: true
                            }
                        ];
                        returned.profile = {
                            firstName: 'First',
                            lastName: 'Last',
                            avatarUrl: 'http://graph.facebook.com/420/picture/?type=large',
                            language: 'en'
                        };
                        expect( AccountsCallbacks.onCreateUser( options, user ) ).toEqual( returned );
                    });

                    it('should build a profile (when no first/last names)', function() {
                        options.profile = {name: 'Some Name'};
                        const facebook = {
                            id: '420',
                            email: 'test@email.com',
                            locale: 'en_US'
                        };
                        user.services.facebook = _.clone(facebook);
                        returned.services.facebook = _.clone(facebook);
                        returned.emails = [
                            {
                                address: 'test@email.com',
                                verified: true
                            }
                        ];
                        returned.profile = {
                            firstName: 'Some Name',
                            lastName: '',
                            avatarUrl: 'http://graph.facebook.com/420/picture/?type=large',
                            language: 'en'
                        };
                        expect( AccountsCallbacks.onCreateUser( options, user ) ).toEqual( returned );
                    });

                } );

                describe( 'twitter', function () {

                    beforeEach(function() {
                        user.services.twitter = {};
                    });

                    it('should build a profile', function() {
                        options.profile = {name: 'First Last'};
                        const twitter = {
                            profile_image_url: 'http://domain.com/image.jpg',
                            lang: 'en',
                            screenName: 'screen_name'
                        };
                        user.services.twitter = _.clone(twitter);
                        returned.services.twitter = _.clone(twitter);
                        returned.profile = {
                            firstName: 'First Last',
                            lastName: '',
                            avatarUrl: 'http://domain.com/image.jpg',
                            language: 'en'
                        };
                        expect( AccountsCallbacks.onCreateUser( options, user ) ).toEqual( returned );
                    });

                    it('should build a profile (when no profile name)', function() {
                        const twitter = {
                            profile_image_url: 'http://domain.com/image.jpg',
                            lang: 'en',
                            screenName: 'screen_name'
                        };
                        user.services.twitter = _.clone(twitter);
                        returned.services.twitter = _.clone(twitter);
                        returned.profile = {
                            firstName: 'screen_name',
                            lastName: '',
                            avatarUrl: 'http://domain.com/image.jpg',
                            language: 'en'
                        };
                        expect( AccountsCallbacks.onCreateUser( options, user ) ).toEqual( returned );
                    });

                } );

                describe( 'google', function () {

                    beforeEach(function() {
                        user.services.google = {};
                    });

                    it('should build a profile', function() {
                        const google = {
                            email: 'test@email.com',
                            verified_email: true,
                            given_name: 'First',
                            family_name: 'Last',
                            picture: 'http://domain.com/image.jpg',
                            locale: 'en-US'
                        };
                        user.services.google = _.clone(google);
                        returned.services.google = _.clone(google);
                        returned.emails = [
                            {
                                address: 'test@email.com',
                                verified: true
                            }
                        ];
                        returned.profile = {
                            firstName: 'First',
                            lastName: 'Last',
                            avatarUrl: 'http://domain.com/image.jpg',
                            language: 'en'
                        };
                        expect( AccountsCallbacks.onCreateUser( options, user ) ).toEqual( returned );
                    });

                    it('should build a profile (and respect if google email is not verified)', function() {
                        const google = {
                            email: 'test@email.com',
                            verified_email: false,
                            given_name: 'First',
                            family_name: 'Last',
                            picture: 'http://domain.com/image.jpg',
                            locale: 'en-US'
                        };
                        user.services.google = _.clone(google);
                        returned.services.google = _.clone(google);
                        returned.emails = [
                            {
                                address: 'test@email.com',
                                verified: false
                            }
                        ];
                        returned.profile = {
                            firstName: 'First',
                            lastName: 'Last',
                            avatarUrl: 'http://domain.com/image.jpg',
                            language: 'en'
                        };
                        expect( AccountsCallbacks.onCreateUser( options, user ) ).toEqual( returned );
                    });

                    it('should build a profile (when no given/family name)', function() {
                        options.profile = {name: 'First Last'};
                        const google = {
                            email: 'test@email.com',
                            verified_email: true,
                            picture: 'http://domain.com/image.jpg',
                            locale: 'en-US'
                        };
                        user.services.google = _.clone(google);
                        returned.services.google = _.clone(google);
                        returned.emails = [
                            {
                                address: 'test@email.com',
                                verified: true
                            }
                        ];
                        returned.profile = {
                            firstName: 'First Last',
                            lastName: '',
                            avatarUrl: 'http://domain.com/image.jpg',
                            language: 'en'
                        };
                        expect( AccountsCallbacks.onCreateUser( options, user ) ).toEqual( returned );
                    });

                } );

                describe( 'linkedin', function () {

                    beforeEach(function() {
                        user.services.linkedin = {};
                    });

                    it('should build a profile', function() {
                        const linkedin = {
                            emailAddress: 'test@email.com',
                            firstName: 'First',
                            lastName: 'Last',
                            pictureUrl: 'http://domain.com/image.jpg',
                            headline: "What a nice biography."
                        };
                        user.services.linkedin = _.clone(linkedin);
                        returned.services.linkedin = _.clone(linkedin);
                        returned.emails = [
                            {
                                address: 'test@email.com',
                                verified: true
                            }
                        ];
                        returned.profile = {
                            firstName: 'First',
                            lastName: 'Last',
                            avatarUrl: 'http://domain.com/image.jpg',
                            biography: "What a nice biography."
                        };
                        expect( AccountsCallbacks.onCreateUser( options, user ) ).toEqual( returned );
                    });

                    it('should build a profile (when no first/last names)', function() {
                        options.profile = {name: 'First Last'};
                        const linkedin = {
                            emailAddress: 'test@email.com',
                            pictureUrl: 'http://domain.com/image.jpg',
                            headline: "What a nice biography."
                        };
                        user.services.linkedin = _.clone(linkedin);
                        returned.services.linkedin = _.clone(linkedin);
                        returned.emails = [
                            {
                                address: 'test@email.com',
                                verified: true
                            }
                        ];
                        returned.profile = {
                            firstName: 'First Last',
                            lastName: '',
                            avatarUrl: 'http://domain.com/image.jpg',
                            biography: "What a nice biography."
                        };
                        expect( AccountsCallbacks.onCreateUser( options, user ) ).toEqual( returned );
                    });

                } );

            });

        });

    } );

} );
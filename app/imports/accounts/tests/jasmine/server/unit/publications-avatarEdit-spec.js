const avatarEdit = Meteor.server.publish_handlers['avatar/edit'];
const { AvatarCollection } = PR.Accounts;

describe( 'avatar/edit publication', function () {

    let context;

    beforeEach( function () {
        context = {
            ready:  jasmine.createSpy( 'ready' )
        };
        spyOn( AvatarCollection, 'find' );
    } );

    describe( 'logged in', function () {

        beforeEach( function () {
            context.userId = '420';
        } );

        afterEach( function() {
            expect( context.ready ).not.toHaveBeenCalled();
        });

        it( 'should publish', function () {
            avatarEdit.call( context );
            expect( AvatarCollection.find ).toHaveBeenCalledWith({owners: context.userId});
        } );

    } );

    describe( 'not logged in', function () {

        afterEach( function () {
            expect( AvatarCollection.find ).not.toHaveBeenCalled();
        } );

        it( 'should not publish', function () {
            avatarEdit.call( context );
        } );

    } );

} );
const adminUserEdit = Meteor.server.publish_handlers['admin/user/edit'];
const { UserHelpers, UserCollection } = PR.Accounts;

describe( 'admin/user/edit publication', function () {

    let context;

    beforeEach( function () {
        context = {
            ready: jasmine.createSpy( 'ready' ),
            //onStop: jasmine.createSpy( 'onStop' ),
            //added:  jasmine.createSpy( 'added' )
        };
        spyOn( UserCollection, 'find' );
    } );

    describe( 'check passed', function () {

        beforeEach( function () {
            spyOn( UserHelpers, 'isAdmin' );
        } );

        afterEach( function () {
            //expect( context.ready ).toHaveBeenCalled();
        } );

        describe( 'logged in', function () {

            beforeEach( function () {
                context.userId = '420';
            } );

            afterEach( function () {
                expect( UserHelpers.isAdmin ).toHaveBeenCalledWith( context.userId );
            } );

            describe( 'as admin', function () {

                beforeEach( function () {
                    UserHelpers.isAdmin.and.returnValue( true );
                } );

                afterEach( function () {
                    expect( context.ready ).not.toHaveBeenCalled();
                } );

                it( 'should publish', function () {
                    adminUserEdit.call( context, '666' );
                    expect( UserCollection.find ).toHaveBeenCalledWith( {_id: '666'} );
                } );
            } );

            describe( 'not as admin', function () {

                beforeEach( function () {
                    UserHelpers.isAdmin.and.returnValue( false );
                } );

                afterEach( function () {
                    expect( UserHelpers.isAdmin ).toHaveBeenCalledWith( context.userId );
                    expect( context.ready ).toHaveBeenCalled();
                    expect( UserCollection.find ).not.toHaveBeenCalled();
                } );

                it( 'should not publish', function () {
                    adminUserEdit.call( context, '666' );
                } );

            } );

        } );

        describe( 'not logged in', function () {

            afterEach( function () {
                expect( UserHelpers.isAdmin ).toHaveBeenCalledWith( undefined );
                expect( UserCollection.find ).not.toHaveBeenCalled();
            } );

            it( 'should not publish', function () {
                adminUserEdit.call( context, '666' );
            } );

        } );

    } );

    describe( 'check failed', function () {

        afterEach( function () {
            expect( context.ready ).not.toHaveBeenCalled();
            expect( UserCollection.find ).not.toHaveBeenCalled();
        } );

        it( 'should throw on bad if (undefined)', function () {
            expect( ()=>adminUserEdit.call( context ) ).toThrowError( /Match\ error/ );
        } );

        it( 'should throw on bad if (number)', function () {
            expect( ()=>adminUserEdit.call( context, 1234 ) ).toThrowError( /Match\ error/ );
        } );

        it( 'should throw on bad id (object)', function () {
            expect( ()=>adminUserEdit.call( context, {not: 'a string'} ) ).toThrowError( /Match\ error/ );
        } );

    } );

} );
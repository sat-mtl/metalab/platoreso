"use strict";

// Monkey patch
import "./monkey/roles";

// Collections
import "./UserCollection";
import "./AvatarCollection";

// Methods
import "./UserMethods";
"use strict";

/**
 * Always-on subscription to the "enhanced" version of the current user
 */
Meteor.subscribe( 'user/current' );
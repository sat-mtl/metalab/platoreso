"use strict";

import flatten from "flat";
import GroupHelpers from "/imports/groups/GroupHelpers";
import UserHelpers from "./UserHelpers";
import UserInvitationSchema from "./model/schema/UserInvitationSchema";
import UserUpdateSchema from "./model/schema/UserUpdateSchema";
import UserCollection from "./UserCollection";
import {RoleList} from "./Roles";

/**
 * Service class used by Meteor methods
 * in order to be able to unit test the methods in isolation.
 *
 * Even though the methods are static Meteor will bind "this" when calling the method.
 * Unit tests should be able to deal with that on a case-by-case basis.
 */
export default class UserServices {

    /**
     * Invite a user
     *
     * @param {object} info User invitation info
     */
    static invite( info ) {
        check( info, Match.ObjectIncluding( {
            email:   String,
            profile: Match.ObjectIncluding( {
                firstName: String,
                lastName:  String
            } )
        } ) );

        // Only allow experts to invite users
        if ( !UserHelpers.isExpert( this.userId ) ) {
            throw new Meteor.Error( 'unauthorized' );
        }

        if ( Meteor.isServer ) {
            // Keep roles and groups on the side for later
            let { roles, groups } = info;

            // Clean the passed info according to the user creation validation schema
            UserInvitationSchema.clean( info, { removeEmptyStrings: false } );

            // Create the user
            const userId = Accounts.createUser( info );

            // Set roles
            if ( _.isEmpty( roles ) || !_.isArray( roles ) ) {
                roles = [];
            }

            // Make sure you can't create a user with higher privileges than yourself
            roles = UserHelpers.filterRoles( roles, this.userId );

            // Make sure they have the user role, no matter what
            roles = _.union( roles, [RoleList.user] );

            // Finally add the user to the list of roles
            if ( !_.isEmpty( roles ) ) {
                Roles.addUsersToRoles(
                    userId,
                    roles,
                    Roles.GLOBAL_GROUP
                );
            }

            // Set groups
            if ( !_.isEmpty( groups ) && _.isObject( groups ) ) {
                _.forEach( groups, ( roles, group ) => {
                    if ( !_.isEmpty( roles ) && _.isArray( roles ) ) {
                        Roles.setUserRoles(
                            userId,
                            roles || [],
                            group
                        );
                    }
                } );
            }

            // User is ready to be invited
            Accounts.sendEnrollmentEmail( userId );
            return userId;
        }
    }

    /**
     * Update a user's email
     * This is used from the update() method in order to make it more concise and have better unit test support.
     *
     * TODO: Handle passing from one email to another without blocking the user out
     *
     * @param {object} info User email update information, should contain an _id and email field
     */
    static _updateEmail( info ) {
        check( info, Match.ObjectIncluding( { _id: String, email: String } ) );

        // Check if we have a logged in user first, this saves us a call to get the roles
        if ( !this.userId ) {
            throw new Meteor.Error( 'unauthorized' );
        }

        // Check is user is editing itself or is an admin, this is done separately here because
        // we'll need the admin status later, so we save it to a const.
        const isSuper = UserHelpers.isSuper( this.userId );
        if ( this.userId !== info._id && !isSuper ) {
            throw new Meteor.Error( 'unauthorized' );
        }

        let user = UserCollection.findOne( { _id: info._id }, { fields: { emails: 1 } } );
        if ( !user ) {
            throw new Meteor.Error( 'user not found' );
        }

        if ( !user.emails || !user.emails[0] || !user.emails[0].address || info.email != user.emails[0].address ) {

            if ( !user.emails ) {
                // Create the emails array if the user doesn't already have one
                UserCollection.update( { _id: info._id }, {
                    $set: {
                        emails: [{
                            address:  info.email,
                            verified: isSuper // Only admins can change emails without verification
                        }]
                    }
                } );
            } else {
                // Update the current array if the user already has an email
                UserCollection.update( { _id: info._id }, {
                    $set: {
                        'emails.0.address':  info.email,
                        'emails.0.verified': isSuper // Only admins can change emails without verification
                    }
                } );
            }

            if ( Meteor.isServer && !isSuper ) {
                Accounts.sendVerificationEmail( info._id );
            }
        }
    }

    /**
     * Update user profile
     * This is used from the update() method in order to make it more concise and have better unit test support.
     *
     * @param {object} info User profile update information, should contain an _id and profile field
     */
    static _updateProfile( info ) {
        check( info, Match.ObjectIncluding( { _id: String, profile: Object } ) );

        // Only allow logged in users to edit themselves or others if they are an admin
        if ( !this.userId || ( this.userId !== info._id && !UserHelpers.isSuper( this.userId ) ) ) {
            throw new Meteor.Error( 'unauthorized' );
        }

        if ( _.isEmpty( info.profile ) ) {
            // Nothing to update
            return;
        }

        // Set sort values for profile, autoValue doesn't seem to work
        // FIXME: This is only a temporary solution because subdocuments autoValues don't work
        if ( info.profile.firstName ) {
            info.profile.firstName_sort = info.profile.firstName.toLowerCase();
        }
        if ( info.profile.lastName ) {
            info.profile.lastName_sort = info.profile.lastName.toLowerCase();
        }

        // Update the user, keeping only the flattened profile for the modifier
        const modifier = { $set: flatten( { profile: info.profile } ) };
        UserCollection.update( { _id: info._id }, modifier );
    }

    /**
     * Update the user's profile completion status
     * This is used from the update() method in order to make it more concise and have better unit test support.
     *
     * @param {String} updateUserId User Id to check for completion
     */
    static _updateProfileCompletion( updateUserId ) {
        check( updateUserId, String );

        // Only allow logged in users to edit themselves or others if they are an admin
        if ( !this.userId || ( this.userId !== updateUserId && !UserHelpers.isSuper( this.userId ) ) ) {
            throw new Meteor.Error( 'unauthorized' );
        }

        const user = UserCollection.findOne( { _id: updateUserId }, {
            fields: {
                emails:    1,
                profile:   1,
                completed: 1
            }
        } );
        if ( !user ) {
            throw new Meteor.Error( 'user not found' );
        }

        // We update no matter what because the user can go from completed to not completed and vice versa
        UserCollection.update( { _id: updateUserId }, {
            $set: {
                completed: UserHelpers.isProfileCompleted( user )
            }
        } );
    }

    /**
     * Update a user's profile and/or email
     *
     * @param  {object} userInfo User update information
     */
    static update( userInfo ) {
        // Clean the passed info according to the user update validation schema
        UserUpdateSchema.clean( userInfo, { removeEmptyStrings: false } );
        check( userInfo, UserUpdateSchema );

        // Only allow logged in users to edit themselves or others if they are an admin
        if ( !this.userId || ( this.userId !== userInfo._id && !UserHelpers.isSuper( this.userId ) ) ) {
            throw new Meteor.Error( 'unauthorized' );
        }

        // Get the user with the completed field, we'll need it later to complete the profile
        let user = UserCollection.findOne( { _id: userInfo._id }, { fields: { _id: 1, completed: 1 } } );
        if ( !user ) {
            throw new Meteor.Error( 'user not found' );
        }

        // Allow only super to directly modify the user
        // Not a very good method but this whole UserMethods thing needs a refactoring
        if ( UserHelpers.isSuper( this.userId ) ) {
            let userData = _.clone( userInfo );
            delete userData._id;
            delete userData.profile;
            delete userData.email;
            if ( !_.isEmpty( userData ) ) {
                UserCollection.update( { _id: user._id }, { $set: userData } );
            }
        }

        // Handle the profile, this is really the only thing besides the email
        // that the user can change with this method
        if ( userInfo.profile ) {
            UserServices._updateProfile.call( this, userInfo );
        }

        // Handle the email separately, it is easier to reason about this method like this
        // and also helps greatly with unit testing, it's going to do more database queries
        // but it's not like updating a user's email is a high frequency event
        if ( userInfo.email ) {
            UserServices._updateEmail.call( this, userInfo );
        }

        // Check for profile completion
        if ( userInfo.profile || userInfo.email ) {
            UserServices._updateProfileCompletion.call( this, userInfo._id );
        }
    }

    /**
     * Update the user's global roles
     * This is used from the updatePermissions() method in order to make it more concise and have better unit test support.
     *
     * @param {Object} info information object containing the _id and roles key
     */
    static _updateRoles( info ) {
        check( info, Match.ObjectIncluding( { _id: String, roles: [String] } ) );

        if ( !this.userId ) {
            throw new Meteor.Error( 'unauthorized' );
        }

        // Only allow supers to edit permissions
        if ( !UserHelpers.isSuper( this.userId ) ) {
            throw new Meteor.Error( 'unauthorized' );
        }

        const user = UserCollection.findOne( { _id: info._id }, { fields: { _id: 1 } } );
        if ( !user ) {
            throw new Meteor.Error( 'user not found' );
        }

        // First get the current roles
        const currentRoles   = Roles.getRolesForUser( user._id, Roles.GLOBAL_GROUP );
        const requestedRoles = info.roles || [];

        // Keep only the roles to be added/removed (as to not disturb existing/blocked roles)
        // If we were to setUserRoles, we wouldn't be able to filter out roles that can't be changes by the logged in user
        let removals  = UserHelpers.filterRoles( currentRoles.filter( role => requestedRoles.indexOf( role ) == -1 ), this.userId );
        let additions = UserHelpers.filterRoles( requestedRoles.filter( role => currentRoles.indexOf( role ) == -1 ), this.userId );

        // If we end up removing all roles and not setting new ones while trying to set some roles,
        // Then something is wrong, we should just abort and not change anything
        if ( removals.length == currentRoles.length && additions.length == 0 && requestedRoles.length > 0 ) {
            return;
        }

        // An admin user cannot demote itself from being an admin
        // So prevent an admin from removing it's admin role as not to lock the system
        if ( user._id == this.userId && UserHelpers.isAdmin( this.userId ) && removals.indexOf( RoleList.admin ) != -1 ) {
            removals = _.without( removals, RoleList.admin );
        }

        // Remove user from roles that are not part of what we received from the client
        Roles.removeUsersFromRoles( user._id, removals, Roles.GLOBAL_GROUP );

        // Add user to new roles that he/she wasn't already a member of
        Roles.addUsersToRoles( user._id, additions, Roles.GLOBAL_GROUP );
    }

    /**
     * Removes group permissions for a user
     * This is used from the updatePermissions() method in order to make it more concise and have better unit test support.
     *
     * @param {Object} info information object containing the _id and removedPermissions key
     */
    static _removeGroupPermissions( info ) {
        check( info, Match.ObjectIncluding( { _id: String, removedPermissions: [String] } ) );

        if ( !this.userId ) {
            throw new Meteor.Error( 'unauthorized' );
        }

        // Only allow admins to edit permissions
        const isSuper = UserHelpers.isSuper( this.userId );
        if ( !isSuper ) {
            throw new Meteor.Error( 'unauthorized' );
        }

        const user = UserCollection.findOne( { _id: info._id }, { fields: { _id: 1 } } );
        if ( !user ) {
            throw new Meteor.Error( 'user not found' );
        }

        // Roles is broken when it comes to removing all references to a group
        // So we have to do it manually
        let unset = {};
        info.removedPermissions.forEach( group => {
            unset['roles.' + group] = null;
        } );
        UserCollection.update( { _id: user._id }, { $unset: unset } );
    }

    /**
     * Sets group permissions for a user
     * This is used from the updatePermissions() method in order to make it more concise and have better unit test support.
     *
     * @param {Object} info information object containing the _id and groups key
     */
    static _setGroupPermissions( info ) {
        check( info, Match.ObjectIncluding( { _id: String, groups: Object } ) );

        if ( !this.userId ) {
            throw new Meteor.Error( 'unauthorized' );
        }

        // Only allow admins to edit permissions
        const isSuper = UserHelpers.isSuper( this.userId );
        if ( !isSuper ) {
            throw new Meteor.Error( 'unauthorized' );
        }

        const user = UserCollection.findOne( { _id: info._id }, { fields: { _id: 1 } } );
        if ( !user ) {
            throw new Meteor.Error( 'user not found' );
        }

        _.forEach( info.groups, ( roles, group ) => {
            Roles.setUserRoles( user._id, roles || [], group );
        } );
    }

    /**
     * Update user's permissions
     *
     * @param {object} info Object containing the permissions info from the form
     */
    static updatePermissions( info ) {
        check( info, Match.ObjectIncluding( { _id: String } ) );

        if ( !this.userId ) {
            throw new Meteor.Error( 'unauthorized' );
        }

        // Only allow admins to edit permissions
        if ( !UserHelpers.isSuper( this.userId ) ) {
            throw new Meteor.Error( 'unauthorized' );
        }

        const user = UserCollection.findOne( { _id: info._id }, { fields: { _id: 1 } } );
        if ( !user ) {
            throw new Meteor.Error( 'user not found' );
        }

        // MAIN ROLES
        // If missing, do nothing, removing roles requires an explicitly empty key
        // Otherwise not changing roles would remove them all
        if ( info.roles ) {
            UserServices._updateRoles.call( this, info );
        }

        // group permissions removals
        if ( info.removedPermissions ) {
            UserServices._removeGroupPermissions.call( this, info );
        }

        // Group permissions
        if ( info.groups ) {
            UserServices._setGroupPermissions.call( this, info );
        }

        // Update Conversation
        GroupHelpers.updateConversationParticipation( user._id );
    }

    /**
     * Remove user
     *
     * @param  {String} userId User Id
     */
    static remove( userId ) {
        check( userId, String );

        // Only allow logged in users to remove themselves or others if they are an admin
        if ( !this.userId || ( this.userId !== userId && !UserHelpers.isSuper( this.userId ) ) ) {
            throw new Meteor.Error( 'unauthorized' );
        }

        // Can't remove yourself, at least with this method
        // TODO: Implement self removal
        if ( this.userId == userId ) {
            throw new Meteor.Error( 'cannot remove self' );
        }

        const user = UserCollection.findOne( { _id: userId }, { fields: { _id: 1 } } );
        if ( !user ) {
            throw new Meteor.Error( 'user not found' );
        }

        const removerIsAdmin = UserHelpers.isAdmin( this.userId );
        const removedIsAdmin = UserHelpers.isAdmin( userId );
        if ( removedIsAdmin && !removerIsAdmin ) {
            throw new Meteor.Error( 'unauthorized' );
        }

        // Remove the user from the database
        UserCollection.remove( { _id: userId } );
    }

    static acceptTerms( contact ) {
        check( contact, Boolean );

        if ( !this.userId ) {
            throw new Meteor.Error( 401, "Must be logged in" );
        }

        UserCollection.update( { _id: this.userId }, {
            $set: {
                acceptedTerms:               true,
                acceptedTermsAt:             new Date(),
                'profile.allowEmailContact': contact
            }
        } );
    }

    static unsubscribe( token ) {
        check( token, String );

        UserCollection.update( { unsubscribeToken: token }, {
            $set: {
                unsubscribeToken: Random.id(), // Refresh token for next use
                'profile.allowEmailContact': false
            }
        } );
    }
}
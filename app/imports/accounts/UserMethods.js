"use strict";

import wrapServiceMethod from "/imports/utils/wrapServiceMethod";
import wrapMethod from "/imports/utils/wrapMethod";
import UserServices from "./UserServices";

const UserMethods = {
    invite:              wrapServiceMethod( 'pr/accounts/invite', UserServices.invite ),
    update:              wrapServiceMethod( 'pr/accounts/update', UserServices.update ),
    remove:              wrapServiceMethod( 'pr/accounts/remove', UserServices.remove ),
    updatePermissions:   wrapServiceMethod( 'pr/accounts/updatePermissions', UserServices.updatePermissions ),
    acceptTerms:         wrapServiceMethod( 'pr/accounts/acceptTerms', UserServices.acceptTerms ),
    unsubscribe:         wrapServiceMethod( 'pr/accounts/unsubscribe', UserServices.unsubscribe ),
    sendValidationEmail: wrapMethod( 'pr/accounts/sendValidationEmail' ),
    setPassword:         wrapMethod( 'pr/accounts/setPassword' )
};

export default UserMethods;
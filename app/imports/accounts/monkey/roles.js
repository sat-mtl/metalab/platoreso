"use strict";

import { Roles } from "meteor/alanning:roles";

/**
 * Roles monkey-patch to remove empty groups from user's roles
 *
 * @type {Function|*}
 */

var removeUsersFromRoles_original = Roles.removeUsersFromRoles;

Roles.removeUsersFromRoles = function (users, roles, group) {
    removeUsersFromRoles_original.call(this, users, roles, group);

    if ( users == null ) {
        return;
    }

    if ( "array" !== typeof users ) {
        users = [users]
    }

    users.forEach( user => {
        let userId = null;
        let unset = {$unset: {}};

        if ("string" === typeof user) {
            userId = user;
        } else if ("object" === typeof user) {
            userId = user._id;
        }

        if ( userId == null ) {
            return;
        }

        if (Roles.getRolesForUser(userId, group).length === 0) {
            unset.$unset['roles.' + group] = "";
            Meteor.users.update({_id: userId}, unset);
        }
    });
};

/**
 * Roles monkey-patch to allow an array of roles in the getGroupsForUser method
 *
 * @deprecated
 * @see https://github.com/alanning/meteor-roles/pull/148
 *
 * @type {Function|*}
 */

Roles.getGroupsForUser = function (user, roles) {
    var userGroups = [];

    if (!user) return [];
    if (roles) {
        if ('string' === typeof roles) roles = [ roles ];
        if ('$' === roles[0]) return []; // What is this?

        // convert any periods to underscores
        roles = roles.map( role => role.replace('.', '_') );
    }

    if ('string' === typeof user) {
        user = Meteor.users.findOne(
            {_id: user},
            {fields: {roles: 1}});

    } else if ('object' !== typeof user) {
        // invalid user object
        return [];
    }

    //User has no roles or is not using groups
    if (!user || !user.roles || _.isArray(user.roles)) return [];

    if (roles) {
        _.each(user.roles, function(groupRoles, groupName) {
            if (_.intersection(groupRoles, roles).length > 0 && groupName !== Roles.GLOBAL_GROUP) {
                userGroups.push(groupName);
            }
        });
        return userGroups;
    } else {
        return _.without(_.keys(user.roles), Roles.GLOBAL_GROUP);
    }
};
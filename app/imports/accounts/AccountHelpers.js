"use strict";

/**
 * Registration Schema
 * @type {SimpleSchema}
 */
const RegistrationSchema = new SimpleSchema( {
    "email":    {
        type:  String,
        regEx: SimpleSchema.RegEx.Email
    },
    "password": {
        type: String
    }
} );


/**
 * Login Validation Schema
 * @type {SimpleSchema}
 */
const PasswordLoginSchema = new SimpleSchema( {
    "email":    {
        type:  String,
        regEx: SimpleSchema.RegEx.Email
    },
    "password": {
        type: String
    }
} );

export default class AccountHelpers {

    /**
     * Register helper
     *
     * @param {object} info User creation info
     * @param cb
     */
    static register( info, cb ) {
        RegistrationSchema.clean( info, { removeEmptyStrings: false } );
        try {
            check( info, RegistrationSchema );
        } catch ( e ) {
            return cb( e );
        }

        Accounts.createUser( info, cb );
    }


    /**
     * Login helper
     *
     * @param credentials
     * @param cb
     */
    static login( credentials, cb ) {
        if ( !credentials || !credentials.service ) {
            cb && cb( new Meteor.Error( 'missing credentials' ) );
            return;
        }

        switch ( credentials.service ) {
            case 'password':
                PasswordLoginSchema.clean( credentials );
                try {
                    check( credentials, PasswordLoginSchema );
                } catch ( e ) {
                    return cb( e );
                }
                Meteor.loginWithPassword( credentials.email, credentials.password, cb );
                break;

            case 'facebook':
                Meteor.loginWithFacebook( { requestPermissions: ['email'] }, cb );
                break;

            case 'twitter':
                Meteor.loginWithTwitter( {}, cb );
                break;

            case 'google':
                Meteor.loginWithGoogle( { requestPermissions: ['openid', 'email'] }, cb );
                break;

            case 'linkedin':
                Meteor.loginWithLinkedIn( { requestPermissions: ['r_emailaddress'] }, cb );
                break;

            default:
                cb && cb( new Meteor.Error( 'unknown service' ) );
                break;
        }
    }

    /**
     * Logout Helper
     * Simple wrapper in case we need to change the behavior
     */
    static logout( cb ) {
        Meteor.logout( cb );
    }
}

// Export from here so that we don't use meteor directly
AccountHelpers.verifyEmail    = Accounts.verifyEmail;
AccountHelpers.forgotPassword = Accounts.forgotPassword;
AccountHelpers.resetPassword  = Accounts.resetPassword;
AccountHelpers.changePassword = Accounts.changePassword;

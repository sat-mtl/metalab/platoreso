"use strict";

import capitalize from "underscore.string/capitalize";
import UserCollection from "./UserCollection";
import {
    RoleList,
    RoleLevels,
    RoleGroups
          } from "./Roles";

export default class UserHelpers {

    /**
     * Checks if the user has at least one verified email
     *
     * @param user {Meteor.User}
     * @returns {boolean}
     */
    static hasEmail( user ) {
        return !!user && !!user.emails && user.emails.some( ( email ) => email.address != null );
    }

    /**
     * Checks if the user has at least one verified email
     *
     * @param user {Meteor.User}
     * @returns {boolean}
     */
    static hasVerifiedEmail( user ) {
        return UserHelpers.hasEmail( user ) && user.emails.some( ( email ) => email.verified );
    }

    /**
     * Check if a user's profile is completed
     * It should contain the bare minimum to be able to use the site
     *
     * @param user {Meteor.User}
     * @returns boolean
     */
    static isProfileCompleted( user ) {
        return UserHelpers.hasVerifiedEmail( user )
            && user.profile != null
            && user.profile.firstName != null
            && user.profile.lastName != null;
    }

    /**
     * Check if user is admin
     *
     * Helper method to check for permissions without depending on the Roles package
     *
     * @param {String|Meteor.User} user
     * @param {String} [group]
     * @param {Boolean} [explicit] Whether or not it has to be that precise role of part of the hierarchy
     * @returns {Boolean}
     */
    static isAdmin( user, group = null, explicit = false ) {
        if ( !user ) {
            return false;
        }

        if ( explicit ) {
            return Roles.userIsInRole( user, RoleList.admin, group );
        } else {
            return Roles.userIsInRole( user, RoleGroups.admins, group );
        }
    }

    /**
     * Check if user is super
     *
     * Helper method to check for permissions without depending on the Roles package
     *
     * @param {String|Meteor.User} user
     * @param {String} [group]
     * @param {Boolean} [explicit] Whether or not it has to be that precise role of part of the hierarchy
     * @returns {Boolean}
     */
    static isSuper( user, group = null, explicit = false ) {
        if ( !user ) {
            return false;
        }

        if ( explicit ) {
            return Roles.userIsInRole( user, RoleList.super, group );
        } else {
            return Roles.userIsInRole( user, RoleGroups.supers, group );
        }
    }

    /**
     * Check if user is expert
     *
     * Helper method to check for permissions without depending on the Roles package
     *
     * @param {String|Meteor.User} user
     * @param {String} [group]
     * @param {Boolean} [explicit] Whether or not it has to be that precise role of part of the hierarchy
     * @returns {Boolean}
     */
    static isExpert( user, group = null, explicit = false ) {
        if ( !user ) {
            return false;
        }

        if ( explicit ) {
            return Roles.userIsInRole( user, RoleList.expert, group );
        } else {
            return Roles.userIsInRole( user, RoleGroups.experts, group );
        }
    }

    /**
     * Check if user is moderator
     *
     * Helper method to check for permissions without depending on the Roles package
     *
     * @param {String|Meteor.User} user
     * @param {String} [group]
     * @param {Boolean} [explicit] Whether or not it has to be that precise role of part of the hierarchy
     * @returns {Boolean}
     */
    static isModerator( user, group = null, explicit = false ) {
        if ( !user ) {
            return false;
        }

        if ( explicit ) {
            return Roles.userIsInRole( user, RoleList.moderator, group );
        } else {
            return Roles.userIsInRole( user, RoleGroups.moderators, group );
        }
    }

    /**
     * Check if user is member
     *
     * Helper method to check for permissions without depending on the Roles package
     *
     * @param {String|Meteor.User} user
     * @param {String} [group]
     * @param {Boolean} [explicit] Whether or not it has to be that precise role of part of the hierarchy
     * @returns {Boolean}
     */
    static isMember( user, group = null, explicit = false ) {
        if ( !user ) {
            return false;
        }

        if ( explicit ) {
            return Roles.userIsInRole( user, RoleList.member, group );
        } else {
            return Roles.userIsInRole( user, RoleGroups.members, group );
        }
    }

    /**
     * Check if user is user
     *
     * Helper method to check for permissions without depending on the Roles package
     *
     * @param {String|Meteor.User} user
     * @param {String} [group]
     * @param {Boolean} [explicit] Whether or not it has to be that precise role of part of the hierarchy
     * @returns {Boolean}
     */
    static isUser( user, group = null, explicit = false ) {
        if ( !user ) {
            return false;
        }

        if ( explicit ) {
            return Roles.userIsInRole( user, RoleList.user, group );
        } else {
            return Roles.userIsInRole( user, RoleGroups.users, group );
        }
    }

    /**
     * GLOBAL GROUP
     * Helper method to check for permissions without depending on the Roles package
     */
    static get GLOBAL_GROUP() {
        return Roles.GLOBAL_GROUP;
    }

    /**
     * Check if user is in role
     * Helper method to check for permissions without depending on the Roles package
     */
    static userIsInRole(...args) {
        return Roles.userIsInRole( ...args );
    }

    /**
     * Add user to roles
     * Helper method to check for permissions without depending on the Roles package
     */
    static addUsersToRoles(...args) {
        return Roles.addUsersToRoles(...args)
    }

    /**
     * Set user roles
     * Helper method to check for permissions without depending on the Roles package
     */
    static setUserRoles(...args) {
        return Roles.setUserRoles(...args)
    }

    /**
     * Remove user from roles
     * Helper method to check for permissions without depending on the Roles package
     */
    static removeUsersFromRoles(...args) {
        return Roles.removeUsersFromRoles(...args);
    }

    /**
     * Get roles for user
     * Helper method to check for permissions without depending on the Roles package
     */
    static getRolesForUser(...args) {
        return Roles.getRolesForUser(...args);
    }

    /**
     * Get the roles for a specific group
     * This was custom-made because alanning:roles ALWAYS checks for GLOBAL_GROUP...
     * 
     * @param userId
     * @param groupId
     * @returns {Array}
     */
    static getGroupRolesForUser( userId, groupId ) {
        const user = UserCollection.findOne({ _id: userId }, { fields: { _id: 1, roles: 1 } } );
        return user ? user.roles[groupId] : [];
    }

    /**
     * Get groups for user
     * Helper method to check for permissions without depending on the Roles package
     */
    static getGroupsForUser(...args) {
        return Roles.getGroupsForUser(...args);
    }

    /**
     * Get users in role
     * Helper method to check for permissions without depending on the Roles package
     */
    static getUsersInRole(...args) {
        return Roles.getUsersInRole(...args);
    }

    /**
     * Filter roles to a set that is not higher than the passed userId's roles.
     * This is used when changing roles in order to prevent someone from giving
     * roles higher than it's own to another user.
     *
     * It will filter any roles according to it's level,
     * it is not this method's responsibility to determine
     * who can set/change roles.
     *
     * @param {Array} roles
     * @param {String} userId
     */
    static filterRoles( roles, userId ) {
        const userRoles = Roles.getRolesForUser( userId, Roles.GLOBAL_GROUP );
        if ( !userRoles ) {
            return [];
        }

        // Tidy up before proceeding
        roles = _.uniq( roles );

        // Calculate the highest level from the user's roles
        const highestLevel = userRoles.reduce( (highest, role) => {
            if (RoleLevels[role] == undefined ) {
                return highest;
            }
            const level = RoleLevels[role];
            return level > highest ? level: highest;
        }, 0 );

        // Remove invalid and higher-leveled roles
        return roles
            .filter(role => RoleList[role] != undefined )
            .filter( role => RoleLevels[role] <= highestLevel );
    }

    /**
     * Get the user's initials.
     *
     * @param {Meteor.User} user
     * @returns {String} Whatever we can come up with to display some identification
     */
    static getInitials( user ) {
        let name = '';
        if ( !user ) {
            return name;
        } else if ( user.profile && user.profile.firstName && user.profile.lastName ) {
            name = (user.profile.firstName.substr(0, 1) + user.profile.lastName.substr(0, 1)).toUpperCase();
        } else if ( user.profile && user.profile.firstName ) {
            name = capitalize( user.profile.firstName.substr(0, 2), true );
        } else if ( user.profile && user.profile.lastName ) {
            name = capitalize( user.profile.lastName.substr(0, 2), true );
        } else if ( user.emails && user.emails[0] ) {
            name = capitalize( user.emails[0].address.substr(0, 2), true );
        } else if ( user._id ) {
            name = capitalize( user._id.substr(0, 2), true );
        }
        return name;
    }

    /**
     * Get the user's display name.
     * It is useful as we support first and last name, contrary the the default full name in Meteor.
     *
     * @param {Meteor.User} user
     * @returns {String} Whatever we can come up with to display some identification
     */
    static getDisplayName( user ) {
        let name = '';
        if ( user.profile && user.profile.firstName && user.profile.lastName ) {
            name = user.profile.firstName + ' ' + user.profile.lastName;
        } else if ( user.profile && user.profile.firstName ) {
            name = user.profile.firstName;
        } else if ( user.profile && user.profile.lastName ) {
            name = user.profile.lastName;
        } else if ( user.emails && user.emails[0] ) {
            name = user.emails[0].address;
        } else if ( user._id ) {
            name = user._id;
        }
        return name;
    }

}
"use strict";

import ImageCollection from "/imports/images/ImageCollection";

/**
 * User Images Collection
 */
const AvatarCollection = new ImageCollection('avatar', null, {
    overwritePrevious: true
});
export default AvatarCollection;
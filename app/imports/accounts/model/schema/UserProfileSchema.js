"use strict";

import { SimpleSchema } from "meteor/aldeed:simple-schema";

/**
 * User Profile Schema
 * @type {SimpleSchema}
 */
const UserProfileSchema = new SimpleSchema( {
    avatarUrl:   {
        type:     String,
        optional: true
    },
    firstName:   {
        type:     String,
        optional: true
    },
    // Auto-valued field to sort by case-insensitive name
    firstName_sort: {
        type: String,
        optional: true, // Because registering doesn't ask for it right away
        index: true,
        autoValue: function() {
            //FIXME: This doesn't work, it should be profile.firstName but it throws because it creates a profile object
            /*var firstName = this.field("profile.firstName");
             if (firstName.isSet) {
             return firstName.value.toLowerCase();
             } else {
             this.unset(); // Prevent users from supplying their own value
             }*/
        }
    },
    lastName:    {
        type:     String,
        optional: true
    },
    // Auto-valued field to sort by case-insensitive name
    lastName_sort: {
        type: String,
        optional: true, // Because registering doesn't ask for it right away
        index: true,
        autoValue: function() {
            //FIXME: This doesn't work, it should be profile.firstName but it throws because it creates a profile object
            /*var lastName = this.field("profile.lastName");
             if (lastName.isSet) {
             return lastName.value.toLowerCase();
             } else {
             this.unset(); // Prevent users from supplying their own value
             }*/
        }
    },
    biography:   {
        type:     String,
        optional: true
    },
    language: {
        type: String,
        allowedValues: ['en','fr'],
        optional: true
    },
    allowEmailContact: {
        type: Boolean,
        optional: true,
        defaultValue: false
    },
    emailNotifications: {
        type: Object,
        optional: true,
        blackbox: true
    }
} );

export default UserProfileSchema;
"use strict";

import { SimpleSchema } from "meteor/aldeed:simple-schema";
import UserProfileSchema from "./UserProfileSchema";

/**
 * User Invitation Validation Schema
 * Defines what is required when inviting a user
 *
 * @type {SimpleSchema}
 */
const UserInvitationSchema = new SimpleSchema({
    "email": {
        type: String,
        regEx: SimpleSchema.RegEx.Email,
        autoValue: function() {
            if ( this.isSet ) {
                return this.value.toLowerCase();
            }
        }
    },
    "profile": {
        type:     UserProfileSchema
    }
});

export default UserInvitationSchema;
"use strict";

import { SimpleSchema } from "meteor/aldeed:simple-schema";
import UserProfileSchema from "./UserProfileSchema";

/**
 * User Update Validation Schema
 * Defines what is allowed to be updated by a user
 *
 * @type {SimpleSchema}
 */
const UserUpdateSchema = new SimpleSchema( {
    _id: {
        type: String
    },

    email:   {
        type:      String,
        regEx:     SimpleSchema.RegEx.Email,
        optional:  true,
        autoValue: function () {
            if ( this.isSet ) {
                return this.value.toLowerCase();
            }
        }
    },
    /*"password": {
     type: String
     },*/
    profile: {
        type:     UserProfileSchema,
        optional: true
    },

    enabled: {
        type:     Boolean,
        optional: true
    }
} );

export default UserUpdateSchema;
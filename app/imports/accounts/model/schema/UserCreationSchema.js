"use strict";

import { SimpleSchema } from "meteor/aldeed:simple-schema";
import UserProfileSchema from "./UserProfileSchema";

/**
 * User Creation Validation Schema
 * Defines what is required when creating a user
 *
 * @type {SimpleSchema}
 */
const UserCreationSchema = new SimpleSchema({
    "email": {
        type: String,
        regEx: SimpleSchema.RegEx.Email,
        autoValue: function() {
            if ( this.isSet ) {
                return this.value.toLowerCase();
            }
        }
    },
    "password": {
        type: String
    },
    "profile": {
        type:     UserProfileSchema,
        optional: true
    }
});

export default UserCreationSchema;
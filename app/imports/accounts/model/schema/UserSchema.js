"use strict";

import {SimpleSchema} from "meteor/aldeed:simple-schema";
import UserProfileSchema from "./UserProfileSchema";

/**
 * User Schema
 * @type {SimpleSchema}
 */
const UserSchema = new SimpleSchema( {
    /**
     * Internal id used to unsubscribe from email communication
     */
    unsubscribeToken: {
        type: String
    },
    emails:              {
        type:     [Object],
        // this must be optional if you also use other login services like facebook,
        // but if you use only accounts-password, then it can be required
        optional: true
    },
    // From splendido:accounts-emails-field for splendido:accounts-meld
    registered_emails:   {
        type:     [Object],
        blackbox: true,
        optional: true
    },
    "emails.$.address":  {
        type:      String,
        regEx:     SimpleSchema.RegEx.Email,
        autoValue: function () {
            if ( this.isSet ) {
                return this.value.toLowerCase();
            }
        }
    },
    "emails.$.verified": {
        type: Boolean
    },
    createdAt:           {
        type: Date
    },
    profile:             {
        type:         UserProfileSchema,
        optional:     true,
        defaultValue: {}
    },
    services:            {
        type:     Object,
        optional: true,
        blackbox: true
    },
    // Add `roles` to your schema if you use the meteor-roles package.
    // Option 1: Object type
    // If you specify that type as Object, you must also specify the
    // `Roles.GLOBAL_GROUP` group whenever you add a user to a role.
    // Example:
    // Roles.addUsersToRoles(userId, ["admin"], Roles.GLOBAL_GROUP);
    // You can't mix and match adding with and without a group since
    // you will fail validation in some cases.
    roles:               {
        type:     Object,
        optional: true,
        blackbox: true
    },

    // CUSTOM

    enabled: {
        type:         Boolean,
        defaultValue: true
    },

    completed: {
        type:         Boolean,
        defaultValue: false
    },

    acceptedTerms: {
        type:         Boolean,
        defaultValue: false
    },

    acceptedTermsAt: {
        type: Date,
        optional: true
    },

    points: {
        type:         Number,
        defaultValue: 0
    },

    pointsPerProject: {
        type:     Object,
        optional: true,
        blackbox: true
    },

    recentProjects: {
        type:         [String],
        optional:     true,
        defaultValue: []
    }
} );

export default UserSchema;
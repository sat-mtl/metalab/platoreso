"use strict";

import { JobCollection as JobCollectionCreator, Job } from "meteor/vsivsi:job-collection";

const JobCollection = JobCollectionCreator("jobs", { noCollectionSuffix: true });
export default JobCollection;
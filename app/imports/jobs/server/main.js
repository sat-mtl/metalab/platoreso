"use strict";

import Logger from "meteor/metalab:logger/Logger";
import "./jobs/CleanupJob";
import "./publications/AdminJobsPublication";

import JobCollection from  "../JobCollection";

const log = Logger( "jobs" );

Meteor.startup(() => {
    // This was very chatty >>> JobCollection.setLogStream(process.stdout);
    JobCollection.startJobServer({}, (error, result) => {
        if ( error ) {
            log.error( error, result );
        } else {
            log.info("Started Job Server");
        }
    });
});
"use strict";

import {Job} from "meteor/vsivsi:job-collection";
import JobCollection from "../../JobCollection";

/**
 * Cleanup Job
 * Add a cleanup job to the collection when starting the app
 * It will cancel any other job of the same type already added and the cancelled job will be removed
 * by that new cleanup job.
 *
 * @type {Job}
 */
const CleanupJob = new Job( JobCollection, 'cleanup', {} )
    .repeat( { schedule: JobCollection.later.parse.text( "every 15 minutes" ) } )
    .save( { cancelRepeats: true } );

/**
 * Process Queue for the cleanup job
 * It will not poll for new jobs since we only add one repeating job on every startup
 */
const cleanupQueue = JobCollection.processJobs( 'cleanup', { pollInterval: false, workTimeout: 60 * 1000 }, ( job, cb ) => {
    
    // Keep jobs updated in the last 5 minutes
    let current = new Date();
    current.setMinutes( current.getMinutes() - 5 );
    
    // Find removable job idss
    const ids = JobCollection.find(
        {
            status:  {
                $in: Job.jobStatusRemovable
            },
            updated: {
                $lt: current
            }
        },
        {
            fields: {
                _id: 1
            }
        }
    ).map( job => job._id );
    
    // Remove jobs
    JobCollection.removeJobs( ids );
    
    // Job done
    job.done( `Removed ${ids.length} old jobs` );
    
    cb()
} );

/**
 * Observe changes to the jobs collection and trigger the queue when a ready cleanup job is found 
 */
JobCollection.find( { type: 'cleanup', status: 'ready' } ).observeChanges( {
    added: () => cleanupQueue.trigger()
} );
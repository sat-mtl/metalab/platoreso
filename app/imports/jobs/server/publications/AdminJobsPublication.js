"use strict";

import slugify from "underscore.string/slugify";
import { Counts } from "meteor/tmeasday:publish-counts";
import UserHelpers from "/imports/accounts/UserHelpers";
import JobCollection from "../../JobCollection";

Meteor.publish( "admin/job/list", function (query = null, options = null) {
    // Check query for only the allowed fields
    check( query, Match.OneOf( null, {

    } ) );

    // Check options for only the allowed fields
    check( options, Match.OneOf( null, {
        sort:  Match.Optional( Object ),
        limit: Match.Optional( Number ),
        skip:  Match.Optional( Number )
    } ) );

    if ( !UserHelpers.isAdmin( this.userId ) ) {
        return this.ready();
    }

    // Setup query
    query = query || {};

    // Setup options
    options = _.extend( options || {}, {} );

    // Slugify the json query as a unique identifier for the count
    Counts.publish( this,
        'job/count/' + slugify( JSON.stringify( query ) ),
        JobCollection.find( query, { fields: { _id: 1 } } ),
        { noReady: true }
    );

    return JobCollection.find( query, options );
} );
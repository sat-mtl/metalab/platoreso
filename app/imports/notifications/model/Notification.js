"use strict";

/**
 * Feed Model
 *
 * @param doc
 * @constructor
 */
export default class Notification {
    constructor( doc ) {
        _.extend( this, doc );
    }

    /**
     * Helper Method to get the first object matching the provided type
     * @param {String} type Entity type name to look for
     * @param {String} [role] Entity role
     */
    getObjectByType( type, role = null ) {
        return _.values( this.objects )
                .find( object => object.type == type && ( !role || object.role == role ) );
    }

    /**
     * Helper Method to get the first object id matching the provided type
     * @param {String} type Entity type name to look for
     * @param {String} [role] Entity role
     */
    getObjectIdByType( type, role = null ) {
        const object = this.getObjectByType( type, role );
        return object ? object.id : null;
    }

    /**
     * Helper Method to get the objects matching the provided type
     * @param {String} type Entity type name to look for
     * @param {String} [role] Entity role
     */
    getObjectsByType( type, role = null ) {
        return _.values( this.objects )
                .filter( object => object.type == type && ( !role || object.role == role ) );
    }

    /**
     * Helper Method to get the object ids matching the provided type
     * @param {String} type Entity type name to look for
     * @param {String} [role] Entity role
     */
    getObjectIdsByType( type, role = null ) {
        return this.getObjectsByType( type, role )
                   .map( object => object.id );
    }
};
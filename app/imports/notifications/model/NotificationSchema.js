"use strict";

import { SimpleSchema } from "meteor/aldeed:simple-schema";

export const NotificationObjectSchema = new SimpleSchema( {
    id:   {
        type: String
    },
    type: {
        type: String
    },
    role: {
        type:     String,
        optional: true
    }
} );

const NotificationSchema = new SimpleSchema( {
    // User to be notified
    user:    {
        type:  String,
        index: true
    },
    // Subject of a notification, most of the time this is the user doing the action.
    // Can be null for system notifications or privacy concerns,
    // protecting reporters of inappropriate content for example.
    // TODO: Figure out if we want it null or a special "system" user
    subject: {
        type: String,
        optional: true
    },
    // Action to notify user about
    action:  {
        type: String
    },
    // Objects of the action
    objects: {
        type: [NotificationObjectSchema]
    },
    // Additional data, to be parsed depending on the objects
    data:    {
        type:     Object,
        optional: true,
        blackbox: true
    },
    // Notification read status
    read:    {
        type:         Boolean,
        optional:     true,
        defaultValue: false
    },
    // Notification clicked status
    clicked: {
        type:         Boolean,
        optional:     true,
        defaultValue: false
    }
} );

export default NotificationSchema
"use strict";

import { ValidatedMethod } from "meteor/mdg:validated-method";
import NotificationCollection from "./NotificationCollection";

const NotificationMethods = {

    markAllAsRead: new ValidatedMethod( {
        name:     'pr/notifications/markAllAsRead',
        validate: null,
        run() {
            if ( !this.userId ) {
                throw new Meteor.Error( 'unauthorized' );
            }

            // Don't bother changing the status if the notification is already read
            NotificationCollection.update( { user: this.userId, read: false }, { $set: { read: true } }, { multi: true } );
        }
    } ),

    markAsClicked: new ValidatedMethod( {
        name:     'pr/notifications/markAsClicked',
        validate: new SimpleSchema( {
            notificationId: { type: String }
        } ).validator(),
        run( { notificationId } ) {
            if ( !this.userId ) {
                throw new Meteor.Error( 'unauthorized' );
            }

            // Include the user in the query, this prevent someone from "clicking" anothe user's notification
            // Don't bother changing the status if the notification is already clicked
            NotificationCollection.update( { _id: notificationId, user: this.userId, clicked: false }, { $set: { clicked: true } } );
        }
    } )

};

export default NotificationMethods;

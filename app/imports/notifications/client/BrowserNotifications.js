"use strict";

import logger from "meteor/metalab:logger/Logger";
import i18n from "/imports/i18n/i18n";
import notify, {canNotify} from "/imports/utils/notify";
import UserCollection from "/imports/accounts/UserCollection";
import NotificationCollection from "/imports/notifications/NotificationCollection";
import NotificationHelper from "/imports/notifications/NotificationHelper";

const log = logger( "notifications-browser-notifications" );

Meteor.startup( () => {
    // Automatic subscription to notifications on the client-side
    log.info( 'Subscribing to notifications' );
    Meteor.subscribe( 'notifications', () => {
        if ( !canNotify() ) {
            return;
        }

        // Make it autorun in a tracker so that we can reset when user logs in
        Tracker.autorun( () => {
            let initializing = true;
            // Meteor.userId() is pretty much only for the Tracker
            NotificationCollection.find( { user: Meteor.userId() }, {
                sort:  { createdAt: -1 },
                limit: 1
            } ).observe( {
                addedAt( notification, atIndex ) {
                    if ( initializing || atIndex != 0 ) {
                        return;
                    }

                    NotificationHelper.getData( notification, data => {
                        const subject = UserCollection.findOne( { _id: notification.subject } );
                        const message = NotificationHelper.getMessage( notification, subject, data, false );
                        const link    = NotificationHelper.getLink( notification, subject, data );
                        notify( __( 'PlatoReso' ), link, {
                            body: message,
                            tag:  notification._id,
                            icon: "/images/platform-avatar.png",
                            lang: i18n.getLanguage() || "en"
                        } );
                    } );
                }
            } );
            initializing = false;
        } );
    } );
} );
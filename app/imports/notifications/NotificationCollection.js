"use strict";

import NotificationSchema from "./model/NotificationSchema";
import Notification from "./model/Notification";

class NotificationCollection extends Mongo.Collection {
    constructor( name, options = {} ) {
        // Change transform before calling super, this is the only way to set it
        // Also, this won't suffer from module loading as it is a callback
        options.transform = notification => new Notification( notification );

        super( name, options );

        this.entityName = "notification";

        /**
         * Schema
         */

        this.attachSchema( NotificationSchema );

        /**
         * Behaviors
         */

        this.attachBehaviour( 'timestampable' );

        /**
         * Security
         */

        this.deny( {
            insert: () => true,
            update: () => true,
            remove: () => true
        } );
    }

    /**
     * Helper method to notify a user of something
     * @param notification
     */
    notify( notification ) {
        this.insert( notification );
    }

    /**
     * Helper method to remove a notification
     *
     * Only check the first level of objects (index 0), as doing otherwise could potentially
     * remove unwanted notifications. i.e.: Removing a like on a card would remove a like on
     * a comment on this card.
     *
     * @param {String} user Destination user Id
     * @param {String} subject Action subject user Id
     * @param {String} action Action type
     * @param {String} object First object's Id
     * @param {String} type First object's type
     * @param {Object} and Extra query $and'ed to the base notification removal query
     */
    unnotify( user, subject, action, object, type, and = {} ) {
        const conditions = {};

        if ( user ) {
            conditions.user = user;
        }

        if ( subject ) {
            conditions.subject = subject;
        }

        if ( action ) {
            conditions.action = action;
        }

        if ( object ) {
            conditions['objects.0.id'] = object;
        }

        if ( type ) {
            conditions['objects.0.type'] = type;
        }

        this.remove( and ? { $and: [conditions, and] } : conditions );
    }

    /**
     * Remove all notifications matching the passed notification object.
     *
     * @param {String} id Entity ID
     * @param {String} entityType Entity type name
     */
    unnotifyAllForObject( id, entityType ) {
        this.remove( { 'objects.id': id, 'objects.type': entityType } );
    }
}

export default new NotificationCollection( "notifications" );
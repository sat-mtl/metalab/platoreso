"use strict";

// Collections
import "./NotificationCollection";

// Methods
import "./NotificationMethods";
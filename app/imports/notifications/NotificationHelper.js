"use strict";

import logger from "meteor/metalab:logger/Logger";
import GroupCollection from "/imports/groups/GroupCollection";
import GroupNotificationMethods from "/imports/groups/notifications/GroupNotificationMethods";
import GroupNotificationMessage from "/imports/groups/notifications/GroupNotificationMessage";
import CardCollection from "/imports/cards/CardCollection";
import CardNotificationMethods from "/imports/cards/notifications/CardNotificationMethods";
import CardNotificationMessage from "/imports/cards/notifications/CardNotificationMessage";
import CommentCollection from "/imports/comments/CommentCollection";
import CardCommentNotificationMethods from "/imports/cards/comments/notifications/CardCommentNotificationMethods";
import CardCommentNotificationMessage from "/imports/cards/comments/notifications/CardCommentNotificationMessage";

const log = logger( "NotificationHelper" );

export default class NotificationHelper {

    static getData( notification, cb ) {
        let method;
        switch ( notification.objects[0].type ) {
            case GroupCollection.entityName:
                method = GroupNotificationMethods.fetch;
                break;
            case CardCollection.entityName:
                method = CardNotificationMethods.fetch;
                break;
            case CommentCollection.entityName:
                switch ( notification.objects[1].type ) {
                    case CardCollection.entityName:
                        method = CardCommentNotificationMethods.fetch;
                        break;
                }
                break;
        }

        if ( method ) {
            method.call( { notificationId: notification._id }, ( error, data ) => {
                if ( error ) {
                    log.error( error );
                }
                cb( data )
            } );
        }
    }

    static getMessage( notification, subject, data, html ) {
        switch ( notification.objects[0].type ) {
            case GroupCollection.entityName:
                return GroupNotificationMessage.getMessage( notification, subject, data, html );
                break;
            case CardCollection.entityName:
                return CardNotificationMessage.getMessage( notification, subject, data, html );
                break;
            case CommentCollection.entityName:
                switch ( notification.objects[1].type ) {
                    case CardCollection.entityName:
                        return CardCommentNotificationMessage.getMessage( notification, subject, data, html );
                        break;
                }
                break;
        }

        return null;
    }

    static getLink( notification, subject, data ) {
        switch ( notification.objects[0].type ) {
            case GroupCollection.entityName:
                return GroupNotificationMessage.getLink( notification, subject, data );
                break;
            case CardCollection.entityName:
                return CardNotificationMessage.getLink( notification, subject, data );
                break;
            case CommentCollection.entityName:
                switch ( notification.objects[1].type ) {
                    case CardCollection.entityName:
                        return CardCommentNotificationMessage.getLink( notification, subject, data );
                        break;
                }
                break;
        }

        return null;
    }
}
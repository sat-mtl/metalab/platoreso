"use strict";

import CardCollection from "/imports/cards/CardCollection";
import UserHelpers from "/imports/accounts/UserHelpers";

export default class NotificationMessageHelpers {

    static getCard( notification, cards, role = null ) {
        return cards.find( card => card.id == notification.getObjectIdByType( CardCollection.entityName, role ) );
    }

    static getPhase( notification, project, dataKey ) {
        return project.phases && notification && notification.data && notification.data[dataKey]
            ? project.phases.find( phase => phase._id == notification.data[dataKey] )
            : null;
    }

    static subjectSpan( subject, html ) {
        return html ? `<span class="user subject">${UserHelpers.getDisplayName( subject )}</span>` : UserHelpers.getDisplayName( subject );
    }

    static cardSpan( card, html ) {
        return html ? `<span class="card object">${card.name}</span>` : card.name;
    }

    static phaseSpan( phase, html ) {
        return phase ? ( html ? `<span class="phase object">${phase.name}</span>` : phase.name ) : null;
    }

    static projectSpan( project, html ) {
        return html ? `<span class="project object">${project.name}</span>` : project.name;
    }

    static groupSpan( group, html ) {
        return html ? `<span class="group object">${group.name}</span>` : group.name;
    }
}
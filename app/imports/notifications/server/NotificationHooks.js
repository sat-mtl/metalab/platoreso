"use strict";

import Logger from "meteor/metalab:logger/Logger";
import UserCollection from "/imports/accounts/UserCollection";
import UserHelpers from "/imports/accounts/UserHelpers";
import NotificationCollection from "../NotificationCollection";

const log = Logger( "notification-hooks" );

/**
 * User Removed
 */
UserCollection.after.remove( function ( userId, user ) {
    // Defer, so that methods can return without waiting
    Meteor.defer( () => {
        log.debug( `User "${UserHelpers.getDisplayName( user )}" (${user._id}) removed, cleaning up relations` );

        // Direct, we don't want any hooks running here
        NotificationCollection.direct.remove( { $or: [{ user: user._id }, { subject: user._id }] } );
    } );
} );
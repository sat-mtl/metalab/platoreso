"use strict";

import { Counts } from "meteor/tmeasday:publish-counts";
import UserCollection from "/imports/accounts/UserCollection";
import UserHelpers from "/imports/accounts/UserHelpers";
import NotificationCollection from "./../NotificationCollection";

/**
 * User Notifications List
 *
 * The extended information about objects is left to the subpackages to implement
 *
 * @params {String} id Id of the entity for the feed
 * @params {Object} [options] Options for the find operation (ex: sort)
 */
Meteor.publishComposite( "notifications", function ( options = null ) {
    check( options, Match.OneOf( null, Match.ObjectIncluding( {
        limit: Match.Optional( Number )
    } ) ) );

    options = _.defaults( options || {}, { sort: { createdAt: -1 }, limit: 10 } );

    return {
        find() {
            //FIXME: This can get nasty ram-wise
            Counts.publish( this,
                'notifications/' + this.userId,
                NotificationCollection.find( { user: this.userId }, { fields: { _id: 1 } } ),
                { noReady: true }
            );

            Counts.publish( this,
                'notifications/' + this.userId + '/unread',
                NotificationCollection.find( { user: this.userId, read: false }, { fields: { _id: 1 } } ),
                { noReady: true }
            );

            return NotificationCollection.find( { user: this.userId }, options )
        },
        children: [
            {
                find( notification ) {
                    return UserCollection.find( { _id: notification.subject }, {
                        fields: {
                            'profile.firstName': 1,
                            'profile.lastName':  1,
                            'profile.avatarUrl': 1
                        }
                    } );
                }
            }
        ]
    };
} );
"use strict";

import Logger from "meteor/metalab:logger/Logger";
import UserCollection from "/imports/accounts/UserCollection";
import AccountsMeldHelper from "/imports/accounts/server/AccountsMeldHelper";
import NotificationCollection from "../NotificationCollection";

const log = Logger("notification-meld");

/**
 * Card Melding
 */
AccountsMeldHelper.onMeld( ( src_user_id, dst_user_id ) => {
    log.debug(`Melding notifications for user ${src_user_id} into ${dst_user_id}`);

    NotificationCollection.update( { 'user': src_user_id }, { $set: { 'user': dst_user_id } }, { multi: true } );
    NotificationCollection.update( { 'subject': src_user_id }, { $set: { 'subject': dst_user_id } }, { multi: true } );
    NotificationCollection.update( { 'objects.id': src_user_id, 'objects.type': UserCollection.entityName }, { $set: { 'objects.$.id': dst_user_id } }, { multi: true } );
} );
"use strict";

import { FS } from "meteor/cfs:base-package";
import FilesCollection from "../../files/FilesCollection";

export default class ImageCollectionBase extends FilesCollection {

    static createStores(name) {
        return [
            new FS.Store.FileSystem(name + '_original'),
            new FS.Store.FileSystem(name + '_large'),
            new FS.Store.FileSystem(name + '_medium'),
            new FS.Store.FileSystem(name + '_small'),
            new FS.Store.FileSystem(name + '_thumb')
        ];
    }

    constructor( name, options = {}, config = {} ) {
        super( name, _.extend( options || {}, {
            filter: {
                allow: {
                    contentTypes: ['image/*'] //allow only images in this FS.Collection
                }
            },
            stores: ImageCollectionBase.createStores(name)
        } ), config );
    }
}
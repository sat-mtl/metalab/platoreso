"use strict";

import { FS } from "meteor/cfs:base-package";

let ImageCollectionBase;
if ( Meteor.isServer ) {
    ImageCollectionBase = require( "./server/ImageCollectionBase" ).default;
} else {
    ImageCollectionBase = require( "./client/ImageCollectionBase" ).default;
}

export default class ImagesCollection extends ImageCollectionBase {
    constructor( name, options = {}, config = {} ) {
        _.defaults( config, {overwritePrevious: false} );
        super( name, options, config );
    }
}
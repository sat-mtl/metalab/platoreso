"use strict";

const ImageCollections = {
    stores:      [
        'original',
        'large',
        'medium',
        'small',
        'thumb'
    ]
};

export default ImageCollections;
"use strict";

import Logger from "meteor/metalab:logger/Logger";
import ColorThief from "meteor/metalab:color-thief";
import { FS } from "meteor/cfs:base-package";
import FilesCollection from "../../files/FilesCollection";

const log = Logger( 'image-collection-base-server' );

export default class ImageCollectionBase extends FilesCollection {

    static createStores( name ) {
        const originalStore = new FS.Store.FileSystem( name + '_original', {
            path:           Meteor.settings.uploads.path + name + '/original',
            beforeWrite:    function ( fileObj ) {
                return {
                    name: 'original.' + fileObj.extension()
                }
            },
            transformWrite: function ( fileObj, readStream, writeStream ) {
                readStream.pipe( writeStream );

                gm( readStream )
                // Get image dimensions
                    .size( { bufferStream: true }, FS.Utility.safeCallback( ( err, size ) => {
                        if ( err ) {
                            log.error( err );
                        } else {
                            fileObj.update( {
                                $set: {
                                    ['copies.' + name + '_original.width']:  size.width,
                                    ['copies.' + name + '_original.height']: size.height
                                }
                            } );
                        }
                    } ) )
                    // Get dominant color
                    .resize( 1024, 1024, '>' ) // Arbitrary size to deal with, smaller = faster
                    .toBuffer( 'PNG', FS.Utility.safeCallback( ( err, buffer ) => {
                        if ( err ) {
                            log.error( err );
                        } else {
                            const thief    = new ColorThief();
                            const rgb      = thief.getColor( buffer );
                            const r_string = rgb[0] < 16 ? '0' + rgb[0].toString( 16 ) : rgb[0].toString( 16 );
                            const g_string = rgb[1] < 16 ? '0' + rgb[1].toString( 16 ) : rgb[1].toString( 16 );
                            const b_string = rgb[2] < 16 ? '0' + rgb[2].toString( 16 ) : rgb[2].toString( 16 );
                            fileObj.update( {
                                $set: {
                                    dominantColor: r_string + g_string + b_string
                                }
                            } );
                        }
                    } ) );
            }
        } );

        const largeStore = new FS.Store.FileSystem( name + '_large', {
            path:           Meteor.settings.uploads.path + name + '/large',
            beforeWrite:    function ( fileObj ) {
                return {
                    type: 'image/jpg',
                    name: 'large.jpg'
                }
            },
            transformWrite: function ( fileObj, readStream, writeStream ) {
                const transformed = gm( readStream )
                    .background( '#FFFFFFFF' )
                    .flatten()
                    .resize( 1600, 1600, '>' )
                    .autoOrient()
                    .unsharp( 0.5 )
                    .noProfile()
                    .setFormat( 'jpg' )
                    .quality( 95 )
                    .stream();

                transformed.pipe( writeStream );

                gm( transformed ).size( { bufferStream: true }, FS.Utility.safeCallback( ( err, size ) => {
                    if ( err ) {
                        log.error( err );
                    } else {
                        fileObj.update( {
                            $set: {
                                ['copies.' + name + '_large.width']:  size.width,
                                ['copies.' + name + '_large.height']: size.height
                            }
                        } );
                    }
                } ) );
            }
        } );

        const mediumStore = new FS.Store.FileSystem( name + '_medium', {
            path:           Meteor.settings.uploads.path + name + '/medium',
            beforeWrite:    function ( fileObj ) {
                return {
                    type: 'image/jpg',
                    name: 'medium.jpg'
                }
            },
            transformWrite: function ( fileObj, readStream, writeStream ) {
                const transformed = gm( readStream )
                    .background( '#FFFFFFFF' )
                    .flatten()
                    .resize( 800, 800, '>' )
                    .autoOrient()
                    .unsharp( 0.5 )
                    .noProfile()
                    .setFormat( 'jpg' )
                    .quality( 95 )
                    .stream();

                transformed.pipe( writeStream );

                gm( transformed ).size( { bufferStream: true }, FS.Utility.safeCallback( ( err, size ) => {
                    if ( err ) {
                        log.error( err );
                    } else {
                        fileObj.update( {
                            $set: {
                                ['copies.' + name + '_medium.width']:  size.width,
                                ['copies.' + name + '_medium.height']: size.height
                            }
                        } );
                    }
                } ) );
            }
        } );

        const smallStore = new FS.Store.FileSystem( name + '_small', {
            path:           Meteor.settings.uploads.path + name + '/small',
            beforeWrite:    function ( fileObj ) {
                return {
                    type: 'image/jpg',
                    name: 'small.jpg'
                }
            },
            transformWrite: function ( fileObj, readStream, writeStream ) {
                const transformed = gm( readStream )
                    .background( '#FFFFFFFF' )
                    .flatten()
                    .resize( 400, 400, '>' )
                    .autoOrient()
                    .unsharp( 0.5 )
                    .noProfile()
                    .setFormat( 'jpg' )
                    .quality( 95 )
                    .stream();

                transformed.pipe( writeStream );

                gm( transformed ).size( { bufferStream: true }, FS.Utility.safeCallback( ( err, size ) => {
                    if ( err ) {
                        log.error( err );
                    } else {
                        fileObj.update( {
                            $set: {
                                ['copies.' + name + '_small.width']:  size.width,
                                ['copies.' + name + '_small.height']: size.height
                            }
                        } );
                    }
                } ) );
            }
        } );

        const thumbStore = new FS.Store.FileSystem( name + '_thumb', {
            path:           Meteor.settings.uploads.path + name + '/thumb',
            beforeWrite:    function ( fileObj ) {
                return {
                    type: 'image/jpg',
                    name: 'thumb.jpg'
                }
            },
            transformWrite: function ( fileObj, readStream, writeStream ) {
                const transformed = gm( readStream )
                    .resize( 160, 160, '^' )
                    .autoOrient()
                    .gravity( 'Center' )
                    .background( '#FFFFFFFF' )
                    .extent( 160, 160 )//, 0, 0 )
                    .unsharp( 0.5 )
                    .noProfile()
                    .setFormat( 'jpg' )
                    .quality( 97 )
                    .stream();

                transformed.pipe( writeStream );

                gm( transformed ).size( { bufferStream: true }, FS.Utility.safeCallback( ( err, size ) => {
                    if ( err ) {
                        log.error( err );
                    } else {
                        fileObj.update( {
                            $set: {
                                ['copies.' + name + '_thumb.width']:  size.width,
                                ['copies.' + name + '_thumb.height']: size.height
                            }
                        } );
                    }
                } ) );
            }
        } );

        return [
            thumbStore,
            smallStore,
            mediumStore,
            largeStore,
            originalStore
        ];
    }

    constructor( name, options = {}, config = {} ) {

        if ( !Meteor.settings || !Meteor.settings.uploads || !Meteor.settings.uploads.path ) {
            log.error( `Upload path setting is missing` );
            throw new Meteor.Error( 'missing upload path setting in server/ImageCollectionBase' );
        }

        super( name, _.extend( options || {}, {
            filter: {
                allow: {
                    contentTypes: ['image/*'] //allow only images in this FS.Collection
                }
            },
            stores: ImageCollectionBase.createStores( name )
        } ), config );

        // Indexes
        this.files._ensureIndex( { owners: 1 }, { name: 'owners' } );

        /**
         * This is legacy for groups and projects
         */
        if ( this.config.overwritePrevious ) {
            /**
             * Collection hook
             * Replace the previous image when a new one is uploaded
             */
            this.files.after.insert( ( userId, image ) => {
                this.unlinkOwner( image.owners, image._id );
            } );
        }
    }
}
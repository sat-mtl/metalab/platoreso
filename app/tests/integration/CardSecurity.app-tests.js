"use strict";

import { chai, assert, expect } from 'meteor/practicalmeteor:chai';
import { sinon, spies, stubs } from 'meteor/practicalmeteor:sinon';

import CardCollection from "/imports/cards/CardCollection";

if ( Meteor.isClient ) {
    describe( 'cards/CardSecurity.integration', function () {

        let sandbox;

        beforeEach( () => {
            sandbox = sinon.sandbox.create();
        } );

        afterEach( () => {
            sandbox.restore();
        } );

        describe( 'card visibility', () => {

            beforeEach( done => {
                Meteor.call( 'tests/cards/reset', () => {
                    done();
                } );
            } );

            afterEach( function ( done ) {
                // Logout
                Meteor.logout( () => {
                    done();
                } );
            } );

            describe( 'list', () => {

                describe( 'as an admin', () => {

                    let cardsSubscription;

                    beforeEach( done => {
                        Meteor.loginWithPassword( "admin1@test.com", "admin0123", err => {
                            expect( err ).to.be.undefined;
                            done();
                        } );
                    } );

                    afterEach( done => {
                        cardsSubscription.stop();
                        done();
                    } );

                    it( 'should see all cards in a public project', done => {
                        Meteor.call( 'tests/projects/find', 'public-project', ( err, project ) => {
                            expect( err ).to.be.undefined;
                            console.log(err);
                            cardsSubscription = Meteor.subscribe( 'project/card/list', project._id, () => {
                                expect( CardCollection.find( { project: project._id } ).fetch().length ).to.equal(1);
                                done();
                            } );
                        } );
                    } );

                    it( 'should see all cards in a private project', done => {
                        Meteor.call( 'tests/projects/find', 'private-project', ( err, project ) => {
                            expect( err ).to.be.undefined;
                            cardsSubscription = Meteor.subscribe( 'project/card/list', project._id, () => {
                                expect( CardCollection.find( { project: project._id } ).fetch().length ).to.equal(1);
                                done();
                            } );
                        } );
                    } );

                } );

                describe( 'as a regular user', () => {

                    let cardsSubscription;

                    beforeEach( done => {
                        Meteor.loginWithPassword( "normal@test.com", "normal0123", err => {
                            expect( err ).to.be.undefined;
                            done();
                        } );
                    } );

                    afterEach( function ( done ) {
                        cardsSubscription.stop();
                        done();
                    } );

                    it( 'should see all cards in a public project', done => {
                        Meteor.call( 'tests/projects/find', 'public-project', ( err, project ) => {
                            expect( err ).to.be.undefined;
                            cardsSubscription = Meteor.subscribe( 'project/card/list', project._id, () => {
                                expect( CardCollection.find( { project: project._id } ).fetch().length ).to.equal(1);
                                done();
                            } );
                        } );
                    } );

                    it( 'should not see cards in a private project', done => {
                        Meteor.call( 'tests/projects/find', 'private-project', ( err, project ) => {
                            expect( err ).to.be.undefined;
                            cardsSubscription = Meteor.subscribe( 'project/card/list', project._id, () => {
                                expect( CardCollection.find( { project: project._id } ).fetch().length ).to.equal(0);
                                done();
                            } );
                        } );
                    } );

                } );

                describe( 'as a participant', () => {

                    let cardsSubscription;

                    beforeEach( done => {
                        Meteor.loginWithPassword( "normal@test.com", "normal0123", err => {
                            expect( err ).to.be.undefined;
                            done();
                        } );
                    } );

                    afterEach( function ( done ) {
                        cardsSubscription.stop();
                        done();
                    } );

                    it( 'should see participating as expert cards in private project', ( done ) => {
                        Meteor.call( '__group__addExpert__', 'private-group', 'normal@test.com', err=> {
                            expect( err ).to.be.undefined;
                            Meteor.call( 'tests/projects/find', 'private-project', ( err, project ) => {
                                expect( err ).to.be.undefined;
                                cardsSubscription = Meteor.subscribe( 'project/card/list', project._id, () => {
                                    expect( CardCollection.find( { project: project._id } ).fetch().length ).to.equal(1);
                                    done();
                                } );
                            } );
                        } );
                    } );

                    it( 'should see participating as moderator cards in private project', ( done ) => {
                        Meteor.call( '__group__addModerator__', 'private-group', 'normal@test.com', err=> {
                            expect( err ).to.be.undefined;
                            Meteor.call( 'tests/projects/find', 'private-project', ( err, project ) => {
                                expect( err ).to.be.undefined;
                                cardsSubscription = Meteor.subscribe( 'project/card/list', project._id, () => {
                                    expect( CardCollection.find( { project: project._id } ).fetch().length ).to.equal(1);
                                    done();
                                } );
                            } );
                        } );
                    } );

                    it( 'should see participating as member cards in private project', ( done ) => {
                        Meteor.call( '__group__addMember__', 'private-group', 'normal@test.com', err=> {
                            expect( err ).to.be.undefined;
                            Meteor.call( 'tests/projects/find', 'private-project', ( err, project ) => {
                                expect( err ).to.be.undefined;
                                cardsSubscription = Meteor.subscribe( 'project/card/list', project._id, () => {
                                    expect( CardCollection.find( { project: project._id } ).fetch().length ).to.equal(1);
                                    done();
                                } );
                            } );
                        } );
                    } );

                } );

                describe( 'as an anonymous user', () => {

                    let cardsSubscription;

                    afterEach( function ( done ) {
                        cardsSubscription.stop();
                        done();
                    } );

                    it( 'should see all cards in a public project', done => {
                        Meteor.call( 'tests/projects/find', 'public-project', ( err, project ) => {
                            expect( err ).to.be.undefined;
                            cardsSubscription = Meteor.subscribe( 'project/card/list', project._id, () => {
                                expect( CardCollection.find( { project: project._id } ).fetch().length ).to.equal(1);
                                done();
                            } );
                        } );
                    } );

                    it( 'should not see cards in a private project', done => {
                        Meteor.call( 'tests/projects/find', 'private-project', ( err, project ) => {
                            expect( err ).to.be.undefined;
                            cardsSubscription = Meteor.subscribe( 'project/card/list', project._id, () => {
                                expect( CardCollection.find( { project: project._id } ).fetch().length ).to.equal(0);
                                done();
                            } );
                        } );
                    } );

                } );

            } );

            describe( 'single cards', () => {

                describe( 'as an admin', () => {

                    let cardSubscription;

                    beforeEach( done => {
                        Meteor.loginWithPassword( "admin1@test.com", "admin0123", err => {
                            expect( err ).to.be.undefined;
                            done();
                        } );
                    } );

                    afterEach( done => {
                        if ( cardSubscription ) {
                            cardSubscription.stop();
                        }
                        done();
                    } );

                    it( 'should see private card', done => {
                    console.log('getting it');
                        Meteor.call( 'tests/cards/find', 'private-card', ( err, card ) => {
console.log(card, err);
                            expect( err ).to.be.undefined;
                            cardSubscription = Meteor.subscribe( 'card', card._id, () => {
                                const c = CardCollection.findOne( { _id: card._id } );
                                expect( c ).not.to.be.undefined;
                                expect( c.name ).toBe( 'private-card' );
                                done();
                            } );
                        } );
                    } );

                    it( 'should see public card', done => {
                        Meteor.call( 'tests/cards/find', 'public-card', ( err, card ) => {
                            expect( err ).to.be.undefined;
                            cardSubscription = Meteor.subscribe( 'card', card._id, () => {
                                const c = CardCollection.findOne( { _id: card._id } );
                                expect( c ).not.to.be.undefined;
                                expect( c.name ).toBe( 'public-card' );
                                done();
                            } );
                        } );
                    } );

                } );

                describe( 'as a regular user', () => {

                    let cardSubscription;

                    beforeEach( done => {
                        Meteor.loginWithPassword( "normal@test.com", "normal0123", err => {
                            expect( err ).to.be.undefined;
                            done();
                        } );
                    } );

                    afterEach( function ( done ) {
                        cardSubscription.stop();
                        done();
                    } );

                    it( 'should not see private card', done => {
                        Meteor.call( 'tests/cards/find', 'private-card', ( err, card ) => {
                            expect( err ).to.be.undefined;
                            cardSubscription = Meteor.subscribe( 'card', card._id, () => {
                                const c = CardCollection.findOne( { _id: card._id } );
                                expect( c ).to.be.undefined;
                                done();
                            } );
                        } );
                    } );

                    it( 'should see public card', done => {
                        Meteor.call( 'tests/cards/find', 'public-card', ( err, card ) => {
                            expect( err ).to.be.undefined;
                            cardSubscription = Meteor.subscribe( 'card', card._id, () => {
                                const c = CardCollection.findOne( { _id: card._id } );
                                expect( c ).not.to.be.undefined;
                                expect( c.name ).toBe( 'public-card' );
                                done();
                            } );
                        } );
                    } );

                } );

                describe( 'as a participant', () => {

                    let cardSubscription;

                    beforeEach( done => {
                        Meteor.loginWithPassword( "normal@test.com", "normal0123", err => {
                            expect( err ).to.be.undefined;
                            done();
                        } );
                    } );

                    afterEach( function ( done ) {
                        cardSubscription.stop();
                        done();
                    } );

                    it( 'should see participating as expert card', ( done ) => {
                        Meteor.call( '__group__addExpert__', 'private-group', 'normal@test.com', err=> {
                            expect( err ).to.be.undefined;
                            Meteor.call( 'tests/cards/find', 'private-card', ( err, card ) => {
                                expect( err ).to.be.undefined;
                                cardSubscription = Meteor.subscribe( 'card', card._id, () => {
                                    const c = CardCollection.findOne( { _id: card._id } );
                                    expect( c ).not.to.be.undefined;
                                    expect( c.name ).toBe( 'private-card' );
                                    done();
                                } );
                            } );
                        } );
                    } );

                    it( 'should participating as moderator card', ( done ) => {
                        Meteor.call( '__group__addModerator__', 'private-group', 'normal@test.com', err=> {
                            expect( err ).to.be.undefined;
                            Meteor.call( 'tests/cards/find', 'private-card', ( err, card ) => {
                                expect( err ).to.be.undefined;
                                cardSubscription = Meteor.subscribe( 'card', card._id, () => {
                                    const c = CardCollection.findOne( { _id: card._id } );
                                    expect( c ).not.to.be.undefined;
                                    expect( c.name ).toBe( 'private-card' );
                                    done();
                                } );
                            } );
                        } );
                    } );

                    it( 'should see participating as member card', ( done ) => {
                        Meteor.call( '__group__addMember__', 'private-group', 'normal@test.com', err=> {
                            expect( err ).to.be.undefined;
                            Meteor.call( 'tests/cards/find', 'private-card', ( err, card ) => {
                                expect( err ).to.be.undefined;
                                cardSubscription = Meteor.subscribe( 'card', card._id, () => {
                                    const c = CardCollection.findOne( { _id: card._id } );
                                    expect( c ).not.to.be.undefined;
                                    expect( c.name ).toBe( 'private-card' );
                                    done();
                                } );
                            } );
                        } );
                    } );

                } );

                describe( 'as an anonymous user', () => {

                    let cardSubscription;

                    afterEach( function ( done ) {
                        cardSubscription.stop();
                        done();
                    } );

                    it( 'should not see private card', done => {
                        Meteor.call( 'tests/cards/find', 'private-card', ( err, card ) => {
                            expect( err ).to.be.undefined;
                            cardSubscription = Meteor.subscribe( 'card', card._id, () => {
                                const c = CardCollection.findOne( { _id: card._id } );
                                expect( c ).to.be.undefined;
                                done();
                            } );
                        } );
                    } );

                    it( 'should see public card', done => {
                        Meteor.call( 'tests/cards/find', 'public-card', ( err, card ) => {
                            expect( err ).to.be.undefined;
                            cardSubscription = Meteor.subscribe( 'card', card._id, () => {
                                const c = CardCollection.findOne( { _id: card._id } );
                                expect( c ).not.to.be.undefined;
                                expect( c.name ).toBe( 'public-card' );
                                done();
                            } );
                        } );
                    } );
                } );
            } );
        } );

    } );
}
"use strict";

import { chai, assert, expect } from 'meteor/practicalmeteor:chai';
import { sinon, spies, stubs } from 'meteor/practicalmeteor:sinon';

import "/imports/cards/CardHooks"; // Implied dependency
import CardCollection from "/imports/cards/CardCollection";
import CardHistoryCollection from "/imports/migrations/legacy/CardHistoryCollection";

if (Meteor.isServer) {
    describe( 'cards/Collection', () => {

        let sandbox;

        beforeEach( () => {
            sandbox = sinon.sandbox.create();
        } );

        afterEach( () => {
            sandbox.restore();
        } );

        describe( 'Versioning', function () {

            beforeEach( () => {
                sandbox.stub( Meteor, 'userId' ).returns( 'someUser' );
            } );

            afterEach( ()=> {
                CardCollection.remove( {} );
            } );

            describe( 'when creating a card', () => {

                it( 'should initialize with version at 0', () => {
                    const cardId = CardCollection.insert( {
                        project: "projectId",
                        phase:   "phaseId",
                        author:  "authorId",
                        name:    "Test Card"
                    } );
                    const card   = CardCollection.findOne( { _id: cardId } );
                    expect( card.version ).to.eql( 0 );
                } );

                it( 'should not create a history snapshot', () => {
                    const cardId = CardCollection.insert( {
                        project: "projectId",
                        phase:   "phaseId",
                        author:  "authorId",
                        name:    "Test Card"
                    } );
                    const card   = CardHistoryCollection.findOne( { _id: cardId } );
                    expect( card ).to.be.undefined
                } );

            } );

            describe( 'when updating a card', () => {

                let cardId;

                beforeEach( () => {
                    cardId = CardCollection.insert( {
                        project: "projectId",
                        phase:   "phaseId",
                        author:  "authorId",
                        name:    "Test Card"
                    } );
                } );

                it( 'should update the card in place', () => {
                    CardCollection.update( { _id: cardId }, { $set: { name: "Updated Name" } } );
                    const card = CardCollection.findOne( { _id: cardId } );
                    expect( card.name ).to.eql( "Updated Name" );
                } );

                it( 'should increment the version number', () => {
                    CardCollection.update( { _id: cardId }, { $set: { name: "Updated Name" } } );
                    const card = CardCollection.findOne( { _id: cardId } );
                    expect( card.version ).to.eql( 1 );
                } );

                it( 'should create an history snapshot', () => {
                    CardCollection.update( { _id: cardId }, { $set: { name: "Updated Name" } } );
                    const snapshots = CardHistoryCollection.find( { ref: cardId } ).fetch();
                    expect( snapshots.length ).to.eql( 1 );
                    expect( snapshots[0].version ).to.eql( 0 );
                    expect( snapshots[0].name ).to.eql( "Test Card" );
                    expect( snapshots[0].deleted ).to.be.false;
                } );

                it( 'should not increment version number certain fields', () => {
                    CardCollection.update( { _id: cardId }, { $set: { viewCount: 1 } } );
                    const card = CardCollection.findOne( { _id: cardId } );
                    expect( card.version ).to.eql( 0 );
                } );

                it( 'should not snapshot on certain fields', () => {
                    CardCollection.update( { _id: cardId }, { $set: { viewCount: 1 } } );
                    const snapshots = CardHistoryCollection.find( { ref: cardId } ).fetch();
                    expect( snapshots.length ).to.eql( 0 );
                } );

                describe( "and a conflict occurs", ()=> {

                    beforeEach( () => {
                        const card = CardCollection.findOne( { _id: cardId } );
                        // Fake conflict
                        card.ref   = card._id;
                        delete card._id;
                        CardHistoryCollection.insert( card );
                    } );

                    it( "should retry by incrementing the version number", () => {
                        CardCollection.update( { _id: cardId }, { $set: { name: "Updated Name" } } );
                        const card = CardCollection.findOne( { _id: cardId } );
                        expect( card.version ).to.eql( 2 ); // 1 + Conflict(1)
                    } );

                    it( "should create the snapshot anyway", () => {
                        CardCollection.update( { _id: cardId }, { $set: { name: "Updated Name" } } );
                        const snapshots = CardHistoryCollection.find( { ref: cardId } ).fetch();
                        expect( snapshots.length ).to.eql( 2 );
                    } );

                } );

            } );

            describe( "when removing a card", () => {

                let cardId;

                beforeEach( ()=> {
                    cardId = CardCollection.insert( {
                        project: "projectId",
                        phase:   "phaseId",
                        author:  "authorId",
                        name:    "Test Card"
                    } );
                } );

                describe( "that already has a history", ()=> {

                    beforeEach( ()=> {
                        CardCollection.update( { _id: cardId }, { $set: { name: "Updated Name" } } );
                        const card = CardCollection.findOne( { _id: cardId } );
                        expect( card.version ).to.eql( 1 );
                        const snapshots = CardHistoryCollection.find( { ref: cardId } ).fetch();
                        expect( snapshots.length ).to.eql( 1 );
                    } );

                    it( "should remove the current version", ()=> {
                        CardCollection.remove( { _id: cardId } );
                        const card = CardCollection.findOne( { _id: cardId } );
                        expect( card ).to.be.undefined
                    } );

                    it( "should create a last snapshot + deleted version", ()=> {
                        CardCollection.remove( { _id: cardId } );
                        const snapshots = CardHistoryCollection.find( { ref: cardId } ).fetch();
                        expect( snapshots.length ).to.eql( 3 );
                    } );

                    it( "the created snapshots should have the right version numbers", ()=> {
                        CardCollection.remove( { _id: cardId } );
                        const snapshots = CardHistoryCollection.find( { ref: cardId }, { sort: { version: 1 } } ).fetch();
                        expect( snapshots[0].version ).to.eql( 0 );
                        expect( snapshots[0].deleted ).to.be.false;
                        expect( snapshots[1].version ).to.eql( 1 );
                        expect( snapshots[1].deleted ).to.be.false;
                        expect( snapshots[2].version ).to.eql( 2 );
                        expect( snapshots[2].deleted ).to.be.true;
                    } );

                } );

                describe( "and a conflict occurs", ()=> {

                    beforeEach( () => {
                        const card = CardCollection.findOne( { _id: cardId } );
                        // Fake conflict
                        card.ref   = card._id;
                        delete card._id;
                        CardHistoryCollection.insert( card );
                    } );

                    it( "should retry by incrementing the version number", () => {
                        CardCollection.remove( { _id: cardId } );
                        const snapshot = CardHistoryCollection.findOne( { ref: cardId }, { sort: { version: -1 }, limit: 1 } );
                        expect( snapshot.version ).to.eql( 2 ); // 0 + Conflict(1) + Deleted(1)
                    } );

                    it( "should create the snapshot anyway", () => {
                        CardCollection.remove( { _id: cardId } );
                        const snapshots = CardHistoryCollection.find( { ref: cardId } ).fetch();
                        expect( snapshots.length ).to.eql( 3 ); // 0 + Conflict(1) + Deleted(1)
                    } );

                } );

            } );

        } );

    } );
}
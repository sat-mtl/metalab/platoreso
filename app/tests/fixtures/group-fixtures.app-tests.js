"use strict";

import "./accounts-fixtures.app-tests";
import GroupCollection from "/imports/groups/GroupCollection";
import { GROUP_PREFIX} from "/imports/groups/GroupRoles";

import UserHelpers from "/imports/accounts/UserHelpers";
import { RoleList } from "/imports/accounts/Roles";

if ( Meteor.isAppTest && Meteor.isServer ) {

    /**
     * Public helper methods for tests
     */
    Meteor.methods( {

        /**
         * Unsafe group getter for tests
         */
        __group_find__( group ) {
            check( group, String );
            return GroupCollection.findOne( { slug: group } );
        },

        /**
         * Unsafe add expert to group method for tests
         */
        __group__addExpert__( group, email ) {
            check( group, String );
            check( email, String );
            UserHelpers.addUsersToRoles( Meteor.users.findOne( { 'emails.address': email } )._id, RoleList.expert, GROUP_PREFIX + '_' + GroupCollection.findOne( { slug: group } )._id );
        },

        /**
         * Unsafe add moderator to group method for tests
         */
        __group__addModerator__( group, email ) {
            check( group, String );
            check( email, String );
            UserHelpers.addUsersToRoles( Meteor.users.findOne( { 'emails.address': email } )._id, RoleList.moderator, GROUP_PREFIX + '_' + GroupCollection.findOne( { slug: group } )._id );
        },

        /**
         * Unsafe add member to group method for tests
         */
        __group__addMember__( group, email ) {
            check( group, String );
            check( email, String );
            UserHelpers.addUsersToRoles( Meteor.users.findOne( { 'emails.address': email } )._id, RoleList.member, GROUP_PREFIX + '_' + GroupCollection.findOne( { slug: group } )._id );
        }

    } );

    /**
     * Public helper methods for tests
     */
    Meteor.methods( {

        /**
         * Reset the groups to something fresh
         */
        'fixtures/groups/reset'() {
            Meteor.call( 'fixtures/users/reset' );

            GroupCollection.remove( {} );

            const groups = [
                { name: "Public group", slug: "public-group", description: "This is a test public group", public: true },
                {
                    name:        "Private group",
                    slug:        "private-group",
                    description: "This is a test private group",
                    public:      false
                }
            ];

            _.each( groups, group => GroupCollection.insert( group, err => {
                if ( err ) {
                    console.error( err );
                }
            } ) );

        }

    } );

}
"use strict";

import "./project-fixtures.app-tests";
import ProjectCollection from "/imports/projects/ProjectCollection";
import CardCollection from "/imports/cards/CardCollection";

/**
 * Public helper methods for tests
 */
if ( Meteor.isAppTest && Meteor.isServer ) {

    Meteor.methods( {

        /**
         * Reset the projects to something fresh
         */
            ['tests/cards/reset']() {
            Meteor.call( 'tests/projects/reset' );

            CardCollection.remove( {} );

            const publicProject  = ProjectCollection.findOne( { slug: 'public-project' }, {
                fields: {
                    _id:          1,
                    'phases._id': 1
                }
            } );
            const privateProject = ProjectCollection.findOne( { slug: 'private-project' }, {
                fields: {
                    _id:          1,
                    'phases._id': 1
                }
            } );

            const cards = [
                { name: "public-card", author: "me", project: publicProject._id, phase: publicProject.phases[0]._id },
                {
                    name:    "private-card",
                    author:  "me",
                    project: privateProject._id,
                    phase:   privateProject.phases[0]._id
                }
            ];

            console.log( 'resetting cards' );

            _.each( cards, card => CardCollection.insert( card, err => {
                if ( err ) {
                    console.error( err );
                }
            } ) );

            console.log( CardCollection.find().fetch());
        },

        /**
         * Unsafe card getter for tests
         */
            ['tests/cards/find']( card ) {
            check( card, String );
            console.log( CardCollection.find().fetch());
            return CardCollection.findOne( { name: card } );
        }

    } );

}
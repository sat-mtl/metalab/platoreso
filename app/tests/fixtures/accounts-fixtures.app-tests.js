"use strict";

import UserHelpers from "/imports/accounts/UserHelpers";


if ( Meteor.isAppTest && Meteor.isServer ) {

    /**
     * Public user list used for tests
     * NEVER PUBLISH THIS OUTSIDE OF TESTS
     */
    Meteor.publish( "fixtures/users/list", function () {
        return Meteor.users.find( {} );
    } );

    /**
     * Public helper methods for tests
     */
    Meteor.methods( {
        // LEGACY

        __create_passwordless_user_and_login__() {
            const id = Accounts.createUser( {
                email:   'nopassword@test.com',
                profile: {
                    firstName: 'No Password',
                    lastName:  'John'
                }
            } );
            this.setUserId( id );
            return id;
        }

    } );

    /**
     * Public helper methods for tests
     */
    Meteor.methods( {

        'fixtures/users/reset'() {
            Meteor.users.remove( {} );

            if ( Meteor.isServer ) {

                // LEGACY

                const users = [
                    {
                        profile:  {
                            firstName: "Other User",
                            lastName:  "Last Name"
                        },
                        email:    "other@test.com",
                        roles:    [],
                        password: "normal0123"
                    },
                    {
                        profile:  {
                            firstName: "Normal User",
                            lastName:  "Last Name"
                        },
                        email:    "normal@test.com",
                        roles:    ['user'],
                        password: "normal0123"
                    },
                    {
                        profile:     {
                            firstName: "Unverified User",
                            lastName:  "Last Name"
                        },
                        email:       "unverified@test.com",
                        roles:       ['user'],
                        password:    "unverified0123",
                        doNotVerify: true
                    },
                    {
                        profile:  {
                            firstName: "Admin User 1",
                            lastName:  "Last Name"
                        },
                        email:    "admin1@test.com",
                        roles:    ['admin'],
                        password: "admin0123"
                    },
                    {
                        profile:  {
                            firstName: "Admin User 2",
                            lastName:  "Last Name"
                        },
                        email:    "admin2@test.com",
                        roles:    ['admin'],
                        password: "admin0123"
                    },
                    {
                        profile:  {
                            firstName: "Super User",
                            lastName:  "Last Name"
                        },
                        email:    "super@test.com",
                        roles:    ['super'],
                        password: "super0123"
                    },
                    {
                        profile:  {
                            firstName: "Expert User",
                            lastName:  "Last Name"
                        },
                        email:    "expert@test.com",
                        roles:    ['expert'],
                        password: "expert0123"
                    },
                    {
                        profile:  {
                            firstName: "Moderator User",
                            lastName:  "Last Name"
                        },
                        email:    "moderator@test.com",
                        roles:    ['moderator'],
                        password: "moderator0123"
                    },
                    {
                        profile:  {
                            firstName: "Member User",
                            lastName:  "Last Name"
                        },
                        email:    "member@test.com",
                        roles:    ['member'],
                        password: "member0123"
                    }
                ];

                _.each( users, user => {
                    // Dirty but hey we're in a test
                    user.profile.firstName_sort = user.profile.firstName.toLowerCase();
                    user.profile.lastName_sort  = user.profile.lastName.toLowerCase();

                    var id = Accounts.createUser( {
                        email:    user.email,
                        password: user.password,
                        profile:  user.profile
                    } );

                    // Assume they all have verified emails
                    if ( !user.doNotVerify ) {
                        Meteor.users.update( { _id: id }, { $set: { 'emails.0.verified': true } }, { validate: false } );
                    }

                    if ( user.roles.length > 0 ) {
                        UserHelpers.addUsersToRoles( id, user.roles, UserHelpers.GLOBAL_GROUP );
                    }
                } );
            }
        },

        'fixtures/users/be/admin'() {
            this.setUserId( Meteor.users.findOne( { 'emails.address': "admin1@test.com" }, { fields: { _id: 1 } } )._id );
        },

        'fixtures/users/be/superuser'() {
            this.setUserId( Meteor.users.findOne( { 'emails.address': "super@test.com" }, { fields: { _id: 1 } } )._id );
        },

        'fixtures/users/be/expert'() {
            this.setUserId( Meteor.users.findOne( { 'emails.address': "expert@test.com" }, { fields: { _id: 1 } } )._id );
        },

        'fixtures/users/be/moderator'() {
            this.setUserId( Meteor.users.findOne( { 'emails.address': "moderator@test.com" }, { fields: { _id: 1 } } )._id );
        },

        'fixtures/users/be/member'() {
            this.setUserId( Meteor.users.findOne( { 'emails.address': "member@test.com" }, { fields: { _id: 1 } } )._id );
        },

        'fixtures/users/be/normal'() {
            this.setUserId( Meteor.users.findOne( { 'emails.address': "normal@test.com" }, { fields: { _id: 1 } } )._id );
        },

        'fixtures/users/be/unverified'() {
            this.setUserId( Meteor.users.findOne( { 'emails.address': "unverified@test.com" }, { fields: { _id: 1 } } )._id );
        }

    } );
}
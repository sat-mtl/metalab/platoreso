"use strict";

import "./group-fixtures.app-tests";
import GroupCollection from "/imports/groups/GroupCollection";
import ProjectCollection from "/imports/projects/ProjectCollection";

if ( Meteor.isAppTest && Meteor.isServer ) {

    /**
     * Public helper methods for tests
     */
    Meteor.methods( {

        /**
         * Reset the projects to something fresh
         */
            ['tests/projects/reset']() {
            Meteor.call( 'fixtures/groups/reset' );

            ProjectCollection.remove( {} );

            const projects = [
                {
                    name:        "Public project",
                    slug:        "public-project",
                    description: "This is a test public project",
                    public:      true,
                    phases:      [
                        { name: 'Phase One', description: 'The first phase' },
                        { name: 'Phase Two', description: 'The second phase' },
                        { name: 'Phase Three', description: 'The third phase' }
                    ]
                },
                {
                    name:        "Private project",
                    slug:        "private-project",
                    description: "This is a test private project",
                    public:      false,
                    phases:      [
                        { name: 'Phase One', description: 'The first phase' },
                        { name: 'Phase Two', description: 'The second phase' },
                        { name: 'Phase Three', description: 'The third phase' }
                    ]
                }
            ];

            _.each( projects, project => ProjectCollection.insert( project, err => {
                if ( err ) {
                    console.error( err );
                }
            } ) );

            Meteor.call( 'tests/projects/addGroup', 'public-project', 'public-group' );
            Meteor.call( 'tests/projects/addGroup', 'private-project', 'public-group' );
            Meteor.call( 'tests/projects/addGroup', 'public-project', 'private-group' );
            Meteor.call( 'tests/projects/addGroup', 'private-project', 'private-group' );
        },

        /**
         * Unsafe project getter for tests
         */
            ['tests/projects/find']( project ) {
            check( project, String );
            return ProjectCollection.findOne( { slug: project } );
        },

        /**
         * Unsafe add group to project method for tests
         */
            ['tests/projects/addGroup']( project, group ) {
            check( project, String );
            check( group, String );
            ProjectCollection.update( {
                _id: ProjectCollection.findOne( { slug: project } )._id
            }, {
                $addToSet: {
                    groups: GroupCollection.findOne( { slug: group } )._id
                }
            }, err => {
                if ( err ) {
                    console.error( err );
                }
            } );
        }

    } );
}
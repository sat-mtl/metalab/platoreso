"use strict";

//  #### ##     ## ########   #######  ########  ########  ######
//   ##  ###   ### ##     ## ##     ## ##     ##    ##    ##    ##
//   ##  #### #### ##     ## ##     ## ##     ##    ##    ##
//   ##  ## ### ## ########  ##     ## ########     ##     ######
//   ##  ##     ## ##        ##     ## ##   ##      ##          ##
//   ##  ##     ## ##        ##     ## ##    ##     ##    ##    ##
//  #### ##     ## ##         #######  ##     ##    ##     ######

// Config
import "/imports/config/collectionfs";

// General
import "/imports/i18n/main";

// Packages
import "/imports/settings/main";
import "/imports/jobs/main";
import "/imports/collections/main";
import "/imports/accounts/main";
import "/imports/notifications/main";
import "/imports/moderation/main";
import "/imports/comments/main";
import "/imports/groups/main";
import "/imports/projects/main";
import "/imports/cards/main";
import "/imports/messages/main";
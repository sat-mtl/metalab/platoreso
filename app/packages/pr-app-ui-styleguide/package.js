Package.describe( {
    name:          'metalab:pr-app-ui-styleguide',
    version:       '1.0.0',
    summary:       'PlatoReso App UI Style Guide Package.',
    git:           '',
    documentation: 'README.md',
    debugOnly: true
} );

Package.onUse( function ( api ) {

    api.use( [
        'less',
        'metalab:pr-base-ui'
    ]);

    api.addFiles( [
        //'client/views/StyleGuide.less'
    ]);

    api.addFiles( [
        'client/views/StyleGuide.import.jsx',
        'client/views/StyleGuide.less',

        'client/fixtures/image-fixtures.import.js',
        'client/fixtures/user-fixtures.import.js',
        'client/fixtures/project-fixtures.import.js',
        'client/fixtures/card-fixtures.import.js',

        'client/views/styleGuide/Forms.import.jsx',
        'client/views/styleGuide/Cards.import.jsx',
        'client/views/styleGuide/CardDetails.import.jsx',

        'client/Routes.import.jsx',
        'client/main.js'
    ], 'client');

    api.addAssets([
        'public/horizontal.jpg',
        'public/horizontal-wide.jpg',
        'public/vertical.jpg'
    ], 'client');
} );
"use strict";

import {Router, Route, IndexRoute, RouterHistory} from 'react-router';

import StyleGuide from './views/StyleGuide';

import Forms from './views/styleGuide/Forms';
import Cards from './views/styleGuide/Cards';
import CardDetails from './views/styleGuide/CardDetails';

export default [
    <Route path="/styleguide" component={StyleGuide}>
        <Route path="forms" component={Forms}/>
        <Route path="cards" component={Cards}/>
        <Route path="card_details" component={CardDetails}/>
    </Route>
];
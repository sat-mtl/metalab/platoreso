"use strict";

import { CardDetails } from '{}/client/views/card/CardDetails';

import * as ImageFixtures from '../../fixtures/image-fixtures';
import * as UserFixtures from '../../fixtures/user-fixtures';
import * as ProjectFixtures from '../../fixtures/project-fixtures';
import * as CardFixtures from '../../fixtures/card-fixtures';

import React, { Component, PropTypes } from "react";

const previewProps = {
    cardId: 'cardId',
    readOnly: false,
    onFinish: () => {},
    showCard: () => {},
    cardLoading: false,
    card: CardFixtures.fullCard,
    projectLoading: false,
    project: ProjectFixtures.dummyProject(),
    author: UserFixtures.dummyUser(),
    image: ImageFixtures.horizontalImage,
    linkedCards: []
};

export default class CardDetailsStyleGuide extends Component {

    render() {

        return (
            <div className="ui padded grid">
                <div className="sixteen wide column">
                    <h1>Card Details</h1>

                    <div className="well">
                        <CardDetails {...previewProps}/>
                    </div>
                </div>
            </div>
        )
    }
}
"use strict";

import { CardPreview } from '{}/client/views/card/CardPreview';
import CardDragPreview from '{}/client/views/project/common/CardDragPreview';

import * as ImageFixtures from '../../fixtures/image-fixtures';
import * as CardFixtures from '../../fixtures/card-fixtures';

import React, { Component, PropTypes } from "react";

const previewProps = {
    project:            {},
    phase:              {},
    card:               CardFixtures.fullCard,
    author:             {
        profile: {
            firstName: 'John',
            lastName:  'Doe'
        }
    },
    editable:           true,
    readOnly:           false,
    isDragging:         false,
    isHovered:          false,
    showLinked:         false,
    showCard:           () => {
    },
    editCard:           () => {
    },
    dragCard:           () => {
    },
    likeCard:           () => {
    },
    reportCard:         () => {
    },
    connectDragSource:  c=>c,
    connectDragPreview: c=>c,
    connectDropTarget:  c=>c
};

export default class Cards extends Component {

    renderCardSuite( layout ) {
        return (
            <div className="ui two column padded grid well">
                <div className="column">
                    <h5>Full featured, horizontal image</h5>
                    <CardPreview layout={layout} {...previewProps} card={CardFixtures.fullCard} image={ImageFixtures.horizontalImage}/>
                </div>
                <div className="column">
                    <h5>Full featured, wide horizontal image</h5>
                    <CardPreview layout={layout} {...previewProps} card={CardFixtures.fullCard} image={ImageFixtures.horizontalImageWide}/>
                </div>
                <div className="column">
                    <h5>Full featured, vertical image</h5>
                    <CardPreview layout={layout} {...previewProps} card={CardFixtures.fullCard} image={ImageFixtures.verticalImage}/>
                </div>
                <div className="column">
                    <h5>Full featured</h5>
                    <CardPreview layout={layout} {...previewProps} card={CardFixtures.fullCard}/>
                </div>
                <div className="column">
                    <h5>Full featured, many tags</h5>
                    <CardPreview layout={layout} {...previewProps} card={CardFixtures.fullCardWithManyTags}/>
                </div>
                <div className="column">
                    <h5>Full featured, horizontal image, too many tags</h5>
                    <CardPreview layout={layout} {...previewProps} card={CardFixtures.fullCard} image={ImageFixtures.horizontalImage}/>
                </div>
                <div className="column">
                    <h5>Full featured, wide horizontal image, too many tags</h5>
                    <CardPreview layout={layout} {...previewProps} card={CardFixtures.fullCardWithManyTags} image={ImageFixtures.horizontalImageWide}/>
                </div>
                <div className="column">
                    <h5>Full featured, vertical image, too many tags</h5>
                    <CardPreview layout={layout} {...previewProps} card={CardFixtures.fullCardWithManyTags} image={ImageFixtures.verticalImage}/>
                </div>
                <div className="column">
                    <h5>Full featured, no tags</h5>
                    <CardPreview layout={layout} {...previewProps} card={CardFixtures.fullCardWithoutTags}/>
                </div>
                <div className="column">
                    <h5>No content, horizontal image</h5>
                    <CardPreview layout={layout} {...previewProps} card={CardFixtures.simpleCardWithTags} image={ImageFixtures.horizontalImage}/>
                </div>
                <div className="column">
                    <h5>No content, vertical image</h5>
                    <CardPreview layout={layout} {...previewProps} card={CardFixtures.simpleCardWithTags} image={ImageFixtures.verticalImage}/>
                </div>
                <div className="column">
                    <h5>No content</h5>
                    <CardPreview layout={layout} {...previewProps} card={CardFixtures.simpleCardWithTags}/>
                </div>
                <div className="column">
                    <h5>No content, no tags</h5>
                    <CardPreview layout={layout} {...previewProps} card={CardFixtures.simpleCardWithoutTags}/>
                </div>
                <div className="column">
                    <h5>Full, featured, long title</h5>
                    <CardPreview layout={layout} {...previewProps} card={CardFixtures.fullLongNamedCard}/>
                </div>
                <div className="column">
                    <h5>No content, long title</h5>
                    <CardPreview layout={layout} {...previewProps} card={CardFixtures.simpleLongNamedCard}/>
                </div>
                <div className="column">
                    <h5>Full featured, loading horizontal image</h5>
                    <CardPreview layout={layout} {...previewProps} card={CardFixtures.fullCard} image={ImageFixtures.loadingHorizontalImage}/>
                </div>
                <div className="column">
                    <h5>Full featured, loading vertical image</h5>
                    <CardPreview layout={layout} {...previewProps} card={CardFixtures.fullCard} image={ImageFixtures.loadingVerticalImage}/>
                </div>
            </div>
        );
    }

    render() {

        return (
            <div className="ui padded grid">
                <div className="sixteen wide column">
                    <h1>Cards</h1>

                    <h2>Card Previews</h2>

                    <h3>CardPreview Wrapper</h3>

                    <h4>Block Layout</h4>
                    { this.renderCardSuite( 'block' ) }

                    <h4>Grid Layout</h4>
                    { this.renderCardSuite( 'grid' ) }

                    <h4>List Layout</h4>
                    { this.renderCardSuite( 'list' ) }

                    <h4>Drag Preview</h4>
                    <div className="well">
                        <CardDragPreview card={previewProps.card}/>
                    </div>
                </div>
            </div>
        )
    }
}
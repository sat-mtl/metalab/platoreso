"use strict";

import {Link} from 'react-router';

import Form from '{}/client/components/forms/Form';
import Field from '{}/client/components/forms/Field';
import Input from '{}/client/components/forms/Input';
import Select from '{}/client/components/forms/Select';
import Textarea from '{}/client/components/forms/Textarea';

import React, { Component, PropTypes } from "react";

export default class Forms extends Component {
    render() {

        return (
            <div className="ui padded grid">
                <div className="sixteen wide column">
                    <h1>Forms</h1>

                    <h2>Fields</h2>

                    <h3>A field can have a label</h3>
                    <div className="ui form">
                        <Field label="Field with a label"/>
                    </div>

                    <h3>A field can have children</h3>
                    <div className="ui form">
                        <Field label="Field with children">
                            <Input/>
                        </Field>
                    </div>

                    <h3>A field can have an error</h3>
                    <Form error={{keys:{test:{type:'This is an error!'}}}}>
                        <Field label="Field with error">
                            <Input name="test" />
                        </Field>
                    </Form>

                    <h2>Select</h2>

                    <h3>A Select can have a label</h3>
                    <Form>
                        <Field label="Select">
                            <Select/>
                        </Field>
                    </Form>

                    <h3>A Select can have an error</h3>
                    <Form error={{keys:{test:{type:'This is an error!'}}}}>
                        <Field label="TextArea with error">
                            <Select name="test"/>
                        </Field>
                    </Form>

                    <h2>TextArea</h2>

                    <h3>A Text Area can have a label</h3>
                    <Form>
                        <Field label="Text area">
                            <Textarea/>
                        </Field>
                    </Form>

                    <h3>A TextArea can have an error</h3>
                    <Form error={{keys:{test:{type:'This is an error!'}}}}>
                        <Field label="TextArea with error">
                            <Textarea name="test"/>
                        </Field>
                    </Form>

                </div>
            </div>
        );
    }
}
"use strict";

import {Link} from 'react-router';

import React, { Component } from 'react';

export default class StyleGuide extends Component {
    render() {
        return (
            <div id="styleguide">

                <header id="header">
                    <nav id="mainMenu" className="ui inverted small menu fixed top">
                        <Link to="/" className="image logo item">
                            <img src="/images/logo.png" title={__('PlatoReso')} className="ui image"/>
                        </Link>
                        <div className="item">Style Guide</div>
                    </nav>
                </header>

                <nav className="ui inverted vertical menu fixed top">
                    <Link to="/styleguide/forms" className="teal item" activeClassName="active">Forms</Link>
                    <Link to="/styleguide/cards" className="teal item" activeClassName="active">Cards</Link>
                    <Link to="/styleguide/card_details" className="teal item" activeClassName="active">Card Details</Link>
                </nav>

                <section className="styleguide">
                    {this.props.children}
                </section>

            </div>
        )
    }
}
"use strict";

export const image = {
    isImage:     () => true,
    isUploaded:  () => true,
    hasStored:   () => true,
    url:         options => '',
    getCopyInfo: store => ({}),
    dominantColor: 'c0c0c0'
};

export const horizontalImage        = _.extend( _.clone( image ), {
    url:         options => '/packages/metalab_pr-app-ui-styleguide/public/horizontal.jpg',
    getCopyInfo: store => ({ width: 2048, height: 1365 })
} );

export const horizontalImageWide        = _.extend( _.clone( image ), {
    url:         options => '/packages/metalab_pr-app-ui-styleguide/public/horizontal-wide.jpg',
    getCopyInfo: store => ({ width: 2048, height: 831 })
} );

export const verticalImage          = _.extend( _.clone( image ), {
    url:         options => '/packages/metalab_pr-app-ui-styleguide/public/vertical.jpg',
    getCopyInfo: store => ({ width: 1366, height: 2048 })
} );

export const loadingHorizontalImage = _.extend( _.clone( image ), {
    isUploaded:  () => false,
    getCopyInfo: store => ({ width: 2048, height: 1365 })
} );

export const loadingVerticalImage   = _.extend( _.clone( image ), {
    isUploaded:  () => false,
    getCopyInfo: store => ({ width: 1366, height: 2048 })
} );
export const brokenImage            = {};
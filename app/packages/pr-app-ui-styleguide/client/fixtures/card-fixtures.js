"use strict";

import Card from "/imports/cards/Card";

export const cardData = {
    name:      'Lorem Ipsum',
    content:   'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sed libero posuere, ' +
               'accumsan nunc finibus, imperdiet nisl. Interdum et malesuada fames ac ante ipsum primis in ' +
               'faucibus. Pellentesque molestie commodo sapien, id tempor felis accumsan ac. Aenean ' +
               'ultrices ipsum in diam pellentesque ultricies. Cras condimentum congue ornare. Nam iaculis ' +
               'nunc in turpis vehicula vestibulum. Mauris quis ex suscipit, pharetra nibh ac, pharetra ' +
               'sapien. Integer accumsan, ipsum vitae tincidunt gravida, ipsum leo lobortis urna, ut ' +
               'lobortis lacus metus ut risus.',
    tags:      ['such', 'much', 'many'],
    createdAt: new Date()
};
export const longName = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sed libero posuere.';

export const fullCard              = new Card( cardData );
export const fullCardWithoutTags   = new Card( _.omit( cardData, 'tags' ) );
export const fullCardWithManyTags  = new Card( _.extend( _.clone( cardData ), { tags: [
    'too','many','freaking','tags','for','this','poor','little','card', '#icantbelieveitsnotbutter', '#hashtag', 'I don\'t know what I am doing', 'tags should be short'
]} ) );
export const simpleCardWithTags    = new Card( _.omit( cardData, 'content' ) );
export const simpleCardWithoutTags = new Card( _.omit( cardData, 'content', 'tags' ) );
export const fullLongNamedCard     = new Card( _.extend( _.clone(cardData), { name: longName } ) );
export const simpleLongNamedCard   = new Card( _.extend( _.clone( _.omit( cardData, 'content' ) ), { name: longName } ) );
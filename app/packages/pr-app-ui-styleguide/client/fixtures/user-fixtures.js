"use strict";

import { RoleList } from "/imports/accounts/Roles";

export function dummyUser() {
    return {
        profile:   {
            firstName: faker.name.firstName(),
            lastName:  faker.name.lastName(),
            biography: faker.lorem.sentence(faker.random.number(300)),
            avatarUrl: faker.image.avatar()
        },
        emails:    [
            {
                address:  faker.internet.email(),
                verified: faker.random.arrayElement( [true, true, true, true, false] )
            }
        ],
        roles:     {'__global_roles__': [faker.random.arrayElement( [
            RoleList.admin,

            RoleList.super,
            RoleList.super,

            RoleList.moderator,
            RoleList.moderator,
            RoleList.moderator,

            RoleList.user,
            RoleList.user,
            RoleList.user,
            RoleList.user,
            RoleList.user,
            RoleList.user,
            RoleList.user,
            RoleList.user
        ] ) ] },
        enabled:   faker.random.arrayElement( [true, true, true, true, true, false] ),
        completed: true,
        createdAt: faker.date.past( 2 ),
        updatedAt: faker.date.recent( 12 )
    }
}
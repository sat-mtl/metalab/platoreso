"use strict";

import slugify from "underscrore.string/slugify";
import Project from "/imports/projects/model/Project";

export function dummyProject() {
    return new Project( {
        name:         faker.commerce.productName(),
        description:  faker.company.catchPhrase(),
        content:      faker.lorem.paragraphs( faker.random.number( 5 ) ),
        hashtag:      slugify( faker.company.bsBuzz() ),
        public:       faker.random.boolean(),
        showTimeline: faker.random.boolean(),
        phases:       []
    } );
}
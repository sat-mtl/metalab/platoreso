import { registerRouteLoader } from "/imports/router/Routes";

if ( typeof debug != 'undefined' && debug != null ) {
    d = debug( 'pr:app-ui-styleguide' );
}

registerRouteLoader( ( location, cb ) => {
    d && d('loading dynamic routes');

    System.import( '{metalab:pr-app-ui-styleguide}/client/Routes' )
        .then( routes => {
            cb( routes.default )
        } )
        .catch( error => {
            console.error( error );
        } );
} );
"use strict";

import ProjectCollection from "/imports/projects/ProjectCollection";

import CardCollection from "/imports/cards/CardCollection";

const { PollPhaseService } = PR.PollPhase;

describe( 'pr-projects-phases-poll/both/unit/PhaseService', ()=> {

    let context;
    let service;
    let security_update;
    let security_for;
    let security_throw;

    beforeEach( ()=> {
        service = new PollPhaseService();

        context = {userId: '420'};

        security_throw  = jasmine.createSpy( 'throw' );
        security_for    = jasmine.createSpy( 'for' ).and.returnValue( {throw: security_throw} );
        security_update = jasmine.createSpy( 'update' ).and.returnValue( {for: security_for} );
        spyOn( Security, 'can' ).and.returnValue( {update: security_update} );
    } );

    describe( 'update card', () => {

        beforeEach( ()=> {
            spyOn( CardCollection, 'update' );
        } );

        describe( 'question removal', ()=> {

            it( 'should remove removedQuestions from the update info', () => {
                const info     = {_id: 'cardId', removedQuestions: ['q1', 'q2']};
                const modifier = {
                    $unset: {
                        'phaseData.phaseId.poll.questions.q1': null,
                        'phaseData.phaseId.poll.questions.q2': null
                    }
                };

                expect( ()=>service.updateComment.call( context, 'cardId', 'phaseId', info ) ).not.toThrow();

                expect( info.removedQuestions ).toBeUndefined();

                expect( Security.can ).toHaveBeenCalledWith( context.userId );
                expect( security_update ).toHaveBeenCalledWith( 'cardId', modifier );
                expect( security_for ).toHaveBeenCalledWith( CardCollection );
                expect( security_throw ).toHaveBeenCalled();
                expect( CardCollection.update ).toHaveBeenCalledWith( {
                    _id: 'cardId'
                }, modifier );
            } );

            it( 'should not remove removedQuestions and throw if not allowed', () => {
                const info     = {_id: 'cardId', removedQuestions: ['q1', 'q2']};
                const modifier = {
                    $unset: {
                        'phaseData.phaseId.poll.questions.q1': null,
                        'phaseData.phaseId.poll.questions.q2': null
                    }
                };
                security_throw.and.throwError( 'security' );

                expect( ()=>service.updateComment.call( context, 'cardId', 'phaseId', info ) ).toThrowError( /security/ );

                expect( Security.can ).toHaveBeenCalledWith( context.userId );
                expect( security_update ).toHaveBeenCalledWith( 'cardId', modifier );
                expect( security_for ).toHaveBeenCalledWith( CardCollection );
                expect( security_throw ).toHaveBeenCalled();
                expect( CardCollection.update ).not.toHaveBeenCalled();
            } );

            function expectNotRemoved() {
                expect( Security.can ).not.toHaveBeenCalled();
                expect( security_update ).not.toHaveBeenCalled();
                expect( security_for ).not.toHaveBeenCalled();
                expect( security_throw ).not.toHaveBeenCalled();
                expect( CardCollection.update ).not.toHaveBeenCalled();
            }

            it( 'should not remove removedQuestions if undefined', () => {
                const info = {_id: 'cardId'};
                expect( ()=>service.updateComment.call( context, 'cardId', 'phaseId', info ) ).not.toThrow();
                expect( info.removedQuestions ).toBeUndefined();
                expectNotRemoved();
            } );

            it( 'should not remove removedQuestions if empty', () => {
                const info = {_id: 'cardId', removedQuestions: []};
                expect( ()=>service.updateComment.call( context, 'cardId', 'phaseId', info ) ).not.toThrow();
                expect( info.removedQuestions ).toBeUndefined();
                expectNotRemoved();
            } );

            it( 'should not remove removedQuestions if not an array', () => {
                const info = {_id: 'cardId', removedQuestions: 'not an array'};
                expect( ()=>service.updateComment.call( context, 'cardId', 'phaseId', info ) ).not.toThrow();
                expect( info.removedQuestions ).toBeUndefined();
                expectNotRemoved();
            } );

        } );

        describe( 'answer removal', ()=> {

            it( 'should remove removedAnswers from the update info', () => {
                const info     = {_id: 'cardId', removedAnswers: {'q1': ['a1', 'a2'], 'q2': ['a3']}};
                const modifier = {
                    $unset: {
                        'phaseData.phaseId.poll.questions.q1.answers.a1': null,
                        'phaseData.phaseId.poll.questions.q1.answers.a2': null,
                        'phaseData.phaseId.poll.questions.q2.answers.a3': null
                    }
                };

                expect( ()=>service.updateComment.call( context, 'cardId', 'phaseId', info ) ).not.toThrow();

                expect( info.removedAnswers ).toBeUndefined();

                expect( Security.can ).toHaveBeenCalledWith( context.userId );
                expect( security_update ).toHaveBeenCalledWith( 'cardId', modifier );
                expect( security_for ).toHaveBeenCalledWith( CardCollection );
                expect( security_throw ).toHaveBeenCalled();
                expect( CardCollection.update ).toHaveBeenCalledWith( {
                    _id: 'cardId'
                }, modifier );
            } );

            it( 'should not remove removedAnswers and throw if not allowed', () => {
                const info     = {_id: 'cardId', removedAnswers: {'q1': ['a1', 'a2'], 'q2': ['a3']}};
                const modifier = {
                    $unset: {
                        'phaseData.phaseId.poll.questions.q1.answers.a1': null,
                        'phaseData.phaseId.poll.questions.q1.answers.a2': null,
                        'phaseData.phaseId.poll.questions.q2.answers.a3': null
                    }
                };
                security_throw.and.throwError( 'security' );

                expect( ()=>service.updateComment.call( context, 'cardId', 'phaseId', info ) ).toThrowError( /security/ );

                expect( Security.can ).toHaveBeenCalledWith( context.userId );
                expect( security_update ).toHaveBeenCalledWith( 'cardId', modifier );
                expect( security_for ).toHaveBeenCalledWith( CardCollection );
                expect( security_throw ).toHaveBeenCalled();
                expect( CardCollection.update ).not.toHaveBeenCalled();
            } );

            function expectNotRemoved() {
                expect( Security.can ).not.toHaveBeenCalled();
                expect( security_update ).not.toHaveBeenCalled();
                expect( security_for ).not.toHaveBeenCalled();
                expect( security_throw ).not.toHaveBeenCalled();
                expect( CardCollection.update ).not.toHaveBeenCalled();
            }

            it( 'should not remove removedAnswers if undefined', () => {
                const info = {_id: 'phaseId'};
                expect( ()=>service.updateComment.call( context, 'cardId', 'phaseId', info ) ).not.toThrow();
                expect( info.removedAnswers ).toBeUndefined();
                expectNotRemoved();
            } );

            it( 'should not remove removedAnswers if empty', () => {
                const info = {_id: 'phaseId', removedAnswers: []};
                expect( ()=>service.updateComment.call( context, 'cardId', 'phaseId', info ) ).not.toThrow();
                expect( info.removedAnswers ).toBeUndefined();
                expectNotRemoved();
            } );

            it( 'should not remove removedAnswers if not an array', () => {
                const info = {_id: 'phaseId', removedAnswers: 'not an array'};
                expect( ()=>service.updateComment.call( context, 'cardId', 'phaseId', info ) ).not.toThrow();
                expect( info.removedAnswers ).toBeUndefined();
                expectNotRemoved();
            } );

        } );

    } );

} );
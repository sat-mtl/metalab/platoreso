"use strict";

import CardCollection from "/imports/cards/CardCollection";

const { PollService } = PR.PollPhase;

describe( 'pr-projects-phases-poll/both/unit/PollService', () => {

    let context;
    let service;
    let security_update;
    let security_for;
    let security_throw;

    beforeEach( () => {
        service = new PollService();

        context = { userId: '420' };

        security_throw  = jasmine.createSpy( 'throw' );
        security_for    = jasmine.createSpy( 'for' ).and.returnValue( {throw: security_throw} );
        security_update = jasmine.createSpy( 'update' ).and.returnValue( {for: security_for} );
        spyOn( Security, 'can' ).and.returnValue( {update: security_update} );
    });

    describe( 'recording answer', () => {

        beforeEach(() => {
            spyOn( CardCollection, 'update');
        });

        it( 'should record a vote in a poll', () => {

            const info = {  };

            expect( () => service.answer.bind( context, 'cardId', 'phaseId', info ) ).not.toThrow();

        });

    });

});
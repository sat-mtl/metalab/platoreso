"use strict";

import PhaseTypes from "/imports/projects/phases/PhaseTypes";

describe('pr-projects-phases-poll/both/integration/PhaseService', ()=>{

    it('should be registered', () => {
        expect(PhaseTypes.list.indexOf('poll')).not.toBe(-1);
        expect(PhaseTypes.get('poll')).toBeDefined();
    })

});
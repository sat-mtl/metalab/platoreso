/*Package.describe( {
    name:          'metalab:pr-projects-phases-poll',
    version:       '0.0.1',
    summary:       'PlatoReso Poll Phase Package',
    git:           '',
    documentation: 'README.md'
} );

Package.onUse( function ( api ) {

    api.use([
        'metalab:pr-base',
        'metalab:pr-collections'
    ]);

    api.addFiles( [
        // GENERAL
        'lib/namespace.js',
        'lib/startup.js',
        'lib/PollPhaseService.js',
        'lib/PollService.js',
        'lib/PollMethods.js'
    ] );

} );

Package.onTest(function(api){
    api.use([
        'sanjo:jasmine@0.21.0',
        'metalab:pr-base',
        'metalab:pr-collections',
        'metalab:pr-test-fixtures',
        'metalab:pr-projects-phases-poll'
    ]);
    api.addFiles([
        // UNIT
        'tests/jasmine/both/unit/PhaseServiceSpec.js',
        // INTEGRATION
        'tests/jasmine/both/integration/namespace-spec.js',
        'tests/jasmine/both/integration/PhaseServiceSpec.js'
    ]);
    api.addFiles([

    ], 'server');

    api.addFiles([
    ], 'client');
});*/
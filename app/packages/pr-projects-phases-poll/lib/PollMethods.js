"use strict";

import wrapServiceMethod from "/imports/utils/wrapServiceMethod";
const { PollService } = PR.PollPhase;

PR.PollPhase.methods = {
    answer: wrapServiceMethod( 'pr/poll/answer', PollService.answer )
};
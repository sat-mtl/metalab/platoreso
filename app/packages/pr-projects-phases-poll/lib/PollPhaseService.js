"use strict";

import PhaseService from "/imports/projects/phases/PhaseService";
import PhaseTypes from "/imports/projects/phases/PhaseTypes";
import CardCollection from "/imports/cards/CardCollection";

class PollPhaseService extends PhaseService {

    /**
     * TODO: This is just a quick and dirty way to support card polls, should integrate better
     */
    updateCard( cardId, phaseId, cardInfo ) {
        check( cardId, String );
        check( phaseId, String );
        check( cardInfo, Object );

        //TODO: Combine both into one update call

        /* QUESTION REMOVAL */
        if ( cardInfo.removedQuestions && !_.isEmpty( cardInfo.removedQuestions ) && _.isArray( cardInfo.removedQuestions ) ) {
            // Reduce the question ids into an object for $unset
            const modifier = {
                $unset: cardInfo.removedQuestions.reduce( ( unset, questionId ) => {
                    unset['phaseData.' + phaseId + '.poll.questions.' + questionId] = null;
                    return unset;
                }, {} )
            };

            Security.can( this.userId ).update( cardId, modifier ).for( CardCollection ).throw();
            CardCollection.update( { id: cardId, curernt: true }, modifier );

            //TODO: Remove related information
        }

        // Don't leave traces, this doesn't go in the document
        delete cardInfo.removedQuestions;

        /* ANSWER REMOVAL */
        if ( cardInfo.removedAnswers && !_.isEmpty( cardInfo.removedAnswers ) && _.isObject( cardInfo.removedAnswers ) ) {
            // Reduce the question ids into an object for $unset
            const modifier = {
                $unset: Object.keys( cardInfo.removedAnswers ).reduce( ( unset, questionId ) => {
                    const answers = cardInfo.removedAnswers[questionId];
                    if ( !_.isEmpty( answers ) && _.isArray( answers ) ) {
                        answers.forEach( answerId=> {
                            unset['phaseData.' + phaseId + '.poll.questions.' + questionId + '.answers.' + answerId] = null;
                        } )
                    }
                    return unset;
                }, {} )
            };

            Security.can( this.userId ).update( cardId, modifier ).for( CardCollection ).throw();
            CardCollection.update( { id: cardId, current: true }, modifier );

            //TODO: Remove related information
        }

        // Don't leave traces, this doesn't go in the document
        delete cardInfo.removedAnswers;
    }

}

PR.PollPhase.PollPhaseService = PollPhaseService;

// Register the poll phase type
PhaseTypes.register( 'poll', new PollPhaseService() );
Npm.depends({
    "tinymce": "4.3.6"
});

Package.describe( {
    name:          'metalab:tinymce',
    version:       '4.3.6',
    summary:       'Wrapper package for tinymce',
    git:           '',
    documentation: 'README.md'
} );

Package.onUse( function ( api ) {

    api.addFiles([
        '.npm/package/node_modules/tinymce/tinymce.min.js',
        '.npm/package/node_modules/tinymce/plugins/link/plugin.min.js',
        '.npm/package/node_modules/tinymce/plugins/autolink/plugin.min.js',
        '.npm/package/node_modules/tinymce/plugins/table/plugin.min.js',
        '.npm/package/node_modules/tinymce/plugins/fullscreen/plugin.min.js'
    ], 'client', { bare: true });

    api.addFiles([
        'tinymce.js'
    ], 'client');

    api.addAssets([
        '.npm/package/node_modules/tinymce/themes/modern/theme.min.js',
        '.npm/package/node_modules/tinymce/skins/lightgray/skin.min.css',
        '.npm/package/node_modules/tinymce/skins/lightgray/content.min.css',
        '.npm/package/node_modules/tinymce/skins/lightgray/fonts/tinymce.eot',
        '.npm/package/node_modules/tinymce/skins/lightgray/fonts/tinymce.svg',
        '.npm/package/node_modules/tinymce/skins/lightgray/fonts/tinymce.ttf',
        '.npm/package/node_modules/tinymce/skins/lightgray/fonts/tinymce.woff',
        '.npm/package/node_modules/tinymce/skins/lightgray/fonts/tinymce-small.eot',
        '.npm/package/node_modules/tinymce/skins/lightgray/fonts/tinymce-small.svg',
        '.npm/package/node_modules/tinymce/skins/lightgray/fonts/tinymce-small.ttf',
        '.npm/package/node_modules/tinymce/skins/lightgray/fonts/tinymce-small.woff',
        '.npm/package/node_modules/tinymce/skins/lightgray/img/anchor.gif',
        '.npm/package/node_modules/tinymce/skins/lightgray/img/loader.gif',
        '.npm/package/node_modules/tinymce/skins/lightgray/img/object.gif',
        '.npm/package/node_modules/tinymce/skins/lightgray/img/trans.gif'
    ], 'client')

} );
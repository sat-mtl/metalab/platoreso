// Base config, because TinyMCE tries to load everything from where it is installed
// and in Meteor we have to play with their own asset system
tinymce.baseURL = "/packages/metalab_tinymce/.npm/package/node_modules/tinymce";
tinymce.suffix = ".min";
"use strict";

import slugify from "underscore.string/slugify";
import GroupCollection from "/imports/groups/GroupCollection";
import ProjectCollection from "/imports/projects/ProjectCollection";
import ProjectSecurity from "/imports/projects/ProjectSecurity";
import PhaseTypes from "/imports/projects/phases/PhaseTypes";
import CardCollection from "/imports/cards/CardCollection";
import CommentCollection from "/imports/comments/CommentCollection";
import { RoleList } from "/imports/accounts/Roles";
import UserCollection from "/imports/accounts/UserCollection";

//var httpsync = Npm.require('httpsync');

// Better safe than sorry, this is dangerous
if ( process.env.NODE_ENV != 'production' ) {

    PR.Faker      = {};
    PR.Faker.fake = function () {
        // CLEANUP
        Meteor.users.remove( {_id: {$ne: 't4bdDrapaMdS9DTiK'}} );
        GroupCollection.remove( {} );
        ProjectCollection.remove( {} );
        CardCollection.remove( {} );
        CommentCollection.remove( {} );

        // CONFIG
        const USER_COUNT    = 64;
        const GROUP_COUNT   = 8;
        const PROJECT_COUNT = 36;
        const CARD_COUNT    = 2000;
        const COMMENT_COUNT = 8000;

        // USERS
        let users = [];
        for ( let i = 0; i < USER_COUNT; i++ ) {
            const user = {
                profile:   {
                    firstName: faker.name.firstName(),
                    lastName:  faker.name.lastName(),
                    biography: faker.lorem.sentence(faker.random.number(300)),
                    avatarUrl: faker.image.avatar()
                },
                emails:    [
                    {
                        address:  faker.internet.email(),
                        verified: faker.random.arrayElement( [true, true, true, true, false] )
                    }
                ],
                roles:     {'__global_roles__': [faker.random.arrayElement( [
                    RoleList.admin,

                    RoleList.super,
                    RoleList.super,

                    RoleList.moderator,
                    RoleList.moderator,
                    RoleList.moderator,

                    RoleList.user,
                    RoleList.user,
                    RoleList.user,
                    RoleList.user,
                    RoleList.user,
                    RoleList.user,
                    RoleList.user,
                    RoleList.user
                ] ) ] },
                enabled:   faker.random.arrayElement( [true, true, true, true, true, false] ),
                completed: true,
                createdAt: faker.date.past( 2 ),
                updatedAt: faker.date.recent( 12 )
            };
            users.push( UserCollection.insert( user ) );

            console.log('User: ' + (i+1) + " of " + USER_COUNT);
        }

        // GROUPS
        let groups = [];
        for ( let i = 0; i < GROUP_COUNT; i++ ) {
            const group_info = {
                name:        faker.company.companyName(),
                description: faker.company.catchPhrase(),
                content:     faker.lorem.paragraphs( faker.random.number( 5 ) ),
                public:      faker.random.boolean(),
                showMembers: faker.random.boolean()
            };
            const group      = GroupCollection.insert( group_info );
            groups.push( group );
            //var image   = new FS.File( faker.image.imageUrl( 800, 800 ) );
            //image.owners = [group];
            //GroupImagesCollection.insert( image );

            console.log('Group: ' + (i+1) + " of " + GROUP_COUNT);
        }

        // PROJECTS
        let projects = [];
        for ( let i = 0; i < PROJECT_COUNT; i++ ) {
            let project_groups = [];
            for ( let i = 0; i < Math.random() * 10; i++ ) {
                project_groups.push( faker.random.arrayElement( groups ) );
            }

            let project_phases = [];
            for ( let i = 0; i < Math.random() * 10; i++ ) {
                const startDate = faker.random.boolean() ? faker.date.past() : faker.date.future();
                let phase       = {
                    name:        faker.hacker.ingverb(),
                    description: faker.hacker.phrase(),
                    content:     faker.lorem.paragraphs( faker.random.number( 5 ) ),
                    startDate:   startDate,
                    endDate:     faker.date.future( 1, startDate ),
                    type:        faker.random.arrayElement( PhaseTypes.list.map( type=>type.id ) ),
                    data:        {
                        questions: {
                            questions: function () {
                                let questions = {};
                                for ( let i = 0; i < Math.random() * 10; i++ ) {
                                    const id      = Random.id();
                                    questions[id] = {
                                        _id:    id,
                                        prompt: faker.lorem.sentence()
                                    }
                                }
                                return questions;
                            }()
                        },
                        poll:      {
                            questions: function () {
                                let questions = {};
                                for ( let i = 0; i < Math.random() * 10; i++ ) {
                                    const id      = Random.id();
                                    questions[id] = {
                                        _id:     id,
                                        prompt:  faker.lorem.sentence(),
                                        answers: function () {
                                            let answers = {};
                                            for ( let i = 0; i < Math.random() * 10; i++ ) {
                                                const id    = Random.id();
                                                answers[id] = {
                                                    _id:    id,
                                                    prompt: faker.lorem.sentence()
                                                }
                                            }
                                            return answers;
                                        }()
                                    }
                                }
                                return questions;
                            }()
                        }
                    }
                };
                project_phases.push( phase );
            }

            const project_info = {
                name:         faker.commerce.productName(),
                description:  faker.company.catchPhrase(),
                content:      faker.lorem.paragraphs( faker.random.number( 5 ) ),
                hashtag:      slugify( faker.company.bsBuzz() ),
                public:       faker.random.boolean(),
                showTimeline: faker.random.boolean(),
                groups:       _.uniq( project_groups ),
                phases:       project_phases
            };

            try {
                const project = ProjectCollection.insert( project_info );
                projects.push( { _id: project, phases: project_phases } );
                //var image   = new FS.File( faker.image.imageUrl( 800, 800 ) );
                //image.owners = [project];
                //PR.Projects.ProjectImagesCollection.insert( image );
            } catch ( e ) {
                console.error( e );
            }

            console.log('Project: ' + (i+1) + " of " + PROJECT_COUNT);
        }

        // CARDS
        let cards = [];
        for ( let i = 0; i < CARD_COUNT; i++ ) {
            const project      = faker.random.arrayElement( projects );
            let phase          = null;
            let phase_attempts = 0;
            //while ( phase == null || !phase.isActive ) {
                //console.log('    finding valid phase for card ...');
                phase = faker.random.arrayElement( project.phases );
                //if ( phase_attempts++ > 100 ) {
                //    phase = null;
                //    break;
                //}
            //}
            if ( phase == null ) {
                continue;
            }
            let author         = null;
            let author_attemps = 0;
            while ( author == null || !ProjectSecurity.canContribute( author, project._id ) ) {
                //console.log('    finding card author...');
                author = faker.random.arrayElement( users );
                if ( author_attemps++ > 100 ) {
                    author = null;
                    break;
                }
            }
            if ( author == null ) {
                continue;
            }
            let card_likes = [];
            for ( let i = 0; i < Math.random() * 500; i++ ) {
                card_likes.push( faker.random.arrayElement( users ) );
            }
            card_likes = _.uniq( card_likes );

            const card_info = {
                author:    author,
                phase:     phase._id,
                project:   project._id,
                name:      faker.random.boolean() ? faker.company.catchPhrase() : faker.company.bs(),
                content:   faker.random.boolean() ? faker.lorem.sentence(faker.random.number(100)) : null,
                viewCount: faker.random.number( 500 ),
                likes:     card_likes,
                likeCount: card_likes.length,
                data:      {
                    questions: {
                        answers: function () {
                            let answers = {};
                            _.forEach( phase.data.questions.questions, question => {
                                answers[question._id] = faker.lorem.sentence();
                            } );
                            return answers;
                        }()
                    },
                    poll:      {
                        answers: function () {
                            let answers = {};
                            _.forEach( phase.data.poll.questions, question => {
                                answers[question._id] = faker.random.arrayElement( _.keys( question.answers ) );
                            } );
                            return answers;
                        }()
                    }
                }
            };
            const card      = CardCollection.insert( card_info );
            card_info._id = card;
            cards.push( card_info );
            /*if ( faker.random.number( 100 ) >= 95 ) {
                var image   = new FS.File( faker.image.imageUrl( faker.random.number( {
                    min: 100,
                    max: 800
                } ), faker.random.number( {min: 100, max: 800} ) ) );
                image.owners = [card];
                CardImagesCollection.insert( image );
            }*/

            console.log('Card: ' + (i+1) + " of " + CARD_COUNT);
        }

        // COMMENTS
        let comments = {};
        for ( let i = 0; i < COMMENT_COUNT; i++ ) {
            const card         = faker.random.arrayElement( cards );
            let author         = null;
            let author_attemps = 0;
            while ( author == null || !ProjectSecurity.canContribute( author, card.project ) ) {
                //console.log('    finding comment author...');
                author = faker.random.arrayElement( users );
                if ( author_attemps++ > 100 ) {
                    author = null;
                    break;
                }
            }
            if ( author == null ) {
                continue;
            }

            const parent = faker.random.boolean() && comments[card._id] ? faker.random.arrayElement( comments[card._id] ) : null;
            const comment_info = {
                entity:     card._id,
                entityType: 'card',
                ancestors:  parent ? parent.ancestors.concat([parent._id]) : [],
                parent:     parent ? parent._id : null,
                author:     author,
                post:       faker.lorem.sentence(faker.random.number(50))
            };
            comment_info._id = CommentCollection.insert( comment_info );
            if ( !comments[card._id] ) {
                comments[card._id] = [];
            }
            comments[card._id].push( comment_info );

            console.log('Comment: ' + (i+1) + " of " + COMMENT_COUNT);
        }
    };
}
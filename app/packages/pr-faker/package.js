Package.describe({
    name: 'metalab:pr-faker',
    version: '0.0.1',
    debugOnly: true
});

Package.onUse( function( api ) {
    api.use([
        'metalab:pr-base',
        'metalab:pr-collections',
        'metalab:pr-images',
        'practicalmeteor:faker@3.0.1_1'
    ], 'server' );

    api.addFiles( [
        'server/fake.js'
    ], 'server' );
});


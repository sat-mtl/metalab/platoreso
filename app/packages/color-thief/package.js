Npm.depends({
    'color-thief': '2.2.1'
});

Package.describe( {
    name:          'metalab:color-thief',
    version:       '2.2.1',
    summary:       'ColorThief Wrappper',
    git:           '',
    documentation: 'README.md'
} );

Package.onUse( function ( api ) {
    api.use(['ecmascript']);
    api.mainModule('main-server.js', 'server');
} );
"use strict";

// Default logger,
// in case an environment fails to register its logger "factory"
function defaultLogger( namespace ) {
    return {
        error: console.error.bind(console, `[${namespace}]`),
        warn: console.warn.bind(console, `[${namespace}]`),
        info: console.info.bind(console, `[${namespace}]`),
        verbose: console.log.bind(console, `[${namespace}]`),
        debug: console.log.bind(console, `[${namespace}]`),
        silly: console.log.bind(console, `[${namespace}]`),
        log(level,...args){ return defaultLogger(namespace)[level](...args) }
    };
}

/**
 * Method to retrieve the current logger
 * Since this code is shared between client and server this is how we retrieve
 * the logger "factory" defined by the environment specific code
 * @type {defaultLogger}
 */
let getLogger;/* = defaultLogger; */

/**
 * Method for the environment specific code to register its
 * logger "factory"
 * @param fn
 */
export function register( fn ) {
    getLogger = fn;
}

/**
 * Loggers for each namespaces, we reuse them instead of creating new ones
 * for various reasons, memory, message processing times and automatically created
 * colors per namespace to remain the same
 */
const namespaces = {};

/**
 * Method to retrieve (or create) loggers for a namespace
 *
 * @param namespace
 * @returns {*}
 */
export default function ( namespace ) {
    if ( !getLogger ) {
        // No factory was registered, just return our default logger
        // and don't save it in the dictionary
        console.warn("Returning default logger");
        return defaultLogger(namespace);
    }

    if ( !namespaces[namespace] ) {
        // No logger for this namespace yet, create one and save it to the dictionary
        namespaces[namespace] = getLogger( namespace );
    }

    // Return dictionary logger for the namespace
    return namespaces[namespace];
}
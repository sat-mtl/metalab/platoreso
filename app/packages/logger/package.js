Npm.depends( {
    "winston": "2.1.1",
    "debug": "2.2.0"
});

Package.describe({
    name: 'metalab:logger',
    version: '0.0.1'
});

Package.onUse( function( api ) {
    api.use([
        'ecmascript'
    ] );

    api.mainModule( 'server/main.js', 'server' );
    api.mainModule( 'client/main.js', 'client' );
});


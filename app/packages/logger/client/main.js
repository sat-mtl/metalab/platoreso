"use strict";

import { register } from "../Logger";
import debug from "debug";

debug.enable( "*" );

/**
 * Returns the right function to call according to the log level
 * This was adapted from what's in debug
 *
 * @param level
 * @param args
 * @returns {boolean|*}
 */
function log( level, ...args ) {
    // this hackery is required for IE8/9, where
    // the `console.log` function doesn't have 'apply'
    return 'object' === typeof console
           && console[level]
           && Function.prototype.apply.call( console[level], console, args );
}

// Register our logger "factory"
// It will create a debug instance per namespace
// It wraps the also debug lib with log level support
register( ( namespace = null ) => {
    const d = debug( namespace || "PlatoReso" );

    /*
     * Prepare the console methods that will be used
     * They are bound here so that it doesn't have to
     * happen on every log call
     */
    const c = {
        error: log.bind( null, 'error' ),
        warn:  log.bind( null, 'warn' ),
        info:  log.bind( null, 'log' ), // I hate that console.info icon
        log:   log.bind( null, 'log' ),
        debug: log.bind( null, 'log' ) // I hate that blue debug color in chrome
    };

    /*
     * Return wrapped methods for debug logger
     * It replaces its internal log method depending on the level required
     * Debug doesn't support log levels internally so this was  done so hack
     * log level into it
     */
    return {
        error( ...args ) {
            d.log = c.error;
            return d( ...args );
        },
        warn( ...args ) {
            d.log = c.warn;
            return d( ...args );
        },
        info( ...args ) {
            d.log = c.info;
            return d( ...args );
        },
        verbose( ...args ) {
            d.log = c.log;
            return d( ...args );
        },
        debug( ...args ) {
            d.log = c.debug;
            return d( ... args );
        },
        silly( ...args ) {
            d.log = c.log;
            return d( ...args );
        },
        log( level, ...args ) {
            d.log = c[level];
            return d( ...args )
        }
    }
} );
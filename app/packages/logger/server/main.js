"use strict";

import fse from "fs-extra";
import expandHomeDir from "expand-home-dir";
import { Log } from "meteor/logging"
import { register } from "../Logger";

const Winston = Npm.require( "winston" );
const { Console: ConsoleTransport, File: FileTransport } = Winston.transports;

// Get config
const { level = 'info', path = null } = Meteor.settings.logger ? Meteor.settings.logger : {};

// We use meteor in --raw-logs mode because that's the way it'll be used in prod anyway
// So output colored text instead of json for the moment, as we are not using a logging analyser of any kind
Log.outputFormat = 'colored-text';

let defaultTransports = [];
let exceptionTransports = [

    new ConsoleTransport( {
        label:                           "PlatoReso",
        colorize:                        true,
        prettyPrint:                     true,
        timestamp:                       true,
        handleExceptions:                true,
        humanReadableUnhandledException: true
    } )

];

// If we have a path for the logs, setup a global file log that will be shared by all loggers
if ( path ) {

    /* check if folder logs exist */
    var fs = require( 'fs' );

    const fullPath = expandHomeDir( path );

    if ( !fs.existsSync( fullPath ) ) {
        // create folder
        fse.mkdirsSync( fullPath );
    }

    const errorLog = new FileTransport( {
        name:                            'error-log',
        filename:                        fullPath + 'platoreso-error.log',
        level:                           'error',
        tailable:                        true,
        zippedArchive:                   true,
        maxsize:                         1024 * 1024 * 10, // 10MB
        maxFiles:                        10
    } );

    defaultTransports = [
        errorLog,
        new FileTransport( {
            name:          'log',
            filename:      fullPath + 'platoreso.log',
            level:         level,
            colorize:      false,
            tailable:      true,
            zippedArchive: true,
            maxsize:       1024 * 1024 * 10, // 10MB
            maxFiles:      1
        } )
    ];

    exceptionTransports.push(errorLog);
}


// Default logger (for exception handling)
Winston.handleExceptions( exceptionTransports );

// Register our logger "factory"
// This will create a console logger per namespace
// And should shared the file logger defined earlier
register( ( namespace = null ) => {
    return new Winston.Logger( {
        level:       level,
        exitOnError: false,
        transports:  defaultTransports.concat( [
            new ConsoleTransport( {
                label:                           namespace || "PlatoReso",
                colorize:                        true,
                prettyPrint:                     true,
                timestamp:                       true
            } )
        ] )
    } );
} );

MODALITÉS ET CONDITIONS D’UTILISATION
======================================

## platoreso.sat.qc.ca

__Version : 1.0__

__Dernière mise à jour : 23 septembre 2016__

Nous vous remercions de visiter la plateforme accessible à platoreso.sat.qc.ca (y compris les pages et sous-pages dont l’hyperlien se termine par « __platoreso.sat.qc.ca__ ») (collectivement la « __Plateforme__ ») exploitée par la S.A.T. Société des arts technologiques (« __SAT__ », « __nous__ » ou « __notre__ »).

La Plateforme permet de faciliter le processus de co-création en structurant les idées et les concepts pour passer d’une problématique à sa solution. Les utilisateurs peuvent utiliser la Plateforme pour soumettre et contribuer leurs idées à divers projets et en suivre l’évolution. Les partenaires et/ou clients de la SAT qui ont conclu un contrat distinct verbal ou écrit avec la SAT pour utiliser la Plateforme pour les besoins de leurs projets et/ou programmes ou pour monitorer un projet existant de la SAT (les « __Partenaires__ ») peuvent quant à eux mettre en œuvre de concert avec l’Utilisateur et/ou soutenir ou s’associer aux idées/projets qui font l’objet d’échanges sur la Plateforme ou s’associer à ces idées/projets.

Aux fins des présentes modalités et conditions d’utilisation (les « __Conditions d’utilisation__ »), toute référence à la SAT est réputée comprendre ses compagnies affiliées et liées, le cas échéant, ainsi que leurs dirigeants, administrateurs, employés, agents, autres représentants respectifs et leurs successeurs ou ayant droits respectifs.

Les présentes Conditions d’utilisation s’appliquent à tout utilisateur de la Plateforme, y compris les utilisateurs autorisés par les Partenaires, et ce, peu importe que son rôle au sein de la Plateforme en soit un de participant, de modérateur, d'expert ou autre (individuellement, l’« __Utilisateur__ », « __vous__ », « __votre__ » et collectivement, les « __Utilisateurs__ »). Veuillez lire attentivement les présentes Conditions d’utilisation, et ce, avant d’utiliser la Plateforme. En accédant et en utilisant la Plateforme, vous acceptez d’être lié par toutes les Conditions d’utilisation y compris la [Politique de protection de la vie privée](https://platoreso.sat.qc.ca/legal/privacy-policy) ainsi que toutes les autres conditions qui pourraient vous être communiquées de temps à autre par la SAT, entre autres lorsque vous utiliserez pour la première fois certains paramètres ou fonctionnalités de la Plateforme. En cas de contradiction entre les présentes Conditions d’utilisation et toutes autres conditions qui pourraient vous être communiquées par la SAT, les présentes Conditions d’utilisation auront préséance.

### 1. ACCEPTATION DES CONDITIONS D’UTILISATION

#### 1.1. Un contrat.
En vous inscrivant, en visitant ou en utilisant la présente Plateforme, vous, l’Utilisateur, concluez, en votre nom personnel ou à titre de représentant dûment autorisé d’une organisation (y compris, mais sans y être limité, un Partenaire), un contrat ayant force obligatoire avec la SAT. Ce contrat comprend les présentes Conditions d’utilisation, la [Politique de protection de la vie privée](https://platoreso.sat.qc.ca/legal/privacy-policy) ainsi que toutes les autres conditions d’utilisation qui pourraient vous être communiquées de temps à autre par la SAT.

#### 1.2. Renseignement.
Dans le contexte de l’utilisation que vous faites de cette Plateforme, vous acceptez de fournir des renseignements véridiques, actuels et complets comme, entre autres, dans le cadre de vos demandes ou de tout formulaire d’inscription vers lequel la Plateforme pourrait vous diriger. Afin de profiter au maximum des fonctionnalités de la Plateforme, vous avez également l’obligation de mettre à jour les renseignements et les informations associés à votre Compte dès qu’un changement survient.

#### 1.3. Modifications des Conditions d’utilisation.
Nous pouvons décider, à notre entière discrétion, d’ajouter, de retirer, de modifier, de suspendre ou d’autrement changer toute partie des présentes Conditions d’utilisation, en totalité ou en partie, à tout moment. Les modifications prendront effet une fois qu’avis est donné sur cette Plateforme. À chaque fois que vous accédez à la Plateforme, vous avez l’obligation de vérifier les Conditions d’utilisation afin de voir si elles ont été mises à jour en vous référant à la version et à la date de « Dernière mise à jour » indiquées au haut du présent document. Si vous n’êtes pas en accord avec les modifications effectuées aux Conditions d’utilisation, vous devez immédiatement cesser l’utilisation de la Plateforme.

### 2. TYPES DE COMPTES

Aux fins des présentes, les termes « __Compte__ » ou « __Comptes__ » désignent individuellement ou collectivement tous les « Comptes participant », « Comptes modérateur » et les « Comptes expert ».

#### 2.1. Compte participant.
Pour accéder à la Plateforme à titre de participant, et y contribuer du Contenu Utilisateur, vous devez remplir le formulaire d’inscription en ligne en donnant, vos noms, prénoms et adresse courriel ou en vous authentifiant par le biais d’une plateforme de médias sociaux intégrée à la Plateforme.

#### 2.2. Compte expert ou compte modérateur.
Si vous agissez pour le compte d’un Partenaire, et que vous désirez obtenir un « Compte expert » ou un « Compte modérateur » pour utiliser la Plateforme, nous vous demandons de contacter la SAT par courriel au [info@sat.qc.ca](mailto:info@sat.qc.ca). La SAT communiquera avec vous afin de valider vos informations, évaluer votre projet ou programme et vous informer des coûts et modalités spécifiques à votre utilisation de la Plateforme à titre de Partenaire. La SAT se réserve le droit d’approuver ou rejeter votre demande à sa seule et entière discrétion. Si vous agissez pour le compte d’un Partenaire, et que vous désirez obtenir un « Compte modérateur » pour modérer la Plateforme, nous vous demandons de contacter le détenteur du Compte expert chez le Partenaire associé au programme ou projet. À titre de Partenaire, les dispositions de l’article 9 des présentes Conditions d’utilisation s’appliquent spécifiquement à vous.

#### 2.3. Responsabilité associée au Compte.
Vous êtes responsable de toute utilisation de votre Compte y compris, de la confidentialité de votre nom d’utilisateur et mot de passe. Nous ne serons en aucun cas responsables d’un usage non autorisé de votre Compte.

#### 2.4. Notification d’une utilisation non autorisée.
Vous convenez de nous aviser immédiatement de toute utilisation non autorisée de votre Compte ou de toute autre violation de sécurité. La SAT, à son entière discrétion, prendra alors les mesures qu’elle juge appropriées et raisonnables pour faire cesser cette utilisation ou violation, sans toutefois encourir une quelconque responsabilité à cet égard.

#### 2.5. Renseignements personnels et envoi de notifications.
À titre de détenteur d’un Compte, vous acceptez que nous utilisions vos renseignements personnels conformément à la [Politique de protection de la vie privée](https://platoreso.sat.qc.ca/legal/privacy-policy). Si les coordonnées que vous nous avez fournies ne sont pas à jour, certaines notifications sont susceptibles de ne pas vous rejoindre. Pour contrôler et limiter les types de messages que vous recevez de notre part, veuillez consulter la section « Notifications » sous la rubrique « Modifier le profil utilisateur » de votre Compte ou suivre les instructions figurant dans chacun de nos courriels.

### 3. UTILISATION DE LA PLATEFORME ET DU CONTENU MIS À LA DISPOSITION DE L’UTILISATEUR

#### 3.1. Propriété.
La Plateforme est la propriété de la SAT. À l’exception du Contenu Utilisateur (comme cette expression est définie ci-dessous), le reste du contenu de la Plateforme, incluant les données, les graphiques, les photographies, les images, les données audio, les données vidéo, les logiciels, les marques de commerce, les logos, les noms commerciaux, le texte et toutes les autres informations fournies par la SAT et incluses dans cette Plateforme, est la propriété de la SAT, ses compagnies liées et/ou de leurs tiers concédants de licences respectifs (collectivement le « __Contenu de la SAT__ »). Le Contenu de la SAT est protégé par les lois canadiennes et internationales en matière de propriété intellectuelle.

#### 3.2. Reproduction.
En utilisant cette Plateforme, vous acceptez de vous conformer aux lois canadiennes et internationales en matière de propriété intellectuelle et de prévenir toute copie non autorisée du Contenu de la SAT. Sauf disposition contraire aux présentes, la SAT ne vous accorde aucun droit d’utiliser ses brevets, ses marques de commerce, ses droits d’auteurs ou ses droits de propriété intellectuelle et vous ne pouvez pas utiliser, modifier, copier, reproduire, publier, afficher, transmettre, distribuer, exécuter, créer des œuvres  dérivées, transférer ou vendre, une partie ou l’ensemble du Contenu de la SAT sans l’autorisation expresse écrite de la SAT. Pour obtenir un consentement par écrit, veuillez communiquer avec la SAT à l'adresse courriel suivante : [pdube@sat.qc.ca](mailto:pdube@sat.qc.ca).

#### 3.3. Utilisation de la Plateforme.
Si vous vous conformez de façon continue aux Conditions d’utilisation, la SAT vous accorde le droit non exclusif, non transférable et limité d’accéder, d’utiliser, d’afficher et de visionner la Plateforme ainsi que le Contenu de la SAT et le Contenu Utilisateur qui y figurent. Vous acceptez de ne pas interrompre ou de ne pas tenter d’interrompre le fonctionnement de la Plateforme d’aucune manière. Vous acceptez de ne pas vous opposer aux droits de propriété de la SAT ou à la validité de ses droits à l’égard du contenu de cette Plateforme. Vous avez le droit de consulter, télécharger, imprimer et reproduire le contenu de la Plateforme pour votre usage personnel uniquement. Cette autorisation est seulement valide à condition que vous : (i) conserviez chacun et tous les droits d'auteur ou autres avis de propriété qui s’y trouvent; (ii) ne modifiez pas le contenu d’aucune manière; (iii) ne reproduisiez pas le contenu, ne l’affichiez, ne l’exécutiez, ne le distribuiez ou ne l’utilisiez publiquement à des fins commerciales. Toutes les licences qui vous sont accordées en lien avec la Plateforme, le Contenu de la SAT et le Contenu Utilisateur seront immédiatement révoquées si vous violez les termes prévus aux Conditions d’utilisation, y compris les restrictions des dispositions précédentes. Après une telle révocation, vous acceptez de cesser d’utiliser immédiatement la Plateforme et son contenu.

### 4. CONTENU UTILISATEUR

#### 4.1. Contenu Utilisateur.
Aux fins des présentes Conditions d’utilisation, « __Contenu Utilisateur__ » signifie tout matériel, élément ou information soumis ou publié par les Utilisateurs sur la Plateforme, y compris par les Partenaires, notamment les idées, concepts, textes, fichiers, œuvres d’art, photographies, listes personnelles, messages, commentaires, critiques, notes, matériel audio, images ou vidéos.

#### 4.2. Accessibilité et utilisation du Contenu Utilisateur.
En soumettant du Contenu Utilisateur sur la Plateforme, vous reconnaissez que votre Contenu Utilisateur sera automatiquement publié et accessible aux autres Utilisateurs sur la Plateforme. Vous reconnaissez également que le Contenu Utilisateur que vous pourriez contribuer à la Plateforme est principalement constitué d’idées et que celles-ci ne bénéficient pas de protection en vertu des lois canadiennes et internationales en matière de propriété intellectuelle. Vous acceptez et permettez que la SAT utilise ce Contenu Utilisateur en collaboration avec vous. Vous acceptez et permettez également aux autres Utilisateurs d’utiliser ce Contenu Utilisateur à leur discrétion. Ainsi, la SAT ou les autres Utilisateurs pourraient utiliser vos idées pour les mettre en œuvre, sans devoir vous impliquer ou vous verser une compensation. Dans la mesure où votre Contenu Utilisateur bénéficie d’une protection quelconque de par la loi, vous octroyez par les présentes à la SAT une licence irrévocable, mondiale, non exclusive, transférable, perpétuelle et gratuite, relativement au Contenu Utilisateur, qui permet d’exécuter, communiquer, reproduire, distribuer, publier, afficher, héberger, modifier, traduire, copier, accorder une sous-licence connexe ou autrement utiliser et exploiter votre Contenu Utilisateur et pour exercer tout autre droit exclusif associé à un tel Contenu Utilisateur que vous fournissez par l’intermédiaire de la Plateforme, sur tout type de médium et dans tout média, connu maintenant ou créé dans le futur sans autre autorisation, notification et/ou compensation financière envers vous ou d’autres tiers (collectivement, les « __Droits acquis__ »). Les Droits Acquis peuvent être exercés par la SAT ainsi que leurs cessionnaires ou licenciés y compris, mais sans y être limité, les Partenaires. Nous ne contrôlons pas ce que les autres Utilisateurs peuvent faire avec votre Contenu Utilisateur et nous n’acceptons aucune responsabilité à cet égard.

#### 4.3. Liens et autres sites web intégrés à la Plateforme.
Certains liens et sites web sur cette Plateforme peuvent vous mener à d’autres applications ou sites web appartenant à des tiers, y compris aux Partenaires, incluant des sites de réseaux sociaux détenus et exploités par des tiers (collectivement, les « __Applications de tiers__ »). Si vous utilisez ces Applications de tiers, vous quittez cette Plateforme. Pas plus que nous révisons le Contenu Utilisateur affiché sur notre Plateforme, nous révisons encore moins ce qui est fait de votre Contenu Utilisateur sur ces Applications de tiers ou le contenu même de ces Applications de tiers. Ainsi, nous n’assumons aucune responsabilité pour tout contenu, publicité, produit, service ou autre matériel rendu disponible sur ces Applications de tiers. Si vous décidez d’utiliser ou de visiter une Application de tiers, vous en assumez l’entière responsabilité et il vous incombe de prendre toutes les précautions nécessaires. Ces Applications de tiers sont régies par des conditions différentes de celles applicables à notre Plateforme. Par conséquent, nous vous encourageons à les consulter.

####  4.4. Forums.
La Plateforme vous donne l'occasion de partager votre Contenu Utilisateur par l'entremise de forums de discussion ou de babillards (les « __Forums__ »). Vous êtes le seul responsable du Contenu Utilisateur que vous diffusez par l’entremise de ces Forums ainsi que des conséquences de ces messages. Les informations et le contenu que vous partagez ou publiez peuvent être vus par d’autres Utilisateurs de la Plateforme.

#### 4.5. Projets.
Il est de votre responsabilité de faire les vérifications nécessaires concernant chaque projet d’un Utilisateur, y compris d’un Partenaire, à l’égard duquel vous soumettez du Contenu Utilisateur. Notamment, nous ne sommes pas responsables des projets des Utilisateurs, y compris des Partenaires, qui ne respectent pas les lois applicables. La SAT ne garantit en aucun cas la faisabilité ou la validité des projets disponibles sur la Plateforme.

#### 4.6. Stockage et destruction des données.
Vous reconnaissez que nous n’avons aucune obligation de maintenir votre Compte ou de stocker, conserver ou de vous fournir une copie des données et du Contenu Utilisateur que vous ou d’autres Utilisateurs nous auront transmis. Sans limiter ce qui précède, vous acceptez que la SAT et ses Partenaires aient le contrôle entier sur votre Compte et sur les données associées à celui-ci (y compris, mais sans y être limité, votre Contenu Utilisateur), et qu’ils puissent conserver ou mettre fin à votre accès à ce Compte ou détruire les données qui y sont associées à leur discrétion, à tout moment et sans préavis. Sans limité ce qui précède, dans la plupart des cas, les Partenaires utilisent la Plateforme pour une période de temps spécifique reliée à un projet et/ou programme, et à la fin de cette période, votre accès à votre Compte et à tout Contenu Utilisateur cessera immédiatement et vous n’aurez aucune possibilité de récupérer les données qui y sont associées. Nonobstant ce qui précède, dans la mesure où vos renseignements personnels sont conservés par la SAT ou ses Partenaires après que l’accès à votre Compte soit terminé, vous pourrez continuer d’avoir accès à ces renseignements personnels conformément à la [Politique de protection de la vie privée](https://platoreso.sat.qc.ca/legal/privacy-policy).

### 5. REPRÉSENTATIONS/GARANTIES

#### 5.1. Représentations et garanties quant à votre Contenu Utilisateur.
Pour pouvoir utiliser cette Plateforme, vous acceptez, représentez et garantissez que:

- vous fournirez uniquement du Contenu Utilisateur qui respecte toutes les lois et tous les règlements fédéraux, provinciaux, territoriaux et autres, ainsi que les droits appartenant à des tiers y compris les droits de propriété intellectuelle ;
- les renseignements et le Contenu Utilisateur que vous soumettez sont exacts, à jour, exempts d’erreurs et complets ; nous nous dégageons de toute responsabilité quant au Contenu Utilisateur, et ce, parce que nous ne vérifions pas, ne relisons pas et n’approuvons pas le Contenu Utilisateur disponible sur notre Plateforme ; ainsi, nous n’assumons aucune responsabilité pour les dommages encourus du fait de leur utilisation ou de la confiance que vous leur avez accordée ; et
- vous n’utiliserez pas, ne téléchargerez pas, ne transmettrez pas de quelconque façon du Contenu Utilisateur qui ne respecte pas le Code de Conduite de l’article 6 ci-dessous.

### 6. CODE DE CONDUITE

Toutes les obligations présentées dans le présent article seront collectivement désignées comme étant le « __Code de conduite__ ».

#### 6.1. Respect de la loi.
En créant un Compte, vous êtes tenu de respecter toutes les lois applicables y compris, mais sans s’y limiter, les lois relatives aux droits de propriété intellectuelle et les lois en matière de protection à la vie privée et de protection de renseignements personnels.

#### 6.2. Usurpation d’identité.
Il est interdit de créer une fausse identité. Vous ne devez pas utiliser ou tenter d’utiliser le compte d’une autre personne. Vous acceptez d’utiliser votre vrai prénom et nom en association avec votre Compte. Si nous estimons que vous avez créé un Compte qui usurpe l’identité d’une autre personne, nous pouvons à tout moment, à notre entière discrétion, avec ou sans préavis, désactiver votre Compte.

#### 6.3. Règles générales.

##### 6.3.1.
Toute activité sur notre Plateforme et tout Contenu Utilisateur doivent être le résultat de votre propre travail et ne doivent pas être copiés d’une œuvre préexistante. Dans l’éventualité où votre Contenu Utilisateur implique le travail de tierce(s) partie(s), c’est le cas notamment, mais sans s’y limiter, d’une œuvre collaborative ou d’un travail d’équipe, vous êtes responsable d’obtenir toutes les autorisations nécessaires. De plus, toutes vos activités sur notre Plateforme et toute composante de votre Contenu Utilisateur ne doit contenir aucun élément qui, selon l’opinion de la SAT : (i) enfreint, détourne ou viole un brevet, un droit d’auteur, une marque de commerce, un secret commercial, les droits moraux ou tout autre droit de propriété intellectuelle, ou les droits de publicité ou de protection de la vie privée appartenant à un tiers; (ii) viole ou encourage tout comportement qui violerait toute loi ou tout règlement applicable ou qui entraînerait une responsabilité civile ou légale; (iii) est frauduleux, faux, trompeur ou mensonger; (iv) est violent, choquant, sensationnel, irrespectueux, diffamatoire, obscène, pornographique, vulgaire ou offensif; (v) favorise ou encourage la discrimination, le fanatisme, le racisme, la haine, le harcèlement, ou la violence contre un individu ou un groupe (qui comprend, sans s’y restreindre, ce qui suit : la race, le sexe, la croyance, l’origine ethnique, l’appartenance religieuse, le statut familial, l’orientation sexuelle, l’identité sexuelle, ou la langue); (vi) est violent ou qui menace ou favorise la violence ou des actions menaçantes envers toute autre personne; (vii) encourage des activités ou l’utilisation de substances illégales ou néfastes; (viii) renferme de l’information ou du contenu affichant les noms, les images, toute ressemblance, des voix ou autre facteur d’identification d’une tierce personne (vivante ou morte), comme des noms personnels, des adresses électroniques ou des adresses postales, incluant sans s’y limiter, ceux de célébrités, ou autres personnalités publiques ou des particuliers, vivants ou morts; (ix) utilise des marques, des logos, des noms de commerce ou des indicateurs de source appartenant à des tierces parties; ou (x) vise les mineurs ou favorise les produits ou services interdits aux mineurs; ou (ix) autrement met à la disposition toute publicité non sollicitée ou non autorisée, présentation commerciale, matériel promotionnel, courrier poubelle, pourriel, opération pyramidale ou toute autre forme de sollicitation interdite.

##### 6.3.2.
Vous ne devez pas utiliser sur la Plateforme toute forme de logiciel destructeur comme un virus, un ver informatique, un cheval de Troie, une bombe à retardement, un robot d’annulation de message, ou tout autre élément nuisible, fichier, programme ou code informatique conçu pour interrompre, détruire ou limiter le fonctionnement de logiciels, de matériel informatique ou d’équipement de télécommunication. Vous ne devez pas non plus recueillir ou autrement vous procurer ou stocker toute information (incluant l'information permettant l'identification personnelle) au sujet des autres Utilisateurs de la Plateforme, y compris les adresses électroniques sans le consentement exprès de ces Utilisateurs. Vous ne devez pas tenter d’avoir accès sans autorisation à la Plateforme, y compris à toute portion de la Plateforme pour laquelle un accès ne vous a pas été octroyé par la SAT ou par un Partenaire autorisé, le cas échéant, à d'autres systèmes ou réseaux d'ordinateurs reliés à la Plateforme, par extraction de mot de passe ou autres moyens; gêner ou perturber les réseaux ou serveurs reliés à cette Plateforme ou enfreindre les règlements, politiques ou procédures de ces réseaux; et utiliser, télécharger ou copier autrement, ou fournir à une personne ou une entité le répertoire des Utilisateurs ou d'autres renseignements sur les Utilisateurs, ou données sur l’utilisation ou une partie de ceux-ci.

### 7. PLAINTES RELATIVES AU CONTENU UTILISATEUR

Si vous désirez nous faire parvenir vos commentaires relativement au Contenu Utilisateur qui se retrouve sur la Plateforme, veuillez nous contacter au courriel suivant : [info@sat.qc.ca](mailto:info@sat.qc.ca). La SAT, à son entière discrétion, prendra alors les mesures qu’elle juge appropriées et raisonnables pour donner suite à cette plainte, sans toutefois encourir une quelconque responsabilité à cet égard.

### 8. DROITS DE RETRAIT DU CONTENU UTILISATEUR ET DE FERMETURE DE COMPTE

#### 8.1.
Vous comprenez que la SAT peut, sans toutefois en avoir l’obligation, passer en revue le Contenu Utilisateur soumis et que la SAT se réserve le droit, à son entière discrétion, d’immédiatement et sans aucun avis, retirer partiellement ou totalement du Contenu Utilisateur et aussi de fermer un Compte si les présentes Conditions d’utilisation ne sont pas respectées.

### 9. UTILISATION DE LA PLATEFORME PAR LES PARTENAIRES

Les dispositions du présent article 9 s’appliquent uniquement aux Partenaires et aux Comptes qui sont créés par des Utilisateurs autorisés de ces Partenaires.

#### 9.1.
À titre d’Utilisateur autorisé pour un Partenaire, des droits de contrôle du Contenu Utilisateur pourraient être associés à votre Compte. Vous devez exercer ces droits de contrôle de manière raisonnable, en conformité avec les lois applicables et les présentes Conditions d’utilisation (y compris, mais sans y être limité le Code de conduite) et vous êtes seul responsable de tout dommage qui pourrait découler de l’exercice (ou de l’absence d’exercice) de ces droits de contrôle.

#### 9.2.
À titre de Partenaire, vous bénéficiez de droits plus étendus à l’égard du Contenu Utilisateur que les droits généraux à l’égard du Contenu Utilisateur qui sont octroyés en vertu de l’article 3.3 ci-dessus. Si vous vous conformez de façon continue aux Conditions d’utilisation, la SAT vous octroie une licence irrévocable, mondiale, non exclusive, transférable, perpétuelle et gratuite, relativement au Contenu Utilisateur, qui permet d’exécuter, communiquer, reproduire, distribuer, publier, afficher, héberger, modifier, traduire, copier, accorder une sous-licence connexe ou autrement utiliser et exploiter le Contenu Utilisateur et pour exercer tout autre droit exclusif associé à un tel Contenu Utilisateur rendu disponible par l’intermédiaire de la Plateforme, sur tout type de médium et dans tout média, connu maintenant ou créé dans le futur.

#### 9.3.
Vous acceptez d’utiliser les renseignements personnels qui vous seront communiqués par rapport aux autres Utilisateurs en conformité avec les lois applicables y compris, mais sans y être limité, les lois relatives aux renseignements personnels. Vous devez divulguer aux Utilisateurs vos pratiques en matière de gestion des renseignements personnels. La SAT n’assumera aucune responsabilité à l’égard de vos pratiques en matière de gestion des renseignements personnels et vous acceptez d’assumer toute responsabilité à l’égard de votre gestion des renseignements personnels qui vous seront communiqués.

### 10. PLATEFORME ET ACCESSIBILITÉ DES SERVICES

#### 10.1.
Nous pouvons mettre fin à tout aspect de cette Plateforme ou le modifier, le suspendre ou l’interrompre, y compris; (i) modifier la disponibilité de l’une ou l’autre de ses caractéristiques, en tout temps, sans avis ni responsabilité; (ii) imposer des limites à l’endroit de certaines fonctions et de certains Services ou limiter votre accès à des parties ou à la totalité de cette Plateforme sans avis ni responsabilité, pour quelque raison que ce soit. Nous nous réservons le droit, sans y être tenus, à notre entière discrétion, de corriger toute erreur ou omission comprise dans toute partie de cette Plateforme, à tout moment, sans avis.

### 11. EXONÉRATION, LIMITATION DE RESPONSABILITÉ ET D’INDEMNISATION

#### 11.1. Clause de non-responsabilité.
Vous reconnaissez que vous utilisez cette Plateforme ou que vous vous y fiez à vos propres risques. Dans la mesure autorisée par la loi, nous n’offrons aucune garantie de quelque nature que ce soit concernant la Plateforme, le tout vous étant fourni « TEL QUEL ». NOUS REJETONS EXPRESSÉMENT LA TOTALITÉ DES DÉCLARATIONS, GARANTIES ET CONDITIONS, Y COMPRIS, NOTAMMENT, LES GARANTIES ET CONDITIONS IMPLICITES DE QUALITÉ MARCHANDE, D’ABSENCE DE CONTREFAÇON ET D’APTITUDE À UN USAGE PARTICULIER, AINSI QUE CELLES DÉCOULANT AUTREMENT D’UNE LOI APPLICABLE OU AUTREMENT EN VERTU DU MARCHÉ OU DU COURS NORMAL DES AFFAIRES. NOUS NE GARANTISSONS PAS QUE CETTE PLATEFORME CONTINUERA DE FONCTIONNER, QU’ELLE FONCTIONNERA SANS INTERRUPTION OU QU’ELLE SERA EXEMPTE D’ERREUR. Étant donné que nous ne révisons pas ce qui est publié sur la Plateforme, nous n’exerçons pas de contrôle sur la qualité, la sécurité, la fiabilité ou la légalité du Contenu Utilisateur qui s’y retrouve, et vous nous dégagez de toute responsabilité à cet égard. De plus, nous ne pouvons être tenus responsables parce que votre Contenu Utilisateur n’a pas été diffusé ou a été diffusé en retard sur la Plateforme. Vous assumez tous les risques reliés au fait de traiter avec d’autres Utilisateurs avec lesquels vous entrez en contact par l’entremise de la Plateforme. Vous reconnaissez que nos Partenaires ont certains droits de contrôle par rapport à votre Contenu Utilisateur, et vous reconnaissez de plus que nous ne sommes pas responsables des activités exercées par ces Partenaires à partir d’un Compte modérateur ou encore de l’absence de services de modération. Nous ne pouvons être tenus responsables des activités ou des décisions prises, pour quelque raison que ce soit, par un Partenaire qui utilise la Plateforme.

#### 11.2. RESPONSABILITÉ LIMITÉE.
DANS LA MESURE AUTORISÉE PAR LA LOI, VOUS CONVENEZ QUE LA SAT N’EST PAS RESPONSABLE DE TOUTE PERTE OU DE TOUT DOMMAGE QUEL QU’IL SOIT, Y COMPRIS, MAIS SANS Y ÊTRE LIMITÉ, DES DOMMAGES DIRECTS OU PUNITIFS OU D’AUTRES TYPES DE DOMMAGES (Y COMPRIS, NOTAMMENT, DES DOMMAGES POUR PRÉJUDICE COMMERCIAL, PERTE D’INFORMATION, DE PROGRAMMES OU DE DONNÉES, PERTE DE PROFITS ET AUTRES PERTES ÉCONOMIQUES) QUI DÉCOULENT DE L’UTILISATION DE CETTE PLATEFORME, DE TOUT CONTENU UTILISATEUR, D’UNE APPLICATION DE TIERS, OU QUI DÉCOULENT DE L’INCAPACITÉ D’UTILISER CETTE PLATEFORME OU D’Y AVOIR ACCÈS, QUE VOUS AYEZ ÉTÉ AVISÉ OU NON DE LA POSSIBILITÉ DE TEL DOMMAGE OU D’UNE TELLE PERTE, OU QU’UN TEL DOMMAGE OU UNE TELLE PERTE SOIT RAISONNABLEMENT PRÉVISIBLE. DANS TOUS LES CAS, SI CES LIMITES DE RESPONSABILITÉ NE SONT PAS APPLICABLES, VOUS RECONNAISSEZ QUE LA RESPONSABILITÉ TOTALE ET CUMULATIVE DE LA SAT À L’ÉGARD DE TELS DOMMAGES SE LIMITE À UN MONTANT DE 100 $ CAN.

#### 11.3. INDEMNISATION.
Vous convenez par la présente de prendre fait et cause pour la SAT advenant le cas où une poursuite judiciaire serait intentée contre nous en lien avec le Contenu Utilisateur que vous avez contribué à la Plateforme ou suite à une violation des présentes Conditions d’utilisation par vous, y compris de la [Politique de protection de la vie privée](https://platoreso.sat.qc.ca/legal/privacy-policy).

### 12. RÉSILIATION

#### 12.1. Résiliation du contrat.
Les présentes Conditions d’utilisation produisent leurs effets pendant toute la durée de votre utilisation de la Plateforme. Les parties peuvent mettre fin au présent contrat à tout moment. Dans le cas d’un Utilisateur, le présent contrat prend fin lorsqu’il ferme volontairement son Compte ou que son Compte est fermé par la SAT ou un Partenaire de façon définitive. Dès la résiliation, vous perdez le droit d’accès ou d’utilisation de la Plateforme, de votre Compte et du Contenu Utilisateur que vous avez contribué. Les stipulations suivantes survivent à la résiliation : toutes les dispositions concernant la propriété intellectuelle, les restrictions relatives à l’utilisation du Contenu Utilisateur, les limites de garantie, les limites de responsabilité, l’indemnité et les dispositions de nature interprétative figurant aux présentes. Dans le cas des Partenaires, toute résiliation des présentes Conditions d’utilisation n’aura aucun effet sur toute obligation de paiement envers la SAT ou autre entente entre la SAT et le Partenaire.

### 13.  MARQUES DE COMMERCE

La Plateforme contient des marques de commerce qui sont la propriété de la SAT, de ses Partenaires ou de ses fournisseurs tiers. Les marques de commerce qui sont la propriété de tiers sont utilisées sur la Plateforme en vertu d’une licence qui accorde des droits à la SAT et non à vous. Vous n’obtenez aucune licence d’utiliser les marques de commerce figurant sur la Plateforme et toute utilisation de la sorte doit faire l’objet d’une approbation préalable par la SAT, ses Partenaires ou ses fournisseurs tiers, selon le cas.

### 14. CESSION

#### 14.1. Cession.
La SAT peut céder ses droits et obligations en vertu du présent contrat sans le consentement des Utilisateurs : (i) à une société affiliée; ou (ii) dans le cadre d’une fusion ou de la vente de tout ou partie substantielle de son entreprise ou de ses actifs; à condition que (i) dans le cas d’une cession à une société affiliée, cette dernière demeure responsable en vertu des présentes; et que (ii) la partie cessionnaire soit libérée de ses obligations en vertu des présentes sur une base prospective, une telle cession opérant novation. Les Utilisateurs ne peuvent céder leurs droits et obligations aux présentes, en tout ou en partie, sans l’autorisation écrite de la SAT.

### 15.  FORCE MAJEURE

#### 15.1. Force majeure.
La SAT ne sera pas tenue responsable de tout dommage ou perte dans l’éventualité où elle serait empêchée d’exécuter toute obligation qui lui incombe aux termes du présent contrat en raison d’un cas de force majeure.

### 16. INTÉGRALITÉ DE LA CONVENTION

#### 16.1. Intégralité de la Convention.
Ce contrat renferme l’entente intégrale entre les parties quant à son objet et elle remplace toute convention, déclaration, garantie ou entente antérieure, qu’elle soit verbale ou écrite, expresse ou implicite, à cet égard.

### 17. DROIT APPLICABLE

#### 17.1. Interprétation.
Si une ou plusieurs clauses du présent contrat sont déclarées inexécutables par un tribunal d’autorité compétente, vous et nous acceptons de supprimer cette partie non exécutoire et de mettre en œuvre le restant du contrat. Si nous omettons d’agir suite à une violation du présent contrat, cela ne signifie pas que nous renonçons à faire valoir nos droits en vertu du présent contrat.

#### 17.2. Le droit applicable.
Le présent contrat est régi par les lois applicables dans la province de Québec, au Canada. Vous reconnaissez par les présentes que tout litige découlant de, ou lié à l’utilisation de la Plateforme sera soumis à la compétence des tribunaux du district de Montréal, province de Québec.

"use strict";

//  #### ##     ## ########   #######  ########  ########  ######
//   ##  ###   ### ##     ## ##     ## ##     ##    ##    ##    ##
//   ##  #### #### ##     ## ##     ## ##     ##    ##    ##
//   ##  ## ### ## ########  ##     ## ########     ##     ######
//   ##  ##     ## ##        ##     ## ##   ##      ##          ##
//   ##  ##     ## ##        ##     ## ##    ##     ##    ##    ##
//  #### ##     ## ##         #######  ##     ##    ##     ######

// General
import "/imports/settings/client/main";
import "/imports/i18n/client/main";
import "/imports/accounts/client/main";
import "/imports/notifications/client/main";
import "/imports/messages/client/main";

//   ######  ########    ###    ########  ######## ##     ## ########
//  ##    ##    ##      ## ##   ##     ##    ##    ##     ## ##     ##
//  ##          ##     ##   ##  ##     ##    ##    ##     ## ##     ##
//   ######     ##    ##     ## ########     ##    ##     ## ########
//        ##    ##    ######### ##   ##      ##    ##     ## ##
//  ##    ##    ##    ##     ## ##    ##     ##    ##     ## ##
//   ######     ##    ##     ## ##     ##    ##     #######  ##

import logger from "meteor/metalab:logger/Logger";
import ReactDOM from "react-dom";

import Router from './Router';

Meteor.startup( ()=> {
    const log = logger('Renderer');

    log.info( 'Starting up' );

    let lastRenderedLanguage = null;

    // Re-render when language changes
    Tracker.autorun( () => {

        // Track language loading in order to re-render
        const lang = Session.get( 'i18n.language' );
        if ( lang == null ) {
            log.verbose( 'Skipping render, language is null' );
            return;
        }
        if ( lang == lastRenderedLanguage ) {
            log.verbose( 'Skipping render, language was already rendered' );
            return;
        }

        log.verbose( 'Rendering: ' + lang );

        lastRenderedLanguage = lang;

        // We have to unmount in order for the full language refresh to occur
        // This was not necessary in previous versions but was discovered later.
        // It's not ideal but only happens on language changes.
        ReactDOM.unmountComponentAtNode( document.getElementById( 'app' ) );
        ReactDOM.render( Router, document.getElementById( 'app' ) );

        log.verbose( 'Rendered' );
    } );
} );
"use strict";

import React, { Component, PropTypes } from "react";
import UserHelpers from "/imports/accounts/UserHelpers";
import {ConnectMeteorData} from '../lib/ReactMeteorData';
import Forbidden from './errors/Forbidden';

export class TopLevelModeration extends Component {

    render() {
        const { user } = this.props;

        if ( !UserHelpers.isModerator( user ) ) {
            return <Forbidden/>
        }

        return this.props.children;
    }

}

TopLevelModeration.propTypes = {
    user: PropTypes.object.isRequired
};

export default ConnectMeteorData( props => {
    return {
        user: Meteor.user()
    }
} )( TopLevelModeration );
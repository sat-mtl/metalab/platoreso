"use strict";

import React, {Component, PropTypes} from "react";
import PureRenderMixin from "react-addons-pure-render-mixin";
import {DragDropContext} from "react-dnd";
import HTML5Backend from "react-dnd-html5-backend";
import classNames from "classnames";
import SettingsCollection from "/imports/settings/SettingsCollection";
import {ConnectMeteorData} from "../lib/ReactMeteorData";
import Warning from "./app/Warning";
import Menu from "./app/Menu";
import ChatBar from "./chat/ChatBar";

class App extends Component {

    constructor( props ) {
        super( props );
        this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind( this );
    }

    render() {
        const { user, settings, location, children } = this.props;
        const showWarning                            = settings && settings.shared.environment != "production" && settings.shared.environment != "local";

        return (
            <div className={ classNames( "interface", { warning: showWarning, chat: user } )}>
                { showWarning && <Warning environment={ settings.shared.environment}/>}
                <header id="header">
                    <Menu user={user} location={location}/>
                </header>

                {children}

                {/* Isolate chatbar, otherwise the chat doesn't like being moved around and scrolls to top */}
                <aside>
                    <ChatBar/>
                </aside>
            </div>
        );
    }
}

// Wrap into a drag drop context to enable drag drop application-wide
export default ConnectMeteorData( props => {
    return {
        user:     Meteor.user(),
        settings: SettingsCollection.findOne( {} )
    }
} )( DragDropContext( HTML5Backend )( App ) );

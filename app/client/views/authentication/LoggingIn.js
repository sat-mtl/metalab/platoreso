"use strict";

import React, {Component, PropTypes} from "react";

export default () => (
    <div className="ui active dimmer">
        <div className="ui large indeterminate text loader">
            {__( 'Logging in' )}
        </div>
    </div>
);
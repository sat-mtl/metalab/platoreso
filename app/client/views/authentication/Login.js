"use strict";

import AccountHelpers from "/imports/accounts/AccountHelpers";
import React, {Component, PropTypes} from "react";
import {Link} from "react-router";
import classNames from "classnames";
import ContainedPage from "../layouts/ContainedPage";
import Form from "../../components/forms/Form";
import Field from "../../components/forms/Field";
import Input from "../../components/forms/Input";

export default class Login extends Component {

    constructor( props ) {
        super( props );

        this.loginWithPassword = this.loginWithPassword.bind( this );
        this.loginWithFacebook = this.loginWithFacebook.bind( this );
        this.loginWithTwitter  = this.loginWithTwitter.bind( this );
        this.loginWithLinkedIn = this.loginWithLinkedIn.bind( this );
        this.loginWithGoogle   = this.loginWithGoogle.bind( this );
        this.loggedIn          = this.loggedIn.bind( this );

        this.state = {
            loggingIn: null,
            error:     null
        }
    }

    loginWithPassword( values ) {
        this.setState( { loggingIn: 'password' } );
        AccountHelpers.login( _.extend( { service: 'password' }, values ), _.bind( this.loggedIn, this ) );
    }

    loginWithFacebook() {
        this.setState( { loggingIn: 'facebook' } );
        AccountHelpers.login( { service: 'facebook' }, _.bind( this.loggedIn, this ) );
    }

    loginWithTwitter() {
        this.setState( { loggingIn: 'twitter' } );
        AccountHelpers.login( { service: 'twitter' }, _.bind( this.loggedIn, this ) );
    }

    loginWithGoogle() {
        this.setState( { loggingIn: 'google' } );
        AccountHelpers.login( { service: 'google' }, _.bind( this.loggedIn, this ) );
    }

    loginWithLinkedIn() {
        this.setState( { loggingIn: 'linkedin' } );
        AccountHelpers.login( { service: 'linkedin' }, _.bind( this.loggedIn, this ) );
    }

    loggedIn( err ) {
        if ( err ) {
            this.setState( { error: err, loggingIn: null } );
        } else {
            if ( this.props.redirect ) {
                this.context.router.replace( { pathname: '/dashboard' } );
            }
        }
    }

    render() {
        return (
            <ContainedPage id="login">

                <header className="sixteen wide column">
                    <h2 className="ui inverted center aligned header">
                        {__( 'Login' )}
                        <div className="sub header">
                            {__( 'Login or register to PlatoReso.' )}
                        </div>
                    </h2>
                </header>

                <div className="centered sixteen wide mobile ten wide computer column">

                    <div className="ui stackable two column grid">

                        <div className="one column row">
                            <div className="column">
                                <Form id="login-form" ref="loginForm" error={this.state.error} onSuccess={this.loginWithPassword} className="inverted">
                                    <Field label={__( 'Email' )}>
                                        <Input name="email" type="email" className="fluid"
                                               rules={[
                                                   { type: 'empty', prompt: __( 'Please enter your email' ) },
                                                   { type: 'email', prompt: __( 'Please enter a valid email' ) }
                                               ]}/>
                                    </Field>
                                    <Field label={__( 'Password' )}>
                                        <Input name="password" type="password" className="fluid"
                                               rules={[
                                                   { type: 'empty', prompt: __( 'Please enter your password' ) },
                                                   {
                                                       type: 'minLength[6]',
                                                       prompt: __( 'Your password should be at least 6 characters long' )
                                                   }
                                               ]}/>
                                    </Field>
                                    <div className={ classNames( "ui huge positive right labeled icon fluid submit button", {
                                        loading:  this.state.loggingIn == 'password',
                                        disabled: this.state.loggingIn
                                    } ) }>
                                        {__( 'Login' )}
                                        <i className="checkmark icon"/>
                                    </div>
                                </Form>
                            </div>
                        </div>

                        <div className="sixteen wide column">
                            <h3 className="ui inverted dividing sub header">{__( 'Login using' )}</h3>
                        </div>

                        <div className="column">
                            <div className={ classNames( "ui labeled icon button facebook fluid", {
                                loading:  this.state.loggingIn == 'facebook',
                                disabled: this.state.loggingIn
                            } ) }
                                 onClick={this.loginWithFacebook}>
                                {__( 'Facebook' )}
                                <i className="inverted facebook icon"/>
                            </div>
                        </div>
                        <div className="column">
                            <div className={ classNames( "ui labeled icon button twitter fluid", {
                                loading:  this.state.loggingIn == 'twitter',
                                disabled: this.state.loggingIn
                            } ) }
                                 onClick={this.loginWithTwitter}>
                                {__( 'Twitter' )}
                                <i className="inverted twitter icon"/>
                            </div>
                        </div>
                        <div className="column">
                            <div className={ classNames( "ui labeled icon button google plus fluid", {
                                loading:  this.state.loggingIn == 'google',
                                disabled: this.state.loggingIn
                            } ) }
                                 onClick={this.loginWithGoogle}>
                                {__( 'Google' )}
                                <i className="inverted google icon"/>
                            </div>
                        </div>
                        <div className="column">
                            <div className={ classNames( "ui labeled icon button linkedin fluid", {
                                loading:  this.state.loggingIn == 'linkedin',
                                disabled: this.state.loggingIn
                            } ) }
                                 onClick={this.loginWithLinkedIn}>
                                {__( 'LinkedIn' )}
                                <i className="inverted linkedin icon"/>
                            </div>
                        </div>

                        <div className="sixteen wide column">
                            <div className="ui divider"></div>
                            <Link to="/register" className="ui grey fluid button">
                                {__( 'Create an account' )}
                            </Link>
                            <div className="ui hidden divider"></div>
                            <Link to="/lost-password" className="ui inverted basic button grey fluid">
                                {__( 'I lost my password' )}
                            </Link>
                        </div>

                    </div>
                </div>

            </ContainedPage>
        );
    }
}

Login.defaultProps = {
    redirect: true
};

Login.contextTypes = {
    router: PropTypes.object.isRequired
};
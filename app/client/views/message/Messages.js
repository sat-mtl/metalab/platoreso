"use strict";

import React, {Component, PropTypes} from "react";
import {Link} from "react-router";
import {ConnectMeteorData} from "../../lib/ReactMeteorData";
import ConversationCollection from "/imports/messages/ConversationCollection";
import Scrollable from "../../components/utils/Scrollable";
import ConversationImage from "./ConversationImage";
import Conversation from "./Conversation";

export class Messages extends Component {

    render() {
        const { conversations, conversation } = this.props;

        return (
            <section id="messages">
                <div className="conversations-bar">
                    <div className="header">{__( 'Conversations' )}</div>
                    <div className="conversation-list-container">
                        <Scrollable className="conversation-list">
                            { conversations.map( conversation => {
                                let unread = 0;
                                if ( conversation && conversation.participant ) {
                                    unread = conversation.participant.unreadCount;
                                }
                                return (
                                    <Link key={conversation._id}
                                          className="conversation"
                                          activeClassName="active"
                                          to={`/messages/${conversation._id}`}>
                                        <ConversationImage conversation={conversation}/>
                                        <span className="conversation-name">#{conversation.slug}</span>
                                        { unread > 0 && <div className="ui mini red circular label">{unread}</div> }
                                    </Link> );
                            } ) }
                        </Scrollable>
                    </div>

                </div>
                <div className="conversation-panel">
                    { conversation && <Conversation conversation={conversation}/> }
                </div>
            </section>
        );
    }

}

export default ConnectMeteorData( props => {
    return {
        loading:       !Meteor.subscribe( 'conversations' ).ready(),
        conversations: ConversationCollection.find( {} ).fetch(),
        conversation:  props.params.conversationId ? ConversationCollection.findOne( { _id: props.params.conversationId } ) : null
    }
} )( Messages );
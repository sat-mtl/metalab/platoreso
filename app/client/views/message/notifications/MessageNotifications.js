"use strict";

import React, { Component, PropTypes } from "react";
import ConversationCollection from "/imports/messages/ConversationCollection";
import { ConnectMeteorData } from "../../../lib/ReactMeteorData";
import MessageNotification from "./MessageNotification";

export class MessageNotifications extends Component {

    render() {
        const { loading, conversations } = this.props;
        const count                      = conversations
            .map( conversation => conversation.messageCount )
            .reduce( ( pre, cur ) => pre + cur, 0 );

        if ( loading && count == 0 ) {
            return (
                <div className="ui large active indeterminate text loader">
                    {__( 'Loading conversations...' )}
                </div>
            );
        } else if ( count == 0 ) {
            return (
                <div className="empty">{__( 'You have no unread messages' )}</div>
            );
        } else {
            return (
                <div className="notificationsContainer">
                    { conversations.map( conversation =>
                        <MessageNotification key={conversation._id} conversation={conversation} onClose={this.props.onClose}/> ) }
                </div> );
        }
    }
}

MessageNotifications.propTypes = {
    loading:       PropTypes.bool,
    conversations: PropTypes.arrayOf( PropTypes.object ).isRequired,
    onClose:       PropTypes.func.isRequired
};

MessageNotifications.defaultProps = {
    loading: false
};

/**
 * Meteor Data Wrapper
 * With an es5 function to keep this.setState
 */
export default ConnectMeteorData( function ( props ) {
    return {
        loading:       !Meteor.subscribe( 'messages/unread' ).ready(),
        conversations: ConversationCollection.find( {} ).fetch(),
    }
} )( MessageNotifications );
"use strict";

import React, {Component, PropTypes} from "react";
import {Link} from "react-router";
import moment from "moment";
import classNames from "classnames";
import UserHelpers from "/imports/accounts/UserHelpers";
import MessageCollection from "/imports/messages/MessageCollection";
import {ConnectMeteorData} from "../../../lib/ReactMeteorData";
import ConversationImage from "../ConversationImage";

export class MessageNotification extends Component {

    constructor( props ) {
        super( props );
        this.getMessage = this.getMessage.bind( this );
        this.visit = this.visit.bind(this);
    }

    getMessage() {
        const { message, author } = this.props;
        return ( author ? `<span>${UserHelpers.getDisplayName( author )}:&nbsp;</span>` : '') + message.excerptMarkdown();
    }

    visit() {
        this.context.router.push( { pathname: `/messages/${this.props.conversation._id}` } );
        this.props.onClose();
    }

    render() {
        const { conversation, message, author } = this.props;
        if ( !message ) {
            return null;
        }
        const messageDateMoment = moment( message.createdAt );
        const unread            = moment( conversation.participant.readAt ).isBefore( messageDateMoment );

        return (
            <div className={classNames( 'notification', {
                           unread,
                unclicked: unread
            } )} onClick={this.visit}>
                <div className="subject image">
                    <ConversationImage conversation={conversation}/>
                </div>
                <div className="content">
                    <div className="conversation">#{conversation.name}</div>
                    <div className="message" dangerouslySetInnerHTML={{ __html: this.getMessage() }}></div>
                    <div className="date">
                        {messageDateMoment.fromNow()}
                    </div>
                </div>
                <div className="extra">
                    <div className="actions">
                        <span className={classNames( 'readIndicator', { unread } )}/>
                    </div>
                </div>
            </div>
        );
    }
}

MessageNotification.propTypes = {
    conversation: PropTypes.object.isRequired,
    message:      PropTypes.object,
    onClose:       PropTypes.func.isRequired
};

MessageNotification.contextTypes = {
    router: PropTypes.object.isRequired
};

/**
 * Meteor Data Wrapper
 */
export default ConnectMeteorData( ( { conversation } ) => {
    const message = MessageCollection.findOne( { conversation: conversation._id }, { sort: { createdAt: -1 } } );
    return {
                message,
        author: message && message.getAuthor()
    }
} )( MessageNotification );
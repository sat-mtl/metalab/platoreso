"use strict";

import React, { Component, PropTypes } from "react";
import PureRenderMixin from 'react-addons-pure-render-mixin';
import { FS } from "meteor/cfs:base-package";
import MessageMethods from "/imports/messages/MessageMethods";
import MessageImagesCollection from "/imports/messages/MessageImagesCollection";
import MessageList from "./conversation/MessageList";
import MessageInput from "./conversation/MessageInput";
import ConversationImage from "./ConversationImage";
import Image from "../widgets/Image";
import Modal from '../../components/Modal';

const { File } = FS;

export default class Conversation extends Component {

    constructor( props ) {
        super( props );

        this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind( this );

        this.close          = this.close.bind( this );
        this.openImage      = this.openImage.bind( this );
        this.closeImage     = this.closeImage.bind( this );
        this.sendMessage    = this.sendMessage.bind( this );
        this.pictureChanged = this.pictureChanged.bind( this );
        this.renderModal    = this.renderModal.bind( this );

        this.state = {
            picture:     null,
            openedImage: null
        }
    }

    pictureChanged( files ) {
        this.setState( {
            picture: ( files && files.length > 0 ) ? files[0] : null
        } );
    }

    close() {
        this.props.onClose( this.props.conversation._id );
    }

    openImage( image ) {
        this.setState( {
            openedImage: image
        } );
    }

    closeImage() {
        this.setState( {
            openedImage: null
        } );
    }

    sendMessage( message ) {
        const { conversation } = this.props;
        const { picture }      = this.state;

        MessageMethods.send.call( {
            conversation: conversation._id,
            content:      message,
            hasImage:     picture != null
        }, ( err, messageId ) => {
            if ( err ) {
                console.error( err );
            } else {
                // Project is created, we can now upload the image
                if ( picture ) {
                    let file    = new File( picture );
                    file.owners = [messageId];
                    MessageImagesCollection.insert( file, err => {
                        if ( err ) {
                            console.error( err );
                        }
                    } );
                }
            }
        } );
    }

    renderModal() {
        const { openedImage }  = this.state;

        return (
            <Modal className="conversation-image" opened={ openedImage != null } onRequestClose={this.closeImage}>
                <Image image={openedImage}/>
            </Modal>
        );
    }

    render() {
        const { conversation } = this.props;

        if ( !conversation ) {
            return null;
        }

        return (
            <div className="conversation-messages">
                <div className="header">
                    <ConversationImage conversation={conversation}/>
                    <div className="details">
                        <div className="title">{conversation.name} <span className="slug">(#{conversation.slug})</span>
                        </div>
                        <div className="topic" dangerouslySetInnerHTML={{ __html: conversation.topicMarkdown }}/>
                    </div>
                </div>
                <div className="message-list-container">
                    <MessageList conversation={conversation} openImage={this.openImage}/>
                </div>
                <MessageInput conversation={conversation} onSend={this.sendMessage} pictureChanged={this.pictureChanged}/>
                { this.renderModal() }
            </div>
        );
    }
}

Conversation.propTypes = {
    conversation: PropTypes.object.isRequired
};
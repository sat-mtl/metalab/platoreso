"use strict";

import React, {Component, PropTypes} from "react";
import classNames from "classnames";
import ConversationType from "/imports/messages/ConversationType";
import GroupImagesCollection from "/imports/groups/GroupImagesCollection";
import {ConnectMeteorData} from "../../lib/ReactMeteorData";
import Image from "../widgets/Image";

export class ConversationImage extends Component {
    render() {
        const { image } = this.props;
        return image
            ? <Image className={classNames("circular avatar", this.props.className )} image={image} size="thumb"/>
            : <div className="ui circular avatar image"><img src="/images/platform-avatar.png"/></div>;
    }
}

ConversationImage.propTypes = {
    conversation: PropTypes.object.isRequired,
    image:        PropTypes.object
};

export default ConnectMeteorData( ( { conversation } ) => {
    let image;
    switch ( conversation.type ) {
        case ConversationType.group:
            image = GroupImagesCollection.findOne( { owners: conversation.entity } );
            break;
    }
    return { image }
} )( ConversationImage );

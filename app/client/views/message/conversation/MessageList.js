"use strict";

import React, {Component, PropTypes} from "react";
import PureRenderMixin from "react-addons-pure-render-mixin";
import moment from "moment";
import ConversationMethods from "/imports/messages/ConversationMethods";
import ConversationHelpers from "/imports/messages/ConversationHelpers";
import MessageCollection from "/imports/messages/MessageCollection";
import Scrollable from "../../../components/utils/Scrollable";
import {ConnectMeteorData} from "../../../lib/ReactMeteorData";
import Message from "./Message";

export class MessageList extends Component {

    constructor( props ) {
        super( props );
        this.lastPinged = null;

        this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind( this );

        this.loadMore            = this.loadMore.bind( this );
        this.onFocus             = this.onFocus.bind( this );
        this.onBlur              = this.onBlur.bind( this );
        this.onScroll            = this.onScroll.bind( this );
        this.checkScrollToBottom = this.checkScrollToBottom.bind( this );
        this.scrollToBottom      = this.scrollToBottom.bind( this );
        this.setupConversation   = this.setupConversation.bind( this );
        // Throttle ping method as not to overload the server
        this.ping                = _.throttle( this.ping.bind( this ), 500 );
        this.checkPing           = this.checkPing.bind( this );

        this.state = {
            focused:         document.hidden != null ? !document.hidden : true,
            atBottom:        true,
            /**
             * New message line date, it needs to be in state
             * so that we can keep it after pinging and also reset it
             * when sending a message
             */
            newMessagesDate: null
        };
    }

    componentWillMount() {
        this.setupConversation();
        this.checkPing();
    }

    componentDidMount() {
        $( window ).on( "focus", this.onFocus );
        $( window ).on( "blur", this.onBlur );
    }

    componentWillUnmount() {
        $( window ).off( "focus", this.onFocus );
        $( window ).off( "blur", this.onBlur );

        // This is used by notifications to determine the focused conversation
        Session.set( 'conversation.focused', null );
    }

    componentDidUpdate( prevProps, prevState ) {
        const { user, conversation, messages }                                         = this.props;
        const { conversation: previousConversation, messages: previousMessages }       = prevProps;
        const { atBottom }                                                             = this.state;

        // If conversation changes, setup again
        if ( conversation._id != previousConversation._id ) {
            this.setupConversation();
        }

        // If we sent the last message and are not at the bottom of the list, scroll to bottom
        const lastMessage         = messages && messages.length > 0 && messages[messages.length - 1];
        const previousLastMessage = previousMessages && previousMessages.length > 0 && previousMessages[previousMessages.length - 1];
        if ( !atBottom
            && lastMessage
            && ( !previousLastMessage || previousLastMessage._id != lastMessage._id )
            && lastMessage.author == user._id
        ) {
            this.scrollToBottom();
        }

        this.checkPing();
    }

    onFocus() {
        const { conversation } = this.props;

        // This is used by notifications to determine the focused conversation
        Session.set( 'conversation.focused', conversation._id );

        this.setState( {
            focused: true
        } );
    }

    onBlur() {
        const { conversation } = this.props;

        // This is used by notifications to determine the focused conversation
        Session.set( 'conversation.focused', null );

        this.setState( {
            focused:         false,
            // Update the new message line when leaving, we can't update it while
            // focused since it would change with every ping
            newMessagesDate: conversation.participant.readAt
        } );
    }

    onScroll( { atBottom } ) {
        this.setState( { atBottom: atBottom } );
    }

    setupConversation() {
        const { conversation } = this.props;

        // This is used by notifications to determine the focused conversation
        Session.set( 'conversation.focused', conversation._id );

        // Set our new message line date
        this.setState( {
            newMessagesDate: conversation.participant.readAt
        } );
    }

    /**
     * Check if we need to ping the conversation
     */
    checkPing() {
        const { user, conversation, messages }           = this.props;
        const { focused, atBottom }                      = this.state;

        // No messages, get out
        if ( !messages || messages.length == 0 ) {
            return;
        }

        // Not focused nor at bottom, get out
        if ( !focused || !atBottom ) {
            return;
        }

        // Get last message
        const pingMessage = messages[messages.length - 1];

        // Message hasn't changed, get out
        if ( this.lastPinged == pingMessage._id ) {
            return;
        }

        if ( pingMessage.author == user._id ) {
            // Last message if from us, so reset new message line, and no need to
            // ping as it is auto-pinged on the server when we are the sender
            this.setState( {
                newMessagesDate: null
            } );

        } else if ( conversation.participant.readAt < pingMessage.createdAt ) {
            // Message is not from us and later than our readAt
            this.ping();
        }

        this.lastPinged = pingMessage._id;
    }

    /**
     * Ping the conversation
     * Is throttled in the contructor, to make sure we don't call too often
     */
    ping() {
        const { conversation } = this.props;
        ConversationMethods.ping.call( { conversationId: conversation._id }, error => {
            if ( error ) {
                console.error( error );
            }
        } );
    }

    /**
     * Check if we need to be at the bottom
     * Scrollable has a method for keeping it scrolled at bottom
     * but we have other constraints about messages being loaded that
     * require us to control it manually
     */
    checkScrollToBottom() {
        if ( this.state.atBottom ) {
            this.scrollToBottom();
        }
    }

    /**
     * Scroll the message list to the bottom
     */
    scrollToBottom() {
        // We cheat a bit here, this is VERY "anti-react"
        // But we have no other way to the component to scroll on-demand
        this.refs.scrollable.scrollToBottom();
    }

    loadMore() {
        this.props.loadMore();
    }

    renderMessages() {
        const { messages, openImage, canModerate }                             = this.props;
        const { newMessagesDate }                                 = this.state;

        let drewDivider     = false;
        let dayDividerCount = 0;
        let lastDay;

        let nodes = [];

        messages.forEach( message => {
            const date = moment( message.createdAt );

            // Render day divider
            if ( lastDay != date.dayOfYear() ) {
                if ( lastDay ) {
                    nodes.push(
                        <div key={`day-divider-${dayDividerCount++}`} className="day ui horizontal divider">{date.format( 'MMMM Do' )}</div>
                    );
                }
                lastDay = date.dayOfYear();
            }

            // Render new messages divider
            if ( !drewDivider && newMessagesDate && newMessagesDate < message.createdAt ) {
                nodes.push(
                    <div key="new-message-divider" className="new divider">{__( 'New Messages' )}</div>
                );
                drewDivider = true;
            }

            // Render message
            nodes.push(
                <Message key={message._id} message={message} canModerate={canModerate} openImage={openImage} onLoaded={this.checkScrollToBottom}/> );
        } );
        return nodes;
    }

    render() {
        const { conversation, messages }           = this.props;
        const { atBottom }                         = this.state;

        let unread = 0;
        if ( conversation && conversation.participant ) {
            unread = conversation.participant.unreadCount;
        }

        return (
            <div>
                <Scrollable ref="scrollable"
                            className="message-list"
                            fixTop={true}
                            fixBottom={true}
                            onScroll={this.onScroll}
                            topReached={this.loadMore}>
                    <div className="ui minimal comments">
                        <div className="ui mini basic button load-more" onClick={this.loadMore}>{__( 'Load More' )}</div>
                        { messages.length > 0 && <div className="since ui horizontal divider">
                            {__( 'Messages since __date__', { date: moment( messages[0].createdAt ).format( 'MMMM Do, HH:mm' ) } )}
                        </div> }
                        { this.renderMessages() }
                    </div>
                </Scrollable>
                { !atBottom && unread > 0 && <div className="new-message-indicator">
                    <div className="ui mini teal right labeled icon button" onClick={this.scrollToBottom}>
                        {__( 'New message' )}
                        <i className="arrow down icon"/>
                    </div>
                </div> }
            </div>
        );
    }
}

MessageList.propTypes = {
    loadMore:  PropTypes.func.isRequired,
    openImage: PropTypes.func.isRequired
};

/**
 * Meteor Data Wrapper
 * With an es5 function to keep this.setState
 */
export default ConnectMeteorData( function ( { conversation } ) {
    const messages = MessageCollection.find( { conversation: conversation._id }, { sort: { createdAt: 1 } } ).fetch();
    return {
        user:        Meteor.user(),
        loading:     !Meteor.subscribe( 'messages', conversation._id, this.state ? this.state.startDate : null ).ready(),
        messages:    messages,
        canModerate: ConversationHelpers.canModerate( Meteor.userId(), conversation._id ),
        loadMore:    () => {
            this.setState( { startDate: messages && messages.length ? messages[0].createdAt : null } );
        }
    };
} )( MessageList );
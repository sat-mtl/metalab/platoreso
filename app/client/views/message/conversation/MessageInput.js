"use strict";

import React, {Component, PropTypes} from "react";
import ReactDOM from "react-dom";
import autosize from "autosize";
import MessageImageChooser from './MessageImageChooser';

export default class MessageInput extends Component {

    constructor( props ) {
        super( props );
        this.pictureChanged = this.pictureChanged.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind( this );
        this.handleChange   = this.handleChange.bind( this );
        this.state          = {
            typed: '',
            picture: null
        }
    }

    componentDidMount() {
        autosize( ReactDOM.findDOMNode( this.refs.input ) );
        this.refs.input.focus();
    }

    componentDidUpdate( prevProps, prevState ) {
        autosize.update( ReactDOM.findDOMNode( this.refs.input ) );
    }

    pictureChanged(picture) {
        this.setState({picture});
        this.props.pictureChanged(picture);
        this.refs.input.focus();
    }

    handleKeyPress( event ) {
        if ( event.key == 'Enter' && !event.altKey && !event.ctrlKey && !event.shiftKey ) {
            event.preventDefault();
            const typed = this.state.typed.trim();
            if ( this.state.picture || !_.isEmpty( typed ) ) {
                this.props.onSend( typed );
                this.refs.imageChooser.removePicture();
            }
            this.setState( { picture: null, typed: '' } );
        }
    }

    handleChange( event ) {
        this.setState( { typed: event.target.value } );
    }

    render() {
        const { conversation, pictureChanged } = this.props;
        const { typed } = this.state;

        return (
            <div className="message-input">
                    <textarea ref="input"
                              type="text"
                              className="input autosize"
                              rows={1}
                              value={typed}
                              placeholder={__("Message __conversation__", { conversation: `#${conversation.slug}`})}
                              onKeyPress={this.handleKeyPress}
                              onChange={this.handleChange}/>
                <MessageImageChooser ref="imageChooser" filesChanged={this.pictureChanged}/>
            </div>
        );
    }
}

MessageInput.propTypes = {
    conversation: PropTypes.object.isRequired,
    onSend: PropTypes.func.isRequired,
    pictureChanged: PropTypes.func.isRequired
};
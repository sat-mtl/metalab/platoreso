"use strict";

import React, { Component, PropTypes } from "react";
import ImageChooser from '../../../components/forms/ImageChooser';

export default class MessageImageChooser extends ImageChooser {

    render() {
        const { preview, image }           = this.props;
        const { hasPreview, removedImage } = this.state;
        const something                    = ( hasPreview || image ) && !removedImage;
        const showImage                    = preview && !hasPreview && image && !removedImage;
        return (
            <div className="imageChooser">
                <input type="file" ref="file" accept="image/*" style={{ display: 'none' }} onChange={this.filesChanged}/>
                <div ref="preview" className="preview"></div>
                { showImage && <Image image={image} size="small" className="preview"/> }
                { something && <div className="remove" onClick={this.removePicture}>
                    <i className="remove icon"/>
                </div> }
                { !something
                    ? <div className="ui mini basic grey icon button add" onClick={this.choosePicture}>
                      <i className="image icon"/></div>
                    : null
                }
            </div>
        );
    }

}
"use strict";

import React, {Component, PropTypes} from "react";
import PureRenderMixin from "react-addons-pure-render-mixin";
import ReactDOM from "react-dom";
import classNames from "classnames";
import {Link} from "react-router";
import md from "/imports/utils/markdown";
import UserHelpers from "/imports/accounts/UserHelpers";
import ModerationMethods from "/imports/moderation/ModerationMethods";
import MessageCollection from "/imports/messages/MessageCollection";
import {ConnectMeteorData} from "../../../lib/ReactMeteorData";
import LiveDate from "../../widgets/LiveDate";
import Avatar from "../../widgets/Avatar";
import Image from "../../widgets/Image";

export class Message extends Component {

    constructor( props ) {
        super( props );

        this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind( this );

        this.onImageLoaded = this.onImageLoaded.bind( this );
        this.report        = this.report.bind( this );
        this.moderate      = this.moderate.bind( this );
        this.approve       = this.approve.bind( this );
        this.openImage     = this.openImage.bind( this );
        this.renderMessage = this.renderMessage.bind( this );
    }

    componentDidMount() {
        $( 'img', ReactDOM.findDOMNode( this.refs.message ) ).on( "load", this.onImageLoaded );
    }

    componentWillUnmount() {
        $( 'img', ReactDOM.findDOMNode( this.refs.message ) ).off( "load", this.onImageLoaded );
    }

    onImageLoaded() {
        if ( this.props.onLoaded ) {
            this.props.onLoaded();
        }
    }

    report() {
        const { message } = this.props;
        if ( !message ) {
            return;
        }
        ModerationMethods.report.call( { entityTypeName: MessageCollection.entityName, entityId: message._id } );
    }

    moderate() {
        const { message } = this.props;
        if ( !message ) {
            return;
        }
        ModerationMethods.moderate.call( { entityTypeName: MessageCollection.entityName, entityId: message._id } );
    }

    approve() {
        const { message } = this.props;
        if ( !message ) {
            return;
        }
        ModerationMethods.approve.call( { entityTypeName: MessageCollection.entityName, entityId: message._id } );
    }

    openImage() {
        this.props.openImage( this.props.image );
    }

    renderMessage() {
        const { message, canModerate } = this.props;
        if ( message.moderated ) {
            if ( canModerate ) {
                return (
                    <div className="ui mini compact message">
                        <div className="header">{__( 'Message moderated, you can still see it because you are a moderator.' )}</div>
                        <p>{message.content}</p>
                    </div>
                );
            } else {
                return ( <div className="ui mini compact message">{__( 'Message moderated' )}</div> );
            }
        } else if ( message.content ) {
            if ( message.content.substr( 0, 1 ) == '/' ) {
                const parsed = message.content.substr( 1 ).split( ' ' );
                switch ( parsed[0] ) {
                    case 'slap':
                        return <div className="message"
                                    dangerouslySetInnerHTML={{
                                        __html: md( __( '__user__ slaps __other__ around a bit with a large trout :fish:', {
                                            user:  UserHelpers.getDisplayName( message.author ),
                                            other: parsed.slice( 1 ).join( ' ' )
                                        } ) )
                                    }}/>;
                        break;
                    default:
                        return <div ref="message"
                                    dangerouslySetInnerHTML={{ __html: message.contentMarkdown }}/>;
                        break;
                }
            } else {
                return <div ref="message"
                            dangerouslySetInnerHTML={{ __html: message.contentMarkdown }}/>;
            }
        } else {
            return null;
        }
    }

    render() {
        const { message, author, image, canModerate } = this.props;
        const youReported                             = message.isReportedBy( Meteor.userId() );

        return (
            <div className="message comment">
                <Avatar user={author}/>
                <div className="content">
                    <span className="author">
                        { author ? (
                            <Link to={'/profile/' + author._id}>
                                {UserHelpers.getDisplayName( author )}
                            </Link>
                        ) : <a className="author">{ __( 'Author not found!' ) }</a> }
                    </span>
                    <div className="metadata">
                        <div className="date">{ message.createdAt &&
                        <LiveDate date={ message.createdAt } relative={true}/> }</div>
                    </div>
                    <div className="text">
                        { this.renderMessage() }
                        { image && <div className="attachment" onClick={this.openImage}>
                            <Image image={image} size="small" className="" onUpdated={this.onImageLoaded}/></div> }
                    </div>
                    <div className="actions">
                        { !message.moderated &&
                        <a className={classNames( "report", { reported: youReported } )} onClick={this.report}>{ youReported ? __( 'You reported this' ) : __( 'Report' ) }</a> }
                        { canModerate && !message.moderated &&
                        <a className={classNames( "moderate" )} onClick={this.moderate}>{ __( 'Moderate' ) }</a> }
                        { canModerate && message.moderated &&
                        <a className={classNames( "approve" )} onClick={this.approve}>{ __( 'Approve' ) }</a> }
                    </div>
                </div>
            </div>
        );
    }
}

Message.propTypes = {
    message:     PropTypes.object.isRequired,
    author:      PropTypes.object,
    onLoaded:    PropTypes.func,
    openImage:   PropTypes.func.isRequired,
    canModerate: PropTypes.bool
};

export default ConnectMeteorData( ( { message } ) => {
    return {
        author: message.getAuthor(),
        image:  message.hasImage && message.getImage()
    }
} )( Message );
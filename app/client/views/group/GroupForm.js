"use strict";

import React, {Component, PropTypes} from "react";

import Field from '../../components/forms/Field';
import Input from '../../components/forms/Input';
import TextArea from '../../components/forms/Textarea';
import CheckBox from '../../components/forms/Checkbox';
import ImageChooser from '../../components/forms/ImageChooser';
import UserInvite from "./edit/UserInvite";

export default class GroupForm extends Component {

    render() {
        const { group, image, pictureChanged, permissionsChanged } = this.props;

        return (
            <div className="ui stackable grid groupForm">

                <div className="row">

                    <div className="ten wide column">
                        <Field label={__("Name")} className="required">
                            <Input ref="name" name="name" type="text"
                                   defaultValue={group && group.name}
                                   rules={[
                                       { type: 'empty', prompt: __('Please enter a name') },
                                       { type: 'minLength[3]', prompt: __( 'Group name should be a minimum of 3 characters long' ) },
                                       { type: 'maxLength[140]', prompt: __('Group name can be a maximum of 140 characters long') },
                                       { type: 'regExp[/(!?[a-zA-Z0-9])/]', prompt: __( 'Group names should not only contain non-alphanumeric characters' ) }
                                   ]}/>
                        </Field>

                        <Field label={__("Description")} className="required">
                            <TextArea ref="description"
                                      name="description"
                                      rows={2}
                                      autosize={true}
                                      defaultValue={ group && group.description }
                                      rules={[
                                          { type: 'empty', prompt: __('Please enter a description') }
                                      ]}/>
                        </Field>

                        <div className="two fields">

                            <Field label={__("Visibility")}>
                                <CheckBox ref="public" name="public" label={__('Public')}
                                          defaultChecked={group && group.public}/>
                            </Field>

                            <Field label={__("Privacy")}>
                                <CheckBox ref="showMembers" name="showMembers" label={__('Show Members')}
                                          defaultChecked={group && group.showMembers}/>
                            </Field>

                        </div>

                        <Field label={__( "Invitation Code" )}>
                            <Input ref="invitationCode" name="invitationCode" type="text"
                                   defaultValue={group && group.invitationCode}
                                   rules={[
                                       { type: 'regExp[/^[a-zA-Z0-9_-]*$/]', prompt: __( 'Only alphanumeric characters, underscores and dashes are allowed' ) }
                                   ]}/>
                        </Field>

                        <UserInvite group={group} permissionsChanged={permissionsChanged}/>
                    </div>

                    <div className="six wide column">
                        <Field label={__('Picture')}>
                            <ImageChooser image={image} filesChanged={pictureChanged}/>
                        </Field>
                    </div>
                </div>
            </div>
        );
    }
}

GroupForm.propTypes = {
    group:              PropTypes.object,
    image:              PropTypes.object,
    permissionsChanged: PropTypes.func.isRequired,
    pictureChanged:     PropTypes.func.isRequired
};
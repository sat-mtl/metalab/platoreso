"use strict";

import React, {Component, PropTypes} from "react";
import {GroupRoles} from "/imports/groups/GroupRoles";
import UserHelpers from "/imports/accounts/UserHelpers";
import Avatar from '../../../views/widgets/Avatar';
import Select from '../../../components/forms/Select';

export default class UserRow extends Component {

    constructor( props ) {
        super( props );
        this.roleChanged = this.roleChanged.bind( this );
        this.remove      = this.remove.bind( this );
    }

    roleChanged( roles ) {
        console.log(roles);
        this.props.changedRole( this.props.permission, roles );
    }

    remove() {
        this.props.onRemove( this.props.permission );
    }

    render() {
        const { permission } = this.props;
        const { user, email, roles } = permission;

        return (
            <tr>
                <td className="collapsing"><Avatar user={user}/></td>
                <td>{ user ? UserHelpers.getDisplayName( user ) : email }</td>
                <td>
                    {/*<Select className="fluid"
                            onSelectChange={this.roleChanged}
                            defaultValue={role}>
                        { GroupRoles.map( role => <option key={role} value={role}>{role}</option> ) }
                    </Select>*/}
                    <Select name="roles"
                            multiple={true}
                            onSelectChange={this.roleChanged}
                            defaultValue={roles}
                            rules={[
                                { type: 'empty', prompt: __( 'Please select at least one role' ) }
                            ]}
                            className="fluid">
                        { GroupRoles.map( role => <option key={role} value={role}>{__(`role.${role}`)}</option> ) }
                    </Select>
                </td>
                <td className="collapsing right aligned">
                    <div ref="actions" className="ui mini buttons">
                        <div className="ui compact red icon button"
                             onClick={this.remove}
                             data-content={__('Remove user')}
                             data-variation="inverted">
                            <i className="remove icon"/>
                        </div>
                    </div>
                </td>
            </tr>
        );
    }
}

UserRow.propTypes = {
    permission:  PropTypes.object.isRequired,
    changedRole: PropTypes.func.isRequired,
    onRemove:    PropTypes.func.isRequired
};
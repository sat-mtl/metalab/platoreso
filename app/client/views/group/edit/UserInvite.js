"use strict";

import React, {Component, PropTypes} from "react";
import ReactDOM from "react-dom";
import classNames from "classnames";
import Immutable from 'immutable';

import {RoleList} from "/imports/accounts/Roles";
import {GroupRoles, GROUP_PREFIX} from "/imports/groups/GroupRoles";

import {ConnectMeteorData} from '../../../lib/ReactMeteorData'

import UserCollection from "/imports/accounts/UserCollection";
import UserHelpers from "/imports/accounts/UserHelpers";
import Modal from '../../../components/semanticui/modules/Modal';
import Avatar from '../../widgets/Avatar';
import UserRow from "./UserRow";

/**
 * Immutable.js record to hold what is considered a "permission" in the ui
 */
const PermissionRecord = Immutable.Record( {
    __new:   false,
    id:      null,
    user:    null,
    email:   null,
    roles:    [RoleList.member],
    deleted: false
} );

export class UserInvite extends Component {

    /**
     * Get a map of permissions from the props
     *
     * @returns {Immutable.Map<PermissionRecord>}
     */
    static getPermissionsFromProps( { group, members } ) {
        return Immutable.Map(
            members
                .map( user => ({ id: user._id, user: user, roles: user.roles[`${GROUP_PREFIX}_${group._id}`] }) )
                .map( permission => [permission.id, new PermissionRecord( permission )] )
        );
    }

    constructor( props ) {
        super( props );

        this.addPermission             = this.addPermission.bind( this );
        this.changedRole               = this.changedRole.bind( this );
        this.dispatchPermissions       = this.dispatchPermissions.bind( this );
        this.confirmRemovePermission   = this.confirmRemovePermission.bind( this );
        this.removePermission          = this.removePermission.bind( this );
        this.removePermissionConfirmed = this.removePermissionConfirmed.bind( this );

        this.state = {
            permissions:              props.group ? UserInvite.getPermissionsFromProps( props ) : Immutable.Map(),
            // The permission to be removed when showing the remove modal
            permissionPendingRemoval: null
        };
    }

    componentDidMount() {
        $( ReactDOM.findDOMNode( this.refs.search ) ).dropdown( {
            allowAdditions: true,
            fullTextSearch: true
        } );
    }

    componentWillReceiveProps( nextProps ) {
        // When props change, rebuild the permissions map
        // FIXME: This has the potential to reset the current changes if we receive a reactive change from meteor
        // TODO: Keep our changes separate from the list representing the "truth"
        if ( !_.isEqual( nextProps, this.props ) ) {
            this.setState( {
                permissions: UserInvite.getPermissionsFromProps( nextProps )
            } );
        }
    }

    componentWillUnmount() {
        $( ReactDOM.findDOMNode( this.refs.search ) ).dropdown( 'destroy' );
    }

    /**
     * Add a permission set for a group
     */
    addPermission() {
        const { group } = this.props;

        const $select     = $( ReactDOM.findDOMNode( this.refs.search ) );
        const selectValue = ( $select.dropdown( 'get value' ) );
        const selectText  = ( $select.dropdown( 'get text' ) );
        $( ReactDOM.findDOMNode( this.refs.search ) ).dropdown( 'clear' );

        let id        = selectValue;
        const isEmail = selectValue == selectText; // Assume what was added is an email
        let roles   = (!isEmail && group) ? _.intersection( UserHelpers.getRolesForUser( id, GROUP_PREFIX + '_' + group._id ), GroupRoles ) : [];
        // Make sure they are at least member
        roles = _.uniq(roles.concat([RoleList.member]));
        const permission = new PermissionRecord( {
            __new: true,
            id:    id,
            user:  !isEmail ? UserCollection.findOne( { _id: id } ) : null,
            email: isEmail ? id : null,
            roles:  roles
        } );

        let state = {
            permissions: this.state.permissions.set( permission.id, permission )
        };

        // Set the state
        this.setState( state );
        this.dispatchPermissions( state.permissions );
    }

    changedRole( permission, roles ) {
        const permissions = this.state.permissions.set(
            permission.id,
            this.state.permissions.get( permission.id ).set( 'roles', roles )
        );
        this.setState( { permissions } );
        this.dispatchPermissions( permissions );
    }

    dispatchPermissions( current ) {
        this.props.permissionsChanged(
            current
                .map( permission => {
                    let perm = {};
                    if ( permission.deleted ) {
                        perm = {
                            user:    permission.user._id,
                            deleted: true
                        };
                    } else if ( permission.user ) {
                        perm = {
                            user: permission.user._id,
                            roles: permission.roles
                        };
                    } else {
                        perm = {
                            email: permission.email,
                            roles:  permission.roles
                        };
                    }
                    return perm;
                } )
                .toArray()
        );
    }

    /**
     * Confirm the removal of a permission
     * It just sets the state so that the modal can open itself
     */
    confirmRemovePermission( permission ) {
        if ( this.props.group ) {
            this.setState( {
                permissionPendingRemoval: permission
            } );
        } else {
            this.removePermission( permission );
        }
    }

    removePermissionConfirmed() {
        this.removePermission( this.state.permissionPendingRemoval )
    }

    /**
     * Remove a permission
     * Called by the modal's confirmation
     *
     * @param {Object} [permission]
     */
    removePermission( permission ) {
        if ( !permission ) {
            return;
        }

        let state = {
            permissions:              this.state.permissions.set( permission.id, this.state.permissions.get( permission.id ).set( 'deleted', true ) ),
            permissionPendingRemoval: null
        };

        // Set the state
        this.setState( state );
        this.dispatchPermissions( state.permissions );
    }

    render() {
        const { users, group } = this.props;
        const { permissions, permissionPendingRemoval } = this.state;

        return (
            <div>
                <h3 className="ui dividing header">{__( 'Invite Users' )}</h3>

                <div className="ui fluid buttons">
                    <div ref="search" className="ui labeled icon search dropdown basic button">
                        <i className="add user icon"/>
                        <span className="default text">{__( "Search or add by email..." )}</span>
                        <div className="menu">
                            { users.map( user => <div key={user._id} className="item" data-value={user._id}>
                                <Avatar user={user}/>
                                { UserHelpers.getDisplayName( user ) }
                                &nbsp;
                                { user.emails && user.emails.length &&
                                  <span className="email">&lt;{ user.emails[0].address }&gt;</span> }
                            </div> ) }
                        </div>
                    </div>
                    <div className="ui button" onClick={this.addPermission} style={{ flex: "0 0 auto" }}>Add</div>
                </div>

                {/* Permissions */}
                <table className="ui very basic small selectable table">
                    <thead>
                    <tr>
                        <th colSpan="2">{__( 'User Name' )}</th>
                        <th>{__( 'Roles' )}</th>
                        <th className="right aligned">{__( 'Actions' )}</th>
                    </tr>
                    </thead>
                    <tbody>
                    { permissions
                        .filter( permission => !permission.deleted )
                        .toArray()
                        .map( permission => (
                            <UserRow key={permission.id}
                                     permission={permission}
                                     changedRole={this.changedRole}
                                     onRemove={this.confirmRemovePermission}/>
                        ) ) }
                    </tbody>
                </table>

                {/* Remove Modal */}
                <Modal
                    opened={!!permissionPendingRemoval}
                    onApprove={this.removePermissionConfirmed}
                    onHide={()=>this.setState({permissionPendingRemoval:null})}>
                    <i className="close icon"/>

                    <div className="header">
                        {__( 'Remove User' )}
                    </div>
                    <div className="image content">
                        <div className="image">
                            <i className="group icon"/>
                        </div>
                        <div className="description">
                            <p>{ group
                                ? __( 'Are you sure you want to remove the user __user__ from group __group__?', {
                                user:  permissionPendingRemoval ? ( permissionPendingRemoval.user ? UserHelpers.getDisplayName( permissionPendingRemoval.user ) : permissionPendingRemoval.email ) : null,
                                group: group.name
                            } )
                                : __( 'Are you sure you want to remove the user __user__ from this group?', {
                                user: permissionPendingRemoval ? ( permissionPendingRemoval.user ? UserHelpers.getDisplayName( permissionPendingRemoval.user ) : permissionPendingRemoval.email ) : null
                            } )
                            }</p>
                        </div>
                    </div>
                    <div className="actions">
                        <div className="ui buttons">
                            <div className="ui red deny button">
                                <i className="remove icon"/>
                                {__( 'No' )}
                            </div>
                            <div className="ui green approve button">
                                <i className="checkmark icon"/>
                                {__( 'Yes' )}
                            </div>
                        </div>
                    </div>
                </Modal>
            </div>
        );
    }
}

UserInvite.propTypes = {
    group:              PropTypes.object,
    usersLoading:       PropTypes.bool.isRequired,
    users:              PropTypes.array,
    members:            PropTypes.array,
    permissionsChanged: PropTypes.func.isRequired
};

/**
 * Meteor Data Wrapper
 */
export default ConnectMeteorData( props => ({
    usersLoading: !Meteor.subscribe( 'user/list' ).ready(),
    users:        props.group ? UserCollection.find( { [`roles.${GROUP_PREFIX}_${props.group._id}`]: { $nin: GroupRoles } } ).fetch() : UserCollection.find().fetch(),
    members:      props.group ? UserCollection.find( { [`roles.${GROUP_PREFIX}_${props.group._id}`]: { $in: GroupRoles } } ).fetch() : []

} ) )( UserInvite );
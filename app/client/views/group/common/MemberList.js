"use strict";

import React, {Component, PropTypes} from "react";
import {Link} from "react-router";
import {GroupRoles, GROUP_PREFIX} from "/imports/groups/GroupRoles";
import UserHelpers from "/imports/accounts/UserHelpers";
import {ConnectMeteorData} from '../../../lib/ReactMeteorData';
import Avatar from "../../widgets/Avatar";

export class MemberList extends Component {

    render() {
        const { members } = this.props;

        return (
            <section id="members" className="sidebar-block">
                <h3 className="ui dividing header">{__( 'Members' )}</h3>
                <div className="ui relaxed list members">
                    { members.map( ( member ) => (
                        <MemberItem key={member._id} user={member}/>
                    ) ) }
                </div>
            </section>
        );
    }
}

MemberList.propTypes = {
    membersLoading: PropTypes.bool.isRequired,
    members:        PropTypes.arrayOf( PropTypes.object )
};

export default ConnectMeteorData( props => {
    const { group } = props;
    return {
        membersLoading: !Meteor.subscribe( 'group/member/list', group._id ).ready(),
        members:        UserHelpers.getUsersInRole( GroupRoles, GROUP_PREFIX + '_' + group._id ).fetch()
    };
} )( MemberList );

export class MemberItem extends Component {
    render() {
        const { user } = this.props;

        return (
            <div className="member item">
                <Avatar user={user}/>
                <div className="content">
                    <Link to={'/profile/' + user._id} className="header">{UserHelpers.getDisplayName( user )}</Link>
                    <div className="description">{user.points}&nbsp;<i className="trophy icon"/></div>
                </div>
            </div>
        );
    }
}
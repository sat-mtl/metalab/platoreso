"use strict";

import React, {Component, PropTypes} from "react";
import classNames from "classnames";
import {Link} from "react-router";
import prune from "underscore.string/prune";
import {Counts} from "meteor/tmeasday:publish-counts";
import GroupImagesCollection from "/imports/groups/GroupImagesCollection";
import {ConnectMeteorData} from "../../../../lib/ReactMeteorData";

export class GroupListItem extends Component {

    constructor( props ) {
        super( props );
        this.goToGroup = this.goToGroup.bind( this );
    }

    goToGroup() {
        const { group } = this.props;
        this.context.router.push( { pathname: `/group/${(group.slug || group._id)}` } );
    }

    render() {
        const { group, groupImage, projectCount } = this.props;

        const style = {
            backgroundColor: groupImage && groupImage.dominantColor ? '#' + groupImage.dominantColor : colorCollection[group.slug.length % colorCollection.length],
            backgroundImage: groupImage ? 'url(' + groupImage.url( { store: groupImage.collectionName + '_medium' } ) + ')' : 'none'
        };

        return (
            <div className={classNames( "column project group-item tile", { colored: !groupImage } )} onClick={this.goToGroup}>
                <div className="background" style={style}></div>
                <div className="layout">
                    <div className="name">
                        <Link to={`/group/${group.uri}`} title={group.name}>
                            { group.name }
                        </Link>
                    </div>
                    <div className="description">{ prune( group.description, 140 ) }</div>
                    <div className="extra">
                        <i className="white bullseye icon"/>
                        {__( 'project.count', { count: projectCount } )}
                    </div>
                </div>
            </div>
        );
    }
}

GroupListItem.propTypes = {
    group:        PropTypes.object.isRequired,
    groupImage:   PropTypes.object,
    projectCount: PropTypes.number.isRequired,
};

GroupListItem.contextTypes = {
    router: PropTypes.object.isRequired
};

export default ConnectMeteorData( props => {
    const { group } = props;
    Meteor.subscribe( 'project/count', group._id );
    return {
        projectCount: Counts.get( 'project/count/' + group._id ),
        groupImage:   GroupImagesCollection.findOne( { owners: group._id } )
    };
} )( GroupListItem );
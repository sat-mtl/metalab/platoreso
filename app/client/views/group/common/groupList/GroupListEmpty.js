"use strict";

import React, { Component, PropTypes } from "react";
import {Link} from 'react-router';
import UserHelpers from "/imports/accounts/UserHelpers";
import {ConnectMeteorData} from '../../../../lib/ReactMeteorData';

export class GroupListEmpty extends Component {

    constructor(props) {
        super(props);
        this.getNextSteps = this.getNextSteps.bind(this);
    }

    getNextSteps() {
        const { user } = this.props;
        if ( user ) {
            if ( UserHelpers.isExpert(user) ) {
                return <Link to="/admin/group/create">{ __('Try creating one.') }</Link>;
            } else {
                return __('You might not have access to any groups due to your permissions.');
            }
        } else {
            return <Link to="/login">{ __('Login or create an account to access more.') }</Link>;
        }
    }

    render() {
        const { user } = this.props;
        return (
            <div className="centered column empty-group-item empty-tiles">
                <div className="info">
                    <h2 className="ui inverted center aligned header">
                        <div className="content">
                            {__('No groups available')}
                            <div className="sub header">
                                <div>{ user ? __('There are no groups available for the moment.') : __('There are no public groups available for the moment.')}</div>
                                <div>{ this.getNextSteps() }</div>
                            </div>
                        </div>
                    </h2>
                </div>
            </div>
        );
    }
}

GroupListEmpty.propTypes = {
    user: PropTypes.object
};

export default ConnectMeteorData( props => {
    return {
        user: Meteor.user()
    }
} )( GroupListEmpty );
"use strict";

import React, {Component, PropTypes} from "react";
import GroupCollection from "/imports/groups/GroupCollection";
import {ConnectMeteorData} from "../../../lib/ReactMeteorData";
import Spinner from "../../widgets/Spinner";
import GroupListEmpty from "./groupList/GroupListEmpty";
import GroupListItem from "./groupList/GroupListItem";

export class GroupList extends Component {

    constructor( props ) {
        super( props );
        this.renderGroups = this.renderGroups.bind( this );
    }

    renderGroups() {
        const { groupsLoading, groups } = this.props;

        if ( groupsLoading ) {
            return <Spinner/>;
        } else if ( !groups || groups.length == 0 ) {
            return <GroupListEmpty/>;
        } else {
            return groups.map( group => <GroupListItem key={group._id} group={group}/> );
        }
    }

    render() {
        return (
            <div className="sixteen wide column groups tiles">
                <div className="ui two column equal width doubling grid">
                    { this.renderGroups() }
                </div>
            </div>
        );
    }
}

GroupList.propTypes = {
    groupsLoading: PropTypes.bool.isRequired,
    groups:        PropTypes.arrayOf( PropTypes.object )
};

export default ConnectMeteorData( props => {
    return {
        groupsLoading: !Meteor.subscribe( 'group/list' ).ready(),
        groups:        GroupCollection.find( {}, { sort: { updatedAt: -1 } } ).fetch()
    };
} )( GroupList );
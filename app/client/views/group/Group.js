"use strict";

import React, { Component, PropTypes } from "react";
import classNames from "classnames";
import prune from "underscore.string/prune";

import ModerationMethods from "/imports/moderation/ModerationMethods";
import GroupCollection from "/imports/groups/GroupCollection";
import GroupImagesCollection from "/imports/groups/GroupImagesCollection";
import ProjectCollection from "/imports/projects/ProjectCollection";

import { ConnectMeteorData } from '../../lib/ReactMeteorData';
import ContainedPage from '../layouts/ContainedPage';
import Spinner from '../widgets/Spinner';
import NotFound from '../errors/NotFound';
import MemberList from './common/MemberList';
import ProjectList, { RecentProjects, TopProjects, TrendingProjects } from "../project/common/ProjectList";
import Feed from '../feed/Feed';
import DropDown from '../../components/semanticui/modules/Dropdown';

const sortByStorage = new StoredVar( 'pr.projects.sortBy' );

export class Group extends Component {

    constructor( props ) {
        super( props );
        this.createProject           = this.createProject.bind( this );
        this.editGroup               = this.editGroup.bind( this );
        this.reportGroup             = this.reportGroup.bind( this );
        this.sortProjects            = this.sortProjects.bind( this );
        this.getProjectSortingString = this.getProjectSortingString.bind( this );
    }

    createProject() {
        this.context.router.push( { pathname: `/project/create` } );
    }

    editGroup() {
        this.context.router.push( { pathname: `/group/${this.props.group.uri}/edit` } );
    }

    reportGroup() {
        const { group } = this.props;
        if ( !group ) {
            return;
        }

        ModerationMethods.report.call( { entityTypeName: GroupCollection.entityName, entityId: group._id } );
    }

    sortProjects( order ) {
        sortByStorage.set( order );
    }

    getProjectSortingString( order ) {
        switch ( order ) {
            case 'date':
                return __( 'Date' );
                break;
            case 'trending':
                return __( 'Trending' );
                break;
            case 'top':
                return __( 'Popularity' );
                break;
            default:
                return __( 'Unknown' );
                break;
        }
    }

    render() {
        const { userId, groupLoading, group, groupImage, projectsLoading, projects, sortBy } = this.props;


        if ( groupLoading ) {
            return <Spinner/>;

        } else if ( !group ) {
            return <NotFound/>;

        } else {
            const headerStyle = {
                backgroundColor: groupImage && groupImage.dominantColor ? '#' + groupImage.dominantColor : colorCollection[group.slug.length % colorCollection.length],
                backgroundImage: groupImage ? 'url(' + groupImage.url( { store: groupImage.collectionName + '_large' } ) + ')' : 'none'
            };

            const youReported = group.isReportedBy( Meteor.userId() );

            return (
                <ContainedPage id="group">

                    {/* Header */}
                    <header className="sixteen wide column heading" style={headerStyle}>
                        <h2 className="ui center aligned inverted header">
                            <div className="content">
                                { group.name }
                                <div className="sub header">
                                    <p>{ prune( group.description, 140 ) }</p>
                                </div>
                            </div>
                        </h2>
                    </header>

                    <div className="sixteen wide column">
                        <div className="ui mobile reversed stackable grid">

                            {/* Left Column */}
                            <div className="ten wide column">

                                <div className="ui grid">

                                    {/* Content */}
                                    { group.content && <section id="project-content" className="sixteen wide column">
                                        <div className="ui basic segment" dangerouslySetInnerHTML={{ __html: group.content }}></div>
                                    </section> }

                                    {/* Feed */}
                                    <div className="sixteen wide column feed group-feed">
                                        <h3 className="ui dividing header">{__( 'Activity' )}</h3>
                                        <Feed entities={_.pluck( projects, '_id' )}/>
                                    </div>

                                </div>

                            </div>

                            {/* Feed Last Column */}
                            <div className="six wide column sidebar">

                                {/* Projects */}
                                <div className="ui grid sidebar-block">
                                    <div className="sixteen wide column projects-intro">
                                        {__( 'Projects' )}
                                        <div className="content">
                                            {__( 'Sorted by' )}&nbsp;
                                            <DropDown className="inline">
                                                <div className="text">{this.getProjectSortingString( sortBy )}</div>
                                                <i className="dropdown icon"/>
                                                <div className="menu">
                                                    { ['date', 'trending', 'top'].map( order =>
                                                        <div key={order} className={classNames( "item", { active: sortBy == order } )} onClick={() => this.sortProjects( order )}>{this.getProjectSortingString( order )}</div>
                                                    ) }
                                                </div>
                                            </DropDown>
                                        </div>
                                    </div>
                                    { (() => {
                                        switch ( sortBy ) {
                                            case 'date':
                                                return <RecentProjects group={group}/>;
                                                break;
                                            case 'trending':
                                                return <TrendingProjects group={group}/>;
                                                break;
                                            case 'top':
                                                return <TopProjects group={group}/>;
                                                break;
                                            default:
                                                return <ProjectList group={group}/>;
                                                break;
                                        }
                                    })() }

                                </div>

                                {/* Menu */}
                                <div className="ui fluid secondary vertical menu">
                                    <h3 className="ui dividing header">{__( 'Menu' )}</h3>

                                    { group.isExpert( userId ) &&
                                      <div className="link item" onClick={this.createProject}>
                                          { __( 'Add Project' ) }
                                      </div> }

                                    { group.isModerator( userId ) &&
                                      <div className="link item" onClick={this.editGroup}>
                                          { __( 'Edit Group' ) }
                                      </div> }

                                    <div className="link item" onClick={this.reportGroup}>
                                        { youReported ? __( 'You reported this' ) : __( 'Report' ) }
                                    </div>
                                </div>

                                {/* Members */}
                                { group.showMembers && <MemberList group={group}/> }

                            </div>

                        </div>
                    </div>

                </ContainedPage>
            );
        }
    }
}

Group.propTypes = {
    userId:          PropTypes.string,
    groupLoading:    PropTypes.bool.isRequired,
    group:           PropTypes.object,
    groupImage:      PropTypes.object,
    projectsLoading: PropTypes.bool.isRequired,
    projects:        PropTypes.arrayOf( PropTypes.object ),
    sortBy:          PropTypes.string
};

Group.defaultProps = {
    sortBy: 'date'
};

Group.contextTypes = {
    router: PropTypes.object.isRequired
};

export default ConnectMeteorData( props => {
    const { id } = props.params;
    const group  = GroupCollection.findOne( { moderated: false, $or: [{ _id: id }, { slug: id }] } );
    return {
        userId:          Meteor.userId(),
        groupLoading:    !Meteor.subscribe( 'group', id ).ready(),
        group:           group,
        groupImage:      group ? GroupImagesCollection.findOne( { owners: group._id } ) : null,
        projectsLoading: group ? !Meteor.subscribe( 'project/list', group._id ).ready() : false,
        projects:        group ? ProjectCollection.find( { groups: group._id } ).fetch() : [],
        sortBy:          sortByStorage.get() || 'date',
    };
} )( Group );

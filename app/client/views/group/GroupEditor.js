"use strict";

import React, {Component, PropTypes} from "react";
import ReactDOM from "react-dom";
import classNames from "classnames";
import async from "async";
import {unflatten} from "flat";
import {FS} from "meteor/cfs:base-package";
import formDiff from "/imports/utils/formDiff";
import GroupCollection from "/imports/groups/GroupCollection";
import GroupMethods from "/imports/groups/GroupMethods";
import GroupImagesCollection from "/imports/groups/GroupImagesCollection";
import GroupEditorDataWrapper from "./GroupEditorDataWrapper";
import Modal from "../../components/semanticui/modules/Modal";
import Form from "../../components/forms/Form";
import GroupForm from "./GroupForm";

const { File } = FS;

export class GroupEditor extends Component {

    constructor( props ) {
        super( props );

        this.confirmConversationClear   = this.confirmConversationClear.bind( this );
        this.cancelConversationClear    = this.cancelConversationClear.bind( this );
        this.clearConversation          = this.clearConversation.bind( this );
        this.confirmRemove      = this.confirmRemove.bind( this );
        this.cancelRemove       = this.cancelRemove.bind( this );
        this.remove             = this.remove.bind( this );
        this.pictureChanged     = this.pictureChanged.bind( this );
        this.permissionsChanged = this.permissionsChanged.bind( this );
        this.updateGroup        = this.updateGroup.bind( this );
        this.submitForm         = this.submitForm.bind( this );
        this.onCancel           = this.onCancel.bind( this );
        this.onFinish           = this.onFinish.bind( this );

        this.state = {
            error:               null,
            saving:              null,
            picture:             null,
            confirmingRemoval:   false,
            confirmingConversationClear: false
        }
    }

    pictureChanged( files ) {
        this.setState( {
            picture: ( files && files.length > 0 ) ? files[0] : null
        } );
    }

    permissionsChanged( permissions ) {
        this.setState( { permissions } );
    }

    confirmRemove( entity ) {
        this.setState( {
            confirmingRemoval: true
        } );
    }

    cancelRemove() {
        this.setState( {
            confirmingRemoval: false
        } );
    }

    remove() {
        this.setState( {
            confirmingRemoval: false
        } );

        GroupMethods.remove.call( { groupId: this.props.group._id }, ( err ) => {
            if ( err ) {
                this.setState( { error: err } );
            } else {
                this.onFinish( true );
            }
        } );
    }

    confirmConversationClear() {
        this.setState( {
            confirmingConversationClear: true
        } );
    }

    cancelConversationClear() {
        this.setState( {
            confirmingConversationClear: false
        } );
    }

    clearConversation() {
        this.setState( {
            confirmingConversationClear: false
        } );

        GroupMethods.clearConversation.call( { groupId: this.props.group._id }, ( err ) => {
            if ( err ) {
                this.setState( { error: err } );
            }
        } );
    }

    submitForm() {
        $( ReactDOM.findDOMNode( this.refs.groupEditForm ) ).form( 'submit' );
    }

    updateGroup( values ) {
        if ( this.state.saving ) {
            return;
        }
        this.setState( { saving: true } );

        const { group }       = this.props;
        const { permissions } = this.state;

        let uploads = [];
        let tasks   = [];

        // Images
        if ( this.state.picture ) {
            let file    = new File( this.state.picture );
            file.owners = [group._id];
            uploads.push( callback => GroupImagesCollection.insert( file, callback ) );
        }

        // Push upload stack as a task
        tasks.push( callback => async.parallelLimit( uploads, 3, callback ) );

        // Data

        // Get boolean value instead of the "on" string
        values.public      = !!values.public;
        values.showMembers = !!values.showMembers;
        // Turn the form values into a proper object and compute diff
        const changes      = formDiff( unflatten( values ), group );

        // Call update if we have changes or uploads pending
        if ( !_.isEmpty( changes ) || permissions.length || uploads.length ) {
            tasks.push( callback => GroupMethods.update.call( {
                _id: group._id,
                     changes,
                     permissions
            }, callback ) );
        }

        // Do tasks
        async.series( tasks, ( err, result ) => {
            this.setState( { saving: false } );
            if ( err ) {
                console.error( err );
                this.setState( { error: err } );
            } else {
                this.onFinish();
            }
        } );
    }

    onCancel() {
        this.onFinish();
    }

    onFinish( deleted = false ) {
        if ( this.props.onFinished ) {
            this.props.onFinished();
        } else if ( deleted ) {
            this.context.router.push( { pathname: '/' } );
        } else {
            // Retrieve the group as we might have made changes to the uri and the props is outdated
            const group = GroupCollection.findOne( { _id: this.props.group._id } );
            if ( group ) {
                this.context.router.push( { pathname: `/group/${group.uri}` } );
            } else {
                this.context.router.goBack();
            }
        }
    }

    renderLoading() {
        return (
            <div className="card details loading">
                <div className="ui large inverted active indeterminate text loader">
                    {__( 'Loading Group' )}
                </div>
            </div>
        );
    }

    renderGroup() {
        const { group, image }                                          = this.props;
        const { error, saving, confirmingRemoval, confirmingConversationClear } = this.state;

        if ( !group ) {
            return null;
        }

        return (
            <div className={classNames( this.props.className, "editor editGroup" )}>

                <header className="ui clearing basic segment">
                    <h2 className="ui left floated header">
                        {__( 'Edit Group' )}
                    </h2>
                    <div className="ui right floated secondary menu">
                        <div className="ui blue button" onClick={this.confirmConversationClear}>
                            <i className="trash icon"/>
                            {__( 'Clear group conversation history' )}
                        </div>
                        <div className="ui red button" onClick={this.confirmRemove}>
                            <i className="trash icon"/>
                            {__( 'Remove' )}
                        </div>
                        <div className="ui buttons">
                            <div className="ui cancel button" onClick={this.onCancel}>
                                <i className="remove icon"/>
                                {__( 'Cancel' )}
                            </div>
                            <div className={classNames( "ui green button", { loading: saving, disabled: saving } )}
                                 onClick={this.submitForm}>
                                <i className="plus icon"/>
                                {__( 'Save' )}
                            </div>
                        </div>
                    </div>
                </header>

                <Form id={"group-edit-form-" + group._id}
                      ref="groupEditForm"
                      error={error}
                      onSuccess={this.updateGroup}>

                    <GroupForm group={group}
                               image={image}
                               permissionsChanged={this.permissionsChanged}
                               pictureChanged={this.pictureChanged}/>
                </Form>

                <div className="actions">
                    <div className="ui red button" onClick={this.confirmRemove}>
                        <i className="trash icon"/>
                        {__( 'Remove' )}
                    </div>
                    <div className="ui buttons">
                        <div className="ui cancel button" onClick={this.onCancel}>
                            <i className="remove icon"/>
                            {__( 'Cancel' )}
                        </div>
                        <div className={classNames( "ui green button", { loading: saving, disabled: saving } )}
                             onClick={this.submitForm}>
                            <i className="plus icon"/>
                            {__( 'Save' )}
                        </div>
                    </div>
                </div>

                <Modal
                    opened={ confirmingRemoval }
                    onApprove={ this.remove }
                    onHide={ this.cancelRemove }>
                    <i className="close icon"/>
                    <div className="header">
                        { __( 'Confirm Removal' ) }
                    </div>
                    <div className="content">
                        <div className="description">
                            <p>{__( 'Are you sure you want to remove the group? All projects in the group, and their cards will be permanently deleted.' )}</p>
                        </div>
                    </div>
                    <div className="actions">
                        <div className="ui buttons">
                            <div className="ui red deny button">
                                <i className="remove icon"/>
                                {__( 'No' )}
                            </div>
                            <div className="ui green approve button">
                                <i className="checkmark icon"/>
                                {__( 'Yes' )}
                            </div>
                        </div>
                    </div>
                </Modal>

                <Modal
                    opened={ confirmingConversationClear }
                    onApprove={ this.clearConversation }
                    onHide={ this.cancelConversationClear }>
                    <i className="close icon"/>
                    <div className="header">
                        { __( 'Confirm Clearing' ) }
                    </div>
                    <div className="content">
                        <div className="description">
                            <p>{__( "Are you sure you want to clear this group's conversation history? All messages will be permanently deleted." )}</p>
                        </div>
                    </div>
                    <div className="actions">
                        <div className="ui buttons">
                            <div className="ui red deny button">
                                <i className="remove icon"/>
                                {__( 'No' )}
                            </div>
                            <div className="ui green approve button">
                                <i className="checkmark icon"/>
                                {__( 'Yes' )}
                            </div>
                        </div>
                    </div>
                </Modal>
            </div>
        );
    }

    render() {
        const { loading } = this.props;
        return loading ? this.renderLoading() : this.renderGroup();
    }
}

GroupEditor.propTypes = {
    loading: PropTypes.bool.isRequired,
    group:   PropTypes.object,
    image:   PropTypes.object
};

GroupEditor.contextTypes = {
    router: PropTypes.object.isRequired
};

export default GroupEditorDataWrapper( GroupEditor );
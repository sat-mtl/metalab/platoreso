"use strict";

import React, {Component, PropTypes} from "react";
import ContainedPage from '../layouts/ContainedPage';
import Spinner from '../widgets/Spinner';
import NotFound from '../errors/NotFound';
import {GroupEditor} from './GroupEditor';
import GroupEditorDataWrapper from './GroupEditorDataWrapper';

export const GroupEdit = ( props ) => {
    if ( props.loading ) {
        return <Spinner/>;

    } else if ( !props.group ) {
        return <NotFound/>;

    } else {
        return (
            <ContainedPage>
                <GroupEditor className="standalone sixteen wide column" {...props}/>
            </ContainedPage>
        );
    }
};

const DataWrapper = GroupEditorDataWrapper( GroupEdit );
export default ( { params } ) => <DataWrapper groupId={params.id}/>;
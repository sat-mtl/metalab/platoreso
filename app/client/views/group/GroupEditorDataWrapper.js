"use strict";

import GroupCollection from "/imports/groups/GroupCollection";
import {ConnectMeteorData} from '../../lib/ReactMeteorData'

/**
 * Meteor Data Wrapper
 */
export default ConnectMeteorData( props => {
    const groupLoading = !!( props.groupId && !Meteor.subscribe( 'group', props.groupId ).ready() );
    const group        = props.groupId ? GroupCollection.findOne( { $or: [{ _id: props.groupId }, { slug: props.groupId }] } ) : null;
    return {
        loading: groupLoading,
        group:   group,
        image:   group && group.getImage()
    }
} );
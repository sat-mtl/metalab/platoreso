"use strict";

import React, {Component, PropTypes} from "react";
import GroupList from './common/GroupList';

export default () => (
    <div className="ui padded grid">
        <section className="column">
            <header className="ui clearing basic segment">
                <h2 className="ui left floated header">
                    {__( 'Groups' )}
                    <div className="sub header">{__( 'Group List.' )}</div>
                </h2>
            </header>
            <div className="ui hidden divider"></div>
            <GroupList/>
        </section>
    </div>
);
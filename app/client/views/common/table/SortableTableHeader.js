"use strict";

import React, { Component, PropTypes } from "react";
import classNames from "classnames";

export default class SortableTableHeader extends Component {

    /**
     * Get Sort classes for the header
     *
     * @param {String} field
     * @param {String} orderBy
     * @param {Number} order
     * @param {String} [extra]
     * @returns {String}
     */
    static sortClasses( field, orderBy, order, extra = '' ) {
        return classNames( { sorted: field == orderBy, ascending: order == 1, descending: order == -1 }, extra );
    }

    constructor(props) {
        super(props);
        this.onSort = this.onSort.bind(this);
    }

    onSort() {
        this.props.onSort( this.props.field, this.props.order * -1 );
    }

    render() {
        const { field, orderBy, order, onSort, className, children, ...rest } = this.props;
        return (
            <th {...rest} className={SortableTableHeader.sortClasses(field, orderBy, order, className)} onClick={this.onSort}>
                {children}
            </th>
        );
    }

}

SortableTableHeader.propTypes = {
    field:   PropTypes.string.isRequired,
    orderBy: PropTypes.string.isRequired,
    order:   PropTypes.number.isRequired,
    onSort:  PropTypes.func.isRequired
};
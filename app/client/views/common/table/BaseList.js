"use strict";

import React, { Component, PropTypes } from "react";
import Modal from "../../../components/semanticui/modules/Modal";
import SortableTable from "./SortableTable";
import SortableTableHeader from "./SortableTableHeader";

export const PAGE_SIZE  = 100;
export const BASE_LIMIT = PAGE_SIZE;

export default class BaseList extends Component {

    constructor( props ) {
        super( props );

        this.tableComponent = BaseTable;

        this.changePage    = this.changePage.bind( this );
        this.sort          = this.sort.bind( this );
        this.onSelect      = this.onSelect.bind( this );
        this.onEdit        = this.onEdit.bind( this );
        this.confirmRemove = this.confirmRemove.bind( this );
        this.cancelRemove  = this.cancelRemove.bind( this );
        this._remove       = this._remove.bind( this );
        this.remove        = this.remove.bind( this );
        this.search        = this.search.bind( this );

        this.getHeadersFromProps = this.getHeadersFromProps.bind( this );
        this.renderRow           = this.renderRow.bind( this );
        this.renderRemoveModal   = this.renderRemoveModal.bind( this );

        this.state = {
            selectedEntity:          null,
            confirmingEntityRemoval: false,
            orderBy:                 'name_sort',
            order:                   1,
            subscriptionLimit:       BASE_LIMIT,
            localLimit:              BASE_LIMIT,
            page:                    0,
            headers:                 this.getHeadersFromProps( props )
        }
    }

    componentWillReceiveProps( nextProps ) {
        this.setState( {
            headers: this.getHeadersFromProps( nextProps )
        } );
    }

    changePage( page ) {
        this.setState( {
            //subscriptionLimit: (page + 1) * PAGE_SIZE,
            page: page
        } );
    }

    sort( orderBy, order ) {
        this.setState( { orderBy: orderBy, order: order } );
    }

    search( event ) {
        this.setState( { search: event.target.value } );
    }

    onSelect( entity ) {
        this.setState( { selectedEntity: entity } );
    }

    onEdit( entity ) {
        //noop
    }

    confirmRemove( entity ) {
        this.setState( {
            confirmingEntityRemoval: true,
            selectedEntity:          entity
        } );
    }

    cancelRemove() {
        this.setState( {
            confirmingEntityRemoval: false
        } );
    }

    _remove() {
        this.setState( {
            confirmingEntityRemoval: false
        } );

        const { selectedEntity } = this.state;
        if ( !selectedEntity ) {
            return;
        }

        this.remove( selectedEntity );

        this.setState( {
            selectedEntity: null
        } );
    }

    remove() {
        //noop
    }

    getHeadersFromProps( props ) {
        return [];
    }

    renderRow( row ) {
        return null;
    }

    renderRemoveModal( selectedEntity ) {
        return (
            <div className="content">
                <div className="description">
                    <p>{__( 'Are you sure you want to remove the selected item?' )}</p>
                </div>
            </div>
        )
    };

    render() {
        const { page, orderBy, order, search, headers, selectedEntity, confirmingEntityRemoval } = this.state;

        const TableComponent = this.tableComponent;

        const basic = {
            sort: { [orderBy]: order || 1 }
        };

        const options = {
            subscription: _.extend( {
                limit: PAGE_SIZE,//subscriptionLimit,
                skip:  page * PAGE_SIZE
            }, basic ),
            local:        _.extend( {
                limit: PAGE_SIZE,//localLimit,
                //skip: page * PAGE_SIZE
            }, basic )
        };

        return (
            <div className="ui grid">
                {/*
                    We have to pass props to the table component because of how the lists are structured
                    For example, moderation lists need this to pass props to the list
                 */}
                <TableComponent {...this.props}
                                page={page}
                                search={search}
                                options={options}
                                orderBy={orderBy}
                                order={order}
                                headers={headers}
                                onSearch={this.search}
                                onPageChanged={this.changePage}
                                onSort={this.sort}
                                renderRow={this.renderRow}/>


                <Modal
                    opened={ confirmingEntityRemoval }
                    onApprove={ this._remove }
                    onHide={ this.cancelRemove }>
                    <i className="close icon"/>
                    <div className="header">
                        { __( 'Confirm Removal' ) }
                    </div>
                    { selectedEntity ? this.renderRemoveModal( selectedEntity ) : null }
                    <div className="actions">
                        <div className="ui buttons">
                            <div className="ui red deny button">
                                <i className="remove icon"/>
                                {__( 'No' )}
                            </div>
                            <div className="ui green approve button">
                                <i className="checkmark icon"/>
                                {__( 'Yes' )}
                            </div>
                        </div>
                    </div>
                </Modal>
            </div>
        );
    }

}

export class BaseTable extends Component {
    render() {
        const { page, search, orderBy, order, headers, onSearch, onSort, onPageChanged, renderRow } = this.props;
        const { rows, total, loading }                                                              = this.props;

        return (
            <SortableTable page={page} pageSize={PAGE_SIZE} total={total} loading={loading} search={search} onSearch={onSearch} onPageChanged={onPageChanged}>
                <thead>
                <tr>
                    { headers.map( header => {
                        const { field, label, ...props } = header;
                        return field
                            ? (
                                   <SortableTableHeader {...props} key={field} field={field} orderBy={orderBy} order={order} onSort={onSort}>
                                       {label}
                                   </SortableTableHeader>
                               )
                            : (
                                   <th key={label} {...props}>{label}</th>
                               );
                    } ) }
                    <th className="right aligned">{__( 'Actions' )}</th>
                </tr>
                </thead>
                <tbody>
                { rows.length > 0
                    ? rows.map( row => renderRow( row ) )
                    : <tr>
                      <td colSpan={headers.reduce( ( count, header ) => count + (header.colSpan ? header.colSpan : 1), 1 /* Count actions */ )} className="center aligned empty disabled">{__( 'The list is empty' )}</td>
                  </tr>
                }
                </tbody>
            </SortableTable>
        );
    }
}
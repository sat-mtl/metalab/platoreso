"use strict";

import React, { Component, PropTypes } from "react";
import ReactDOM from "react-dom";

export default class BaseRow extends Component {

    constructor(props) {
        super(props);

        this.checkViewport = this.checkViewport.bind(this);
        this.isInViewport = this.isInViewport.bind(this);
        this.select = this.select.bind(this);
        this.edit = this.edit.bind(this);
        this.remove = this.remove.bind(this);

        this.state = {
            visible: false,
            hasBeenVisible: false
        };
    }

    componentDidMount() {
        $( '.button', ReactDOM.findDOMNode( this ) ).popup({
            variation: "tiny inverted"
        });
        this.node = ReactDOM.findDOMNode( this );
        window.addEventListener( 'scroll', this.checkViewport );
        this.interval = setInterval( this.checkViewport, 50 );
    }

    shouldComponentUpdate(nextProps, nextState) {
        if ( document.hidden ) {
            return false;
        } else if ( this.state.visible != nextState.visible ) {
            return true;
        } else {
            return nextState.visible;
        }
    }

    componentWillUnmount() {
        $( '.button', ReactDOM.findDOMNode( this ) ).popup('destroy');
        window.removeEventListener( 'scroll', this.checkViewport );
        clearInterval( this.interval );
    }

    checkViewport() {
        const visible = this.isInViewport();
        if ( visible && !this.state.visible ) {
            this.setState({visible: true, hasBeenVisible: true});
        } else if ( !visible && this.state.visible ) {
            this.setState({visible: false});
        }
    }

    isInViewport() {
        const rect     = this.node.getBoundingClientRect();
        const vpHeight = window.innerHeight || document.documentElement.clientHeight;
        const vpWidth  = window.innerWidth || document.documentElement.clientWidth;

        return ( rect.bottom >= 0 && rect.right >= 0 && rect.top <= vpHeight && rect.left <= vpWidth );
    }

    select() {
        this.props.onSelect(this.props.entity);
    }

    edit() {
        this.props.onEdit(this.props.entity);
    }

    remove() {
        this.props.onRemove(this.props.entity);
    }
}

BaseRow.propTypes = {
    entity: PropTypes.object.isRequired,
    selected: PropTypes.bool,
    onSelect: PropTypes.func.isRequired,
    onEdit: PropTypes.func.isRequired,
    onRemove: PropTypes.func.isRequired
};

BaseRow.defaultProps = {
    selected: false
};
"use strict";

import React, {Component, PropTypes} from "react";
import classNames from "classnames";
import DropDown from "../../../components/semanticui/modules/Dropdown";

export default class SortableTable extends Component {

    constructor( props ) {
        super( props );
        this.previousPage = this.previousPage.bind( this );
        this.nextPage     = this.nextPage.bind( this );
    }

    previousPage() {
        if ( this.props.page > 0 ) {
            this.props.onPageChanged( this.props.page - 1 );
        }
    }

    nextPage() {
        this.props.onPageChanged( this.props.page + 1 )
    }

    render() {
        const { page, pageSize, total, loading, search, onSearch } = this.props;

        const pageCount = total > 0 ? Math.ceil( total / pageSize ) : 1;

        return (
            <div className="sortable table row">
                <div className="sixteen wide column">
                    <div className="ui secondary menu">
                        <DropDown className="scrolling item">
                            {__( 'Filter' )}
                            <i className="dropdown icon"/>
                            <div className="menu">
                                <div className="item">Not implemented</div>
                            </div>
                        </DropDown>
                        <div className="right menu">
                            <div className="search item">
                                <div className="ui transparent icon input">
                                    <input type="text" placeholder="Search..." value={search || ''} onChange={onSearch}/>
                                    <i className="search link icon"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="sixteen wide column">
                    <table className="ui very basic small selectable sortable table">
                        {this.props.children}
                    </table>
                </div>

                <div className="sixteen wide center aligned column pager">
                    <div className="ui mini buttons">
                        <button className={classNames( "ui left labeled icon button", { disabled: page == 0 } ) } onClick={this.previousPage}>
                            <i className="left arrow icon"/>
                            {__( 'Previous' )}
                        </button>
                        <div className={classNames( 'ui disabled button', { loading: loading } )}>
                            {__( 'Page __pageNum__ of __pageCount__ (__itemCount__ items)', {
                                pageNum:   page + 1,
                                pageCount: pageCount,
                                itemCount: total
                            } )}
                        </div>
                        <button className={classNames( "ui right labeled icon button", { disabled: page + 1 == pageCount } ) } onClick={this.nextPage}>
                            <i className="right arrow icon"/>
                            {__( 'Next' )}
                        </button>
                    </div>
                </div>

            </div>
        );
    }

}

SortableTable.propTypes = {
    page:          PropTypes.number.isRequired,
    pageSize:      PropTypes.number.isRequired,
    total:         PropTypes.number.isRequired,
    loading:       PropTypes.bool.isRequired,
    onPageChanged: PropTypes.func.isRequired,
    search:        PropTypes.string,
    onSearch:      PropTypes.func.isRequired
};

SortableTable.defaultProps = {
    search: ''
};
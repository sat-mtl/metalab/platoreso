"use strict";

import React, { Component, PropTypes } from "react";
import classNames from "classnames";
import { unflatten } from 'flat';
import formDiff from "../../../imports/utils/formDiff";

import SettingsCollection from "../../../imports/settings/SettingsCollection";
import SettingsMethods from "../../../imports/settings/SettingsMethods";
import { ConnectMeteorData } from '../../lib/ReactMeteorData';

import Spinner from '../widgets/Spinner';
import NotFound from '../errors/NotFound';

import Form from '../../components/forms/Form';
import Field from '../../components/forms/Field';
import Input from '../../components/forms/Input';

export class Gamification extends Component {

    constructor( props ) {
        super( props );

        this.updateSettings = this.updateSettings.bind( this );

        this.state = {
            error:  null,
            saving: false
        }
    }

    updateSettings( values ) {
        if ( this.state.saving ) {
            return;
        }
        this.setState( { saving: true } );

        // Turn the form values into a proper object and compute diff
        //NOTE: This diff thing doesn't really work here because numbers are turned into strings by semantic-ui
        const changes = formDiff( unflatten( values ), this.props.settings );
        SettingsMethods.update.call( changes, err => {
            if ( err ) {
                this.setState( { error: err } );
            }
            this.setState( { saving: false } );
        } );
    }


    render() {
        const { loading, settings } = this.props;
        const { saving, error }     = this.state;

        if ( loading ) {
            return <Spinner/>;

        } else if ( !settings ) {
            return <NotFound/>;
        } else {
            return (
                <section id="settings" className="sixteen wide column settings">
                    <header className="ui clearing basic segment">
                        <h2 className="ui header">
                            {__( 'Gamification Settings' )}
                            <div className="sub header">{__( 'Configure how many points are given to a user for various actions on the platform.' )}</div>
                        </h2>
                    </header>

                    <Form id="gamification-settings-form" ref="gamificationSettingsForm"
                          error={error}
                          onSuccess={this.updateSettings}>

                        <div className="ui stackable padded grid">

                            <div className="eight wide column">
                                <h3 className="ui header">
                                    {__( 'Card Actions' )}
                                    <div className="sub header">{__( 'Points given for actions on a card.' )}</div>
                                </h3>
                                <div className="three fields">

                                    <Field label={__( "Creation" )}>
                                        <Input ref="shared.gamification.points.card.creation" name="shared.gamification.points.card.creation"
                                               defaultValue={settings.shared.gamification.points.card.creation}
                                               rules={[
                                                   { type: 'empty', prompt: __( 'Please enter a value' ) },
                                                   { type: 'integer[0..100]', prompt: __( 'Please enter a number between 0 and 100' ) }
                                               ]}/>
                                    </Field>

                                    <Field label={__( "Like" )}>
                                        <Input ref="shared.gamification.points.card.like" name="shared.gamification.points.card.like"
                                               defaultValue={settings.shared.gamification.points.card.like}
                                               rules={[
                                                   { type: 'empty', prompt: __( 'Please enter a value' ) },
                                                   { type: 'integer[0..100]', prompt: __( 'Please enter a number between 0 and 100' ) }
                                               ]}/>
                                    </Field>

                                    <Field label={__( "Comment" )}>
                                        <Input ref="shared.gamification.points.card.comment" name="shared.gamification.points.card.comment"
                                               defaultValue={settings.shared.gamification.points.card.comment}
                                               rules={[
                                                   { type: 'empty', prompt: __( 'Please enter a value' ) },
                                                   { type: 'integer[0..100]', prompt: __( 'Please enter a number between 0 and 100' ) }
                                               ]}/>
                                    </Field>

                                </div>
                                <div className="three fields">

                                    <Field label={__( "Link" )}>
                                        <Input ref="shared.gamification.points.card.link" name="shared.gamification.points.card.link"
                                               defaultValue={settings.shared.gamification.points.card.link}
                                               rules={[
                                                   { type: 'empty', prompt: __( 'Please enter a value' ) },
                                                   { type: 'integer[0..100]', prompt: __( 'Please enter a number between 0 and 100' ) }
                                               ]}/>
                                    </Field>

                                    <Field label={__( "Move" )}>
                                        <Input ref="shared.gamification.points.card.move" name="shared.gamification.points.card.move"
                                               defaultValue={settings.shared.gamification.points.card.move}
                                               rules={[
                                                   { type: 'empty', prompt: __( 'Please enter a value' ) },
                                                   { type: 'integer[0..100]', prompt: __( 'Please enter a number between 0 and 100' ) }
                                               ]}/>
                                    </Field>

                                </div>
                            </div>

                            <div className="eight wide column">
                                <h3 className="ui header">
                                    {__( 'Comment Actions' )}
                                    <div className="sub header">{__( 'Points given for actions on a comment.' )}</div>
                                </h3>
                                <Field label={__( "Like" )}>
                                    <Input ref="shared.gamification.points.comment.like" name="shared.gamification.points.comment.like"
                                           defaultValue={settings.shared.gamification.points.comment.like}
                                           rules={[
                                               { type: 'empty', prompt: __( 'Please enter a value' ) },
                                               { type: 'integer[0..100]', prompt: __( 'Please enter a number between 0 and 100' ) }
                                           ]}/>
                                </Field>
                            </div>

                            {/*<div className="eight wide column">

                             <h3 className="ui header">
                             {__( 'Card "Cooldown"' )}
                             <div className="sub header">{__( 'Number of "heat" points lost every hour. Use any floating point number between 0 and 100 inclusively. Temperature will never go below 0.' )}</div>
                             </h3>
                             <Field label={__( "Overall" )}>
                             <Input ref="shared.analytics.card.cooldown" name="shared.analytics.card.cooldown"
                             defaultValue={settings.shared.analytics.card.cooldown}
                             rules={[
                             { type: 'empty', prompt: __( 'Please enter a value' ) },
                             { type: 'number[0..100]', prompt: __( 'Please enter a number between 0 and 100' ) }
                             ]}/>
                             </Field>
                             </div>*/}

                            <div className="sixteen wide column">
                                <div className="ui divider"></div>
                                <div className={classNames( "ui green large right labeled icon submit button", { loading: saving, disabled: saving } )}>
                                    <i className="save icon"/>
                                    {__( 'Save' )}
                                </div>
                            </div>

                        </div>

                    </Form>

                </section>
            );
        }
    }
}

Gamification.propTypes = {
    loading:  PropTypes.bool,
    settings: PropTypes.object
};

export default ConnectMeteorData( props => ({
    loading:  !Meteor.subscribe( 'admin/settings' ).ready(),
    settings: SettingsCollection.findOne( {} )
}) )( Gamification );
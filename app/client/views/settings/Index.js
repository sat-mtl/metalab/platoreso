"use strict";

import React, { Component, PropTypes } from "react";

export default class Index extends Component {

    render() {
        return (
            <section id="settings" className="sixteen wide column settings">
                <header className="ui clearing basic segment">
                    <h2 className="ui header">
                        {__( 'Settings' )}
                        <div className="sub header">{__( 'Server and site-wide configuration' )}</div>
                    </h2>
                </header>
            </section>
        );
    }
}

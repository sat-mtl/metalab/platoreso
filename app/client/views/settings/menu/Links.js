"use strict";

export default [
    {
        to: '/settings/',
        get label() { return __('General'); },
        icon: 'setting',
        index: true,
        enabled: true
    },
    {
        to: '/settings/analytics',
        get label() { return __('Analytics'); },
        icon: 'line chart',
        index: false,
        enabled: true
    },
    {
        to: '/settings/gamification',
            get label() { return __('Gamification'); },
        icon: 'game',
        index: false,
        enabled: true
    }
];
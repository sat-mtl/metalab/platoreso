"use strict";

import React, { Component, PropTypes } from "react";
import classNames from "classnames";
import { unflatten } from 'flat';
import formDiff from "../../../imports/utils/formDiff";

import SettingsCollection from "../../../imports/settings/SettingsCollection";
import SettingsMethods from "../../../imports/settings/SettingsMethods";
import { ConnectMeteorData } from '../../lib/ReactMeteorData';

import Spinner from '../widgets/Spinner';
import NotFound from '../errors/NotFound';

import Form from '../../components/forms/Form';
import Field from '../../components/forms/Field';
import Input from '../../components/forms/Input';

export class Analytics extends Component {

    constructor( props ) {
        super( props );

        this.updateSettings = this.updateSettings.bind( this );

        this.state = {
            error:  null,
            saving: false
        }
    }

    updateSettings( values ) {
        if ( this.state.saving ) {
            return;
        }
        this.setState( { saving: true } );

        // Turn the form values into a proper object and compute diff
        //NOTE: This diff thing doesn't really work here because numbers are turned into strings by semantic-ui
        const changes = formDiff( unflatten( values ), this.props.settings );
        SettingsMethods.update.call( changes, err => {
            if ( err ) {
                this.setState( { error: err } );
            }
            this.setState( { saving: false } );
        } );
    }


    render() {
        const { loading, settings } = this.props;
        const { saving, error } = this.state;

        if ( loading ) {
            return <Spinner/>;

        } else if ( !settings ) {
            return <NotFound/>;
        } else {
            return (
                <section id="settings" className="sixteen wide column settings">
                    <header className="ui clearing basic segment">
                        <h2 className="ui header">
                            {__( 'Analytics Settings' )}
                            <div className="sub header">{__( 'Analytics configuration' )}</div>
                        </h2>
                    </header>

                    <Form id="analytics-settings-form" ref="analyticsSettingsForm"
                          error={error}
                          onSuccess={this.updateSettings}>

                        <div className="ui stackable padded grid">

                            <div className="eight wide column">
                                <h3 className="ui header">
                                    {__( 'Card "Heat"' )}
                                    <div className="sub header">{__( 'Number of "heat" points added to a card for each actions made on it. In order to keep people from cheating, points will not be given to a card if the person doing the action is the card author. Use a value between 0 and 100 inclusively.' )}</div>
                                </h3>
                                <div className="two fields">

                                    <Field label={__( "Creation" )}>
                                        <Input ref="shared.analytics.card.heat.creation" name="shared.analytics.card.heat.creation"
                                               defaultValue={settings.shared.analytics.card.heat.creation}
                                               rules={[
                                                   { type: 'empty', prompt: __( 'Please enter a value' ) },
                                                   { type: 'number[0..100]', prompt: __( 'Please enter a number between 0 and 100' ) }
                                               ]}/>
                                    </Field>

                                    <Field label={__( "Update" )}>
                                        <Input ref="shared.analytics.card.heat.update" name="shared.analytics.card.heat.update"
                                               defaultValue={settings.shared.analytics.card.heat.update}
                                               rules={[
                                                   { type: 'empty', prompt: __( 'Please enter a value' ) },
                                                   { type: 'number[0..100]', prompt: __( 'Please enter a number between 0 and 100' ) }
                                               ]}/>
                                    </Field>

                                </div>
                                <div className="three fields">

                                    <Field label={__( "View" )}>
                                        <Input ref="shared.analytics.card.heat.view" name="shared.analytics.card.heat.view"
                                               defaultValue={settings.shared.analytics.card.heat.view}
                                               rules={[
                                                   { type: 'empty', prompt: __( 'Please enter a value' ) },
                                                   { type: 'number[0..100]', prompt: __( 'Please enter a number between 0 and 100' ) }
                                               ]}/>
                                    </Field>

                                    <Field label={__( "Like" )}>
                                        <Input ref="shared.analytics.card.heat.like" name="shared.analytics.card.heat.like"
                                               defaultValue={settings.shared.analytics.card.heat.like}
                                               rules={[
                                                   { type: 'empty', prompt: __( 'Please enter a value' ) },
                                                   { type: 'number[0..100]', prompt: __( 'Please enter a number between 0 and 100' ) }
                                               ]}/>
                                    </Field>

                                    <Field label={__( "Comment" )}>
                                        <Input ref="shared.analytics.card.heat.comment" name="shared.analytics.card.heat.comment"
                                               defaultValue={settings.shared.analytics.card.heat.comment}
                                               rules={[
                                                   { type: 'empty', prompt: __( 'Please enter a value' ) },
                                                   { type: 'number[0..100]', prompt: __( 'Please enter a number between 0 and 100' ) }
                                               ]}/>
                                    </Field>

                                </div>
                                <div className="three fields">

                                    <Field label={__( "Link (source)" )}>
                                        <Input ref="shared.analytics.card.heat.linkSource" name="shared.analytics.card.heat.linkSource"
                                               defaultValue={settings.shared.analytics.card.heat.linkSource}
                                               rules={[
                                                   { type: 'empty', prompt: __( 'Please enter a value' ) },
                                                   { type: 'number[0..100]', prompt: __( 'Please enter a number between 0 and 100' ) }
                                               ]}/>
                                    </Field>

                                    <Field label={__( "Link (destination)" )}>
                                        <Input ref="shared.analytics.card.heat.linkDestination" name="shared.analytics.card.heat.linkDestination"
                                               defaultValue={settings.shared.analytics.card.heat.linkDestination}
                                               rules={[
                                                   { type: 'empty', prompt: __( 'Please enter a value' ) },
                                                   { type: 'number[0..100]', prompt: __( 'Please enter a number between 0 and 100' ) }
                                               ]}/>
                                    </Field>

                                    <Field label={__( "Move" )}>
                                        <Input ref="shared.analytics.card.heat.move" name="shared.analytics.card.heat.move"
                                               defaultValue={settings.shared.analytics.card.heat.move}
                                               rules={[
                                                   { type: 'empty', prompt: __( 'Please enter a value' ) },
                                                   { type: 'number[0..100]', prompt: __( 'Please enter a number between 0 and 100' ) }
                                               ]}/>
                                    </Field>

                                </div>
                            </div>

                            <div className="eight wide column">

                                <h3 className="ui header">
                                    {__( 'Card "Cooldown"' )}
                                    <div className="sub header">{__( 'Number of "heat" points lost every hour. Use any floating point number between 0 and 100 inclusively. Temperature will never go below 0.' )}</div>
                                </h3>
                                <Field label={__( "Overall" )}>
                                    <Input ref="shared.analytics.card.cooldown" name="shared.analytics.card.cooldown"
                                           defaultValue={settings.shared.analytics.card.cooldown}
                                           rules={[
                                               { type: 'empty', prompt: __( 'Please enter a value' ) },
                                               { type: 'number[0..100]', prompt: __( 'Please enter a number between 0 and 100' ) }
                                           ]}/>
                                </Field>

                                <h3 className="ui header">
                                    {__( 'Project "Cooldown"' )}
                                    <div className="sub header">{__( 'Number of "heat" points lost every hour. Use any floating point number between 0 and 100 inclusively. Temperature will never go below 0.' )}</div>
                                </h3>
                                <Field label={__( "Overall" )}>
                                    <Input ref="shared.analytics.project.cooldown" name="shared.analytics.project.cooldown"
                                           defaultValue={settings.shared.analytics.project.cooldown}
                                           rules={[
                                               { type: 'empty', prompt: __( 'Please enter a value' ) },
                                               { type: 'number[0..100]', prompt: __( 'Please enter a number between 0 and 100' ) }
                                           ]}/>
                                </Field>
                            </div>

                            <div className="sixteen wide column">
                                <div className="ui divider"></div>
                                <div className={classNames( "ui green large right labeled icon submit button", { loading: saving, disabled: saving } )}>
                                    <i className="save icon"/>
                                    {__( 'Save' )}
                                </div>
                            </div>

                        </div>

                    </Form>

                </section>
            );
        }
    }
}

Analytics.propTypes = {
    loading:  PropTypes.bool,
    settings: PropTypes.object
};

export default ConnectMeteorData( props => ({
    loading:  !Meteor.subscribe( 'admin/settings' ).ready(),
    settings: SettingsCollection.findOne( {} )
}) )( Analytics );
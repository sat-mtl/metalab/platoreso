"use strict";

import {Link} from 'react-router';
import links from './menu/Links';

import React, { Component, PropTypes } from "react";
import classNames from "classnames";

export default class Menu extends Component {

    render() {
        return (
            <nav id="moderationMenu" className="ui inverted vertical menu fixed top management-menu">
                { links.filter(link => link.enabled).map( link => (
                <Link key={link.to} to={link.to} onlyActiveOnIndex={link.index} className="teal item" activeClassName="active">
                    <i className="icons icon">
                        <i className={ classNames( link.icon, 'icon' )}/>
                    </i>
                    {link.label}
                </Link>
                    ) ) }
                {this.props.children}
            </nav>
        );
    }
}
"use strict";

import React, { Component, PropTypes } from "react";
import PureRenderMixin from 'react-addons-pure-render-mixin';
import ConversationImage from "../message/ConversationImage";

export default class ChatBarMenuItem extends Component {

    constructor( props ) {
        super( props );
        this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind( this );
        this.clickHandler          = this.clickHandler.bind( this );
    }

    clickHandler() {
        this.props.onJoin( this.props.conversation._id )
    }

    render() {
        const { conversation } = this.props;

        let unread = 0;
        if ( conversation && conversation.participant ) {
            unread = conversation.participant.unreadCount;
        }

        return (
            <div key={conversation._id} className="conversation link item" onClick={this.clickHandler}>
                <ConversationImage conversation={conversation}/>
                <span className="conversation-name">#{conversation.slug}</span>
                { unread > 0 && <div className="ui mini red circular label">{unread}</div> }
            </div>
        );
    }
}

ChatBarMenuItem.propTypes = {
    conversation: PropTypes.object.isRequired,
    onJoin: PropTypes.func.isRequired,
};
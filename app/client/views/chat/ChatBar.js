"use strict";

import React, { Component, PropTypes } from "react";
import PureRenderMixin from 'react-addons-pure-render-mixin';
import classNames from "classnames";
import ConversationCollection from "/imports/messages/ConversationCollection";
import { ConnectMeteorData } from "../../lib/ReactMeteorData";
import DropDown from "../../components/semanticui/modules/Dropdown";
import ChatRoom from "./ChatRoom";
import ChatBarButton from "./ChatBarButton";
import ChatBarMenuItem from "./ChatBarMenuItem";

const openedConversationsStorage = new StoredVar( 'pr.chat.openedConversations' );

export class ChatBar extends Component {

    constructor( props ) {
        super( props );
        this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind( this );
        this.joinConversation      = this.joinConversation.bind( this );
        this.leaveConversation     = this.leaveConversation.bind( this );
        this.toggleConversation    = this.toggleConversation.bind( this );
        this.state                 = {
            conversationId: null
        };
    }

    joinConversation( conversationId ) {
        const { openedConversations } = this.props;

        const joined = _.uniq( openedConversations.map( conversation => conversation._id ).concat( [conversationId] ) );
        openedConversationsStorage.set( joined );

        this.setState( { conversationId: conversationId } );
    }

    leaveConversation( conversationId ) {
        const { openedConversations } = this.props;

        const joined = openedConversations.map( conversation => conversation._id ).filter( cId => cId != conversationId );
        openedConversationsStorage.set( joined );

        if ( this.state.conversationId == conversationId ) {
            this.setState( { conversationId: null } );
        }
    }

    toggleConversation( conversationId ) {
        this.setState( {
            conversationId: ( this.state.conversationId != conversationId ) ? conversationId : null
        } );
    }

    render() {
        const { user, conversations, openedConversations }  = this.props;
        const { conversationId }                            = this.state;

        if ( !user ) {
            return null;
        }

        return (
            <div id="chat">
                <nav className="chat-bar-menu ui inverted mini menu bottom fixed">
                    <div className="right menu">
                        { openedConversations.map( conversation =>
                            <ChatBarButton
                                key={conversation._id}
                                conversation={conversation}
                                className={classNames( { active: conversation._id == conversationId } )}
                                onToggle={this.toggleConversation}
                                onClose={this.leaveConversation}/>
                        )}
                        <DropDown className="item conversationsMenu">
                            <i className="talk icon"/>
                            <span>{__( 'Chat' )}</span>
                            <i className="caret up icon"/>
                            <div className="menu">
                                <div className="header">{__( 'Conversations' )}</div>
                                { conversations.map( conversation =>
                                    <ChatBarMenuItem key={conversation._id} conversation={conversation} onJoin={this.joinConversation}/> ) }
                            </div>
                        </DropDown>
                    </div>
                </nav>
                { conversationId && <ChatRoom conversationId={conversationId} onClose={this.toggleConversation}/> }
            </div>
        );
    }
}

export default ConnectMeteorData( props => {
    const openedConversations = openedConversationsStorage.get() || [];
    return {
        user:                Meteor.user(),
        loading:             !Meteor.subscribe( 'conversations' ).ready(),
        conversations:       ConversationCollection.find( {} ).fetch(),
        openedConversations: ConversationCollection.find( { _id: { $in: openedConversations } } ).fetch()
    }
} )( ChatBar );

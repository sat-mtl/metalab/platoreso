"use strict";

import React, { Component, PropTypes } from "react";
import { Link } from "react-router";
import ConversationCollection from "/imports/messages/ConversationCollection";
import { ConnectMeteorData } from "../../lib/ReactMeteorData";
import Conversation from "../message/Conversation";
import MessageList from "../message/conversation/MessageList";
import MessageInput from "../message/conversation/MessageInput";

export class ChatRoom extends Conversation {

    render() {
        const { conversation } = this.props;

        if ( !conversation ) {
            return null;
        }

        return (
            <div className="conversation-messages">
                <div className="title-bar">
                    <div className="title">
                        <Link to={`/messages/${conversation._id}`} onClick={this.close}>{conversation.name}
                            <span className="slug">(#{conversation.slug})</span></Link></div>
                    <div className="actions">
                        <div className="close action" onClick={this.close}><i className="close icon"/></div>
                    </div>
                </div>
                <div className="message-list-container">
                    <MessageList conversation={conversation} openImage={this.openImage}/>
                </div>
                <MessageInput conversation={conversation} onSend={this.sendMessage} pictureChanged={this.pictureChanged}/>
                { this.renderModal() }
            </div>
        );
    }
}

ChatRoom.propTypes = _.extend( {
    onClose: PropTypes.func.isRequired
}, Conversation.propTypes );

export default ConnectMeteorData( ( { conversationId } ) => ( {
    conversation: ConversationCollection.findOne( { _id: conversationId } )
} ) )( ChatRoom );

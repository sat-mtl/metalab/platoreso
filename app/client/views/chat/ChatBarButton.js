"use strict";

import React, {Component, PropTypes} from "react";
import classNames from "classnames";
import {Counts} from "meteor/tmeasday:publish-counts";
import ConversationImage from "../message/ConversationImage";

export default class ChatBarButton extends Component {

    constructor( props ) {
        super( props );
        this.toggle = this.toggle.bind( this );
        this.close  = this.close.bind( this );
    }

    toggle() {
        this.props.onToggle( this.props.conversation._id );
    }

    close( event ) {
        if ( event ) {
            event.stopPropagation();
        }
        this.props.onClose( this.props.conversation._id );
    }

    render() {
        const { conversation, className }  = this.props;

        let unread = 0;
        if ( conversation && conversation.participant) {
            unread = conversation.participant.unreadCount;
        }

        return (
            <div key={conversation._id} className={classNames( "conversation link item", className )} onClick={this.toggle}>
                <ConversationImage conversation={conversation}/>
                <span className="conversation-name">#{conversation.slug}</span>
                { unread > 0 && <div className="ui mini red circular label">{unread}</div> }
                <div className="close action" onClick={this.close}>
                    <i className="close icon"/></div>
            </div>
        );
    }
}

ChatBarButton.propTypes = {
    conversation: PropTypes.object.isRequired,
    onToggle:     PropTypes.func.isRequired,
    onClose:      PropTypes.func.isRequired,
    className:    PropTypes.string
};
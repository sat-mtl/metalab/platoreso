"use strict";

import React, {Component, PropTypes} from "react";
import UserHelpers from "/imports/accounts/UserHelpers";
import {ConnectMeteorData} from "../lib/ReactMeteorData";
import ManagementPage from "./layouts/ManagementPage";
import SettingsMenu from "./settings/Menu";
import Forbidden from "./errors/Forbidden";

export class Settings extends Component {

    render() {
        const { user } = this.props;

        if ( !UserHelpers.isAdmin( user ) ) {
            return <Forbidden/>
        }

        return (
            <ManagementPage id="settings" className="settings" menu={<SettingsMenu/>}>
                {this.props.children}
            </ManagementPage>
        );
    }

}

Settings.propTypes = {
    user: PropTypes.object.isRequired
};

export default ConnectMeteorData( props => {
    return {
        user: Meteor.user()
    }
} )( Settings );
"use strict";

import React, { Component, PropTypes } from "react";
import ReactDOM from "react-dom";
import classNames from "classnames";
import { FS } from "meteor/cfs:base-package";
import ImageCollections from "/imports/images/ImageCollections";

/**
 * Helper Component to work more elegantly with CollectionFS Images
 * Lets us customize the loading and state of the image from a unique component.
 *
 * Just pass it the FS.File reference and a size (the part after the underscore in the store name: collectionName_sizeName)
 */
export default class Image extends Component {

    componentDidMount() {
        if ( this.props.onUpdated ) {
            $( 'img', ReactDOM.findDOMNode( this ) ).on( "load", this.props.onUpdated );
        }
    }

    shouldComponentUpdate( nextProps, nextState ) {
        return !_.isEqual( this.props, nextProps );
    }

    componentWillUpdate( nextProps, nextState) {
        if ( this.props.onUpdated ) {
            $( 'img', ReactDOM.findDOMNode( this ) ).off( "load", this.props.onUpdated );
        }
    }

    componentDidUpdate() {
        if ( this.props.onUpdated ) {
            $( 'img', ReactDOM.findDOMNode( this ) ).on( "load", this.props.onUpdated );
            this.props.onUpdated();
        }
    }

    render() {
        const { image, asBackground, tagOnly, className, ...props } = this.props;

        if ( image && image.isImage() ) {
            const store = image.collectionName + '_' + this.props.size;

            if ( image.isUploaded() && image.hasStored( store ) ) {
                const url = image.url( { store: store } );

                if ( asBackground ) {
                    // Image used as background, return a div with a background image
                    const copyInfo = image.getCopyInfo( store );
                    return (
                        <div className={ classNames( 'ui image', className ) }
                             style={ {
                                 backgroundImage: 'url(' + url + ')',
                                 width:           copyInfo ? copyInfo.width : 'initial',
                                 height:          copyInfo ? copyInfo.height : 'initial'
                             } }>
                        </div>
                    );
                } else if ( tagOnly ) {
                    // Image used as-is, return an img tag
                    return <img src={url} className={ classNames( 'ui image', className ) }/>;
                } else {
                    return (
                        <div className={ classNames( 'ui image', className ) }>
                            <img src={url}/>
                        </div>
                    );
                }
            } else if ( tagOnly ) {
                return <div className="ui active inline loader"></div>;
            } else {
                // Image exists but is not yet available, return a loader
                return <div className={ className }>
                    <div className="ui active inline loader"></div>
                </div>
            }
        } else {
            return <i className={ classNames( 'inverted grey picture icon', className ) }/>;
        }
    }
}

Image.propTypes = {
    image:        PropTypes.oneOfType( [
        PropTypes.instanceOf( FS.File ),
        PropTypes.shape( {
            isImage:     PropTypes.func.isRequired,
            isUploaded:  PropTypes.func.isRequired,
            hasStores:   PropTypes.func.isRequired,
            url:         PropTypes.func.isRequired,
            getCopyInfo: PropTypes.func.isRequired
        } )
    ] ),
    size:         PropTypes.oneOf( ImageCollections.stores ),
    asBackground: PropTypes.bool,
    tagOnly:      PropTypes.bool,
    onUpdated:    PropTypes.func
};

Image.defaultProps = {
    image:        null,
    size:         'large',
    asBackground: false,
    tagOnly:      false
};

"use strict";

import moment from "moment";
import React, {Component, PropTypes} from "react";

export default class LiveDate extends Component {

    constructor( props ) {
        super( props );
        this.start          = this.start.bind( this );
        this.stop           = this.stop.bind( this );
        this.updateInterval = this.updateInterval.bind( this );
        this.state          = {
            date: moment.isMoment( props.date ) ? props.date : moment( props.date )
        };
    }

    componentDidMount() {
        this.updateInterval();
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.date != this.props.date ) {
            this.setState( {
                date: moment.isMoment( nextProps.date ) ? nextProps.date : moment( nextProps.date )
            } );
        }
    }

    componentDidUpdate( lastProps, lastState ) {
        this.updateInterval();
    }

    componentWillUnmount() {
        this.stop();
    }

    updateInterval() {
        let interval;
        if ( this.props.relative ) {
            const now  = moment();
            const diff = moment.duration( now.diff( this.state.date ) );
            if ( diff.asMinutes() > 45 ) {
                interval = moment.duration( 1, 'hours' ).asMilliseconds();
            } else if ( diff.asSeconds() > 45 ) {
                interval = moment.duration( 1, 'minutes' ).asMilliseconds();
            } else {
                interval = moment.duration( 10, 'seconds' ).asMilliseconds();
            }

        } else {
            interval = this.props.interval;
        }

        if ( this.interval != interval ) {
            this.interval = interval;
            this.stop();
            this.start();
        }
    }

    start() {
        this.ticker = setInterval( this.forceUpdate.bind( this ), this.interval );
    }

    stop() {
        if ( this.ticker ) {
            clearInterval( this.ticker );
        }
    }

    render() {
        const { relative, format, strings } = this.props;
        const { date }                      = this.state;
        const string                        = relative ? date.fromNow() : date.format( format );
        if ( this.props['data-i18n'] ) {
            return (
                <span dangerouslySetInnerHTML={{ __html: __( this.props['data-i18n'], _.extend( { date: `<span class="date">${ string }</span>` }, strings ) ) }}/>
            );
        } else {
            return <span className="date">{string}</span>;
        }
    }
}

LiveDate.propTypes = {
    'data-i18n': PropTypes.string,
    strings:     PropTypes.object,
    date:        PropTypes.any.isRequired,
    relative:    PropTypes.bool,
    format:      PropTypes.string,
    interval:    PropTypes.number
};

LiveDate.defaultProps = {
    'data-i18n': null,
    strings:     {},
    relative:    false,
    format:      'data.medium',
    interval:    10000
};
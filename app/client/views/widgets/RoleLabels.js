"use strict";

import UserHelpers from "/imports/accounts/UserHelpers";
import { RoleList, RoleLevels } from "/imports/accounts/Roles";
import React, { Component, PropTypes } from "react";
import classNames from "classnames";

export default class RoleLabels extends Component {

    constructor( props ) {
        super( props );
    }

    componentWillMount() {
        this.setState( RoleLabels.computeRoles( this.props.user ) );
    }

    componentWillReceiveProps( nextProps ) {
        if ( this.props.user != nextProps.user ) {
            this.setState( RoleLabels.computeRoles( nextProps.user ) );
        }
    }

    shouldComponentUpdate( nextProps, nextState ) {
        return !_.isEqual( this.props.user.roles, nextProps.user.roles );
    }

    static computeRoles( user ) {
        const groups = UserHelpers.getGroupsForUser( user );
        return {
            userRoles:  UserHelpers.getRolesForUser( user ),
            groupRoles: _.reduce( groups, ( roles, group ) => _.union( roles, UserHelpers.getRolesForUser( user, group ) ), [] )
        }
    }

    render() {
        const { userRoles, groupRoles } = this.state;

        return (
            <span className="ui mini horizontal labels roles">
                { _.union( userRoles, groupRoles ).sort( ( a, b )=>RoleLevels[a] < RoleLevels[b] ).map( role => {
                    let className = classNames( "ui label", { basic: userRoles.indexOf( role ) == -1 } ) + ' ' + role;
                    let label     = null;
                    switch ( role ) {
                        case RoleList.admin:
                            className += ' red';
                            label = __( 'Administrator' );
                            break;
                        case RoleList.super:
                            className += ' orange';
                            label = __( 'Superuser' );
                            break;
                        case RoleList.expert:
                            className += ' yellow';
                            label = __( 'Expert' );
                            break;
                        case RoleList.moderator:
                            className += ' olive';
                            label = __( 'Moderator' );
                            break;
                        case RoleList.member:
                            className += ' teal';
                            label = __( 'Member' );
                            break;
                        default:
                            className += ' grey';
                            label = role;
                            break;
                    }
                    return (<span key={role} className={className}>{label}</span>);
                } ) }
            </span>
        );
    }

}

RoleLabels.propTypes = {
    user: PropTypes.object.isRequired
};
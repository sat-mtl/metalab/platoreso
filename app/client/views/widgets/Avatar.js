"use strict";

import React, {Component, PropTypes} from "react";
import classNames from "classnames";
import UserHelpers from "/imports/accounts/UserHelpers";

export default class Avatar extends Component {
    render() {
        const { user, className } = this.props;
        if ( user && user.profile && user.profile.avatarUrl ) {
            return (
                <div className={ classNames( "ui circular avatar image", className ) }>
                    <img src={user.profile.avatarUrl}/>
                </div>
            );
        } else {
            const initials = user ? UserHelpers.getInitials(user) : 'xx';
            const index = ( initials.charCodeAt(0) * 128 ) + initials.charCodeAt(1);
            const avatarStyle = { backgroundColor: colorCollection[index % colorCollection.length], };
            return (
                <div className={ classNames( "ui circular avatar initials image", className ) } style={avatarStyle}>
                    <div>{initials}</div>
                </div>
            );
        }
    }
}

Avatar.propTypes = {
    user: PropTypes.object.isRequired
};
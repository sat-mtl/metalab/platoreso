"use strict";

import React, { Component, PropTypes } from "react";
import classNames from "classnames";

//export default class Spinner extends Component {
    export default function render({className}) {
        return (
            <div className="spinner">
                <div className={classNames("ui active indeterminate loader", className)}></div>
            </div>
        );
    }
//}
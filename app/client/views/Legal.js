"use strict";

import React, { Component, PropTypes } from "react";
import classNames from "classnames";
import md from "/imports/utils/markdown";
import i18n, { language } from "/imports/i18n/i18n";
import I18nMethods from "/imports/i18n/I18nMethods";
import NotFound from "./errors/NotFound";
import Spinner from "./widgets/Spinner";
import ContainedPage from "./layouts/ContainedPage";

export default class Legal extends Component {

    constructor( props ) {
        super( props );
        this.state = {
            document: null,
            loading: true
        }
    }

    componentWillMount() {
        this.getDocument();
    }

    componentDidUpdate() {
        this.getDocument();
    }

    getDocument() {
        const { params } = this.props;

        let document     = "tos";
        if ( params.document ) {
            document = params.document;
        }
        I18nMethods.getDocument.call( { name: document, lang: language.get() }, ( err, doc ) => {
            if ( err ) {
                console.error( err );
                this.setState( { error: err } );
            } else {
                this.setState( { document: doc } );
            }
            this.setState({loading: false});
        } );
    }

    render() {
        const { loading, document } = this.state;

        if ( loading ) {
            return <Spinner/>;
        } else if ( _.isEmpty(document) ) {
            return <NotFound/>;
        } else {
            return (
                <ContainedPage id="legal">
                    <div className="ten wide centered column document" dangerouslySetInnerHTML={{ __html: md( document ) }}/>
                </ContainedPage>
            );
        }
    }

}
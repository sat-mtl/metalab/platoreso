"use strict";

import UserMethods from "/imports/accounts/UserMethods";
import AccountHelpers from "/imports/accounts/AccountHelpers";
import React, { Component, PropTypes } from "react";
import ReactDOM from "react-dom";
import classNames from "classnames";
import Form from '../../../components/forms/Form';
import Field from '../../../components/forms/Field';
import Input from '../../../components/forms/Input';
import TextArea from '../../../components/forms/Textarea';
import Select from '../../../components/forms/Select';
import ImageUploader from '../../../components/forms/ImageUploader';

export default class ProfilePassword extends Component {

    constructor(props) {
        super(props);

        this.changePassword = this.changePassword.bind(this);

        this.state = {
            error: null,
            changing: false
        }
    }

    changePassword( values ) {
        if ( this.state.changing ) {
            return;
        }
        this.setState({changing:true});

        const {user} = this.props;
        if ( user.services && user.services.password ) {
            AccountHelpers.changePassword( values.oldPassword, values.newPassword, err => {
                if ( err ) {
                    this.setState( { error: err } );
                }
                $( ReactDOM.findDOMNode( this.refs.profilePassfordForm ) ).form( 'reset' );
                this.setState( { changing: false } );
            } );
        } else {
            UserMethods.setPassword( values.newPassword, err => {
                if ( err ) {
                    this.setState( { error: err } );
                }
                $( ReactDOM.findDOMNode( this.refs.profilePassfordForm ) ).form( 'reset' );
                this.setState( { changing: false } );
            } );
        }
    }

    render() {
        const {user} = this.props;
        const {changing} = this.state;

        if ( !user || !user.services ) {
            return null;
        }

        return (
            <section>
                <h3 className="ui dividing header">{__('Password')}</h3>

                <Form id="profile-password-form" ref="profilePasswordForm"
                      error={this.state.error}
                      onSuccess={this.changePassword}>
                    <input type="hidden" name="_id" value={user._id}/>

                    { user.services.password ?
                    <Field label={__("Current Password")}>
                        <Input name="oldPassword" type="password" className="fluid"
                               rules={[
                                   {type: 'empty', prompt: __('Please enter your password')},
                                   {type: 'minLength[6]', prompt: __('Your password should be at least 6 characters long')}
                               ]}/>
                    </Field>
                        : null }

                    <Field label={__("New Password")}>
                        <Input name="newPassword" type="password" className="fluid"
                               rules={[
                                   {type: 'empty', prompt: __('Please enter your new password')},
                                   {type: 'minLength[6]', prompt: __('Your new password should be at least 6 characters long')}
                               ]}/>
                    </Field>
                    <Field label={__("New Password (Again)")}>
                        <Input name="newPasswordVerification" type="password" className="fluid"
                               rules={[
                                   {type: 'match[newPassword]', prompt: __('Passwords must match') }
                               ]}/>
                    </Field>

                    <div className="ui divider"></div>
                    <div className={classNames("ui green large right labeled icon submit button", {loading: changing, disabled: changing})}>
                        <i className="save icon"/>
                        {__('Change password')}
                    </div>
                </Form>
            </section>
        );
    }
}

ProfilePassword.propTypes = {
    user: PropTypes.object//.isRequired
};

"use strict";

import UserMethods from "/imports/accounts/UserMethods";
import { unflatten } from 'flat';
import formDiff from "/imports/utils/formDiff";
import React, { Component, PropTypes } from "react";
import classNames from "classnames";
import Form from '../../../components/forms/Form';
import Field from '../../../components/forms/Field';
import Input from '../../../components/forms/Input';
import TextArea from '../../../components/forms/Textarea';

export default class UserInfo extends Component {

    constructor(props) {
        super(props);

        this.updateProfile = this.updateProfile.bind(this);

        this.state = {
            error: null,
            saving: false
        }
    }

    updateProfile( values ) {
        if (this.state.saving) return;
        this.setState({saving:true});

        // Turn the form values into a proper object and compute diff
        const changes = formDiff(unflatten(values), this.props.user, ['_id']);
        UserMethods.update( changes, err => {
            if ( err ) {
                this.setState({error:err});
            }
            this.setState({saving:false});
        })
    }

    render() {
        const {user} = this.props;
        const {saving, error} = this.state;

        if ( !user ) {
            return null;
        }

        return (
            <section>
                <h3 className="ui dividing header">{__('Profile')}</h3>

                <Form id="profile-info-form" ref="profileInfoForm"
                      error={error}
                      onSuccess={this.updateProfile}>
                    <input type="hidden" name="_id" value={user._id}/>

                    <Field name="profile.biography" label={__('Biography')}>
                        <TextArea ref="profile.biography" name="profile.biography" defaultValue={user.profile.biography}/>
                    </Field>

                    <div className="ui divider"></div>
                    <div className={classNames("ui green large right labeled icon submit button", {loading: saving, disabled: saving})}>
                        <i className="save icon"/>
                        {__('Save')}
                    </div>
                </Form>
            </section>
        );
    }
}

UserInfo.propTypes = {
    user: PropTypes.object//.isRequired
};
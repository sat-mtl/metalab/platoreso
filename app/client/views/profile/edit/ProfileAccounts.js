"use strict";

import AccountHelpers from "/imports/accounts/AccountHelpers";
import React, { Component, PropTypes } from "react";
import classNames from "classnames";
import Field from '../../../components/forms/Field';
import ImageUploader from '../../../components/forms/ImageUploader';

export default class ProfileAccounts extends Component {

    constructor(props) {
        super(props);

        this.loginWithFacebook = this.loginWithFacebook.bind(this);
        this.loginWithTwitter = this.loginWithTwitter.bind(this);
        this.loginWithGoogle = this.loginWithGoogle.bind(this);
        this.loginWithLinkedIn = this.loginWithLinkedIn.bind(this);
        this.renderFacebookAccount = this.renderFacebookAccount.bind(this);
        this.renderTwitterAccount = this.renderTwitterAccount.bind(this);
        this.renderLinkedInAccount = this.renderLinkedInAccount.bind(this);
        this.renderGoogleAccount = this.renderGoogleAccount.bind(this);

        this.mounted= false; // Damn you React and your isMounted() deprecation: https://github.com/facebook/react/issues/3417

        this.state = {
            error: false,
            loggingInWithFacebook: false,
            loggingInWithTwitter: false,
            loggingInWithGoogle: false,
            loggingInWithLinkedIn: false
        }
    }

    componentDidMount(){
        this.mounted = true;
    }

    componentWillUnmount() {
        this.mounted = false;
    }

    loginWithFacebook() {
        this.setState({loggingInWithFacebook: true});
        AccountHelpers.login( { service: 'facebook' }, err => {
            if ( !this.mounted ) return;
            if ( err ) {
                this.setState({error: err});
            }
            this.setState({loggingInWithFacebook:false});
        } );
    }

    loginWithTwitter() {
        this.setState({loggingInWithTwitter: true});
        AccountHelpers.login( { service: 'twitter' }, err => {
            if ( !this.mounted ) return;
            if ( err ) {
                this.setState({error: err});
            }
            this.setState({loggingInWithTwitter: false});
        } );
    }

    loginWithGoogle() {
        this.setState({loggingInWithGoogle: true});
        AccountHelpers.login( { service: 'google' }, err => {
            if ( !this.mounted ) return;
            if ( err ) {
                this.setState({error: err});
            }
            this.setState({loggingInWithGoogle: false});
        } );
    }

    loginWithLinkedIn() {
        this.setState({loggingInWithLinkedIn: true});
        AccountHelpers.login( { service: 'linkedin' }, err => {
            if ( !this.mounted ) return;
            if ( err ) {
                this.setState({error: err});
            }
            this.setState({loggingInWithLinkedIn: false});
        } );
    }

    renderFacebookAccount() {
        const {user} = this.props;

        if ( user.services && user.services.facebook ) {
            return (
                <div className="ui large labeled icon gray fluid disabled button">
                    {__( 'Linked to Facebook' )}
                    <i className="ui icon facebook"/>
                </div>
            );
        } else {
            return (
                <div className={classNames("ui large labeled icon facebook fluid button", {loading: this.state.loggingInWithFacebook, disabled: this.state.loggingInWithFacebook})}
                     onClick={this.loginWithFacebook}>
                    {__( 'Link with Facebook' )}
                    <i className="ui inverted icon facebook"/>
                </div>
            );
        }
    }

    renderTwitterAccount() {
        const {user} = this.props;

        if ( user.services && user.services.twitter ) {
            return (
                <div className="ui large labeled icon gray fluid disabled button">
                    {__( 'Linked to Twitter' )}
                    <i className="ui icon twitter"/>
                </div>
            );
        } else {
            return (
                <div className={classNames("ui large labeled icon button twitter fluid", {loading: this.state.loggingInWithTwitter, disabled: this.state.loggingInWithTwitter})}
                     onClick={this.loginWithTwitter}>
                    {__( 'Link with Twitter' )}
                    <i className="ui inverted icon twitter"/>
                </div>
            );
        }
    }

    renderLinkedInAccount() {
        const {user} = this.props;

        if ( user.services && user.services.linkedin ) {
            return (
                <div className="ui large labeled icon gray fluid disabled button">
                    {__( 'Linked to LinkedIn' )}
                    <i className="ui icon linkedin"/>
                </div>
            );
        } else {
            return (
                <div className={classNames("ui large labeled icon button linkedin fluid", {loading: this.state.loggingInWithLinkedIn, disabled: this.state.loggingInWithLinkedIn})}
                     onClick={this.loginWithLinkedIn}>
                    {__( 'Link with LinkedIn' )}
                    <i className="ui inverted icon linkedin"/>
                </div>
            );
        }
    }

    renderGoogleAccount() {
        const {user} = this.props;

        if ( user.services && user.services.google ) {
            return (
                <div className="ui large labeled icon gray fluid disabled button">
                    {__( 'Linked to Google' )}
                    <i className="ui icon google"/>
                </div>
            );
        } else {
            return (
                <div className={classNames("ui large labeled icon button google plus fluid", {loading: this.state.loggingInWithGoogle, disabled: this.state.loggingInWithGoogle})}
                     onClick={this.loginWithGoogle}>
                    {__( 'Link with Google' )}
                    <i className="ui inverted icon google"/>
                </div>
            );
        }
    }

    render() {
        const {user} = this.props;

        if ( !user ) {
            return null;
        }

        return (
            <section>
                <h3 className="ui dividing header">{__('Accounts')}</h3>
                <h4 className="ui dividing header">{__( 'Linked Accounts' )}</h4>
                { this.renderFacebookAccount() }
                <div className="ui hidden divider"></div>
                { this.renderTwitterAccount() }
                <div className="ui hidden divider"></div>
                { this.renderGoogleAccount() }
                <div className="ui hidden divider"></div>
                { this.renderLinkedInAccount() }
            </section>
        );
    }
}

ProfileAccounts.propTypes = {
    user: PropTypes.object//.isRequired
};
"use strict";

import React, { Component, PropTypes } from "react";
import UserCollection from "/imports/accounts/UserCollection";
import AvatarCollection from "/imports/accounts/AvatarCollection";

import Field from '../../../components/forms/Field';
import ImageUploader from '../../../components/forms/ImageUploader';
import {ConnectMeteorData} from '../../../lib/ReactMeteorData';

class UserInfo extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const {user, avatar} = this.props;

        if ( !user ) {
            return null;
        }

        return (
            <section>
                <h3 className="ui dividing header">{__('Avatar')}</h3>

                <Field name="profilePicture" label={__('Profile Picture')}>
                    <ImageUploader collection={AvatarCollection} file={avatar} owner={user._id}/>
                </Field>
            </section>
        );
    }
}

UserInfo.propTypes = {
    user: PropTypes.object//.isRequired
};

/**
 * Meteor Data Wrapper
 */
const UserInfoComponent = ConnectMeteorData( props => ({
    avatarLoading: !Meteor.subscribe( 'avatar/edit' ).ready(),
    avatar: AvatarCollection.findOne( { owners: Meteor.userId() } )
}))(UserInfo);

export default UserInfoComponent;
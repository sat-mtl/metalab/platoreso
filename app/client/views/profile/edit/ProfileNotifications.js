"use strict";

import React, { Component, PropTypes } from "react";
import classNames from "classnames";
import { unflatten } from 'flat';

import formDiff from "/imports/utils/formDiff";
import UserMethods from "/imports/accounts/UserMethods";
import Form from '../../../components/forms/Form';
import Field from '../../../components/forms/Field';
import CheckBox from '../../../components/forms/Checkbox';

export default class ProfileNotifications extends Component {

    constructor( props ) {
        super( props );

        this.updateProfile = this.updateProfile.bind( this );

        this.state = {
            error:  null,
            saving: false
        }
    }

    updateProfile( values ) {
        if ( this.state.saving ) {
            return;
        }
        this.setState( { saving: true } );

        // Semantic UI being dumb again
        values['profile.allowEmailContact']      = !!values['profile.allowEmailContact'];
        values['profile.emailNotifications.cardComment']      = !!values['profile.emailNotifications.cardComment'];
        values['profile.emailNotifications.cardCommentReply'] = !!values['profile.emailNotifications.cardCommentReply'];

        // Turn the form values into a proper object and compute diff
        const changes = formDiff( unflatten( values ), this.props.user, ['_id'] );
        UserMethods.update( changes, err => {
            if ( err ) {
                this.setState( { error: err } );
            }
            this.setState( { saving: false } );
        } );
    }

    render() {
        const { user }          = this.props;
        const { saving, error } = this.state;

        if ( !user ) {
            return null;
        }

        return (
            <section>
                <h3 className="ui dividing header">{__( 'Email Notifications' )}</h3>

                <Form id="profile-notifications-form" ref="profileNotificationsForm"
                      error={error}
                      onSuccess={this.updateProfile}>
                    <input type="hidden" name="_id" value={user._id}/>

                    <Field label={__( "TOS.contact.title" )}>
                        <CheckBox name="profile.allowEmailContact" defaultChecked={user.profile.allowEmailContact} label={__( 'TOS.contact.description' )}/>
                    </Field>

                    <h4>{__( 'Comments' )}</h4>
                    <div className="two fields">

                        <Field label={__( "Card comments" )}>
                            <CheckBox name="profile.emailNotifications.cardComment" defaultChecked={user.profile.emailNotifications.cardComment} label={__( 'New card comment' )}/>
                            <br/>
                            <CheckBox name="profile.emailNotifications.cardCommentReply" defaultChecked={user.profile.emailNotifications.cardCommentReply} label={__( 'New card comment reply' )}/>
                        </Field>

                    </div>

                    <div className="ui divider"></div>
                    <div className={classNames( "ui green large right labeled icon submit button", { loading: saving, disabled: saving } )}>
                        <i className="save icon"/>
                        {__( 'Save' )}
                    </div>
                </Form>
            </section>
        );
    }
}

ProfileNotifications.propTypes = {
    user: PropTypes.object//.isRequired
};

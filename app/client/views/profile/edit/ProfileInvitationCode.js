"use strict";

import React, { Component, PropTypes } from "react";
import classNames from "classnames";

import GroupMethods from "/imports/groups/GroupMethods";

import Form from '../../../components/forms/Form';
import Field from '../../../components/forms/Field';
import Input from '../../../components/forms/Input';

export default class ProfileInvitationCode extends Component {

    constructor( props ) {
        super( props );

        this.validateInvitation = this.validateInvitation.bind( this );

        this.state = {
            validating: false,
            validated:  false,
            error:      null
        }
    }

    validateInvitation( values ) {
        if ( this.state.validating ) {
            return;
        }
        this.setState( { validating: true } );

        GroupMethods.validateInvitationCode.call( values, ( err, result ) => {
            this.setState( {
                validating: false,
                validated:  result,
                error:      err
            } );
        } );
    }

    render() {
        const { user }                         = this.props;
        const { validating, validated, error } = this.state;

        if ( !user ) {
            return null;
        }

        return (
            <section>
                <h3 className="ui dividing header">{__( 'Invitation Code' )}</h3>

                <Form id="invitation-form" ref="invitationForm" error={error} onSuccess={this.validateInvitation}>
                    <Field label={__( 'Code' )}>
                        <Input ref="invitationCode" name="invitationCode" type="text"
                               rules={[
                                   { type: 'regExp[/^[a-zA-Z0-9_-]*$/]', prompt: __( 'Only alphanumeric characters, underscores and dashes are allowed' ) }
                               ]}/>
                    </Field>

                    <div className={ classNames( "ui right labeled icon fluid submit button", { loading: validating, disabled: validating } ) }>
                        {__( 'Validate' )}
                        <i className="checkmark right icon"/>
                    </div>
                </Form>
                { validated && <div className="ui positive message">
                    <div className="header">
                        Invitation Code Validated!
                    </div>
                </div> }

            </section>
        );
    }
}

ProfileInvitationCode.propTypes = {
    user: PropTypes.object//.isRequired
};

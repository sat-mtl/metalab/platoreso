"use strict";

import UserMethods from "/imports/accounts/UserMethods";
import { unflatten } from 'flat';
import formDiff from "/imports/utils/formDiff";
import React, { Component, PropTypes } from "react";
import classNames from "classnames";
import Form from '../../../components/forms/Form';
import Field from '../../../components/forms/Field';
import Input from '../../../components/forms/Input';
import TextArea from '../../../components/forms/Textarea';
import Select from '../../../components/forms/Select';
import ImageUploader from '../../../components/forms/ImageUploader';

export default class ProfileInfo extends Component {

    constructor(props) {
        super(props);

        this.updateProfile = this.updateProfile.bind(this);

        this.state = {
            error: null,
            saving: false
        }
    }

    updateProfile( values ) {
        if (this.state.saving) return;
        this.setState({saving:true});

        // Turn the form values into a proper object and compute diff
        const changes = formDiff(unflatten(values), this.props.user, ['_id']);
        UserMethods.update( changes, err => {
            if ( err ) {
                this.setState({error:err});
            }
            this.setState({saving:false});
        });
    }

    render() {
        const {user} = this.props;
        const {saving, error} = this.state;

        if ( !user ) {
            return null;
        }

        return (
            <section>
                <h3 className="ui dividing header">{__('Personal Information')}</h3>

                <Form id="profile-info-form" ref="profileInfoForm"
                      error={error}
                      onSuccess={this.updateProfile}>
                    <input type="hidden" name="_id" value={user._id}/>

                    <div className="two fields">

                        <Field label="First Name" className="required">
                            <Input ref="profile.firstName" name="profile.firstName" type="text"
                                   defaultValue={user.profile.firstName}
                                   rules={[
                                                       { type: 'empty', prompt: __('Please enter your first name') }
                                                   ]}/>
                        </Field>
                        <Field label="Last Name" className="required">
                            <Input ref="profile.lastName" name="profile.lastName" type="text"
                                   defaultValue={user.profile.lastName}
                                   rules={[
                                                       { type: 'empty', prompt: __('Please enter your last name') }
                                                   ]}/>
                        </Field>
                    </div>
                    <Field label="Email" className="required">
                        <div className={ classNames('ui', 'input', {'right labeled': user.emails && user.emails[0] } ) }>
                            <Input ref="email" name="email" type="email"
                                   defaultValue={user.emails && user.emails[0] ? user.emails[0].address : ''}
                                   rules={[
                                       { type: 'empty', prompt: __('Please enter an email') },
                                       { type: 'email', prompt: __('Please enter a valid email') }
                                   ]}/>
                            {(() => {
                                if ( user.emails && user.emails[0] ) {
                                    if ( user.emails[0].verified ) {
                                        return (
                                            <div className="ui green horizontal label">{__( 'Verified' )}</div>);
                                    } else {
                                        return (
                                            <div className="ui orange horizontal label">{__( 'Unverified' )}</div>);
                                    }
                                } else {
                                    return null;
                                }
                            })()}
                        </div>
                    </Field>

                    <div className="ui divider"></div>
                    <div className={classNames("ui green large right labeled icon submit button", {loading: saving, disabled: saving})}>
                        <i className="save icon"/>
                        {__('Save')}
                    </div>
                </Form>
            </section>
        );
    }
}

ProfileInfo.propTypes = {
    user: PropTypes.object//.isRequired
};

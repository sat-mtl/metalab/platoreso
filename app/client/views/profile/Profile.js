"use strict";

import React, { Component, PropTypes } from "react";
import UserCollection from "/imports/accounts/UserCollection";
import AvatarCollection from "/imports/accounts/AvatarCollection";
import UserHelpers from "/imports/accounts/UserHelpers";
import ModerationMethods from "/imports/moderation/ModerationMethods";
import { ConnectMeteorData } from "../../lib/ReactMeteorData";
import ContainedPage from "../layouts/ContainedPage";
import Spinner from "../widgets/Spinner";
import NotFound from "../errors/NotFound";
import Avatar from "../widgets/Avatar";
import Roles from "../widgets/RoleLabels";
import Feed from "../feed/Feed";

export class Profile extends Component {

    constructor( props ) {
        super( props );
        this.reportUser = this.reportUser.bind( this );
    }

    reportUser() {
        const { user } = this.props;
        if ( !user ) {
            return;
        }
        ModerationMethods.report.call( {
            entityTypeName: UserCollection.entityName,
            entityId:       user._id
        } );
    }

    render() {
        const { userLoading, user, userImage } = this.props;

        if ( userLoading ) {
            return <Spinner/>;

        } else if ( !user ) {
            return <NotFound/>;

        } else {
            const headerStyle = {
                backgroundColor: userImage && userImage.dominantColor ? '#' + userImage.dominantColor : colorCollection[user.profile.firstName.length % colorCollection.length],
                backgroundImage: userImage ? 'url(' + userImage.url( { store: userImage.collectionName + '_large' } ) + ')' : 'none'
            };

            // We can't use the transform model on the users collection so we use this method instead here
            const youReported = user.reportedBy && user.reportedBy.indexOf( Meteor.userId() ) != -1;

            return (
                <ContainedPage id="profile">

                    {/* Header */}
                    <header className="sixteen wide column heading" style={headerStyle}>
                        <h2 className="ui center aligned inverted header">
                            <div className="content">
                                { UserHelpers.getDisplayName( user ) }
                            </div>
                        </h2>
                    </header>

                    <header className="sixteen wide column">
                        <h2 className="ui header column">
                            <Avatar user={user} className="big"/>
                            <div className="content">
                                {user.profile ? user.profile.firstName + ' ' + user.profile.lastName : ''}
                                <div className="ui small teal label">
                                    <i className="trophy icon"/>&nbsp;{user.points}
                                </div>
                                <Roles user={user}/>
                                <div className="sub header">{__( 'User profile.' )}</div>
                            </div>
                        </h2>
                    </header>

                    {/* Left Column */}
                    <div className="four wide column">

                        {/* Menu */}
                        <div className="ui fluid secondary vertical menu">
                            <div className="link item" onClick={this.reportUser}>
                                { youReported ? __( 'You reported this' ) : __( 'Report' ) }
                            </div>
                        </div>

                    </div>

                    {/* Feed */}
                    <div className="twelve wide column">
                        <Feed entities={user._id}/>
                    </div>

                </ContainedPage>
            );
        }
    }
}

Profile.propTypes = {
    userLoading: PropTypes.bool.isRequired,
    user:        PropTypes.object.isRequired,
    userImage:   PropTypes.object
};

export default ConnectMeteorData( props => {
    const { id } = props.params;
    const user   = UserCollection.findOne( id );
    return {
        userLoading: !Meteor.subscribe( 'user/public', id ).ready(),
        user:        user,
        userImage:   user ? AvatarCollection.findOne( { owners: user._id } ) : null
    };
} )( Profile );
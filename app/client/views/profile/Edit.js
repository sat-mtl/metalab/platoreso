"use strict";

import {Link} from 'react-router';
import {ConnectMeteorData} from '../../lib/ReactMeteorData';
import ContainedPage from '../layouts/ContainedPage';
import NotFound from '../errors/NotFound';
import Avatar from '../widgets/Avatar';
import Roles from '../widgets/RoleLabels';

import React, { Component, PropTypes } from "react";

class ProfileEdit extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const {user} = this.props;

        if ( !user ) {
            return <NotFound/>;
        } else {
            return (
                <ContainedPage id="profileEdit" className="profile-edit">
                    <header className="sixteen wide column">
                        <h2 className="ui header column">
                            <Avatar user={user} className="big"/>
                            <div className="content">
                                {user.profile.firstName + ' ' + user.profile.lastName}&nbsp;
                                <Roles user={user}/>
                                <div className="sub header">{__( 'Edit user profile.' )}</div>
                            </div>
                        </h2>
                    </header>
                    <div className="sixteen wide column">
                        <div className="ui stackable grid">
                            <div className="four wide tablet three wide computer column">
                                <div ref="tabs" className="ui secondary vertical fluid pointing menu">
                                    <Link to="/profile/edit" onlyActiveOnIndex={true} className="item" activeClassName="active">
                                        {__( 'Personal Information' )}
                                    </Link>
                                    <Link to="/profile/edit/password" className="item" activeClassName="active">
                                        {__( 'Password' )}
                                    </Link>
                                    <Link to="/profile/edit/avatar" className="item" activeClassName="active">
                                        {__( 'Avatar' )}
                                    </Link>
                                    <Link to="/profile/edit/biography" className="item" activeClassName="active">
                                        {__( 'Biography' )}
                                    </Link>
                                    <Link to="/profile/edit/accounts" className="item" activeClassName="active">
                                        {__( 'Linked Accounts' )}
                                    </Link>
                                    <Link to="/profile/edit/notifications" className="item" activeClassName="active">
                                        {__( 'Notifications' )}
                                    </Link>
                                    <Link to="/profile/edit/code" className="item" activeClassName="active">
                                        {__( 'Invitation Code' )}
                                    </Link>
                                </div>
                            </div>
                            <div className="twelve wide tablet thirteen wide computer stretched column">
                                { React.Children.map(this.props.children, child => React.cloneElement( child, { user: user } ) ) }
                            </div>
                        </div>
                    </div>
                </ContainedPage>
            );
        }
    }
}

/**
 * Meteor Data Wrapper
 */
const ProfileEditComponent = ConnectMeteorData( props => ({
    userLoading: !Meteor.subscribe( 'user/edit' ).ready(),
    user: Meteor.user()
}))(ProfileEdit);

export default ProfileEditComponent;

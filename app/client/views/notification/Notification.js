"use strict";

import GroupCollection from "/imports/groups/GroupCollection";
import CardCollection from "/imports/cards/CardCollection";
import CommentCollection from "/imports/comments/CommentCollection";

import {ConnectMeteorData} from '../../lib/ReactMeteorData'

import GroupNotification from './notificationTypes/GroupNotification';
import CardNotification from './notificationTypes/CardNotification';
import CardCommentNotification from './notificationTypes/CardCommentNotification';

import UserCollection from "/imports/accounts/UserCollection";
import React, { Component, PropTypes } from "react";

export class Notification extends Component {
    render() {
        const { notification } = this.props;

        let NotificationComponent = null;
        switch (notification.objects[0].type) {
            case GroupCollection.entityName:
                NotificationComponent = GroupNotification;
                break;
            case CardCollection.entityName:
                NotificationComponent = CardNotification;
                break;
            case CommentCollection.entityName:
                switch (notification.objects[1].type) {
                    case CardCollection.entityName:
                        NotificationComponent = CardCommentNotification;
                        break;
                }
                break;
        }
        return NotificationComponent ? <NotificationComponent {...this.props}/> : null;
    }
}

Notification.propTypes = {
    notification: PropTypes.object.isRequired,
    subject: PropTypes.object
};

/**
 * Meteor Data Wrapper
 */
export default ConnectMeteorData( props => {
    return {
        subject: props.notification.subject ? UserCollection.findOne({ _id: props.notification.subject }) : null
    }
} )( Notification );
"use strict";

import NotificationCollection from "/imports/notifications/NotificationCollection";
import {ConnectMeteorData} from '../../lib/ReactMeteorData'
import Notification from './Notification';

 import { Counts } from "meteor/tmeasday:publish-counts";
import React, { Component, PropTypes } from "react";
import classNames from "classnames";

const DEFAULT_NOTIFICATIONS_LIMIT     = 10;
const DEFAULT_NOTIFICATIONS_INCREMENT = DEFAULT_NOTIFICATIONS_LIMIT;

export class Notifications extends Component {

    render() {
        const { loading, notifications, unreadCount, total } = this.props;
        const empty = _.isEmpty( notifications );

        if ( loading && empty ) {
            return (
                <div className="ui large active indeterminate text loader">
                    {__( 'Loading notifications...' )}
                </div>
            );
        } else if ( empty ) {
            return (
                <div className="empty">{__( 'You have no notifications' )}</div>
            );
        } else {
            return (
                <div className="notificationsContainer">
                    { notifications.map( notification => <Notification key={notification._id} notification={notification} onClose={this.props.onClose}/> ) }
                    { notifications.length != total &&
                      <div className={classNames("ui basic fluid button", {loading: loading, disabled: loading})} onClick={this.props.loadMore}>{__( 'Load more' )}</div>
                    }
                </div> );
        }
    }
}

Notifications.propTypes = {
    loading:       PropTypes.bool,
    notifications: PropTypes.arrayOf( PropTypes.object ).isRequired,
    unreadCount:   PropTypes.number.isRequired,
    onClose:       PropTypes.func.isRequired
};

Notifications.defaultProps = {
    loading: false
};

/**
 * Meteor Data Wrapper
 * With an es5 function to keep this.setState
 */
export default ConnectMeteorData( function ( props ) {
    const userId = Meteor.userId();
    const limit  = this.state && this.state.limit ? this.state.limit : DEFAULT_NOTIFICATIONS_LIMIT;
    return {
        loading:       !!( userId && !Meteor.subscribe( 'notifications', {limit: limit} ).ready() ),
        notifications: userId ? NotificationCollection.find(
            {user: userId},
            {sort: {createdAt: -1}, limit: limit}
        ).fetch() : [],
        unreadCount:   userId ? Counts.get( 'notifications/' + userId + '/unread' ) : 0,
        total: userId ? Counts.get( 'notifications/' + userId ) : 0,
        loadMore:      () => {
            this.setState( {limit: ( this.state && this.state.limit ? this.state.limit : DEFAULT_NOTIFICATIONS_LIMIT ) + DEFAULT_NOTIFICATIONS_INCREMENT} );
        }
    }
} )( Notifications );
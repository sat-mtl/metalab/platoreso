"use strict";

import React, {Component, PropTypes} from "react";
import GroupNotificationMethods from "/imports/groups/notifications/GroupNotificationMethods";
import GroupNotificationMessage from "/imports/groups/notifications/GroupNotificationMessage";
import NotificationTypeBase from "./NotificationTypeBase";

export default class GroupNotification extends NotificationTypeBase {

    constructor( props ) {
        super( props );
        this.state = {};
    }

    getNotificationData( notificationId, cb ) {
        return GroupNotificationMethods.fetch.call( { notificationId }, cb );
    }

    getLink() {
        const { notification, subject } = this.props;
        return GroupNotificationMessage.getLink( notification, subject, this.state );
    }

    renderMessage() {
        const { notification, subject } = this.props;
        const { group }                 = this.state;

        if ( !group ) {
            return null;
        } else {
            return <div className="message"
                        dangerouslySetInnerHTML={{ __html: GroupNotificationMessage.getMessage( notification, subject, this.state, true ) }}></div>;
        }
    }
}

GroupNotification.propTypes = _.defaults( {}, NotificationTypeBase.propTypes );
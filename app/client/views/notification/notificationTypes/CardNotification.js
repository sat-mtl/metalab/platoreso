"use strict";

import React, {Component, PropTypes} from "react";
import CardNotificationMethods from "/imports/cards/notifications/CardNotificationMethods";
import CardNotificationMessage from "/imports/cards/notifications/CardNotificationMessage";
import NotificationMessageHelpers from "/imports/notifications/NotificationMessageHelpers";
import NotificationTypeBase from "./NotificationTypeBase";

export default class CardNotification extends NotificationTypeBase {

    constructor( props ) {
        super( props );
        this.state = { cards: [] };
    }

    getNotificationData( notificationId, cb ) {
        return CardNotificationMethods.fetch.call( { notificationId }, cb );
    }

    getLink() {
        const { notification, subject } = this.props;
        return CardNotificationMessage.getLink( notification, subject, this.state );
    }

    renderMessage() {
        const { notification, subject }   = this.props;
        const { cards, project }          = this.state;

        if ( !cards || !project ) {
            return null;
        } else {
            return <div className="message"
                        dangerouslySetInnerHTML={{ __html: CardNotificationMessage.getMessage( notification, subject, this.state, true ) }}></div>;
        }
    }
}

CardNotification.propTypes = _.defaults( {}, NotificationTypeBase.propTypes );
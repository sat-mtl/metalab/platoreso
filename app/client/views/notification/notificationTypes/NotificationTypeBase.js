"use strict";

import React, {Component, PropTypes} from "react";
import classNames from "classnames";
import moment from "moment";
import NotificationMethods from "/imports/notifications/NotificationMethods";
import Image from "../../widgets/Image";
import Avatar from "../../widgets/Avatar";

export default class NotificationTypeBase extends Component {

    constructor( props ) {
        super( props );
        this.getLink             = this.getLink.bind( this );
        this.visitNotification   = this.visitNotification.bind( this );
        this.getNotificationData = this.getNotificationData.bind( this );
        this._getNotificationData = this._getNotificationData.bind( this );
    }

    componentWillMount() {
        if ( this.props.notification ) {
            this._getNotificationData( this.props.notification._id );
        }
    }

    componentWillReceiveProps( nextProps ) {
        if (
            ( !this.props.notification && nextProps.notification )
            || ( nextProps.notification && this.props.notification._id != nextProps.notification._id )
        ) {
            this._getNotificationData( nextProps.notification._id )
        }
    }

    _getNotificationData( notificationId) {
        const data = this.getNotificationData(notificationId, (error,data) => {
            if ( error ) {
                console.error(error);
            }
            if (data != null) {
                this.setState(data)
            }
        } );
        if ( data != null ) {
            // Client simulation run
            this.setState(data);
        }
    }

    getNotificationData(notificationId, cb) {
        //noop
    }

    /**
     * Get the link associated with the notification
     *
     * @returns {String|null}
     */
    getLink() {
        return null;
    }

    visitNotification() {
        const { notification } = this.props;
        const link             = this.getLink();
        if ( !link ) {
            return;
        }

        this.context.router.push( { pathname: link } );

        if ( !notification.unclicked ) {
            NotificationMethods.markAsClicked.call( { notificationId: notification._id } )
        }

        this.props.onClose();
    }

    renderMessage() {
        return null;
    }

    render() {
        const { notification, subject, image } = this.props;

        return (
            <div className={classNames( 'notification', {
                unread:    !notification.read,
                unclicked: !notification.clicked
            } )} onClick={this.visitNotification}>
                <div className="subject image">
                    { subject ? <Avatar user={subject}/> :
                        <img className="ui avatar image" src="/images/platform-avatar.png"/> }
                </div>
                <div className="content">
                    {this.renderMessage()}
                    <div className="date">
                        {moment( notification.createdAt ).fromNow()}
                    </div>
                </div>
                <div className="extra">
                    { image && <Image image={image} size="thumb" className="thumbnail"/> }
                    <div className="actions">
                        <span className={classNames( 'readIndicator', { unread: !notification.read } )}/>
                    </div>
                </div>
            </div>
        );
    }

}

NotificationTypeBase.propTypes = {
    loading:      PropTypes.bool,
    notification: PropTypes.object.isRequired,
    subject:      PropTypes.object,
    image:        PropTypes.object,
    onClose:      PropTypes.func.isRequired
};

NotificationTypeBase.defaultProps = {
    loading: false
};

NotificationTypeBase.contextTypes = {
    router: PropTypes.object.isRequired
};
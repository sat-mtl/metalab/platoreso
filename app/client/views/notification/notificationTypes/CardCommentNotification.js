"use strict";

import React, {Component, PropTypes} from "react";
import CardCommentNotificationMethods from "/imports/cards/comments/notifications/CardCommentNotificationMethods";
import CardCommentNotificationMessage from "/imports/cards/comments/notifications/CardCommentNotificationMessage";
import NotificationTypeBase from "./NotificationTypeBase";

export default class CardCommentNotification extends NotificationTypeBase {

    constructor( props ) {
        super( props );
        this.state = {};
    }

    getNotificationData( notificationId, cb ) {
        return CardCommentNotificationMethods.fetch.call( { notificationId }, cb );
    }

    getLink() {
        const { notification, subject } = this.props;
        return CardCommentNotificationMessage.getLink( notification, subject, this.state );
    }

    renderMessage() {
        const { notification, subject }  = this.props;
        const { card, project } = this.state;

        if ( !card || !project ) {
            return null;
        } else {
            return <div className="message"
                        dangerouslySetInnerHTML={{ __html: CardCommentNotificationMessage.getMessage( notification, subject, this.state, true ) }}></div>;
        }
    }
}

CardCommentNotification.propTypes = _.defaults( {}, NotificationTypeBase.propTypes );
"use strict";

import React, {Component, PropTypes} from "react";
import ReactDOM from "react-dom";
import classNames from "classnames";
import async from "async";

import {FS} from "meteor/cfs:base-package";

import ProjectMethods from "/imports/projects/ProjectMethods";
import ProjectImagesCollection from "/imports/projects/ProjectImagesCollection";
import ContainedPage from '../layouts/ContainedPage';
import Form from '../../components/forms/Form';
import ProjectForm from "./ProjectForm";

const { File } = FS;

export default class Create extends Component {

    constructor( props ) {
        super( props );

        this.pictureChanged = this.pictureChanged.bind( this );
        this.doUploads      = this.doUploads.bind( this );
        this.cancel         = this.cancel.bind( this );
        this.submitForm     = this.submitForm.bind( this );
        this.createProject  = this.createProject.bind( this );

        this.state = {
            error:   null,
            saving:  null,
            picture: null
        }
    }

    pictureChanged( files ) {
        this.setState( {
            picture: ( files && files.length > 0 ) ? files[0] : null
        } );
    }

    permissionsChanged( permissions ) {
        this.setState( { permissions } );
    }

    cancel() {
        if ( this.props.onFinished ) {
            this.props.onFinished();
        } else {
            this.context.router.goBack();
        }
    }

    submitForm() {
        $( ReactDOM.findDOMNode( this.refs.projectCreationForm ) ).form( 'submit' );
    }

    createProject( values ) {
        if ( this.state.saving ) {
            return;
        }
        this.setState( { saving: true } );

        // Get boolean value instead of the "on" string
        values.public = !!values.public;
        values.showTimeline = !!values.showTimeline;

        ProjectMethods.create.call( {
            project: values
        }, ( err, projectId ) => {
            if ( err ) {
                this.setState( { saving: false, error: err } );
            } else {
                this.doUploads( projectId );
            }
        } )
    }

    doUploads( projectId ) {
        let tasks = [];

        // Project is created, we can now upload the image
        if ( this.state.picture ) {
            let file    = new File( this.state.picture );
            file.owners = [projectId];
            tasks.push( callback => ProjectImagesCollection.insert( file, callback ) );
        }

        async.parallelLimit( tasks, 3, ( err, result ) => {
            this.setState( { saving: false } );
            if ( err ) {
                this.setState( { error: err } );
            } else {
                if ( this.props.onFinished ) {
                    this.props.onFinished();
                } else {
                    this.context.router.replace( { pathname: '/project/' + projectId } );
                }
            }
        } );
    }

    render() {
        const { saving } = this.state;

        return (
            <ContainedPage>
                <div className="standalone sixteen wide column editor createProject">
                    <div className="header">
                        {__( 'Create Project' )}
                    </div>

                    <Form id="project-creation-form" ref="projectCreationForm"
                          error={this.state.error}
                          onSuccess={this.createProject}>
                        <ProjectForm
                            permissionsChanged={this.permissionsChanged}
                            pictureChanged={this.pictureChanged}/>
                    </Form>

                    <div className="actions">
                        <div className="ui buttons">
                            <div className="ui cancel button" onClick={this.cancel}>
                                <i className="remove icon"/>
                                {__( 'Cancel' )}
                            </div>
                            <div className={classNames("ui green button",{ loading: saving, disabled: saving })} onClick={this.submitForm}>
                                <i className="plus icon"/>
                                {__( 'Create' )}
                            </div>
                        </div>
                    </div>
                </div>
            </ContainedPage>
        );
    }
}

Create.contextTypes = {
    router: PropTypes.object.isRequired
};

"use strict";

import React, { Component, PropTypes } from "react";
import classNames from "classnames";
import { Link } from "react-router";
import CardCollection from "/imports/cards/CardCollection";
import CardLinksCollection from "/imports/cards/links/CardLinksCollection";
import { ConnectMeteorData } from '../../../lib/ReactMeteorData';
import DropDown from '../../../components/semanticui/modules/Dropdown';
import NotFound from '../../errors/NotFound';
import D3GraphView from './graph/D3GraphView';
import BoardView from './BoardView';
import CardPreview from '../../card/CardPreview';
import CurvedCircleGraph from './graph/d3/CurvedCircleGraph';

const typeStorage        = new StoredVar( 'pr.graph.type' );
const layoutStorage      = new StoredVar( 'pr.graph.layout' );
const sizedByStorage     = new StoredVar( 'pr.graph.sizedBy' );
const labelsStorage      = new StoredVar( 'pr.graph.showLabels' );
const lockDraggedStorage = new StoredVar( 'pr.graph.lockDragged' );
const historyStorage     = new StoredVar( 'pr.graph.showHistory' );

export class Graph extends BoardView {

    constructor( props ) {
        super( props );
        this.moveTooltip     = this.moveTooltip.bind( this );
        this.showCardTooltip = this.showCardTooltip.bind( this );
        this.renderLoading   = this.renderLoading.bind( this );
        this.refresh         = this.refresh.bind( this );
        this.state           = {
            refresh:         false,
            previewCard:     null,
            previewPosition: [0, 0]
        };
    }

    /**
     * Change the board sort field
     *
     * @param field
     */
    sortBy( field ) {
        sizedByStorage.set( field );
    }

    /**
     * Change the graph type
     *
     * @param type
     */
    changeType( type ) {
        typeStorage.set( type );
    }

    /**
     * Change the graph layout
     *
     * @param layout
     */
    setLayout( layout ) {
        layoutStorage.set( layout );
    }

    /**
     * Toggle history visibility
     */
    toggleHistory() {
        historyStorage.set( !historyStorage.get() );
    }

    /**
     * Toggle labels visibility
     */
    toggleLabels() {
        labelsStorage.set( !labelsStorage.get() );
    }

    /**
     * Toggle lock dragged nodes
     */
    toggleLockDragged() {
        lockDraggedStorage.set( !lockDraggedStorage.get() );
    }

    showCardTooltip( card ) {
        this.setState( {
            previewCard: card
        } );
    }

    moveTooltip( position ) {
        this.setState( {
            previewPosition: position
        } );
    }

    /**
     * Refresh the graph
     * Just a flip-flop to trigger a graph update since we can't call it directly in react
     */
    refresh() {
        this.setState( {
            refresh: !this.state.refresh
        } );
    }

    /**
     * Render the loading animation
     */
    renderLoading() {
        return (
            <div className="ui active dimmer">
                <div className="ui large indeterminate text loader">
                    {__( 'Loading Cards' )}
                </div>
            </div>
        );
    }

    render() {
        const { project, showCard, loading, cards, links, type, sizedBy, showHistory, showLabels, lockDragged, layout, showSidebar, toggleSidebar } = this.props;
        const { previewCard, previewPosition }                                                                                                      = this.state;

        if ( loading ) {
            // If we are loading show the indicator
            return this.renderLoading();

        } else if ( !cards ) {
            // We are neither loading or having a project, so show an error message
            return <NotFound/>

        } else {
            let GraphType = null;
            switch ( type ) {
                case 'nodes':
                default:
                    GraphType = CurvedCircleGraph;
                    break;
            }

            return (
                <div className="graphDisplay board view">
                    {/* Toolbar */}
                    <div className="toolbar ui inverted menu">

                        {/* Show/Hide Sidebar */}
                        { !showSidebar &&
                          <div className="ui link item" onClick={toggleSidebar}>
                              <i className="indent icon"/>
                              <span className="optional-text">{__( 'Show Sidebar' )}</span>
                          </div>
                        }

                        <Link to={`/project/${project.uri}/board`} className="board item" activeClassName="active">
                            <i className="columns icon"/>
                            <span className="optional-text">{__( 'Board' )}</span>
                        </Link>

                        <Link to={`/project/${project.uri}/graph`} className="graph item" activeClassName="active">
                            <i className="sitemap icon"/>
                            <span className="optional-text">{__( 'Graph' )}</span>
                        </Link>

                        {/* Display */}
                        <DropDown className="ui dropdown item">
                            <i className="block layout icon"/>
                            <span className="optional-text">{__( 'Display' )}</span>
                            <i className="dropdown icon"/>
                            <div className="menu">
                                <div className="item">
                                    <i className="dropdown icon"/>
                                    <span className="text">{__( 'Size' )}</span>
                                    <div className="menu">
                                        <div className={classNames( 'temperature sort option item', { active: sizedBy == 'temperature' } )}
                                             onClick={() => this.sortBy( 'temperature' )}>
                                            <i className="fire icon"/>
                                            {__( 'Trending' )}
                                        </div>
                                        <div className={classNames( 'totalTemperature sort option item', { active: sizedBy == 'totalTemperature' } )}
                                             onClick={() => this.sortBy( 'totalTemperature' )}>
                                            <i className="star icon"/>
                                            {__( 'Popularity' )}
                                        </div>
                                        <div className={classNames( 'like sort option item', { active: sizedBy == 'likeCount' } )}
                                             onClick={() => this.sortBy( 'likeCount' )}>
                                            <i className="heart icon"/>
                                            {__( 'Likes' )}
                                        </div>
                                        <div className={classNames( 'view sort option item', { active: sizedBy == 'viewCount' } )}
                                             onClick={() => this.sortBy( 'viewCount' )}>
                                            <i className="eye icon"/>
                                            {__( 'Views' )}
                                        </div>
                                        <div className={classNames( 'comment sort option item', { active: sizedBy == 'commentCount' } )}
                                             onClick={() => this.sortBy( 'commentCount' )}>
                                            <i className="comments icon"/>
                                            {__( 'Comments' )}
                                        </div>
                                    </div>
                                </div>

                                <div className="item">
                                    <i className="dropdown icon"/>
                                    <span className="text">{__( 'Layout' )}</span>
                                    <div className="menu">
                                        <div className={classNames( 'cluster layout option item', { active: layout == 'cluster' } )}
                                             onClick={() => this.setLayout( 'cluster' )}>
                                            <i className="share alternate icon"/>
                                            {__( 'Cluster' )}
                                        </div>
                                        <div className={classNames( 'horizontal layout option item', { active: layout == 'horizontal' } )}
                                             onClick={() => this.setLayout( 'horizontal' )}>
                                            <i className="resize horizontal icon"/>
                                            {__( 'Horizontal' )}
                                        </div>
                                        <div className={classNames( 'radial layout option item', { active: layout == 'radial' } )}
                                             onClick={() => this.setLayout( 'radial' )}>
                                            <i className="selected radio icon"/>
                                            {__( 'Radial' )}
                                        </div>
                                        <div className={classNames( 'radial layout option item', { active: layout == 'radial-inverted' } )}
                                             onClick={() => this.setLayout( 'radial-inverted' )}>
                                            <i className="selected radio icon"/>
                                            {__( 'Radial (Inverted)' )}
                                        </div>
                                    </div>
                                </div>

                                <div className="item">
                                    <i className="dropdown icon"/>
                                    <span className="text">{__( 'Options' )}</span>
                                    <div className="menu">
                                        <div className={classNames( 'labels view option item', { active: showLabels } )}
                                             onClick={this.toggleLabels}>
                                            <i className="tag icon"/>
                                            {__( 'Show labels' )}
                                        </div>
                                        <div className={classNames( 'lockDragged view option item', { active: lockDragged } )}
                                             onClick={this.toggleLockDragged}>
                                            <i className="lock icon"/>
                                            {__( 'Lock dragged nodes' )}
                                        </div>
                                        <div className={classNames( 'ghosts view option item', { active: showHistory } )}
                                             onClick={this.toggleHistory}>
                                            <i className="history icon"/>
                                            {__( 'Show card history' )}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </DropDown>

                        <div className="right menu">
                            <div className="ui icon button item" onClick={this.refresh}>
                                <i className="refresh icon"/>
                            </div>
                        </div>
                    </div>

                    {/* Graph Implementation */}
                    <D3GraphView type={GraphType}
                                 layout={layout}
                                 project={project}
                                 cards={cards}
                                 links={links}
                                 refresh={this.state.refresh}
                                 sizedBy={sizedBy}
                                 showLabels={showLabels}
                                 lockDragged={lockDragged}
                                 showCardTooltip={this.showCardTooltip}
                                 moveTooltip={this.moveTooltip}
                                 openCard={showCard}
                                 className="displayArea"/>

                    <div className="overlay">
                        { previewCard &&
                          <div className="tooltip" style={{ left: (previewPosition[0] + 20) + 'px', top: ( previewPosition[1] - 100 ) + 'px' }}>
                              <CardPreview card={previewCard}
                                           project={project}
                                           layout="grid"/>
                          </div> }
                    </div>
                </div>
            );
        }
    }

}

Graph.propTypes = _.defaults( {
    sizedBy: PropTypes.string.isRequired,
    layout:  PropTypes.string.isRequired
}, BoardView.propTypes );

Graph.defaultProps = _.defaults( {
    sizedBy: 'likeCount',
    layout:  'horizontal'
}, BoardView.defaultProps );

export default ConnectMeteorData( props => {
    const showHistory = !!historyStorage.get(); // It might be null, so take no chance
    const query       = { project: props.project._id };
    if ( !showHistory ) {
        query.current = true;
    }

    let data = {
        loading:     !Meteor.subscribe( 'project/card/list', props.project._id, null, { showCardHistory: showHistory, linked: true } ).ready(),
        cards:       CardCollection.find( query ).fetch(),
        sizedBy:     sizedByStorage.get() || 'likeCount',
        type:        typeStorage.get() || 'circleGraph',
        layout:      layoutStorage.get() || 'horizontal',
        showLabels:  labelsStorage.get() == null ? true : labelsStorage.get(),
        lockDragged: !!lockDraggedStorage.get(), // It might be null, so take no chance,
        showHistory: showHistory,
        links:       []
    };

    // Card current links
    const cardIds = data.cards.map( card => card.id );
    data.links    = CardLinksCollection.find( { $or: [{ 'source.id': { $in: cardIds } }, { 'destination.id': { $in: cardIds } }] } ).fetch();

    return data;
} )( Graph );
"use strict";

import Scrollable from '../../../components/utils/Scrollable';

import React, { Component, PropTypes } from "react";
import classNames from "classnames";

export default class ContentPanel extends Component {

    render() {
        const { project, opened, toggle, showSidebar } = this.props;

        return (
            <div className={classNames( 'project-panel', { opened: opened, closed: !opened, hidden: !showSidebar } )}>
                <div className="handle" onClick={toggle}>
                    <i className={classNames( "chevron circle icon", { left: opened, right: !opened } )}/>
                </div>
                <Scrollable className="panel-scroller">
                    <div className="panel-content">
                        <h2>{project.name}</h2>
                        <h3>{project.description}</h3>
                        <div dangerouslySetInnerHTML={{ __html: project.content }}></div>
                    </div>
                </Scrollable>
            </div>
        );
    }
}

ContentPanel.propTypes = {
    project:     PropTypes.object.isRequired,
    opened:      PropTypes.bool.isRequired,
    toggle:      PropTypes.func.isRequired,
    showSidebar: PropTypes.bool.isRequired
};
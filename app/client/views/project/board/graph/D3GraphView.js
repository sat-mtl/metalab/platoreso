'use strict';

import React, {Component, PropTypes} from "react";
import shallowCompare from "react-addons-shallow-compare";
import classNames from "classnames";

export default class D3GraphView extends Component {

    constructor( props ) {
        super( props );

        this.initGraph    = this.initGraph.bind( this );
        this.updateGraph  = this.updateGraph.bind( this );
        this.destroyGraph = this.destroyGraph.bind( this );
    }

    componentDidMount() {
        this.initGraph();
    }

    shouldComponentUpdate( nextProps, nextState ) {
        return shallowCompare( this, nextProps, nextState );
    }

    componentDidUpdate( lastProps, lastState ) {
        if (
            lastProps.refresh != this.props.refresh
            || lastProps.type != this.props.type
            || lastProps.layout != this.props.layout
            || lastProps.showLabels != this.props.showLabels
        ) {
            this.initGraph();
        } else {
            this.updateGraph();
        }
    }

    componentWillUnmount() {
        this.destroyGraph();
    }

    initGraph() {
        this.destroyGraph();
        if ( this.props.type ) {
            this.graph = new this.props.type( $( '#graph' ).get( 0 ), this.props );
            this.graph.init();
        }
    }

    updateGraph() {
        if ( this.graph ) {
            this.graph.update( this.props );
        }
    }

    destroyGraph() {
        if ( this.graph ) {
            this.graph.destroy();
            this.graph = null;
        }
    }

    render() {
        return (
            <div id='graph' className={classNames("graph", this.props.className)}></div>
        );
    }

}

D3GraphView.propTypes = {
    refresh:         PropTypes.bool,
    type:            PropTypes.func.isRequired,
    project:         PropTypes.object.isRequired,
    cards:           PropTypes.arrayOf( PropTypes.object ).isRequired,
    links:           PropTypes.arrayOf( PropTypes.object ).isRequired,
    moveTooltip:     PropTypes.func.isRequired,
    showCardTooltip: PropTypes.func.isRequired,
    openCard:        PropTypes.func.isRequired,
    layout:          PropTypes.string,
    sizedBy:         PropTypes.string,
    showLabels:      PropTypes.bool,
    lockDragged:     PropTypes.bool
};

D3GraphView.defaultProps = {
    sizedBy: 'likeCount'
};

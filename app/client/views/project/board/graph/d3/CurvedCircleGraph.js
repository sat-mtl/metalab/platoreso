"use strict";

import prune from "underscore.string/prune";
import classNames from "classnames";
import d3 from "d3";
import D3Graph from './D3Graph';

const traverse = [{
    linkType: "sourceLinks",
    nodeType: "target"
}, {
    linkType: "targetLinks",
    nodeType: "source"
}];

export default class CurvedCircleGraph extends D3Graph {

    constructor( el, props ) {
        super( el, props );

        this.name = "circleGraph";

        this.radiusScale = d3.scale.log().domain( [1, 100] ).range( [5, 128] ).clamp( true );

        this.cardNodes = [];
        this.nodes     = [];
        this.links     = [];
        this.bilinks   = [];
    }

    initSVG() {
        super.initSVG();

        // Create the tooltip element
        //this.tooltip = this.graph.append( 'div' )
        //                   .attr( 'class', 'tooltip' );

        this.svg
            .on( 'click', () => {
                if ( d3.event.defaultPrevented ) {
                    return;
                }
                this.restoreConnectedNodes( true )
            } );

        // Arrow markers
        this.svg.append( "defs" )
            .selectAll( "marker" )
            .data( ["linked-references", "linked-complements", "linked-derives", "linked-parent"] )
            .enter()
            .append( "marker" )
            .attr( "id", d => d )
            .attr( "viewBox", "0 -5 10 10" )
            .attr( "refX", 10 )
            .attr( "refY", 0 )
            .attr( "markerWidth", 10 )
            .attr( "markerHeight", 10 )
            .attr( "orient", "auto" )
            .append( "path" )
            .attr( "d", "M0,-5L10,0L0,5 L10,0 L0, -5" );

        // Phases
        this.phasesGroup = this.canvas.append( 'g' );

        // Links
        this.linksGroup = this.canvas.append( 'g' );

        // Nodes
        this.nodesGroup = this.canvas.append( 'g' );
    }

    initLayout() {
        super.initLayout();

        const { project, layout } = this.props;

        const self    = this;
        const padding = 36;

        // Draw line to the circle's outside instead of center
        function lineX( source, target ) {
            const length = Math.sqrt( Math.pow( target.y - source.y, 2 ) + Math.pow( target.x - source.x, 2 ) );
            const scale  = (length - target.interpolatedRadius) / length;
            const offset = (target.x - source.x) - (target.x - source.x) * scale;
            return target.x - offset;
        }

        function lineY( source, target ) {
            const length = Math.sqrt( Math.pow( target.y - source.y, 2 ) + Math.pow( target.x - source.x, 2 ) );
            const scale  = (length - target.interpolatedRadius) / length;
            const offset = (target.y - source.y) - (target.y - source.y) * scale;
            return target.y - offset;
        }

        // Circle collision
        function collide( d, quadtree, alpha ) {
            var rb  = 2 * d.radius + padding,
                nx1 = d.x - rb,
                nx2 = d.x + rb,
                ny1 = d.y - rb,
                ny2 = d.y + rb;
            quadtree.visit( function ( quad, x1, y1, x2, y2 ) {
                if ( quad.point && (quad.point !== d) ) {
                    var x = d.x - quad.point.x,
                        y = d.y - quad.point.y,
                        l = Math.sqrt( x * x + y * y );
                    if ( l < rb ) {
                        l = (l - rb) / l * alpha;
                        d.x -= x *= l;
                        d.y -= y *= l;
                        quad.point.x += x;
                        quad.point.y += y;
                    }
                }
                return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
            } );
        }

        // Clustering (by project phase)
        function gravity( d, alpha ) {
            switch ( layout ) {
                case "cluster":
                    //noop
                    break;

                case "radial":
                case "radial-inverted":
                    const x = d.x - self.halfWidth;
                    const y = d.y - self.halfHeight;

                    const angle = Math.atan2( x, y );

                    const target_x = ( (d.tr + d.fudge ) * Math.sin( angle ) ) + self.halfWidth;
                    const target_y = ( (d.tr + d.fudge) * Math.cos( angle ) ) + self.halfHeight;

                    d.x += (target_x - d.x) * alpha;
                    d.y += (target_y - d.y) * alpha;

                    break;

                case "horizontal":
                default:
                    const a = .1 * alpha;
                    d.x += (d.tx - d.x) * a;
                    d.y += (self.halfHeight - d.y) * ( a * 0.5 );
                    break;
            }

        }

        // Interpolate radius
        function interpolateRadius( d, alpha ) {
            d.interpolatedRadius += (d.radius - d.interpolatedRadius) * alpha;
        }

        function line( l ) {
            return "M" + lineX( l.dummy, l.source ) + "," + lineY( l.dummy, l.source )
                   + "S" + l.dummy.x + "," + l.dummy.y
                   + " " + lineX( l.dummy, l.target ) + "," + lineY( l.dummy, l.target );
        }

        function straightLine( l ) {
            return "M" + lineX( l.target, l.source ) + "," + lineY( l.target, l.source )
                   + " " + lineX( l.source, l.target ) + "," + lineY( l.source, l.target );
        }

        function tick( e ) {
            const quadtree = d3.geom.quadtree( self.cardNodes );
            self.node
                .each( node => {
                    if ( !node.fixed ) {
                        gravity( node, e.alpha );
                    }
                    collide( node, quadtree, 1.5 * e.alpha );
                    interpolateRadius( node, 0.15 );
                } )
                .attr( 'transform', n => 'translate(' + n.x + ',' + n.y + ')' );
            self.link
                .attr( "d", layout == "cluster" ? straightLine : line );
            /*self.dummy
             .attr( 'transform', n => 'translate(' + n.dummy.x + ',' + n.dummy.y + ')' );*/
        }

        this.force = d3.layout.force()
                       .charge( node => node.radius * ( node.dummy ? 0 : -5 ) )
                       .linkDistance( link => {
                           let distance = padding;
                           switch ( this.props.layout ) {
                               case "cluster":
                                   break;

                               case "radial":
                               case "radial-inverted":
                                   distance = ( Math.abs( link.sourceCard.phase - link.targetCard.phase ) / project.phases.length ) * Math.min( this.halfWidth, this.halfHeight ) * ( link.dummy ? 0.5 : 1.0 );
                                   break;

                               case "horizontal":
                               default:
                                   distance = ( Math.abs( link.sourceCard.phase - link.targetCard.phase ) / project.phases.length ) * this.width * ( link.dummy ? 0.5 : 1.0 );
                                   break;

                           }

                           return ( ( padding * this.scale ) + Math.max(
                               // Direct link relationship
                               ( link.source.radius + link.target.radius ) * ( link.dummy ? 1.0 : 2.0 ),
                               // Actual node relationship
                               distance * this.scale
                           ) );
                       } )
                       .linkStrength( link => this.linkStrength( ( Math.abs( link.sourceCard.phase - link.targetCard.phase ) * ( link.dummy ? 0.5 : 1.0 ) ) + 1 ) )
                       .nodes( this.nodes )
                       .links( this.links )
                       .gravity( 0.03 )
                       .on( 'tick', tick );

        // Fix nodes when interacted with
        this.force.drag()
            .on( 'dragstart', function ( d ) {
                if ( self.props.lockDragged ) {
                    d3.select( this ).classed( "fixed", d.fixed = true );
                }
            } );
    }

    resize() {
        super.resize();

        // Update nodes gravity
        this.cardNodes.forEach( node => {
            node.tx = this.phaseX( node.phase );
            node.tr = this.phaseRadius( node.phase );
        } );
    }

    updateConstraints() {
        super.updateConstraints();

        const { project } = this.props;

        // Create a link strength scale from phases
        this.linkStrength = d3.scale.log().domain( [1, project.phases.length] ).range( [1, 0] ).clamp( true );

        // Update phase gravity
        this.phaseX = d3.scale.ordinal()
                        .domain( d3.range( this.props.project.phases.length ) )
                        .rangePoints( [0, this.width], 1 );

        if ( this.props.layout == "radial" ) {
            this.phaseRadius = d3.scale.ordinal()
                                 .domain( d3.range( this.props.project.phases.length ) )
                                 .rangePoints( [Math.min( this.halfWidth, this.halfHeight ), 64 * this.scale], 1 );
        } else if ( this.props.layout == "radial-inverted" ) {
            this.phaseRadius = d3.scale.ordinal()
                                 .domain( d3.range( this.props.project.phases.length ) )
                                 .rangePoints( [64 * this.scale, Math.min( this.halfWidth, this.halfHeight )], 1 );
        } else {
            this.phaseRadius = () => 0;
        }
    }

    updateData() {
        super.updateData();

        const { project, cards, links:cardLinks, layout } = this.props;

        if ( !cards ) {
            this.log.warn( 'Trying to update data of a CircleGraph without cards' );
            return;
        }

        // CARDS
        const nextCardNodes = [];

        cards.forEach( card => {
            // Get the previous node, we are going to reuse it if it exists because
            // it contains data that can be reused by d3 force layout
            const oldNode    = this.cardNodes.find( node => node.card._id == card._id );
            const phase      = project.getPhase( card.phase );

            // Filter out card without a found phase
            // This happens during phase removal sync to the client,
            // cards still reference the deleted phase for a while
            // before being removed by the subscription
            if ( !phase ) {
                return;
            }

            // Keep previous node or create a new one
            let node         = oldNode || {};
            node.card        = card;
            node.phase       = phase.index;
            node.radius      = this.getRadius( node );
            node.color       = this.getColor( node.phase );
            node.sourceLinks = [];
            node.targetLinks = [];
            node.fixed       = this.props.lockDragged && node.fixed;

            switch ( layout ) {
                case "cluster":
                    break;

                case "radial":
                case "radial-inverted":
                    node.tr = this.phaseRadius( node.phase );
                    break;

                case "horizontal":
                default:
                    node.tx = this.phaseX( node.phase );
                    break;
            }

            // If this is a fresh node, initialize its position
            if ( !oldNode ) {
                // Start without animating the radius change
                node.interpolatedRadius = node.radius;

                // Starting position, distribute a bit randomly (no collision) around where they should go
                switch ( layout ) {
                    case "cluster":
                        node.px = node.x = Math.random() * this.width;
                        node.py = node.y = Math.random() * this.height;
                        break;

                    case "radial":
                    case "radial-inverted":
                        node.fudge  = ( Math.random() * 0.5 - 0.25 ) * ( Math.min( this.halfWidth, this.halfHeight ) / project.phases.length );
                        const angle = Math.random() * ( Math.PI * 2 );

                        node.px = node.x = ( ( node.tr + node.fudge ) * Math.cos( angle ) ) + this.halfWidth;
                        node.py = node.y = ( ( node.tr + node.fudge ) * Math.sin( angle ) ) + this.halfHeight;
                        break;

                    case "horizontal":
                    default:
                        node.px = node.x = node.tx + ( ( ( Math.random() * 2 ) - 1 ) * ( this.width / project.phases.length ) );
                        node.py = node.y = ( this.height * .25 ) + ( Math.random() * this.halfHeight );
                        break;
                }
            }

            nextCardNodes.push( node );
        } );

        // Keep ordered by index, this is important as d3
        // assumes new nodes are added at the end of the array
        nextCardNodes.sort( ( a, b ) => {
            if ( a.index == undefined || a.index == null ) {
                return 1;
            } else if ( b.index == undefined || b.index == null ) {
                return -1;
            } else {
                return a.index - b.index;
            }
        } );

        this.cardNodes = nextCardNodes;

        // Update the nodes array, it needs to stay the same as this is the one referenced by d3
        this.nodes.splice( 0, this.nodes.length, ...this.cardNodes );

        // LINKS

        // Empty the links array, it needs to stay the same as this is the one referenced by d3
        this.links.splice( 0, this.links.length );

        // COMPUTE LINKS

        // Sort by version since finding the last matching version for links is important
        const sortedCardNodes = this.cardNodes.slice().sort( ( a, b ) => b.card.version - a.card.version );

        const nodeLinks = cardLinks.map( cardLink => ({
            source: sortedCardNodes.find( n =>
                n.card.id == cardLink.source.id
                && n.card.version >= cardLink.source.versionFrom
                && ( cardLink.source.versionTo == null || n.card.version <= cardLink.source.versionTo )
            ),
            target: sortedCardNodes.find( n =>
                n.card.id == cardLink.destination.id
                && n.card.version >= cardLink.destination.versionFrom
                && ( cardLink.destination.versionTo == null || n.card.version <= cardLink.destination.versionTo )
            ),
            type:   cardLink.type
        }) ).filter( link => link.source && link.target ); // FIXME: Temporary solution for the bad graph, should remove references to links when deleting a card

        // Hierarchical links
        this.cardNodes.forEach( node => {
            if ( node.card.version > 0 ) {
                // Loop over the already sorted by descending version card to find the one lower than the current
                const parent = sortedCardNodes.find( cardNode => cardNode.card.id == node.card.id && cardNode.card.version < node.card.version );
                if ( parent ) {
                    nodeLinks.push( {
                        source: parent,
                        target: node,
                        type:   'parent'
                    } );
                }
            }
        } );

        // Special nodes used to avoid node/link collision and draw curves
        const oldBilinks = this.bilinks;
        this.bilinks     = nodeLinks.map( link => {
            const source = link.source;
            const target = link.target;

            // Cache link information for highlighting paths in the graph
            source.sourceLinks.push( link );
            target.targetLinks.push( link );

            // Create or update bilinks
            const oldBilink     = oldBilinks.find( bilink => bilink.source == source && bilink.target == target );
            const bilink        = oldBilink || {
                    source: source,
                    target: target,
                    type:   link.type,
                    dummy:  {
                        dummy: true,
                        px:    (source.x + target.x) * 0.5,
                        py:    (source.y + target.y) * 0.5,
                        x:     (source.x + target.x) * 0.5,
                        y:     (source.y + target.y) * 0.5
                    }
                };
            bilink.dummy.radius = ( source.radius + target.radius ) * 0.5;

            // The dummy needs to be calculated by the layout forces
            //NOTE: Removed as it was impacting performance a lot for little result
            //      The dummy node will only be moved per the link distance not graph forces
            //this.nodes.push( bilink.dummy );

            // Add links between source/target and dummies
            this.links.push(
                {
                    dummy:      true,
                    source:     source,
                    target:     layout != "cluster" ? bilink.dummy : target,
                    type:       link.type,
                    sourceCard: source,
                    targetCard: target
                },
                {
                    dummy:      true,
                    source:     layout != "cluster" ? bilink.dummy : source,
                    target:     target,
                    type:       link.type,
                    sourceCard: source,
                    targetCard: target
                }
            );

            if ( layout != "cluster" ) {
                this.links.push( {
                    source:     source,
                    target:     target,
                    type:       link.type,
                    sourceCard: source,
                    targetCard: target
                } );
            }

            return bilink;
        } );
    }

    updateLayout() {
        super.updateLayout();

        // SVG Manipulation

        if ( this.props.layout == "radial" || this.props.layout == "radial-inverted" ) {
            // PHASES
            this.phase = this.phasesGroup.selectAll( '.phase' ).data( this.props.project.phases );
            this.phase.exit().remove();
            const phase = this.phase.enter().append( 'circle' )
                              .attr( 'class', 'phase' );
            this.phase
                .attr( 'cx', this.halfWidth )
                .attr( 'cy', this.halfHeight )
                .attr( 'fill', 'none' )
                .attr( 'stroke', phase => this.getColor( phase.index ) )
                .attr( 'stroke-opacity', 0.25 )
                .attr( 'r', phase => this.phaseRadius( phase.index ) );
        }

        // NODES
        this.node = this.nodesGroup.selectAll( '.node' ).data( this.cardNodes );
        this.node.exit().remove();
        const card = this.node.enter().append( 'g' )
                         .attr( 'class', n => classNames( 'node card', { snapshot: !n.card.current } ) )
                         .call( this.force.drag );

        // Card circle
        card.append( 'circle' )
            .attr( 'class', 'circle' )
            .on( 'mouseover', this.highlightConnectedNodes.bind( this, false ) )
            .on( 'mouseout', this.restoreConnectedNodes.bind( this, false ) )
            .on( 'click', node => {
                if ( d3.event.defaultPrevented ) {
                    return;
                }
                d3.event.stopPropagation();
                this.highlightConnectedNodes( true, node )
            } );

        // Card label
        if ( this.props.showLabels ) {
            card.append( 'text' );
            //.attr( 'class', 'name label' )
            //.text( node => !node.dummy ? prune( node.card.name, 32 ) : '' );

            // Update all label now just in case the card name changed
            this.nodesGroup
                .selectAll( 'text' )
                .attr( 'class', 'name label' )
                .text( node => !node.dummy ? prune( node.card.name, 32 ) : '' );
        }

        // Tooltip
        card.on( 'mousemove', node => this.props.moveTooltip( d3.mouse( this.root.node() ) ) )
            .on( 'mouseover', node => this.props.showCardTooltip( node.card ) )
            .on( 'mouseout', node => this.props.showCardTooltip( null ) )
            .on( 'dblclick', node => {
                if ( !d3.event.defaultPrevented ) {
                    this.props.openCard( node.card.uri );
                }
            } );

        // Update all
        this.node.select( '.circle' )
            .attr( 'fill', node => node.color )
            .attr( 'stroke', node => node.color )
            .attr( 'r', node => node.radius );

        this.node.select( '.label' )
            .attr( 'dx', node => Math.sqrt( ( node.radius * node.radius ) * 0.5 ) + 6 )
            .attr( 'dy', node => Math.sqrt( ( node.radius * node.radius) * 0.5 ) + 12 );

        // LINKS
        this.link = this.linksGroup.selectAll( '.link' ).data( this.bilinks );
        this.link.exit().remove();
        this.link.enter().append( 'path' )
            .attr( 'class', l => `link ${l.type}` );
        //NOTE: Moved to highlightConnectedNodes
        //.attr( 'marker-end', l => `url(#linked-${l.type})` ); // Firefox doesn't like this being set in CSS

        // DUMMIES
        /*this.dummy = this.nodesGroup.selectAll( '.dummy' ).data( this.bilinks );
         this.dummy.exit().remove();
         this.dummy.enter().append( 'circle' )
         .attr( 'class', 'dummy' );
         .style( 'stroke', '#ffffff' )
         .style( 'stroke-opacity', 0.85 )
         .style( 'stroke-width', '1px' )
         .style( 'fill', 'none' );
         this.dummy
         .attr( 'r', node => node.dummy.radius );*/
    }

    /**
     * Get node radius
     *
     * @param {Object} node
     */
    getRadius( node ) {
        let count = node.card[this.props.sizedBy];
        if ( isNaN( count ) ) {
            count = 0;
        }
        return this.radiusScale( count + 1 ); // +1 because of the log() method not going to 0
    }

    /**
     * Get node color
     *
     * @param {int} phase
     * @returns {int}
     */
    getColor( phase ) {
        return phase == -1 ? '#C0C0C0' : colorCollection[phase % colorCollection.length];
    }

    /**
     * Highlight node connections
     *
     * @param {Boolean} stayHighlighted
     * @param {Object} node
     */
    highlightConnectedNodes( stayHighlighted = false, node ) {
        if ( d3.event.defaultPrevented ) {
            return;
        }

        if ( stayHighlighted ) {
            this.stayHighlighted = node;
        }

        let remainingNodes = [];
        let nextNodes      = [];

        let highlightedNodes         = [];
        highlightedNodes[node.index] = true;

        traverse.forEach( step => {
            node[step.linkType].forEach( link => {
                const n = link[step.nodeType];
                remainingNodes.push( n );
                highlightedNodes[n.index] = true;
            } );

            while ( remainingNodes.length ) {
                nextNodes = [];
                remainingNodes.forEach( node => {
                    node[step.linkType].forEach( link => {
                        const n = link[step.nodeType];
                        if ( !highlightedNodes[n.index] ) {
                            nextNodes.push( n );
                            highlightedNodes[link[step.nodeType].index] = true;
                        }
                    } );
                } );
                remainingNodes = nextNodes;
            }
        } );

        this.node
            .classed( 'highlighted', n => highlightedNodes[n.index] )
            .classed( 'faded', n => !highlightedNodes[n.index] );
        this.link
            .classed( 'highlighted', l => highlightedNodes[l.source.index] && highlightedNodes[l.target.index] )
            .classed( 'faded', l => !highlightedNodes[l.source.index] || !highlightedNodes[l.target.index] )
            .attr( 'marker-end', l => (highlightedNodes[l.source.index] && highlightedNodes[l.target.index]) ? `url(#linked-${l.type})` : 'none' ); // Firefox doesn't like this being set in CSS;
    }

    /**
     * Unhighlight node connections
     *
     * @param {Boolean} force
     */
    restoreConnectedNodes( force = false ) {
        if ( force || !this.stayHighlighted ) {
            this.node.classed( "highlighted faded", false );
            this.link.classed( "highlighted faded", false ).attr( 'marker-end', 'none' );
            this.stayHighlighted = null;
        } else if ( this.stayHighlighted ) {
            this.highlightConnectedNodes( false, this.stayHighlighted );
        }
    }
}
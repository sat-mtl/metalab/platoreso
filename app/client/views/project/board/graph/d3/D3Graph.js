"use strict";

import logger from "meteor/metalab:logger/Logger";
import d3 from "d3";

const log = logger( "D3 Graph" );

export default class D3Graph {

    constructor( el, props ) {
        this.log   = log;
        this.el    = el;
        this.props = props;

        this.name   = 'd3Graph';
        this.scale  = 1;
        this.width  = 0;
        this.height = 0;
    }

    init() {
        log.debug( 'init' );

        // D3 Root
        this.root = d3.select( this.el );

        // Graph container that we can remove at once without losing root
        this.graph = this.root.append( 'div' )
                         .attr( 'class', this.name )
                         .style( 'position', 'absolute' )
                         .style( 'top', '0' )
                         .style( 'bottom', '0' )
                         .style( 'left', '0' )
                         .style( 'right', '0' );

        // Watch for window resize
        d3.select( window ).on( 'resize', this.resize.bind( this ) );

        // Initialization steps
        this.initSVG();
        this.initLayout();

        // Initial resize
        this.resize();

        // Initial update
        this.update();
    }

    initSVG() {
        log.debug( 'initSVG' );

        // Graph zoom behavior
        const zoom = d3.behavior.zoom()
                       .scaleExtent( [0.1, 3] )
                       .on( 'zoom', this.zoom.bind( this ) );

        // Main drawing svg
        this.svg = this.graph.append( 'svg' )
                       .call( zoom )
                       .on( 'dblclick.zoom', null );

        // Drawing canvas
        this.canvas = this.svg.append( 'g' )
                          .on( 'mousedown', () => d3.event.stopPropagation() ); // Prevents panning while dragging a node
    }

    initLayout() {
        log.debug( 'initLayout' );
    }

    resize() {
        log.debug( 'resize' );

        this.width      = this.el.clientWidth * this.scale;
        this.halfWidth  = this.width / 2;
        this.height     = this.el.clientHeight * this.scale;
        this.halfHeight = this.height / 2;

        this.updateConstraints();
        this.updateLayout();

        if ( this.force ) {
            this.force.size( [this.width, this.height] ).resume();
        }
    }

    zoom() {
        log.debug( "zoom" );

        this.canvas.attr( 'transform', 'translate(' + d3.event.translate + ') scale(' + d3.event.scale + ')' );

        //this.scale = d3.event.scale;
        //this.canvas.attr( 'transform', 'translate(' + d3.event.translate + ')' );
        //this.resize();
    }

    update( props = null ) {
        log.debug( 'update' );

        if ( props ) {
            this.props = props;
        }

        this.updateConstraints();
        this.updateData();
        this.updateLayout();
        this.startForces()
    }

    updateConstraints() {
        log.debug( 'updateConstraints' );
    }

    updateData() {
        log.debug( 'updateData' );
    }

    updateLayout() {
        log.debug( 'updateLayout' );
    }

    startForces() {
        log.debug( 'startForces' );

        if ( !this.force ) {
            return;
        }

        this.force.start();
        if ( !this.started ) {
            for ( var i = 0; i < 30; i++ ) {
                this.force.tick();
            }
            this.started = true;
        }
    }

    destroy() {
        log.debug( 'destroy' );

        this.force.stop();
        this.force = null;
        d3.select( window ).on( 'resize', null );

        // Remove the graph
        this.graph.remove();

        this.props = null;
    }
}
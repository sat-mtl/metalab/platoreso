"use strict";

import React, { Component, PropTypes } from "react";
import classNames from "classnames";
import prune from "underscore.string/prune";
import moment from "moment";
import { Link } from "react-router";
import ProjectCollection from "/imports/projects/ProjectCollection";
import ProjectSecurity from "/imports/projects/ProjectSecurity";
import ModerationMethods from "/imports/moderation/ModerationMethods";
import DateUtil from "../../../lib/utils/DateUtil";
import Scrollable from "../../../components/utils/Scrollable";
import ProjectTimeline from "../common/ProjectTimeline";
import DropDown from "../../../components/semanticui/modules/Dropdown";

export default class Sidebar extends Component {

    constructor( props ) {
        super( props );

        this.renderPhaseShortcut   = this.renderPhaseShortcut.bind( this );
        this.renderTwitterShortcut = this.renderTwitterShortcut.bind( this );
        this.reportProject         = this.reportProject.bind( this );

        this.state = {
            phase: null
        };
    }

    reportProject() {
        const { project } = this.props;
        if ( !project ) {
            return;
        }

        ModerationMethods.report.call( {
            entityTypeName: ProjectCollection.entityName,
            entityId:       project._id
        } );
    }

    /**
     * Render the phase list item
     *
     * @param {object} phase
     * @param {number} index
     */
    renderPhaseShortcut( phase, index ) {
        //TODO: Date from server would be better here because of timezones and stuff
        const now    = moment();
        const start  = phase.startDate ? moment( phase.startDate ) : null;
        const end    = phase.endDate ? moment( phase.endDate ) : null;
        const over   = end && end.isBefore( now );
        const hidden = _.contains( this.props.hiddenPhases, phase._id );
        const active = this.state.phase && this.state.phase._id == phase._id;
        const color  = colorCollection[phase.index % colorCollection.length];

        return (
            <div key={phase._id}
                 className={classNames( "phase", { over: over, hidden: hidden, active: active } )}
                 onClick={() => this.props.togglePhase( phase )}
                 onMouseEnter={() => this.setState( { phase: phase } )}
                 onMouseLeave={() => this.setState( { phase: null } )}
                 style={ active ? { color: color } : null }>
                <div className="infos">
                    <div className="name">{phase.name}</div>
                    <div className="date">{ over ? __( 'Phase is over' ) : DateUtil.getShortDateRange( start, end, now ) }</div>
                </div>
                <div className="actions">
                    <div className="visibility">
                        <i className={classNames( {
                            hide:   hidden,
                            unhide: !hidden
                        }, 'icon' )} style={{ color: color }}/>
                    </div>
                </div>
            </div>
        );
    }

    renderTwitterShortcut() {
        const { project, twitterShown } = this.props;

        return (
            <div className="hashtag"
                 onClick={this.props.toggleTwitter}>
                <div className="infos">
                    <div className="name">{__( 'Twitter feed' )} #{project.hashtag}</div>
                </div>
                <div className="actions">
                    <div className="visibility">
                        <i className={classNames( { hide: !twitterShown, unhide: twitterShown }, 'icon' )}/>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        const { user, project, projectImage, toggleSidebar, showSidebar } = this.props;
        const { phase }                                                   = this.state;

        const headerStyle = {
            backgroundColor: projectImage && projectImage.dominantColor ? '#' + projectImage.dominantColor : colorCollection[project.slug.length % colorCollection.length],
            backgroundImage: projectImage ? 'url(' + projectImage.url( { store: projectImage.collectionName + '_small' } ) + ')' : 'none'
        };

        const canEdit     = user && ProjectSecurity.canEdit( user._id, project._id );
        const youReported = user && project.isReportedBy( user._id );

        return (
            <aside className={classNames( "sidebar", { hidden: !showSidebar } )}>

                {/* Toolbar */}
                <div className="toolbar ui inverted menu">

                    {/* Show/Hide Sidebar */}
                    <div className="hide link item" onClick={toggleSidebar}>
                        <i className="outdent icon"/>
                        {__( 'Hide Sidebar' )}
                    </div>

                    <div className="right menu">
                        <div className="board link item" onClick={toggleSidebar}>
                            <i className="indent icon"/>
                            {__( 'Back to Board' )}
                        </div>

                        {/*<DropDown className="inverted item">
                            <i className="ellipsis vertical icon"/>
                            <div className="menu">

                                { canEdit &&
                                  <div className="add phase item" onClick={this.props.createPhase}>
                                      <i className="add icon"/>
                                      {__( 'Add Phase' )}
                                  </div> }


                                { canEdit &&
                                  <Link to={`/project/${project.uri}/edit`} className="edit item" activeClassName="active">
                                      <i className="pencil icon"/>
                                      {__( 'Edit project' )}
                                  </Link> }

                                <div className="report item" onClick={this.reportProject}>
                                    <i className="legal icon"/>
                                    { youReported ? __( 'You reported this' ) : __( 'Report' ) }
                                </div>

                            </div>
                        </DropDown>*/}
                    </div>

                </div>

                <Scrollable className="sidebar-scroll">

                    <Link to={"/project/" + (project.slug || project._id)}>
                        <header className="heading" style={headerStyle}>
                            <h2 className="ui center aligned inverted header">
                                <div className="content">
                                    { project.name }
                                    <div className="sub header">
                                        <p>{ prune( project.description, 140 ) }</p>
                                    </div>
                                </div>
                            </h2>
                        </header>
                    </Link>

                    { !_.isEmpty( project.phases ) && <ProjectTimeline project={project} selectedPhase={phase}/> }

                    <div className="phase-list">
                        { project.phases && project.phases.map( this.renderPhaseShortcut ) }
                        { project.hashtag && this.renderTwitterShortcut() }
                    </div>

                    {/* Menu */}
                    <div className="ui padded grid">
                        <div className="sixteen wide column">

                            <div className="ui fluid secondary inverted tiny vertical menu">

                                { canEdit &&
                                  <div className="link item" onClick={this.props.createPhase}>
                                      <i className="add icon"/>
                                      {__( 'Add Phase' )}
                                  </div> }

                                { canEdit &&
                                  <Link to={`/project/${project.uri}/edit`} className="item" activeClassName="active">
                                      <i className="pencil icon"/>
                                      { __( 'Edit Project' ) }
                                  </Link>
                                }

                                <div className="link item" onClick={this.reportProject}>
                                    <i className="legal icon"/>
                                    { youReported ? __( 'You reported this' ) : __( 'Report' ) }
                                </div>

                            </div>

                        </div>

                    </div>

                </Scrollable>

            </aside>
        );
    }
}

Sidebar.propTypes = {
    user:          PropTypes.object,
    project:       PropTypes.object.isRequired,
    projectImage:  PropTypes.object,//.isRequired,
    hiddenPhases:  PropTypes.arrayOf( PropTypes.string ).isRequired,
    togglePhase:   PropTypes.func.isRequired,
    toggleTwitter: PropTypes.func.isRequired,
    twitterShown:  PropTypes.bool.isRequired,
    createPhase:   PropTypes.func.isRequired,
    toggleSidebar: PropTypes.func.isRequired,
    showSidebar:   PropTypes.bool.isRequired
};
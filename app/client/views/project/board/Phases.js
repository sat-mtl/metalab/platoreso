"use strict";

import React, { Component, PropTypes } from "react";
import classNames from "classnames";
import { ContextMenu, MenuItem, SubMenu, connect } from "react-contextmenu";
import { Link } from "react-router";

import CardCollection from "/imports/cards/CardCollection";
import CardMethods from "/imports/cards/CardMethods";
import CardSecurity from "/imports/cards/CardSecurity";

import { ConnectMeteorData } from '../../../lib/ReactMeteorData';
import DropDown from '../../../components/semanticui/modules/Dropdown';

import BoardView from './BoardView';
import Tweets from './phases/Tweets';
import Phase from './phases/Phase';

const layoutStorage        = new StoredVar( 'pr.board.layout' );
const sortByStorage        = new StoredVar( 'pr.board.sortBy' );
const sortDirectionStorage = new StoredVar( 'pr.board.sortDirection' );
const historyStorage       = new StoredVar( 'pr.board.showHistory' );
const linkBadgesStorage    = new StoredVar( 'pr.board.showLinkBadges' );

//The context-menu to be triggered
class CardContextMenuComponent extends Component {

    constructor( props ) {
        super( props );
        this.moveToPhase = this.moveToPhase.bind( this );
        this.showCard    = this.showCard.bind( this );
        this.editCard    = this.editCard.bind( this );
        this.pinCard     = this.pinCard.bind( this );
        this.unpinCard   = this.unpinCard.bind( this );
    }

    showCard() {
        const { item: card, showCard } = this.props;
        showCard( card.uri );
    }

    editCard() {
        const { item: card, editCard } = this.props;
        editCard( card.uri );
    }

    pinCard() {
        const { item: card } = this.props;
        CardMethods.pin.call( { cardId: card.id } );
    }

    unpinCard() {
        const { item: card } = this.props;
        CardMethods.unpin.call( { cardId: card.id } );
    }

    moveToPhase( phaseId ) {
        const { item: card } = this.props;
        CardMethods.moveToPhase.call( { cardId: card.id, phaseId: phaseId } );
    }

    render() {
        const { canContribute, canEditProject, canEditCard, project, item: card } = this.props;

        return (
            <ContextMenu identifier="card" currentItem={this.currentItem}>
                <MenuItem onClick={this.showCard}>
                    {__( 'Open' )}
                </MenuItem>
                { canEditCard && <MenuItem onClick={this.editCard}>
                    {__( 'Edit' )}
                </MenuItem> }
                <div className="divider"></div>
                { canEditProject && card.current && !card.pinned && <MenuItem onClick={this.pinCard}>
                    {__( 'Pin card' )}
                </MenuItem> }
                { canEditProject && card.current && card.pinned && <MenuItem onClick={this.unpinCard}>
                    {__( 'Unpin card' )}
                </MenuItem> }
                <div className="divider"></div>
                { card.current && canContribute && <SubMenu title={__( 'Move to phase' )}>
                    { project.phases
                             .filter( phase => phase._id != card.phase && phase.isActive )
                             .map( phase =>
                                 <MenuItem key={phase._id} onClick={() => this.moveToPhase( phase._id )}>
                                     <span className="phase-menu-dot" style={{ backgroundColor: colorCollection[phase.index % colorCollection.length] }}/>
                                     {phase.name}
                                 </MenuItem>
                             )}
                </SubMenu> }
            </ContextMenu>
        );
    }
}

export const WrappedCardContextMenuComponent = ConnectMeteorData( ( { item:card } ) => ({
    // Shortcut: Not editable if it is a snapshot
    canEditCard: card ? ( card.current && CardSecurity.canEdit( Meteor.userId(), card.id ) ) : false
}) )( CardContextMenuComponent );

const CardContextMenu = connect( WrappedCardContextMenuComponent );

class Phases extends BoardView {

    //region Not render()

    constructor( props ) {
        super( props );

        this.dragCard    = this.dragCard.bind( this );
        this.search      = this.search.bind( this );
        this.resetSearch = this.resetSearch.bind( this );
        this.renderEmpty = this.renderEmpty.bind( this );

        this.state = {
            draggedCard:   null,
            search:        "",
            filteredByTag: null
        };
    }

    /**
     * Set a card as being dragged
     */
    dragCard( card ) {
        this.setState( {
            draggedCard: card
        } );
    }

    /**
     * Change the board layout
     *
     * @param layout
     */
    changeLayout( layout ) {
        layoutStorage.set( layout );
    }

    /**
     * Change the board sort field
     *
     * @param field
     */
    sortBy( field ) {
        sortByStorage.set( field );
    }

    /**
     * Change the board sort direction
     *
     * @param direction
     */
    sortDirection( direction ) {
        sortDirectionStorage.set( direction );
    }

    /**
     * Toggle history visibility
     */
    toggleHistory() {
        historyStorage.set( !historyStorage.get() );
    }

    /**
     * Toggle link badges
     */
    toggleLinkBadges() {
        linkBadgesStorage.set( linkBadgesStorage.get() != null ? !linkBadgesStorage.get() : false );
    }


    search( event ) {
        this.setState( {
            search: event.target.value
        } );
    }

    resetSearch() {
        this.setState( {
            search: ""
        } );
    }

    /**
     * Render the loading animation
     */
    renderLoading() {
        return (
            <div className="ui active dimmer">
                <div className="ui large indeterminate text loader">
                    {__( 'Loading Project' )}
                </div>
            </div>
        );
    }

    //endregion

    renderEmpty() {
        const { canEdit, createPhase } = this.props;
        return (
            <div className="empty panel">
                <div className="message">{__( 'This project does not contain any phases yet.' )}</div>
                { canEdit &&
                  <div className="actions">
                      <button className="ui olive right labeled icon button" onClick={createPhase}>
                          <i className="add icon"/>
                          {__( "Add phase" )}
                      </button>
                  </div>
                }
            </div>
        );
    }

    /**
     * Render the board
     */
    render() {
        const { draggedCard, search } = this.state;
        const {
                  project,
                  hiddenPhases,
                  togglePhase,
                  editPhase,
                  archivePhase,
                  layout,
                  sortedBy,
                  sortedDirection,
                  showHistory,
                  canContribute,
                  canEdit,
                  createCard,
                  editCard,
                  linkCards,
                  showCard,
                  showTwitter,
                  toggleTwitter,
                  showLinkBadges,
                  showSidebar,
                  toggleSidebar
              }                       = this.props;

        const filteredPhases = project.phases ? project.phases.filter( phase => !_.contains( hiddenPhases, phase._id ) ) : [];
        return (
            <div className={classNames( "phasesDisplay board view", { nagged: !_.isEmpty( search ) } )}>

                {/* Toolbar */}
                <div className="toolbar ui inverted menu">

                    {/* Show/Hide Sidebar */}
                    { !showSidebar &&
                      <div className="ui link item" onClick={toggleSidebar}>
                          <i className="indent icon"/>
                          <span className="optional-text">{__( 'Show Sidebar' )}</span>
                      </div>
                    }

                    <Link to={`/project/${project.uri}/board`} className="board item" activeClassName="active">
                        <i className="columns icon"/>
                        <span className="optional-text">{__( 'Board' )}</span>
                    </Link>

                    <Link to={`/project/${project.uri}/graph`} className="graph item" activeClassName="active">
                        <i className="sitemap icon"/>
                        <span className="optional-text">{__( 'Graph' )}</span>
                    </Link>

                    {/* Display */}
                    <DropDown className="ui dropdown item">
                        <i className="block layout icon"/>
                        <span className="optional-text">{__( 'Display' )}</span>
                        <i className="dropdown icon"/>
                        <div className="menu">
                            <div className="header">
                                {__( 'Display' )}
                            </div>
                            <div className={classNames( 'list view option item', { active: layout == 'list' } )}
                                 onClick={() => this.changeLayout( 'list' )}>
                                <i className="list layout icon"/>
                                {__( 'List' )}
                            </div>
                            <div className={classNames( 'grid view option item', { active: layout == 'grid' } )}
                                 onClick={() => this.changeLayout( 'grid' )}>
                                <i className="grid layout icon"/>
                                {__( 'Grid' )}
                            </div>
                            <div className={classNames( 'block view option item', { active: layout == 'block' } )}
                                 onClick={() => this.changeLayout( 'block' )}>
                                <i className="block layout icon"/>
                                {__( 'Full' )}
                            </div>
                            <div className="header">
                                {__( 'Options' )}
                            </div>
                            <div className={classNames( 'ghosts view option item', { active: showHistory } )}
                                 onClick={this.toggleHistory}>
                                <i className="history icon"/>
                                {__( 'Show card history' )}
                            </div>
                            <div className={classNames( 'links view option item', { active: showLinkBadges } )}
                                 onClick={this.toggleLinkBadges}>
                                <i className="linkify icon"/>
                                {__( 'Show link badges' )}
                            </div>
                        </div>
                    </DropDown>

                    {/* Sort */}
                    <DropDown className="ui dropdown item">
                        <i className="sort icon"/>
                        <span className="optional-text">{__( 'Sort' )}</span>
                        <i className="dropdown icon"/>
                        <div className="menu">
                            <div className="header">
                                {__( 'Sort By' )}
                            </div>
                            <div className={classNames( 'temperature sort option item', { active: sortedBy == 'temperature' } )}
                                 onClick={() => this.sortBy( 'temperature' )}>
                                <i className="fire icon"/>
                                {__( 'Trending' )}
                            </div>
                            <div className={classNames( 'totalTemperature sort option item', { active: sortedBy == 'totalTemperature' } )}
                                 onClick={() => this.sortBy( 'totalTemperature' )}>
                                <i className="star icon"/>
                                {__( 'Popularity' )}
                            </div>
                            <div className={classNames( 'date sort option item', { active: sortedBy == 'createdAt' } )}
                                 onClick={() => this.sortBy( 'createdAt' )}>
                                <i className="calendar icon"/>
                                {__( 'Date' )}
                            </div>
                            <div className={classNames( 'like sort option item', { active: sortedBy == 'likeCount' } )}
                                 onClick={() => this.sortBy( 'likeCount' )}>
                                <i className="heart icon"/>
                                {__( 'Likes' )}
                            </div>
                            <div className={classNames( 'view sort option item', { active: sortedBy == 'viewCount' } )}
                                 onClick={() => this.sortBy( 'viewCount' )}>
                                <i className="eye icon"/>
                                {__( 'Views' )}
                            </div>
                            <div className={classNames( 'comment sort option item', { active: sortedBy == 'commentCount' } )}
                                 onClick={() => this.sortBy( 'commentCount' )}>
                                <i className="comments icon"/>
                                {__( 'Comments' )}
                            </div>
                            <div className="header">
                                {__( 'Sort Direction' )}
                            </div>
                            <div className={classNames( 'date sort option item', { active: sortedDirection > 0 } )}
                                 onClick={() => this.sortDirection( 1 )}>
                                <i className="sort ascending icon"/>
                                {__( 'Ascending' )}
                            </div>
                            <div className={classNames( 'like sort option item', { active: sortedDirection < 0 } )}
                                 onClick={() => this.sortDirection( -1 )}>
                                <i className="sort descending icon"/>
                                {__( 'Descending' )}
                            </div>
                        </div>
                    </DropDown>

                    {/* Search */}
                    <div className="right menu">
                        <div className="search item">
                            <div className="ui inverted transparent icon input">
                                <input type="text" placeholder="Search..." value={search} onChange={this.search}/>
                                <i className="search link icon"/>
                            </div>
                        </div>
                    </div>
                </div>

                {/* Search nag */}
                { !_.isEmpty( search ) && <div className="search nag">
                    <div className="message">{__( 'Showing search results for' )}:&nbsp;
                        <span className="term">{search}</span></div>
                    <div className="options"><a onClick={this.resetSearch}>{__( 'Close' )}</a></div>
                </div> }

                <div className="panels displayArea">
                    <div className="panels-container">

                        {/* Tweets, only if package available and visible */}
                        { showTwitter &&
                          <Tweets project={project} canContribute={canContribute} togglePanel={toggleTwitter}/> }

                        {/* Phases */ }
                        { filteredPhases.length > 0
                            ? filteredPhases.map( phase => ( <Phase key={phase._id}
                                                                    project={project}
                                                                    phase={phase}
                                                                    canContribute={canContribute}
                                                                    canEdit={canEdit}
                                                                    layout={layout}
                                                                    search={search}
                                                                    sortedBy={sortedBy}
                                                                    sortedDirection={sortedDirection}
                                                                    showHistory={showHistory}
                                                                    showLinkBadges={showLinkBadges}
                                                                    togglePhase={togglePhase}
                                                                    editPhase={editPhase}
                                                                    archivePhase={archivePhase}
                                                                    createCard={createCard}
                                                                    showCard={showCard}
                                                                    editCard={editCard}
                                                                    linkCards={linkCards}
                                                                    dragCard={this.dragCard}
                                                                    draggedCard={draggedCard}/> ) )
                            : this.renderEmpty() }
                    </div>
                </div>

                {/* Contextual Menu */}
                <CardContextMenu canContribute={canContribute}
                                 canEditProject={canEdit}
                                 showCard={showCard}
                                 editCard={editCard}
                                 project={project}/>

            </div>
        );
    }
}

Phases.propTypes = _.defaults( {
    layout:          PropTypes.string.isRequired,
    sortedBy:        PropTypes.string.isRequired,
    sortedDirection: PropTypes.number.isRequired,
    showHistory:     PropTypes.bool.isRequired,
    showLinkBadges:  PropTypes.bool.isRequired
}, BoardView.propTypes );

const PhasesComponent = ConnectMeteorData( props => {
    return {
        layout:          layoutStorage.get() || 'block',
        sortedBy:        sortByStorage.get() || 'createdAt',
        sortedDirection: sortDirectionStorage.get() || -1,
        showHistory:     !!historyStorage.get(), // It might be null, so take no chance,
        showLinkBadges:  linkBadgesStorage.get() != null ? linkBadgesStorage.get() : true, // It might be null, so take no chance
        tags:            CardCollection.getTags( { project: props.project._id } )
    };
} )( Phases );

export default PhasesComponent;

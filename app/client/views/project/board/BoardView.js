"use strict";

import React, { Component, PropTypes } from "react";

export default class BoardView extends Component {

}

BoardView.propTypes = {
    project:       PropTypes.object.isRequired,
    hiddenPhases:  PropTypes.arrayOf( PropTypes.string ).isRequired,
    canContribute: PropTypes.bool.isRequired,
    canEdit:       PropTypes.bool.isRequired,
    showCard:      PropTypes.func.isRequired,
    createCard:    PropTypes.func.isRequired,
    editCard:      PropTypes.func.isRequired,
    linkCards:     PropTypes.func.isRequired,
    togglePhase:   PropTypes.func.isRequired,
    createPhase:   PropTypes.func.isRequired,
    editPhase:     PropTypes.func.isRequired,
    showTwitter:   PropTypes.bool.isRequired,
    toggleTwitter: PropTypes.func.isRequired,
    showSidebar:   PropTypes.bool.isRequired,
    toggleSidebar: PropTypes.func.isRequired
};
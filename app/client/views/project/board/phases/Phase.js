"use strict";

import escapeStringRegexp from "escape-string-regexp";
import React, { Component, PropTypes } from "react";
import Color from "color";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";
import classNames from "classnames";
import moment from "moment";
import { DropTarget } from "react-dnd";
import parseSearch from "/imports/utils/parseSearch";
import CardMethods from "/imports/cards/CardMethods";
import CardCollection from "/imports/cards/CardCollection";
import UserCollection from "/imports/accounts/UserCollection";
import { ConnectMeteorData } from "../../../../lib/ReactMeteorData";
import { DraggableCardPreview } from "./../../../card/CardPreview";
import PhaseScroller from "./phase/PhaseScroller";
import DropDown from "../../../../components/semanticui/modules/Dropdown";

class Phase extends Component {

    constructor( props ) {
        super( props );
        this.createCard    = this.createCard.bind( this );
        this.editPhase     = this.editPhase.bind( this );
        this.archivePhase  = this.archivePhase.bind( this );
        this.togglePhase   = this.togglePhase.bind( this );
        this.renderLoading = this.renderLoading.bind( this );
        this.renderEmpty   = this.renderEmpty.bind( this );
        this.renderCards   = this.renderCards.bind( this );
        this.renderInfo    = this.renderInfo.bind( this );
        this.state         = {
            animationsDisabled: false
        }
    }

    componentWillReceiveProps( nextProps ) {
        this.setState( { animationsDisabled: nextProps.search != this.props.search } );
    }

    createCard() {
        this.props.createCard( this.props.phase._id );
    }

    editPhase() {
        this.props.editPhase( this.props.phase._id );
    }

    archivePhase() {
        this.props.archivePhase( this.props.phase._id );
    }

    togglePhase() {
        this.props.togglePhase( this.props.phase );
    }

    renderEmpty() {
        const { phase, search } = this.props;
        let message             = null;
        if ( search ) {
            message = __( 'Search returned no results' );
        } else if ( phase.isActive ) {
            message = __( 'This phase does not contain any cards yet' );
        } else if ( !phase.hasStarted ) {
            message = __( 'This phase starts __fromNow__', { fromNow: moment( phase.startDate ).fromNow() } );
        } else if ( phase.hasEnded ) {
            message = __( 'This phase ended __fromNow__', { fromNow: moment( phase.endDate ).fromNow() } );
        }
        return (
            <div className="empty">
                <div className="message">{message}</div>
            </div>
        );
    }

    renderLoading() {
        return (
            <div className="empty">
                <div className="ui active large indeterminate loader"></div>
            </div>
        );
    }

    renderCards() {
        const {
                  project,
                  phase,
                  cards,
                  showCard,
                  editCard,
                  canContribute,
                  layout,
                  dragCard,
                  draggedCard,
                  linkCards,
                  showLinkBadges
              }                      = this.props;
        const { animationsDisabled } = this.state;

        return (
            <PhaseScroller>
                <ReactCSSTransitionGroup transitionName="card"
                                         transitionEnter={!animationsDisabled}
                                         transitionEnterTimeout={5000}
                                         transitionLeave={!animationsDisabled}
                                         transitionLeaveTimeout={500}
                                         component="div">
                    {/* Keyed by _id/ref + version
                     This way react doesn't complain about duplicate id and we have the
                     benefit of being able to reuse the node when a card moves, since the snapshot
                     will share the _id/ref and the previous version number */}
                    { cards.map( card =>
                        <DraggableCardPreview key={card.id + "_" + card.version}
                                              project={project}
                                              phase={phase}
                                              card={card}
                                              layout={layout}
                                              showLinkBadges={showLinkBadges}
                                              showCard={showCard}
                                              editCard={editCard}
                                              linkCards={linkCards}
                                              canContribute={canContribute}
                                              dragCard={dragCard}
                                              draggedCard={draggedCard}/>
                    ) }
                    {/* FIXME: Layout grid hack
                     Hack to get flex rows to leave enough empty space
                     We leave the same number of empty cards as there are cards to make sure we have enough
                     */}
                    { (layout == 'grid') && cards.map( ( card, index ) =>
                        <div key={index} className="grid card preview"></div> )}
                </ReactCSSTransitionGroup>
            </PhaseScroller>
        );
    }

    renderInfo() {
        const { cards, phase, search } = this.props;

        let message = null;

        if ( !phase.isActive ) { //&& (cards.length || search ) ) {
            if ( !phase.hasStarted ) {
                message = __( 'This phase starts __fromNow__', { fromNow: moment( phase.startDate ).fromNow() } );
            } else if ( phase.hasEnded ) {
                message = __( 'This phase ended __fromNow__', { fromNow: moment( phase.endDate ).fromNow() } );
            }
        }

        if ( message ) {
            const color = Color(colorCollection[phase.index % colorCollection.length]);
            color.darken(0.25);
            const color2 = color.clone();
            color2.darken(0.125);

            return (
                <footer className="panel-info" style={{ backgroundColor: color.hexString(), borderColor: color2.hexString() }}>
                    <div key="messages" className="messages">
                        <div key="message" className="message">
                            {message}
                        </div>
                    </div>
                </footer>
            );
        } else {
            return null;
        }
    }

    render() {

        const {
                  loading,
                  search,
                  phase,
                  cards,
                  canContribute,
                  canEdit,
                  layout,
                  draggedCard,
                  connectDropTarget
              } = this.props;

        const color = colorCollection[phase.index % colorCollection.length];

        return connectDropTarget(
            <section className={classNames(
                "phase panel layout",
                layout,
                {
                    faded:    !phase.isActive && draggedCard != null,
                    dragging: draggedCard != null,
                    ended:    phase.hasEnded
                }
            )}>
                <header className="panel-header" style={{ backgroundColor: color }}>
                    <div className="panel-name">{phase.name}</div>
                    <div className="actions">
                        { phase.isActive && canContribute &&
                          <div className="add action" onClick={this.createCard}>
                              <i className="plus icon"/>
                          </div> }
                        <DropDown className="inverted action">
                            <i className="ellipsis vertical icon"/>
                            <div className="menu">
                                { canEdit && <div className="edit item" onClick={this.editPhase}>
                                    <i className="pencil icon"/>
                                    {__( 'Edit' )}
                                </div> }
                                { canEdit && <div className="archive item" onClick={this.archivePhase}>
                                    <i className="archive icon"/>
                                    {__( 'Archive' )}
                                </div> }
                                <div className="close item" onClick={this.togglePhase}>
                                    <i className="remove icon"/>
                                    {__( 'Hide' )}
                                </div>
                            </div>
                        </DropDown>
                    </div>
                </header>

                { this.renderInfo() }

                {/* Cards */}
                { !loading && cards.length > 0 || !_.isEmpty( search ) ? this.renderCards() : ( loading ? this.renderLoading() : this.renderEmpty() ) }

            </section>
        );
    }
}

Phase.propTypes = {
    loading:         PropTypes.bool.isRequired,
    cards:           PropTypes.arrayOf( PropTypes.object ).isRequired,
    project:         PropTypes.object.isRequired,
    phase:           PropTypes.object.isRequired,
    togglePhase:     PropTypes.func.isRequired,
    editPhase:       PropTypes.func.isRequired,
    createCard:      PropTypes.func.isRequired,
    showCard:        PropTypes.func.isRequired,
    editCard:        PropTypes.func.isRequired,
    linkCards:       PropTypes.func.isRequired,
    canContribute:   PropTypes.bool.isRequired,
    canEdit:         PropTypes.bool.isRequired,
    layout:          PropTypes.string.isRequired,
    search:          PropTypes.string,
    sortedBy:        PropTypes.string.isRequired,
    sortedDirection: PropTypes.number.isRequired,
    showHistory:     PropTypes.bool.isRequired,
    showLinkBadges:  PropTypes.bool.isRequired,
    dragCard:        PropTypes.func.isRequired,
    draggedCard:     PropTypes.object
};

const DroppablePhase = DropTarget( ['card', 'tweet'], {
    canDrop( props, monitor ) {
        // We act as a move drop target
        const item = monitor.getItem();
        return props.phase.isActive && item.phase != props.phase._id;
    },
    drop( props, monitor, component ) {
        // Default to move when not already dropped on something else
        if ( !monitor.didDrop() ) {
            const item = monitor.getItem();
            const type = monitor.getItemType();
            switch ( type ) {
                case 'card':
                    //CardMethods.copyToPhase.call( { cardId: item.id, phaseId: props.phase._id } );
                    CardMethods.moveToPhase.call( { cardId: item.id, phaseId: props.phase._id } );
                    break;

                case 'tweet':
                    CardMethods.createFromTweet.call( {
                        tweetId:   item.id,
                        projectId: props.project._id,
                        phaseId:   props.phase._id
                    } );
                    break;
            }
        }
    }
}, ( connect, monitor ) => ({
    connectDropTarget: connect.dropTarget()
}) )( Phase );

/**
 * Meteor Data Wrapper
 */
export default ConnectMeteorData( props => {

    let data = {
        loading: !Meteor.subscribe( 'project/card/list', props.project._id, props.phase._id, {
            showCardHistory: props.showHistory,
            linked:          true
        } ).ready()
    };

    let query = { project: props.project._id, phase: props.phase._id };

    if ( !props.showHistory ) {
        query.current = true;
    }

    /**
     * Search
     * It will search only locally, so only the excerpt is available to search not the complete content.
     *
     */
    if ( !_.isEmpty( props.search ) ) {
        query.$and = [];

        const search = parseSearch( props.search, ["name", "title", "content", "body", "tag", "tags", "author", "user"] );

        if ( search.commands ) {
            search.commands.forEach( command => {
                const commandRegExp = { $regex: escapeStringRegexp( command.term ), $options: 'i' };
                let and             = null;
                switch ( command.field ) {
                    case "name":
                    case "title":
                        and = { name: commandRegExp };
                        break;
                    case "content":
                    case "body":
                        and = {
                            $or: [
                                { excerpt: commandRegExp },
                                { content: commandRegExp }
                            ]
                        };
                        break;
                    case "tags":
                    case "tag":
                        and = { tags: commandRegExp };
                        break;
                    case "author":
                    case "user":
                        and = {
                            author: {
                                $in: _.pluck( UserCollection.find(
                                    {
                                        $or: [{ 'profile.firstName': commandRegExp }, { 'profile.lastName': commandRegExp }]
                                    },
                                    {
                                        fields: { _id: 1 }
                                    }
                                ).fetch(), '_id' )
                            }
                        };
                        break;
                }
                if ( and ) {
                    query.$and.push( and );
                }
            } );
        }

        if ( search.terms.length > 0 ) {
            const termsRegExp = {
                $regex:   search.terms.map( term => escapeStringRegexp( term ) ).join( '|' ),
                $options: 'i'
            };
            const users       = UserCollection.find( { $or: [{ 'profile.firstName': termsRegExp }, { 'profile.lastName': termsRegExp }] }, { fields: { _id: 1 } } ).fetch();
            query.$and.push( {
                $or: [
                    { name: termsRegExp },
                    { excerpt: termsRegExp },
                    { content: termsRegExp },
                    { tags: termsRegExp },
                    { author: { $in: _.pluck( users, '_id' ) } }
                ]
            } );
        }
    }

    /*
     Here we don't use mongo sorting because sorting by virtual fields is not possible.
     So we just sort manually. And also we have to pin some cards to the top.
     */
    data.cards = CardCollection
        .find( query )
        .fetch()
        .sort( ( card_a, card_b ) => {
            if ( card_a.pinned && !card_b.pinned ) {
                return -1;
            } else if ( !card_a.pinned && card_b.pinned ) {
                return 1;
            }

            const a = card_a[props.sortedBy];
            const b = card_b[props.sortedBy];
            if ( a > b ) {
                return props.sortedDirection;
            }
            if ( a < b ) {
                return props.sortedDirection * -1;
            }
            return 0;
        } );

    return data;
} )
( DroppablePhase );

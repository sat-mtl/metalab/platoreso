"use strict";

import moment from "moment";
import { DragSource, DropTarget } from 'react-dnd';

import React, { Component, PropTypes } from "react";

class Tweet extends Component {
    render() {
        const {tweet} = this.props;
        const { connectDragSource, isDragging } = this.props;

        const tweetHtml = { __html: tweet.htmlText };

        return connectDragSource(
            <div key={tweet._id} className="tweet">
                <div className="text" dangerouslySetInnerHTML={tweetHtml}></div>

                {/* CARD META */}
                <div className="meta">
                    <a href={'https://twitter.com/'+tweet.user.screen_name} className="profile">
                        { tweet.user.profile_image_url
                            ? <img src={tweet.user.profile_image_url} className="ui avatar image"/>
                            : <i className="profile circular inverted grey twitter icon"/>
                            }
                    </a>
                    <div className="info">
                        <div className="author">
                            <a href={'https://twitter.com/'+tweet.user.screen_name}>@{tweet.user.screen_name}</a>
                        </div>
                        <div className="date">{ moment(tweet.timestamp_ms).fromNow() }</div>
                    </div>
                </div>
            </div>
        );
    }
}

Tweet.propTypes = {
    tweet: PropTypes.object.isRequired,
    canContribute: PropTypes.bool.isRequired,
    // Drag & Drop
    connectDragSource: PropTypes.func.isRequired,
    isDragging: PropTypes.bool.isRequired
};

const TweetComponent = _.compose(

    DragSource('tweet', {
        canDrag(props, monitor) {
            return props.canContribute;
        },
        beginDrag(props, monitor, component) {
            return {
                id: props.tweet._id
            };
        },
        endDrag(props, monitor, component) {

        }
    }, (connect, monitor) => ({
        connectDragSource: connect.dragSource(),
        connectDragPreview: connect.dragPreview(),
        isDragging: monitor.isDragging()
    }))

)(Tweet);

export default TweetComponent;
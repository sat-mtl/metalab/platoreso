"use strict";

import React, {Component, PropTypes} from "react";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";
import TweetCollection from "/imports/twitter/TweetCollection";
import {ConnectMeteorData} from "../../../../lib/ReactMeteorData";
import Scrollable from "../../../../components/utils/Scrollable";
import Tweet from "./tweets/Tweet";

export class Tweets extends Component {

    getMeteorData() {

    }

    render() {
        const { togglePanel, canContribute, tweetsLoading, tweets } = this.props;

        return (
            <section className="twitter panel">
                <header className="panel-header">
                    <div className="panel-name">{__( 'Tweets' )}</div>
                    <div className="actions">
                        <div className="close action" onClick={togglePanel}>
                            <i className="remove icon"/>
                        </div>
                    </div>
                </header>

                <div className="tweetsScroller panelScroller">
                    <Scrollable className="scroller">
                        <div className="tweets-container scrolledContainer">
                            <ReactCSSTransitionGroup transitionName="tweet"
                                                     transitionEnter={!tweetsLoading}
                                                     transitionEnterTimeout={500}
                                                     transitionLeaveTimeout={500}
                                                     component="div">
                                { tweets.map( tweet => (
                                    <Tweet key={tweet._id} tweet={tweet} canContribute={canContribute}/>
                                ) ) }
                            </ReactCSSTransitionGroup>
                        </div>
                    </Scrollable>
                </div>

                <footer className="panel-footer">

                </footer>

            </section>
        );
    }
}

Tweets.propTypes = {
    project:       PropTypes.object.isRequired,
    canContribute: PropTypes.bool.isRequired,
    togglePanel:   PropTypes.func.isRequired,
    tweetsLoading: PropTypes.bool.isRequired,
    tweets:        PropTypes.arrayOf( PropTypes.object )
};

export default ConnectMeteorData( props => {
    const { hashtag } = props.project;
    return {
        tweetsLoading: !Meteor.subscribe( 'tweets', hashtag ).ready(),
        tweets:        TweetCollection.find( {
            hashtags: hashtag.toLowerCase()
        }, {
            sort: {
                timestamp_ms: -1
            }
        } ).fetch()
    };
} )( Tweets );
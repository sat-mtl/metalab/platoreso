"use strict";

import React, { Component, PropTypes } from "react";
import ReactDOM from "react-dom";

import { DropTarget } from 'react-dnd';
import Scrollable from '../../../../../components/utils/Scrollable';

export class PhaseScroller extends Component {

    constructor( props ) {
        super( props );
        this.state = {
            scrollVelocity: null
        }
    }

    render() {
        const { connectDropTarget, isOver } = this.props;
        const { scrollVelocity } = this.state;
        return connectDropTarget(
            <div className="phaseScroller panelScroller">
                <Scrollable className="scroller" fixBottom={true} scrollVelocity={isOver ? scrollVelocity : 0}>
                    { this.props.children }
                </Scrollable>
            </div>
        );
    }

}

PhaseScroller.propTypes = {
    connectDropTarget: PropTypes.func.isRequired,
    isOver:            PropTypes.bool.isRequired
};

PhaseScroller.defaultProps = {
    disabled: false
};

export default DropTarget( ['card', 'tweet'], {
    canDrop( /* props, monitor */ ) {
        // This target is only used to know if we are
        // over the general area, we can't drop on it
        return false;
    },
    hover( props, monitor, component ) {
        const offset       = monitor.getClientOffset();
        const bounds       = ReactDOM.findDOMNode( component ).getBoundingClientRect();
        const percent      = (offset.y - bounds.top) / ( bounds.height );
        let scrollVelocity = null;

        if ( percent < 0.25 ) {
            scrollVelocity = -( 0.25 - percent ) / 0.25;
        } else if ( percent > 0.75 ) {
            scrollVelocity = ( percent - 0.75 ) / 0.25;
        }

        if ( component.state.scrollVelocity != scrollVelocity ) {
            component.setState( {scrollVelocity: scrollVelocity} );
        }
    }
}, ( connect, monitor ) => ({
    connectDropTarget: connect.dropTarget(),
    isOver:            monitor.isOver()
}) )( PhaseScroller );
"use strict";

import ProjectCollection from "/imports/projects/ProjectCollection";
import ProjectImagesCollection from "/imports/projects/ProjectImagesCollection";
import ProjectSecurity from "/imports/projects/ProjectSecurity";
import { ConnectMeteorData } from "../../lib/ReactMeteorData";
import NotFound from "../errors/NotFound";
import Sidebar from "./board/Sidebar";
import ContentPanel from "./board/ContentPanel";
import CardsDragLayer from "./common/CardsDragLayer";
import { CreateCardModal } from "../card/CreateCard";
import CardEditorModal from "../card/CardEditorModal";
import CardModal from "../card/CardModal";
import { PhaseEditModal } from "./phases/PhaseEdit";
import { PhaseArchiveModal } from "./phases/PhaseArchive";
import CardLinkModal from "../card/CardLinkModal";
import Phases from "./board/Phases";
import Graph from "./board/Graph";
import React, { Component, PropTypes } from "react";
import update from "react-addons-update";
import classNames from "classnames";

const hiddenPhasesStorage = new StoredVar( 'pr.board.hiddenPhases' );
const showTwitterStorage  = new StoredVar( 'pr.board.showTwitter' );
const showSidebarStorage  = new StoredVar( 'pr.board.showSidebar' );

export class Board extends Component {

    constructor( props ) {
        super( props );

        this.createCard             = this.createCard.bind( this );
        this.finishedCreatingCard   = this.finishedCreatingCard.bind( this );
        this.showCard               = this.showCard.bind( this );
        this.finishedShowingCard    = this.finishedShowingCard.bind( this );
        this.editCard               = this.editCard.bind( this );
        this.finishedEditingCard    = this.finishedEditingCard.bind( this );
        this.linkCards              = this.linkCards.bind( this );
        this.finishedLinkingCards   = this.finishedLinkingCards.bind( this );
        this.createPhase            = this.createPhase.bind( this );
        this.editPhase              = this.editPhase.bind( this );
        this.finishedEditingPhase   = this.finishedEditingPhase.bind( this );
        this.archivePhase           = this.archivePhase.bind( this );
        this.finishedArchivingPhase = this.finishedArchivingPhase.bind( this );
        this.toggleTwitter          = this.toggleTwitter.bind( this );
        this.togglePhase            = this.togglePhase.bind( this );
        //this.toggleContentPanel     = this.toggleContentPanel.bind( this );
        this.toggleSidebar = this.toggleSidebar.bind( this );

        this.state = {
            //contentPanelOpened:  false,
            cardCreationPhaseId: null,
            editedCardId:        null,
            editPhase:           null,
            archivePhaseId:      null,
        }
    }

    /**
     * Create a card
     */
    createCard( phaseId ) {
        const { project, params } = this.props;
        const { router }          = this.context;
        router.push( { pathname: `/project/${project.uri}/${params.view}/create/${phaseId}` } );
    }

    /**
     * Finished creating a card
     */
    finishedCreatingCard() {
        const { project, params } = this.props;
        const { router }          = this.context;
        router.replace( { pathname: `/project/${project.uri}/${params.view}` } );
    }

    /**
     * Show a card
     *
     * Uses history so that we can link directly to a card
     */
    showCard( cardId ) {
        const { project, params } = this.props;
        const { router }          = this.context;
        router.push( { pathname: `/project/${project.uri}/${params.view}/${cardId}` } );
    }

    /**
     * Finished showing a card
     *
     * Uses history so that we can link directly to a card
     */
    finishedShowingCard() {
        const { project, params } = this.props;
        const { router }          = this.context;
        router.push( { pathname: `/project/${project.uri}/${params.view}` } );
    }

    /**
     * Edit a card
     */
    editCard( cardId ) {
        const { project, params } = this.props;
        const { router }          = this.context;
        router.push( { pathname: `/project/${project.uri}/${params.view}/edit/card/${cardId}` } );
    }

    /**
     * Finished editing a card
     */
    finishedEditingCard() {
        const { project, params } = this.props;
        const { router }          = this.context;
        router.replace( { pathname: `/project/${project.uri}/${params.view}` } );
    }

    /**
     * Link Cards
     */
    linkCards( { linkCardId, toCardId } ) {
        this.setState( { linkCardId, toCardId } );
    }

    /**
     * Finished linking cards
     */
    finishedLinkingCards() {
        this.setState( {
            linkCardId: null,
            toCardId:   null
        } );
    }

    /**
     * Create a phase
     */
    createPhase() {
        this.setState( {
            editPhase: {
                name: __( "New Phase" )
            }
        } );
    }

    /**
     * Edit a phase
     *
     * @param phaseId
     */
    editPhase( phaseId ) {
        const { project, params } = this.props;
        const { router }          = this.context;
        router.push( { pathname: `/project/${project.uri}/${params.view}/edit/phase/${phaseId}` } );
    }

    /**
     * Finished editing a phase
     */
    finishedEditingPhase() {
        const { project, params } = this.props;
        const { router }          = this.context;
        router.replace( { pathname: `/project/${project.uri}/${params.view}` } );
        this.setState( {
            editPhase: null
        } );
    }

    /**
     * Archive a phase
     *
     * @param phaseId
     */
    archivePhase( phaseId ) {
        this.setState( {
            archivePhaseId: phaseId
        } );
    }

    /**
     * Finished linking cards
     */
    finishedArchivingPhase() {
        this.setState( {
            archivePhaseId: null
        } );
    }

    /**
     * Toggle the display of a phase on the board
     */
    togglePhase( phase ) {
        let hiddenPhases = this.props.hiddenPhases;
        if ( !_.isArray( hiddenPhases ) ) {
            hiddenPhases = [];
        }
        if ( _.contains( hiddenPhases, phase._id ) ) {
            hiddenPhases = update( hiddenPhases, {
                $splice: [[_.indexOf( hiddenPhases, phase._id ), 1]]
            } );
        } else {
            hiddenPhases = update( hiddenPhases, {
                $push: [phase._id]
            } );
        }

        hiddenPhasesStorage.set( hiddenPhases );
    }

    /**
     * Toggle the display of the twitter feed
     */
    toggleTwitter() {
        showTwitterStorage.set( !this.props.showTwitter );
    }

    /**
     * Toggle the content panel
     */
    /*toggleContentPanel() {
        this.setState( {
            contentPanelOpened: !this.state.contentPanelOpened
        } );
    }*/

    toggleSidebar() {
        showSidebarStorage.set(!this.props.showSidebar);
    }

    /**
     * Render the loading animation
     */
    renderLoading() {
        return (
            <div className="ui active dimmer">
                <div className="ui large indeterminate text loader">
                    {__( 'Loading Project' )}
                </div>
            </div>
        );
    }

    /**
     * Render the board
     */
    render() {
        const { /*contentPanelOpened,*/ editPhase, archivePhaseId, linkCardId, toCardId } = this.state;
        const { user, projectLoading, project, projectImage, hiddenPhases, canContribute, canEdit, showTwitter, showSidebar } = this.props;
        const { cardId, cardVersion, cardCreationPhaseId, editedCardId, editedPhaseId } = this.props.params;

        if ( projectLoading ) {
            // If we are loading show the indicator
            return this.renderLoading();

        } else if ( !project ) {
            // We are neither loading or having a project, so show an error message
            return <NotFound/>

        } else {
            // Get the board view
            let BoardView;
            switch ( this.props.params.view ) {
                case 'board':
                    BoardView = Phases;
                    break;

                case 'graph':
                    BoardView = Graph;
                    break;

                default:
                    //FIXME: This will leave the subscriptions going
                    return <NotFound/>;
                    break;
            }

            // If we have a project, render the board
            return (
                <section id="project-board" className={classNames({'no-sidebar': !showSidebar})}>

                    {/* Sidebar */}
                    <Sidebar
                        user={user}
                        project={project}
                        projectImage={projectImage}
                        hiddenPhases={hiddenPhases}
                        twitterShown={showTwitter}
                        createPhase={this.createPhase}
                        togglePhase={this.togglePhase}
                        toggleTwitter={this.toggleTwitter}
                        toggleSidebar={this.toggleSidebar}
                        showSidebar={showSidebar}
                        changeLayout={this.changeLayout}/>

                    {/* Project Description */}
                    {/*<ContentPanel
                        project={project}
                        opened={contentPanelOpened}
                        toggle={this.toggleContentPanel}
                        showSidebar={showSidebar}/>*/}

                    {/* Content */}
                    <div className="display">
                        <BoardView
                            project={project}
                            hiddenPhases={hiddenPhases}
                            canContribute={canContribute}
                            canEdit={canEdit}
                            showTwitter={showTwitter}
                            toggleTwitter={this.toggleTwitter}
                            togglePhase={this.togglePhase}
                            createPhase={this.createPhase}
                            editPhase={this.editPhase}
                            archivePhase={this.archivePhase}
                            createCard={this.createCard}
                            showCard={this.showCard}
                            editCard={this.editCard}
                            linkCards={this.linkCards}
                            showSidebar={showSidebar}
                            toggleSidebar={this.toggleSidebar}
                        />
                    </div>

                    {/* Edit Phase Modal */}
                    <PhaseEditModal project={project}
                                    phase={editPhase || project.getPhase( editedPhaseId )}
                                    isNewPhase={editPhase != null}
                                    onRequestClose={this.finishedEditingPhase}/>

                    {/* Archive Phase Modal */}
                    <PhaseArchiveModal project={project}
                                       phase={archivePhaseId && project.getPhase( archivePhaseId )}
                                       onRequestClose={this.finishedArchivingPhase}/>

                    {/* Create Modal */}
                    <CreateCardModal project={project}
                                     phaseId={cardCreationPhaseId}
                                     onRequestClose={this.finishedCreatingCard}/>

                    {/* Edit Card Modal */}
                    <CardEditorModal cardId={editedCardId}
                                     onRequestClose={this.finishedEditingCard}/>

                    {/* Show Card Modal */}
                    <CardModal cardId={cardId}
                               cardVersion={cardVersion}
                               canContribute={canContribute}
                               showCard={this.showCard}
                               editCard={this.editCard}
                               onRequestClose={this.finishedShowingCard}/>

                    {/* Link Modal */}
                    <CardLinkModal linkCardId={linkCardId} toCardId={toCardId} onRequestClose={this.finishedLinkingCards}/>

                    {/* Drag layer */}
                    {<CardsDragLayer/>}
                </section>
            );
        }
    }
}

Board.propTypes = {
    user:           PropTypes.object,
    projectLoading: PropTypes.bool.isRequired,
    project:        PropTypes.object,
    projectImage:   PropTypes.object,
    hiddenPhases:   PropTypes.arrayOf( PropTypes.string ).isRequired,
    canContribute:  PropTypes.bool.isRequired,
    canEdit:        PropTypes.bool.isRequired,
    showSidebar:    PropTypes.bool.isRequired
};

Board.contextTypes = {
    router: PropTypes.object.isRequired
};

/**
 * Meteor Data Wrapper
 */
const BoardComponent = ConnectMeteorData( props => {
    const user    = Meteor.user();
    const project = ProjectCollection.findOne( { $or: [{ _id: props.params.id }, { slug: props.params.id }] } );
    return {
        user:           user,
        projectLoading: !Meteor.subscribe( 'project', props.params.id ).ready(),
        project:        project,
        projectImage:   project ? ProjectImagesCollection.findOne( { owners: project._id } ) : null,
        //NOTE: Removed in favor of phase-based subscription -> cardsLoading:   project ? !Meteor.subscribe( 'project/card/list', project._id ).ready() : true,
        canContribute:  user && project ? ProjectSecurity.canContribute( user._id, project._id ) : false,
        canEdit:        user && project ? ProjectSecurity.canEdit( user._id, project._id ) : false,
        hiddenPhases:   hiddenPhasesStorage.get() || [],
        showTwitter:    project && project.hashtag && showTwitterStorage.get() != null ? showTwitterStorage.get() : false,
        showSidebar:    showSidebarStorage.get() != null ? showSidebarStorage.get() : false
    };
} )( Board );

export default BoardComponent;

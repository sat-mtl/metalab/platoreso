"use strict";

import React, { Component, PropTypes } from "react";
import ReactDOM from "react-dom";
import classNames from "classnames";
import async from "async";
import { unflatten } from "flat";
import { FS } from "meteor/cfs:base-package";
import formDiff from "/imports/utils/formDiff";
import ProjectCollection from "/imports/projects/ProjectCollection";
import ProjectMethods from "/imports/projects/ProjectMethods";
import ProjectImagesCollection from "/imports/projects/ProjectImagesCollection";
import ProjectEditorDataWrapper from "./ProjectEditorDataWrapper";
import Modal from "../../components/semanticui/modules/Modal";
import TinyMCE from "../../components/tinymce/TinyMCE";
import Form from "../../components/forms/Form";
import Field from "../../components/forms/Field";
import ProjectForm from "./ProjectForm";

const { File } = FS;

export class ProjectEditor extends Component {

    constructor( props ) {
        super( props );

        this.confirmRemove  = this.confirmRemove.bind( this );
        this.cancelRemove   = this.cancelRemove.bind( this );
        this.remove         = this.remove.bind( this );
        this.pictureChanged = this.pictureChanged.bind( this );
        this.updateProject  = this.updateProject.bind( this );
        this.submitForm     = this.submitForm.bind( this );
        this.onCancel       = this.onCancel.bind( this );
        this.onFinish       = this.onFinish.bind( this );

        this.state = {
            error:             null,
            saving:            null,
            picture:           null,
            confirmingRemoval: false
        }
    }

    pictureChanged( files ) {
        this.setState( {
            picture: ( files && files.length > 0 ) ? files[0] : null
        } );
    }

    confirmRemove( entity ) {
        this.setState( {
            confirmingRemoval: true
        } );
    }

    cancelRemove() {
        this.setState( {
            confirmingRemoval: false
        } );
    }

    remove() {
        this.setState( {
            confirmingRemoval: false
        } );

        ProjectMethods.remove.call( { projectId: this.props.project._id }, ( err ) => {
            if ( err ) {
                this.setState( { error: err } );
            } else {
                this.onFinish( true );
            }
        } );
    }

    submitForm() {
        $( ReactDOM.findDOMNode( this.refs.projectEditForm ) ).form( 'submit' );
    }

    updateProject( values ) {
        if ( this.state.saving ) {
            return;
        }
        this.setState( { saving: true } );

        const { project } = this.props;

        let uploads = [];
        let tasks   = [];

        // Images
        if ( this.state.picture ) {
            let file    = new File( this.state.picture );
            file.owners = [project._id];
            uploads.push( callback => ProjectImagesCollection.insert( file, callback ) );
        }

        // Push upload stack as a task
        tasks.push( callback => async.parallelLimit( uploads, 3, callback ) );

        // Data

        // Get boolean value instead of the "on" string
        values.public       = !!values.public;
        values.showTimeline = !!values.showTimeline;
        // Get tinyMCE content
        values.content      = this.refs.content.getValue();
        // Turn the form values into a proper object and compute diff

        const changes = formDiff( unflatten( values ), project );

        // Call update if we have changes or uploads pending
        if ( !_.isEmpty( changes ) || uploads.length ) {
            tasks.push( callback => ProjectMethods.update.call( {
                _id: project._id,
                     changes
            }, callback ) );
        }

        // Do tasks
        async.series( tasks, ( err, result ) => {
            this.setState( { saving: false } );
            if ( err ) {
                console.error( err );
                this.setState( { error: err } );
            } else {
                this.onFinish();
            }
        } );
    }

    onCancel() {
        this.onFinish();
    }

    onFinish( deleted = false ) {
        if ( this.props.onFinished ) {
            this.props.onFinished();
        } else if ( deleted ) {
            this.context.router.push( { pathname: '/' } );
        } else {
            // Retrieve the project as we might have made changes to the uri and the props is outdated
            const project = ProjectCollection.findOne( { _id: this.props.project._id } );
            if ( project ) {
                this.context.router.push( { pathname: `/project/${project.uri}` } );
            } else {
                this.context.router.goBack();
            }
        }
    }

    renderLoading() {
        return (
            <div className="card details loading">
                <div className="ui large inverted active indeterminate text loader">
                    {__( 'Loading Project' )}
                </div>
            </div>
        );
    }

    renderProject() {
        const { project, image }                           = this.props;
        const { error, saving, confirmingRemoval }         = this.state;

        if ( !project ) {
            return null;
        }

        return (
            <div className={classNames( this.props.className, "editor editProject" )}>

                <header className="ui clearing basic segment">
                    <h2 className="ui left floated header">
                        {__( 'Edit Project' )}
                    </h2>
                    <div className="ui right floated secondary menu">
                        <div className="ui red button" onClick={this.confirmRemove}>
                            <i className="trash icon"/>
                            {__( 'Remove' )}
                        </div>
                        <div className="ui buttons">
                            <div className="ui cancel button" onClick={this.onCancel}>
                                <i className="remove icon"/>
                                {__( 'Cancel' )}
                            </div>
                            <div className={classNames( "ui green button", { loading: saving, disabled: saving } )}
                                 onClick={this.submitForm}>
                                <i className="plus icon"/>
                                {__( 'Save' )}
                            </div>
                        </div>
                    </div>
                </header>

                <Form id={"project-edit-form-" + project._id}
                      ref="projectEditForm"
                      error={error}
                      onSuccess={this.updateProject}>

                    <ProjectForm project={project}
                                 image={image}
                                 pictureChanged={this.pictureChanged}>

                        {/* Content, managed here because we need the ref */}
                        <div className="sixteen wide column">
                            <Field label={__( 'Page' )}>
                                <TinyMCE ref="content" name="content" defaultValue={project.content}/>
                            </Field>
                        </div>

                    </ProjectForm>
                </Form>

                <div className="actions">
                    <div className="ui red button" onClick={this.confirmRemove}>
                        <i className="trash icon"/>
                        {__( 'Remove' )}
                    </div>
                    <div className="ui buttons">
                        <div className="ui cancel button" onClick={this.onCancel}>
                            <i className="remove icon"/>
                            {__( 'Cancel' )}
                        </div>
                        <div className={classNames( "ui green button", { loading: saving, disabled: saving } )}
                             onClick={this.submitForm}>
                            <i className="plus icon"/>
                            {__( 'Save' )}
                        </div>
                    </div>
                </div>

                <Modal
                    opened={ confirmingRemoval }
                    onApprove={ this.remove }
                    onHide={ this.cancelRemove }>
                    <i className="close icon"/>
                    <div className="header">
                        { __( 'Confirm Removal' ) }
                    </div>
                    <div className="content">
                        <div className="description">
                            <p>{__( 'Are you sure you want to remove the project? All cards in the project will be permanently deleted.' )}</p>
                        </div>
                    </div>
                    <div className="actions">
                        <div className="ui buttons">
                            <div className="ui red deny button">
                                <i className="remove icon"/>
                                {__( 'No' )}
                            </div>
                            <div className="ui green approve button">
                                <i className="checkmark icon"/>
                                {__( 'Yes' )}
                            </div>
                        </div>
                    </div>
                </Modal>
            </div>
        );
    }

    render() {
        const { loading } = this.props;
        return loading ? this.renderLoading() : this.renderProject();
    }
}

ProjectEditor.propTypes = {
    loading: PropTypes.bool.isRequired,
    project: PropTypes.object,
    image:   PropTypes.object
};

ProjectEditor.contextTypes = {
    router: PropTypes.object.isRequired
};

export default ProjectEditorDataWrapper( ProjectEditor );
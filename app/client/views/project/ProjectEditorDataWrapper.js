"use strict";

import ProjectCollection from "/imports/projects/ProjectCollection";
import {ConnectMeteorData} from '../../lib/ReactMeteorData'

/**
 * Meteor Data Wrapper
 */
export default ConnectMeteorData( props => {
    const projectLoading = !!( props.projectId && !Meteor.subscribe( 'project', props.projectId ).ready() );
    const project        = props.projectId ? ProjectCollection.findOne( { $or: [{ _id: props.projectId }, { slug: props.projectId }] } ) : null;
    return {
        loading: projectLoading,
        project: project,
        image:   project && project.getImage()
    }
} );
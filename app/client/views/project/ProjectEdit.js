"use strict";

import React, {Component, PropTypes} from "react";
import ContainedPage from '../layouts/ContainedPage';
import Spinner from '../widgets/Spinner';
import NotFound from '../errors/NotFound';
import {ProjectEditor} from './ProjectEditor';
import ProjectEditorDataWrapper from './ProjectEditorDataWrapper';

export const ProjectEdit = ( props ) => {
    if ( props.loading ) {
        return <Spinner/>;

    } else if ( !props.project ) {
        return <NotFound/>;

    } else {
        return (
            <ContainedPage>
                <ProjectEditor className="standalone sixteen wide column" {...props}/>
            </ContainedPage>
        );
    }
};

const DataWrapper = ProjectEditorDataWrapper( ProjectEdit );
export default ( { params } ) => <DataWrapper projectId={params.id}/>;
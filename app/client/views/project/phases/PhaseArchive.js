"use strict";

import React, {Component, PropTypes} from "react";
import ReactDOM from "react-dom";
import {unflatten} from "flat";
import classNames from "classnames";
import ProjectMethods from "/imports/projects/ProjectMethods";
import Modal from "../../../components/Modal";
import Dialog from "../../../components/Dialog";
import Form from "../../../components/forms/Form";
import Field from "../../../components/forms/Field";
import Select from "../../../components/forms/Select";
import Radio from "../../../components/forms/Radio";

export default class PhaseArchive extends Component {

    constructor( props ) {
        super( props );

        // Bind methods, not automatic in current React version when using classes
        this.submitForm   = this.submitForm.bind( this );
        this.archivePhase = this.archivePhase.bind( this );

        // Initial state
        this.state = {
            archiving: false,
            error:     null
        };
    }

    submitForm() {
        $( ReactDOM.findDOMNode( this.refs.projectPhaseArchivalForm ) ).form( 'submit' );
    }

    archivePhase( values ) {
        const { project, phase } = this.props;

        if ( this.state.archiving ) {
            return;
        }
        this.setState( { archiving: true } );

        const removeConfig = {
            projectId: project._id,
            phaseId:   phase._id,
            action:    values.action
        };
        if ( values.action == 'move' ) {
            removeConfig.moveToPhase = values.moveToPhase;
        }
        ProjectMethods.removePhase.call( removeConfig, err => {
            this.setState( { archiving: false } );
            if ( err ) {
                this.setState( { error: err } );
            } else if ( this.props.onFinished ) {
                this.props.onFinished();
            }
        } );
    }

    render() {
        const { project, phase, onFinished } = this.props;
        const { error, archiving }           = this.state;

        const availablePhases = project.phases.filter( p => p._id != phase._id );

        return (
            <div className="editor archivePhase">

                <div className="header">
                    { __( 'Archive Phase' ) }
                    <div className="sub header">{__( 'Please select what you want to do with the cards in the phase you are removing.' )}</div>
                </div>

                <Form id={"project-phase-archival-form-" + phase._id}
                      ref="projectPhaseArchivalForm"
                      key={phase._id}
                      error={error}
                      onSuccess={this.archivePhase}>

                    <Field name="action">
                        <Radio ref="delete"
                               name="action"
                               value="archive"
                               defaultChecked={true}
                               label={__( 'Archive cards' )}/>
                    </Field>
                    <div className="inline fields">
                        <Field name="action">
                            <Radio ref="move"
                                   name="action"
                                   value="move"
                                   disabled={availablePhases.length == 0}
                                   label={__( 'Move cards to another phase' )}/>
                        </Field>
                        <Field name="moveToPhase">
                            <Select id="moveToPhase"
                                    name="moveToPhase"
                                    disabled={availablePhases.length == 0}
                                    className={classNames( { disabled: availablePhases.length == 0 } )}>
                                { availablePhases.map( p =>
                                    <option key={p._id} value={p._id}>{p.name}</option> ) }
                            </Select>
                        </Field>
                    </div>
                </Form>

                <div className="actions">
                    <div className="ui buttons">
                        <div className="ui cancel button" onClick={onFinished}>
                            <i className="remove icon"/>
                            {__( 'Cancel' )}
                        </div>
                        <div className={classNames( "ui red button", {
                            loading:  archiving,
                            disabled: archiving
                        } )} onClick={this.submitForm}>
                            <i className="checkmark icon"/>
                            {__( 'Archive' )}
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

PhaseArchive.propTypes = {
    project:    PropTypes.object.isRequired,
    phase:      PropTypes.object.isRequired,
    onFinished: PropTypes.func
};

export class PhaseArchiveModal extends Component {
    render() {
        const { phase, onRequestClose } = this.props;
        return (
            <Modal opened={ phase != null } onRequestClose={onRequestClose}>
                <Dialog onRequestClose={onRequestClose}>
                    { phase && <PhaseArchive key={phase._id} {...this.props} onFinished={onRequestClose}/> }
                </Dialog>
            </Modal>
        );
    }
}

PhaseArchiveModal.propTypes = {
    onRequestClose: PropTypes.func.isRequired
};
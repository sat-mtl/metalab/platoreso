"use strict";

import React, { Component, PropTypes } from "react";
import classNames from "classnames";
import { DragSource, DropTarget } from 'react-dnd';

import Field from '../../../../../components/forms/Field';
import Input from '../../../../../components/forms/Input';

export class Answer extends Component {

    render() {
        const { question, answer } = this.props;
        const { connectDragSource, connectDragPreview, connectDropTarget, isDragging } = this.props;

        const before = (
            connectDragSource(<i className="move handle sidebar icon"/>)
        );

        const after = (
            <div className="ui red right icon button" onClick={this.props.onRemove}>
                <i className="remove icon"/>
            </div>
        );

        return connectDropTarget( connectDragPreview (
            <div className={ classNames('answer', { dragged: isDragging } ) }>
                <input type="hidden" name={"data.questions.questions."+question._id+".answers."+answer._id+".order"} value={answer.order}/>
                <Field className="required">
                    <Input ref={"data.questions.questions."+question._id+".answers."+answer._id+".prompt"}
                           name={"data.questions.questions."+question._id+".answers."+answer._id+".prompt"}
                           type="text"
                           className="fluid left icon action prompt"
                           defaultValue={answer.prompt}
                           rules={[
                               { type: 'empty', prompt: __('Please enter a answer') }
                           ]}
                           before={before}
                           after={after}/>
                </Field>
                <div className="ui hidden divider"></div>
            </div>
        ) );
    }
}

Answer.propTypes = {
    // Answer
    question: PropTypes.object.isRequired,
    answer: PropTypes.object.isRequired,
    // List
    onRemove: PropTypes.func.isRequired,
    // Drag & Drop
    connectDragSource: PropTypes.func.isRequired,
    connectDragPreview: PropTypes.func.isRequired,
    connectDropTarget: PropTypes.func.isRequired,
    isDragging: PropTypes.bool.isRequired,
    // Drag & Drop Support
    moveItem: PropTypes.func.isRequired,
    restoreItem: PropTypes.func.isRequired
};

export default _.compose(

    DragSource('answer', {
        /* SOURCE */

        beginDrag(props) {
            return { _id: props.answer._id, question: props.question._id };
        },

        endDrag(props, monitor, component) {
            if (!monitor.didDrop()) {
                props.restoreItem();
            }
        }
    }, (connect, monitor) => ({
        connectDragSource: connect.dragSource(),
        connectDragPreview: connect.dragPreview(),
        isDragging: monitor.isDragging()
    })),

    DropTarget('answer', {
        /* TARGET */

        canDrop() {
            // We can't drop on the dummy target
            return false;
        },

        hover(props, monitor, component) {
            // Simulate the reordering on hover
            const item = monitor.getItem();
            if ( item._id === props.answer._id || item.question != props.question._id ) {
                return;
            }
            props.moveItem( item, props.answer );
        }
    }, connect => ({
        connectDropTarget: connect.dropTarget()
    }))

)(Answer);
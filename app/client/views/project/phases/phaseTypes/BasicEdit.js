"use strict";

import React, { Component, PropTypes } from "react";

export default class BasicEdit extends Component {
    render() {
        return (
            <div className="ui message">
                <p>{__('Card creator will be able to add text and images to a card.')}</p>
            </div>
        );
    }
}
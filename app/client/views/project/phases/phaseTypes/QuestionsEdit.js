"use strict";

import Immutable from 'immutable';

import React, { Component, PropTypes } from "react";
import { DragSource, DropTarget } from 'react-dnd';

import Modal from '../../../../components/semanticui/modules/Modal';

import Form from '../../../../components/forms/Form';
import Field from '../../../../components/forms/Field';
import Input from '../../../../components/forms/Input';

import Question from './questions/Question';

const QuestionRecord = Immutable.Record({
    __new: false,
    _id:null,
    order:-1,
    prompt: '',
    required: false,
    answers: Immutable.List()
});

const AnswerRecord = Immutable.Record({
    __new: false,
    _id:null,
    order:-1,
    prompt: ''
});

export class QuestionsEdit extends Component {

    constructor( props ) {
        super( props );

        this.addQuestion = this.addQuestion.bind(this);
        this.moveQuestion = this.moveQuestion.bind(this);
        this.resetQuestionsOrder = this.resetQuestionsOrder.bind(this);
        this.confirmRemoveQuestion = this.confirmRemoveQuestion.bind(this);
        this.removeQuestion = this.removeQuestion.bind(this);

        this.addAnswer = this.addAnswer.bind(this);
        //this.moveAnswer = this.moveAnswer.bind(this);

        this.state = {
            questions: QuestionsEdit.getQuestionsFromProps(props),
            removedQuestions: Immutable.List(),
            removeQuestion: null,
            movedQuestionIndex: -1
        }
    }

    /**
     * Get the questions as an indexed Immutable List from the props
     *
     * @param props
     * @returns {*}
     */
    static getQuestionsFromProps(props) {
        const { phase } = props;

        if ( !phase.data || !phase.data.questions || !phase.data.questions.questions ) {
            return Immutable.List();
        }

        // Return an immutable list of also immutable records so that modifications don't affect the real object
        const questions = Immutable.List(
            _.map( phase.data.questions.questions, (questionValue, questionId) => {
                let question = new QuestionRecord(questionValue).set('_id', questionId);
                if ( !questionValue.answers ) {
                    // This might not be needed if records respect their default values
                    question = question.set('answers', Immutable.List() );
                } else {
                    question = question.set('answers', Immutable.List(
                        _.map(questionValue.answers, (answerValue, answerKey) => {
                            return new AnswerRecord(answerValue).set('_id', answerKey);
                        } )
                    ).sortBy( answer => answer.order) );
                }
                return question;
            } ) ).sortBy( question => question.order );

        return questions;
    }

    /**
     * Index the questions so that they can be ordered in the interface
     *
     * @param question
     * @param index
     * @returns {*}
     */
    static indexQuestions( question, index ) {
        return question.set('order', index);
    }

    /**
     * When props change, set the questions as a state, this will be our Immutable List that we work with
     *
     * @param nextProps
     */
    componentWillReceiveProps(nextProps) {
        this.setState({
            questions: QuestionsEdit.getQuestionsFromProps(nextProps)
        });
    }

    /**
     * Add a question to the phase
     */
    addQuestion() {
        this.setState({
            questions: this.state.questions.push( new QuestionRecord( {
                __new: true,
                _id: Random.id()
            } ) ).map(QuestionsEdit.indexQuestions)
        });
    }

    /**
     * Move the question in the state
     * This move operation is only for visual preview
     * We keep the movedQuestionIndex in the state as it's the only place where we can retrieve it later when dropping
     * The real (server-side) move operation will occur when dropping the item
     *
     * @param movedQuestion
     * @param atQuestion
     */
    moveQuestion(movedQuestion, atQuestion) {
        const { questions } = this.state;

        const atIndex = questions.indexOf( atQuestion );
        const movedQuestionIndex = questions.findIndex( question => question._id == movedQuestion._id );

        this.setState({
            // We don't reindex here as this is not a definitive move, only when dropping will we reindex
            questions: this.state.questions
                           .delete( movedQuestionIndex )
                           .splice( atIndex, 0, questions.get(movedQuestionIndex) )
        });
    }

    /**
     * Reset the questions to their original order before the drag
     */
    resetQuestionsOrder() {
        this.setState({
            questions: this.state.questions.sortBy( question => question.order )
        });
    }

    /**
     * Confirm question removal
     */
    confirmRemoveQuestion(question) {
        this.setState({
            removeQuestion: question
        });
    }

    /**
     * Remove the question
     * It is already confirmed, removes it then resets confirmation and selection state
     */
    removeQuestion() {
        const { removeQuestion: removedQuestion } = this.state;
        let state = {
            questions: this.state.questions
                           .delete(this.state.questions.indexOf(removedQuestion))
                           .map(QuestionsEdit.indexQuestions),
            removeQuestion: null
        };

        // Only keep removed questions that weren't new
        if ( !removedQuestion.__new ) {
            state.removedQuestions = this.state.removedQuestions.push(removedQuestion)
        }

        // Set the state
        this.setState(state);
    }

    /**
     * Index the answers so that they can be ordered in the interface
     *
     * @param answer
     * @param index
     * @returns {*}
     */
    static indexAnswers( answer, index ) {
        return answer.set('order', index);
    }

    addAnswer(questionId, answer) {
        const newAnswer = new AnswerRecord( {
            __new: true,
            _id: Random.id()
        } );

        this.setState( {
            questions: this.state.questions.updateIn(
                [
                    this.state.questions.findIndex(question => question._id == questionId ),
                    'answers'
                ],
                Immutable.List(),
                answers => answers.push(newAnswer).map( QuestionsEdit.indexAnswers )
            )
        } );
    }
    /*

     moveAnswer(questionId, movedAnswerIndex, atIndex) {
     this.setState( {
     questions: this.state.questions.updateIn(
     [
     this.state.questions.findIndex(question => question._id == questionId ),
     'answers'
     ],
     Immutable.List(),
     // We don't reindex here as this is not a definitive move, only when dropping will we reindex
     answers => answers.delete( movedAnswerIndex ).splice( atIndex, 0, answers.get(movedAnswerIndex) )
     )
     } );
     }
     */

    render() {
        const { connectDropTarget } = this.props;
        const { questions, removedQuestions } = this.state;

        return (
            <div>
                <h4 className="ui dividing header">{__('Questions')}</h4>

                <div className="ui tiny buttons">
                    <div className="ui green right labeled icon button" onClick={this.addQuestion}>
                        <i className="add icon"/>
                        {__('Add Question')}
                    </div>
                </div>

                <div className="ui hidden divider"></div>

                {/* Removed Questions */}
                { removedQuestions.map( question => (
                    <input type="hidden"
                           name="removedQuestions[]"
                           key={question._id}
                           value={question._id}/>
                ) ) }

                {/* Questions */}
                { connectDropTarget(
                    <div>
                        { questions.map( question => (
                            <Question
                                key={question._id}
                                question={question}
                                addAnswer={this.addAnswer}
                                moveItem={this.moveQuestion}
                                restoreItem={this.resetQuestionsOrder}
                                onRemove={() => this.confirmRemoveQuestion(question)}/>
                        ) ) }
                    </div>
                ) }

                {/* Remove Modal */}
                <Modal
                    opened={!!this.state.removeQuestion}
                    onApprove={this.removeQuestion}
                    onHide={()=>this.setState({removeQuestion:null})}>
                    <i className="close icon"/>
                    <div className="header">
                        {__('Remove Question')}
                    </div>
                    <div className="image content">
                        <div className="image">
                            <i className="inbox icon"/>
                        </div>
                        <div className="description">
                            <p>{__('Are you sure you want to remove the question __question__?', { question: this.state.removeQuestion ? this.state.removeQuestion.prompt : null })}</p>
                        </div>
                    </div>
                    <div className="actions">
                        <div className="ui buttons">
                            <div className="ui red deny button">
                                <i className="remove icon"/>
                                {__('No')}
                            </div>
                            <div className="ui green approve button">
                                <i className="checkmark icon"/>
                                {__('Yes')}
                            </div>
                        </div>
                    </div>
                </Modal>
            </div>
        );
    }
}

QuestionsEdit.propTypes = {
    project: PropTypes.object.isRequired,
    phase: PropTypes.object.isRequired,
    connectDropTarget: PropTypes.func.isRequired
};

export default DropTarget('question', {
    /* TARGET */
    drop(props, monitor, component) {
        component.setState({
            // Set the state to the newly indexed questions to preserve the order
            questions: component.state.questions.map(QuestionsEdit.indexQuestions)
        });
    }
}, connect => ({
    connectDropTarget: connect.dropTarget()
}))(QuestionsEdit);

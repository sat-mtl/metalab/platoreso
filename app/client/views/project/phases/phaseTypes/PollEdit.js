"use strict";

import React, { Component, PropTypes } from "react";

export default class PollEdit extends Component {
    render() {
        return (
            <div className="ui message">
                <p>{__('Card creator will be able to create questions for the group members to answer.')}</p>
            </div>
        );
    }
}
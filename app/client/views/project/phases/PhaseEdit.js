"use strict";

import React, {Component, PropTypes} from "react";
import ReactDOM from "react-dom";
import moment from "moment";
import Pikaday from "pikaday";
import {unflatten} from "flat";
import classNames from "classnames";
import formDiff from "/imports/utils/formDiff";
import ProjectMethods from "/imports/projects/ProjectMethods";
import PhaseTypes from "/imports/projects/phases/PhaseTypes";
import Modal from "../../../components/Modal";
import Dialog from "../../../components/Dialog";
import QuestionsEdit from "./phaseTypes/QuestionsEdit";
import PollEdit from "./phaseTypes/PollEdit";
import BasicEdit from "./phaseTypes/BasicEdit";
import Form from "../../../components/forms/Form";
import Field from "../../../components/forms/Field";
import Input from "../../../components/forms/Input";
import TextArea from "../../../components/forms/Textarea";
import Select from "../../../components/forms/Select";

export default class PhaseEdit extends Component {

    constructor( props ) {
        super( props );

        // Bind methods, not automatic in current React version when using classes
        this.setupPickers          = this.setupPickers.bind( this );
        this.submitForm            = this.submitForm.bind( this );
        this.updatePhase           = this.updatePhase.bind( this );
        this.destroyPickers        = this.destroyPickers.bind( this );
        this.phaseTypeChanged      = this.phaseTypeChanged.bind( this );
        this.renderPhaseTypeEditor = this.renderPhaseTypeEditor.bind( this );

        this.pikadayConfig = {
            format:                          'YYYY-MM-DD',
            numberOfMonths:                  2,
            showDaysInNextAndPreviousMonths: true
        };

        $.fn.form.settings.rules.noTimeTravel = ( value ) => {
            return this.startPicker.getDate() <= this.endPicker.getDate();
        };

        // Initial state
        this.state = {
            saving:    false,
            error:     null,
            phaseType: props.phase.type
        };
    }

    componentDidMount() {
        this.setupPickers();
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.phase != this.props.phase ) {
            this.setState( {
                phaseType: nextProps.phase ? nextProps.phase.type : null
            } );
        }
    }

    componentWillUpdate() {
        this.destroyPickers();
    }

    componentDidUpdate( prevProps, prevState ) {
        this.setupPickers();
    }

    componentWillUnmount() {
        this.destroyPickers();
    }

    setupPickers() {
        this.startPicker = new Pikaday( _.defaults( {
            field:    $( '#startDate', ReactDOM.findDOMNode( this.refs.startDate ) )[0],
            onSelect: ( date ) => {
                if ( this.endPicker.getDate() < date ) {
                    this.endPicker.setDate( date );
                }
                this.endPicker.setMinDate( date );
            }
        }, this.pikadayConfig ) );

        this.endPicker = new Pikaday( _.defaults( {
            field:    $( '#endDate', ReactDOM.findDOMNode( this.refs.endDate ) )[0],
            onSelect: ( date ) => {
                //this.startPicker.setMaxDate( date );
            }
        }, this.pikadayConfig ) );

        //this.startPicker.setMaxDate( this.endPicker.getDate() );
        this.endPicker.setMinDate( this.startPicker.getDate() );
    }

    destroyPickers() {
        if ( this.startPicker ) {
            this.startPicker.destroy();
        }
        if ( this.endPicker ) {
            this.endPicker.destroy();
        }
    }

    submitForm() {
        $( ReactDOM.findDOMNode( this.refs.projectPhaseForm ) ).form( 'submit' );
    }

    updatePhase( values ) {
        if ( this.state.saving ) {
            return;
        }
        this.setState( { saving: true } );

        // Forget what semantic says, get the dates from pikaday
        values.startDate = this.startPicker.getDate();
        values.startDate.setHours( 0, 0, 0, 0 );
        values.endDate = this.endPicker.getDate();
        values.endDate.setHours( 23, 59, 59, 999 );

        if ( this.props.isNewPhase ) {
            ProjectMethods.addPhase.call( { projectId: this.props.project._id, phase: unflatten( values ) }, err => {
                this.setState( { saving: false } );
                if ( err ) {
                    this.setState( { error: err } );
                } else if ( this.props.onFinished ) {
                    this.props.onFinished();
                }
            } );

        } else {
            const changes = formDiff( unflatten( values ), this.props.phase, ['_id'] );
            ProjectMethods.updatePhase.call( { projectId: this.props.project._id, phase: changes }, err => {
                this.setState( { saving: false } );
                if ( err ) {
                    this.setState( { error: err } );
                } else if ( this.props.onFinished ) {
                    this.props.onFinished();
                }
            } );
        }
    }

    phaseTypeChanged( phaseType ) {
        this.setState( {
            phaseType: phaseType
        } );
    }

    renderPhaseTypeEditor() {
        switch ( this.state.phaseType ) {
            case PhaseTypes.map.questions:
                return <QuestionsEdit ref="phaseTypeEditor"
                                      key={PhaseTypes.map.questions}
                                      project={this.props.project}
                                      phase={this.props.phase}/>;
                break;
            case PhaseTypes.map.poll:
                return <PollEdit ref="phaseTypeEditor"
                                 key={PhaseTypes.map.poll}
                                 project={this.props.project}
                                 phase={this.props.phase}/>;
                break;
            case PhaseTypes.map.basic:
                return <BasicEdit ref="phaseTypeEditor"
                                  key={PhaseTypes.map.basic}
                                  project={this.props.project}
                                  phase={this.props.phase}/>;
                break;
            default:
                return null;
                break;
        }
    }

    render() {
        const { phase, isNewPhase } = this.props;
        const { error, saving }     = this.state;

        return (
            <div className="editor editPhase">

                <div className="header">
                    { isNewPhase ? __( 'Create Phase' ) : __( 'Edit Phase' ) }
                </div>

                <Form id={"project-phase-form-" + phase._id}
                      ref="projectPhaseForm"
                      key={phase._id}
                      error={error}
                      onSuccess={this.updatePhase}>

                    <input type="hidden" name="_id" value={phase._id}/>

                    <Field label={__( "Name" )} className="required">
                        <Input ref="name" name="name" type="text"
                               defaultValue={phase.name}
                               autofocus={true}
                               rules={[
                                   { type: 'empty', prompt: __( 'Please enter a name' ) }
                               ]}/>
                    </Field>

                    <div className="two fields">

                        <Field label={__( "Start date" )} className="required">
                            <Input ref="startDate" name="startDate" type="text"
                                   defaultValue={moment( phase.startDate ).format( 'YYYY-MM-DD' )}
                                   rules={[
                                       { type: 'empty', prompt: __( 'Please enter a start date' ) },
                                       { type: 'noTimeTravel', prompt: __( 'Phase cannot start after it ends' ) }
                                   ]}/>
                        </Field>

                        <Field label={__( "End date" )} className="required">
                            <Input ref="endDate" name="endDate" type="text"
                                   defaultValue={moment( phase.endDate ).format( 'YYYY-MM-DD' )}
                                   rules={[
                                       { type: 'empty', prompt: __( 'Please enter an end date' ) },
                                       { type: 'noTimeTravel', prompt: __( 'Phase cannot end before it starts' ) }
                                   ]}/>
                        </Field>

                    </div>

                    <Field label={__( "Description" )}>
                            <TextArea ref="description" name="description"
                                      rows={3}
                                      autosize={true}
                                      defaultValue={phase.description}/>
                    </Field>

                    <Field label={__( "Phase Type" )}>
                        <Select ref="type" name="type" defaultValue={phase.type} onSelectChange={this.phaseTypeChanged}>
                            { PhaseTypes.list.map( type =>
                                <option key={type} value={type}>{__( 'project.phase.type.' + type )}</option> )}
                        </Select>
                    </Field>

                    {this.renderPhaseTypeEditor()}

                </Form>

                <div className="actions">
                    <div className="ui buttons">
                        <div className="ui cancel button" onClick={this.props.onFinished}>
                            <i className="remove icon"/>
                            {__( 'Cancel' )}
                        </div>
                        <div className={classNames( "ui green button", {
                            loading:  saving,
                            disabled: saving
                        } )} onClick={this.submitForm}>
                            <i className="checkmark icon"/>
                            {__( 'Save' )}
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

PhaseEdit.propTypes = {
    project:    PropTypes.object.isRequired,
    phase:      PropTypes.object.isRequired,
    isNewPhase: PropTypes.bool,
    onFinished: PropTypes.func
};

PhaseEdit.defaultProps = {
    isNewPhase: false
};

export class PhaseEditModal extends Component {
    render() {
        const { phase, onRequestClose } = this.props;
        return (
            <Modal opened={ phase != null } onRequestClose={onRequestClose}>
                <Dialog onRequestClose={onRequestClose}>
                    { phase && <PhaseEdit key={phase._id || 'newPhase'} {...this.props} onFinished={onRequestClose}/> }
                </Dialog>
            </Modal>
        );
    }
}

PhaseEditModal.propTypes = {
    onRequestClose: PropTypes.func.isRequired
};
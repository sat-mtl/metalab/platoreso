"use strict";

import React, { Component, PropTypes } from "react";

import {DragLayer} from 'react-dnd';
import CardDragPreview from './CardDragPreview';

class CardsDragLayer extends Component {
    constructor( props ) {
        super( props );
    }

    shouldComponentUpdate( nextProps, nextState ) {
        return (
            nextProps.itemType != this.props.itemType
            || nextProps.item != this.props.item
            || nextProps.currentOffset != this.props.currentOffset
            || nextProps.currentOffset.x != this.props.currentOffset.x
            || nextProps.currentOffset.y != this.props.currentOffset.y
        );
    }

    renderItem( type, item ) {
        switch ( type ) {
            case 'card':
                return (
                    <CardDragPreview card={item}/>
                );
        }
    }

    render() {
        const { item, itemType, isDragging } = this.props;
        
        if ( !isDragging ) {
            return null;
        }
        return (
            <div id="cardsDragLayer">
                <div id="draggedItem" style={getItemStyles(this.props)}>
                    {this.renderItem( itemType, item )}
                </div>
            </div>
        );
    }
}

CardsDragLayer.propTypes = {
    item:          PropTypes.object,
    itemType:      PropTypes.string,
    currentOffset: PropTypes.shape( {
        x: PropTypes.number.isRequired,
        y: PropTypes.number.isRequired
    } ),
    isDragging:    PropTypes.bool.isRequired
};

function getItemStyles( props ) {
    const { currentOffset } = props;
    if ( !currentOffset ) {
        return {
            display: 'none'
        };
    }

    const { x, y } = currentOffset;

    const transform = `translate3d(${x}px, ${y}px, 0)`;
    return {
        transform:       transform,
        WebkitTransform: transform
    };
}

export default DragLayer( monitor => {
    return {
        item:          monitor.getItem(),
        itemType:      monitor.getItemType(),
        currentOffset: monitor.getClientOffset(),
        isDragging:    monitor.isDragging()
    };
} )( CardsDragLayer );
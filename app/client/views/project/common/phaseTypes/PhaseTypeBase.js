"use strict";

import React, { Component, PropTypes } from "react";

export default class PhaseTypeBase extends Component {
    componentWillReceiveProps(nextProps) {
    
    }
}

PhaseTypeBase.propTypes = {
    phase: PropTypes.object.isRequired,
    card: PropTypes.object.isRequired
};
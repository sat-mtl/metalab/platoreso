"use strict";

import React, {Component, PropTypes} from "react";
import PhaseTypeBase from './PhaseTypeBase';
import {getQuestionList} from './questions/QuestionRecord';
import Question from './questions/Question';

export default class Questions extends PhaseTypeBase {

    constructor( props ) {
        super( props );

        this.state = {
            questions: getQuestionList( props.phase )
        };
    }

    /**
     * When props change, set the questions as a state, this will be our Immutable List that we work with
     *
     * @param nextProps
     */
    componentWillReceiveProps( nextProps ) {
        super.componentWillReceiveProps( nextProps );

        this.setState( {
            questions: getQuestionList( nextProps.phase )
        } );
    }

    render() {
        const { phase, card } = this.props;
        const { questions } = this.state;

        const data = card.getPhaseData( phase._id );

        // Don't bother if we don't have answers
        if ( !data || !data.questions || !data.questions.answers ) {
            return null;
        }

        return (
            <div className="questions">
                { questions.map( question => <Question key={question._id}
                                                       question={question}
                                                       data={data}/> ) }
            </div>
        );
    }
}
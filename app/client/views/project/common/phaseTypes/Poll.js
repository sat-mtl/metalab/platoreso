"use strict";

import React, {Component, PropTypes} from "react";
import PhaseTypeBase from './PhaseTypeBase';
import PollRecords from './poll/PollRecords';
import Question from './poll/Question';
import Form from '../../../../components/forms/Form';

export default class Poll extends PhaseTypeBase {

    constructor( props ) {
        super( props );

        this.state = {
            questions: PollRecords.getQuestionList( props.card, props.phase )
        };
    }

    /**
     * When props change, set the questions as a state, this will be our Immutable List that we work with
     *
     * @param nextProps
     */
    componentWillReceiveProps( nextProps ) {
        super.componentWillReceiveProps( nextProps );

        this.setState( {
            questions: PollRecords.getQuestionList( nextProps.card, nextProps.phase )
        } );
    }

    answerPoll(values) {
        console.log(values);
    }

    render() {
        const { phase, card } = this.props;
        const { questions } = this.state;

        return (
            <Form id={"card-poll-form-"+card.id}
                  ref="pollForm"
                  className="poll"
                  key={card.id}
                  error={this.state.error}
                  onSuccess={this.answerPoll}>
                <input type="hidden" name="_id" value={card.id}/>

                { questions.map( question =>
                    <Question key={question._id} question={question} phase={phase} card={card}/> ) }

                <div className="ui green labeled icon submit button">
                    <i className="checkmark icon"/>
                    {__( 'Answer' )}
                </div>

            </Form>
        );
    }
}
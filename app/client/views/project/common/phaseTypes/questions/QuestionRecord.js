"use strict";

import Immutable from 'immutable';

export const QuestionRecord = Immutable.Record({
    _id:null,
    order:-1,
    prompt: '',
    required: false,
    answers: null
});

export const AnswerRecord = Immutable.Record({
    _id:null,
    order:-1,
    prompt: ''
});

export default QuestionRecord;

export const getQuestionList = function getQuestionList(phase) {
    if ( !phase.data || !phase.data.questions || !phase.data.questions.questions ) {
        return Immutable.List();
    }

    return Immutable.List(
        _.map( phase.data.questions.questions, (questionValue, questionId) => {
            let question = new QuestionRecord(questionValue).set('_id', questionId);
            if ( question.answers ) {
                question = question.set('answers', Immutable.List(
                    _.map(question.answers, (answerValue, answerKey) => {
                        return new AnswerRecord(answerValue).set('_id', answerKey);
                    } )
                ).sortBy( answer => answer.order) );
            }
            return question;
        } ) ).sortBy( question => question.order );
};

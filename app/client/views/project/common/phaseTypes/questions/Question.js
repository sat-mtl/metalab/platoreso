"use strict";

import React, {Component, PropTypes} from "react";
import md from "/imports/utils/markdown";

export default class Question extends Component {

    render() {
        const { data, question } = this.props;
        if ( !data || !question ) {
            return null;
        }

        // Don't bother if we don't have answers
        if ( !data.questions || !data.questions.answers ) {
            return null;
        }

        // Get the answer
        let answerValue = data.questions.answers[question._id];
        if ( !answerValue ) {
            return null;
        }
        // If it's not an open question, get the answer text from the question
        if ( question.answers && question.answers.size > 0 ) {
            const answer = question.answers.find( answer => answer._id == answerValue );
            if ( !answer ) {
                return null;
            }
            answerValue = answer.prompt;
        }

        answerValue = md( answerValue || '' );

        return (
            <div className="question">
                <div className="prompt">{question.prompt}</div>
                <div className="answer" dangerouslySetInnerHTML={{__html: answerValue }}></div>
            </div>
        );
    }
}

Question.propTypes = {
    data:     PropTypes.object.isRequired,
    question: PropTypes.object.isRequired
};
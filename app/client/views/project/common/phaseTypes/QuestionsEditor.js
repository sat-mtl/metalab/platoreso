"use strict";

import React, {Component, PropTypes} from "react";
import PhaseTypeEditorBase from './PhaseTypeEditorBase';
import {getQuestionList} from './questions/QuestionRecord';
import QuestionEditor from './questions/QuestionEditor';

export default class QuestionsEditor extends PhaseTypeEditorBase {

    constructor( props ) {
        super( props );

        this.state = {
            questions: getQuestionList( props.phase )
        };
    }

    /**
     * When props change, set the questions as a state, this will be our Immutable List that we work with
     *
     * @param nextProps
     */
    componentWillReceiveProps( nextProps ) {
        super.componentWillReceiveProps( nextProps );

        this.setState( {
            questions: getQuestionList( nextProps.phase )
        } );
    }

    render() {
        const { phase, card } = this.props;
        const { questions } = this.state;

        const data = card ? card.getPhaseData( phase._id ) : null;

        return (
            <div className="questions">
                { questions.map( question => <QuestionEditor key={question._id}
                                                             question={question}
                                                             phase={phase}
                                                             data={data}/> ) }
            </div>
        );
    }
}
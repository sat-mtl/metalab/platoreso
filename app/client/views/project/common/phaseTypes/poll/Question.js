"use strict";

import React, { Component, PropTypes } from "react";
import Field from '../../../../../components/forms/Field';
import Input from '../../../../../components/forms/Input';

export default class Question extends Component {
    constructor( props ) {
        super( props );

        this.renderQuestion = this.renderQuestion.bind( this );

        this.state = {};
    }

    renderQuestion() {
        const { data, question, phase } = this.props;
        if ( !question ) {
            return null;
        }

        // Answers are buried deep, check if we have one
        let defaultValue = null;
        if ( data && data.questions && data.questions.answers ) {
            defaultValue = data.questions.answers[question._id];
        }

        if ( question.answers && question.answers.size > 0 ) {
            return (
                <div className="grouped fields">
                    { question.answers.map( answer => (
                        <div key={answer._id} className="field">
                            <div className="ui radio checkbox">
                                <input type="radio" name={"phaseData."+phase._id+".questions.answers."+question._id} value={answer._id} defaultChecked={answer._id == defaultValue}/>
                                <label>{answer.prompt}</label>
                            </div>
                        </div>
                    ) ) }
                </div>
            );
        } else {
            return (
                <Input ref={"phaseData."+phase._id+".questions.answers."+question._id}
                       name={"phaseData."+phase._id+".questions.answers."+question._id}
                       type="text"
                       defaultValue={defaultValue}
                       rules={[
                           { type: 'empty', prompt: __('Please provide an answer') }
                       ]}/>
            );
        }
    }

    render() {
        const { question } = this.props;
        if ( !question ) {
            return null;
        }

        return (
            <Field className="question" label={question.prompt}>
                { this.renderQuestion() }
            </Field>
        );
    }
}

Question.propTypes = {
    data:     PropTypes.object,//.isRequired,
    phase:    PropTypes.object.isRequired,
    question: PropTypes.object.isRequired
};
"use strict";

import Immutable from 'immutable';

export const QuestionRecord = Immutable.Record( {
    __new:   false,
    _id:     null,
    order:   -1,
    prompt:  '',
    answers: Immutable.List()
} );

export const AnswerRecord = Immutable.Record( {
    __new:  false,
    _id:    null,
    order:  -1,
    prompt: ''
} );

export default class PollRecords {

    /**
     * Get the questions as an indexed Immutable List
     *
     * @param card
     * @param phase
     * @returns {*}
     */
    static getQuestionList( card, phase ) {
        const data = card ? card.getPhaseData( phase._id ) : null;

        if ( !data || !data.poll || !data.poll.questions ) {
            return Immutable.List();
        }

        return Immutable.List(
            _.map( data.poll.questions, ( questionValue, questionId ) => {
                const question = _.clone( questionValue );
                if ( question.answers ) {
                    question.answers = Immutable.List(
                        _.map( question.answers, ( answerValue, answerKey ) => {
                            return new AnswerRecord( answerValue ).set( '_id', answerKey );
                        } )
                    ).sortBy( answer => answer.order );
                }
                return new QuestionRecord( question ).set( '_id', questionId );
            } ) ).sortBy( question => question.order );
    }

    static addQuestion( questions ) {
        return questions.push( new QuestionRecord( {
            __new: true,
            _id:   Random.id()
        } ) ).map( PollRecords.indexQuestions );
    };

    static moveQuestion( questions, from, to ) {
        return questions
            .delete( from )
            .splice( to, 0, questions.get( from ) );
    }

    static removeQuestion( questions, question ) {
        return questions
            .delete( questions.indexOf( question ) )
            .map( PollRecords.indexQuestions )
    }

    static addAnswer( questions, questionId ) {
        const newAnswer = new AnswerRecord( {
            __new: true,
            _id:   Random.id()
        } );

        return questions.updateIn(
            [
                questions.findIndex( question => question._id == questionId ),
                'answers'
            ],
            Immutable.List(),
            answers => answers
                .push( newAnswer )
                .map( PollRecords.indexAnswers )
        )
    }

    static moveAnswer( answers, from, to ) {
        return answers
            .delete( from )
            .splice( to, 0, answers.get( from ) );
    }

    static removeAnswer( answers, answer ) {
        return answers
            .delete(answers.indexOf(answer))
            .map(PollRecords.indexAnswers);
    }

    /**
     * Index the questions so that they can be ordered in the interface
     *
     * @param question
     * @param index
     * @returns {*}
     */
    static indexQuestions( question, index ) {
        return question.set( 'order', index );
    }

    /**
     * Index the answers so that they can be ordered in the interface
     *
     * @param answer
     * @param index
     * @returns {*}
     */
    static indexAnswers( answer, index ) {
        return answer.set( 'order', index );
    }


}
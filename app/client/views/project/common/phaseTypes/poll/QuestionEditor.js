"use strict";

import React, { Component, PropTypes } from "react";
import { DragSource, DropTarget } from 'react-dnd';
import Immutable from 'immutable';

import PollRecords from './PollRecords';

import Modal from '../../../../../components/semanticui/modules/Modal';
import Field from '../../../../../components/forms/Field';
import TextArea from '../../../../../components/forms/Textarea';

import AnswerEditor from './AnswerEditor';

export class QuestionEditor extends Component {

    constructor( props ) {
        super( props );

        this.addAnswer           = this.addAnswer.bind( this );
        this.moveAnswer          = this.moveAnswer.bind( this );
        this.resetAnswersOrder   = this.resetAnswersOrder.bind( this );
        this.confirmRemoveAnswer = this.confirmRemoveAnswer.bind( this );
        this.removeAnswer        = this.removeAnswer.bind( this );

        this.state = {
            answers:          props.question.answers,
            originalAnswers:  props.question.answers,
            removedAnswers:   Immutable.List(),
            removeAnswer:     null,
            movedAnswerIndex: -1
        }
    }

    /**
     * When props change, set the answers as a state, this will be our Immutable List that we work with
     * TODO: This resets the answers
     *
     * @param nextProps
     */
    componentWillReceiveProps( nextProps ) {
        if ( nextProps.question.answers != this.state.originalAnswers ) {
            this.setState( {
                answers:         nextProps.question.answers,
                originalAnswers: nextProps.question.answers
            } );
        }
    }

    /**
     * Add a answer to the phase
     */
    addAnswer() {
        this.props.addAnswer( this.props.question._id );
    }

    /**
     * Move the answer in the state
     * This move operation is only for visual preview
     * We keep the movedAnswerIndex in the state as it's the only place where we can retrieve it later when dropping
     * The real (server-side) move operation will occur when dropping the item
     *
     * @param movedAnswer
     * @param atAnswer
     */
    moveAnswer( movedAnswer, atAnswer ) {
        const { answers } = this.state;

        // We don't reindex here as this is not a definitive move, only when dropping will we reindex
        this.setState( {
            answers: PollRecords.moveAnswer(
                answers,
                answers.findIndex( answer => answer._id == movedAnswer._id ),
                answers.indexOf( atAnswer )
            )
        } );
    }

    /**
     * Reset the answers to their original order before the drag
     */
    resetAnswersOrder() {
        this.setState( {
            answers: this.state.answers.sortBy( answer => answer.order )
        } );
    }

    /**
     * Confirm answer removal
     */
    confirmRemoveAnswer( answer ) {
        this.setState( {
            removeAnswer: answer
        } );
    }

    /**
     * Remove the answer
     * It is already confirmed, removes it then resets confirmation and selection state
     */
    removeAnswer() {
        const { answers, removeAnswer: removedAnswer } = this.state;
        let state = {
            answers:      PollRecords.removeAnswer( answers, removedAnswer ),
            removeAnswer: null
        };

        // Only keep removed answers that weren't new, the includes() check is just a precaution against duplicates
        if ( !removedAnswer.__new && !this.state.removedAnswers.includes( removedAnswer._id ) ) {
            state.removedAnswers = this.state.removedAnswers.push( removedAnswer._id )
        }

        // Set the state
        this.setState( state );
    }

    render() {
        const { phase, question } = this.props;
        const {
                  connectQuestionDragSource,
                  connectQuestionDragPreview,
                  connectQuestionDropTarget,
                  isDraggingQuestion,
                  connectAnswerDropTarget
                  } = this.props;
        const { answers, removedAnswers } = this.state;

        return connectQuestionDropTarget( connectQuestionDragPreview(
            <div className="question">
                <input type="hidden" name={"phaseData."+phase._id+".poll.questions."+question._id+".order"} value={question.order}/>

                <div className="ui top attached menu">
                    <div className="borderless header item">
                        {__( 'Question' )}
                    </div>
                    <div className="right menu">
                        <div className="icon item">
                            { connectQuestionDragSource( <i className="move handle sidebar icon"/> ) }
                        </div>
                        <div className="icon item" onClick={this.props.onRemove}>
                            {/*<div className="ui red right icon button" onClick={this.props.onRemove}>*/}
                            <i className="red remove icon"/>
                            {/*</div>*/}
                        </div>
                    </div>
                </div>

                <div className="ui attached segment">
                    <Field className="required">
                        <TextArea ref={"phaseData."+phase._id+".poll.questions."+question._id+".prompt"}
                                  name={"phaseData."+phase._id+".poll.questions."+question._id+".prompt"}
                                  type="text"
                                  className="fluid left icon action prompt"
                                  defaultValue={question.prompt}
                                  rows={3}
                                  rules={[
                                   { type: 'empty', prompt: __('Please enter a question') }
                               ]}/>
                    </Field>
                </div>

                <div className="ui attached menu">
                    <div className="borderless header item">
                        {__( 'Answer choices' )}
                    </div>
                    <div className="right menu">
                        <div className="item" onClick={this.addAnswer}>
                            {/*div className="ui tiny green right labeled icon button" onClick={this.addAnswer}>*/}
                            <i className="add icon"/>
                            {__( 'Add Answer' )}
                            {/*</div>*/}
                        </div>
                    </div>
                </div>

                <div className="ui attached segment">

                    {/* Removed Questions */}
                    { removedAnswers.map( answerId => (
                        <input type="hidden"
                               name={"removedAnswers." + question._id + "[]"}
                               key={answerId}
                               value={answerId}/>
                    ) ) }

                    {/* Answers */}
                    { connectAnswerDropTarget(
                        <div className="answers">
                            { answers
                                .filter( answer => !removedAnswers.includes( answer._id ) )
                                .map( answer => (
                                <AnswerEditor
                                    key={answer._id}
                                    phase={phase}
                                    question={question}
                                    answer={answer}
                                    moveItem={this.moveAnswer}
                                    restoreItem={this.resetAnswersOrder}
                                    onRemove={() => this.confirmRemoveAnswer(answer)}/>
                            ) ) }
                        </div>
                    ) }

                    {/* Remove Modal */}
                    <Modal
                        opened={!!this.state.removeAnswer}
                        onApprove={this.removeAnswer}
                        onHide={()=>this.setState({removeAnswer:null})}>
                        <i className="close icon"/>
                        <div className="header">
                            {__( 'Remove Answer' )}
                        </div>
                        <div className="image content">
                            <div className="image">
                                <i className="inbox icon"/>
                            </div>
                            <div className="description">
                                <p>{__( 'Are you sure you want to remove the answer __answer__?', {answer: this.state.removeAnswer ? this.state.removeAnswer.prompt : null} )}</p>
                            </div>
                        </div>
                        <div className="actions">
                            <div className="ui buttons">
                                <div className="ui red deny button">
                                    <i className="remove icon"/>
                                    {__( 'No' )}
                                </div>
                                <div className="ui green approve button">
                                    <i className="checkmark icon"/>
                                    {__( 'Yes' )}
                                </div>
                            </div>
                        </div>
                    </Modal>
                </div>
            </div>
        ) );
    }
}

QuestionEditor.propTypes = {
    phase:                      PropTypes.object.isRequired,
    // Question
    question:                   PropTypes.object.isRequired,
    // Answers
    addAnswer:                  PropTypes.func.isRequired,
    //moveAnswer: PropTypes.func.isRequired,
    // List
    onRemove:                   PropTypes.func.isRequired,
    // Drag & Drop
    connectQuestionDragSource:  PropTypes.func.isRequired,
    connectQuestionDragPreview: PropTypes.func.isRequired,
    connectQuestionDropTarget:  PropTypes.func.isRequired,
    isDraggingQuestion:         PropTypes.bool.isRequired,
    connectAnswerDropTarget:    PropTypes.func.isRequired,
    // Drag & Drop Support
    moveItem:                   PropTypes.func.isRequired,
    restoreItem:                PropTypes.func.isRequired
};

export default _.compose(
    DragSource( 'question', {
        /* SOURCE */

        beginDrag( props ) {
            return {_id: props.question._id};
        },

        endDrag( props, monitor, component ) {
            if ( !monitor.didDrop() ) {
                props.restoreItem();
            }
        }
    }, ( connect, monitor ) => ({
        connectQuestionDragSource:  connect.dragSource(),
        connectQuestionDragPreview: connect.dragPreview(),
        isDraggingQuestion:         monitor.isDragging()
    }) ),

    DropTarget( 'question', {
        /* TARGET */

        canDrop() {
            // We can't drop on the dummy target
            return false;
        },

        hover( props, monitor, component ) {
            // Simulate the reordering on hover
            const item = monitor.getItem();
            if ( item._id === props.question._id ) {
                return;
            }
            props.moveItem( item, props.question );
        }
    }, connect => ({
        connectQuestionDropTarget: connect.dropTarget()
    }) ),

    DropTarget( 'answer', {
        /* TARGET */
        drop( props, monitor, component ) {
            const item = monitor.getItem();
            if ( item.question != props.question._id ) {
                return false;
            }
            component.setState( {
                // Set the state to the newly indexed questions to preserve the order
                answers: component.state.answers.map( PollRecords.indexAnswers )
            } );
        }
    }, connect => ({
        connectAnswerDropTarget: connect.dropTarget()
    }) )
)( QuestionEditor );
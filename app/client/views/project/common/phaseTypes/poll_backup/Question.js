"use strict";

import React, { Component, PropTypes } from "react";

export default class Question extends Component {

    render() {
        const { card, question } = this.props;
        if ( !card || !question ) {
            return null;
        }

        if ( !card.data || !card.data.poll || !card.data.poll.answers ) {
            return null;
        }

        const answerId = card.data.poll.answers[question._id];
        if ( !answerId ) {
            return null;
        }

        const answer = question.answers.find(answer => answer._id == answerId);
        if ( !answer ) {
            return null;
        }

        return (
            <div className="question">
                <div className="content">
                    <div className="prompt">{question.prompt}</div>
                    <div className="answer">{answer.prompt}</div>
                </div>
            </div>
        );
    }
}

Question.propTypes = {
    card: PropTypes.object.isRequired,
    question: PropTypes.object.isRequired
};
"use strict";

import React, { Component, PropTypes } from "react";

export default class QuestionEditor extends Component {

    render() {
        const { card, question } = this.props;
        if ( !question ) {
            return null;
        }

        let answerId = null;
        if ( card && card.data && card.data.poll && card.data.poll.answers ) {
            answerId = card.data.poll.answers[question._id];
        }

        return (
            <div className="question">
                <div className="grouped fields">
                    <label>{question.prompt}</label>
                    { question.answers.map( answer => (
                        <div key={answer._id} className="field">
                            <div className="ui radio checkbox">
                                <input type="radio" name={"data.poll.answers."+question._id} value={answer._id} defaultChecked={answer._id == answerId}/>
                                <label>{answer.prompt}</label>
                            </div>
                        </div>
                    )) }
                </div>
            </div>
        );
    }
}

QuestionEditor.propTypes = {
    card: PropTypes.object, //.isRequired,
    question: PropTypes.object.isRequired
};
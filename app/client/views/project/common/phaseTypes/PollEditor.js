"use strict";

import React, { Component, PropTypes } from "react";
import { DragSource, DropTarget } from 'react-dnd';
import Immutable from 'immutable';

import PollRecords from './poll/PollRecords';

import PhaseTypeEditorBase from './PhaseTypeEditorBase';
import Modal from '../../../../components/semanticui/modules/Modal';
import Form from '../../../../components/forms/Form';
import Field from '../../../../components/forms/Field';
import Input from '../../../../components/forms/Input';

import QuestionEditor from './poll/QuestionEditor';

class PollEditor extends PhaseTypeEditorBase {

    constructor( props ) {
        super( props );

        this.addQuestion           = this.addQuestion.bind( this );
        this.moveQuestion          = this.moveQuestion.bind( this );
        this.resetQuestionsOrder   = this.resetQuestionsOrder.bind( this );
        this.confirmRemoveQuestion = this.confirmRemoveQuestion.bind( this );
        this.removeQuestion        = this.removeQuestion.bind( this );

        this.addAnswer = this.addAnswer.bind( this );

        this.state = {
            questions:          PollRecords.getQuestionList( props.card, props.phase ),
            removedQuestions:   Immutable.List(),
            removeQuestion:     null,
            movedQuestionIndex: -1
        }
    }

    /**
     * When props change, set the questions as a state, this will be our Immutable List that we work with
     *
     * @param nextProps
     */
    componentWillReceiveProps( nextProps ) {
        if ( this.props.card != nextProps.card || this.props.phase != nextProps.phase ) {
            this.setState( {
                questions: PollRecords.getQuestionList( nextProps.card, nextProps.phase )
            } );
        }
    }

    /**
     * Add a question to the phase
     */
    addQuestion() {
        this.setState( {
            questions: PollRecords.addQuestion( this.state.questions )
        } );
    }

    /**
     * Move the question in the state
     * This move operation is only for visual preview
     * We keep the movedQuestionIndex in the state as it's the only place where we can retrieve it later when dropping
     * The real (server-side) move operation will occur when dropping the item
     *
     * @param movedQuestion
     * @param atQuestion
     */
    moveQuestion( movedQuestion, atQuestion ) {
        const { questions } = this.state;

        this.setState( {
            // We don't reindex here as this is not a definitive move, only when dropping will we reindex
            questions: PollRecords.moveQuestion(
                questions,
                questions.findIndex( question => question._id == movedQuestion._id ),
                questions.indexOf( atQuestion )
            )
        } );
    }

    /**
     * Reset the questions to their original order before the drag
     */
    resetQuestionsOrder() {
        this.setState( {
            questions: this.state.questions.sortBy( question => question.order )
        } );
    }

    /**
     * Confirm question removal
     */
    confirmRemoveQuestion( question ) {
        this.setState( {
            removeQuestion: question
        } );
    }

    /**
     * Remove the question
     * It is already confirmed, removes it then resets confirmation and selection state
     */
    removeQuestion() {
        const { removeQuestion: removedQuestion } = this.state;
        let state = {
            questions:      PollRecords.removeQuestion( this.state.questions, removedQuestion ),
            removeQuestion: null
        };

        // Only keep removed questions that weren't new, the includes() check is just a precaution against duplicates
        if ( !removedQuestion.__new && !this.state.removedQuestions.includes( removedQuestion._id ) ) {
            state.removedQuestions = this.state.removedQuestions.push( removedQuestion._id )
        }

        // Set the state
        this.setState( state );
    }

    addAnswer( questionId ) {
        this.setState( {
            questions: PollRecords.addAnswer( this.state.questions, questionId )
        } );
    }

    render() {
        const { connectDropTarget, phase } = this.props;
        const { questions, removedQuestions } = this.state;

        return (
            <div>
                <h4 className="ui dividing header">{__( 'Questions' )}</h4>

                <div className="ui tiny buttons">
                    <div className="ui green right labeled icon button" onClick={this.addQuestion}>
                        <i className="add icon"/>
                        {__( 'Add Question' )}
                    </div>
                </div>

                <div className="ui hidden divider"></div>

                {/* Removed Questions */}
                { removedQuestions.map( questionId => (
                    <input type="hidden"
                           name={"removedQuestions[]"}
                           key={questionId}
                           value={questionId}/>
                ) ) }

                {/* Questions */}
                { connectDropTarget(
                    <div>
                        { questions.map( question => (
                            <QuestionEditor
                                key={question._id}
                                question={question}
                                phase={phase}
                                addAnswer={this.addAnswer}
                                moveItem={this.moveQuestion}
                                restoreItem={this.resetQuestionsOrder}
                                onRemove={() => this.confirmRemoveQuestion(question)}/>
                        ) ) }
                    </div>
                ) }

                {/* Remove Modal */}
                <Modal
                    opened={!!this.state.removeQuestion}
                    onApprove={this.removeQuestion}
                    onHide={()=>this.setState({removeQuestion:null})}>
                    <i className="close icon"/>
                    <div className="header">
                        {__( 'Remove Question' )}
                    </div>
                    <div className="image content">
                        <div className="image">
                            <i className="inbox icon"/>
                        </div>
                        <div className="description">
                            <p>{__( 'Are you sure you want to remove the question __question__?', {question: this.state.removeQuestion ? this.state.removeQuestion.prompt : null} )}</p>
                        </div>
                    </div>
                    <div className="actions">
                        <div className="ui buttons">
                            <div className="ui red deny button">
                                <i className="remove icon"/>
                                {__( 'No' )}
                            </div>
                            <div className="ui green approve button">
                                <i className="checkmark icon"/>
                                {__( 'Yes' )}
                            </div>
                        </div>
                    </div>
                </Modal>
            </div>
        );
    }
}

PollEditor.propTypes = {
    project:           PropTypes.object.isRequired,
    phase:             PropTypes.object.isRequired,
    card:              PropTypes.object,
    connectDropTarget: PropTypes.func.isRequired
};

export default DropTarget( 'question', {
    /* TARGET */
    drop( props, monitor, component ) {
        component.setState( {
            // Set the state to the newly indexed questions to preserve the order
            questions: component.state.questions.map( PollRecords.indexQuestions )
        } );
    }
}, connect => ({
    connectDropTarget: connect.dropTarget()
}) )( PollEditor );

"use strict";

import React, { Component, PropTypes } from "react";
import ProjectCollection from "/imports/projects/ProjectCollection";
import { ConnectMeteorData } from "../../../lib/ReactMeteorData";
import Spinner from "../../widgets/Spinner";
import ProjectListEmpty from "./projectList/ProjectListEmpty";
import ProjectListItem from "./projectList/ProjectListItem";

export class ProjectList extends Component {

    constructor( props ) {
        super( props );
        this.renderProjects = this.renderProjects.bind( this );
    }

    renderProjects() {
        const { group, projectsLoading, projects } = this.props;

        if ( projectsLoading ) {
            return <Spinner/>;
        } else if ( !projects || projects.length == 0 ) {
            return <ProjectListEmpty group={group}/>;
        } else {
            return projects.map( project => <ProjectListItem key={project._id} project={project}/> );
        }
    }

    render() {
        return (
            <div className="sixteen wide column projects tiles">
                <div className="ui three column equal width doubling grid">
                    { this.renderProjects() }
                </div>
            </div>
        );
    }
}

ProjectList.propTypes = {
    group:           PropTypes.object,
    projectsLoading: PropTypes.bool.isRequired,
    projects:        PropTypes.arrayOf( PropTypes.object )
};

export default ConnectMeteorData( props => {
    const { group } = props;
    return {
        projectsLoading: !Meteor.subscribe( 'project/list', group ? group._id : null ).ready(),
        projects:        ProjectCollection.find( group ? { groups: group._id } : {}, { sort: { updatedAt: -1 } } ).fetch()
    };
} )( ProjectList );

export const RecentProjects = ConnectMeteorData( props => {
    const { group, limit } = props;
    const options          = {
        sort: { createdAt: -1 },
    };
    if ( limit ) {
        options.limit = limit;
    }
    return {
        projectsLoading: !Meteor.subscribe( 'project/list', group ? group._id : null ).ready(),
        projects:        ProjectCollection.find(
            group ? { groups: group._id } : {},
            options
        ).fetch()
    };
} )( ProjectList );

export const TrendingProjects = ConnectMeteorData( props => {
    const { group, limit } = props;
    let projects           = ProjectCollection.find( group ? { groups: group._id } : {} )
                                              .fetch()
                                              .sort( ( a, b ) => b.temperature - a.temperature );
    if ( limit ) {
        projects = projects.slice( 0, limit );
    }
    return {
        projectsLoading: !Meteor.subscribe( 'project/list', group ? group._id : null ).ready(),
        projects:        projects
    };
} )( ProjectList );

export const TopProjects = ConnectMeteorData( props => {
    const { group, limit } = props;
    const options          = {
        sort: { totalTemperature: -1 }
    };
    if ( limit ) {
        options.limit = limit;
    }
    return {
        projectsLoading: !Meteor.subscribe( 'project/list', group ? group._id : null ).ready(),
        projects:        ProjectCollection.find(
            group ? { groups: group._id } : {},
            options
        ).fetch()
    };
} )( ProjectList );
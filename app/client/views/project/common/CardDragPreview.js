"use strict";

import React, { Component, PropTypes } from "react";

export default class CardDragPreview extends Component {

    shouldComponentUpdate( nextProps, nextState ) {
        return nextProps.card.name != this.props.card.name
    }

    render() {
        const { card } = this.props;

        return (
            <div className="card dragPreview">
                <div className="content">
                    <div className="name">
                        { card.name }
                    </div>
                </div>
            </div>
        );
    }
}

CardDragPreview.propTypes = {
    card: PropTypes.object.isRequired
};
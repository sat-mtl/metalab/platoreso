"use strict";

import Questions from './phaseTypes/Questions';
import Poll from './phaseTypes/Poll';
import Basic from './phaseTypes/Basic';

import React, { Component, PropTypes } from "react";

export default class PhaseType extends Component {
    render() {
        const { phase, card } = this.props;

        switch ( phase.type ) {

            case 'questions':
                return <Questions ref="questions"
                                  key={phase.type}
                                  phase={phase}
                                  card={card}/>;
                break;

            case 'poll':
                return <Poll ref="poll"
                             key={phase.type}
                             phase={phase}
                             card={card}/>;
                break;

            case 'basic':
                return <Basic ref="basic"
                              key={phase.type}
                              phase={phase}
                              card={card}/>;
                break;

            default:
                return null;
                break;
        }
    }
}

PhaseType.propTypes = {
    phase:   PropTypes.object.isRequired,
    card:    PropTypes.object.isRequired
};
"use strict";

import PhaseTypes from "/imports/projects/phases/PhaseTypes";

import QuestionsEditor from './phaseTypes/QuestionsEditor';
import PollEditor from './phaseTypes/PollEditor';
import BasicEditor from './phaseTypes/BasicEditor';

import React, { Component, PropTypes } from "react";

export default class PhaseTypeEditor extends Component {
    render() {
        const { project, phase, card } = this.props;

        switch ( phase.type ) {
            case 'questions':
                return <QuestionsEditor ref="phaseTypeForm"
                                        key={phase.type}
                                        project={project}
                                        phase={phase}
                                        card={card}/>;
                break;

            case'poll':
                return <PollEditor ref="phaseTypeForm"
                                   key={phase.type}
                                   project={project}
                                   phase={phase}
                                   card={card}/>;
                break;

            case 'basic':
                return <BasicEditor ref="phaseTypeForm"
                                    key={phase.type}
                                    project={project}
                                    phase={phase}
                                    card={card}/>;
                break;

            default:
                return null;
                break;
        }
    }
}

PhaseTypeEditor.propTypes = {
    project: PropTypes.object.isRequired,
    phase:   PropTypes.object.isRequired,
    card:    PropTypes.object//.isRequired
};
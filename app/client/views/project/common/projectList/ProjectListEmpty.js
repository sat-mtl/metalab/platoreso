"use strict";

import {Link} from 'react-router';
import React, { Component, PropTypes } from "react";
import UserHelpers from "/imports/accounts/UserHelpers";
import {ConnectMeteorData} from '../../../../lib/ReactMeteorData';

export class ProjectListEmpty extends Component {

    constructor(props) {
        super(props);
        this.getNextSteps = this.getNextSteps.bind(this);
    }

    getNextSteps() {
        const { user, group } = this.props;

        if ( user ) {
            if ( UserHelpers.isExpert(user) || ( group && group.isExpert(user) ) ) {
                return <Link to="/project/create">{ __('Try creating one.') }</Link>;
            } else {
                return __('You might not have access to any projects due to your permissions.');
            }
        } else {
            return <Link to="/login">{ __('Login or create an account to access more.') }</Link>;
        }
    }

    render() {
        const { user } = this.props;
        return (
            <div className="centered column empty-project-item empty-tiles">
                <div className="info">
                    <h2 className="ui inverted center aligned header">
                        <div className="content">
                            {__('No projects available')}
                            <div className="sub header">
                                <div>{ user ? __('There are no projects available for the moment.') : __('There are no public projects available for the moment.')}</div>
                                <div>{ this.getNextSteps() }</div>
                            </div>
                        </div>
                    </h2>
                </div>
            </div>
        );
    }
}

ProjectListEmpty.propTypes = {
    user: PropTypes.object,
    group: PropTypes.object
};

export default ConnectMeteorData( props => {
    return {
        user: Meteor.user()
    }
} )( ProjectListEmpty );
"use strict";

import React, {Component, PropTypes} from "react";
import classNames from "classnames";
import prune from "underscore.string/prune";
import {Link} from "react-router";
import {Counts} from "meteor/tmeasday:publish-counts";
import ProjectImagesCollection from "/imports/projects/ProjectImagesCollection";
import {ConnectMeteorData} from "../../../../lib/ReactMeteorData";

export class ProjectListItem extends Component {

    constructor( props ) {
        super( props );
        this.goToProject = this.goToProject.bind( this );
    }

    goToProject() {
        const { project } = this.props;
        this.context.router.push( { pathname: `/project/${(project.slug || project._id)}` } );
    }

    render() {
        const { project, cardCount, projectImage } = this.props;

        const style = {
            backgroundColor: projectImage && projectImage.dominantColor ? '#' + projectImage.dominantColor : colorCollection[project.slug.length % colorCollection.length],
            backgroundImage: projectImage ? 'url(' + projectImage.url( { store: projectImage.collectionName + '_medium' } ) + ')' : 'none'
        };

        return (
            <div className={classNames( "column project project-item tile", { colored: !projectImage } )} onClick={this.goToProject}>
                <div className="background" style={style}></div>
                <div className="layout">
                    <div className="name">
                        <Link to={`/project/${project.uri}`} title={project.name}>
                            { project.name }
                        </Link>
                    </div>
                    <div className="description">{ prune( project.description, 140 ) }</div>
                    <div className="extra">
                        <i className="white idea icon"/>
                        {__( 'card.count', { count: cardCount } )}
                    </div>
                </div>
            </div>
        );
    }
}

ProjectListItem.propTypes = {
    project:      PropTypes.object.isRequired,
    projectImage: PropTypes.object,
    cardCount:    PropTypes.number
};

ProjectListItem.contextTypes = {
    router: PropTypes.object.isRequired
};

export default ConnectMeteorData( props => {
    const { project } = props;
    Meteor.subscribe( 'project/card/count', project._id );
    return {
        cardCount:    Counts.get( 'project/card/count/' + project._id ),
        projectImage: ProjectImagesCollection.findOne( { owners: project._id } )
    };
} )( ProjectListItem );
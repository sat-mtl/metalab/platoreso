"use strict";

import DateUtil from '../../../lib/utils/DateUtil';

import React, { Component, PropTypes } from "react";
import classNames from "classnames";

export default class PhaseBlock extends Component {

    render() {
        const { phase } = this.props;

        return (
            <div className={classNames("phaseBlock", this.props.className)} style={{borderColor: colorCollection[phase.index % colorCollection.length] }}>
                <div className="header">
                    <div className="name">{phase.name}</div>
                    <div className="date">
                        {DateUtil.getShortDateRange(phase.startDate, phase.endDate)}
                    </div>
                </div>
                { !_.isEmpty( phase.description ) && <p className="description">{phase.description}</p> }
                { /* !_.isEmpty( phase.description )
                    ? <p className="description">{phase.description}</p>
                    : <p className="no description">{__( 'This phase has no description' )}</p>
                */ }
                { this.props.children }
            </div>
        );
    }
}

PhaseBlock.propTypes = {
    phase: PropTypes.object.isRequired
};
"use strict";

import moment from "moment";
import DateUtil from '../../../lib/utils/DateUtil';

import React, {Component, PropTypes} from "react";
import classNames from "classnames";

export default class ProjectTimeline extends Component {

    constructor( props ) {
        super( props );

        this.state = {
            selectedPhase: null
        };
    }

    componentWillReceiveProps( nextProps ) {
        this.setState( {
            selectedPhase: nextProps.selectedPhase
        } );
    }

    render() {
        const { project } = this.props;
        const { selectedPhase } = this.state;

        if ( !project.phases ) {
            return null;
        }

        const now = moment();

        // Map only the info we need
        const mappedPhases = project.phases
                                    .map( ( phase ) => ({
                                        _id:   phase._id,
                                        start: phase.startDate ? moment( phase.startDate ) : null,
                                        end:   phase.endDate ? moment( phase.endDate ) : null,
                                        color: colorCollection[phase.index % colorCollection.length]
                                    }) );

        // Compute date range for the whole project
        let min = null;
        let max = null;
        mappedPhases.forEach( phase => {
            if ( !!phase.start ) {
                min = min == null ? phase.start : moment.min( min, phase.start );
            }
            if ( !!phase.end ) {
                max = max == null ? phase.end : moment.max( max, phase.end );
            }
        } );

        if ( min == null || max == null ) {
            // We can't reliably render a timeline without min or max
            // This will be a problem when we use phases without data ranges
            return null;
        }

        const range = max.diff( min );
        const renderNow = now.isBetween( min, max );

        // Render
        return (
            <div className="timelines">
                <div className="project dates">
                    <div className="date start">{ DateUtil.getShortDate( min ) }</div>
                    <div className="date end">{ DateUtil.getShortDate( max ) }</div>
                </div>
                <div className="phases">
                    { renderNow &&
                      <div className="now" style={{ left: (( now.valueOf() - min.valueOf() ) / range)*100+'%' } }></div> }

                    { mappedPhases.map( phase => {
                        const started      = now.isAfter( phase.start );
                        const ended        = now.isAfter( phase.end );
                        const hasSelection = !!selectedPhase;
                        const isSelected   = hasSelection && selectedPhase._id == phase._id;

                        const start = !!phase.start ? phase.start : min;
                        const end   = !!phase.end ? phase.end : max;

                        const startLeft = ( start.valueOf() - min.valueOf() ) / range;
                        const nowLeft   = ( now.valueOf() - min.valueOf() ) / range;
                        const width     = ( end.valueOf() - start.valueOf() ) / range;

                        const pastTimelineStyle = {
                            backgroundColor: phase.color,
                            opacity:         hasSelection ? ( isSelected ? 0.75 : 0.25 ) : 0.5,
                            left:            startLeft * 100 + '%',
                            width:           ( ended ? width : ( nowLeft - startLeft ) ) * 100 + '%'
                        };

                        const futureTimelineStyle = {
                            backgroundColor: phase.color,
                            opacity:         hasSelection ? ( isSelected ? 1.0 : 0.5 ) : 1.0,
                            left:            Math.max( nowLeft, startLeft ) * 100 + '%',
                            width:           ( started ? width - (nowLeft - startLeft) : width ) * 100 + '%'
                        };

                        const beforeStyle = {
                            width: startLeft * 100 + '%'
                        };

                        const afterStyle = {
                            width: (1 - ( startLeft + width ) ) * 100 + '%'
                        };

                        return (
                            <div key={phase._id}
                                 className={classNames('phase', {
                                     faded: selectedPhase && selectedPhase._id != phase._id,
                                     active: selectedPhase && selectedPhase._id == phase._id,
                                     indeterminate: !phase.start || !phase.end
                                })}
                                 onMouseEnter={()=>this.setState({selectedPhase: phase})}
                                 onMouseLeave={()=>this.setState({selectedPhase: this.props.selectedPhase })}>
                                <div className="dates">
                                    <span className="before" style={beforeStyle}/>
                                    <div className="labels">
                                        { phase.start ?
                                          <div className="label date start" style={{backgroundColor: phase.color}}>{ DateUtil.getShortDate( phase.start ) }</div> : null }
                                        { phase.end ?
                                          <div className="label date end" style={{backgroundColor: phase.color}}>{ DateUtil.getShortDate( phase.end ) }</div> : null }
                                    </div>
                                    <span className="after" style={afterStyle}/>
                                </div>
                                { started && <div style={pastTimelineStyle} className="timeline"></div> }
                                { !ended && <div style={futureTimelineStyle} className="timeline"></div> }
                            </div>
                        );
                    } ) }
                </div>
            </div>
        );
    }

}

ProjectTimeline.propTypes = {
    project:       PropTypes.object.isRequired,
    selectedPhase: PropTypes.object
};
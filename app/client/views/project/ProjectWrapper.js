"use strict";

import React, {Component, PropTypes} from "react";
import ProjectMethods from '/imports/projects/ProjectMethods';

export default class ProjectWrapper extends Component {

    constructor(props) {
        super(props);

        this.trackProjectView = this.trackProjectView.bind(this);

        this.state = {
            trackedProjectView:  false
        }
    }

    trackProjectView() {
        if ( this.props.params.id && !this.state.trackedProjectView ) {
            ProjectMethods.view.call( { projectId: this.props.params.id } );
            this.setState( { trackedProjectView: true } );
        }
    }

    componentDidMount(){
        this.trackProjectView();
    }

    componentWillReceiveProps( nextProps ) {
        if ( this.props.params.id != nextProps.params.id  ) {
            // We're changing the displayed project, so prepare to track a new project view
            this.setState( {
                trackedProjectView: false
            } );
        }
    }

    componentDidUpdate( prevProps, prevState ) {
        this.trackProjectView();
    }

    render() {
        return this.props.children;
    }
}
"use strict";

import React, { Component, PropTypes } from "react";

import GroupsCollection from "/imports/groups/GroupCollection";

import Field from '../../components/forms/Field';
import Input from '../../components/forms/Input';
import Select from '../../components/forms/Select';
import TextArea from '../../components/forms/Textarea';
import CheckBox from '../../components/forms/Checkbox';
import ImageChooser from '../../components/forms/ImageChooser';
import { ConnectMeteorData } from '../../lib/ReactMeteorData'

export class ProjectForm extends Component {

    render() {
        const { groups, project, image, pictureChanged } = this.props;

        return (
            <div className="ui stackable grid projectForm">

                <div className="row">

                    <div className="ten wide column">
                        <Field label={__( "Name" )} className="required">
                            <Input ref="name" name="name" type="text"
                                   defaultValue={project && project.name}
                                   rules={[
                                       { type: 'empty', prompt: __( 'Please enter a name' ) },
                                       { type: 'minLength[3]', prompt: __( 'Project name should be a minimum of 3 characters long' ) },
                                       { type: 'maxLength[140]', prompt: __( 'Project name can be a maximum of 140 characters long' ) },
                                       { type: 'regExp[/(!?[a-zA-Z0-9])/]', prompt: __( 'Project names should not only contain non-alphanumeric characters' ) }
                                   ]}/>
                        </Field>

                        <Field label={__( "Description" )} className="required">
                            <TextArea ref="description"
                                      name="description"
                                      rows={2}
                                      autosize={true}
                                      defaultValue={ project && project.description }
                                      rules={[
                                          { type: 'empty', prompt: __( 'Please enter a description' ) }
                                      ]}/>
                        </Field>

                        <Field label={__( "Hashtag" )}>
                            <Input ref="hashtag" name="hashtag" type="text" className="labeled"
                                   defaultValue={project && project.hashtag}
                                   rules={[
                                       { type: 'doesntContain[#]', prompt: __( 'You do not need to put the # symbol.' ) },
                                       { type: 'regExp[/^[a-z0-9_]*$/gi]', prompt: __( 'Only letters, numbers and underscores are allowed.' ) }
                                   ]}
                                   before={<div className="ui label">#</div>}/>
                        </Field>

                        <div className="two fields">

                            <Field label={__( "Visibility" )}>
                                <CheckBox ref="public" name="public" label={__( 'Public' )}
                                          defaultChecked={project && project.public}/>
                            </Field>

                            <Field label={__( "Timeline" )}>
                                <CheckBox ref="showTimeline" name="showTimeline" defaultChecked={project && project.showTimeline} label={__( 'Show Timeline' )}/>
                            </Field>

                        </div>

                        <Field label={__( "Groups" )} className="required">
                            <Select name="groups"
                                    multiple={true}
                                    className="fluid"
                                    defaultValue={project && project.groups}
                                    rules={[
                                        { type: 'empty', prompt: __( 'Please select at least one group' ) }
                                    ]}>
                                { groups.map( group => {
                                    return (<option key={group._id} value={group._id}>{group.name}</option>)
                                } ) }
                            </Select>
                        </Field>

                    </div>

                    <div className="six wide column">
                        <Field label={__( 'Picture' )}>
                            <ImageChooser image={image} filesChanged={pictureChanged}/>
                        </Field>
                    </div>
                </div>

                {/* Extra form elements, in case they need to be managed by the parent */}
                { this.props.children }
            </div>
        );
    }
}

ProjectForm.propTypes = {
    groups:         PropTypes.arrayOf( PropTypes.object ).isRequired,
    project:        PropTypes.object,
    image:          PropTypes.object,
    pictureChanged: PropTypes.func.isRequired
};

/**
 * Meteor Data Wrapper
 */
export default ConnectMeteorData( ( { project } ) => {
    const groupsLoading = !Meteor.subscribe( 'project/edit/group/list', project && project._id ).ready();
    const groups        = GroupsCollection.findUserManagedGroups( Meteor.userId(), project ? { _id: { $in: project.groups } } : null ).fetch();
    return {
        loading: groupsLoading,
        groups:  groups
    }
} )( ProjectForm );
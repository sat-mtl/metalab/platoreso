"use strict";

import React, { Component, PropTypes } from "react";
import prune from "underscore.string/prune";
import ModerationMethods from "/imports/moderation/ModerationMethods";
import GroupCollection from "/imports/groups/GroupCollection";
import ProjectSecurity from "/imports/projects/ProjectSecurity";
import ProjectCollection from "/imports/projects/ProjectCollection";
import ProjectImagesCollection from "/imports/projects/ProjectImagesCollection";
import { Link } from "react-router";
import ContainedPage from "../layouts/ContainedPage";
import { ConnectMeteorData } from "../../lib/ReactMeteorData";
import Spinner from "../widgets/Spinner";
import NotFound from "../errors/NotFound";
import Feed from "../feed/Feed";
import Phases from "./page/Phases";
import ProjectTimeline from "./common/ProjectTimeline";
import GroupLink from "./page/GroupLink";
import TrendingCards from "./page/TrendingCards";
import TopCards from "./page/TopCards";
import TopUsers from "./page/TopUsers";

export class Project extends Component {

    constructor( props ) {
        super( props );
        this.reportProject = this.reportProject.bind( this );
        this.editProject   = this.editProject.bind( this );
    }

    reportProject() {
        const { project } = this.props;
        if ( !project ) {
            return;
        }

        ModerationMethods.report.call( {
            entityTypeName: ProjectCollection.entityName,
            entityId:       project._id
        } );
    }

    editProject() {
        this.context.router.push( { pathname: `/project/${this.props.project.uri}/edit` } );
    }

    render() {
        const { userId, projectLoading, project, projectImage, groups } = this.props;

        if ( projectLoading ) {
            return <Spinner/>;

        } else if ( !project ) {
            return <NotFound/>;

        } else {
            const headerStyle = {
                backgroundColor: projectImage && projectImage.dominantColor ? '#' + projectImage.dominantColor : colorCollection[project.slug.length % colorCollection.length],
                backgroundImage: projectImage ? 'url(' + projectImage.url( { store: projectImage.collectionName + '_large' } ) + ')' : 'none'
            };

            const youReported = project.isReportedBy( userId );

            return (
                <ContainedPage id="project">

                    {/* Header */}
                    <header className="sixteen wide column heading" style={headerStyle}>
                        <h2 className="ui center aligned inverted header">
                            <div className="content">
                                { project.name }
                                <div className="sub header">
                                    <p>{ prune( project.description, 140 ) }</p>
                                </div>
                            </div>
                        </h2>
                    </header>

                    {/* Board Teaser */}
                    <div className="sixteen wide column board-teaser">
                        { !_.isEmpty( project.phases ) && <ProjectTimeline project={project}/> }
                        <Link to={`/project/${project.uri}/board`} className="ui massive olive button">{__( "View Board" )}</Link>
                        <Link to={`/project/${project.uri}/graph`} className="ui massive olive button">{__( "View Graph" )}</Link>
                    </div>

                    {/* Left Column */}
                    <div className="ten wide column">

                        <div className="ui grid">

                            {/* Content */}
                            { project.content && <section id="project-content" className="sixteen wide column">
                                <div className="ui basic segment" dangerouslySetInnerHTML={{ __html: project.content }}></div>
                            </section> }

                            {/* Feed */}
                            <div className="sixteen wide column feed project-feed">
                                <h3 className="ui dividing header">{__( 'Activity' )}</h3>
                                <Feed entities={project._id}/>
                            </div>

                        </div>

                    </div>

                    <div className="six wide column sidebar">

                        {/* Phases */}
                        { !_.isEmpty( project.phases ) && <div className="sidebar-block">
                            <h3 className="ui dividing header">{__( 'Phases' )}</h3>
                            <Phases project={project}/>
                        </div> }

                        {/* Top Users */}
                        <TopUsers project={project}/>

                        {/* Top */}
                        <TopCards project={project}/>

                        {/* Trending */}
                        <TrendingCards project={project}/>

                        {/* Groups */}
                        <div className="sidebar-block">
                            <h3 className="ui dividing header">{__( 'Groups' )}</h3>
                            <div className="ui selection list">
                                { groups.map( group => <GroupLink key={group._id} group={group}/> ) }
                            </div>
                        </div>

                        {/* Menu */}
                        <div className="ui fluid secondary vertical menu">
                            <h3 className="ui dividing header">{__( 'Menu' )}</h3>

                            {/* Board */}
                            <Link to={`/project/${project.uri}/board`} className="link item">
                                {__( 'Project Board' )}
                            </Link>

                            {/* Graph */}
                            <Link to={`/project/${project.uri}/graph`} className="link item">
                                {__( 'Project Graph' )}
                            </Link>

                            {/* Edit */}
                            { ProjectSecurity.canEdit( userId, project._id ) &&
                              <div className="link item" onClick={this.editProject}>
                                  { __( 'Edit Project' ) }
                              </div> }

                            {/* Report */}
                            <div className="link item" onClick={this.reportProject}>
                                { youReported ? __( 'You reported this' ) : __( 'Report' ) }
                            </div>

                        </div>
                    </div>

                </ContainedPage>
            );
        }
    }
}

Project.propTypes = {
    userId:         PropTypes.string,
    projectLoading: PropTypes.bool.isRequired,
    project:        PropTypes.object,
    projectImage:   PropTypes.object,
    groups:         PropTypes.arrayOf( PropTypes.object )
};

Project.contextTypes = {
    router: PropTypes.object.isRequired
};

export default ConnectMeteorData( props => {
    const { id }  = props.params;
    const project = ProjectCollection.findOne( { moderated: false, $or: [{ _id: id }, { slug: id }] } );
    return {
        userId:         Meteor.userId(),
        projectLoading: !Meteor.subscribe( 'project', id ).ready(),
        project:        project,
        projectImage:   project ? ProjectImagesCollection.findOne( { owners: project._id } ) : null,
        groups:         project && project.groups ? GroupCollection.find( { _id: { $in: project.groups } } ).fetch() : []
    }
} )( Project );

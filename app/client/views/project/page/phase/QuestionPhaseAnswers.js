"use strict";

import {getQuestionList} from '../../common/phaseTypes/questions/QuestionRecord';
import QuestionPhaseAnswer from './QuestionPhaseAnswer';

import React, { Component, PropTypes } from "react";

export default class QuestionPhaseAnswers extends Component {

    constructor( props ) {
        super( props );

        this.state = {
            questions: getQuestionList( props.phase )
        };
    }

    render() {
        const { project, phase } = this.props;
        const { questions } = this.state;

        const filteredQuestions = questions.filter( question => question.answers && question.answers.size > 0 );
        if ( filteredQuestions.size == 0 ) {
            return null;
        }

        return (
            <div className="ui stackable grid questions">
                { filteredQuestions.map( question => <QuestionPhaseAnswer key={question._id} project={project} phase={phase} question={question}/> ) }
            </div>
        )
    }
}

QuestionPhaseAnswers.propTypes = {
    project: PropTypes.object.isRequired,
    phase:   PropTypes.object.isRequired
};
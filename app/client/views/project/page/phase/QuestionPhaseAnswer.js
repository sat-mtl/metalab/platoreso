"use strict";

import React, {Component, PropTypes} from "react";
import {Counts} from "meteor/tmeasday:publish-counts";
import {ConnectMeteorData} from "../../../../lib/ReactMeteorData";
import Chart from "../../../../components/chartjs/Chart";

export class QuestionPhaseAnswer extends Component {

    render() {
        const { project, phase, question, chart = {} } = this.props;

        // Only render if at least an answer got a vote, this is due to a bug in chart.js
        return (
            <div className="sixteen wide column question">
                {question.prompt}
                <Chart type="doughnut" data={chart}/>
            </div>
        )
    }
}

QuestionPhaseAnswer.propTypes = {
    project:  PropTypes.object.isRequired,
    phase:    PropTypes.object.isRequired,
    question: PropTypes.object.isRequired
};

export default ConnectMeteorData( props => {
    const { project, phase, question } = props;

    let data = {
        resultsLoading: !Meteor.subscribe( 'question/results', project._id, phase._id, question._id ).ready(),
        chart:          { labels: [], datasets: [{ data: [], backgroundColor: [], hoverBackgroundColor: [] }] }
    };

    question.answers.forEach( answer => {
        data.chart.labels.push( answer.prompt );
        data.chart.datasets[0].data.push( Counts.get( 'question/results/' + project._id + '/' + phase._id + '/' + question._id + '/' + answer._id ) );
        data.chart.datasets[0].backgroundColor.push( colorCollection[data.chart.datasets[0].backgroundColor.length % colorCollection.length] );
        data.chart.datasets[0].hoverBackgroundColor.push( "#FF5A5E" );
        /*data.answers.push( {
         color:"#F7464A",
         highlight: "#FF5A5E",
         label: answer.prompt,
         value: Counts.get( 'question/results/' + project._id + '/' + phase._id + '/' + question._id + '/' + answer._id )
         } );*/
    } );

    return data;
} )( QuestionPhaseAnswer );
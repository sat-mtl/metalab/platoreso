"use strict";

import React, {Component, PropTypes} from "react";
import {Link} from "react-router";
import {ConnectMeteorData} from "../../../lib/ReactMeteorData";
import UsersCollection from "/imports/accounts/UserCollection";
import UserHelpers from "/imports/accounts/UserHelpers";
import Avatar from "../../widgets/Avatar";

export class TopUsers extends Component {

    constructor( props ) {
        super( props );
    }

    render() {
        const { loading, users, project } = this.props;

        if ( !users || !users.length ) {
            return null;

        } else {
            return (
                <div className="sidebar-block">
                    <h3 className="ui dividing header">{__( 'Top Contributors' )}</h3>
                    <div className="ui relaxed list topUsers">
                        { users.map( user => <div key={user._id} className="topUser item">
                            <Avatar user={user}/>
                            <div className="content">
                                <Link to={`/profile/${user._id}`} className="header">{UserHelpers.getDisplayName( user )}</Link>
                                <div className="description">
                                    {/*<div className="ui tiny teal label">*/}
                                    <i className="trophy icon"/>&nbsp;{user.pointsPerProject[project._id]}
                                    {/*</div>*/}
                                </div>
                            </div>
                        </div> ) }
                    </div>
                </div>
            );
        }
    }
}

TopUsers.propTypes = {
    project: PropTypes.object.isRequired,
    loading: PropTypes.bool,
    users:   PropTypes.arrayOf( PropTypes.object )
};

TopUsers.defaultProps = {
    loading: true,
    users:   []
};

const LIMIT = 6;

export default ConnectMeteorData( props => {
    const { project } = props;
    const usersQuery  = UsersCollection.find( {
        [`pointsPerProject.${project._id}`]: { $exists: true }
    }, {
        sort:  { [`pointsPerProject.${project._id}`]: -1 },
        limit: LIMIT
    } );

    return {
        loading: !Meteor.subscribe( 'project/users/top', project._id, LIMIT ).ready(),
        users:   usersQuery.fetch()
    }
} )( TopUsers );

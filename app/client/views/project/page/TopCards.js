"use strict";

import React, {Component, PropTypes} from "react";
import {ConnectMeteorData} from "../../../lib/ReactMeteorData";
import CardCollection from "../../../../imports/cards/CardCollection";
import {FullyWrappedCardPreview as CardPreview} from "../../card/CardPreview";

export class TopCards extends Component {

    constructor( props ) {
        super( props );
    }

    render() {
        const { loading, cards, project } = this.props;

        if ( !cards || !cards.length ) {
            return null;

        } else {
            return (
                <div className="sidebar-block">
                    <h3 className="ui dividing header">{__( 'Top Cards' )}</h3>
                    <div className="topCards">
                        { cards.map( card =>
                            <CardPreview key={card.id}
                                         cardId={card.id}
                                         project={project}
                                         layout="list"
                                         className="topCard"/>
                        ) }
                        {/* FIXME: Layout grid hack
                         Hack to get flex rows to leave enough empty space
                         We leave the same number of empty cards as there are cards to make sure we have enough
                         */}
                        { cards.map( ( card, index ) =>
                            <div key={index} className="topCard"></div> )}
                    </div>
                </div>
            );
        }
    }
}

TopCards.propTypes = {
    project: PropTypes.object.isRequired,
    loading: PropTypes.bool,
    cards:   PropTypes.arrayOf( PropTypes.object )
};

TopCards.defaultProps = {
    loading: true,
    cards:   []
};

const LIMIT = 5;

export default ConnectMeteorData( props => {
    const { project } = props;
    const cardsQuery  = CardCollection.find( {
        project:          project._id,
        current:          true,
        moderated:        false,
        totalTemperature: { $gt: 0 }
    }, {
        sort:  { totalTemperature: -1 },
        limit: LIMIT
    } );

    return {
        loading: !Meteor.subscribe( 'project/card/top', project._id, LIMIT ).ready(),
        cards:   cardsQuery.fetch()
    }
} )( TopCards );

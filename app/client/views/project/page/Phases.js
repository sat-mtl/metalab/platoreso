"use strict";

import Phase from './Phase';

import React, { Component, PropTypes } from "react";

export default class Phases extends Component {
    render() {
        const { project } = this.props;
        return (
            <div className="phases">
                { project.phases.map( phase => <Phase key={phase._id} project={project} phase={phase}/> ) }
            </div>
        );
    }
}

Phases.propTypes = {
    project: PropTypes.object.isRequired
};
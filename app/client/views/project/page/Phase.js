"use strict";

import PhaseTypes from "/imports/projects/phases/PhaseTypes";

import moment from "moment";
import DateUtil from '../../../lib/utils/DateUtil';
import PhaseBlock from '../common/PhaseBlock';
import QuestionPhaseAnswers from './phase/QuestionPhaseAnswers';

import React, { Component, PropTypes } from "react";

export default class Phase extends Component {

    renderDetails( project, phase ) {
        if ( !phase.hasStarted ) {
            return (
                <div className="extra">
                    <div className="ui tiny grey statistic">
                        <div className="label">
                            {__('Starts in')}
                        </div>
                        <div className="value">
                            {moment(phase.startDate).toNow(true)}
                        </div>
                    </div>
                </div>
            );
        }

        switch ( phase.type ) {

            case PhaseTypes.map.questions:
                return <QuestionPhaseAnswers ref="questions"
                                  key={PhaseTypes.map.questions}
                                  project={project}
                                  phase={phase}/>;
                break;

            default:
                return null;
                break;
        }
    }

    render() {
        const { project, phase } = this.props;

        return (
            <PhaseBlock phase={phase}>
                    { this.renderDetails( project, phase ) }
            </PhaseBlock>
        );
    }
}

Phase.propTypes = {
    project: PropTypes.object.isRequired,
    phase: PropTypes.object.isRequired
};
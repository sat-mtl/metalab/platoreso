"use strict";

import React, {Component, PropTypes} from "react";
import classNames from "classnames";
import prune from "underscore.string/prune";
import {Link} from 'react-router';
import {ConnectMeteorData} from '../../../lib/ReactMeteorData';
import GroupImagesCollection from "/imports/groups/GroupImagesCollection";
import Image from '../../widgets/Image';

export class GroupLink extends Component {

    renderAvatar(group, image) {

        if ( image ) {
            return (
                <Image className="mini circular" image={image} size="thumb"/>

            );
        } else {
            const initials = group.initials;
            const index = ( initials.charCodeAt(0) * 128 ) + initials.charCodeAt(1);
            const avatarStyle = { backgroundColor: colorCollection[index % colorCollection.length], };
            return (
                <div className="ui circular avatar initials image" style={avatarStyle}>
                    <div className="text">{initials}</div>
                </div>
            );
        }
    }

    render() {
        const { group, image } = this.props;
        return (
            <div className="item">
                {this.renderAvatar(group, image)}
                <div className="middle aligned content">
                    <Link to={`/group/${group.uri}`} className="header">{group.name}</Link>
                    <div className="description">{prune( group.description, 36 )}</div>
                </div>
            </div>
        );
    }
}

GroupLink.propTypes = {
    group: PropTypes.object.isRequired,
    image: PropTypes.object
};

export default ConnectMeteorData( props => ({
    image: GroupImagesCollection.findOne( { owners: props.group._id } )
}) )( GroupLink );

"use strict";

import React, {Component, PropTypes} from "react";
import UserMethods from "/imports/accounts/UserMethods";
import ErrorPage from "../layouts/ErrorPage";
import ContainedPage from "../layouts/ContainedPage";

export default class Unsubscribe extends Component {

    constructor( props ) {
        super( props );

        this.state = {
            error:        null,
            unsubscribed: false
        }
    }

    componentDidMount() {
        const { token } = this.props.params;

        if ( token ) {
            UserMethods.unsubscribe( token, err => {
                if ( err ) {
                    this.setState( { error: err.reason } );
                } else {
                    this.setState( { unsubscribed: true } );
                }
            } );
        }
    }

    render() {
        const { error, unsubscribed } = this.state;

        if ( error ) {
            return (
                <ErrorPage>
                    <div className="error-page forbidden">
                        <h2 className="ui inverted icon header">
                            <i className="warning circle icon"/>
                            <div className="content">
                                {__( 'Unsubscription error' )}
                                <div className="sub header">{error}</div>
                            </div>
                        </h2>
                    </div>
                    <div>
                        {this.props.children}
                    </div>
                </ErrorPage>
            );
        } else if ( !unsubscribed ) {
            return (
                <div className="ui active dimmer">
                    <div className="ui large indeterminate text loader">
                        {__( 'Unsubscribing...' )}
                    </div>
                </div>
            );
        } else {
            return (
                <ContainedPage>
                    <h2 className="ui center aligned icon header">
                        <i className="mail outline icon"/>
                        <div className="content">
                            {__( 'Unsubscribed' )}
                        </div>
                    </h2>
                </ContainedPage>
            );
        }
    }

}
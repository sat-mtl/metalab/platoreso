"use strict";

import UserHelpers from "/imports/accounts/UserHelpers";
import React, {Component, PropTypes} from "react";
import classNames from "classnames";
import {ConnectMeteorData} from "../../lib/ReactMeteorData";
import ContainedPage from "../layouts/ContainedPage";
import Registration from "./creation/Registration";
import ProfileCompletion from "./creation/ProfileCompletion";
import EmailValidation from "./creation/EmailValidation";
import TermsOfService from "./creation/TermsOfService";
import Congratulations from "./creation/Congratulations";

class AccountCreation extends Component {

    constructor( props ) {
        super( props );
    }

    render() {
        const { user }    = this.props;
        let step          = 0;
        let stepComponent = null;
        if ( user == null ) {
            step          = 1;
            stepComponent = <Registration/>
        } else if ( !UserHelpers.hasEmail( user ) ) {
            //TODO: Email only form
            step          = 1;
            stepComponent = <Registration/>
        } else if ( !UserHelpers.hasVerifiedEmail( user ) ) {
            step          = 2;
            stepComponent = <EmailValidation/>;
        } else if ( !UserHelpers.isProfileCompleted( user ) || !user.completed ) { //FIXME: Yeah, one does not mean the other
            step          = 3;
            stepComponent = <ProfileCompletion/>;
        } else if ( !user.acceptedTerms ) {
            step          = 4;
            stepComponent = <TermsOfService/>;
        } else {
            step          = 5;
            stepComponent = <Congratulations/>;
        }

        return (
            <ContainedPage>

                <header className="sixteen wide column">
                    <h2 className="ui center aligned header">
                        {__( 'Profile Completion' )}
                        <div className="sub header">{__( 'Please complete your profile before continuing.' )}</div>
                    </h2>
                </header>

                <div className="centered row">
                    <div className="sixteen wide column">
                        <div className="ui ordered computer stackable tiny steps">
                            <div className={ classNames( { completed: step > 1, active: step == 1 }, 'step' ) }>
                                <div className="content">
                                    <div className="title">{__( 'Register' )}</div>
                                    <div className="description">{__( 'Create your account' )}</div>
                                </div>
                            </div>
                            <div className={ classNames( { completed: step > 2, active: step == 2 }, 'step' ) }>
                                <div className="content">
                                    <div className="title">{__( 'Verification' )}</div>
                                    <div className="description">{__( 'Verify your email address' )}</div>
                                </div>
                            </div>
                            <div className={ classNames( { completed: step > 3, active: step == 3 }, 'step' ) }>
                                <div className="content">
                                    <div className="title">{__( 'Profile' )}</div>
                                    <div className="description">{__( 'Enter profile information' )}</div>
                                </div>
                            </div>
                            <div className={ classNames( { completed: step > 4, active: step == 4 }, 'step' ) }>
                                <div className="content">
                                    <div className="title">{__( 'Terms' )}</div>
                                    <div className="description">{__( 'Terms of service' )}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="centered doubling twelve wide column">
                        {stepComponent}
                    </div>
                </div>

            </ContainedPage>
        );
    }

}

/**
 * Meteor Data Wrapper
 */
const AccountCreationComponent = ConnectMeteorData( props => ({
    user: Meteor.user()
}) )( AccountCreation );

export default AccountCreationComponent;
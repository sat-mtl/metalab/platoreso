"use strict";

import AccountHelpers from "/imports/accounts/AccountHelpers";
import ErrorPage from '../layouts/ErrorPage';

import React, { Component, PropTypes } from "react";

export default class EmailVerification extends Component {

    constructor( props ) {
        super( props );

        this.state = {
            error: null
        }
    }

    componentDidMount() {
        const {token} = this.props.params;

        if ( token ) {
            //TODO: Move out of the component
            AccountHelpers.verifyEmail( token, err => {
                if ( err ) {
                    this.setState( { error: err.reason } );
                } else {
                    this.context.router.replace( { pathname: '/register' } );
                }
            } );
        }
    }

    render() {
        const {error} = this.state;
        let message = null;

        if ( error ) {
            message = (
                <div className="error-page forbidden">
                    <h2 className="ui inverted icon header">
                        <i className="warning circle icon"/>
                        <div className="content">
                            {__('Email validation error')}
                            <div className="sub header">{error}</div>
                        </div>
                    </h2>
                </div>
            );
        } else {
            message = (
                <div className="ui active dimmer">
                    <div className="ui large indeterminate text loader">
                        {__( 'Verifying email...' )}
                    </div>
                </div>
            );
        }
        return (
            <ErrorPage>
                {message}
                <div>
                    {this.props.children}
                </div>
            </ErrorPage>
        );
    }

}

EmailVerification.contextTypes = {
    router: PropTypes.object.isRequired
};

"use strict";

import React, { Component, PropTypes } from "react";
import classNames from "classnames";
import AccountHelpers from "/imports/accounts/AccountHelpers";
import ContainedPage from '../layouts/ContainedPage';
import Form from '../../components/forms/Form';
import Field from '../../components/forms/Field';
import Input from '../../components/forms/Input';

export default class LostPassword extends Component {

    constructor( props ) {
        super( props );

        this.reset     = this.reset.bind( this );
        this.resetForm = this.resetForm.bind( this );

        this.state = {
            reset:     false,
            resetting: false,
            error:     null
        }
    }

    resetForm() {
        this.setState( {
            reset:     false,
            resetting: false,
            error:     null
        } );
    }

    reset( values ) {
        if ( this.state.resetting ) {
            return;
        }

        this.setState( { resetting: true } );

        AccountHelpers.forgotPassword( values, err => {
            if ( err ) {
                this.setState( { error: err } )
            } else {
                this.setState( { reset: true } )
            }
            this.setState( { resetting: false } );
        } );
    }

    render() {
        return (
            <ContainedPage>

                <div className="row">
                    <header className="column">
                        <h2 className="ui center aligned header">
                            {__('Lost password')}
                            <div className="sub header">
                                {__('Reset your lost PlatoReso password.')}
                            </div>
                        </h2>
                    </header>
                </div>

                <div className="row">
                    <div className="center aligned column">
                        { this.state.reset
                            ? __('An email was sent to your mailbox, click the password reset in that email to reset your password.')
                            : __('Enter your email to receive a password reset link.')
                        }
                    </div>
                </div>

                <div className="row">
                    <div className="ui column centered grid">
                        <div className="twelve wide mobile six wide column">
                            { this.state.reset ? (
                                <div className="ui basic button grey fluid" onClick={this.resetForm}>
                                    {__('Resend')}
                                </div>
                            ) : (
                                <Form id="password-lost-form" ref="passwordLostForm"
                                      error={this.state.error}
                                      onSuccess={this.reset}>

                                    <Field label={__("Email")}>
                                        <Input name="email" type="email" className="fluid"
                                               rules={[
                                                   { type: 'empty', prompt: __('Please enter your email')}
                                               ]}/>
                                    </Field>

                                    <div className={ classNames("ui huge positive right labeled icon fluid submit button", { loading: this.state.resetting, disabled: this.state.resetting } ) }>
                                        {__('Send password reset email')}
                                        <i className="checkmark icon"/>
                                    </div>
                                </Form>
                            ) }
                        </div>
                    </div>
                </div>

            </ContainedPage>
        );
    }
}
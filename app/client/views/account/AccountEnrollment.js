"use strict";

import React, { Component, PropTypes } from "react";
import classNames from "classnames";

import AccountHelpers from "/imports/accounts/AccountHelpers";
import ContainedPage from '../layouts/ContainedPage';
import Form from '../../components/forms/Form';
import Field from '../../components/forms/Field';
import Input from '../../components/forms/Input';

export default class AccountEnrollment extends Component {

    constructor(props) {
        super(props);

        this.enroll = this.enroll.bind(this);

        this.state = {
            enrolling: false,
            error: null
        }
    }

    componentDidMount() {
        if (!this.props.params.token) {
            this.context.router.replace( { pathname:  '/register'  } );
        }
    }

    enroll( values ) {
        const {token} = this.props.params;

        if (token) {
            if ( this.state.enrolling ) {
                return;
            }

            this.setState({enrolling: true});

            AccountHelpers.resetPassword(token, values.password, err => {
                if (err) {
                    this.setState({ error: err, enrolling: false })
                } else {
                    this.context.router.replace( { pathname: '/register'  } );
                }
            });
        }
    }

    render() {
        return (
            <ContainedPage>
                <div className="row">
                    <header className="column">
                        <h2 className="ui center aligned header">
                            {__('Register')}
                            <div className="sub header">
                                {__('Create a PlatoReso account.')}
                            </div>
                        </h2>
                    </header>
                </div>
                <div className="row">
                    <div className="center aligned column">{__('Welcome to PlatoReso. Before continuing you will need to set a password.')}</div>
                </div>
                <div className="row">
                    <div className="ui column centered grid">
                        <div className="twelve wide mobile six wide column">
                            <Form id="enrollment-form" ref="enrollmentForm"
                                  error={this.state.error}
                                  onSuccess={this.enroll}>

                                <Field label={__("Password")}>
                                    <Input name="password" type="password" className="fluid"
                                           rules={[
                                               { type: 'empty', prompt: __('Please enter your password')},
                                               {type: 'minLength[6]', prompt: __('Your password should be at least 6 characters long')}
                                           ]}/>
                                </Field>
                                <Field label={__("Password (Again)")}>
                                    <Input name="passwordVerification" type="password" className="fluid"
                                           rules={[
                                               { type: 'match[password]', prompt: __('Passwords must match') }
                                           ]}/>
                                </Field>

                                <div className={ classNames("ui huge positive right labeled icon fluid submit button", { loading: this.state.enrolling, disabled: this.state.enrolling } ) }>
                                    {__('Register')}
                                    <i className="checkmark icon"/>
                                </div>
                            </Form>
                        </div>
                    </div>
                </div>
            </ContainedPage>
        );
    }
}

AccountEnrollment.contextTypes = {
    router: PropTypes.object.isRequired
};

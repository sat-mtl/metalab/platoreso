"use strict";

import UserMethods from "/imports/accounts/UserMethods";

import React, { Component, PropTypes } from "react";
import classNames from "classnames";

export default class EmailValidation extends Component {

    constructor(props) {
        super(props);

        this.onSendValidationEmail = this.onSendValidationEmail.bind(this);

        this.state = {
            sending: false,
            sent: false
        }
    }

    onSendValidationEmail(e) {
        this.setState({sending: true});
        UserMethods.sendValidationEmail( err => {
            this.setState({sending: false, sent: true});
        });
    }

    render() {
        const {sending} = this.state;

        return (
            <section className="ui grid">
                <div className="row">
                    <header className="column">
                        <h2 className="ui center aligned header">
                            {__('Email validation')}
                            <div className="sub header">{__('Please validate your email before continuing.')}</div>
                        </h2>
                    </header>
                </div>
                <div className="row">
                    <div className="center aligned column">{__('An email was sent to the address you provided containing a link to activate your account. Check your email and click the link to access PlatoReso.')}</div>
                </div>
                <div className="row">
                    <div className="ui column centered grid">
                        <div className="twelve wide mobile six wide column">
                            <a className={ classNames('ui', 'button', 'grey', 'fluid', { loading: sending, disabled: sending } ) }
                               onClick={this.onSendValidationEmail}>
                                {__('Resend validation email')}
                            </a>
                        </div>
                    </div>
                </div>
            </section>
        );
    }

}
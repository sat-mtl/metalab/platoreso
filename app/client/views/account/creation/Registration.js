"use strict";

import AccountHelpers from "/imports/accounts/AccountHelpers";
import React, { Component, PropTypes } from "react";
import classNames from "classnames";
import Form from '../../../components/forms/Form';
import Field from '../../../components/forms/Field';
import Input from '../../../components/forms/Input';

export default class Registration extends Component {

    constructor( props ) {
        super( props );

        this.register = this.register.bind( this );

        this.state = {
            registering: false,
            error:       null
        }
    }

    /**
     * Send Registration information
     */
    register( values ) {
        if ( this.state.registering ) {
            return;
        }

        this.setState( { registering: true } );

        AccountHelpers.register( values, err => {
            if ( err ) {
                this.setState( { error: err } );
            }
            this.setState( { registering: false } );
        } );
    }

    render() {
        return (
            <section className="ui grid">
                <div className="row">
                    <header className="column">
                        <h2 className="ui center aligned header">
                            {__( 'Register' )}
                            <div className="sub header">
                                {__( 'Create a PlatoReso account.' )}
                            </div>
                        </h2>
                    </header>
                </div>
                <div className="row">
                    <div className="centered doubling twelve wide column">
                        <Form id="registration-form" ref="registrationForm" error={this.state.error} onSuccess={this.register}>

                            <Field label={__( "Email" )}>
                                <Input name="email" type="email" className="fluid"
                                       rules={[
                                           { type: 'empty', prompt: __( 'Please enter your email' ) },
                                           { type: 'email', prompt: __( 'Please enter a valid email' ) }
                                       ]}/>
                            </Field>

                            <Field label={__( "Password" )}>
                                <Input name="password" type="password" className="fluid"
                                       rules={[
                                           { type: 'empty', prompt: __( 'Please enter your password' ) },
                                           { type: 'minLength[6]', prompt: __( 'Your password should be at least 6 characters long' ) }
                                       ]}/>
                            </Field>
                            <Field label={__( "Password (Again)" )}>
                                <Input name="passwordVerification" type="password" className="fluid"
                                       rules={[
                                           { type: 'match[password]', prompt: __( 'Passwords must match' ) }
                                       ]}/>
                            </Field>

                            <div className={ classNames( "ui huge positive right labeled icon fluid submit button", { loading: this.state.registering, disabled: this.state.registering } ) }>
                                {__( 'Register' )}
                                <i className="checkmark icon"/>
                            </div>
                        </Form>
                    </div>
                </div>
            </section>
        );
    }
}

"use strict";

import React, { Component, PropTypes } from "react";
import { Link } from 'react-router';
import classNames from "classnames";

import GroupMethods from "/imports/groups/GroupMethods";

import Form from '../../../components/forms/Form';
import Field from '../../../components/forms/Field';
import Input from '../../../components/forms/Input';

export default class Congratulations extends Component {

    constructor( props ) {
        super( props );

        this.validateInvitation = this.validateInvitation.bind( this );

        this.state = {
            validating: false,
            validated:  false,
            error:      null
        }
    }

    validateInvitation( values ) {
        if ( this.state.validating ) {
            return;
        }
        this.setState( { validating: true } );

        GroupMethods.validateInvitationCode.call( values, ( err, result ) => {
            this.setState( {
                validating: false,
                validated:  result,
                error:      err
            } );
        } );
    }

    render() {
        const { validating, validated, error } = this.state;

        return (
            <section id="congratulations">
                <header>
                    <h2 className="ui center aligned icon header">
                        <i className="teal birthday icon"/>
                        <div className="content">
                            {__( 'Congratulations' )}
                            <div className="sub header">{__( 'You now have access to PlatoReso.' )}</div>
                        </div>
                    </h2>
                </header>

                <div className="ui grid">
                    <div className="sixteen wide centered column">

                        <div className="ui segment">
                            <Form id="invitation-form" ref="invitationForm" error={error} onSuccess={this.validateInvitation}>
                                <h3 className="ui dividing header">
                                    {__( 'Invitation Code' )}
                                    <div className="sub header">{__( 'If you have an invitation code, enter it now to validate it.' )}</div>
                                </h3>
                                <Field>
                                    <Input ref="invitationCode" name="invitationCode" type="text"
                                           rules={[
                                               { type: 'regExp[/^[a-zA-Z0-9_-]*$/]', prompt: __( 'Only alphanumeric characters, underscores and dashes are allowed' ) }
                                           ]}/>
                                </Field>

                                <div className={ classNames( "ui right labeled icon fluid submit button", { loading: validating, disabled: validating } ) }>
                                    {__( 'Validate' )}
                                    <i className="checkmark right icon"/>
                                </div>
                            </Form>
                            { validated && <div className="ui positive message">
                                <div className="header">
                                    Invitation Code Validated!
                                </div>
                            </div> }
                        </div>

                        <div className="ui hidden divider"></div>

                        <Link to="/dashboard" className="ui huge positive right labeled icon fluid button">
                            {__( 'Continue' )}
                            <i className="chevron right icon"/>
                        </Link>

                    </div>
                </div>

            </section>
        );
    }

}
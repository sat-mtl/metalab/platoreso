"use strict";

import UserMethods from "/imports/accounts/UserMethods";
import { unflatten } from "flat";
import React, { Component, PropTypes } from "react";
import classNames from "classnames";
import md from "/imports/utils/markdown";
import i18n, { language } from "/imports/i18n/i18n";
import I18nMethods from "/imports/i18n/I18nMethods";
import Form from "../../../components/forms/Form";
import Field from "../../../components/forms/Field";
import TextArea from "../../../components/forms/Textarea";
import CheckBox from "../../../components/forms/Checkbox";

export default class TermsOfService extends Component {

    constructor( props ) {
        super( props );

        this.acceptTerms = this.acceptTerms.bind( this );

        this.state = {
            tos:      null,
            privacy:  null,
            updating: false,
            error:    null
        }
    }

    componentWillMount() {
        I18nMethods.getDocument.call( { name: "tos", lang: language.get() }, ( err, doc ) => {
            if ( err ) {
                this.setState( { error: err } );
            } else {
                this.setState( { tos: doc } );
            }
        } );
        I18nMethods.getDocument.call( { name: "privacy-policy", lang: language.get() }, ( err, doc ) => {
            if ( err ) {
                this.setState( { error: err } );
            } else {
                this.setState( { privacy: doc } );
            }
        } );
    }

    acceptTerms( values ) {
        if ( !this.state.tos || this.state.updating ) {
            return;
        }

        this.setState( { updating: true } );

        const contact = this.refs.contact.getValue();

        UserMethods.acceptTerms( contact, err => {
            if ( err ) {
                this.setState( { error: err } );
            }
            this.setState( { updating: false } );
        } );
    }

    render() {
        const { updating, error, tos, privacy } = this.state;

        return (
            <Form id="terms-form" ref="termsForm" error={error} onSuccess={this.acceptTerms}>

                <div className="ui stackable padded grid">

                    <div className="aligned sixteen wide column">
                        <h3>{__( 'TOS.terms.title' )}</h3>
                        <div className="ui tall stacked segment tos">
                            <div dangerouslySetInnerHTML={{ __html: tos ? md( tos ) : '' }}/>
                        </div>
                        <div className="ui tall stacked segment privacy">
                            <div dangerouslySetInnerHTML={{ __html: privacy ? md( privacy ) : '' }}/>
                        </div>
                        <div className="ui hidden divider"/>
                        <Field className={classNames( { disabled: tos == null || privacy == null } )}>
                            <CheckBox
                                ref="agree"
                                name="agree"
                                defaultChecked={false}
                                label={__( 'TOS.terms.description' )}
                                rules={[
                                    { type: 'checked', prompt: __( 'You must agree to the terms to continue' ) }
                                ]}
                            />
                        </Field>

                        <h3>{__( 'TOS.contact.title' )}</h3>
                        <Field>
                            <CheckBox
                                ref="contact"
                                name="contact"
                                defaultChecked={false}
                                label={__( 'TOS.contact.description' )}
                            />
                        </Field>
                    </div>

                    <div className="right aligned sixteen wide column">

                        <div className={ classNames( "ui huge positive right labeled icon submit button", {
                            loading:  updating,
                            disabled: updating || tos == null || privacy == null
                        } ) }>
                            {__( 'Continue' )}
                            <i className="chevron right icon"/>
                        </div>

                    </div>
                </div>
            </Form>
        );
    }

}
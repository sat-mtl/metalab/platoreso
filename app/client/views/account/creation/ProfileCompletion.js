"use strict";

import AvatarCollection from "/imports/accounts/AvatarCollection";

import UserMethods from "/imports/accounts/UserMethods";
import { unflatten } from 'flat';
import formDiff from "/imports/utils/formDiff";
import React, { Component, PropTypes } from "react";
import classNames from "classnames";
import { ConnectMeteorData } from '../../../lib/ReactMeteorData';

import Form from '../../../components/forms/Form';
import Field from '../../../components/forms/Field';
import Input from '../../../components/forms/Input';
import TextArea from '../../../components/forms/Textarea';
import Select from '../../../components/forms/Select';
import ImageUploader from '../../../components/forms/ImageUploader';

class ProfileCompletion extends Component {

    constructor( props ) {
        super( props );

        this.updateProfile = this.updateProfile.bind( this );

        this.state = {
            updating: false,
            error:    null
        }
    }

    updateProfile( values ) {
        if ( this.state.updating ) {
            return;
        }

        this.setState( { updating: true } );

        const changes = formDiff( unflatten( values ), this.props.user, ['_id'] );
        UserMethods.update( changes, err => {
            if ( err ) {
                this.setState( { error: err } );
            }
            this.setState( { updating: false } );
        } );
    }

    render() {
        const { user, avatar }    = this.props;
        const { updating, error } = this.state;

        return (
            <Form id="profile-form" ref="profileForm" error={error} onSuccess={this.updateProfile}>
                <input type="hidden" name="_id" value={user._id}/>

                <div className="ui stackable padded grid">

                    <div className="ten wide column">

                        <h3 className="ui dividing header">{__( 'Personal Information' )}</h3>
                        <Field label={__( "First Name" )} className="required">
                            <Input ref="profile.firstName" name="profile.firstName" type="text"
                                   defaultValue={user.profile.firstName}
                                   rules={[
                                       { type: 'empty', prompt: __( 'Please enter your first name' ) }
                                   ]}/>
                        </Field>
                        <Field label={__( "Last Name" )} className="required">
                            <Input ref="profile.lastName" name="profile.lastName" type="text"
                                   defaultValue={user.profile.lastName}
                                   rules={[
                                       { type: 'empty', prompt: __( 'Please enter your last name' ) }
                                   ]}/>
                        </Field>

                    </div>

                    <div className="six wide column">

                        <h3 className="ui dividing header">{__( 'Avatar' )}</h3>
                        <Field name="profilePicture" label={__( "Profile Picture" )}>
                            <ImageUploader collection={AvatarCollection} file={avatar} owner={user._id}/>
                        </Field>

                    </div>

                    <div className="sixteen wide column">

                        <h3 className="ui dividing header">{__( 'Profile' )}</h3>
                        <Field name="profile.biography" label={__( "Biography" )}>
                            <TextArea ref="profile.biography" name="profile.biography" defaultValue={user.profile.biography}/>
                        </Field>

                    </div>

                    <div className="right aligned sixteen wide column">

                        <div className={ classNames( "ui huge positive right labeled icon submit button", { loading: updating, disabled: updating } ) }>
                            {__( 'Continue' )}
                            <i className="chevron right icon"/>
                        </div>

                    </div>
                </div>
            </Form>
        );
    }

}

/**
 * Meteor Data Wrapper
 */
const ProfileCompletionComponent = ConnectMeteorData( props => ({
    userLoading:   !Meteor.subscribe( 'user/edit' ).ready(),
    user:          Meteor.user(),
    avatarLoading: !Meteor.subscribe( 'avatar/edit' ).ready(),
    avatar:        AvatarCollection.findOne( { owners: Meteor.userId() } )
}) )( ProfileCompletion );

export default ProfileCompletionComponent;
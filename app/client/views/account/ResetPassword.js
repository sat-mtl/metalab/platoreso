"use strict";

import React, { Component, PropTypes } from "react";
import classNames from "classnames";
import AccountHelpers from "/imports/accounts/AccountHelpers";
import ContainedPage from '../layouts/ContainedPage';
import Form from '../../components/forms/Form';
import Field from '../../components/forms/Field';
import Input from '../../components/forms/Input';

export default class ResetPassword extends Component {

    constructor( props ) {
        super( props );

        this.reset = this.reset.bind( this );

        this.state = {
            resetting: false,
            error:     null
        }
    }

    componentDidMount() {
        if ( !this.props.params.token ) {
            this.context.router.replace( { pathname: '/register' } );
        }
    }

    reset( values ) {
        const {token} = this.props.params;

        if ( token ) {
            if ( this.state.resetting ) {
                return;
            }

            this.setState( { resetting: true } );

            AccountHelpers.resetPassword( token, values.password, err => {
                if ( err ) {
                    this.setState( { error: err, resetting: false } )
                } else {
                    this.context.router.replace( { pathname: '/dashboard' } );
                }
            } );
        }
    }

    render() {
        return (
            <ContainedPage>
                <div className="row">
                    <header className="column">
                        <h2 className="ui center aligned header">
                            {__('Reset password')}
                            <div className="sub header">
                                {__('Reset your PlatoReso password.')}
                            </div>
                        </h2>
                    </header>
                </div>
                <div className="row">
                    <div className="center aligned column">{__('Welcome to PlatoReso. Before continuing you will need to reset your password.')}</div>
                </div>
                <div className="row">
                    <div className="ui column centered grid">
                        <div className="twelve wide mobile six wide column">
                            <Form id="password-reset-form" ref="passwordResetForm"
                                  error={this.state.error}
                                  onSuccess={this.reset}>

                                <Field label={__("Password")}>
                                    <Input name="password" type="password" className="fluid"
                                           rules={[
                                               { type: 'empty', prompt: __('Please enter your password')},
                                               {type: 'minLength[6]', prompt: __('Your password should be at least 6 characters long')}
                                           ]}/>
                                </Field>
                                <Field label={__("Password (Again)")}>
                                    <Input name="passwordVerification" type="password" className="fluid"
                                           rules={[
                                               { type: 'match[password]', prompt: __('Passwords must match') }
                                           ]}/>
                                </Field>

                                <div className={ classNames("ui huge positive right labeled icon fluid submit button", { loading: this.state.resetting, disabled: this.state.resetting } ) }>
                                    {__('Reset password')}
                                    <i className="checkmark icon"/>
                                </div>
                            </Form>
                        </div>
                    </div>
                </div>
            </ContainedPage>
        );
    }
}

ResetPassword.contextTypes = {
    router: PropTypes.object.isRequired
};
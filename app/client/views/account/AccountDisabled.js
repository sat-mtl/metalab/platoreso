"use strict";

import React, {Component, PropTypes} from "react";

export default props => (
    <div className="error-page forbidden">
        <h2 className="ui inverted icon header">
            <i className="protect icon"/>
            <div className="content">
                {__( 'Account Disabled' )}
                <div className="sub header">{__( 'Your account was disabled.' )}</div>
            </div>
        </h2>
    </div>
);
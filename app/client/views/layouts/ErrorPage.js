"use strict";

import React, {Component, PropTypes} from "react";
import Footer from "./Footer";

export default props => (
    <div className="error page-layout">
        {props.children}
        <Footer/>
    </div>
);

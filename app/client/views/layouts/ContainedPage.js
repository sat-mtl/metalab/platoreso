"use strict";

import React, {Component, PropTypes} from "react";
import Footer from "./Footer";

export default props => (
    <div className="page-layout">
        <section id={props.id} className="ui container">
            <div className="ui stackable padded grid page-content">
                {props.children}
            </div>
        </section>
        <Footer/>
    </div>
);

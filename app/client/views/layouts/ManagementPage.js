"use strict";

import React, {Component, PropTypes} from "react";
import Footer from "./Footer";

export default props => (
    <div className="management page-layout">
        {props.menu}
        <section id={props.id}>{/* className="ui container"*/}
            <div className="ui stackable padded grid page-content">
                {props.children}
            </div>
        </section>
        <Footer/>
    </div>
);

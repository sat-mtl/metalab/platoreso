"use strict";

import React, {Component, PropTypes} from "react";
import {Link} from"react-router";

export default props => (
    <footer id="footer">
        <div className="ui center aligned container copyrights">
            {__( 'PlatoReso version __version__', {version: version} )}&nbsp;
            {__( 'Developed by the Métalab. ©2015-2016 Société des arts technologiques, All rights reserved.' )}
            <div className="sat logo"></div>
        </div>
        <div className="ui center aligned container links">
            <div className="link"><Link to="/legal">{__('Terms of use')}</Link></div>
            <div className="link"><Link to="/legal/privacy-policy">{__('Privacy policy')}</Link></div>
        </div>
    </footer>
);

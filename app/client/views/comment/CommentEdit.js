"use strict";

import React, {Component, PropTypes} from "react";
import ContainedPage from '../layouts/ContainedPage';
import Spinner from '../widgets/Spinner';
import NotFound from '../errors/NotFound';
import {CommentEditor} from './CommentEditor';
import CommentEditorDataWrapper from './CommentEditorDataWrapper';

export const CommentEdit = ( props ) => {
    if ( props.loading ) {
        return <Spinner/>;

    } else if ( !props.comment ) {
        return <NotFound/>;

    } else {
        return (
            <ContainedPage>
                <CommentEditor className="standalone sixteen wide column" {...props}/>
            </ContainedPage>
        );
    }
};

const DataWrapper = CommentEditorDataWrapper( CommentEdit );
export default ( { params } ) => <DataWrapper commentId={params.id}/>;
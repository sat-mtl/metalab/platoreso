"use strict";

import CommentCollection from "/imports/comments/CommentCollection";
import { ConnectMeteorData } from '../../lib/ReactMeteorData'

/**
 * Meteor Data Wrapper
 */
export default ConnectMeteorData( props => {
    const commentLoading = !!( props.commentId && !Meteor.subscribe( 'comment', props.commentId ).ready() );
    const comment        = props.commentId ? CommentCollection.findOne( { _id: props.commentId } ) : null;
    return {
        loading: commentLoading,
        comment: comment
    }
} );
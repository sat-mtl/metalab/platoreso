"use strict";

import CommentCollection from "/imports/comments/CommentCollection";
import CommentMethods from "/imports/comments/CommentMethods";

import React, { Component, PropTypes } from "react";
import { ConnectMeteorData } from '../../lib/ReactMeteorData';
import Form from '../../components/forms/Form';
import Field from '../../components/forms/Field';
import TextArea from '../../components/forms/Textarea';
import Comment from './comments/Comment';
import CommentForm from './comments/CommentForm';
import Modal from '../../components/semanticui/modules/Modal';

class Comments extends Component {

    constructor( props ) {
        super( props );
        this.confirmRemove     = this.confirmRemove.bind( this );
        this.cancelRemove      = this.cancelRemove.bind( this );
        this.remove            = this.remove.bind( this );
        this.renderRemoveModal = this.renderRemoveModal.bind( this );

        this.state = {
            selectedComment:          null,
            confirmingCommentRemoval: false
        };
    }

    confirmRemove( comment ) {
        this.setState( {
            confirmingCommentRemoval: true,
            selectedComment:          comment
        } );
    }

    cancelRemove() {
        this.setState( {
            confirmingCommentRemoval: false
        } );
    }

    remove() {
        this.setState( {
            confirmingCommentRemoval: false
        } );

        const { selectedComment } = this.state;
        if ( !selectedComment ) {
            return;
        }

        CommentMethods.remove.call( { commentId: selectedComment._id } );

        this.setState( {
            selectedComment: null
        } );
    }

    renderRemoveModal( selectedComment ) {
        return (
            <div className="content">
                <div className="description">
                    <p>{__( 'Are you sure you want to remove the selected comment?' )}</p>
                </div>
            </div>
        )
    };

    render() {
        const { objects, comments, canContribute } = this.props;
        const { confirmingCommentRemoval, selectedComment }   = this.state;

        return (
            <div className="ui threaded comments">
                <h3 className="ui dividing sub header">{__( 'Comments' )}</h3>

                {/* COMMENT THREAD */}
                { comments
                    .filter( comment => comment.parent == null )
                    .map( comment => (
                        <Comment key={comment._id}
                                 objects={objects}
                                 canContribute={canContribute}
                                 comments={comments}
                                 comment={comment}
                                 onRemove={this.confirmRemove}/>
                    ) ) }

                {/* COMMENT FORM */}
                { canContribute &&
                  <CommentForm objects={objects} callToAction={__( 'Add Comment' )}/> }

                {/* COMMENT REMOVAL MODAL */}
                <Modal
                    opened={ confirmingCommentRemoval }
                    onApprove={ this.remove }
                    onHide={ this.cancelRemove }>
                    <i className="close icon"/>
                    <div className="header">
                        { __( 'Confirm Removal' ) }
                    </div>
                    { selectedComment ? this.renderRemoveModal( selectedComment ) : null }
                    <div className="actions">
                        <div className="ui buttons">
                            <div className="ui red deny button">
                                <i className="remove icon"/>
                                {__( 'No' )}
                            </div>
                            <div className="ui green approve button">
                                <i className="checkmark icon"/>
                                {__( 'Yes' )}
                            </div>
                        </div>
                    </div>
                </Modal>
            </div>
        );
    }
}

Comments.propTypes = {
    objects:       PropTypes.arrayOf( PropTypes.shape( {
        id:   PropTypes.string.isRequired,
        type: PropTypes.string.isRequired
    } ) ).isRequired,
    maxDate:       PropTypes.any,
    canContribute: PropTypes.bool.isRequired
};

Comments.defaultProps = {
    maxDate: null
};

export default ConnectMeteorData( ( { objects, maxDate } ) => {
    const entityType = objects[0].type;
    const entityId   = objects[0].id;
    const query      = {
        'objects.0.type': entityType,
        'objects.0.id':   entityId
    };
    if ( maxDate ) {
        query.createdAt = { $lte: maxDate };
    }
    return {
        commentsLoading: !Meteor.subscribe(
            entityType + '/comments',
            entityId,
            maxDate ? { maxDate } : null )
                                .ready(),
        comments:        CommentCollection.find( query ).fetch()
    };
} )( Comments );
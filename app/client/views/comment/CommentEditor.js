"use strict";

// NPM
import React, { Component, PropTypes } from "react";
import ReactDOM from "react-dom";
import classNames from "classnames";
import CommentMethods from "/imports/comments/CommentMethods";
import CommentEditorDataWrapper from "./CommentEditorDataWrapper";
import Form from '../../components/forms/Form';
import Field from '../../components/forms/Field';
import TextArea from '../../components/forms/Textarea';
import Modal from "../../components/semanticui/modules/Modal";

const { File } = FS;

export class CommentEditor extends Component {

    constructor( props ) {
        super( props );

        this.submitForm    = this.submitForm.bind( this );
        this.updateComment = this.updateComment.bind( this );
        this.onFinish      = this.onFinish.bind( this );
        this.confirmRemove = this.confirmRemove.bind( this );
        this.cancelRemove  = this.cancelRemove.bind( this );
        this.remove        = this.remove.bind( this );

        // Initial state
        this.state = {
            error:             null,
            saving:            false,
            confirmingRemoval: false,
        };
    }

    submitForm() {
        $( ReactDOM.findDOMNode( this.refs.commentEditForm ) ).form( 'submit' );
    }

    updateComment( values ) {
        if ( this.state.saving ) {
            return;
        }
        this.setState( { saving: true } );

        const { comment } = this.props;

        // Call update if we have changes or uploads pending
        CommentMethods.update.call( {
            commentId: comment._id,
            post:      values.post
        }, err => {
            this.setState( { saving: false } );
            if ( err ) {
                this.setState( { error: err } );
            } else {
                this.onFinish();
            }
        } );
    }

    confirmRemove() {
        this.setState( {
            confirmingRemoval: true
        } );
    }

    cancelRemove() {
        this.setState( {
            confirmingRemoval: false
        } );
    }

    remove() {
        this.setState( {
            confirmingRemoval: false
        } );

        CommentMethods.remove.call( { commentId: this.props.comment._id }, ( err ) => {
            if ( err ) {
                this.setState( { error: err } );
            } else {
                this.onFinish();
            }
        } );
    }

    onFinish() {
        if ( this.props.onFinished ) {
            this.props.onFinished();
        } else {
            this.context.router.goBack();
        }
    }

    renderLoading() {
        return (
            <div className="comment details loading">
                <div className="ui large inverted active indeterminate text loader">
                    {__( 'Loading Comment' )}
                </div>
            </div>
        );
    }

    renderComment() {
        const { comment }                          = this.props;
        const { error, saving, confirmingRemoval } = this.state;

        if ( !comment ) {
            return null;
        }

        return (
            <div className={classNames( this.props.className, "editor editComment" )}>

                <div className="header">
                    {__( 'Edit Comment' )}
                </div>

                <Form id={"comment-edit-form-" + comment._id}
                      ref="commentEditForm"
                      key={comment._id}
                      error={error}
                      onSuccess={this.updateComment}>

                    <Field>
                        <TextArea ref="post" name="post"
                                  defaultValue={ comment.post }
                                  rows={3}
                                  autosize={true}
                                  rules={[
                                      { type: 'empty', prompt: __( 'Please enter a comment' ) }
                                  ]}/>
                    </Field>

                </Form>

                <div className="actions">
                    <div className="ui red button" onClick={this.confirmRemove}>
                        <i className="trash icon"/>
                        {__( 'Remove' )}
                    </div>
                    <div className="ui buttons">
                        <div className="ui cancel button" onClick={this.onFinish}>
                            <i className="remove icon"/>
                            {__( 'Cancel' )}
                        </div>
                        <div className={classNames( "ui green button", { loading: saving, disabled: saving } )}
                             onClick={this.submitForm}>
                            <i className="plus icon"/>
                            {__( 'Save' )}
                        </div>
                    </div>
                </div>

                <Modal
                    opened={ confirmingRemoval }
                    onApprove={ this.remove }
                    onHide={ this.cancelRemove }>
                    <i className="close icon"/>
                    <div className="header">
                        { __( 'Confirm Removal' ) }
                    </div>
                    <div className="content">
                        <div className="description">
                            <p>{__( 'Are you sure you want to remove the comment? All replies of the comment will be permanently deleted.' )}</p>
                        </div>
                    </div>
                    <div className="actions">
                        <div className="ui buttons">
                            <div className="ui red deny button">
                                <i className="remove icon"/>
                                {__( 'No' )}
                            </div>
                            <div className="ui green approve button">
                                <i className="checkmark icon"/>
                                {__( 'Yes' )}
                            </div>
                        </div>
                    </div>
                </Modal>

            </div>
        );
    }

    render() {
        const { loading } = this.props;
        return loading ? this.renderLoading() : this.renderComment();
    }
}

CommentEditor.propTypes = {
    loading:    PropTypes.bool.isRequired,
    comment:    PropTypes.object.isRequired,
    onFinished: PropTypes.func
};

CommentEditor.contextTypes = {
    router: PropTypes.object.isRequired
};

export default CommentEditorDataWrapper( CommentEditor );

"use strict";

import React, { Component, PropTypes } from "react";
import classNames from "classnames";
import moment from "moment";
import { Link } from "react-router";
import UserHelpers from "/imports/accounts/UserHelpers";
import UserCollection from "/imports/accounts/UserCollection";
import ModerationMethods from "/imports/moderation/ModerationMethods";
import CommentCollection from "/imports/comments/CommentCollection";
import CommentMethods from "/imports/comments/CommentMethods";
import CommentSecurity from "/imports/comments/CommentSecurity";
import { ConnectMeteorData } from "../../../lib/ReactMeteorData";
import Avatar from "../../widgets/Avatar";
import CommentForm from "./CommentForm";

export class Comment extends Component {

    constructor( props ) {
        super( props );

        this.toggleEdit  = this.toggleEdit.bind( this );
        this.toggleReply = this.toggleReply.bind( this );
        this.like        = this.like.bind( this );
        this.report      = this.report.bind( this );
        this.remove      = this.remove.bind( this );
        this.onReplied   = this.onReplied.bind( this );
        this.onEdited    = this.onEdited.bind( this );

        this.renderActions = this.renderActions.bind( this );
        this.renderComment = this.renderComment.bind( this );

        this.state = {
            showEdit:  false,
            showReply: false
        }
    }

    toggleEdit() {
        this.setState( { showEdit: !this.state.showEdit } );
    }

    toggleReply() {
        this.setState( { showReply: !this.state.showReply } );
    }

    like() {
        const { comment } = this.props;
        if ( !comment ) {
            return;
        }
        CommentMethods.like.call( { commentId: comment._id } );
    }

    report() {
        const { comment } = this.props;
        if ( !comment ) {
            return;
        }
        ModerationMethods.report.call( { entityTypeName: CommentCollection.entityName, entityId: comment._id } );
    }

    remove() {
        this.props.onRemove( this.props.comment );
    }

    onEdited() {
        this.setState( {
            showEdit: false
        } );
    }

    onReplied() {
        this.setState( {
            showReply: false
        } );
    }

    renderActions() {
        const { comment, canContribute } = this.props;
        const canEdit                    = canContribute && CommentSecurity.canEdit( comment, Meteor.userId() );
        const canReply                   = canContribute;
        const youReported                = comment.isReportedBy( Meteor.userId() );

        return (
            <div className="actions">
                { canContribute &&
                  <a className="like" onClick={this.like}>
                      <i className={classNames( 'heart icon', { outline: (comment.likeCount || 0) == 0, red: comment.likes } )}/>
                      {__( 'like.count', { count: comment.likeCount || 0 } ) }
                  </a> }
                { canEdit && <a className="edit" onClick={this.toggleEdit}>{__( 'Edit' )}</a> }
                { canEdit && <a className="remove" onClick={this.remove}>{__( 'Remove' )}</a> }
                { canReply && <a className="reply" onClick={this.toggleReply}>{__( 'Reply' )}</a> }
                <a className={classNames( "report", { reported: youReported } )} onClick={this.report}>{ youReported ? __( 'You reported this' ) : __( 'Report' ) }</a>
            </div>
        );
    }

    renderComment() {
        const { comment } = this.props;
        if ( comment.moderated ) {
            return ( <div className="ui mini compact message">{__( 'Comment moderated' )}</div> );
        } else {
            return <div dangerouslySetInnerHTML={{ __html: comment.postMarkdown }}/>;
        }
    }

    render() {
        const { objects, comments, comment, canContribute, onRemove, author }            = this.props;
        const { showEdit, showReply }                                                    = this.state;
        const replies                                                                    = comments ? comments.filter( c => c.parent == comment._id ) : [];

        return (
            <div className="comment">
                <Avatar user={author}/>
                <div className="content">
                <span className="author">
                    { author ? (
                        <Link to={'/profile/' + author._id}>
                            {UserHelpers.getDisplayName( author )}
                        </Link>
                    ) : <a className="author">{ __( 'Author not found!' ) }</a> }
                    { author && <span className="points">
                        {author.points}&nbsp;<i className="trophy icon"/>
                    </span> }
                    </span>
                    <div className="metadata">
                        <div className="date">{comment.createdAt ? moment( comment.createdAt ).fromNow() : ''}</div>
                    </div>
                    <div className="text">
                        { showEdit ?
                          <CommentForm objects={objects} comment={comment} callToAction={__( 'Save' )} onComment={this.onEdited}/>
                            : this.renderComment()
                        }
                    </div>
                    { this.renderActions() }
                    { showReply &&
                      <CommentForm objects={objects} parent={comment} callToAction={__( 'Add Reply' )} onComment={this.onReplied}/> }
                </div>
                { replies.length > 0 && <div className="comments">
                    { replies.map( c => (
                        <WrappedComment key={c._id}
                                        objects={objects}
                                        comments={comments}
                                        comment={c}
                                        canContribute={canContribute}
                                        onRemove={onRemove}/>
                    ) ) }
                </div> }
            </div>
        );
    }
}

Comment.propTypes = {
    objects:       PropTypes.arrayOf( PropTypes.shape( {
        id:   PropTypes.string.isRequired,
        type: PropTypes.string.isRequired
    } ) ).isRequired,
    comments:      PropTypes.array.isRequired,
    comment:       PropTypes.object.isRequired,
    canContribute: PropTypes.bool.isRequired,
    onRemove:      PropTypes.func.isRequired,
    author:        PropTypes.object
};

const WrappedComment = ConnectMeteorData( ( { comment } ) => ({
    author: comment ? UserCollection.findOne( { _id: comment.author } ) : null
} ) )( Comment );

export default WrappedComment;
"use strict";

import React, { Component, PropTypes } from "react";
import ReactDOM from "react-dom";
import flatten from "flat";
import classNames from "classnames";
import CommentMethods from "/imports/comments/CommentMethods";
import Form from '../../../components/forms/Form';
import Field from '../../../components/forms/Field';
import TextArea from '../../../components/forms/Textarea';

export default class CommentForm extends Component {

    constructor( props ) {
        super( props );

        this.save = this.save.bind( this );

        this.state = {
            error:  null,
            saving: false
        };
    }

    save( values ) {
        const { objects, parent, comment } = this.props;

        if ( this.state.saving ) {
            return;
        }

        this.setState( { saving: true } );

        const request = values;
        if ( comment ) {
            request.commentId = comment._id;
        } else {
            request.objects = objects;
            if ( parent ) {
                request.parent = parent._id;
            }
        }
        const method = comment ? CommentMethods.update : CommentMethods.add;

        method.call( request, err => {
            if ( err ) {
                this.setState( { error: err, saving: false } );
            } else {
                this.setState( { saving: false } );
                $( ReactDOM.findDOMNode( this.refs.commentForm ) ).form( 'set value', 'post', '' );
                if ( this.props.onComment ) {
                    this.props.onComment();
                }
            }
        } );
    }

    render() {
        const { comment, callToAction }                             = this.props;
        const { saving, error }                                     = this.state;

        return (
            <Form ref="commentForm"
                  error={error}
                  onSuccess={this.save}
                  className={classNames( 'comment', comment ? "edit" : "reply", { loading: saving } )}>
                <Field>
                    <TextArea ref="post" name="post"
                              defaultValue={ comment ? comment.post : null }
                              managed={false}
                              rows={3}
                              autosize={true}
                              rules={[
                                  { type: 'empty', prompt: __( 'Please enter a comment' ) }
                              ]}/>
                </Field>
                <div className="ui mini primary submit labeled icon button">
                    <i className="icon edit"/>{ callToAction || __( 'Comment' ) }
                </div>
            </Form>
        );
    }
}

CommentForm.propTypes = {
    objects:      PropTypes.arrayOf( PropTypes.shape( {
        id:   PropTypes.string.isRequired,
        type: PropTypes.string.isRequired
    } ) ).isRequired,
    comment:      PropTypes.object,
    parent:       PropTypes.object,
    callToAction: PropTypes.string,
    onComment:    PropTypes.func
};
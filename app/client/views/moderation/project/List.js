"use strict";

import ReportedProjectList from './list/ReportedProjectList';
import React, { Component, PropTypes } from "react";

export default class Projects extends Component {

    render() {
        return (
            <section id="moderation-project-list" className="sixteen wide column moderation-project-list">
                <header className="ui clearing basic segment">
                    <h2 className="ui left floated header">
                        {__( 'Reported Projects' )}
                        <div className="sub header">{__( 'Manage projects that were reported by users' )}</div>
                    </h2>
                </header>
                <ReportedProjectList/>
                <header className="ui clearing basic segment">
                    <h2 className="ui left floated header">
                        {__( 'Moderated Projects' )}
                        <div className="sub header">{__( 'Manage projects that were moderated' )}</div>
                    </h2>
                </header>
                <ReportedProjectList moderated={true}/>
            </section>
        );
    }
}
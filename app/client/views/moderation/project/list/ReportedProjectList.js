"use strict";

import React, { Component, PropTypes } from "react";
import slugify from "underscore.string/slugify";
import escapeStringRegexp from "escape-string-regexp";
import { Counts } from "meteor/tmeasday:publish-counts";
import ProjectCollection from "/imports/projects/ProjectCollection";
import ProjectMethods from "/imports/projects/ProjectMethods";
import { ConnectMeteorData } from "../../../../lib/ReactMeteorData";
import BaseList, { BaseTable } from "../../../common/table/BaseList";
import ReportedProjectRow from "./ReportedProjectRow";

export default class ReportedProjectList extends BaseList {

    constructor( props ) {
        super( props );
        this.tableComponent = ReportedProjectTable;
        this.state          = _.extend( this.state, {
            orderBy: 'reportCount',
            order:   -1
        } );
    }

    remove( project ) {
        ProjectMethods.remove.call( { projectId: project._id }, ( err ) => {
            if ( err ) {
                this.setState( { error: err } );
            }
        } );
    }

    getHeadersFromProps( props ) {
        let headers = super.getHeadersFromProps( props );
        headers.push( { field: 'name_sort', label: __( 'Name' ), colSpan: 2 } );
        headers.push( { field: 'reportCount', label: __( 'Report Count' ) } );
        return headers;
    }

    renderRow( project ) {
        const { selectedEntity: selectedProject } = this.state;
        return (
            <ReportedProjectRow key={project._id}
                                entityTypeName={ProjectCollection.entityName}
                                entity={project}
                                selected={selectedProject && selectedProject._id == project._id}
                                onSelect={this.onSelect}
                                onEdit={this.onEdit}
                                onRemove={this.confirmRemove}/>
        );
    }

    renderRemoveModal( project ) {
        return (
            <div className="image content">
                <div className="image">
                    <i className="bullseye icon"/>
                </div>
                <div className="description">
                    <p>{__( 'Are you sure you want to remove the project __project__?', { project: project.name } )}</p>
                </div>
            </div>
        );
    }
}

ReportedProjectList.propTypes = {
    moderated: PropTypes.bool
};

ReportedProjectList.defaultProps = {
    moderated: false
};

const ReportedProjectTable = ConnectMeteorData( ( { moderated, search, options } ) => {
    let query = { moderated: moderated };

    if ( !_.isEmpty( search ) ) {
        query.name = { $regex: escapeStringRegexp( search ), $options: 'i' };
    }

    return {
        loading: !Meteor.subscribe( 'project/moderation/report/list', query, options.subscription ).ready(),
        rows:    ProjectCollection.findReported( query, options.local ).fetch(),
        total:   Counts.get( 'project/moderation/report/count/' + slugify( JSON.stringify( query ) ) )
    };
} )( BaseTable );
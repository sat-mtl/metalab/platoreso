"use strict";

import React, {Component, PropTypes} from "react";
import {Link} from "react-router";
import classNames from "classnames";
import ProjectImagesCollection from "/imports/projects/ProjectImagesCollection";
import {ConnectMeteorData} from '../../../../lib/ReactMeteorData';
import ModerationBaseRow from "../../common/ModerationBaseRow";
import Image from "../../../widgets/Image";

export class ReportedProjectRow extends ModerationBaseRow {

    shouldComponentUpdate( nextProps, nextState ) {
        return super.shouldComponentUpdate( nextProps, nextState ) && ( !_.isEqual( nextProps, this.props ) || !_.isEqual( nextState, this.state ) );
    }

    render() {
        const {
                  entity: project,
                  projectImage,
                  selected
              }                  = this.props;
        const { hasBeenVisible } = this.state;

        return (
            <tr className={ classNames( { 'active': selected } ) }
                onClick={this.select}
                onDoubleClick={this.edit}>
                <td className="collapsing">
                    <Image image={ hasBeenVisible ? projectImage : null }
                           size="thumb"
                           className="circular idea avatar"/>
                </td>
                <td><Link to={`/project/${project.uri}`}>{project.name}</Link></td>
                <td className="collapsing">
                    { project.reportCount }
                </td>
                <td className="right aligned collapsing">
                    <div ref="actions" className="ui mini buttons">
                        <Link className="ui compact icon button"
                              data-content={__( 'Project Page' )}
                              to={`/project/${project.uri}`}>
                            <i className="idea icon"/>
                        </Link>
                        <button className="ui compact olive icon button"
                                onClick={this.approve}
                                data-content={__( 'Approve' )}>
                            <i className="thumbs up icon"/>
                        </button>
                        <Link className="ui compact yellow icon button"
                              data-content={__( 'Edit' )}
                              to={`/project/${project.uri}/edit`}>
                            <i className="write icon"/>
                        </Link>
                        <button className="ui compact orange icon button"
                                onClick={this.moderate}
                                data-content={__( 'Moderate' )}>
                            <i className="legal icon"/>
                        </button>
                        <button className="ui compact red icon button"
                                onClick={this.remove}
                                data-content={__( 'Remove' )}>
                            <i className="remove icon"/>
                        </button>
                    </div>
                </td>
            </tr>
        );
    }
}

ReportedProjectRow.propTypes = _.defaults( {
    projectImage: PropTypes.object
}, ModerationBaseRow.propTypes );

export default ConnectMeteorData( props => {
    const { entity: project } = props;
    return {
        projectImage: project ? ProjectImagesCollection.findOne( { owners: project._id } ) : null
    }
} )( ReportedProjectRow );
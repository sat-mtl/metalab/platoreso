"use strict";

export default [
    {
        to: '/moderation/',
        get label() { return __('Dashboard'); },
        icon: 'dashboard',
        index: true,
        groupModeratorsAllowed: true,
        enabled: true
    },
    {
        to: '/moderation/user',
        get label() { return __('Reported Users'); },
        icon: 'user',
        groupModeratorsAllowed: false,
        enabled: true
    },
    {
        to: '/moderation/group',
        get label() { return __('Reported Groups'); },
        icon: 'group',
        groupModeratorsAllowed: false, // Group moderators cannot moderate groups, that would be like police watching over itself
        enabled: true
    },
    {
        to: '/moderation/project',
        get label() { return __('Reported Projects'); },
        icon: 'bullseye',
        groupModeratorsAllowed: true,
        enabled: true
    },
    {
        to: '/moderation/card',
        get label() { return __('Reported Cards'); },
        icon: 'idea',
        groupModeratorsAllowed: true,
        enabled: true
    },
    {
        to: '/moderation/comment',
        get label() { return __('Reported Comments'); },
        icon: 'comments',
        groupModeratorsAllowed: true,
        enabled: true
    },
    {
        to: '/moderation/message',
            get label() { return __('Reported Messages'); },
        icon: 'talk',
        groupModeratorsAllowed: true,
        enabled: true
    }
];
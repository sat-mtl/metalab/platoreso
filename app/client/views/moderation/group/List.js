"use strict";

import ReportedGroupList from './list/ReportedGroupList';
import React, { Component, PropTypes } from "react";

export default class Groups extends Component {

    render() {
        return (
            <section id="moderation-group-list" className="sixteen wide column moderation-group-list">
                <header className="ui clearing basic segment">
                    <h2 className="ui left floated header">
                        {__( 'Reported Groups' )}
                        <div className="sub header">{__( 'Manage groups that were reported by users' )}</div>
                    </h2>
                </header>
                <ReportedGroupList/>
                <header className="ui clearing basic segment">
                    <h2 className="ui left floated header">
                        {__( 'Moderated Groups' )}
                        <div className="sub header">{__( 'Manage groups that were moderated' )}</div>
                    </h2>
                </header>
                <ReportedGroupList moderated={true}/>
            </section>
        );
    }
}
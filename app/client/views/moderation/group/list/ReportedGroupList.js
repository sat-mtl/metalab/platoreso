"use strict";

import React, {Component, PropTypes} from "react";
import slugify from "underscore.string/slugify";
import escapeStringRegexp from "escape-string-regexp";
import {Counts} from "meteor/tmeasday:publish-counts";
import GroupCollection from "/imports/groups/GroupCollection";
import GroupMethods from "/imports/groups/GroupMethods";
import {ConnectMeteorData} from "../../../../lib/ReactMeteorData";
import BaseList, {BaseTable} from "../../../common/table/BaseList";
import ReportedGroupRow from "./ReportedGroupRow";

export default class ReportedGroupList extends BaseList {

    constructor( props ) {
        super( props );
        this.tableComponent = ReportedGroupTable;
        this.state          = _.extend( this.state, {
            orderBy: 'reportCount',
            order:   -1
        } );
    }

    remove( group ) {
        GroupMethods.remove.call( { groupId: group._id }, ( err ) => {
            if ( err ) {
                this.setState( { error: err } );
            }
        } );
    }

    getHeadersFromProps( props ) {
        let headers = super.getHeadersFromProps( props );
        headers.push( { field: 'name_sort', label: __( 'Name' ), colSpan: 2 } );
        headers.push( { field: 'reportCount', label: __( 'Report Count' ) } );
        return headers;
    }

    renderRow( group ) {
        const { selectedEntity: selectedGroup } = this.state;

        return (
            <ReportedGroupRow key={group._id}
                              entity={group}
                              entityTypeName={GroupCollection.entityName}
                              selected={selectedGroup && selectedGroup._id == group._id}
                              onSelect={this.onSelect}
                              onEdit={this.onEdit}
                              onRemove={this.confirmRemove}/>
        );
    }

    renderRemoveModal( group ) {
        return (
            <div className="image content">
                <div className="image">
                    <i className="users icon"/>
                </div>
                <div className="description">
                    <p>{__( 'Are you sure you want to remove the group __group__?', { group: group.name } )}</p>
                </div>
            </div>
        );
    }
}

ReportedGroupList.propTypes = {
    moderated: PropTypes.bool
};

ReportedGroupList.defaultProps = {
    moderated: false
};

const ReportedGroupTable = ConnectMeteorData( ( { moderated, search, options } ) => {
    let query = { moderated: moderated };

    if ( !_.isEmpty( search ) ) {
        query.name = { $regex: escapeStringRegexp( search ), $options: 'i' }
    }

    return {
        loading: !Meteor.subscribe( 'group/moderation/report/list', query, options.subscription ).ready(),
        rows:    GroupCollection.findReported( query, options.local ).fetch(),
        total:   Counts.get( 'group/moderation/report/count/' + slugify( JSON.stringify( query ) ) )
    };
} )( BaseTable );
"use strict";

import React, { Component, PropTypes } from "react";
import {Link} from 'react-router';
import classNames from "classnames";
import GroupImagesCollection from "/imports/groups/GroupImagesCollection";
import {ConnectMeteorData} from '../../../../lib/ReactMeteorData';
import ModerationBaseRow from '../../common/ModerationBaseRow';
import Image from '../../../widgets/Image';

export class ReportedGroupRow extends ModerationBaseRow {

    shouldComponentUpdate( nextProps, nextState ) {
        return super.shouldComponentUpdate( nextProps, nextState ) && ( !_.isEqual( nextProps, this.props ) || !_.isEqual( nextState, this.state ) );
    }

    render() {
        const {
                  entity: group,
                  groupImage,
                  selected
                  } = this.props;
        const { hasBeenVisible } = this.state;

        return (
            <tr className={ classNames( { 'active' : selected } ) }
                onClick={this.select}
                onDoubleClick={this.edit}>
                <td className="collapsing">
                    <Image image={ hasBeenVisible ? groupImage : null }
                           size="thumb"
                           className="circular idea avatar"/>
                </td>
                <td><Link to={`/group/${group.uri}`}>{group.name}</Link></td>
                <td className="collapsing">
                    { group.reportCount }
                </td>
                <td className="right aligned collapsing">
                    <div ref="actions" className="ui mini buttons">
                        <Link className="ui compact icon button"
                              data-content={__('Group Page')}
                              to={`/group/${group.uri}`}>
                            <i className="idea icon"/>
                        </Link>
                        <button className="ui compact olive icon button"
                                onClick={this.approve}
                                data-content={__('Approve')}>
                            <i className="thumbs up icon"/>
                        </button>
                        <Link className="ui compact yellow icon button"
                                data-content={__('Edit')}
                                to={`/admin/group/${group._id}`}>
                            <i className="write icon"/>
                        </Link>
                        <button className="ui compact orange icon button"
                                onClick={this.moderate}
                                data-content={__('Moderate')}>
                            <i className="legal icon"/>
                        </button>
                        <button className="ui compact red icon button"
                                onClick={this.remove}
                                data-content={__('Remove')}>
                            <i className="remove icon"/>
                        </button>
                    </div>
                </td>
            </tr>
        );
    }
}

ReportedGroupRow.propTypes = _.defaults( {
    groupImage: PropTypes.object
}, ModerationBaseRow.propTypes );

export default ConnectMeteorData( props => {
    const { entity: group } = props;
    return {
        groupImage: group ? GroupImagesCollection.findOne( { owners: group._id } ) : null
    }
} )( ReportedGroupRow );
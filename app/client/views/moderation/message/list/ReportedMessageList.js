"use strict";

import React, {Component, PropTypes} from "react";
import slugify from "underscore.string/slugify";
import escapeStringRegexp from "escape-string-regexp";
import prune from "underscore.string/prune";
import {Counts} from "meteor/tmeasday:publish-counts";
import MessageCollection from "/imports/messages/MessageCollection";
import MessageMethods from "/imports/messages/MessageMethods";
import UserCollection from "/imports/accounts/UserCollection";
import {ConnectMeteorData} from "../../../../lib/ReactMeteorData";
import BaseList, {BaseTable} from "../../../common/table/BaseList";
import ReportedMessageRow from "./ReportedMessageRow";

export class ReportedMessageList extends BaseList {

    constructor( props ) {
        super( props );
        this.tableComponent = ReportedMessageTable;
        this.state          = _.extend( this.state, {
            orderBy: 'reportCount',
            order:   -1
        } );
    }

    remove( message ) {
        MessageMethods.remove.call( { messageId: message._id }, ( err ) => {
            if ( err ) {
                console.error(err);
                this.setState( { error: err } );
            }
        } );
    }

    getHeadersFromProps( props ) {
        let headers = super.getHeadersFromProps( props );
        headers.push( { field: 'content', label: __( 'Message' ) } );
        headers.push( { field: 'author', label: __( 'Author' ) } );
        headers.push( { field: 'conversation', label: __( 'Conversation' ) } );
        headers.push( { field: 'reportCount', label: __( 'Report Count' ) } );
        return headers;
    }

    renderRow( message ) {
        const { selectedEntity: selectedMessage } = this.state;
        const { users }                           = this.props;

        return (
            <ReportedMessageRow key={message._id}
                                entityTypeName={MessageCollection.entityName}
                                entity={message}
                                author={ users ? _.findWhere( users, { _id: message.author } ) : null }
                                selected={selectedMessage && selectedMessage._id == message._id}
                                onSelect={this.onSelect}
                                onEdit={this.onEdit}
                                onRemove={this.confirmRemove}/>
        );
    }

    renderRemoveModal( message ) {
        return (
            <div className="image content">
                <div className="image">
                    <i className="bullseye icon"/>
                </div>
                <div className="description">
                    <p>{__( 'Are you sure you want to remove the message __message__?', { message: prune( message.content, 32 ) } )}</p>
                </div>
            </div>
        );
    }
}

ReportedMessageList.propTypes = {
    moderated: PropTypes.bool
};

ReportedMessageList.defaultProps = {
    moderated: false
};

export default ConnectMeteorData( props => {
    return {
        users: UserCollection.find( {} ).fetch()
    }
} )( ReportedMessageList );


const ReportedMessageTable = ConnectMeteorData( ( { moderated, search, options } ) => {
    let query = { moderated: moderated };

    if ( !_.isEmpty( search ) ) {
        query.post = { $regex: escapeStringRegexp( search ), $options: 'i' }
    }

    return {
        loading: !Meteor.subscribe( 'message/moderation/report/list', query, options.subscription ).ready(),
        rows:    MessageCollection.findReported( query, options.local ).fetch(),
        total:   Counts.get( 'message/moderation/report/count/' + slugify( JSON.stringify( query ) ) )
    };
} )( BaseTable );
"use strict";

import React, {Component, PropTypes} from "react";
import {Link} from "react-router";
import classNames from "classnames";
import prune from "underscore.string/prune";
import UserHelpers from "/imports/accounts/UserHelpers";
import {ConnectMeteorData} from "../../../../lib/ReactMeteorData";
import ModerationBaseRow from "../../common/ModerationBaseRow";

export class ReportedMessageRow extends ModerationBaseRow {

    shouldComponentUpdate( nextProps, nextState ) {
        return super.shouldComponentUpdate( nextProps, nextState ) && ( !_.isEqual( nextProps, this.props ) || !_.isEqual( nextState, this.state ) );
    }

    render() {
        const {
                  entity: message,
                  author,
                  conversation,
                  selected
              } = this.props;

        return (
            <tr className={ classNames( { 'active': selected } ) }
                onClick={this.select}>{/* onDoubleClick={this.edit}> */}
                <td>{prune( message.content, 64 )}</td>
                <td className={classNames( { error: !author } )}>
                    <Link to={`/profile/${message.author}`}>{author ? UserHelpers.getDisplayName( author ) : message.author}</Link>
                </td>
                <td className="collapsing">{conversation ? conversation.name : message.conversation}</td>
                <td className="collapsing">{message.reportCount}</td>
                <td className="right aligned collapsing">
                    <div ref="actions" className="ui mini buttons">
                        {/*<Link className="ui compact icon button"
                         data-content={__('View')}
                         to={'/message/' + message._id}>
                         <i className="idea icon"/>
                         </Link>*/}
                        <button className="ui compact olive icon button"
                                onClick={this.approve}
                                data-content={__( 'Approve' )}>
                            <i className="thumbs up icon"/>
                        </button>
                        {/*<Link className="ui compact yellow icon button"
                              data-content={__( 'Edit' )}
                              to={`/messages/${message._id}/edit`}>
                            <i className="write icon"/>
                        </Link>*/}
                        <button className="ui compact orange icon button"
                                onClick={this.moderate}
                                data-content={__( 'Moderate' )}>
                            <i className="legal icon"/>
                        </button>
                        <button className="ui compact red icon button"
                                onClick={this.remove}
                                data-content={__( 'Remove' )}>
                            <i className="remove icon"/>
                        </button>
                    </div>
                </td>
            </tr>
        );
    }
}

ReportedMessageRow.propTypes = _.defaults( {
    author:       PropTypes.object,
    conversation: PropTypes.object
}, ModerationBaseRow.propTypes );

export default ConnectMeteorData( ( { entity: message } ) => {
    return {
        author:       message.getAuthor(),
        conversation: message.getConversation()
    }
} )( ReportedMessageRow );

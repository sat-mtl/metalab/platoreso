"use strict";

import ReportedMessageList from './list/ReportedMessageList';
import React, { Component, PropTypes } from "react";

export default class Messages extends Component {

    render() {
        return (
            <section id="moderation-message-list" className="sixteen wide column moderation-message-list">
                <header className="ui clearing basic segment">
                    <h2 className="ui left floated header">
                        {__( 'Reported Messages' )}
                        <div className="sub header">{__( 'Manage messages that were reported by users' )}</div>
                    </h2>
                </header>
                <ReportedMessageList/>
                <header className="ui clearing basic segment">
                    <h2 className="ui left floated header">
                        {__( 'Moderated Messages' )}
                        <div className="sub header">{__( 'Manage messages that were moderated' )}</div>
                    </h2>
                </header>
                <ReportedMessageList moderated={true}/>
            </section>
        );
    }
}
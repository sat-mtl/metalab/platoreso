"use strict";

import ModerationMethods from "/imports/moderation/ModerationMethods";

import React, { Component, PropTypes } from "react";
import BaseRow from '../../common/table/BaseRow';

export default class ModerationBaseRow extends BaseRow {

    constructor( props ) {
        super( props );
        this.entityIdField = '_id';
        this.approve  = this.approve.bind( this );
        this.moderate = this.moderate.bind( this );
    }

    approve() {
        ModerationMethods.approve.call( {
            entityTypeName: this.props.entityTypeName,
            entityId:       this.props.entity[this.entityIdField]
        } );
    }

    moderate() {
        ModerationMethods.moderate.call( {
            entityTypeName: this.props.entityTypeName,
            entityId:       this.props.entity[this.entityIdField]
        } );
    }
}

ModerationBaseRow.propTypes = _.defaults( {
    entityTypeName: PropTypes.string.isRequired
}, BaseRow.propTypes );
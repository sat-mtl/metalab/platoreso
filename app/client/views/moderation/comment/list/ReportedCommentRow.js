"use strict";

import React, {Component, PropTypes} from "react";
import {Link} from 'react-router';
import classNames from "classnames";
import prune from "underscore.string/prune";
import UserHelpers from "/imports/accounts/UserHelpers";
import ModerationBaseRow from '../../common/ModerationBaseRow';

export default class ReportedCommentRow extends ModerationBaseRow {

    shouldComponentUpdate( nextProps, nextState ) {
        return super.shouldComponentUpdate( nextProps, nextState ) && ( !_.isEqual( nextProps, this.props ) || !_.isEqual( nextState, this.state ) );
    }

    render() {
        const {
                  entity: comment,
                  author,
                  selected
                  } = this.props;

        return (
            <tr className={ classNames( { 'active' : selected } ) }
                onClick={this.select}
                onDoubleClick={this.edit}>
                <td><Link to={`${comment.objects[0].type}/${comment.objects[0].id}`}>{prune(comment.post, 64)}</Link></td>
                <td className={classNames({error: !author})}>
                    <Link to={`/profile/${comment.author}`}>{author ? UserHelpers.getDisplayName(author) : comment.author}</Link>
                </td>
                <td className="collapsing">{comment.entityType}</td>
                <td className="collapsing">{comment.entityId}</td>
                <td className="collapsing">
                    { comment.reportCount }
                </td>
                <td className="right aligned collapsing">
                    <div ref="actions" className="ui mini buttons">
                        {/*<Link className="ui compact icon button"
                              data-content={__('View')}
                              to={'/comment/' + comment._id}>
                            <i className="idea icon"/>
                        </Link>*/}
                        <button className="ui compact olive icon button"
                                onClick={this.approve}
                                data-content={__('Approve')}>
                            <i className="thumbs up icon"/>
                        </button>
                        <Link className="ui compact yellow icon button"
                                data-content={__('Edit')}
                                to={`/comment/${comment._id}/edit`}>
                            <i className="write icon"/>
                        </Link>
                        <button className="ui compact orange icon button"
                                onClick={this.moderate}
                                data-content={__('Moderate')}>
                            <i className="legal icon"/>
                        </button>
                        <button className="ui compact red icon button"
                                onClick={this.remove}
                                data-content={__('Remove')}>
                            <i className="remove icon"/>
                        </button>
                    </div>
                </td>
            </tr>
        );
    }
}

ReportedCommentRow.propTypes = _.defaults( {}, ModerationBaseRow.propTypes );
"use strict";

import React, {Component, PropTypes} from "react";
import slugify from "underscore.string/slugify";
import escapeStringRegexp from "escape-string-regexp";
import {Counts} from "meteor/tmeasday:publish-counts";
import CommentCollection from "/imports/comments/CommentCollection";
import CommentMethods from "/imports/comments/CommentMethods";
import UserCollection from "/imports/accounts/UserCollection";
import {ConnectMeteorData} from "../../../../lib/ReactMeteorData";
import BaseList, {BaseTable} from "../../../common/table/BaseList";
import ReportedCommentRow from "./ReportedCommentRow";

export class ReportedCommentList extends BaseList {

    constructor( props ) {
        super( props );
        this.tableComponent = ReportedCommentTable;
        this.state          = _.extend( this.state, {
            orderBy: 'reportCount',
            order:   -1
        } );
    }

    remove( comment ) {
        CommentMethods.remove.call( { commentId: comment._id }, ( err ) => {
            if ( err ) {
                this.setState( { error: err } );
            }
        } );
    }

    getHeadersFromProps( props ) {
        let headers = super.getHeadersFromProps( props );
        headers.push( { field: 'post', label: __( 'Comment' ) } );
        headers.push( { field: 'author', label: __( 'Author' ) } );
        headers.push( { field: 'entityType', label: __( 'Entity Type' ) } );
        headers.push( { field: 'entityId', label: __( 'Entity Id' ) } );
        headers.push( { field: 'reportCount', label: __( 'Report Count' ) } );
        return headers;
    }

    renderRow( comment ) {
        const { selectedEntity: selectedComment } = this.state;
        const { users }                           = this.props;

        return (
            <ReportedCommentRow key={comment._id}
                                entityTypeName={CommentCollection.entityName}
                                entity={comment}
                                author={ users ? _.findWhere( users, { _id: comment.author } ) : null }
                                selected={selectedComment && selectedComment._id == comment._id}
                                onSelect={this.onSelect}
                                onEdit={this.onEdit}
                                onRemove={this.confirmRemove}/>
        );
    }

    renderRemoveModal( comment ) {
        return (
            <div className="image content">
                <div className="image">
                    <i className="bullseye icon"/>
                </div>
                <div className="description">
                    <p>{__( 'Are you sure you want to remove the comment __comment__?', { comment: comment.name } )}</p>
                </div>
            </div>
        );
    }
}

ReportedCommentList.propTypes = {
    moderated: PropTypes.bool
};

ReportedCommentList.defaultProps = {
    moderated: false
};

export default ConnectMeteorData( props => {
    return {
        users: UserCollection.find( {} ).fetch()
    }
} )( ReportedCommentList );


const ReportedCommentTable = ConnectMeteorData( ( { moderated, search, options } ) => {
    let query = { moderated: moderated };

    if ( !_.isEmpty( search ) ) {
        query.post = { $regex: escapeStringRegexp( search ), $options: 'i' }
    }

    return {
        loading: !Meteor.subscribe( 'comment/moderation/report/list', query, options.subscription ).ready(),
        rows:    CommentCollection.findReported( query, options.local ).fetch(),
        total:   Counts.get( 'comment/moderation/report/count/' + slugify( JSON.stringify( query ) ) )
    };
} )( BaseTable );
"use strict";

import ReportedCommentList from './list/ReportedCommentList';
import React, { Component, PropTypes } from "react";

export default class Comments extends Component {

    render() {
        return (
            <section id="moderation-comment-list" className="sixteen wide column moderation-comment-list">
                <header className="ui clearing basic segment">
                    <h2 className="ui left floated header">
                        {__( 'Reported Comments' )}
                        <div className="sub header">{__( 'Manage comments that were reported by users' )}</div>
                    </h2>
                </header>
                <ReportedCommentList/>
                <header className="ui clearing basic segment">
                    <h2 className="ui left floated header">
                        {__( 'Moderated Comments' )}
                        <div className="sub header">{__( 'Manage comments that were moderated' )}</div>
                    </h2>
                </header>
                <ReportedCommentList moderated={true}/>
            </section>
        );
    }
}
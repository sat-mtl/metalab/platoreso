"use strict";

import React, {Component, PropTypes} from "react";

export default () => (
    <section id="moderation-dashboard" className="sixteen wide column moderation-dashboard">
        <header>
            <h2 className="ui header">
                {__( 'Moderation' )}
                <div className="sub header">{__( 'All reported items.' )}</div>
            </h2>
        </header>
    </section>
);
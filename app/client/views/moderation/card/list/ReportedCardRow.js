"use strict";

import React, {Component, PropTypes} from "react";
import classNames from "classnames";
import {Link} from "react-router";
import CardImagesCollection from "/imports/cards/images/CardImagesCollection";
import {ConnectMeteorData} from '../../../../lib/ReactMeteorData';
import ModerationBaseRow from "../../common/ModerationBaseRow";
import Image from "../../../widgets/Image";

export class ReportedCardRow extends ModerationBaseRow {

    constructor( props ) {
        super( props );
        this.entityIdField = 'id';
    }

    shouldComponentUpdate( nextProps, nextState ) {
        return super.shouldComponentUpdate( nextProps, nextState ) && ( !_.isEqual( nextProps, this.props ) || !_.isEqual( nextState, this.state ) );
    }

    render() {
        const {
                  users,
                  author,
                  projects,
                  project,
                  entity: card,
                  cardImage,
                  selected
              }                  = this.props;
        const { hasBeenVisible } = this.state;

        return (
            <tr className={ classNames( { 'active': selected } ) }
                onClick={this.select}
                onDoubleClick={this.edit}>
                <td className="collapsing">
                    <Image image={ hasBeenVisible ? cardImage : null }
                           size="thumb"
                           className="circular idea avatar"/>
                </td>
                <td><Link to={`/card/${card.uri}`}>{card.name}</Link></td>
                { users ? (
                    <td className={classNames( { error: !author } )}>
                        <Link to={`/profile/${card.author}`}>{author ? author.profile.firstName + ' ' + author.profile.lastName : card.author}</Link>
                    </td>
                ) : null }
                { projects ? (
                    <td className={classNames( { error: !project } )}>
                        <Link to={`/project/${project ? project.uri : card.project}`}>{project ? project.name : card.project}</Link>
                    </td>
                ) : null }
                <td className="collapsing">
                    { card.reportCount }
                </td>
                <td className="right aligned collapsing">
                    <div ref="actions" className="ui mini buttons">
                        <Link className="ui compact icon button"
                              data-content={__( 'Card Page' )}
                              to={`/card/${card.uri}`}>
                            <i className="idea icon"/>
                        </Link>
                        <button className="ui compact olive icon button"
                                onClick={this.approve}
                                data-content={__( 'Approve' )}>
                            <i className="thumbs up icon"/>
                        </button>
                        <Link className="ui compact yellow icon button"
                              data-content={__( 'Edit' )}
                              to={`/card/${card.uri}/edit`}>
                            <i className="write icon"/>
                        </Link>
                        <button className="ui compact orange icon button"
                                onClick={this.moderate}
                                data-content={__( 'Moderate' )}>
                            <i className="legal icon"/>
                        </button>
                        <button className="ui compact red icon button"
                                onClick={this.remove}
                                data-content={__( 'Remove' )}>
                            <i className="remove icon"/>
                        </button>
                    </div>
                </td>
            </tr>
        );
    }
}

ReportedCardRow.propTypes = _.defaults( {
    users:     PropTypes.bool.isRequired,
    author:    PropTypes.object,
    projects:  PropTypes.bool.isRequired,
    project:   PropTypes.object,
    cardImage: PropTypes.object
}, ModerationBaseRow.propTypes );

export default ConnectMeteorData( props => {
    const { entity: card } = props;
    return {
        cardImage: card ? CardImagesCollection.findOne( { owners: card.id } ) : null
    }
} )( ReportedCardRow );
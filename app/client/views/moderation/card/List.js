"use strict";

import ReportedCardList from './list/ReportedCardList';
import React, { Component, PropTypes } from "react";

export default class Cards extends Component {

    render() {
        return (
            <section id="moderation-card-list" className="sixteen wide column moderation-card-list">
                <header className="ui clearing basic segment">
                    <h2 className="ui left floated header">
                        {__( 'Reported Cards' )}
                        <div className="sub header">{__( 'Manage cards that were reported by users' )}</div>
                    </h2>
                </header>
                <ReportedCardList/>
                <header className="ui clearing basic segment">
                    <h2 className="ui left floated header">
                        {__( 'Moderated Cards' )}
                        <div className="sub header">{__( 'Manage cards that were moderated' )}</div>
                    </h2>
                </header>
                <ReportedCardList moderated={true}/>
            </section>
        );
    }
}
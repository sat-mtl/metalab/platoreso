"use strict";

import ReportedUserList from './list/ReportedUserList';
import React, { Component, PropTypes } from "react";

export default class Users extends Component {

    render() {
        return (
            <section id="moderation-user-list" className="sixteen wide column moderation-user-list">
                <header className="ui clearing basic segment">
                    <h2 className="ui left floated header">
                        {__( 'Reported Users' )}
                        <div className="sub header">{__( 'Manage users that were reported by users' )}</div>
                    </h2>
                </header>
                <ReportedUserList/>
                <header className="ui clearing basic segment">
                    <h2 className="ui left floated header">
                        {__( 'Moderated Users' )}
                        <div className="sub header">{__( 'Manage users that were moderated' )}</div>
                    </h2>
                </header>
                <ReportedUserList moderated={true}/>
            </section>
        );
    }
}
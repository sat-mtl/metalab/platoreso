"use strict";

import React, {Component, PropTypes} from "react";
import {Link} from 'react-router';
import classNames from "classnames";
import ModerationBaseRow from '../../common/ModerationBaseRow';
import Avatar from '../../../widgets/Avatar';

export default class ReportedUserRow extends ModerationBaseRow {

    shouldComponentUpdate( nextProps, nextState ) {
        return super.shouldComponentUpdate( nextProps, nextState ) && ( !_.isEqual( nextProps, this.props ) || !_.isEqual( nextState, this.state ) );
    }

    render() {
        const {
                  entity: user,
                  selected
                  } = this.props;

        return (
            <tr className={ classNames( { 'active' : selected } ) }
                onClick={this.select}
                onDoubleClick={this.edit}>
                <td className="collapsing">
                    <Avatar user={user}/>
                </td>
                <td><Link to={`/profile/${user._id}`}>{user.profile ? user.profile.firstName : ''}</Link></td>
                <td><Link to={`/profile/${user._id}`}>{user.profile ? user.profile.lastName : ''}</Link></td>
                <td className="collapsing">
                    { user.reportCount }
                </td>
                <td className="right aligned collapsing">
                    <div ref="actions" className="ui mini buttons">
                        <Link className="ui compact icon button"
                              data-content={__('User Page')}
                              to={`/profile/${user._id}`}>
                            <i className="idea icon"/>
                        </Link>
                        <button className="ui compact olive icon button"
                                onClick={this.approve}
                                data-content={__('Approve')}>
                            <i className="thumbs up icon"/>
                        </button>
                        <Link className="ui compact yellow icon button"
                                data-content={__('Edit')}
                                to={`/admin/user/${user._id}`}>
                            <i className="write icon"/>
                        </Link>
                        <button className="ui compact orange icon button"
                                onClick={this.moderate}
                                data-content={__('Moderate')}>
                            <i className="legal icon"/>
                        </button>
                        <button className="ui compact red icon button"
                                onClick={this.remove}
                                data-content={__('Remove')}>
                            <i className="remove icon"/>
                        </button>
                    </div>
                </td>
            </tr>
        );
    }
}

ReportedUserRow.propTypes = _.defaults( {

}, ModerationBaseRow.propTypes );
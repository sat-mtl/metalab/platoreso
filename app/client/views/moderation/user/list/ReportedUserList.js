"use strict";

import React, {Component, PropTypes} from "react";
import slugify from "underscore.string/slugify";
import escapeStringRegexp from "escape-string-regexp";
import {Counts} from "meteor/tmeasday:publish-counts";
import UserCollection from "/imports/accounts/UserCollection";
import UserHelpers from "/imports/accounts/UserHelpers";
import UserMethods from "/imports/accounts/UserMethods";
import {ConnectMeteorData} from "../../../../lib/ReactMeteorData";
import BaseList, {BaseTable} from "../../../common/table/BaseList";
import ReportedUserRow from "./ReportedUserRow";

export default class ReportedUserList extends BaseList {

    constructor( props ) {
        super( props );
        this.tableComponent = ReportedUserTable;
        this.state          = _.extend( this.state, {
            orderBy: 'reportCount',
            order:   -1
        } );
    }

    remove( user ) {
        UserMethods.remove( user._id, ( err ) => {
            if ( err ) {
                this.setState( { error: err } );
            }
        } );
    }

    getHeadersFromProps( props ) {
        let headers = super.getHeadersFromProps( props );
        headers.push( { field: 'profile.firstName_sort', label: __( 'First Name' ), colSpan: 2 } );
        headers.push( { field: 'profile.lastName_sort', label: __( 'Last Name' ) } );
        headers.push( { field: 'reportCount', label: __( 'Report Count' ) } );
        return headers;
    }

    renderRow( user ) {
        const { selectedEntity: selectedUser } = this.state;
        return (
            <ReportedUserRow key={user._id}
                             entity={user}
                             entityTypeName={UserCollection.entityName}
                             selected={selectedUser && selectedUser._id == user._id}
                             onSelect={this.onSelect}
                             onEdit={this.onEdit}
                             onRemove={this.confirmRemove}/>
        );
    }

    renderRemoveModal( user ) {
        return (
            <div className="image content">
                <div className="image">
                    <i className="bullseye icon"/>
                </div>
                <div className="description">
                    <p>{__( 'Are you sure you want to remove the user __user__?', { user: UserHelpers.getDisplayName( user ) } )}</p>
                </div>
            </div>
        );
    }
}

ReportedUserList.propTypes = {
    moderated: PropTypes.bool
};

ReportedUserList.defaultProps = {
    moderated: false
};

const ReportedUserTable = ConnectMeteorData( ( { moderated, search, options } ) => {
    let query = { moderated: moderated };

    if ( !_.isEmpty( search ) ) {
        query.$or = [
            { 'profile.firstName': { $regex: escapeStringRegexp( search ), $options: 'i' } },
            { 'profile.lastName': { $regex: escapeStringRegexp( search ), $options: 'i' } },
            { 'emails.address': { $regex: escapeStringRegexp( search ), $options: 'i' } }
        ];
    }

    return {
        loading: !Meteor.subscribe( 'user/moderation/report/list', query, options.subscription ).ready(),
        rows:    UserCollection.findReported( query, options.local ).fetch(),
        total:   Counts.get( 'user/moderation/report/count/' + slugify( JSON.stringify( query ) ) )
    };
} )( BaseTable );
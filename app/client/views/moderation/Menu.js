"use strict";

import React, { Component, PropTypes } from "react";
import classNames from "classnames";
import {Link} from 'react-router';
import UserHelpers from "/imports/accounts/UserHelpers";
import {ConnectMeteorData} from '../../lib/ReactMeteorData';
import links from './menu/Links';

export class Menu extends Component {

    render() {
        const { user } = this.props;
        const topLevelModerator = UserHelpers.isModerator( user );

        return (
            <nav id="moderationMenu" className="ui inverted vertical menu fixed top management-menu">
                { links
                    .filter( link => link.enabled && ( topLevelModerator || link.groupModeratorsAllowed ) )
                    .map( link => (
                    <Link key={link.to} to={link.to} onlyActiveOnIndex={link.index} className="teal item" activeClassName="active">
                        <i className="icons icon">
                            <i className={ classNames( link.icon, 'icon' )}/>
                            <i className="inverted corner flag icon"/>
                        </i>
                        {link.label}
                    </Link>
                ) ) }
                {this.props.children}
            </nav>
        );
    }
}

Menu.propTypes = {
    user: PropTypes.object.isRequired
};

export default ConnectMeteorData( props => {
    return {
        user: Meteor.user()
    }
} )( Menu );
"use strict";

import React, { Component, PropTypes } from "react";
import {ConnectMeteorData} from '../lib/ReactMeteorData';
import UserHelpers from "/imports/accounts/UserHelpers";
import ManagementPage from './layouts/ManagementPage';
import AdminMenu from './admin/Menu';
import Forbidden from './errors/Forbidden';

export class Admin extends Component {

    render() {
        const { user } = this.props;

        if ( !UserHelpers.isExpert( user ) ) {
            return <Forbidden/>
        }

        return (
            <ManagementPage id="admin" className="admin" menu={<AdminMenu/>}>
                {this.props.children}
            </ManagementPage>
        );
    }

}

Admin.propTypes = {
    user: PropTypes.object.isRequired
};

export default ConnectMeteorData( props => {
    return {
        user: Meteor.user()
    }
} )( Admin );
"use strict";

import GroupList from './list/GroupList';

import React, { Component, PropTypes } from "react";

export default class Groups extends Component {

    render() {
        const {router} = this.context;

        return (
            <section id="admin-group-list" className="sixteen wide column admin-group-list">
                <header className="ui clearing basic segment">
                    <h2 className="ui left floated header">
                        {__( 'Groups' )}
                        <div className="sub header">{__( 'Manage groups.' )}</div>
                    </h2>
                    <div className="ui right floated secondary menu">
                        <button className="ui green right labeled icon button" onClick={() => router.push( { pathname: '/admin/group/create' } )}>
                            <i className="add icon"/>
                            {__( 'Add Group' )}
                        </button>
                    </div>
                </header>
                <GroupList/>
            </section>
        );
    }
}

Groups.contextTypes = {
    router: PropTypes.object.isRequired
};
"use strict";

import React, {Component, PropTypes} from "react";
import slugify from "underscore.string/slugify";
import escapeStringRegexp from "escape-string-regexp";
import {Counts} from "meteor/tmeasday:publish-counts";
import GroupCollection from "/imports/groups/GroupCollection";
import GroupMethods from "/imports/groups/GroupMethods";
import {ConnectMeteorData} from "../../../../lib/ReactMeteorData";
import BaseList, {BaseTable} from "../../../common/table/BaseList";
import GroupRow from "./GroupRow";

export default class GroupList extends BaseList {

    constructor( props ) {
        super( props );
        this.tableComponent = GroupTable;
        this.state          = _.extend( this.state, {
            orderBy: 'name_sort'
        } );
    }

    onEdit( group ) {
        this.context.router.push( { pathname: '/admin/group/' + group._id } );
    }

    remove( group ) {
        GroupMethods.remove.call( { groupId: group._id }, ( err ) => {
            if ( err ) {
                this.setState( { error: err } );
            }
        } );
    }

    getHeadersFromProps( props ) {
        let headers = super.getHeadersFromProps( props );
        headers.push( { field: 'name_sort', label: __( 'Name' ), colSpan: 2 } );
        headers.push( { field: 'createdAt', label: __( 'Created' ) } );
        headers.push( { field: 'updatedAt', label: __( 'Modified' ) } );
        headers.push( { field: 'public', label: __( 'Visibility' ) } );
        return headers;
    }

    renderRow( group ) {
        const { selectedEntity: selectedGroup } = this.state;
        return (
            <GroupRow key={group._id}
                      entity={group}
                      selected={selectedGroup && selectedGroup._id == group._id}
                      onSelect={this.onSelect}
                      onEdit={this.onEdit}
                      onRemove={this.confirmRemove}/>
        );
    }

    renderRemoveModal( group ) {
        return (
            <div className="image content">
                <div className="image">
                    <i className="users icon"/>
                </div>
                <div className="description">
                    <p>{__( 'Are you sure you want to remove the group __group__?', { group: group.name } )}</p>
                </div>
            </div>
        );
    }
}

GroupList.contextTypes = {
    router: PropTypes.object.isRequired
};

const GroupTable = ConnectMeteorData( ( { search, options } ) => {
    const query = {};

    if ( !_.isEmpty( search ) ) {
        query.name = { $regex: escapeStringRegexp( search ), $options: 'i' };
    }

    return {
        loading: !Meteor.subscribe( 'admin/group/list', query, options.subscription ).ready(),
        rows:    GroupCollection.find( query, options.local ).fetch(),
        total:   Counts.get( 'group/count/' + slugify( JSON.stringify( query ) ) )
    };
} )( BaseTable );
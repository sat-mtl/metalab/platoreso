"use strict";

import React, { Component, PropTypes } from "react";
import moment from "moment";
import classNames from "classnames";
import {Link} from 'react-router';
import GroupImagesCollection from "/imports/groups/GroupImagesCollection";
import {ConnectMeteorData} from '../../../../lib/ReactMeteorData';
import BaseRow from '../../../common/table/BaseRow';
import Image from '../../../widgets/Image';
import GroupVisibility from '../widgets/Visibility';

export class GroupRow extends BaseRow {

    shouldComponentUpdate(nextProps, nextState) {
        return super.shouldComponentUpdate(nextProps, nextState) && ( !_.isEqual( nextProps, this.props ) || !_.isEqual( nextState, this.state ) );
    }

    render() {
        const { entity: group, groupImage, selected } = this.props;
        const { hasBeenVisible } = this.state;

        return (
            <tr className={ classNames( { 'active' : selected } ) }
                onClick={this.select}
                onDoubleClick={this.edit}>
                <td className="collapsing">
                    <Image image={ hasBeenVisible ? groupImage : null }
                           size="thumb"
                           className="circular group avatar"/>
                </td>
                <td>
                    <Link to={'/admin/group/' + group._id}>{group.name}</Link>
                </td>
                <td className="collapsing">
                    { moment(group.createdAt).fromNow() }
                </td>
                <td className="collapsing">
                    { moment(group.updatedAt ? group.updatedAt : group.createdAt).fromNow() }
                </td>
                <td className="collapsing"><GroupVisibility group={group}/></td>
                <td className="right aligned collapsing">
                    <div ref="actions" className="ui mini buttons">
                        <Link className="ui compact icon button"
                              data-content={__('Group Page')}
                              to={'/group/' + group._id}>
                            <i className="group icon"/>
                        </Link>
                        <button className="ui compact teal icon button"
                                onClick={this.edit}
                                data-content={__('Edit')}>
                            <i className="write icon"/>
                        </button>
                        <button className="ui compact red icon button"
                                onClick={this.remove}
                                data-content={__('Remove')}>
                            <i className="remove icon"/>
                        </button>
                    </div>
                </td>
            </tr>
        );
    }
}

GroupRow.propTypes = _.defaults( {
    groupImage: PropTypes.object
}, BaseRow.propTypes );

export default ConnectMeteorData( props => {
    const { entity: group } = props;
    return {
        groupImage: group ? GroupImagesCollection.findOne( { owners: group._id } ) : null
    }
} )( GroupRow );
"use strict";

import React, { Component, PropTypes } from "react";
import { RoleList } from "/imports/accounts/Roles";
import GroupUsers from './GroupUsers';

export default class GroupModerators extends Component {
    render() {
        const { group } = this.props;

        return (
            <section>
                <h3 className="ui dividing header">{__( 'Moderators' )}</h3>
                <GroupUsers group={group} role={RoleList.moderator}/>
            </section>
        );
    }
};

GroupModerators.propTypes = {
    group: PropTypes.object.isRequired
};
"use strict";

import React, {Component, PropTypes} from "react";
import GroupImagesCollection from "/imports/groups/GroupImagesCollection";
import {ConnectMeteorData} from "../../../../lib/ReactMeteorData";
import ImageUploader from "../../../../components/forms/ImageUploader";

export class GroupPicture extends Component {

    render() {
        const { group, groupImage } = this.props;

        return (
            <section>
                <h3 className="ui dividing header">{__( 'Picture' )}</h3>
                <ImageUploader collection={GroupImagesCollection}
                               file={groupImage}
                               owner={group._id}/>
            </section>
        );
    }
}

GroupPicture.propTypes = {
    group:      PropTypes.object.isRequired,
    groupImage: PropTypes.object
};

export default ConnectMeteorData( props => {
    return {
        groupImage: GroupImagesCollection.findOne( { owners: props.group._id } )
    }
} )( GroupPicture );
"use strict";

import ProjectList from '../../project/list/ProjectList';

import React, { Component, PropTypes } from "react";

export default class GroupProjects extends Component {

    render() {
        const {group} = this.props;

        return (
            <section>
                <h3 className="ui dividing header">{__( 'Projects' )}</h3>
                <ProjectList group={group}/>
            </section>
        );
    }
}

GroupProjects.propTypes = {
    group: PropTypes.object.isRequired
};
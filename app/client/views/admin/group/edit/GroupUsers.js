"use strict";

import React, {Component, PropTypes} from "react";
import UserCollection from "/imports/accounts/UserCollection";
import GroupMethods from "/imports/groups/GroupMethods";
import {GROUP_PREFIX} from "/imports/groups/GroupRoles";
import {ConnectMeteorData} from "../../../../lib/ReactMeteorData";
import UserSelect from "../../common/UserSelect";

export class GroupUsers extends Component {

    constructor( props ) {
        super( props );

        this.addUser    = this.addUser.bind( this );
        this.removeUser = this.removeUser.bind( this );

        this.state = {
            error: null
        };
    }

    addUser( userId ) {
        GroupMethods.addUser.call( {
            groupId: this.props.group._id,
            userId:  userId,
            role:    this.props.role
        }, ( err ) => {
            if ( err ) {
                this.setState( { error: err } );
            }
        } );
    }

    removeUser( userId ) {
        GroupMethods.removeUser.call( {
            groupId: this.props.group._id,
            userId:  userId,
            role:    this.props.role
        }, ( err ) => {
            if ( err ) {
                this.setState( { error: err } );
            }
        } );
    }

    render() {
        const { users } = this.props;

        return ( <UserSelect value={users} onAdd={this.addUser} onRemove={this.removeUser}/> );
    }
}

GroupUsers.propTypes = {
    group: PropTypes.object.isRequired,
    role:  PropTypes.string.isRequired
};

export default ConnectMeteorData( props => {
    const { group, role } = props;
    return {
        usersLoading: !Meteor.subscribe( 'admin/group/member/list', group._id, role ).ready(),
        // Doing the query manually here as the roles package does not let us ignore the global roles
        users:        UserCollection.find( { ['roles.' + GROUP_PREFIX + '_' + group._id]: { $in: [role] } }, { fields: { _id: 1 } } ).fetch().map( user => user._id )
    };
} )( GroupUsers );
"use strict";

import GroupUsers from './GroupUsers';

import { RoleList } from "/imports/accounts/Roles";

import React, { Component, PropTypes } from "react";

export default class GroupExperts extends Component {
    render() {
        const { group } = this.props;

        return (
            <section>
                <h3 className="ui dividing header">{__( 'Experts' )}</h3>
                <GroupUsers group={group} role={RoleList.expert}/>
            </section>
        );
    }
};

GroupExperts.propTypes = {
    group: PropTypes.object.isRequired
};
"use strict";

import GroupUsers from './GroupUsers';

import { RoleList } from "/imports/accounts/Roles";

import React, { Component, PropTypes } from "react";

export default class GroupMembers extends Component {
    render() {
        const { group } = this.props;

        return (
            <section>
                <h3 className="ui dividing header">{__( 'Members' )}</h3>
                <GroupUsers group={group} role={RoleList.member}/>
            </section>
        );
    }
};

GroupMembers.propTypes = {
    group: PropTypes.object.isRequired
};
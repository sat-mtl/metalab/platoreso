"use strict";

import { unflatten } from 'flat';
import formDiff from "/imports/utils/formDiff";
import GroupMethods from "/imports/groups/GroupMethods";

import Form from '../../../../components/forms/Form';
import Field from '../../../../components/forms/Field';
import Input from '../../../../components/forms/Input';
import TextArea from '../../../../components/forms/Textarea';
import Select from '../../../../components/forms/Select';
import CheckBox from '../../../../components/forms/Checkbox';

import React, { Component, PropTypes } from "react";
import classNames from "classnames";

export default class GroupInfo extends Component {

    constructor( props ) {
        super( props );

        this.updateGroup = this.updateGroup.bind( this );

        this.state = {
            error:  null,
            saving: false
        }
    }

    updateGroup( values ) {
        if ( this.state.saving ) {
            return;
        }
        this.setState( { saving: true } );

        //TODO: Is a boolean enough granularity?
        values.public = this.refs.visibility.getValue() == 'public';
        delete values.visibility;
        // Get boolean value instead of the "on" string
        values.showMembers = this.refs.showMembers.getValue();

        // Turn the form values into a proper object and compute diff
        const changes = formDiff( unflatten( values ), this.props.group, ['_id'] );
        GroupMethods.update.call( { _id: this.props.group._id, changes }, ( err ) => {
            if ( err ) {
                this.setState( { error: err } );
            }
            this.setState( { saving: false } );
        } )
    }

    render() {
        const { group }         = this.props;
        const { saving, error } = this.state;

        return (
            <section>
                <h3 className="ui dividing header">{__( 'Information' )}</h3>

                <Form id="group-edit-form" ref="groupEditForm"
                      error={error}
                      onSuccess={this.updateGroup}>

                    <div className="two fields">
                        <Field label={__( "Name" )} className="required">
                            <Input ref="name" name="name" type="text"
                                   defaultValue={group.name}
                                   rules={[
                                       { type: 'empty', prompt: __( 'Please enter a name' ) },
                                       { type: 'minLength[3]', prompt: __( 'Group name should be a minimum of 3 characters long' ) },
                                       { type: 'maxLength[140]', prompt: __( 'Group name can be a maximum of 140 characters long' ) },
                                       { type: 'regExp[/(!?[a-zA-Z0-9])/]', prompt: __( 'Group names should not only contain non-alphanumeric characters' ) }
                                   ]}/>
                        </Field>

                        <Field label={__( "Slug" )} className="required">
                            <Input ref="slug" name="slug" type="text"
                                   defaultValue={group.slug}/>
                        </Field>
                    </div>

                    <Field label={__( "Description" )} className="required">
                        <TextArea ref="description" name="description"
                                  defaultValue={group.description}
                                  rules={[
                                      { type: 'empty', prompt: __( 'Please enter a description' ) }
                                  ]}/>
                    </Field>

                    <div className="two fields">

                        <Field label={__( "Visibility" )}>
                            <Select ref="visibility" name="visibility" defaultValue={group.public ? 'public' : 'private'}>
                                <option value="public">{__( 'Public' )}</option>
                                <option value="private">{__( 'Private' )}</option>
                            </Select>
                        </Field>

                        {/*<Field label={__("Visibility")}>
                         <CheckBox ref="public" name="public" defaultChecked={group.public} label={__('Public')}/>
                         </Field>*/}

                        <Field label={__( "Privacy" )}>
                            <CheckBox ref="showMembers" name="showMembers" defaultChecked={group.showMembers} label={__( 'Show Members' )}/>
                        </Field>

                    </div>

                    <Field label={__( "Invitation Code" )}>
                        <Input ref="invitationCode" name="invitationCode" type="text"
                               defaultValue={group.invitationCode}
                               rules={[
                                   { type: 'regExp[/^[a-zA-Z0-9_-]*$/]', prompt: __( 'Only alphanumeric characters, underscores and dashes are allowed' ) }
                               ]}/>
                    </Field>

                    <div className="ui hidden divider"></div>
                    <div className={classNames( "ui green large right labeled icon fluid submit button", { loading: saving, disabled: saving } )}>
                        <i className="save icon"/>
                        {__( 'Save' )}
                    </div>

                </Form>

            </section>
        );
    }
}

GroupInfo.propTypes = {
    group: PropTypes.object.isRequired
};

"use strict";

import { unflatten } from 'flat';
import formDiff from "/imports/utils/formDiff";
import GroupMethods from "/imports/groups/GroupMethods";

import Form from '../../../../components/forms/Form';
import TinyMCE from '../../../../components/tinymce/TinyMCE';

import React, { Component, PropTypes } from "react";
import classNames from "classnames";

export default class GroupPage extends Component {

    constructor(props) {
        super(props);

        this.updateGroup = this.updateGroup.bind(this);

        this.state =  {
            error: null,
            saving: false
        }
    }

    updateGroup( values ) {
        if ( this.state.saving ) return;
        this.setState({saving: true });

        // Get tinyMCE content
        values.content = this.refs.content.getValue();

        // Turn the form values into a proper object and compute diff
        const changes = formDiff(unflatten(values), this.props.group, ['_id']);
        GroupMethods.update.call( { _id: this.props.group._id, changes }, ( err ) => {
            if ( err ) {
                this.setState({error: err});
            }
            this.setState({saving: false});
        });
    }

    render() {
        const {group} = this.props;
        const {saving, error} = this.state;

        return (
            <section>
                <h3 className="ui dividing header">{__( 'Page' )}</h3>

                <Form id="group-edit-form" ref="groupEditForm"
                      error={error}
                      onSuccess={this.updateGroup}>
                    <TinyMCE ref="content" name="content" defaultValue={group.content}/>
                    {/*<TextArea ref="content" name="content" defaultValue={group.content}/>*/}
                    <div className="ui hidden divider"></div>
                    <div className={classNames("ui green large right labeled icon fluid submit button", {loading: saving, disabled: saving})}>
                        <i className="save icon"/>
                        {__( 'Save' )}
                    </div>
                </Form>

            </section>
        );
    }
}

"use strict";

import GroupMethods from "/imports/groups/GroupMethods";

import Form from '../../../components/forms/Form';
import Field from '../../../components/forms/Field';
import Input from '../../../components/forms/Input';
import TextArea from '../../../components/forms/Textarea';
import CheckBox from '../../../components/forms/Checkbox';

import React, { Component, PropTypes } from "react";

export default class Create extends Component {

    constructor( props ) {
        super( props );

        this.createGroup = this.createGroup.bind( this );

        this.state = {
            error: null
        }
    }

    createGroup( values ) {
        const {router} = this.context;

        // Get boolean value instead of the "on" string
        values.public      = this.refs.public.getValue();
        values.showMembers = this.refs.showMembers.getValue();
        GroupMethods.create.call( { group: values }, ( err, groupId ) => {
            if ( err ) {
                this.setState( {error: err} );
            } else {
                router.replace( { pathname: '/admin/group/' + groupId } );
            }
        } )
    }

    render() {
        return (
            <section id="admin-group-create" className="sixteen wide column admin-group-create">
                <header>
                    <h2 className="ui center aligned icon header">
                        <i className="grey group icon"/>
                        <div className="content">
                            {__( 'New Group' )}
                            <div className="sub header">{__( 'Group creation form.' )}</div>
                        </div>
                    </h2>
                </header>
                <div className="ui centered grid">
                    <div className="twelve wide mobile six wide column">
                        <Form id="group-creation-form" ref="groupCreationForm"
                              error={this.state.error}
                              onSuccess={this.createGroup}>

                            <Field label={__("Name")} className="required">
                                <Input ref="name" name="name" type="text"
                                       rules={[
                                               { type: 'empty', prompt: __('Please enter a name') }
                                           ]}/>
                            </Field>

                            <Field label={__("Description")} className="required">
                                    <TextArea ref="description" name="description" type="text"
                                              rules={[
                                                  { type: 'empty', prompt: __('Please enter a description') }
                                              ]}/>
                            </Field>

                            <div className="two fields">

                                <Field label={__("Visibility")}>
                                    <CheckBox ref="public" name="public" label={__('Public')}/>
                                </Field>

                                <Field label={__("Privacy")}>
                                    <CheckBox ref="showMembers" name="showMembers" label={__('Show Members')}/>
                                </Field>

                            </div>


                            <div className="ui divider"></div>
                            <div className="ui green large right labeled icon fluid submit button">
                                <i className="save icon"/>
                                {__( 'Create Group' )}
                            </div>

                        </Form>

                    </div>
                </div>
            </section>
        );
    }
}

Create.contextTypes = {
    router: PropTypes.object.isRequired
};

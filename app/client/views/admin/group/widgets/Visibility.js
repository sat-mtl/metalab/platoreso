"use strict";

import React, { Component, PropTypes } from "react";

export default class Visibility extends Component {

    constructor(props) {
        super(props);

        this.renderVisibility = this.renderVisibility.bind(this);
    }

    renderVisibility() {
        if ( this.props.group.public ) {
            return (<span className="ui green horizontal label">{__( 'Public' )}</span>);
        } else {
            return (<span className="ui red horizontal label">{__('Private')}</span>);
        }
    }

    render() {
        return (
            <span className="visibility">
                { this.renderVisibility() }
            </span>
        );
    }
}

Visibility.propTypes= {
    group: PropTypes.object.isRequired
};
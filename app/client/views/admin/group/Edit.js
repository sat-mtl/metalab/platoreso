"use strict";

import GroupCollection from "/imports/groups/GroupCollection";
import GroupImagesCollection from "/imports/groups/GroupImagesCollection";

import {Link} from 'react-router';
import React, { Component, PropTypes } from "react";
import {ConnectMeteorData} from '../../../lib/ReactMeteorData';
import NotFound from '../../errors/NotFound';
import Image from '../../widgets/Image';
import GroupVisibility from './widgets/Visibility';

class Edit extends Component {

    render() {
        const { group, groupImage } = this.props;

        if ( !group ) {
            return <NotFound/>;
        } else {
            return (
                <section id="admin-group-edit" className="sixteen wide column admin-group-edit">
                    <header>
                        <h2 className="ui header">
                            <Image image={ groupImage }
                                   size="thumb"
                                   className="small circular group"/>

                            <div className="content">
                                <Link to={'/group/' + (group.slug||group._id) }>{group.name}</Link>
                                <GroupVisibility group={group}/>
                                <div className="sub header">{__( 'Edit group.' )}</div>
                            </div>
                        </h2>
                    </header>
                    <div className="ui stackable grid">
                        <div className="four wide tablet three wide computer column">
                            <div ref="tabs" className="ui secondary vertical fluid pointing menu">
                                <Link to={'/admin/group/' + group._id } onlyActiveOnIndex={true} className="item" activeClassName="active">
                                    {__( 'Information' )}
                                </Link>
                                <Link to={'/admin/group/' + group._id + '/picture'} className="item" activeClassName="active">
                                    {__( 'Picture' )}
                                </Link>
                                <Link to={'/admin/group/' + group._id + '/page'} className="item" activeClassName="active">
                                    {__( 'Page' )}
                                </Link>
                                <Link to={'/admin/group/' + group._id + '/projects'} className="item" activeClassName="active">
                                    {__( 'Projects' )}
                                </Link>
                                <Link to={'/admin/group/' + group._id + '/experts'} className="item" activeClassName="active">
                                    {__( 'Experts' )}
                                </Link>
                                <Link to={'/admin/group/' + group._id + '/moderators'} className="item" activeClassName="active">
                                    {__( 'Moderators' )}
                                </Link>
                                <Link to={'/admin/group/' + group._id + '/members'} className="item" activeClassName="active">
                                    {__( 'Members' )}
                                </Link>
                            </div>
                        </div>
                        <div className="twelve wide tablet thirteen wide computer stretched column">
                            { React.Children.map(this.props.children, child => React.cloneElement( child, { group: group } ) ) }
                        </div>
                    </div>
                </section>
            );
        }
    }
}

export default ConnectMeteorData( props => ({
    groupLoading: !Meteor.subscribe( 'admin/group/edit', props.params.id ),
    group:      GroupCollection.findOne( props.params.id ),
    groupImage: GroupImagesCollection.findOne( { owners: props.params.id } )
}))(Edit);

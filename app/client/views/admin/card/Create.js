"use strict";

import React, {Component, PropTypes} from "react";
import ProjectCollection from "/imports/projects/ProjectCollection";
import CardMethods from "/imports/cards/CardMethods";
import {ConnectMeteorData} from "../../../lib/ReactMeteorData";
import Form from "../../../components/forms/Form";
import Field from "../../../components/forms/Field";
import Input from "../../../components/forms/Input";
import Select from "../../../components/forms/Select";

export class Create extends Component {

    constructor( props ) {
        super( props );

        this.changeProject = this.changeProject.bind( this );
        this.createCard    = this.createCard.bind( this );

        this.state = {
            projectId: null,
            error:     null
        };
    }

    changeProject( projectId ) {
        this.setState( { project: ProjectCollection.findOne( { _id: projectId } ) } );
    }

    createCard( values ) {
        CardMethods.create.call( values, ( err, cardId ) => {
            if ( err ) {
                this.setState( { error: err } );
            } else {
                this.context.router.replace( { pathname: '/admin/card/' + cardId } );
            }
        } );
    }

    render() {
        const { projects } = this.props;

        return (
            <section id="admin-card-create" className="sixteen wide column admin-card-create">
                <header>
                    <h2 className="ui center aligned icon header">
                        <i className="grey idea icon"/>
                        <div className="content">
                            {__( 'New Card' )}
                            <div className="sub header">{__( 'Card creation form.' )}</div>
                        </div>
                    </h2>
                </header>
                <div className="ui centered grid">
                    <div className="twelve wide mobile six wide column">
                        <Form id="card-creation-form" ref="cardCreationForm"
                              error={this.state.error}
                              onSuccess={this.createCard}>

                            <Field label={__( "Project" )} className="required">
                                <Select ref="project" name="project" search={true}
                                        onSelectChange={this.changeProject}
                                        rules={[
                                            { type: 'empty', prompt: __( 'Please select a project' ) }
                                        ]}>
                                    <option disabled value={null}>Select a project</option>
                                    {projects.map( project =>
                                        <option key={project._id} value={project._id}>{project.name}</option> )}
                                </Select>
                            </Field>

                            <Field label={__( "Phase" )} className="required">
                                <Select ref="phase" name="phase" search={true}
                                        rules={[
                                            { type: 'empty', prompt: __( 'Please select a phase' ) }
                                        ]}>
                                    <option disabled>Select a phase</option>
                                    { this.state.project ? this.state.project.phases.map( phase =>
                                        <option key={phase._id} value={phase._id}>{phase.name}</option>
                                    ) : null }
                                </Select>
                            </Field>

                            <Field label={__( "Title" )} className="required">
                                <Input ref="name" name="name" type="text" className="fluid"
                                       rules={[
                                           { type: 'empty', prompt: __( 'Please enter a title' ) }
                                       ]}/>
                            </Field>

                            <div className="ui divider"></div>
                            <div className="ui green large right labeled icon fluid submit button">
                                <i className="save icon"/>
                                {__( 'Create Card' )}
                            </div>

                        </Form>

                    </div>
                </div>
            </section>
        );
    }
}

Create.contextTypes = {
    router: PropTypes.object.isRequired
};

Create.propTypes = {
    projects: PropTypes.arrayOf( PropTypes.object )
};

export default ConnectMeteorData( props => {
    Meteor.subscribe( 'admin/project/list' );
    return {
        projects: ProjectCollection.find( {} ).fetch()
    }
} )( Create );
"use strict";

import CardList from './list/CardList';

import React, { Component, PropTypes } from "react";

export default class Cards extends Component {

    render() {
        const {router} = this.context;

        return (
            <section id="admin-card-list" className="sixteen wide column admin-card-list">
                <header className="ui clearing basic segment">
                    <h2 className="ui left floated header">
                        {__( 'Cards' )}
                        <div className="sub header">{__( 'Manage cards.' )}</div>
                    </h2>
                    <div className="ui right floated secondary menu">
                        <button className="ui green right labeled icon button" onClick={() => router.push( { pathname: '/admin/card/create' } )}>
                            <i className="add icon"/>
                            {__( 'Add Card' )}
                        </button>
                    </div>
                </header>
                <CardList/>
            </section>
        );
    }
}

Cards.contextTypes = {
    router: PropTypes.object.isRequired
};
"use strict";

import moment from "moment";
import React, {Component, PropTypes} from "react";
import classNames from "classnames";
import {Link} from "react-router";
import CardImagesCollection from "/imports/cards/images/CardImagesCollection";
import {ConnectMeteorData} from "../../../../lib/ReactMeteorData";
import BaseRow from "../../../common/table/BaseRow";
import Image from "../../../widgets/Image";

export class CardRow extends BaseRow {

    shouldComponentUpdate( nextProps, nextState ) {
        return super.shouldComponentUpdate( nextProps, nextState ) && ( !_.isEqual( nextProps, this.props ) || !_.isEqual( nextState, this.state ) );
    }

    render() {
        const {
                  users,
                  author,
                  projects,
                  project,
                  entity: card,
                  cardImage,
                  selected
              }                  = this.props;
        const { hasBeenVisible } = this.state;

        return (
            <tr className={ classNames( { 'active': selected } ) }
                onClick={this.select}
                onDoubleClick={this.edit}>
                <td className="collapsing">
                    <Image image={ hasBeenVisible ? cardImage : null}
                           size="thumb"
                           className="circular idea avatar"/>
                </td>
                <td><Link to={'/admin/card/' + card.id }>{card.name}</Link></td>
                { users ? (
                    <td className={classNames( { error: !author } )}>
                        <Link to={'/admin/user/' + card.author}>{author ? author.profile.firstName + ' ' + author.profile.lastName : card.author}</Link>
                    </td>
                ) : null }
                { projects ? (
                    <td className={classNames( { error: !project } )}>
                        <Link to={'/admin/project/' + card.project}>{project ? project.name : card.project}</Link>
                    </td>
                ) : null }
                <td className="collapsing">
                    { moment( card.createdAt ).fromNow() }
                </td>
                <td className="collapsing">
                    { moment( card.updatedAt ? card.updatedAt : card.createdAt ).fromNow() }
                </td>
                <td className="right aligned collapsing">
                    <div ref="actions" className="ui mini buttons">
                        <Link className="ui compact icon button"
                              data-content={__( 'Card Page' )}
                              to={'/card/' + card.slug}>
                            <i className="idea icon"/>
                        </Link>
                        <button className="ui compact teal icon button"
                                onClick={this.edit}
                                data-content={__( 'Edit' )}>
                            <i className="write icon"/>
                        </button>
                        <button className="ui compact red icon button"
                                onClick={this.remove}
                                data-content={__( 'Remove' )}>
                            <i className="remove icon"/>
                        </button>
                    </div>
                </td>
            </tr>
        );
    }
}

CardRow.propTypes = _.defaults( {
    users:     PropTypes.bool.isRequired,
    author:    PropTypes.object,
    projects:  PropTypes.bool.isRequired,
    project:   PropTypes.object,
    cardImage: PropTypes.object
}, BaseRow.propTypes );

export default ConnectMeteorData( props => {
    const { entity: card } = props;
    return {
        cardImage: card ? CardImagesCollection.findOne( { owners: card.id } ) : null
    }
} )( CardRow );
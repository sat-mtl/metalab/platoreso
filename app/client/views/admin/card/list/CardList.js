"use strict";

import React, {Component, PropTypes} from "react";
import slugify from "underscore.string/slugify";
import escapeStringRegexp from "escape-string-regexp";
import {Counts} from "meteor/tmeasday:publish-counts";
import ProjectCollection from "/imports/projects/ProjectCollection";
import CardCollection from "/imports/cards/CardCollection";
import CardMethods from "/imports/cards/CardMethods";
import UserCollection from "/imports/accounts/UserCollection";
import {ConnectMeteorData} from "../../../../lib/ReactMeteorData";
import BaseList, {BaseTable} from "../../../common/table/BaseList";
import CardRow from "./CardRow";

export class CardList extends BaseList {

    constructor( props ) {
        super( props );
        this.tableComponent = CardTable;
        this.state          = _.extend( this.state, {
            orderBy: 'name_sort'
        } );
    }

    onEdit( card ) {
        this.context.router.push( { pathname: '/admin/card/' + card.id } );
    }

    remove( card ) {
        CardMethods.archive.call( { cardId: card.id }, ( err ) => {
            if ( err ) {
                this.setState( { error: err } );
            }
        } );
    }

    getHeadersFromProps( props ) {
        let headers = super.getHeadersFromProps( props );
        headers.push( { field: 'name_sort', label: __( 'Title' ), colSpan: 2 } );
        if ( !props.author ) {
            headers.push( { field: 'author', label: __( 'Author' ) } );
        }
        if ( !props.project ) {
            headers.push( { field: 'project', label: __( 'Project' ) } );
        }
        headers.push( { field: 'createdAt', label: __( 'Created' ) } );
        headers.push( { field: 'updatedAt', label: __( 'Modified' ) } );
        return headers;
    }

    renderRow( card ) {
        const { selectedEntity: selectedCard } = this.state;
        const { users, projects }              = this.props;

        return (
            <CardRow key={card.id}
                     entity={card}
                     projects={!!projects}
                     project={ projects ? _.findWhere( projects, { _id: card.project } ) : null }
                     users={!!users}
                     author={ users ? _.findWhere( users, { _id: card.author } ) : null }
                     selected={selectedCard && selectedCard.id == card.id}
                     onSelect={this.onSelect}
                     onEdit={this.onEdit}
                     onRemove={this.confirmRemove}/>
        );
    }

    renderRemoveModal( card ) {
        return (
            <div className="image content">
                <div className="image">
                    <i className="idea icon"/>
                </div>
                <div className="description">
                    <p>{__( 'Are you sure you want to remove the card __card__?', { card: card.name } )}
                    </p>
                </div>
            </div>
        );
    }

}

CardList.propTypes = {
    project: PropTypes.object,
    author:  PropTypes.object
};

CardList.contextTypes = {
    router: PropTypes.object.isRequired
};

export default ConnectMeteorData( ( { project, author } ) => {
    let data = {};

    if ( !project ) {
        data.projects = ProjectCollection.find( {} ).fetch();
    }

    if ( !author ) {
        data.users = UserCollection.find( {} ).fetch();
    }

    return data;
} )( CardList );

const CardTable = ConnectMeteorData( ( { project, author, search, options } ) => {
    let query = {};

    if ( project ) {
        query.project = project._id;
    }

    if ( author ) {
        query.author = author._id;
    }

    if ( !_.isEmpty( search ) ) {
        query.name = { $regex: escapeStringRegexp( search ), $options: 'i' }
    }

    return {
        loading: !Meteor.subscribe( 'admin/card/list', query, options.subscription ).ready(),
        rows:    CardCollection.find( _.extend( { current: true }, query ), options.local ).fetch(),
        total:   Counts.get( 'card/count/' + slugify( JSON.stringify( query ) ) )
    };
} )( BaseTable );
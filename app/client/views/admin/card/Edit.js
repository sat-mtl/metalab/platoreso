"use strict";

import CardCollection from "/imports/cards/CardCollection";
import CardImagesCollection from "/imports/cards/images/CardImagesCollection";

import React, { Component, PropTypes } from "react";
import {Link} from 'react-router';
import {ConnectMeteorData} from '../../../lib/ReactMeteorData';
import NotFound from '../../errors/NotFound';
import Image from '../../widgets/Image';


class Edit extends Component {

    render() {
        const {card, cardImage} = this.props;

        if ( !card ) {
            return <NotFound/>;
        } else {
            return (
                <section id="admin-card-edit" className="sixteen wide column admin-card-edit">
                    <header>
                        <h2 className="ui header">
                            <Image image={ cardImage }
                                   size="thumb"
                                   className="small circular idea"/>

                            <div className="content">
                                <Link to={"/project/" + card.project + "/board/" + (card.slug||card.id)}>{card.name}</Link>
                                <div className="sub header">{__( 'Edit card.' )}</div>
                            </div>
                        </h2>
                    </header>
                    <div className="ui stackable grid">
                        <div className="four wide tablet three wide computer column">
                            <div className="ui secondary vertical fluid pointing menu">
                                <Link to={`/admin/card/${card.id}`} onlyActiveOnIndex={true} className="item" activeClassName="active">
                                    {__( 'Card' )}
                                </Link>
                                <Link to={`/admin/card/${card.id}/picture` } className="item" activeClassName="active">
                                    {__( 'Picture' )}
                                </Link>
                                <Link to={`/admin/card/${card.id}/history` } className="item" activeClassName="active">
                                    {__( 'History' )}
                                </Link>
                            </div>
                        </div>
                        <div className="twelve wide tablet thirteen wide computer stretched column">
                            { React.Children.map(this.props.children, child => React.cloneElement( child, { card: card } ) ) }
                        </div>
                    </div>
                </section>
            );
        }
    }
}

export default ConnectMeteorData( props => ({
    cardLoading: !Meteor.subscribe( 'admin/card/edit', props.params.id ),
    card: CardCollection.findOne( { id: props.params.id, current: true }),
    cardImage: CardImagesCollection.findOne( { owners: props.params.id } )
}))(Edit);

"use strict";

import moment from "moment";
import React, {Component, PropTypes} from "react";
import classNames from "classnames";
import {Link} from "react-router";
import CardImagesCollection from "/imports/cards/images/CardImagesCollection";
import {ConnectMeteorData} from "../../../../../lib/ReactMeteorData";
import BaseRow from "../../../../common/table/BaseRow";
import Image from "../../../../widgets/Image";

export class SnapshotRow extends BaseRow {

    shouldComponentUpdate( nextProps, nextState ) {
        return super.shouldComponentUpdate( nextProps, nextState ) && ( !_.isEqual( nextProps, this.props ) || !_.isEqual( nextState, this.state ) );
    }

    render() {
        const {
                  entity: card,
                  cardImage,
                  selected
              }                  = this.props;
        const { hasBeenVisible } = this.state;

        return (
            <tr className={ classNames( { 'active': selected } ) }
                onClick={this.select}
                onDoubleClick={this.edit}>
                <td className="collapsing">
                    <Image image={ hasBeenVisible ? cardImage : null}
                           size="thumb"
                           className="circular idea avatar"/>
                </td>
                <td><Link to={`/card/${card.uri}`}>{card.name}</Link></td>
                <td className="collapsing">
                    { card.version }
                </td>
                <td className="center aligned collapsing">
                    <i className={ classNames('icon', { 'green checkmark' : card.current } ) }/>
                </td>
                <td className="collapsing">
                    { moment( card.updatedAt ).fromNow() }
                </td>
                <td className="collapsing">
                    { moment( card.versionedAt ).fromNow() }
                </td>
                <td className="right aligned collapsing">
                    <div ref="actions" className="ui mini buttons">
                        <Link className="ui compact icon button"
                              data-content={__( 'Card Page' )}
                              to={`/card/${card.uri}`}>
                            <i className="idea icon"/>
                        </Link>
                    </div>
                </td>
            </tr>
        );
    }
}

SnapshotRow.propTypes = _.defaults( {
    cardImage: PropTypes.object
}, BaseRow.propTypes );

export default ConnectMeteorData( props => {
    const { entity: card } = props;
    return {
        cardImage: card ? CardImagesCollection.findOne( { owners: card.id } ) : null
    }
} )( SnapshotRow );
"use strict";

import React, { Component, PropTypes } from "react";
import slugify from "underscore.string/slugify";
import escapeStringRegexp from "escape-string-regexp";
import { Counts } from "meteor/tmeasday:publish-counts";
import CardCollection from "/imports/cards/CardCollection";
import { ConnectMeteorData } from "../../../../../lib/ReactMeteorData";
import BaseList, {BaseTable} from "../../../../common/table/BaseList";
import SnapshotRow from "./SnapshotRow";

export default class CardList extends BaseList {

    constructor( props ) {
        super( props );
        this.tableComponent = SnapshotTable;
        this.state          = _.extend( this.state, {
            orderBy: 'name_sort'
        } );
    }

    onEdit( card ) {
        this.context.router.push( { pathname: '/admin/card/' + card.id } );
    }

    getHeadersFromProps( props ) {
        let headers = super.getHeadersFromProps( props );
        headers.push( { field: 'name_sort', label: __( 'Title' ), colSpan: 2 } );
        headers.push( { field: 'version', label: __( 'Version' ) } );
        headers.push( { field: 'current', label: __( 'Current' ) } );
        headers.push( { field: 'updatedAt', label: __( 'Modified' ) } );
        headers.push( { field: 'versionedAt', label: __( 'Versioned' ) } );
        return headers;
    }

    renderRow( card ) {
        const { users, projects }              = this.props;
        const { selectedEntity: selectedCard } = this.state;

        return (
            <SnapshotRow key={card._id}
                         entity={card}
                         projects={!!projects}
                         project={ projects ? _.findWhere( projects, { _id: card.project } ) : null }
                         users={!!users}
                         author={ users ? _.findWhere( users, { _id: card.author } ) : null }
                         selected={selectedCard && selectedCard._id == card._id}
                         onSelect={this.onSelect}
                         onEdit={this.onEdit}
                         onRemove={this.confirmRemove}/>
        );
    }

}

CardList.propTypes = {
    card: PropTypes.object
};

CardList.contextTypes = {
    router: PropTypes.object.isRequired
};

const SnapshotTable = ConnectMeteorData( ( { card, search, options } ) => {
    const query = {
        id:      card.id
    };

    if ( !_.isEmpty( search ) ) {
        query.name = { $regex: escapeStringRegexp( search ), $options: 'i' }
    }

    return {
        loading: !Meteor.subscribe( 'admin/card/history', query, options.subscription ).ready(),
        rows:    CardCollection.find( query, options.local ).fetch(),
        total:   Counts.get( 'card/history/count/' + slugify( JSON.stringify( query ) ) )
    }
} )( BaseTable );
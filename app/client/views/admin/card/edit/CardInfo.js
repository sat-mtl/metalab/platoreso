"use strict";

import { unflatten } from 'flat';
import formDiff from "/imports/utils/formDiff";
import {Link} from 'react-router';
import classNames from "classnames";
import {ConnectMeteorData} from '../../../../lib/ReactMeteorData';

import Form from '../../../../components/forms/Form';
import Field from '../../../../components/forms/Field';
import Input from '../../../../components/forms/Input';
import TextArea from '../../../../components/forms/Textarea';
import Select from '../../../../components/forms/Select';
import ImageUploader from '../../../../components/forms/ImageUploader';

import CardTags from '../../../card/edit/CardTags';

import React, { Component, PropTypes } from "react";
import ProjectCollection from "/imports/projects/ProjectCollection";
import CardMethods from "/imports/cards/CardMethods";

export class CardInfo extends Component {

    constructor(props) {
        super(props);

        this.switchProject = this.switchProject.bind(this);
        this.updateCard = this.updateCard.bind(this);

        this.state = {
            error: null,
            saving: false,
            overrideProject: null
        }
    }

    switchProject(projectId) {
        this.setState({
            overrideProject: projectId
        });
    }

    updateCard( values ) {
        if ( this.state.saving ) return;
        this.setState({saving: true });

        // Turn the form values into a proper object and compute diff
        const changes = formDiff(unflatten(values), this.props.card, ['_id']);
        CardMethods.update.call( changes, ( err ) => {
            if ( err ) {
                this.setState({error:err});
            }
            this.setState({saving: false});
        })
    }

    render() {
        const {card, projects} = this.props;
        const {saving, overrideProject} = this.state;

        const project = projects && projects.find( project => project._id == (overrideProject || card.project) );

        return (
            <section>
                <h3 className="ui dividing header">{__( 'Card' )}</h3>

                <Form id="card-edit-form" ref="cardEditForm"
                      error={this.state.error}
                      onSuccess={this.updateCard}>
                    <input type="hidden" name="_id" value={card.id}/>

                    <Field label={__("Title")} className="required">
                        <Input ref="name" name="name" type="text" className="fluid"
                               defaultValue={card.name}
                               rules={[
                                   { type: 'empty', prompt: __('Please enter a title') },
                                   { type: 'minLength[3]', prompt: __( 'Title should be a minimum of 3 characters long' ) },
                                   { type: 'maxLength[140]', prompt: __( 'Title can be a maximum of 140 characters long' ) }
                               ]}/>
                    </Field>

                    <Field label={__("Project")} className="required">
                        <Select ref="project" name="project" search={true}
                                defaultValue={card.project}
                                onSelectChange={this.switchProject}
                                rules={[
                                   { type: 'empty', prompt: __('Please select a project') }
                                ]}>
                            <option disabled>{__('Select a project')}</option>
                            { _.map( projects, ( project ) => (
                                <option key={project._id} value={project._id}>{project.name}</option>
                            ) )}
                        </Select>
                        <Link to={'/admin/project/' + card.project}>{__('Edit Project')}</Link>
                    </Field>

                    { project && project.phases && <Field label={__("Phase")} className="required">
                        <Select ref="phase" name="phase" search={true}
                                defaultValue={card.phase}
                                rules={[
                                   { type: 'empty', prompt: __('Please select a phase') }
                                ]}>
                            <option disabled>{__('Select a phase')}</option>
                            { _.map( project.phases, phase => (
                                <option key={phase._id} value={phase._id}>{phase.name}</option>
                            ) )}
                        </Select>
                        <Link to={'/admin/project/' + card.project + '/phases'}>{__('Edit Phase')}</Link>
                    </Field> }

                    <CardTags tags={card.tags} projectId={project._id}/>

                    <Field label={__("Content")}>
                        <TextArea ref="content" name="content" defaultValue={card.content}/>
                    </Field>

                    <div className="ui hidden divider"></div>
                    <div className={classNames("ui green large right labeled icon fluid submit button", {loading: saving, disabled: saving})}>
                        <i className="save icon"/>
                        {__( 'Save' )}
                    </div>

                </Form>

            </section>
        );
    }
}

CardInfo.propTypes = {
    card: PropTypes.object.isRequired
};

export default ConnectMeteorData( props => {
    return {
        projects: ProjectCollection.find( {} ).fetch()
    }
})(CardInfo);
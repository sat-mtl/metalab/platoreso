"use strict";

import React, {Component, PropTypes} from "react";
import CardImagesCollection from "/imports/cards/images/CardImagesCollection";
import {ConnectMeteorData} from '../../../../lib/ReactMeteorData';
import ImageUploader from "../../../../components/forms/ImageUploader";

export class CardPicture extends Component {

    render() {
        const { card, cardImage } = this.props;

        return (
            <section>
                <h3 className="ui dividing header">{__( 'Picture' )}</h3>
                <ImageUploader collection={CardImagesCollection}
                               file={cardImage}
                               owner={card.id}/>
            </section>
        );
    }
}

CardPicture.propTypes = {
    card:      PropTypes.object.isRequired,
    cardImage: PropTypes.object
};

export default ConnectMeteorData( props => {
    return {
        cardImage: CardImagesCollection.findOne( { owners: props.card.id } )
    }
} )( CardPicture );
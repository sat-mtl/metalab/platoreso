"use strict";

import React, { Component, PropTypes } from "react";
import SnapshotList from "./history/SnapshotList";

export default ( {card} ) => (
    <section>
        <h3 className="ui dividing header">{__( 'History' )}</h3>
        <SnapshotList card={card}/>
    </section>
);
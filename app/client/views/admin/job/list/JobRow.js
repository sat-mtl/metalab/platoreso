"use strict";

import moment from "moment";
import React, {Component, PropTypes} from "react";
import classNames from "classnames";
import {Link} from 'react-router';
import Progress from '../../../../components/semanticui/modules/Progress';
import BaseRow from '../../../common/table/BaseRow';

export default class JobRow extends BaseRow {

    shouldComponentUpdate( nextProps, nextState ) {
        return super.shouldComponentUpdate( nextProps, nextState ) && ( !_.isEqual( nextProps, this.props ) || !_.isEqual( nextState, this.state ) );
    }

    render() {
        const {
                  entity: job,
                  selected
              } = this.props;

        return (
            <tr className={ classNames( { 'active' : selected } ) }
                onClick={this.select}
                onDoubleClick={this.edit}>
                <td><Link to={'/admin/job/' + job._id}>{job.type}</Link></td>
                <td>
                    <Progress showLabel={false}
                              showProgress={false}
                              progress={job.progress.percent}
                              className={classNames({
                                disabled: job.status == "waiting",
                                active: job.status == "running",
                                //warning: job.status == "waiting",
                                error: job.status == "failed",
                                success: job.status = "completed"
                              })}/>
                </td>
                <td className="collapsing">{__( "__percent__%", { percent: job.progress.percent } )}</td>
                <td className="collapsing">{job.status}</td>
                <td className="collapsing">{job.priority}</td>
                <td className="collapsing">{job.retries}</td>
                <td className="collapsing">{job.retried}</td>
                <td className="collapsing">{job.repeated}</td>
                <td className="collapsing">
                    { moment( job.created ).fromNow() }
                </td>
                <td className="collapsing">
                    { moment( job.updated ? job.updated : job.created ).fromNow() }
                </td>
                <td className="right aligned collapsing">
                    {/*<div ref="actions" className="ui mini buttons">
                        <button className="ui compact teal icon button"
                                onClick={this.edit}
                                data-content={__('Edit')}>
                            <i className="write icon"/>
                        </button>
                        <button className="ui compact red icon button"
                                onClick={this.remove}
                                data-content={__('Remove')}>
                            <i className="remove icon"/>
                        </button>
                    </div>*/}
                </td>
            </tr>
        );
    }
}

JobRow.propTypes = _.defaults( {}, BaseRow.propTypes );
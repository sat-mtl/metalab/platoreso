"use strict";

import React, {Component, PropTypes} from "react";
import slugify from "underscore.string/slugify";
import escapeStringRegexp from "escape-string-regexp";
import {Counts} from "meteor/tmeasday:publish-counts";
import JobCollection from "/imports/jobs/JobCollection";
import {ConnectMeteorData} from "../../../../lib/ReactMeteorData";
import BaseList, {BaseTable} from "../../../common/table/BaseList";
import JobRow from "./JobRow";

export default class JobList extends BaseList {

    constructor( props ) {
        super( props );
        this.tableComponent = JobTable;
        this.state          = _.extend( this.state, {
            orderBy: 'status'
        } );
    }

    onEdit( job ) {
        //this.context.router.push( { pathname: '/admin/job/' + job._id } );
    }

    remove( job ) {
        /*JobMethods.remove.call( { jobId: job._id }, ( err ) => {
         if ( err ) {
         this.setState( {error: err} );
         }
         } );*/
    }

    getHeadersFromProps( props ) {
        let headers = super.getHeadersFromProps( props );
        headers.push( { field: 'type', label: __( 'Type' ) } );
        headers.push( { field: 'progress.percent', label: __( 'Progress' ), colSpan: 2 } );
        headers.push( { field: 'status', label: __( 'Status' ) } );
        headers.push( { field: 'priority', label: __( 'Priority' ) } );
        headers.push( { field: 'retries', label: __( 'Retries' ) } );
        headers.push( { field: 'retried', label: __( 'Retried' ) } );
        headers.push( { field: 'repeated', label: __( 'Repeated' ) } );
        headers.push( { field: 'created', label: __( 'Created' ) } );
        headers.push( { field: 'updated', label: __( 'Modified' ) } );
        return headers;
    }

    renderRow( job ) {
        const { selectedEntity: selectedJob } = this.state;

        return (
            <JobRow key={job._id}
                    entity={job}
                    selected={selectedJob && selectedJob._id == job._id}
                    onSelect={this.onSelect}
                    onEdit={this.onEdit}
                    onRemove={this.confirmRemove}/>
        );
    }

    renderRemoveModal( job ) {
        return (
            <div className="image content">
                <div className="image">
                    <i className="job icon"/>
                </div>
                <div className="description">
                    <p>{__( 'Are you sure you want to remove the job __job__?', { job: job.type } )}</p>
                </div>
            </div>
        );
    }
}

JobList.propTypes = {};

JobList.contextTypes = {
    router: PropTypes.object.isRequired
};

const JobTable = ConnectMeteorData( ( { search, options } ) => {
    let query = {};

    if ( !_.isEmpty( search ) ) {
        query.type = { $regex: escapeStringRegexp( search ), $options: 'i' }
    }

    return {
        loading: !Meteor.subscribe( 'admin/job/list', query, options.subscription ).ready(),
        rows:    JobCollection.find( query, options.local ).fetch(),
        total:   Counts.get( 'job/count/' + slugify( JSON.stringify( query ) ) )
    };
} )( BaseTable );
"use strict";

import JobList from './list/JobList';

import React, { Component, PropTypes } from "react";

export default class Jobs extends Component {

    render() {
        const {history} = this.props;

        return (
            <section id="admin-job-list" className="sixteen wide column admin-job-list">
                <header className="ui clearing basic segment">
                    <h2 className="ui left floated header">
                        {__( 'Jobs' )}
                        <div className="sub header">{__( 'Manage jobs.' )}</div>
                    </h2>
                </header>
                <JobList/>
            </section>
        );
    }
}
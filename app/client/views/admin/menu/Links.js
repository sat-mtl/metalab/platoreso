"use strict";

export default [
    {
        to: '/admin/',
        get label() { return __('Dashboard'); },
        icon: 'dashboard',
        index: true
    },
    {
        to: '/admin/user',
        get label() { return __('Users'); },
        icon: 'user'
    },
    {
        to: '/admin/group',
        get label() { return __('Groups'); },
        icon: 'group'
    },
    {
        to: '/admin/project',
        get label() { return __('Projects'); },
        icon: 'bullseye'
    },
    {
        to: '/admin/card',
        get label() { return __('Cards'); },
        icon: 'idea'
    },
    {
        to: '/admin/comment',
        get label() { return __('Comments'); },
        icon: 'comments'
    },
    {
        to: '/admin/job',
            get label() { return __('Jobs'); },
        icon: 'wait'
    }
];
"use strict";

import CommentCollection from "/imports/comments/CommentCollection";
import React, { Component, PropTypes } from "react";
import prune from "underscore.string/prune";
import {Link} from 'react-router';
import {ConnectMeteorData} from '../../../lib/ReactMeteorData';
import NotFound from '../../errors/NotFound';
import CommentInfo from './edit/CommentInfo';

class Edit extends Component {

    render() {
        const {comment} = this.props;

        if ( !comment ) {
            return <NotFound/>;
        } else {
            return (
                <section id="admin-comment-edit" className="sixteen wide column admin-comment-edit">
                    <header>
                        <h2 className="ui header">
                            <div className="content">
                                <Link to={`/comment/${comment._id}`}>{prune( comment.post, 64 )}</Link>
                                <div className="sub header">{__( 'Edit comment.' )}</div>
                            </div>
                        </h2>
                    </header>
                    <div className="ui stackable grid">
                        <div className="four wide tablet three wide computer column">
                            <div className="ui secondary vertical fluid pointing menu">
                                <Link to={`/admin/comment/${comment._id}`} onlyActiveOnIndex={true} className="item" activeClassName="active">
                                    {__( 'Information' )}
                                </Link>
                            </div>
                        </div>
                        <div className="twelve wide tablet thirteen wide computer stretched column">
                            { React.Children.map( this.props.children, child => React.cloneElement( child, {comment: comment} ) ) }
                        </div>
                    </div>
                </section>
            );
        }
    }
}

export default ConnectMeteorData( props => ({
    commentLoading: !Meteor.subscribe( 'admin/comment/edit', props.params.id ),
    comment:        CommentCollection.findOne( props.params.id )
}) )( Edit );
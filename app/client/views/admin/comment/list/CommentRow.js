"use strict";

import UserHelpers from "/imports/accounts/UserHelpers";
import moment from "moment";
import prune from "underscore.string/prune";
import React, { Component, PropTypes } from "react";
import classNames from "classnames";
import {Link} from 'react-router';
import BaseRow from '../../../common/table/BaseRow';

export default class CommentRow extends BaseRow {

    shouldComponentUpdate(nextProps, nextState) {
        return super.shouldComponentUpdate(nextProps, nextState) && ( !_.isEqual( nextProps, this.props ) || !_.isEqual( nextState, this.state ) );
    }

    render() {
        const {
                  entity: comment,
                  author,
                  users,
                  selected
                  } = this.props;

        return (
            <tr className={ classNames( { 'active' : selected } ) }
                onClick={this.select}
                onDoubleClick={this.edit}>
                <td><Link to={'/admin/comment/' + comment._id}>{prune(comment.post, 64)}</Link></td>
                { users ? (
                <td className={classNames({error: !author})}>
                    <Link to={'/admin/user/' + comment.author}>{author ? UserHelpers.getDisplayName(author) : comment.author}</Link>
                </td>
                    ) : null }
                <td className="collapsing">{comment.entityType}</td>
                <td className="collapsing">{comment.entityId}</td>
                <td className="collapsing">
                    { moment(comment.createdAt).fromNow() }
                </td>
                <td className="collapsing">
                    { moment(comment.updatedAt ? comment.updatedAt : comment.createdAt).fromNow() }
                </td>
                <td className="right aligned collapsing">
                    <div ref="actions" className="ui mini buttons">
                        <Link className="ui compact icon button"
                              data-content={__('Comment Page')}
                              to={"/comment/" + comment._id}>
                            <i className="bullseye icon"/>
                        </Link>
                        <button className="ui compact teal icon button"
                                onClick={this.edit}
                                data-content={__('Edit')}>
                            <i className="write icon"/>
                        </button>
                        <button className="ui compact red icon button"
                                onClick={this.remove}
                                data-content={__('Remove')}>
                            <i className="remove icon"/>
                        </button>
                    </div>
                </td>
            </tr>
        );
    }
}

CommentRow.propTypes = _.defaults( {
    users:   PropTypes.bool
}, BaseRow.propTypes );
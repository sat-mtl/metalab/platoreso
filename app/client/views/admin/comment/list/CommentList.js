"use strict";

import React, { Component, PropTypes } from "react";
import slugify from "underscore.string/slugify";
import escapeStringRegexp from "escape-string-regexp";
import { Counts } from "meteor/tmeasday:publish-counts";
import CommentCollection from "/imports/comments/CommentCollection";
import CommentMethods from "/imports/comments/CommentMethods";
import UserCollection from "/imports/accounts/UserCollection";
import { ConnectMeteorData } from "../../../../lib/ReactMeteorData";
import BaseList, { BaseTable } from "../../../common/table/BaseList";
import CommentRow from "./CommentRow";

export class CommentList extends BaseList {

    constructor( props ) {
        super( props );
        this.tableComponent = CommentTable;
        this.state          = _.extend( this.state, {
            orderBy: 'post'
        } );
    }

    onEdit( comment ) {
        this.context.router.push( { pathname: '/admin/comment/' + comment._id } );
    }

    remove( comment ) {
        CommentMethods.remove.call( { commentId: comment._id }, ( err ) => {
            if ( err ) {
                this.setState( { error: err } );
            }
        } );
    }

    getHeadersFromProps( props ) {
        let headers = super.getHeadersFromProps( props );
        headers.push( { field: 'post', label: __( 'Comment' ) } );
        if ( !props.author ) {
            headers.push( { field: 'author', label: __( 'Author' ) } );
        }
        headers.push( { field: 'entityType', label: __( 'Entity Type' ) } );
        headers.push( { field: 'entityId', label: __( 'Entity Id' ) } );
        headers.push( { field: 'createdAt', label: __( 'Created' ) } );
        headers.push( { field: 'updatedAt', label: __( 'Modified' ) } );
        return headers;
    }

    renderRow( comment ) {
        const { selectedEntity: selectedComment } = this.state;
        const { users }                           = this.props;

        return (
            <CommentRow key={comment._id}
                        entity={comment}
                        users={!!users}
                        author={ users ? _.findWhere( users, { _id: comment.author } ) : null }
                        selected={selectedComment && selectedComment._id == comment._id}
                        onSelect={this.onSelect}
                        onEdit={this.onEdit}
                        onRemove={this.confirmRemove}/>
        );
    }

    renderRemoveModal( comment ) {
        return (
            <div className="image content">
                <div className="image">
                    <i className="comment icon"/>
                </div>
                <div className="description">
                    <p>{__( 'Are you sure you want to remove the comment __comment__?', { comment: comment.post } )}</p>
                </div>
            </div>
        );
    }
}

CommentList.propTypes = {
    author: PropTypes.object
};

CommentList.contextTypes = {
    router: PropTypes.object.isRequired
};

export default ConnectMeteorData( ( { author } ) => {
    let data = {};

    if ( !author ) {
        data.users = UserCollection.find( {} ).fetch();
    }

    return data;
} )( CommentList );

const CommentTable = ConnectMeteorData( ( { author, search, options } ) => {
    let query = {};

    if ( author ) {
        query.author = author._id;
    }

    if ( !_.isEmpty( search ) ) {
        query.post = { $regex: escapeStringRegexp( search ), $options: 'i' }
    }

    return {
        loading: !Meteor.subscribe( 'admin/comment/list', query, options.subscription ).ready(),
        rows:    CommentCollection.find( query, options.local ).fetch(),
        total:   Counts.get( 'comment/count/' + slugify( JSON.stringify( query ) ) )
    };
} )( BaseTable );
"use strict";

import { unflatten } from 'flat';
import formDiff from "/imports/utils/formDiff";
import CommentMethods from "/imports/comments/CommentMethods";

import React, { Component, PropTypes } from "react";
import classNames from "classnames";
import Form from '../../../../components/forms/Form';
import Field from '../../../../components/forms/Field';
import Input from '../../../../components/forms/Input';
import TextArea from '../../../../components/forms/Textarea';

export default class CommentInfo extends Component {

    constructor( props ) {
        super( props );

        this.updateComment = this.updateComment.bind( this );

        this.state = {
            error:  null,
            saving: false
        }
    }

    updateComment( values ) {
        const { comment } = this.props;

        if ( this.state.saving ) {
            return;
        }
        this.setState( { saving: true } );

        // Turn the form values into a proper object and compute diff
        //const changes = formDiff(unflatten(values), this.props.comment, ['_id']);
        CommentMethods.update.call( {
            commentId: comment._id,
            post:      values.post
        }, ( err ) => {
            if ( err ) {
                this.setState( { error: err } );
            }
            this.setState( { saving: false } );
        } );
    }

    render() {
        const { comment } = this.props;
        const { saving }  = this.state;

        if ( !comment ) {
            return null;
        }

        return (
            <section>
                <h3 className="ui dividing header">{__( 'Information' )}</h3>

                <Form id="comment-info-form" ref="commentInfoForm"
                      error={this.state.error}
                      onSuccess={this.updateComment}>
                    <Field label={__( "Post" )} className="required">
                        <TextArea ref="post" name="post"
                                  defaultValue={comment.post}
                                  rules={[
                                      { type: 'empty', prompt: __( 'Please enter a post' ) }
                                  ]}/>
                    </Field>

                    <div className="ui hidden divider"/>
                    <div className={classNames( "ui green large right labeled icon fluid submit button", { loading: saving, disabled: saving } )}>
                        <i className="save icon"/>
                        {__( 'Save' )}
                    </div>

                </Form>
            </section>
        );
    }
}

CommentInfo.propTypes = {
    comment: PropTypes.object//.isRequired
};
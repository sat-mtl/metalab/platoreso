"use strict";

import CommentList from './list/CommentList';

import React, { Component, PropTypes } from "react";

export default class Comments extends Component {

    render() {
        const {history} = this.props;

        return (
            <section id="admin-comment-list" className="sixteen wide column admin-comment-list">
                <header className="ui clearing basic segment">
                    <h2 className="ui left floated header">
                        {__( 'Comments' )}
                        <div className="sub header">{__( 'Manage comments.' )}</div>
                    </h2>
                </header>
                <CommentList/>
            </section>
        );
    }
}
"use strict";

import React, {Component, PropTypes} from "react";
import slugify from "underscore.string/slugify";
import escapeStringRegexp from "escape-string-regexp";
import {Counts} from "meteor/tmeasday:publish-counts";
import ProjectCollection from "/imports/projects/ProjectCollection";
import ProjectMethods from "/imports/projects/ProjectMethods";
import {ConnectMeteorData} from "../../../../lib/ReactMeteorData";
import BaseList, {BaseTable} from "../../../common/table/BaseList";
import ProjectRow from "./ProjectRow";

export default class ProjectList extends BaseList {

    constructor( props ) {
        super( props );
        this.tableComponent = ProjectTable;
        this.state          = _.extend( this.state, {
            orderBy: 'name_sort'
        } );
    }

    onEdit( project ) {
        this.context.router.push( { pathname: '/admin/project/' + project._id } );
    }

    remove( project ) {
        ProjectMethods.remove.call( { projectId: project._id }, ( err ) => {
            if ( err ) {
                this.setState( { error: err } );
            }
        } );
    }

    getHeadersFromProps( props ) {
        let headers = super.getHeadersFromProps( props );
        headers.push( { field: 'name_sort', label: __( 'Name' ), colSpan: 2 } );
        headers.push( { field: 'createdAt', label: __( 'Created' ) } );
        headers.push( { field: 'updatedAt', label: __( 'Modified' ) } );
        headers.push( { field: 'public', label: __( 'Visibility' ) } );
        return headers;
    }

    renderRow( project ) {
        const { selectedEntity: selectedProject } = this.state;

        return (
            <ProjectRow key={project._id}
                        entity={project}
                        selected={selectedProject && selectedProject._id == project._id}
                        onSelect={this.onSelect}
                        onEdit={this.onEdit}
                        onRemove={this.confirmRemove}/>
        );
    }

    renderRemoveModal( project ) {
        return (
            <div className="image content">
                <div className="image">
                    <i className="bullseye icon"/>
                </div>
                <div className="description">
                    <p>{__( 'Are you sure you want to remove the project __project__?', { project: project.name } )}</p>
                </div>
            </div>
        );
    }
}

ProjectList.contextTypes = {
    router: PropTypes.object.isRequired
};

const ProjectTable = ConnectMeteorData( ( { group, search, options } ) => {
    let query = {};

    if ( !_.isEmpty( search ) ) {
        query.name = { $regex: escapeStringRegexp( search ), $options: 'i' };
    }

    if ( group ) {
        query.groups = group._id;
    }

    return {
        loading: !Meteor.subscribe( 'admin/project/list', query, options.subscription ).ready(),
        rows:    ProjectCollection.find( query, options.local ).fetch(),
        total:   Counts.get( 'project/count/' + slugify( JSON.stringify( query ) ) )
    };
} )( BaseTable );
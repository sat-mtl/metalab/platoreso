"use strict";

import React, { Component, PropTypes } from "react";
import moment from "moment";
import classNames from "classnames";
import {Link} from 'react-router';
import ProjectImagesCollection from "/imports/projects/ProjectImagesCollection";
import {ConnectMeteorData} from '../../../../lib/ReactMeteorData';
import BaseRow from '../../../common/table/BaseRow';
import Image from '../../../widgets/Image';
import ProjectVisibility from '../widgets/Visibility'

export class ProjectRow extends BaseRow {

    shouldComponentUpdate(nextProps, nextState) {
        return super.shouldComponentUpdate(nextProps, nextState) && ( !_.isEqual( nextProps, this.props ) || !_.isEqual( nextState, this.state ) );
    }

    render() {
        const { entity: project, projectImage, selected} = this.props;
        const { hasBeenVisible } = this.state;

        return (
            <tr className={ classNames( { 'active' : selected } ) }
                onClick={this.select}
                onDoubleClick={this.edit}>
                <td className="collapsing">
                    <Image image={ hasBeenVisible ? projectImage : null }
                           size="thumb"
                           className="circular project avatar"/>
                </td>
                <td><Link to={'/admin/project/' + project._id}>{project.name}</Link></td>
                <td className="collapsing">
                    { moment(project.createdAt).fromNow() }
                </td>
                <td className="collapsing">
                    { moment(project.updatedAt ? project.updatedAt : project.createdAt).fromNow() }
                </td>
                <td className="collapsing">{<ProjectVisibility project={project}/>}</td>
                <td className="right aligned collapsing">
                    <div ref="actions" className="ui mini buttons">
                        <Link className="ui compact icon button"
                              data-content={__('Project Page')}
                              to={"/project/" + project._id}>
                            <i className="bullseye icon"/>
                        </Link>
                        <button className="ui compact teal icon button"
                                onClick={this.edit}
                                data-content={__('Edit')}>
                            <i className="write icon"/>
                        </button>
                        <button className="ui compact red icon button"
                                onClick={this.remove}
                                data-content={__('Remove')}>
                            <i className="remove icon"/>
                        </button>
                    </div>
                </td>
            </tr>
        );
    }
}

ProjectRow.propTypes = _.defaults( {
    projectImage: PropTypes.object
}, BaseRow.propTypes );

export default ConnectMeteorData( props => {
    const { entity: project } = props;
    return {
        projectImage: project ? ProjectImagesCollection.findOne( { owners: project._id } ) : null
    }
} )( ProjectRow );

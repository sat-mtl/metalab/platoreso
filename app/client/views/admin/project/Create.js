"use strict";

import React, { Component, PropTypes } from "react";
import {History} from 'react-router';

import ProjectMethods from "/imports/projects/ProjectMethods";

import Form from '../../../components/forms/Form';
import Field from '../../../components/forms/Field';
import Input from '../../../components/forms/Input';
import TextArea from '../../../components/forms/Textarea';
import Select from '../../../components/forms/Select';
import ImageUploader from '../../../components/forms/ImageUploader';

export default React.createClass( {

    contextTypes: {
        router: PropTypes.object.isRequired
    },

    getInitialState() {
        return {
            error: null
        }
    },

    createProject( values ) {
        ProjectMethods.create.call( { project: values }, ( err, projectId ) => {
            if ( err ) {
                console.error(err);
                this.setState( {error: err} );
            } else {
                this.context.router.replace( { pathname: '/admin/project/' + projectId } );
            }
        } );
    },

    render() {
        return (
            <section id="admin-project-create" className="sixteen wide column admin-project-create">
                <header>
                    <h2 className="ui center aligned icon header">
                        <i className="grey bullseye icon"/>
                        <div className="content">
                            {__( 'New Project' )}
                            <div className="sub header">{__( 'Project creation form.' )}</div>
                        </div>
                    </h2>
                </header>
                <div className="ui centered grid">
                    <div className="twelve wide mobile six wide column">
                        <Form id="project-creation-form" ref="projectCreationForm"
                              error={this.state.error}
                              onSuccess={this.createProject}>

                            {/*<div className="two fields">*/}
                            <Field label={__("Name")} className="required">
                                <Input ref="name" name="name" type="text"
                                       rules={[
                                                   { type: 'empty', prompt: __('Please enter a name') }
                                               ]}/>
                            </Field>

                            {/*<Field label={__("Slug")} className="required">
                             <Input ref="slug" name="slug" type="text"
                             rules={[
                             { type: 'empty', prompt: __('Please enter a slug') }
                             ]}/>
                             </Field>*/}
                            {/*</div>*/}

                            <Field label={__("Description")} className="required">
                                    <TextArea ref="description" name="description" type="text"
                                              rules={[
                                                  { type: 'empty', prompt: __('Please enter a description') }
                                              ]}/>
                            </Field>

                            <div className="ui divider"></div>
                            <div className="ui green large right labeled icon fluid submit button">
                                <i className="save icon"/>
                                {__( 'Create Project' )}
                            </div>

                        </Form>

                    </div>
                </div>
            </section>
        );
    }
} );
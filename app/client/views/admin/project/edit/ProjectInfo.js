"use strict";

import { unflatten } from 'flat';
import formDiff from "/imports/utils/formDiff";
import ProjectMethods from "/imports/projects/ProjectMethods";

import React, { Component, PropTypes } from "react";
import classNames from "classnames";
import Form from '../../../../components/forms/Form';
import Field from '../../../../components/forms/Field';
import Input from '../../../../components/forms/Input';
import TextArea from '../../../../components/forms/Textarea';
import Select from '../../../../components/forms/Select';
import CheckBox from '../../../../components/forms/Checkbox';

export default class ProjectInfo extends Component {

    constructor( props ) {
        super( props );

        this.updateProject = this.updateProject.bind( this );

        this.state = {
            error:  null,
            saving: false
        }
    }

    updateProject( values ) {
        if ( this.state.saving ) {
            return;
        }
        this.setState( { saving: true } );

        //TODO: Is a boolean enough granularity?
        values.public = this.refs.visibility.getValue() == 'public';
        delete values.visibility;
        values.showTimeline = this.refs.showTimeline.getValue();

        // Turn the form values into a proper object and compute diff
        const changes = formDiff( unflatten( values ), this.props.project );
        ProjectMethods.update.call( { _id: this.props.project._id, changes }, ( err ) => {
            if ( err ) {
                this.setState( { error: err } );
            }
            this.setState( { saving: false } );
        } );
    }

    render() {
        const { project } = this.props;
        const { saving }  = this.state;

        if ( !project ) {
            return null;
        }

        return (
            <section>
                <h3 className="ui dividing header">{__( 'Information' )}</h3>

                <Form id="project-info-form" ref="projectInfoForm"
                      error={this.state.error}
                      onSuccess={this.updateProject}>

                    <div className="two fields">
                        <Field label={__( "Name" )} className="required">
                            <Input ref="name" name="name" type="text"
                                   defaultValue={project.name}
                                   rules={[
                                       { type: 'empty', prompt: __( 'Please enter a name' ) },
                                       { type: 'minLength[3]', prompt: __( 'Project name should be a minimum of 3 characters long' ) },
                                       { type: 'maxLength[140]', prompt: __( 'Project name can be a maximum of 140 characters long' ) },
                                       { type: 'regExp[/(!?[a-zA-Z0-9])/]', prompt: __( 'Project names should not only contain non-alphanumeric characters' ) }
                                   ]}/>
                        </Field>

                        <Field label={__( "Slug" )} className="required">
                            <Input ref="slug" name="slug" type="text"
                                   defaultValue={project.slug}
                                   readOnly={true}/>
                        </Field>
                    </div>

                    <Field label={__( "Description" )} className="required">
                        <TextArea ref="description" name="description"
                                  defaultValue={project.description}
                                  rules={[
                                      { type: 'empty', prompt: __( 'Please enter a description' ) }
                                  ]}/>
                    </Field>

                    <Field label={__( "Hashtag" )}>
                        <Input ref="hashtag" name="hashtag" type="text" className="labeled"
                               defaultValue={project.hashtag}
                               rules={[
                                   { type: 'doesntContain[#]', prompt: __( 'You do not need to put the # symbol.' ) },
                                   { type: 'regExp[/^[a-z0-9_]*$/gi]', prompt: __( 'Only letters, numbers and underscores are allowed.' ) }
                               ]}
                               before={<div className="ui label">#</div>}/>
                    </Field>

                    <div className="two fields">

                        <Field label={__( "Visibility" )}>
                            <Select ref="visibility" name="visibility" defaultValue={project.public ? 'public' : 'private'}>
                                <option value="public">{__( 'Public' )}</option>
                                <option value="private">{__( 'Private' )}</option>
                            </Select>
                        </Field>

                        <Field label={__( "Timeline" )}>
                            <CheckBox ref="showTimeline" name="showTimeline" defaultChecked={project.showTimeline} label={__( 'Show Timeline' )}/>
                        </Field>

                    </div>

                    <div className="ui hidden divider"></div>
                    <div className={classNames( "ui green large right labeled icon fluid submit button", { loading: saving, disabled: saving } )}>
                        <i className="save icon"/>
                        {__( 'Save' )}
                    </div>

                </Form>
            </section>
        );
    }
}

ProjectInfo.propTypes = {
    project: PropTypes.object//.isRequired
};
"use strict";

import { unflatten } from 'flat';
import formDiff from "/imports/utils/formDiff";
import ProjectMethods from "/imports/projects/ProjectMethods";
import React, { Component, PropTypes } from "react";
import classNames from "classnames";
import Form from '../../../../components/forms/Form';
import TinyMCE from '../../../../components/tinymce/TinyMCE';

export default class ProjectPage extends Component {

    constructor(props) {
        super(props);

        this.updateProject = this.updateProject.bind(this);

        this.state = {
            error: null,
            saving: false
        }
    }

    updateProject( values ) {
        if (this.state.saving) return;
        this.setState({saving:true});

        // Get tinyMCE content
        values.content = this.refs.content.getValue();

        // Turn the form values into a proper object and compute diff
        const changes = formDiff(unflatten(values), this.props.project );
        ProjectMethods.update.call( { _id: this.props.project._id, changes }, ( err ) => {
            if ( err ) {
                this.setState({error: err});
            }
            this.setState({saving: false});
        });
    }

    render() {
        const {project} = this.props;
        const {saving} = this.state;

        return (
            <section>
                <h3 className="ui dividing header">{__( 'Page' )}</h3>

                <Form id="project-page-form" ref="projectPageForm"
                      error={this.state.error}
                      onSuccess={this.updateProject}>
                    <input type="hidden" name="_id" value={project._id}/>
                    <TinyMCE ref="content" name="content" defaultValue={project.content}/>
                    {/*<TextArea ref="content" name="content" defaultValue={project.content}/>*/}
                    <div className="ui hidden divider"></div>
                    <div className={classNames("ui green large right labeled icon fluid submit button", { loading: saving, disabled: saving})}>
                        <i className="save icon"/>
                        {__( 'Save' )}
                    </div>

                </Form>
            </section>
        );
    }
}

ProjectPage.propTypes = {
    project: PropTypes.object.isRequired
};
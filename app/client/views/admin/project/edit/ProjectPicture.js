"use strict";

import React, { Component, PropTypes } from "react";
import ProjectImagesCollection from "/imports/projects/ProjectImagesCollection";
import {ConnectMeteorData} from '../../../../lib/ReactMeteorData';
import ImageUploader from '../../../../components/forms/ImageUploader';

export class ProjectPicture extends Component {

    render() {
        const { project, projectImage } = this.props;

        return (
            <section>
                <h3 className="ui dividing header">{__( 'Picture' )}</h3>
                <ImageUploader collection={ProjectImagesCollection}
                               file={projectImage}
                               owner={project._id}/>
            </section>
        );
    }
}

ProjectPicture.propTypes = {
    project: PropTypes.object.isRequired,
    projectImage: PropTypes.object
};

export default ConnectMeteorData( props => {
    return {
        projectImage: ProjectImagesCollection.findOne( { owners: props.project._id } )
    }
} )( ProjectPicture );
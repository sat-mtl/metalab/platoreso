"use strict";

import React, {Component, PropTypes} from "react";
import classNames from "classnames";

//import { DragSource, DropTarget } from 'react-dnd';

export default class PhaseRow extends Component {

    constructor( props ) {
        super( props );
        this.onSelect = this.onSelect.bind( this );
        this.onEdit   = this.onEdit.bind( this );
        this.onRemove = this.onRemove.bind( this );
    }

    onSelect() {
        this.props.onSelect( this.props.phase );
    }

    onEdit() {
        this.props.onEdit( this.props.phase );
    }

    onRemove() {
        this.props.onRemove( this.props.phase );
    }

    render() {
        const { phase, selected } = this.props;
        //const { connectDragSource, connectDragPreview, connectDropTarget, isDragging } = this.props;

        return /*connectDropTarget( connectDragPreview*/ (
            <div className={ classNames( 'item', 'phase', /*{ dragged: isDragging },*/ { active: selected, disabled: phase.removed } ) }
                 onClick={this.onSelect}
                 onDoubleClick={this.onEdit}>
                <div className="right floated content">
                    { !phase.removed && <div className="ui mini buttons">
                        <div className={ classNames( "ui compact teal right labeled icon button", { disabled: phase.removed } ) }
                             onClick={this.onEdit}
                             data-content={__( 'Edit' )}
                             data-variation="inverted">
                            {__( 'Edit' )}
                            <i className="write icon"/>
                        </div>
                        <div className={ classNames( "ui compact red icon button", { disabled: phase.removed } ) }
                             onClick={this.onRemove}
                             data-content={__( 'Remove' )}
                             data-variation="inverted">
                            <i className="remove icon"/>
                        </div>
                    </div> }
                    { phase.removed && <div className="ui tiny grey label">
                        {__( 'Removed' )}
                    </div> }
                </div>
                {/*connectDragSource(<i className="move handle sidebar icon"/>)*/}
                <div className="content">
                    <div className="header">{phase.name}</div>
                    <div className="description">
                        {phase.startDate ? __( 'From __date__', { date: phase.startDate.toLocaleDateString() } ) : null}
                        {phase.startDate && phase.endDate ? ' ' : null}
                        {phase.endDate ? __( 'until __date__', { date: phase.endDate.toLocaleDateString() } ) : null}
                    </div>
                </div>
            </div>
            /*)*/ );
    }
}

PhaseRow.propTypes = {
    // Phase
    project:  PropTypes.object.isRequired,
    phase:    PropTypes.object.isRequired,
    // List
    selected: PropTypes.bool,
    onSelect: PropTypes.func.isRequired,
    onEdit:   PropTypes.func.isRequired,
    onRemove: PropTypes.func.isRequired,
    // Drag & Drop
    //connectDragSource: PropTypes.func.isRequired,
    //connectDragPreview: PropTypes.func.isRequired,
    //connectDropTarget: PropTypes.func.isRequired,
    //isDragging: PropTypes.bool.isRequired,
    // Drag & Drop Support
    //moveItem: PropTypes.func.isRequired,
    //restoreItem: PropTypes.func.isRequired
};

PhaseRow.defaultProps = {
    selected: false
};

/**
 * Phase Source
 *
 * @type {{beginDrag, endDrag}}
 */
/*const phaseSource = {

 /!**
 * Begin Drag
 * Return the phase being dragged
 * This is the value carried by the drag operation
 *!/
 beginDrag(props) {
 return { id: props.phase._id };
 },

 /!**
 * End Drag
 * If we were not dropped on any target, restore the original list
 *!/
 endDrag(props, monitor, component) {
 if (!monitor.didDrop()) {
 props.restoreItem();
 }
 }
 };*/

/**
 * Phase Target
 * Used to provide list feedback while dragging the item
 *
 * @type {{canDrop, hover}}
 */
/*const phaseTarget = {

 /!**
 * We can't drop on the dummy target
 *!/
 canDrop() {
 return false;
 },

 /!**
 * Simulate the reordering on hover
 *!/
 hover(props, monitor, component) {
 // Item being dragged over this one
 const item = monitor.getItem();

 // Don't replace items with themselves
 if ( item.id === props.phase._id ) {
 return;
 }

 // Move item "over" props.phase
 props.moveItem( item, props.phase );
 }
 };*/

/*export default _.compose(

 DragSource('phase', phaseSource, (connect, monitor) => ({
 connectDragSource: connect.dragSource(),
 connectDragPreview: connect.dragPreview(),
 isDragging: monitor.isDragging()
 })),

 DropTarget('phase', phaseTarget, connect => ({
 connectDropTarget: connect.dropTarget()
 }))

 )(PhaseRow);*/

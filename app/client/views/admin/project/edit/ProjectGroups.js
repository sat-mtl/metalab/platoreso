"use strict";

import React, { Component, PropTypes } from "react";
import ProjectMethods from "/imports/projects/ProjectMethods";
import GroupSelectionList from '../../../../views/admin/common/GroupSelectionList';

export default React.createClass( {

    propTypes: {
        project: PropTypes.object.isRequired
    },

    addGroup(groupId) {
        ProjectMethods.addGroup.call( { projectId: this.props.project._id, groupId: groupId }, ( err ) => {
            if ( err ) {
                this.setState({error: err});
            }
        });
    },

    removeGroup(groupId) {
        ProjectMethods.removeGroup.call( { projectId: this.props.project._id, groupId: groupId }, ( err ) => {
            if ( err ) {
                this.setState({error: err});
            }
        });
    },

    render() {
        return (
            <section>
                <h3 className="ui dividing header">{__( 'Groups' )}</h3>
                <GroupSelectionList value={this.props.project.groups} onAdd={this.addGroup} onRemove={this.removeGroup}/>
            </section>
        );
    }
} );

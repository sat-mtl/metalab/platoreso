"use strict";

import React, { Component, PropTypes } from "react";
import CardList from '../../card/list/CardList';

export default class ProjectCards extends Component {

    render() {
        const {project} = this.props;

        return (
            <section>
                <h3 className="ui dividing header">{__( 'Cards' )}</h3>
                <CardList project={project}/>
            </section>
        );
    }
}

ProjectCards.propTypes = {
    project: PropTypes.object.isRequired
};
"use strict";

import ProjectMethods from "/imports/projects/ProjectMethods";
import React, {Component, PropTypes} from "react";
import Modal from "../../../../components/semanticui/modules/Modal";
import PhaseRow from "./phases/PhaseRow";
import {PhaseEditModal} from "../../../project/phases/PhaseEdit";
import {PhaseArchiveModal} from "../../../project/phases/PhaseArchive";


export default class ProjectPhases extends Component {

    constructor( props ) {
        super( props );

        // Bind methods, not automatic in current React version when using classes
        this.addPhase              = this.addPhase.bind( this );
        //this.movePhase = this.movePhase.bind(this);
        this.resetPhases           = this.resetPhases.bind( this );
        this.selectPhase           = this.selectPhase.bind( this );
        this.editPhase             = this.editPhase.bind( this );
        this.finishedEditingPhase  = this.finishedEditingPhase.bind( this );
        this.confirmRemovePhase    = this.confirmRemovePhase.bind( this );
        this.finishedRemovingPhase = this.finishedRemovingPhase.bind( this );

        // Initial state comes from props
        this.state = {
            phases:        props.project.phases,
            selectedPhase: null,
            editPhase:     null,
            removePhase:   null
        }
    }

    /**
     * When props change, set the phases as a state since they can be reordered in the list
     * @param nextProps
     */
    componentWillReceiveProps( nextProps ) {
        this.setState( {
            phases: nextProps.project.phases
        } );
    }

    /**
     * Add a phase to the project
     */
    addPhase() {
        this.editPhase( {
            name: __( 'New Phase' )
        }, true );
    }

    /**
     * Move the phase in the state
     * This move operation is only for visual preview
     * We keep the movedPhaseIndex in the state as it's the only place where we can retrieve it later when dropping
     * The real (server-side) move operation will occur when dropping the item
     *
     * @param movedPhase
     * @param atPhase
     */
    /*movePhase(movedPhase, atPhase) {
     const { phases } = this.state;
     const atIndex = _.indexOf(phases, atPhase);
     const movedPhaseIndex = phases.findIndex( phase => phase._id == movedPhase.id );
     this.setState(addons.update(this.state, {
     phases: {
     $splice: [
     [movedPhaseIndex, 1],
     [atIndex, 0, phases[movedPhaseIndex]]
     ]
     },
     movedPhaseIndex: { $set: atIndex }
     }));
     }*/

    /**
     * Reset the phases to the original project's
     */
    resetPhases() {
        this.setState( {
            phases: this.props.project.phases
        } );
    }

    /**
     * Select the phase
     */
    selectPhase( phase ) {
        this.setState( {
            selectedPhase: phase
        } );
    }

    /**
     * Edit the phase
     * First selects it then set state to edit
     *
     * @param {object} phase
     * @param {boolean} isNewPhase Is the phase a new one being created?
     */
    editPhase( phase, isNewPhase = false ) {
        this.setState( {
            selectedPhase: phase,
            editPhase:     phase,
            isNewPhase:    isNewPhase
        } );
    }

    /**
     * Finished editing a phase
     */
    finishedEditingPhase() {
        this.setState( {
            editPhase: null
        } );
    }

    /**
     * Confirm state removal
     * First selects it then set state to confirm
     */
    confirmRemovePhase( phase ) {
        this.setState( {
            selectedPhase: phase,
            removePhase:   phase
        } );
    }

    /**
     * Finished linking cards
     */
    finishedRemovingPhase() {
        this.setState( {
            removePhase:   null,
            selectedPhase: null
        } );
    }

    render() {
        const { project/*, connectDropTarget*/ }                            = this.props;
        const { phases, selectedPhase, editPhase, removePhase, isNewPhase } = this.state;

        return (
            <section className="project-phases">
                <h3 className="ui dividing header">{__( 'Phases' )}</h3>

                <div className="ui tiny buttons">
                    <button className="ui green right labeled icon button" onClick={this.addPhase}>
                        <i className="add icon"/>
                        {__( 'Add Phase' )}
                    </button>
                </div>

                <div className="ui hidden divider"></div>

                { /*connectDropTarget(*/
                    <div className="ui middle aligned divided selection list">
                        { phases ? phases.map( phase => (
                            <PhaseRow key={phase._id}
                                      project={project}
                                      phase={phase}
                                      restoreItem={this.resetPhases}
                                      selected={selectedPhase && selectedPhase._id == phase._id}
                                      onSelect={this.selectPhase }
                                      onEdit={this.editPhase }
                                      onRemove={this.confirmRemovePhase}/>
                        ) ) : null }
                    </div>
                    /*)*/ }

                {/* Edit Modal */}
                <PhaseEditModal project={project} phase={editPhase} isNewPhase={isNewPhase} onRequestClose={this.finishedEditingPhase}/>

                {/* Archive Phase Modal */}
                <PhaseArchiveModal project={project}
                                   phase={removePhase}
                                   onRequestClose={this.finishedRemovingPhase}/>

                {/* Remove Modal */}
                {/*<Modal
                    opened={!!removePhase}
                    onApprove={this.removePhase}
                    onHide={()=>this.setState( { removePhase: null } )}>
                    <i className="close icon"/>
                    <div className="header">
                        {__( 'Remove Project Phase' )}
                    </div>
                    <div className="image content">
                        <div className="image">
                            <i className="inbox icon"/>
                        </div>
                        <div className="description">
                            <p>{__( 'Are you sure you want to remove the project phase __phase__?', { phase: removePhase ? removePhase.name : null } )}</p>
                        </div>
                    </div>
                    <div className="actions">
                        <div className="ui buttons">
                            <div className="ui red deny button">
                                <i className="remove icon"/>
                                {__( 'No' )}
                            </div>
                            <div className="ui green approve button">
                                <i className="checkmark icon"/>
                                {__( 'Yes' )}
                            </div>
                        </div>
                    </div>
                </Modal>*/}

            </section>
        );
    }
}

ProjectPhases.propTypes = {
    project: PropTypes.object.isRequired
    //connectDropTarget: PropTypes.func.isRequired
};

/*const phaseTarget = {

 /!**
 * Drop
 * Call the method to do the actual reordering
 *!/
 drop(props, monitor, component) {
 ProjectMethods.movePhase.call( { projectId: props.project._id, phaseId: monitor.getItem().id, index: component.state.movedPhaseIndex });
 }
 };

 export default _.compose(

 DropTarget('phase', phaseTarget, connect => ({
 connectDropTarget: connect.dropTarget()
 }))

 )(ProjectPhases);*/

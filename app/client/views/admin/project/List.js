"use strict";

import ProjectList from './list/ProjectList';

import React, { Component, PropTypes } from "react";

export default class Projects extends Component {

    render() {
        const {router} = this.context;

        return (
            <section id="admin-project-list" className="sixteen wide column admin-project-list">
                <header className="ui clearing basic segment">
                    <h2 className="ui left floated header">
                        {__( 'Projects' )}
                        <div className="sub header">{__( 'Manage projects.' )}</div>
                    </h2>
                    <div className="ui right floated secondary menu">
                        <button className="ui green right labeled icon button" onClick={() => router.push( { pathname: '/admin/project/create' } )}>
                            <i className="add icon"/>
                            {__( 'Add Project' )}
                        </button>
                    </div>
                </header>
                <ProjectList/>
            </section>
        );
    }
}

Projects.contextTypes = {
    router: PropTypes.object.isRequired
};
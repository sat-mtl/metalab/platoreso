"use strict";

import React, { Component, PropTypes } from "react";
import {Link} from 'react-router';
import ProjectCollection from "/imports/projects/ProjectCollection";
import ProjectImagesCollection from "/imports/projects/ProjectImagesCollection";

import {ConnectMeteorData} from '../../../lib/ReactMeteorData';

import Image from '../../widgets/Image';
import NotFound from '../../errors/NotFound';

class Edit extends React.Component {

    render() {
        const {project, projectImage} = this.props;

        if ( !project ) {
            return <NotFound/>;
        } else {
            return (
                <section id="admin-project-edit" className="sixteen wide column admin-project-edit">
                    <header>
                        <h2 className="ui header">
                            <Image image={ projectImage }
                               size="thumb"
                               className="small circular bullseye"/>

                            <div className="content">
                                <Link to={'/project/' + (project.slug||project._id) }>{project.name}</Link>
                                <div className="sub header">{__( 'Edit project.' )}</div>
                            </div>
                        </h2>
                    </header>
                    <div className="ui stackable grid">
                        <div className="four wide tablet three wide computer column">
                            <div className="ui secondary vertical fluid pointing menu">
                                <Link to={'/admin/project/' + project._id } onlyActiveOnIndex={true} className="item" activeClassName="active">
                                    {__( 'Information' )}
                                </Link>
                                <Link to={'/admin/project/' + project._id + '/picture'} className="item" activeClassName="active">
                                    {__( 'Picture' )}
                                </Link>
                                <Link to={'/admin/project/' + project._id + '/page'} className="item" activeClassName="active">
                                    {__( 'Page' )}
                                </Link>
                                <Link to={'/admin/project/' + project._id + '/groups'} className="item" activeClassName="active">
                                    {__( 'Groups' )}
                                </Link>
                                <Link to={'/admin/project/' + project._id + '/phases'} className="item" activeClassName="active">
                                    {__( 'Phases' )}
                                </Link>
                                <Link to={'/admin/project/' + project._id + '/cards'} className="item" activeClassName="active">
                                    {__( 'Cards' )}
                                </Link>
                            </div>
                        </div>
                        <div className="twelve wide tablet thirteen wide computer stretched column">
                            { React.Children.map(this.props.children, child => React.cloneElement( child, { project: project } ) ) }
                        </div>
                    </div>
                </section>
            );
        }
    }
}


export default ConnectMeteorData( props => ({
    projectLoading: !Meteor.subscribe( 'admin/project/edit', props.params.id ),
    project:      ProjectCollection.findOne( props.params.id ),
    projectImage: ProjectImagesCollection.findOne( { owners: props.params.id } )
}))(Edit);
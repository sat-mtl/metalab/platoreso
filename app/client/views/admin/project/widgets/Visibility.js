"use strict";

import React, { Component, PropTypes } from "react";

export default React.createClass( {

    propTypes: {
        project: PropTypes.object.isRequired
    },

    renderVisibility() {
        if ( this.props.project.public ) {
            return (<span className="ui green horizontal label">{__( 'Public' )}</span>);
        } else {
            return (<span className="ui red horizontal label">{__('Private')}</span>);
        }
    },

    render() {
        return (
            <span className="visibility">
                { this.renderVisibility() }
            </span>
        );
    }
} );
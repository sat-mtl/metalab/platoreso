"use strict";

import React, { Component, PropTypes } from "react";
import {Link} from 'react-router';
import Feed from '../feed/Feed';
import Chart from '../../components/chartjs/Chart';

export default class Index extends Component {

    render() {
        return (
            <section id="dashboard" className="sixteen wide column dashboard">
                <header>
                    <h2 className="ui header">
                        {__( 'Administration' )}
                        <div className="sub header">{__( 'Dashboard' )}</div>
                    </h2>
                </header>
                <div className="ui stackable grid">
                    <div className="sixteen wide column">
                        <Feed internal={true} showPreviews={false}/>
                    </div>
                </div>
            </section>
        );
    }
}

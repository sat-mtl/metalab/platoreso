"use strict";

import UserList from './list/UserList';

import React, { Component, PropTypes } from "react";

export default class Users extends Component {

    render() {
        const {router} = this.context;

        return (
            <section id="admin-user-list" className="sixteen wide column admin-user-list">
                <header className="ui clearing basic segment">
                    <h2 className="ui left floated header">
                        {__( 'User Accounts' )}
                        <div className="sub header">{__( 'Manage account settings.' )}</div>
                    </h2>
                    <div className="ui right floated secondary menu">
                        <button className="ui green right labeled icon button" onClick={() => router.push( { pathname: '/admin/user/create' } )}>
                            <i className="add icon"/>
                            {__( 'Add User' )}
                        </button>
                    </div>
                </header>
                <UserList/>
            </section>
        );
    }
}

Users.contextTypes = {
    router: PropTypes.object.isRequired
};
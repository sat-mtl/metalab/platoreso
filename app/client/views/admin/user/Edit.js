"use strict";

import React, { Component, PropTypes } from "react";
import { Link } from 'react-router';
import classNames from "classnames";
import UserHelpers from "/imports/accounts/UserHelpers";
import { ConnectMeteorData } from '../../../lib/ReactMeteorData';
import NotFound from '../../errors/NotFound';
import Avatar from '../../widgets/Avatar';
import Roles from '../../widgets/RoleLabels';

class Edit extends Component {

    render() {
        const { user } = this.props;

        if ( !user ) {
            return <NotFound/>;
        } else {
            const verified = UserHelpers.hasVerifiedEmail( user );

            return (
                <section id="admin-user-edit" className="sixteen wide column admin-user-edit">
                    <header>
                        <h2 className="ui header">
                            <Avatar user={user} className="small"/>
                            <div className="content">
                                <Link to={"/profile/" + user._id}>{user.profile.firstName + ' ' + user.profile.lastName}</Link>
                                <div className="sub header">
                                <div className="ui mini horizontal labels">
                                    <div className={classNames( "ui label", { green: user.enabled, red: !user.enabled } )}>
                                        <i className={ classNames( 'icon', { checkmark: user.enabled, remove: !user.enabled } ) }/>
                                        { user.enabled ? __( 'Enabled' ) : __( 'Disabled' )}
                                    </div>
                                    <div className={classNames( "ui label", { green: verified, red: !verified } )}>
                                        <i className={ classNames( 'icon', { checkmark: verified, remove: !verified } ) }/>
                                        { verified ? __( 'Verified' ) : __( 'Not Verified' )}
                                    </div>
                                    <div className={classNames( "ui label", { green: user.completed, red: !user.completed } )}>
                                        <i className={ classNames( 'icon', { checkmark: user.completed, remove: !user.completed } ) }/>
                                        { user.completed ? __( 'Completed' ) : __( 'Pending' )}
                                    </div>
                                    { user.moderated &&
                                      <div className="ui orange label">
                                          <i className="legal icon"/>
                                          { __( 'Moderated' )}
                                      </div> }
                                </div>
                                <Roles user={user}/>
                                </div>
                            </div>
                        </h2>
                    </header>
                    <div className="ui stackable grid">
                        <div className="four wide tablet three wide computer column">
                            <div ref="tabs" className="ui secondary vertical fluid pointing menu">
                                <Link to={'/admin/user/' + user._id } onlyActiveOnIndex={true} className="item" activeClassName="active">
                                    {__( 'Personal Information' )}
                                </Link>
                                <Link to={'/admin/user/' + user._id + '/avatar'} className="item" activeClassName="active">
                                    {__( 'Avatar' )}
                                </Link>
                                <Link to={'/admin/user/' + user._id + '/profile'} className="item" activeClassName="active">
                                    {__( 'Profile' )}
                                </Link>
                                <Link to={'/admin/user/' + user._id + '/roles'} className="item" activeClassName="active">
                                    {__( 'Roles' )}
                                </Link>
                                <Link to={'/admin/user/' + user._id + '/cards'} className="item" activeClassName="active">
                                    {__( 'Cards' )}
                                </Link>
                                <Link to={'/admin/user/' + user._id + '/comments'} className="item" activeClassName="active">
                                    {__( 'Comments' )}
                                </Link>
                            </div>
                        </div>
                        <div className="twelve wide tablet thirteen wide computer stretched column">
                            { React.Children.map( this.props.children, child => React.cloneElement( child, { user: user } ) ) }
                        </div>
                    </div>
                </section>
            );
        }
    }
}

export default ConnectMeteorData( props => ({
    userLoading: !Meteor.subscribe( 'admin/user/edit', props.params.id ),
    user:        Meteor.users.findOne( props.params.id )
}) )( Edit );

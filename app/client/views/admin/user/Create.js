"use strict";

import { RoleList } from "/imports/accounts/Roles";
import UserMethods from "/imports/accounts/UserMethods";
import { unflatten } from 'flat';
import Form from '../../../components/forms/Form';
import Field from '../../../components/forms/Field';
import Input from '../../../components/forms/Input';
import Select from '../../../components/forms/Select';
import GroupSelection from './edit/roles/GroupSelection';

import React, { Component, PropTypes } from "react";
import classNames from "classnames";

export default class Create extends Component {

    constructor( props ) {
        super( props );

        this.resetCreationForm = this.resetCreationForm.bind( this );
        this.createUser        = this.createUser.bind( this );

        this.state = {
            created:  false,
            creating: false,
            error:    null
        }
    }

    resetCreationForm() {
        this.setState( {
            created:  false,
            creating: false
        } );
    }

    createUser( values ) {
        if ( this.state.creating ) {
            return;
        }

        this.setState( {error: null, creating: true} );

        UserMethods.invite( unflatten( values ), ( err ) => {
            if ( err ) {
                this.setState( {error: err} );
            } else {
                this.setState( {created: true} );
            }
            this.setState( {creating: false} );
        } );
    }

    render() {
        if ( this.state.created ) {
            return (
                <section id="admin-user-created" className="sixteen wide column admin-user-created">
                    <header>
                        <h2 className="ui center aligned header">
                            {__( 'User created' )}
                            <div className="sub header">{__( 'Invitation email sent.' )}</div>
                        </h2>
                    </header>
                    <div className="ui centered grid">
                        <div className="sixteen wide center aligned column">
                            {__( 'An email was sent to the address you provided containing a link for the user to activate their account.' )}
                        </div>
                        <div className="twelve wide mobile six wide column">
                            <a className="ui button grey fluid"
                               onClick={this.resetCreationForm}>
                                {__( 'Create another user' )}
                            </a>
                        </div>
                    </div>
                </section>
            );
        } else {
            return (
                <section id="admin-user-create" className="sixteen wide column admin-user-create">
                    <header>
                        <h2 className="ui center aligned icon header">
                            <i className="grey add user icon"/>
                            <div className="content">
                                {__( 'New User' )}
                                <div className="sub header">{__( 'User creation form.' )}</div>
                            </div>
                        </h2>
                    </header>
                    <div className="ui centered grid">
                        <div className="twelve wide mobile six wide column">
                            <Form id="user-creation-form" ref="userCreationForm"
                                  error={this.state.error}
                                  onSuccess={this.createUser}>

                                <div className="two fields">
                                    <Field label={__("First Name")} className="required">
                                        <Input ref="profile.firstName" name="profile.firstName" type="text"
                                               rules={[
                                                           { type: 'empty', prompt: __('Please enter a first name') }
                                                       ]}/>
                                    </Field>
                                    <Field label={__("Last Name")} className="required">
                                        <Input ref="profile.lastName" name="profile.lastName" type="text"
                                               rules={[
                                                           { type: 'empty', prompt: __('Please enter a last name') }
                                                       ]}/>
                                    </Field>
                                </div>

                                <Field label={__("Email")} className="required">
                                    <div className="ui fluid input">
                                        <Input ref="email" name="email" type="email"
                                               rules={[
                                                       { type: 'empty', prompt: __('Please enter an email') },
                                                       { type: 'email', prompt: __('Please enter a valid email' )}
                                                   ]}/>
                                    </div>
                                </Field>

                                <Field label={__("Main role")}>
                                    <Select name="roles"
                                            multiple={true}
                                            className="fluid"
                                            defaultValue={[RoleList.user]}
                                            rules={[
                                                   { type: 'empty', prompt: __('Please select at least one role') }
                                                ]}>
                                        { _.map( RoleList, role => {
                                            const labelKey = 'role.' + role;
                                            return (<option key={role} value={role}>{__( `${labelKey}` )}</option>)
                                        } ) }
                                    </Select>
                                </Field>

                                <GroupSelection/>

                                <div className="ui divider"></div>
                                <div className={classNames("ui green large right labeled icon submit button", {loading: this.state.creating, disabled: this.state.creating})}>
                                    <i className="save icon"/>
                                    {__( 'Send invitation email' )}
                                </div>

                            </Form>

                        </div>
                    </div>
                </section>
            );
        }
    }
}



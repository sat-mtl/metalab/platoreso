"use strict";

import React, {Component, PropTypes} from "react";
import {ConnectMeteorData} from "../../../lib/ReactMeteorData";

/**
 * Wrapper component keeping the user list subscription alive
 */
export class Users extends Component {
    render() {
        return this.props.children;
    }
}

export default ConnectMeteorData( props => {
    return {
        loading: !Meteor.subscribe( 'admin/user/list' ).ready()
    }
} )( Users );
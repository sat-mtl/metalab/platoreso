"use strict";

import React, { Component, PropTypes } from "react";
import CardList from '../../card/list/CardList';

export default class UserCards extends Component {

    render() {
        const {user} = this.props;

        return (
            <section>
                <h3 className="ui dividing header">{__( 'Cards' )}</h3>
                <CardList author={user}/>
            </section>
        )
    }
}

UserCards.propTypes = {
    user: PropTypes.object.isRequired
};
"use strict";

import GroupImagesCollection from "/imports/groups/GroupImagesCollection";
import { GroupRoles } from "/imports/groups/GroupRoles";

import {Link} from 'react-router';
import {ConnectMeteorData} from '../../../../../lib/ReactMeteorData';
import Image from '../../../../../views/widgets/Image';
import Select from '../../../../../components/forms/Select';

import React, { Component, PropTypes } from "react";

class PermissionRow extends Component {

    constructor(props) {
        super(props);
        this.remove = this.remove.bind(this);
    }

    remove() {
        this.props.onRemove(this.props.permission);
    }

    render() {
        const { permission, image } = this.props;
        const { group, roles } = permission;

        if ( !group ) {
            return null;
        }

        return (
            <tr>
                <td><Image image={image} size="thumb" className="circular group avatar"/></td>
                <td><Link to={'/admin/group/' + group._id}>{group.name}</Link></td>
                <td>
                    <Select id={"groups." + permission._id} name={"groups." + permission._id}
                            multiple={true}
                            className="fluid"
                            defaultValue={roles}>
                        { GroupRoles.map(role => <option key={role} value={role}>{role}</option>) }
                    </Select>
                </td>
                <td className="right aligned">
                    <div ref="actions" className="ui mini buttons">
                        <div className="ui compact red icon button"
                                onClick={this.remove}
                                data-content={__('Remove all group roles')}
                                data-variation="inverted">
                            <i className="remove icon"/>
                        </div>
                    </div>
                </td>
            </tr>
        );
    }
}

PermissionRow.propTypes = {
    permission: PropTypes.object.isRequired,
    onRemove: PropTypes.func.isRequired
};

/**
 * Meteor Data Wrapper
 */
const PermissionRowComponent = ConnectMeteorData( props => ({
    image: GroupImagesCollection.findOne( { owners: props.permission.group._id } )
}))(PermissionRow);

export default PermissionRowComponent;
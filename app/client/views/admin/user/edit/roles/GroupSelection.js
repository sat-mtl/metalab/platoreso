"use strict";

import GroupCollection from "/imports/groups/GroupCollection";
import { GROUP_PREFIX } from "/imports/groups/GroupRoles";

import Immutable from 'immutable';
import {ConnectMeteorData} from '../../../../../lib/ReactMeteorData';
import Modal from '../../../../../components/semanticui/modules/Modal';
import GroupPicker from '../../../../admin/common/GroupPicker';
import PermissionRow from './PermissionRow';

import UserHelpers from "/imports/accounts/UserHelpers";
import React, { Component, PropTypes } from "react";

/**
 * Immutable.js record to hold what is considered a "permission" in the ui
 */
const PermissionRecord = Immutable.Record( {
    __new: false,
    _id: null,
    prefix: null,
    group: null,
    roles: []
} );

class GroupSelection extends Component {

    /**
     * Get a map of permissions from the props
     *
     * @returns {Immutable.Map<PermissionRecord>}
     */
    static getPermissionsFromProps( { user, groups, userRoleGroups } ) {

        // Build a permission records map from the properties
        return Immutable.Map(
            ( userRoleGroups || [] )
            // Split on the colon to isolate the prefix from the id
                .map( userRoleGroup => ({ _id: userRoleGroup, parts: userRoleGroup.split( '_' ) }) )
                // Keep only group prefixes, we might support more than groups later on
                .filter( permission => permission.parts[0] === GROUP_PREFIX )
                // Retrieve the actual group from the id
                .map( permission => {
                    permission.group = groups.find( group => group._id == permission.parts[1] )
                    return permission;
                } )
                // Remove any null groups
                .filter( permission => permission.group != null )
                // Create record map using the group, and the roles the user has for that group
                .map( permission => {
                    permission.prefix = permission.parts[0];
                    permission.roles = UserHelpers.getGroupRolesForUser( user._id, GROUP_PREFIX + '_' + permission.group._id );
                    return [permission._id, new PermissionRecord( permission )];
                } ) );
    }

    constructor(props) {
        super(props);

        this.addPermission = this.addPermission.bind(this);
        this.confirmRemovePermission = this.confirmRemovePermission.bind( this );
        this.removePermission = this.removePermission.bind(this);
        this.removePermissionConfirmed = this.removePermissionConfirmed.bind(this);

        this.state = {
            permissions: props.user ? GroupSelection.getPermissionsFromProps( props ) : Immutable.Map(),
            // The permission to be removed when showing the remove modal
            permissionPendingRemoval: null,
            // The removed permission ids
            removedPermissions: Immutable.List()
        }
    }

    /**
     * Add a permission set for a group
     */
    addPermission(groupId) {
        const permissionGroupId = GROUP_PREFIX + '_' + groupId;
        const permission = new PermissionRecord( {
            __new: true,
            _id: permissionGroupId,
            prefix: GROUP_PREFIX,
            group: this.props.groups.find( group => group._id == groupId ),
            roles: this.props.user ? UserHelpers.getRolesForUser( this.props.user._id, permissionGroupId ) : []
        } );

        let state = {
            permissions: this.state.permissions.set( permission._id, permission )
        };

        // Make sure we don't keep it in the removed permissions is case we are re-adding a previously removed one
        let index = this.state.removedPermissions.indexOf( permission._id );
        if ( index != -1 ) {
            state.removedPermissions = this.state.removedPermissions.delete( index );
        }

        // Set the state
        this.setState( state );
    }

    /**
     * Confirm the removal of a permission
     * It just sets the state so that the modal can open itself
     */
    confirmRemovePermission( permission ) {
        if ( this.props.user ) {
            this.setState( {
                permissionPendingRemoval: permission
            } );
        } else {
            this.removePermission(permission);
        }
    }

    removePermissionConfirmed() {
        this.removePermission(this.state.permissionPendingRemoval)
    }

    /**
     * Remove a permission
     * Called by the modal's confirmation
     *
     * @param {Object} [permission]
     */
    removePermission(permission) {
        if ( !permission ) {
            return;
        }

        let state = {
            permissions: this.state.permissions.delete( permission._id ),
            permissionPendingRemoval: null
        };

        // Only keep removed questions that weren't new
        // The server doesn't care about permissions that were never saved
        if ( !permission.__new ) {
            state.removedPermissions = this.state.removedPermissions.push( permission._id )
        }

        // Set the state
        this.setState( state );
    }

    componentWillReceiveProps( nextProps ) {
        // When props change, rebuild the permissions map
        // FIXME: This has the potential to reset the current changes if we receive a reactive change from meteor
        // TODO: Keep our changes separate from the list representing the "truth"
        if (!_.isEqual(nextProps, this.props)) {
            this.setState( {
                permissions: GroupSelection.getPermissionsFromProps( nextProps )
            } );
        }
    }

    render() {
        const { permissions, removedPermissions, permissionPendingRemoval } = this.state;

        return (
            <div>
                <h3 className="ui dividing header">{__( 'Groups' )}</h3>

                {/* Removed Permissions */}
                { removedPermissions.map( permission => (
                    <input type="hidden"
                           name="removedPermissions[]"
                           key={permission}
                           value={permission}/>
                ) ) }

                <GroupPicker
                    value={permissions.filter(permission=>permission.prefix==GROUP_PREFIX).map(permission=>permission.group._id).toArray()}
                    onAdd={this.addPermission}/>

                <div className="ui hidden divider"></div>

                {/* Permissions */}
                <table className="ui very basic small selectable table">
                    <thead>
                    <tr>
                        <th colSpan="2">{__( 'Group Name' )}</th>
                        <th>{__( 'Roles' )}</th>
                        <th className="right aligned">{__( 'Actions' )}</th>
                    </tr>
                    </thead>
                    <tbody>
                    { permissions.filter(permission=>permission.prefix==GROUP_PREFIX).toArray().map( permission => (
                        <PermissionRow key={permission._id}
                                       permission={permission}
                                       onRemove={this.confirmRemovePermission}/>
                    ) ) }
                    </tbody>
                </table>

                {/* Remove Modal */}
                <Modal
                    opened={!!permissionPendingRemoval}
                    onApprove={this.removePermissionConfirmed}
                    onHide={()=>this.setState({permissionPendingRemoval:null})}>
                    <i className="close icon"/>

                    <div className="header">
                        {__( 'Remove Permission' )}
                    </div>
                    <div className="image content">
                        <div className="image">
                            <i className="group icon"/>
                        </div>
                        <div className="description">
                            <p>{__( 'Are you sure you want to remove the permissions for group __group__?', { group: permissionPendingRemoval ? permissionPendingRemoval.group.name : null } )}</p>
                        </div>
                    </div>
                    <div className="actions">
                        <div className="ui buttons">
                            <div className="ui red deny button">
                                <i className="remove icon"/>
                                {__( 'No' )}
                            </div>
                            <div className="ui green approve button">
                                <i className="checkmark icon"/>
                                {__( 'Yes' )}
                            </div>
                        </div>
                    </div>
                </Modal>
            </div>
        );
    }

}

GroupSelection.propTypes = {
    user: PropTypes.object // Can be omitted when creating a new user
};

export default ConnectMeteorData( props => ( {
    groupsLoading: !Meteor.subscribe( 'admin/group/list' ).ready(),
    groups: GroupCollection.find( {} ).fetch(),
    userRoleGroups: props.user ? UserHelpers.getGroupsForUser( props.user._id ) : []
} ) )( GroupSelection );
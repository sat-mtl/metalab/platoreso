"use strict";

import React, { Component, PropTypes } from "react";
import classNames from "classnames";
import { unflatten } from 'flat';
import UserHelpers from "/imports/accounts/UserHelpers";
import { RoleList } from "/imports/accounts/Roles";
import UserMethods from "/imports/accounts/UserMethods";
import { ConnectMeteorData } from '../../../../lib/ReactMeteorData';
import Form from '../../../../components/forms/Form';
import Field from '../../../../components/forms/Field';
import Select from '../../../../components/forms/Select';
import GroupSelection from './roles/GroupSelection';

/**
 * User Roles administration screen
 */
class UserRoles extends Component {

    constructor( props ) {
        super( props );

        this.updateRoles = this.updateRoles.bind( this );

        this.state = {
            saving: false,
            error:  null
        }
    }

    /**
     * Update the roles/permissions
     */
    updateRoles( values ) {
        if ( this.state.saving ) {
            return;
        }
        this.setState( { saving: true } );
        UserMethods.updatePermissions( unflatten( values ), err => {
            if ( err ) {
                this.setState( { error: err } );
            }
            this.setState( { saving: false } );
        } );
    }

    render() {
        const { user, userRoles } = this.props;
        const editingItself = (Meteor.userId() == user._id);

        return (
            <section>
                <h3 className="ui dividing header">{__( 'Roles' )}</h3>

                <Form id="user-roles-form" ref="userRolesForm"
                      error={this.state.error}
                      onSuccess={this.updateRoles}>
                    <input type="hidden" name="_id" value={user._id}/>

                    <Field label={__( "Main role" )}>
                        <Select name="roles"
                                multiple={true}
                                defaultValue={userRoles}
                                rules={[
                                    { type: 'empty', prompt: __( 'Please select at least one role' ) }
                                ]}
                                className={classNames( 'fluid', { disabled: editingItself } )}>
                            { _.map( RoleList, role => {
                                const labelKey = 'role.' + role;
                                return (<option key={role} value={role}>{__( `${labelKey}` )}</option>)
                            } ) }
                        </Select>
                        { editingItself
                            ? (
                              <div className="ui info message">
                                  <div className="header">{__( 'You cannot change your own role!' )}</div>
                                  <ul className="list">
                                      <li>{__( 'As an admin, you cannot demote yourself from your role, this would have the potential of rendering the system unusable if you were to be the last admin and none remained after saving.' )}</li>
                                  </ul>
                              </div>
                          )
                            : null }
                    </Field>

                    <GroupSelection user={user}/>

                    <div className="ui divider"></div>
                    <div className={classNames( "ui green large right labeled icon submit button", { loading: this.state.saving, disabled: this.state.saving } )}>
                        <i className="save icon"/>
                        {__( 'Save' )}
                    </div>
                </Form>
            </section>
        );
    }
}

UserRoles.propTypes = {
    user: PropTypes.object.isRequired
};

/**
 * Meteor Data Wrapper
 */
const UserRolesComponent = ConnectMeteorData( props => {
    return {
        userRoles: UserHelpers.getRolesForUser( props.user._id, UserHelpers.GLOBAL_GROUP )
    }
} )( UserRoles );

export default UserRolesComponent;
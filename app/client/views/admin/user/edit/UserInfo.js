"use strict";

import UserMethods from "/imports/accounts/UserMethods";
import { unflatten } from 'flat';
import formDiff from "/imports/utils/formDiff";
import i18n from "/imports/i18n/i18n";
import React, { Component, PropTypes } from "react";
import classNames from "classnames";
import Form from '../../../../components/forms/Form';
import Field from '../../../../components/forms/Field';
import Input from '../../../../components/forms/Input';
import TextArea from '../../../../components/forms/Textarea';
import CheckBox from '../../../../components/forms/Checkbox';
import Select from '../../../../components/forms/Select';
import ImageUploader from '../../../../components/forms/ImageUploader';

export default class UserInfo extends Component {

    constructor( props ) {
        super( props );

        this.sendValidationEmail = this.sendValidationEmail.bind( this );
        this.updateProfile       = this.updateProfile.bind( this );

        this.state = {
            error:                    null,
            saving:                   false,
            sendingVerificationEmail: false
        }
    }

    sendValidationEmail() {
        this.setState( { sendingVerificationEmail: true } );
        UserMethods.sendValidationEmail( this.props.user._id, err => {
            if ( err ) {
                this.setState( { error: err } );
            }
            this.setState( { sendingVerificationEmail: false } );
        } );
    }

    updateProfile( values ) {
        if ( this.state.saving ) {
            return;
        }
        this.setState( { saving: true } );

        // Get boolean value instead of the "on" string
        values.enabled = this.refs.enabled.getValue();

        // Turn the form values into a proper object and compute diff
        const changes = formDiff( unflatten( values ), this.props.user, ['_id'] );

        UserMethods.update( changes, err => {
            if ( err ) {
                this.setState( { error: err } );
            }
            this.setState( { saving: false } );
        } )
    }

    render() {
        const { user }   = this.props;
        const { saving } = this.state;

        return (
            <section>
                <h3 className="ui dividing header">{__( 'Personal Information' )}</h3>

                <Form id="user-edit-form" ref="userEditForm"
                      error={this.state.error}
                      onSuccess={this.updateProfile}>
                    <input type="hidden" name="_id" value={user._id}/>

                    <div className="two fields">
                        <Field label={__( "First Name" )} className="required">
                            <Input ref="profile.firstName" name="profile.firstName" type="text"
                                   defaultValue={user.profile.firstName}
                                   rules={[
                                       { type: 'empty', prompt: __( 'Please enter a first name' ) }
                                   ]}/>
                        </Field>
                        <Field label={__( "Last Name" )} className="required">
                            <Input ref="profile.lastName" name="profile.lastName" type="text"
                                   defaultValue={user.profile.lastName}
                                   rules={[
                                       { type: 'empty', prompt: __( 'Please enter a last name' ) }
                                   ]}/>
                        </Field>
                    </div>
                    <Field label={__( "Email" )} className="required">
                        <Input ref="email" name="email" type="email"
                               defaultValue={user.emails && user.emails[0] ? user.emails[0].address : ''}
                               className={ classNames( { 'right labeled': user.emails && user.emails[0] } ) }
                               rules={[
                                   { type: 'empty', prompt: __( 'Please enter an email' ) },
                                   { type: 'email', prompt: __( 'Please enter a valid email' ) }
                               ]}
                               after={(() => {
                                   if ( user.emails && user.emails[0] ) {
                                       if ( user.emails[0].verified ) {
                                           return (<div className="ui green horizontal label">{__( 'Verified' )}</div>);
                                       } else {
                                           return (
                                               <div className="ui orange horizontal label">{__( 'Unverified' )}</div>);
                                       }
                                   } else {
                                       return null;
                                   }
                               })()}/>

                    </Field>

                    {(user.emails && user.emails[0] && !user.emails[0].verified)
                        ? (<Field>
                        <div className={classNames( "ui button", { loading: this.state.sendingVerificationEmail } )} onClick={this.sendValidationEmail}>{__( 'Resend invitation email' )}</div>
                    </Field>)
                        : null
                    }

                    <Field label={__( "Language" )}>
                        <Select ref="profile.language" name="profile.language" className="search"
                                defaultValue={user.profile.language || 'en'}
                                rules={[
                                    { type: 'empty', prompt: __( 'Please choose a language' ) }
                                ]}>
                            { _.map( i18n.getLanguages(), ( lang, key ) => (
                                <option key={key} value={key}>{lang.name}</option>
                            ) ) }
                        </Select>
                    </Field>

                    <Field label={__( "Account Status" )}>
                        <CheckBox ref="enabled" name="enabled" defaultChecked={user.enabled} label={__( 'Enabled' )}/>
                    </Field>

                    <div className="ui divider"></div>
                    <div className={classNames( "ui green large right labeled icon submit button", { loading: saving, disabled: saving } )}>
                        <i className="save icon"/>
                        {__( 'Save' )}
                    </div>
                </Form>
            </section>
        );
    }
}

UserInfo.propTypes = {
    user: PropTypes.object.isRequired
};

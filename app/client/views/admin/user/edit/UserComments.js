"use strict";

import React, { Component, PropTypes } from "react";
import CommentList from '../../comment/list/CommentList';

export default class UserComments extends Component {

    render() {
        const {user} = this.props;

        return (
            <section>
                <h3 className="ui dividing header">{__( 'Comments' )}</h3>
                <CommentList author={user}/>
            </section>
        )
    }
}

UserComments.propTypes = {
    user: PropTypes.object.isRequired
};
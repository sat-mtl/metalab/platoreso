"use strict";

import React, {Component, PropTypes} from "react";
import AvatarCollection from "/imports/accounts/AvatarCollection";
import {ConnectMeteorData} from "../../../../lib/ReactMeteorData";
import ImageUploader from "../../../../components/forms/ImageUploader";

export class UserPicture extends Component {

    render() {
        const { user, avatar } = this.props;

        return (
            <section id="userPicture">
                <h3 className="ui dividing header">{__( 'Avatar' )}</h3>
                <ImageUploader collection={AvatarCollection}
                               file={avatar}
                               owner={user._id}/>
            </section>
        );
    }
}

UserPicture.propTypes = {
    user:   PropTypes.object.isRequired,
    avatar: PropTypes.object
};

export default ConnectMeteorData( props => {
    Meteor.subscribe( 'admin/avatar/edit', props.user._id );
    return {
        avatar: AvatarCollection.findOne( { owners: props.user._id } )
    };
} )( UserPicture );
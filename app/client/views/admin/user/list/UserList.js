"use strict";

import React, { Component, PropTypes } from "react";
import slugify from "underscore.string/slugify";
import escapeStringRegexp from "escape-string-regexp";
import { Counts } from "meteor/tmeasday:publish-counts";
import UserCollection from "/imports/accounts/UserCollection";
import UserHelpers from "/imports/accounts/UserHelpers";
import UserMethods from "/imports/accounts/UserMethods";
import { ConnectMeteorData } from "../../../../lib/ReactMeteorData";
import BaseList, { BaseTable } from "../../../common/table/BaseList";
import UserRow from "./UserRow";

export default class UserList extends BaseList {

    constructor( props ) {
        super( props );
        this.tableComponent = UserTable;
        this.state          = _.extend( this.state, {
            orderBy: 'profile.firstName_sort'
        } );
    }

    onEdit( user ) {
        this.context.router.push( { pathname: '/admin/user/' + user._id } );
    }

    remove( user ) {
        UserMethods.remove( user._id, ( err ) => {
            if ( err ) {
                this.setState( { error: err } );
            }
        } );
    }

    getHeadersFromProps( props ) {
        let headers = super.getHeadersFromProps( props );
        headers.push( { field: 'profile.firstName_sort', label: __( 'First Name' ), colSpan: 2 } );
        headers.push( { field: 'profile.lastName_sort', label: __( 'Last Name' ) } );
        headers.push( { field: 'emails.address', label: __( 'Email' ) } );
        headers.push( { field: 'points', label: __( 'Points' ) } );
        headers.push( { label: __( 'Roles' ) } );
        headers.push( { field: 'enabled', label: __( 'Enabled' ), className: 'center aligned' } );
        headers.push( { field: 'emails.verified', label: __( 'Verified' ), className: 'center aligned' } );
        headers.push( { field: 'completed', label: __( 'Completed' ), className: 'center aligned' } );
        headers.push( { field: 'moderated', label: __( 'Moderated' ), className: 'center aligned' } );
        headers.push( { label: __( 'Services' ) } );
        headers.push( { field: 'createdAt', label: __( 'Created' ) } );
        headers.push( { field: 'updatedAt', label: __( 'Modified' ) } );
        return headers;
    }

    renderRow( user ) {
        const { selectedEntity: selectedUser } = this.state;
        return (
            <UserRow key={user._id}
                     entity={user}
                     selected={selectedUser && selectedUser._id == user._id}
                     onSelect={this.onSelect}
                     onEdit={this.onEdit}
                     onRemove={this.confirmRemove}/>
        );
    }

    renderRemoveModal( user ) {
        return (
            <div className="image content">
                <div className="image">
                    <i className="user icon"/>
                </div>
                <div className="description">
                    <p>{__( 'Are you sure you want to remove the user __user__?', { user: UserHelpers.getDisplayName( user ) } )}</p>
                </div>
            </div>
        );
    }
}

UserList.contextTypes = {
    router: PropTypes.object.isRequired
};

const UserTable = ConnectMeteorData( ( { search, options } ) => {
    const query = {};

    if ( !_.isEmpty( search ) ) {
        query.$or = [
            { 'profile.firstName': { $regex: escapeStringRegexp( search ), $options: 'i' } },
            { 'profile.lastName': { $regex: escapeStringRegexp( search ), $options: 'i' } },
            { 'emails.address': { $regex: escapeStringRegexp( search ), $options: 'i' } }
        ];
    }

    return {
        loading: !Meteor.subscribe( 'admin/user/list', query, options.subscription ).ready(),
        rows:    UserCollection.find( query, options.local ).fetch(),
        total:   Counts.get( 'user/count/' + slugify( JSON.stringify( query ) ) )
    };
} )( BaseTable );
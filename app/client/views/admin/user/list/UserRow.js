"use strict";

import React, {Component, PropTypes} from "react";
import moment from "moment";
import classNames from "classnames";
import {Link} from 'react-router';
import UserHelpers from "/imports/accounts/UserHelpers";
import BaseRow from '../../../common/table/BaseRow';
import Avatar from '../../../widgets/Avatar';
import Roles from '../../../widgets/RoleLabels';

export default class UserRow extends BaseRow {

    shouldComponentUpdate(nextProps, nextState) {
        return super.shouldComponentUpdate(nextProps, nextState) && ( !_.isEqual( nextProps, this.props ) || !_.isEqual( nextState, this.state ) );
    }

    render() {
        const {entity: user, selected} = this.props;
        const verified = UserHelpers.hasVerifiedEmail(user);

        return (
            <tr className={ classNames( { 'active' : selected } ) }
                onClick={this.select}
                onDoubleClick={this.edit}>
                <td className="collapsing">
                    <Avatar user={user}/>
                </td>
                <td>{user.profile ? user.profile.firstName : ''}</td>
                <td>{user.profile ? user.profile.lastName : ''}</td>
                <td>{user.emails && user.emails[0] ? user.emails[0].address : ''}</td>
                <td>{user.points}</td>
                <td className="collapsing"><Roles user={user}/></td>
                <td className="center aligned collapsing">
                    <i className={ classNames('icon', { 'green checkmark' : user.enabled, 'red remove' : !user.enabled } ) }/>
                </td>
                <td className="center aligned collapsing">
                    <i className={ classNames('icon', { 'green checkmark' : verified, 'red remove' : !verified } ) }/>
                </td>
                <td className="center aligned collapsing">
                    <i className={ classNames('icon', { 'green checkmark' : user.completed, 'red remove' : !user.completed } ) }/>
                </td>
                <td className="center aligned collapsing">
                    <i className={ classNames('icon', { 'orange legal' : user.moderated } ) }/>
                </td>
                <td className="collapsing">{(() => {
                    if ( user.services ) {
                        return _.map( _.keys( user.services ), ( service )=> {
                            switch ( service ) {
                                case 'facebook':
                                    return (<i key={service} className="circular inverted facebook icon"/>);
                                    break;
                                case 'twitter':
                                    return (<i key={service} className="circular inverted twitter icon"/>);
                                    break;
                                case 'google':
                                    return (<i key={service} className="circular inverted google icon"/>);
                                    break;
                                case 'linkedin':
                                    return (<i key={service} className="circular inverted linkedin icon"/>);
                                    break;
                                default:
                                    return null;
                                    break;
                                }
                            } );
                    } else {
                        return null;
                    }
                })() }</td>
                <td className="collapsing">
                    { moment(user.createdAt).fromNow() }
                </td>
                <td className="collapsing">
                    { moment(user.updatedAt ? user.updatedAt : user.createdAt).fromNow() }
                </td>
                <td className="right aligned collapsing">
                    <div ref="actions" className="ui mini buttons">
                        <Link className="ui compact icon button"
                                data-content={__('Profile')}
                                to={'/profile/' + user._id}>
                            <i className="user icon"/>
                        </Link>
                        <button className="ui compact teal icon button"
                                onClick={this.edit}
                                data-content={__('Edit')}>
                            <i className="write icon"/>
                        </button>
                        <button className="ui compact red icon button"
                                onClick={this.remove}
                                data-content={__('Remove')}>
                            <i className="remove icon"/>
                        </button>
                    </div>
                </td>
            </tr>
        );
    }
}

UserRow.propTypes = _.defaults({

}, BaseRow.propTypes );
"use strict";

import React, {Component, PropTypes} from "react";
import ReactDOM from "react-dom";
import GroupCollection from "/imports/groups/GroupCollection";
import GroupImagesCollection from "/imports/groups/GroupImagesCollection";
import {ConnectMeteorData} from "../../../lib/ReactMeteorData";
import Image from "../../../views/widgets/Image";

export class GroupPicker extends Component {

    constructor( props ) {
        super( props );
        this.addSelectedGroup = this.addSelectedGroup.bind( this );
        this.state            = {
            selectedGroup: null
        }
    }

    componentDidMount() {
        $( ReactDOM.findDOMNode( this.refs.selectGroup ) ).dropdown( {
            fullTextSearch: true
        } );
    }

    addSelectedGroup() {
        const $selectGroup = $( ReactDOM.findDOMNode( this.refs.selectGroup ) );
        this.props.onAdd( $selectGroup.dropdown( 'get value' ) );
        $selectGroup.dropdown( 'restore defaults' );
    }

    render() {
        const { groups } = this.props;

        return (
            <div className="ui fluid buttons">
                <div ref="selectGroup" className="ui floating labeled icon search dropdown basic button">
                    <i className="add group icon"/>
                    <span className="text">{__( 'Search groups' )}</span>
                    <div className="menu">
                        { groups.map( group => <div key={group._id} data-value={group._id} className="item">
                            <Image image={GroupImagesCollection.findOne( { owners: group._id } )}
                                   size="thumb"
                                   className="circular bullseye avatar"/>
                            {group.name}
                        </div> ) }
                    </div>
                </div>
                <div className="ui teal button" onClick={this.addSelectedGroup}>Add</div>
            </div>
        );
    }

}

GroupPicker.propTypes = {
    value:          PropTypes.arrayOf( PropTypes.string ).isRequired,
    onAdd:          PropTypes.func.isRequired,
    groups:         PropTypes.arrayOf( PropTypes.object ),
    selectedGroups: PropTypes.arrayOf( PropTypes.object )
};

GroupPicker.defaultProps = {
    value: []
};

export default ConnectMeteorData( ( { value } )=> {
    Meteor.subscribe( 'admin/group/list' );
    return {
        groups:         GroupCollection.find( { _id: { $nin: value } } ).fetch(),
        selectedGroups: GroupCollection.find( { _id: { $in: value } } ).fetch()
    };
} )( GroupPicker );

"use strict";

import React, {Component, PropTypes} from "react";
import ReactDOM from "react-dom";
import classNames from "classnames";
import GroupCollection from "/imports/groups/GroupCollection";
import GroupImagesCollection from "/imports/groups/GroupImagesCollection";
import {ConnectMeteorData} from "../../../lib/ReactMeteorData";
import GroupPicker from "./GroupPicker";
import Image from "../../../views/widgets/Image";

export class GroupSelectionList extends Component {

    constructor( props ) {
        super( props );
        this.state = {
            selectedGroup: null
        };
    }

    render() {
        const { value, onAdd, selectedGroups } = this.props;

        return (
            <div>
                <GroupPicker value={value} onAdd={onAdd}/>
                <table className="ui very basic small selectable table">
                    <thead>
                    <tr>
                        <th></th>
                        <th>{__( 'Name' )}</th>
                        <th className="right aligned">{__( 'Actions' )}</th>
                    </tr>
                    </thead>
                    <tbody>
                    { selectedGroups.map( group => <GroupRow key={group._id}
                                                             group={group}
                                                             selected={this.state.selectedGroup && this.state.selectedGroup._id == group._id}
                                                             onSelect={() => this.setState( { selectedGroup: group } ) }
                                                             onRemove={this.props.onRemove}/> ) }
                    </tbody>
                </table>
            </div>
        );
    }

}

GroupSelectionList.propTypes = {
    value:    PropTypes.arrayOf( PropTypes.string ).isRequired,
    onAdd:    PropTypes.func.isRequired,
    onRemove: PropTypes.func.isRequired
};

GroupSelectionList.defaultProps = {
    value: []
};

export default ConnectMeteorData( props => {
    Meteor.subscribe( 'admin/group/list' );
    return {
        selectedGroups: GroupCollection.find( { _id: { $in: props.value } } ).fetch()
    };
} )( GroupSelectionList );

export class GroupRow extends Component {

    componentDidMount() {
        $( '.button', ReactDOM.findDOMNode( this.refs.actions ) ).popup();
    }

    render() {
        return (
            <tr className={ classNames( { 'active': this.props.selected } ) }
                onClick={this.props.onSelect}>
                <td>
                    <Image image={GroupImagesCollection.findOne( { owners: this.props.group._id } )}
                           size="thumb"
                           className="circular group avatar"/>
                </td>
                <td>{this.props.group.name}</td>
                <td className="right aligned">
                    <div ref="actions" className="ui mini buttons">
                        <button className="ui compact red icon button"
                                onClick={() => this.props.onRemove( this.props.group._id )}
                                data-content={__( 'Remove' )}
                                data-variation="inverted">
                            <i className="remove icon"/>
                        </button>
                    </div>
                </td>
            </tr>
        );
    }
}

GroupRow.propTypes = {
    onRemove: PropTypes.func.isRequired
};
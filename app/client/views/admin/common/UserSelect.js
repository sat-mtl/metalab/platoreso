"use strict";

import React, {Component, PropTypes} from "react";
import ReactDOM from "react-dom";
import classNames from "classnames";
import UserCollection from "/imports/accounts/UserCollection";
import {ConnectMeteorData} from "../../../lib/ReactMeteorData";
import Avatar from "../../../views/widgets/Avatar";
import Roles from "../../../views/widgets/RoleLabels";

const { findDOMNode } = ReactDOM;

export class UserSelect extends Component {

    constructor( props ) {
        super( props );

        this.addSelectedUser = this.addSelectedUser.bind( this );

        this.state = {
            selectedUser: null
        };
    }

    componentDidMount() {
        $( findDOMNode( this.refs.selectUser ) ).dropdown( {
            fullTextSearch: true
        } );
    }

    addSelectedUser() {
        const $selectUser = $( findDOMNode( this.refs.selectUser ) );
        this.props.onAdd( $selectUser.dropdown( 'get value' ) );
        $selectUser.dropdown( 'restore defaults' );
    }

    render() {
        const { selectedUser }         = this.state;
        const { users, selectedUsers } = this.props;

        return (
            <div>
                <div className="ui fluid buttons">
                    <div ref="selectUser" className="ui labeled icon search dropdown basic button">
                        <i className="add user icon"/>
                        <span className="text">{__( 'Search users' )}</span>

                        <div className="menu">
                            {/*<div className="ui icon search input">
                             <i className="search icon"/>
                             <input type="text" placeholder={__('Search users...')}/>
                             </div>
                             <div className="divider"></div>
                             <div className="header">
                             {__( 'Search Users' )}
                             </div>*/}
                            { _.map( users, ( user ) => {
                                return ( <div key={user._id} data-value={user._id} className="item">
                                    <Avatar user={user}/>
                                    { user.profile ? user.profile.firstName + ' ' + user.profile.lastName : user._id }
                                </div> );
                            } ) }
                        </div>
                    </div>
                    <div className="ui teal button" onClick={this.addSelectedUser}>Add</div>
                </div>
                <table className="ui very basic small selectable table">
                    <thead>
                    <tr>
                        <th></th>
                        <th>{__( 'First Name' )}</th>
                        <th>{__( 'Last Name' )}</th>
                        <th>{__( 'Roles' )}</th>
                        <th className="right aligned">{__( 'Actions' )}</th>
                    </tr>
                    </thead>
                    <tbody>
                    { selectedUsers.map( ( user ) => {
                        return (
                            <UserRow key={user._id}
                                     user={user}
                                     selected={selectedUser && selectedUser._id == user._id}
                                     onSelect={() => this.setState( { selectedUser: user } ) }
                                     onRemove={this.props.onRemove}/>);
                    } ) }
                    </tbody>
                </table>
            </div>
        );
    }

}

UserSelect.propTypes = {
    value:         PropTypes.arrayOf( PropTypes.string ).isRequired,
    onAdd:         PropTypes.func.isRequired,
    onRemove:      PropTypes.func.isRequired,
    users:         PropTypes.arrayOf( PropTypes.object ),
    selectedUsers: PropTypes.arrayOf( PropTypes.object )
};

UserSelect.defaultProps = {
    value: []
};

export default ConnectMeteorData( props => {
    const { value } = props;

    //TODO: Use a more concise list, this gets all of the user's info
    Meteor.subscribe( 'admin/user/list' );

    return {
        users:         UserCollection.find( { _id: { $nin: value } } ).fetch(),
        selectedUsers: UserCollection.find( { _id: { $in: value } } ).fetch()
    };
} )( UserSelect );

export class UserRow extends Component {

    componentDidMount() {
        $( '.button', findDOMNode( this.refs.actions ) ).popup();
    }

    render() {
        const { user, selected } = this.props;
        return (
            <tr className={ classNames( { 'active': selected } ) }
                onClick={this.props.onSelect}>
                <td><Avatar user={user}/></td>
                <td>{user.profile ? user.profile.firstName : ''}</td>
                <td>{user.profile ? user.profile.lastName : ''}</td>
                <td><Roles user={user}/></td>
                <td className="right aligned">
                    <div ref="actions" className="ui mini buttons">
                        <button className="ui compact red icon button"
                                onClick={() => this.props.onRemove( user._id )}
                                data-content={__( 'Remove' )}
                                data-variation="inverted">
                            <i className="remove icon"/>
                        </button>
                    </div>
                </td>
            </tr>
        );
    }
}

UserRow.propTypes = {
    user:     PropTypes.object.isRequired,
    selected: PropTypes.bool.isRequired,
    onSelect: PropTypes.func.isRequired,
    onRemove: PropTypes.func.isRequired
};
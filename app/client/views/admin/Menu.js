"use strict";

import React, { Component, PropTypes } from "react";
import classNames from "classnames";
import {Link} from 'react-router';
import links from './menu/Links';

export default class Menu extends Component {

    render() {
        return (
            <nav id="adminMenu" className="ui inverted vertical menu fixed top management-menu">
                { links.map( link => (
                    <Link key={link.to} to={link.to} onlyActiveOnIndex={link.index} className="teal item" activeClassName="active">
                        <i className={ classNames( link.icon, 'icon' )}/>
                        {link.label}
                    </Link>
                ) ) }
                {this.props.children}
            </nav>
        );
    }
}
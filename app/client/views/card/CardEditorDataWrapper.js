"use strict";

import CardCollection from "/imports/cards/CardCollection";
import { ConnectMeteorData } from '../../lib/ReactMeteorData'

/**
 * Meteor Data Wrapper
 */
export default ConnectMeteorData( props => {
    const cardLoading    = !!( props.cardId && !Meteor.subscribe( 'card', props.cardId ).ready() );
    const card           = props.cardId ? CardCollection.findOne( { $or: [{ id: props.cardId }, { slug: props.cardId }], current: true } ) : null;
    const projectLoading = !!( props.cardId && ( !card || !Meteor.subscribe( 'project', card.project ).ready() ) );
    return {
        loading: cardLoading || projectLoading,
        card:    card,
        image:   card && card.getImage(),
        files:   card && card.getAttachments(),
        project: card && card.getProject()
    }
} );
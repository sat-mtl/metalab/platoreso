"use strict";

import Modal from '../../components/Modal';
import Dialog from '../../components/Dialog';
import { CardDetails } from './CardDetails';
import CardNotFound from './CardNotFound';
import CardDataWrapper from './CardDataWrapper';

import React, { Component, PropTypes } from "react";

export class CardModal extends Component {

    render() {
        const { loading, card, cardId, onRequestClose } = this.props;

        return (
            <Modal opened={ cardId != null } onRequestClose={onRequestClose}>
                { loading && <div className="ui large inverted active indeterminate text loader">
                    {__( 'Loading Card' )}
                </div> }
                <Dialog onRequestClose={onRequestClose}>
                    { !loading && !card && <CardNotFound key="cardNotFound" {...this.props}/> }
                </Dialog>
                <Dialog onRequestClose={onRequestClose}>
                    { !loading && card && <CardDetails key={card.id} {...this.props}/> }
                </Dialog>
            </Modal>
        );
    }
}

CardModal.propTypes = {
    cardId:         PropTypes.string,
    loading:        PropTypes.bool.isRequired,
    card:           PropTypes.object,
    onRequestClose: PropTypes.func.isRequired
};

export default CardDataWrapper( CardModal );

"use strict";

import Field from '../../../components/forms/Field';
import Select from '../../../components/forms/Select';

import CardMethods from "/imports/cards/CardMethods";

import React, { Component, PropTypes } from "react";

export default class CardTags extends Component {

    constructor( props ) {
        super( props );

        this.mounted= false; // Damn you React and your isMounted() deprecation: https://github.com/facebook/react/issues/3417

        this.getTags = this.getTags.bind( this );

        this.state = {
            availableTags: []
        };
    }

    componentDidMount(){
        this.mounted = true;
        this.getTags();
    }

    componentWillUnmount() {
        this.mounted = false;
    }

    componentWillReceiveProps( nextProps ) {
        if ( this.props.projectId != nextProps.projectId ) {
            this.getTags();
        }
    }

    getTags() {
        // Method returns what is currently available on the client first
        // Then the callback has all the available tags
        const availableTags = CardMethods.getAvailableTagList.call( { project: this.props.projectId }, ( err, result ) => {
            if ( !this.mounted ) {
                return;
            }

            if ( err ) {
                console.error( err );
            } else {
                this.setState( { availableTags: result } );
            }
        } );

        this.setState( { availableTags: availableTags } );
    }

    render() {
        const { tags } = this.props;
        const { availableTags } = this.state;

        return (
            <Field label={__("Tags")}>
                <Select name="tags"
                        multiple={true}
                        combo={true}
                        search={true}
                        label={{variation:'yellow'}}
                        className="fluid"
                        defaultValue={tags}>
                    { _.map( availableTags, tag => <option key={tag} value={tag}>{tag}</option> ) }
                </Select>
            </Field>
        );
    }

};

CardTags.propTypes = {
    tags:  PropTypes.arrayOf( PropTypes.string ),
    projectId: PropTypes.string.isRequired
};
"use strict";

import React, {Component, PropTypes} from "react";
import ReactDOM from "react-dom";
import Pikaday from "pikaday";
import moment from "moment";
import pad from "underscore.string/pad";

import {FS} from "meteor/cfs:base-package";

import Field from '../../../components/forms/Field';
import Input from '../../../components/forms/Input';
import TextArea from '../../../components/forms/Textarea';
import Select from '../../../components/forms/Select';
import ImageChooser from '../../../components/forms/ImageChooser';
import AttachmentChooser from '../../../components/forms/AttachmentChooser';
import PhaseBlock from '../../project/common/PhaseBlock';
import PhaseTypeEditor from '../../project/common/PhaseTypeEditor';

import CardTags from './CardTags';

const { File } = FS;

export default class CardForm extends Component {

    /*constructor( props ) {
        super( props );

        this.setupPickers   = this.setupPickers.bind( this );
        this.destroyPickers = this.destroyPickers.bind( this );

        this.hours = [];
        for ( let i = 0; i < 24; i++ ) {
            this.hours.push( pad( i, 2, '0' ) );
        }

        this.minutes = [];
        for ( let i = 0; i < 60; i++ ) {
            this.minutes.push( pad( i, 2, '0' ) );
        }

        this.pikadayConfig = {
            format:                          'YYYY-MM-DD',
            numberOfMonths:                  2,
            showDaysInNextAndPreviousMonths: true
        };
    }

    componentDidMount() {
        this.setupPickers();
    }

    componentWillUpdate() {
        this.destroyPickers();
    }

    componentDidUpdate( prevProps, prevState ) {
        this.setupPickers();
    }

    componentWillUnmount() {
        this.destroyPickers();
    }

    setupPickers() {
        this.publishedPicker = new Pikaday( _.defaults( {
            field: $( '#publishedDate', ReactDOM.findDOMNode( this.refs.publishedDate ) )[0]
        }, this.pikadayConfig ) );
    }

    destroyPickers() {
        if ( this.publishedPicker ) {
            this.publishedPicker.destroy();
        }
    }*/

    render() {
        const { project, phase, card, image, pictureChanged, files } = this.props;

        if ( !project || !phase ) {
            return null;
        }

        return (
            <div className="ui stackable grid cardForm">

                <div className="row">

                    <div className="ten wide column">
                        <Field label={__("Title")} className="required">
                            <Input ref="name" name="name" type="text"
                                   defaultValue={card && card.name}
                                   autofocus={true}
                                   rules={[
                                       { type: 'empty', prompt: __('Please enter a title') },
                                       { type: 'minLength[3]', prompt: __( 'Card title should be a minimum of 3 characters long' ) },
                                       { type: 'maxLength[140]', prompt: __('Card title can be a maximum of 140 characters long') }
                                   ]}/>
                        </Field>

                        <Field label={__("Content")}>
                            <TextArea ref="content" name="content" rows={2} autosize={true} defaultValue={ card && card.content }/>
                        </Field>

                        <CardTags tags={card && card.tags} projectId={project._id}/>

                    </div>

                    <div className="six wide column">
                        <Field label={__('Picture')}>
                            <ImageChooser image={image} filesChanged={pictureChanged}/>
                        </Field>

                        {/*<h4 className="ui dividing header">Publication</h4>
                        <Field label={__("Date")} className="required">
                            <Input ref="publishedDate" name="publishedDate" type="text"
                                   defaultValue={moment( card ? card.publishedAt : undefined ).format('YYYY-MM-DD')}
                                   rules={[
                                           { type: 'empty', prompt: __('Please enter a published date') }
                                       ]}/>

                        </Field>

                        <div className="two fields">
                            <Field label={__("Hour")} className="required">
                                <Select ref="publishedHour" name="publishedHour" className="fluid"
                                        defaultValue={moment( card ? card.publishedAt : undefined ).hour()}
                                        rules={[
                                               { type: 'empty', prompt: __('Please select a published hour') }
                                            ]}>
                                    { this.hours.map( hour => <option key={hour} value={hour}>{hour}</option> ) }
                                </Select>
                            </Field>
                            <Field label={__("Minutes")} className="required">
                                <Select ref="publishedMinute" name="publishedMinute" className="fluid"
                                        defaultValue={ moment( card ? card.publishedAt : undefined ).minute() }
                                        rules={[
                                               { type: 'empty', prompt: __('Please select a published minute') }
                                            ]}>
                                    { this.minutes.map( minute =>
                                        <option key={minute} value={minute}>{minute}</option> ) }
                                </Select>
                            </Field>
                        </div>
                        <div className="ui small message">
                            <p>{__( 'Publishing a card in the future will make that card available only after the specified published date and time.' )}</p>
                        </div>*/}
                    </div>
                </div>

                {/* PHASE TYPE */}
                <div className="sixteen wide column">
                    <PhaseBlock phase={phase}>
                        <PhaseTypeEditor project={project} phase={phase} card={card}/>
                    </PhaseBlock>
                </div>

                {/* ATTACHMENTS */}
                <div className="sixteen wide column">
                    <Field label={__('Attachments')}>
                        <AttachmentChooser files={files}
                                           allowMultiple={true}
                                           fileRemoved={this.props.fileRemoved}
                                           filesChanged={this.props.filesChanged}/>
                    </Field>
                </div>


            </div>
        );
    }
}

CardForm.propTypes = {
    project:        PropTypes.object.isRequired,
    phase:          PropTypes.object.isRequired,
    card:           PropTypes.object,
    image:          PropTypes.object,
    files:          PropTypes.arrayOf( PropTypes.instanceOf( File ) ),
    pictureChanged: PropTypes.func.isRequired,
    filesChanged:   PropTypes.func.isRequired,
    fileRemoved:    PropTypes.func
};

CardForm.defaultProps = {
    files: []
};
"use strict";

import React, { Component, PropTypes } from "react";
import classNames from "classnames";
import { DragSource, DropTarget } from "react-dnd";
import { getEmptyImage } from "react-dnd-html5-backend";
import store from "react-contextmenu/modules/redux/store";
import ModerationMethods from "/imports/moderation/ModerationMethods";
import CardCollection from "/imports/cards/CardCollection";
import CardMethods from "/imports/cards/CardMethods";
import CardSecurity from "/imports/cards/CardSecurity";
import { ConnectMeteorData } from "../../lib/ReactMeteorData";
import LiveDate from "../widgets/LiveDate";
import ListCardPreview from "./cardPreview/ListCardPreview";
import GridCardPreview from "./cardPreview/GridCardPreview";
import BlockCardPreview from "./cardPreview/BlockCardPreview";
import FeedCardPreview from "./cardPreview/FeedCardPreview";
import Spinner from "../widgets/Spinner";

// Import the store directly, we don't use the useless component wrapper

export class CardPreview extends Component {

    constructor( props ) {
        super( props );
        this.showCard      = this.showCard.bind( this );
        this.editCard      = this.editCard.bind( this );
        this.likeCard      = this.likeCard.bind( this );
        this.reportCard    = this.reportCard.bind( this );
        this.onContextMenu = this.onContextMenu.bind( this );
    }

    /**
     * View the card details
     */
    showCard() {
        if ( this.props.showCard ) {
            this.props.showCard( this.props.card.uri );
        } else {
            this.context.router.push( { pathname: '/card/' + this.props.card.uri } );
        }
    }

    /**
     * Edit the card
     */
    editCard() {
        if ( this.props.card.current ) {
            this.props.editCard( this.props.card.id );
        }
    }

    /**
     * Like the card
     */
    likeCard() {
        if ( this.props.card.current ) {
            CardMethods.like.call( { cardId: this.props.card.id } );
        }
    }

    /**
     * Report the card
     */
    reportCard() {
        const { card } = this.props;
        if ( !card ) {
            return;
        }
        ModerationMethods.report.call( { entityTypeName: CardCollection.entityName, entityId: card.id } );
    }

    /**
     * On Context Menu
     * A copy/paste of what is needed from react-contextmenu/src/contextmenu-layer.js
     * We don't want to wrap our card preview with their context menu layer div
     * only to have this little code that actually does something.
     */
    onContextMenu( event ) {
        event.preventDefault();
        store.dispatch( {
            type: "SET_PARAMS",
            data: {
                x:           event.clientX,
                y:           event.clientY,
                currentItem: this.props.card,
                isVisible:   'card'
            }
        } );
    }

    componentDidMount() {
        if ( this.props.connectDragPreview ) {
            // Use empty image as a drag preview so browsers don't draw it
            // and we can draw whatever we want on the custom drag layer instead.
            this.props.connectDragPreview( getEmptyImage(), {
                // IE fallback: specify that we'd rather screenshot the node
                // when it already knows it's being dragged so we can hide it with CSS.
                captureDraggingState: true
            } );
        }
    }

    getImageOrientation( image ) {
        let imageOrientation = null;
        if ( image ) {
            const copyInfo = image.getCopyInfo( 'card_images_medium' );
            if ( copyInfo ) {
                const { width, height } = copyInfo;
                if ( width == null || height == null || width > height ) {
                    imageOrientation = 'horizontal-image';
                } else if ( width < height ) {
                    imageOrientation = 'vertical-image';
                } else {
                    imageOrientation = 'square-image';
                }
            } else {
                imageOrientation = 'horizontal-image';
            }
        } else {
            imageOrientation = 'no-image';
        }
        return imageOrientation;
    }

    getImageStyle( image ) {
        let imageStyle = {};
        if ( image && image.dominantColor != null ) {
            imageStyle.backgroundColor = '#' + image.dominantColor;
        }
        return imageStyle;
    }

    render() {
        const { loading, userId, project, card, image, layout, showLinkBadges, linkedCard, draggedCard } = this.props;
        const { connectDragSource, connectDropTarget, isHovered, isDragging }                            = this.props;

        if ( loading ) {
            return <Spinner/>
        }

        if ( !card ) {
            return null;
        }

        const hasContent       = card.hasExcerpt;
        const imageOrientation = this.getImageOrientation( image );
        const showLinked       = draggedCard ? card.isLinkedTo( draggedCard ) || draggedCard.isLinkedTo( card ) : false;
        const isSnapshot       = !card.current;
        const isDragged        = isDragging || ( draggedCard && draggedCard.id == card.id );

        // Links
        // This is a mouthful but is serves to enable the display of link badges and also
        // show either all links or just links to a specified card.
        const hasReference = showLinkBadges && ( linkedCard ? card.hasReferenceTo( linkedCard ) : card.hasReference );
        const isDerived    = showLinkBadges && ( linkedCard ? card.isDerivedFrom( linkedCard ) : card.isDerived );
        const isComplement = showLinkBadges && ( linkedCard ? card.complements( linkedCard ) : card.isComplement );
        const hasInBadge   = showLinkBadges && ( hasReference || isDerived || isComplement );

        const isReference    = showLinkBadges && ( linkedCard ? card.isReferenceFor( linkedCard ) : card.isReference );
        const isOriginal     = showLinkBadges && ( linkedCard ? card.isOriginalOf( linkedCard ) : card.isOriginal );
        const isComplemented = showLinkBadges && ( linkedCard ? card.isComplementedBy( linkedCard ) : card.isComplemented );
        const hasOutBadge    = showLinkBadges && ( isReference || isOriginal || isComplemented );

        let lastVisit;
        let lastPhase;
        if ( isSnapshot ) {
            lastVisit = card.lastPhaseVisit;
            lastPhase = lastVisit ? project.getPhase( lastVisit.exitedTo ) : null;
        }

        const previewProps = {
            //imageOrientation: this.getImageOrientation(image),
            imageStyle:  this.getImageStyle( image ),
            youLike:     !!card.likes && card.likes.indexOf( userId ) != -1,
            youReported: card.isReportedBy( userId ),
            showCard:    this.showCard,
            editCard:    this.props.editCard ? this.editCard : null,
            likeCard:    this.likeCard,
            reportCard:  this.reportCard,
            showLinked:  showLinked,
        };

        let CardPreviewClass;
        let showBadges = showLinkBadges;
        switch ( layout ) {
            case 'feed':
                CardPreviewClass = FeedCardPreview;
                break;
            case 'list':
                CardPreviewClass = ListCardPreview;
                showBadges       = false;
                break;
            case 'grid':
                CardPreviewClass = GridCardPreview;
                break;
            case 'block':
            default:
                CardPreviewClass = BlockCardPreview;
                break;
        }

        let content = (
            <div className={classNames( layout, "card preview", imageOrientation, {
                'with-details': hasContent,
                'noContent':    !hasContent,
                dragged:        isDragged,
                faded:          draggedCard && !card.current,
                pinned:         card.pinned,
                linked:         showLinked,
                snapshot:       isSnapshot,
                badgesIn:       showBadges && hasInBadge,
                badgesOut:      showBadges && hasOutBadge
            }, this.props.className ) }
                 onContextMenu={this.onContextMenu}
            >
                <CardPreviewClass {...this.props} {...previewProps}/>

                { isSnapshot && lastVisit && <div className="footer">
                    <div className="message">
                        { lastPhase
                            ?
                          <LiveDate data-i18n="This card was moved to __phase__ __date__" strings={ { phase: `<span class="phase">${lastPhase.name}</span>` } } date={lastVisit.exitedAt} relative={true}/>
                            :
                          <LiveDate data-i18n="This card was moved __date__" date={lastVisit.exitedAt} relative={true}/>
                        }
                    </div>
                    <div className="extra">
                        <i className="small external icon"/>
                    </div>
                </div> }

                { showBadges && <div className="badgesContainer">

                    <div className="badges in">
                        { hasReference && <div className="badge"><i className="book icon"/></div> }
                        { isDerived && <div className="badge"><i className="clone icon"/></div> }
                        { isComplement && <div className="badge"><i className="idea icon"/></div> }
                    </div>

                    <div className="badges out">
                        { isReference && <div className="badge"><i className="book icon"/></div> }
                        { isOriginal && <div className="badge"><i className="clone icon"/></div> }
                        { isComplemented && <div className="badge"><i className="idea icon"/></div> }
                    </div>

                </div> }

                { showLinked && <div className="ui left orange corner label">
                    <i className="linkify icon"/>
                </div> }

                {/* DROP TARGET */ }
                { !isSnapshot && connectDropTarget &&
                  <div className={classNames( "drop targets", { opened: isHovered } ) }>
                      <div className="link target">
                          { connectDropTarget( <div className="content">
                              <i className="linkify icon"/>
                          </div> ) }
                      </div>
                  </div> }
            </div>
        );

        if ( !isSnapshot && connectDragSource ) {
            content = connectDragSource( content );
        }

        return content;
    }
}

CardPreview.propTypes = {
    loading:            PropTypes.bool,
    userId:             PropTypes.string.isRequired,
    project:            PropTypes.object.isRequired,
    phase:              PropTypes.object,
    card:               PropTypes.object,//.isRequired,
    linkedCard:         PropTypes.object,
    author:             PropTypes.object,
    image:              PropTypes.object,
    showCard:           PropTypes.func,
    editCard:           PropTypes.func,
    editable:           PropTypes.bool,
    layout:             PropTypes.string.isRequired,
    showLinkBadges:     PropTypes.bool,
    dragCard:           PropTypes.func,
    draggedCard:        PropTypes.object,
    // Drag & Drop
    connectDragSource:  PropTypes.func,
    connectDragPreview: PropTypes.func,
    connectDropTarget:  PropTypes.func,
    isDragging:         PropTypes.bool,
    isHovered:          PropTypes.bool
};

CardPreview.defaultProps = {
    loading:        false,
    editable:       false,
    showLinkBadges: false
};

CardPreview.contextTypes = {
    router: PropTypes.object.isRequired
};

/**
 * Default export, requires a card to be passed in props
 */
export const WrappedCardPreview = ConnectMeteorData( props => ({
    userId:   Meteor.userId(),
    author:   props.card && props.card.getAuthor(),
    image:    props.card && props.card.getImage(),
    // Shortcut: Not editable if it is a snapshot
    editable: props.card ? ( props.card.current && CardSecurity.canEdit( Meteor.userId(), props.card.id ) ) : false
}) )( CardPreview );
export default WrappedCardPreview;

/**
 * Full data wrap, only requires a cardId to be passed in the props
 */
export const FullyWrappedCardPreview = ConnectMeteorData( props => {
    const loading = !!( props.cardId && !Meteor.subscribe( 'card/preview', props.cardId ).ready() );
    const card    = props.cardId && !loading ? CardCollection.findOne( { id: props.cardId, current: true } ) : null;
    return { loading, card }
} )( WrappedCardPreview );

/**
 * Draggable card
 */
export const DraggableCardPreview = _.compose(
    DragSource( 'card', {
        canDrag( props, monitor ) {
            // Account for history cards (ref) not being draggable
            return props.card.current && props.canContribute && (props.card.phase == props.phase._id);
        },
        beginDrag( props, monitor, component ) {
            props.dragCard( props.card );
            return {
                id:    props.card.id,
                phase: props.card.phase,
                name:  props.card.name
            };
        },
        endDrag( props, monitor, component ) {
            props.dragCard( null );
        }
    }, ( connect, monitor ) => ({
        connectDragSource:  connect.dragSource(),
        connectDragPreview: connect.dragPreview(),
        isDragging:         monitor.isDragging()
    }) ),

    DropTarget( 'card', {
        canDrop( props, monitor ) {
            const item = monitor.getItem();

            // Can't drop on yourself
            if ( props.card.id == item.id ) {
                return false;
            }

            // Can't drop on snapshots
            if ( !props.card.current ) {
                return false;
            }

            // Can't drop on inactive phases
            if ( !props.phase.isActive ) {
                return false;
            }

            // Can't drop if already linked
            // 2016-07-14 -> Yes, you can modify the link type now
            /*if ( props.card.linkedCards && props.card.linkedCards[item.id] ) {
             return false;
             }*/

            return true;
        },
        drop( props, monitor, component ) {
            const item = monitor.getItem();
            const type = monitor.getItemType();
            switch ( type ) {
                case 'card':
                    props.linkCards( { linkCardId: item.id, toCardId: props.card.id } );
                    //CardMethods.linkCards.call( { linkCardId: item.id, toCardId: props.card.id } );
                    break;
            }
        }
    }, ( connect, monitor ) => ({
        connectDropTarget: connect.dropTarget(),
        isHovered:         monitor.isOver() && monitor.canDrop()
    }) )
)( WrappedCardPreview );

DraggableCardPreview.propTypes = {
    canContribute: PropTypes.bool.isRequired,
    linkCards:     PropTypes.func.isRequired
};


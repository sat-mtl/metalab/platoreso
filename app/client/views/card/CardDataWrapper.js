"use strict";

import ProjectSecurity from "/imports/projects/ProjectSecurity";
import CardCollection from "/imports/cards/CardCollection";
import CardSecurity from "/imports/cards/CardSecurity";
import { ConnectMeteorData } from '../../lib/ReactMeteorData'

/**
 * Card Data Wrapper
 */
export default ConnectMeteorData( props => {
    let options = { linked: true };
    if ( props.cardVersion ) {
        options.version = parseInt( props.cardVersion );
    }
    const user        = Meteor.userId();
    const cardLoading = !!( props.cardId && !Meteor.subscribe( 'card', props.cardId, options ).ready() );

    const query = { $or: [{ id: props.cardId }, { slug: props.cardId }] };
    if ( props.cardVersion == null ) {
        query.current = true;
    } else {
        query.version = parseInt( props.cardVersion );
    }

    const card           = props.cardId ? CardCollection.findOne( query ) : null;
    const projectLoading = !( card && card.project && Meteor.subscribe( 'project', card.project ).ready() );
    const project        = card && card.getProject();

    return {
        loading:       cardLoading || projectLoading,
        card:          card,
        project:       project,
        author:        card && card.getAuthor(),
        image:         card && card.getImage(),
        files:         card && card.getAttachments(),
        //linkedCards:   card && card.getLinked(),
        links:         card && card.getLinks(),
        canContribute: user && project && card && card.current && ProjectSecurity.canContribute( user, project._id ),
        editable:      user && project && card && card.current && CardSecurity.canEdit( user, card.id ) // Project is needed before checking editability
    }
} );

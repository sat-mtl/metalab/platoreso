"use strict";

import { Link } from 'react-router';
import ContainedPage from '../layouts/ContainedPage';
import Spinner from '../widgets/Spinner';
import NotFound from '../errors/NotFound';
import { CardDetails } from './CardDetails';
import CardDataWrapper from './CardDataWrapper';

import React, { Component, PropTypes } from "react";

export class Card extends Component {

    render() {
        const { loading, card, project } = this.props;
        if ( loading ) {
            return <Spinner/>;

        } else if ( !card ) {
            return <NotFound/>;

        } else {
            return (
                <ContainedPage>
                    <div className="card-navigation sixteen wide column">
                        <div className="links">
                            <Link to={`/project/${project ? project._id : ''}`}>
                                <i className="chevron left icon"/>
                                {__( 'Back to project' )}</Link>
                            <Link to={`/project/${project ? project._id : ''}/board`}>
                                {__( 'View project board' )}
                                <i className="chevron right icon"/>
                            </Link>
                        </div>
                    </div>
                    <CardDetails className="standalone sixteen wide column" {...this.props}/>
                </ContainedPage>
            );
        }
    }
}

const DataWrapper = CardDataWrapper( Card );

export default class WrappedCard extends Component {
    render() {
        return <DataWrapper cardId={this.props.params.id} cardVersion={this.props.params.cardVersion}/>;
    }
}
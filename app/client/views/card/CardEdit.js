"use strict";

import React, {Component, PropTypes} from "react";
import ContainedPage from '../layouts/ContainedPage';
import Spinner from '../widgets/Spinner';
import NotFound from '../errors/NotFound';
import {CardEditor} from './CardEditor';
import CardEditorDataWrapper from './CardEditorDataWrapper';


export const CardEdit = ( props ) => {
    if ( props.loading ) {
        return <Spinner/>;

    } else if ( !props.card ) {
        return <NotFound/>;

    } else {
        return (
            <ContainedPage>
                <CardEditor className="standalone sixteen wide column" {...props}/>
            </ContainedPage>
        );
    }
};

const DataWrapper = CardEditorDataWrapper( CardEdit );
export default ( { params } ) => <DataWrapper cardId={params.id}/>;
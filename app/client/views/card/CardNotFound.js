"use strict";

import React, { Component, PropTypes } from "react";

export default class CardNotFound extends Component {

    render() {
        return (
            <div className="cardNotFound">
                <div className="message">{__('Card not found!')}</div>
                <div className="ui olive button" onClick={this.props.onRequestClose}>{__('Back to project')}</div>
            </div>
        );
    }

}

CardNotFound.propTypes = {
    onRequestClose: PropTypes.func.isRequired
};
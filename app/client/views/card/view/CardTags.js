"use strict";

import React, { Component, PropTypes } from "react";

export default class CardTags extends Component {

    render() {
        const { tags } = this.props;
        return (
            <div className="tags">
                { _.map( tags, tag => <div className="tag" key={tag}>{tag}</div> ) }
            </div>
        );
    }

}

CardTags.propTypes = {
    tags: PropTypes.arrayOf(PropTypes.string ).isRequired
};
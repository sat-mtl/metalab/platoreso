"use strict";

import Modal from '../../components/Modal';
import Dialog from '../../components/Dialog';
import { CardEditor } from './CardEditor';
import CardEditorDataWrapper from './CardEditorDataWrapper';

import React, { Component, PropTypes } from "react";

export class CardEditorModal extends Component {

    render() {
        const { loading, card, cardId, onRequestClose } = this.props;
        return (
            <Modal opened={ cardId != null } onRequestClose={onRequestClose}>
                { loading && <div className="ui large inverted active indeterminate text loader">
                    {__( 'Loading Card' )}
                </div> }
                <Dialog onRequestClose={onRequestClose}>
                    { !loading && card && <CardEditor key={card.id} {...this.props} onFinished={onRequestClose}/> }
                </Dialog>
            </Modal>
        );
    }
}

CardEditorModal.propTypes = {
    cardId: PropTypes.string,
    onRequestClose: PropTypes.func.isRequired
};

export default CardEditorDataWrapper(CardEditorModal);

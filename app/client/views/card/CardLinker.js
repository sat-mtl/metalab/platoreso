"use strict";

import React, { Component, PropTypes } from "react";
import ReactDOM from "react-dom";
import classNames from "classnames";

import CardLinkTypes from "/imports/cards/links/CardLinkTypes";
import CardMethods from "/imports/cards/CardMethods";
import DropDown from '../../components/semanticui/modules/Dropdown';
import Form from '../../components/forms/Form';
import Field from '../../components/forms/Field';
import Select from '../../components/forms/Select';
import CardPreview from './CardPreview';

export class CardLinker extends Component {

    constructor( props ) {
        super( props );

        this.onLinkTypeChanged = this.onLinkTypeChanged.bind( this );
        this.swapCards         = this.swapCards.bind( this );
        this.submitForm        = this.submitForm.bind( this );
        this.linkCards         = this.linkCards.bind( this );
        this.onFinish          = this.onFinish.bind( this );

        this.state = {
            error:    null,
            swapped:  false,
            linkType: null,
            linking:  false
        };
    }

    onLinkTypeChanged( value ) {
        this.setState( {
            linkType: value
        } );
    }

    swapCards() {
        this.setState( { swapped: !this.state.swapped } );
    }

    submitForm() {
        $( ReactDOM.findDOMNode( this.refs.cardLinkForm ) ).form( 'submit' );
    }

    linkCards( values ) {
        if ( this.state.linking ) {
            return false;
        }
        this.setState( { linking: true } );

        const { linkCard:cardA, toCard:cardB } = this.props;
        const { swapped } = this.state;
        const linkCard = swapped ? cardB : cardA;
        const toCard   = swapped ? cardA : cardB;

        CardMethods.linkCards.call( {
            sourceCardId:      linkCard.id,
            linkType:          values.linkType,
            destinationCardId: toCard.id
        }, error => {
            this.setState( { linking: false } );
            if ( error ) {
                console.error( error );
                this.setState( { error } )
            } else {
                this.onFinish();
            }
        } );
    }

    onFinish() {
        if ( this.props.onFinished ) {
            this.props.onFinished();
        }
    }

    renderActions() {
        const { linking } = this.state;

        return (
            <div className="actions">
                <div className="ui buttons">
                    <div className="ui cancel button" onClick={this.onFinish}>
                        <i className="remove icon"/>
                        {__( 'Cancel' )}
                    </div>
                    <div className={classNames( "ui green button", { loading: linking, disabled: linking } )}
                         onClick={this.submitForm}>
                        <i className="linkify icon"/>
                        {__( 'Link' )}
                    </div>
                </div>
            </div>
        );
    }

    render() {
        const { linkCard:cardA, toCard:cardB, project } = this.props;
        const { swapped, linkType } = this.state;

        const linkCard = swapped ? cardB : cardA;
        const toCard   = swapped ? cardA : cardB;

        return (
            <div className={classNames( this.props.className, "editor linkCards" )}>

                <div className="header">
                    {__( 'Link Cards' )}
                </div>

                <Form id={`card-link-form-${linkCard.id}-${toCard.id}`}
                      ref="cardLinkForm"
                      key={`${linkCard.id}-${toCard.id}`}
                      error={this.state.error}
                      onSuccess={this.linkCards}>

                    <div className="linker">

                        <CardPreview key={toCard.id}
                                     card={toCard}
                                     project={project}
                                     layout="grid"
                                     className="linking"/>

                        <div className="options">
                            <Field label={__( "Link Type" )} className="required">
                                <Select id="linkType" name="linkType" onSelectChange={this.onLinkTypeChanged} defaultValue={linkType}>
                                    { Object.keys( CardLinkTypes ).map( key =>
                                        <option key={CardLinkTypes[key].id} value={CardLinkTypes[key].id}>{__( `${CardLinkTypes[key].label}` )}</option> ) }
                                </Select>
                            </Field>
                            <div className="ui labeled icon button" title={__( 'Swap' )} onClick={this.swapCards}>
                                <i className="exchange icon"/>
                                {__('Change direction')}
                            </div>
                        </div>

                        <CardPreview key={linkCard.id}
                                     card={linkCard}
                                     project={project}
                                     layout="grid"
                                     className="linking"/>

                    </div>

                </Form>

                { this.renderActions() }
            </div>
        );
    }
}

CardLinker.propTypes = {
    linkCard:   PropTypes.object.isRequired,
    toCard:     PropTypes.object.isRequired,
    project:    PropTypes.object.isRequired,
    onFinished: PropTypes.func.isRequired
};
"use strict";

import React, { Component, PropTypes } from "react";
import classNames from "classnames";
import filesize from "filesize";
import moment from "moment";
import { Link } from 'react-router';
import { FS } from "meteor/cfs:base-package";

import ModerationMethods from "/imports/moderation/ModerationMethods";

import CardMethods from "/imports/cards/CardMethods";
import CardCollection from "/imports/cards/CardCollection";

import Modal from '../../components/semanticui/modules/Modal';

import CardDataWrapper from './CardDataWrapper';
import LiveDate from '../widgets/LiveDate';
import Image from '../widgets/Image';
import Avatar from '../widgets/Avatar';
import Comments from '../comment/Comments';
import Feed from '../feed/Feed';
import PhaseBlock from '../project/common/PhaseBlock';
import PhaseType from '../project/common/PhaseType';
import CardTags from './view/CardTags';
import LinkedCard from './LinkedCard';

const { File } = FS;

export class CardDetails extends Component {

    constructor( props ) {
        super( props );

        this.renderCard        = this.renderCard.bind( this );
        this.renderLoading     = this.renderLoading.bind( this );
        this.trackCardView     = this.trackCardView.bind( this );
        this.likeCard          = this.likeCard.bind( this );
        this.reportCard        = this.reportCard.bind( this );
        this.showCard          = this.showCard.bind( this );
        this.editCard          = this.editCard.bind( this );
        this.unlinkCard        = this.unlinkCard.bind( this );
        this.cancelUnlinkCard  = this.cancelUnlinkCard.bind( this );
        this.confirmUnlinkCard = this.confirmUnlinkCard.bind( this );

        this.state = {
            trackedCardView:  false,
            confirmingUnlink: null
        }
    }

    componentDidMount() {
        this.trackCardView();
    }

    componentWillReceiveProps( nextProps ) {
        if ( this.props.cardId != nextProps.cardId ) {
            // We're changing the displayed card, so prepare to track a new card view
            this.setState( {
                trackedCardView: false
            } );
        }
    }

    componentDidUpdate( prevProps, prevState ) {
        this.trackCardView();
    }

    trackCardView() {
        // Do not track page views for referenced snapshots
        if ( this.props.card && this.props.card.current && !this.state.trackedCardView ) {
            CardMethods.view.call( { cardId: this.props.card.id } );
            this.setState( { trackedCardView: true } );
        }
    }

    likeCard() {
        const { card } = this.props;
        if ( !card ) {
            return;
        }
        CardMethods.like.call( { cardId: card.id } );
    }

    reportCard() {
        const { card } = this.props;
        if ( !card ) {
            return;
        }
        ModerationMethods.report.call( { entityTypeName: CardCollection.entityName, entityId: card.id } );
    }

    unlinkCard( linkId ) {
        this.setState( {
            confirmingUnlink: linkId
        } );
    }

    cancelUnlinkCard() {
        this.setState( {
            confirmingUnlink: null
        } );
    }

    confirmUnlinkCard() {
        if ( this.state.confirmingUnlink ) {
            CardMethods.unlink.call( { linkId: this.state.confirmingUnlink } );
            this.setState( {
                confirmingUnlink: null
            } );
        }
    }

    /**
     * Show a card
     *
     * Uses history so that we can link directly to a card
     */
    showCard( cardId ) {
        if ( this.props.showCard ) {
            this.props.showCard( cardId );
        } else {
            this.context.router.push( { pathname: '/card/' + cardId } );
        }
    }

    /**
     * Edit the card
     */
    editCard() {
        if ( this.props.editCard ) {
            this.props.editCard( this.props.card.slug );
        } else {
            this.context.router.push( { pathname: '/card/' + this.props.card.slug + '/edit' } );
        }
    }

    getImageOrientation( image ) {
        let imageOrientation = null;
        if ( image ) {
            const copyInfo = image.getCopyInfo( 'card_images_large' );
            if ( copyInfo ) {
                const { width, height } = copyInfo;
                if ( width == null || height == null || width > height ) {
                    imageOrientation = 'horizontal-image';
                } else if ( width < height ) {
                    imageOrientation = 'vertical-image';
                } else {
                    imageOrientation = 'square-image';
                }
            } else {
                imageOrientation = 'horizontal-image';
            }
        } else {
            imageOrientation = 'no-image';
        }
        return imageOrientation;
    }

    getImageStyle( image ) {
        let imageStyle = {};
        if ( image && image.dominantColor != null ) {
            imageStyle.backgroundColor = '#' + image.dominantColor;
        }
        return imageStyle;
    }

    renderLoading() {
        return (
            <div className="card details loading">
                <div className="ui large inverted active indeterminate text loader">
                    {__( 'Loading Card' )}
                </div>
            </div>
        );
    }

    renderImage( image, imageStyle ) {
        return (
            <div className="imageContainer" style={ imageStyle }>
                <Image image={image} size="large" className="cardImage"/>
            </div>
        );
    }

    renderCard() {
        const { project, card, image, files, author, canContribute, /*linkedCards,*/ links, editable } = this.props;
        const { confirmingUnlink }                                                                     = this.state;

        const phase            = project.phases.find( phase => phase._id == card.phase );
        const imageOrientation = this.getImageOrientation( image );
        const imageStyle       = this.getImageStyle( image );
        const hasContent       = card.hasContent;
        const youReported      = card.isReportedBy( Meteor.userId() );
        const availablePhases  = project.phases.filter( p => p.hasStarted && card.getPhaseData( p._id ) != null );

        const sourceLinks = links ? links.filter(link => link.source.id == card.id) : [];
        const destinationLinks = links ? links.filter(link => link.destination.id == card.id) : [];

        return (
            <div className={classNames( "card details", this.props.className, imageOrientation, { 'with-details': hasContent, noContent: !hasContent, snapshot: !card.current } ) }>

                { image && imageOrientation == 'horizontal-image' && this.renderImage( image, imageStyle ) }

                <div className="ui stackable grid content">

                    <div className="ten wide column">

                        {/* CARD IMAGE (if NOT horizontal) */}
                        { image && imageOrientation != 'horizontal-image' && this.renderImage( image, imageStyle ) }

                        {/* SNAPSHOT MESSAGE */}
                        { !card.current &&
                          <div className="snapshot-message">
                              <i className="ui archive icon"/>
                              <div className="content">
                                  <div className="title">{__( "Past Version" )}</div>
                                  <div className="message">{__( "You are currently viewing a past version of the card as it was __date__", { date: moment( card.versionedAt ).fromNow() } )}</div>
                              </div>
                          </div>
                        }

                        {/* TITLE */}
                        <div className="details name" dangerouslySetInnerHTML={{ __html: card.nameMarkdown }}></div>

                        {/* TAGS */}
                        <CardTags tags={card.tags || []}/>

                        {/* DETAILS */}
                        { hasContent &&
                          <div className="details body" dangerouslySetInnerHTML={{ __html: card.contentMarkdown }}></div> }

                        {/* ATTACHMENTS */}
                        { files && files.length > 0 && <div>
                            <h4 className="ui dividing header">{__( 'Attachments' )}</h4>
                            <div className="attachments">
                                { files.map( ( file, index ) => <div key={index} className="attachment">
                                    <div className="fileIcon"><i className="ui file outline icon"/></div>
                                    <div className="fileName">{ file.original.name }</div>
                                    <div className="fileSize">{ filesize( file.original.size ) }</div>
                                    <div className="fileDate"><LiveDate date={file.original.updatedAt} relative={true}/>
                                    </div>
                                    <div className="fileActions">
                                        <a href={file.url()} download={file.original.name} className="ui mini olive right labeled icon button"><i className="cloud download icon"/>{__( 'Download' )}
                                        </a>
                                    </div>
                                </div> ) }
                            </div>
                        </div> }

                        {/* PHASES */}
                        { availablePhases.length > 0 && <h4 className="ui dividing header">{__( 'Phases' )}</h4> }
                        { availablePhases.length > 0 && <div className="phases">
                            { availablePhases.map( p => {
                                return (
                                    <PhaseBlock key={p._id} className="phase" phase={p}>
                                        <PhaseType phase={p} card={card}/>
                                    </PhaseBlock>
                                );
                            } ) }
                        </div> }

                        {/* LINKED CARDS */}
                        { destinationLinks.length > 0 && <div>
                            <h4 className="ui dividing header">{__( 'Incoming Links' )}</h4>
                            <div className="linkedCards">
                                { destinationLinks.map( link => <LinkedCard key={link._id}
                                                                 card={card}
                                                                 link={link}
                                                                 project={project}
                                                                 layout="grid"
                                                                 showCard={this.showCard}
                                                                 className="linked"
                                                                 canUnlink={canContribute}
                                                                 unlinkCard={this.unlinkCard}/>
                                ) }

                                {/* FIXME: Layout grid hack
                                 Hack to get flex rows to leave enough empty space
                                 We leave the same number of empty cards as there are cards to make sure we have enough
                                 */}
                                { links.map( ( link, index ) =>
                                    <div key={index} className="linkedCard"></div> )}
                            </div>
                        </div> }
                        { sourceLinks.length > 0 && <div>
                            <h4 className="ui dividing header">{__( 'Outgoing Links' )}</h4>
                            <div className="linkedCards">
                                { sourceLinks.map( link => <LinkedCard key={link._id}
                                                                            card={card}
                                                                            link={link}
                                                                            project={project}
                                                                            layout="grid"
                                                                            showCard={this.showCard}
                                                                            className="linked"
                                                                            canUnlink={canContribute}
                                                                            unlinkCard={this.unlinkCard}/>
                                ) }

                                {/* FIXME: Layout grid hack
                                 Hack to get flex rows to leave enough empty space
                                 We leave the same number of empty cards as there are cards to make sure we have enough
                                 */}
                                { links.map( ( link, index ) =>
                                    <div key={index} className="linkedCard"></div> )}
                            </div>
                        </div> }

                        {/* Feed */}
                        <div>
                            <h4 className="ui dividing header">{__( 'Activity' )}</h4>
                            <Feed entities={card.id}
                                  archived={!card.current}
                                  maxDate={!card.current ? card.versionedAt : null}
                                  showPreviews={false}/>
                        </div>

                    </div>

                    <div className="six wide column">

                        {/* METADATA */}
                        <div className="details metadata">
                            { author && <Link to={'/profile/' + author._id}><Avatar user={author} className="big"/></Link> }
                            <div className="info">
                                <div className="author">
                                    { author ?
                                      <Link to={'/profile/' + author._id}>
                                          {author.profile.firstName + ' ' + author.profile.lastName}
                                      </Link>
                                        : __( 'Author not found!' )
                                    }
                                    { author && <span className="points">
                                        {author.points}&nbsp;<i className="trophy icon"/>
                                    </span> }
                                </div>
                                <div className="created-date"><LiveDate date={card.createdAt} relative={true}/></div>
                            </div>
                        </div>

                        <div className="ui grid">

                            {/* STATS & ACTIONS */}
                            <div className="sixteen wide column">
                                <div className="details stats actions">
                                    <div className="views stat">
                                        <i className={classNames( 'eye icon', { outline: (card.viewCount || 0) == 0 } )}/>
                                        {__( 'view.count', { count: card.viewCount || 0 } ) }
                                    </div>

                                    <div className="likes stat" onClick={this.likeCard}>
                                        <i className={classNames( 'heart icon', { outline: (card.likeCount || 0) == 0, red: card.likes } )}/>
                                        {__( 'like.count', { count: card.likeCount || 0 } ) }
                                    </div>

                                    <div className="comments stat">
                                        <i className={classNames( 'comment icon', { outline: (card.commentCount || 0) == 0 } )}/>
                                        {__( 'comment.count', { count: card.commentCount || 0 } ) }
                                    </div>

                                    { card.attachmentCount > 0 && <div className="attachments stat">
                                        <i className="attach icon"/>
                                        {__( 'attachment.count', { count: card.attachmentCount } ) }
                                    </div> }

                                    <br/>

                                    <div className={classNames( "report stat", { reported: youReported } )} onClick={this.reportCard}>
                                        <i className="legal icon"/>
                                        { youReported ? __( 'You reported this' ) : __( 'Report' ) }
                                    </div>

                                    { editable && <div onClick={this.editCard} className="edit action">
                                        <i className="pencil icon"/>{__( 'Edit' )}</div> }
                                </div>
                            </div>

                            {/* COMMENTS */}
                            <div className="sixteen wide column cardComments">
                                <Comments objects={card.objectAncestors}
                                          maxDate={!card.current ? card.versionedAt : null}
                                          canContribute={canContribute && phase.isActive }/>
                            </div>

                        </div>

                    </div>

                </div>

                {/* Unlink Modal */}
                <Modal
                    opened={!!confirmingUnlink}
                    onApprove={this.confirmUnlinkCard}
                    onHide={this.cancelUnlinkCard}>
                    <i className="close icon"/>

                    <div className="header">
                        {__( 'Unlink Card' )}
                    </div>
                    <div className="image content">
                        <div className="image">
                            <i className="group icon"/>
                        </div>
                        <div className="description">
                            <p>{ __( 'Are you sure you want to unlink the two cards?' ) }</p>
                        </div>
                    </div>
                    <div className="actions">
                        <div className="ui buttons">
                            <div className="ui red deny button">
                                <i className="remove icon"/>
                                {__( 'No' )}
                            </div>
                            <div className="ui green approve button">
                                <i className="checkmark icon"/>
                                {__( 'Yes' )}
                            </div>
                        </div>
                    </div>
                </Modal>

            </div>
        );
    }

    render() {
        const { loading } = this.props;
        return loading ? this.renderLoading() : this.renderCard();
    }
}

CardDetails.propTypes = {
    cardId:        PropTypes.string.isRequired,
    onFinish:      PropTypes.func,
    showCard:      PropTypes.func,
    editCard:      PropTypes.func,
    loading:       PropTypes.bool.isRequired,
    card:          PropTypes.object.isRequired,
    project:       PropTypes.object.isRequired,
    author:        PropTypes.object.isRequired,
    image:         PropTypes.object,
    files:         PropTypes.arrayOf( PropTypes.instanceOf( File ) ),
    //linkedCards:   PropTypes.arrayOf( PropTypes.object ),
    links:         PropTypes.arrayOf( PropTypes.object ),
    canContribute: PropTypes.bool,
    editable:      PropTypes.bool
};

CardDetails.defaultProps = {
    canContribute: false,
    editable:      false
};

CardDetails.contextTypes = {
    router: PropTypes.object.isRequired
};

export default CardDataWrapper( CardDetails );

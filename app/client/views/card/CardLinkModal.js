"use strict";

import Modal from '../../components/Modal';
import Dialog from '../../components/Dialog';
import { CardLinker } from './CardLinker';
import CardLinkerDataWrapper from './CardLinkerDataWrapper';

import React, { Component, PropTypes } from "react";

export class CardLinkModal extends Component {

    render() {
        const { loading, linkCardId, linkCard, toCardId, toCard, onRequestClose } = this.props;
        const ready = linkCard != null && toCard != null;
        return (
            <Modal opened={ linkCardId != null && toCardId != null } onRequestClose={onRequestClose}>
                { loading && <div className="ui large inverted active indeterminate text loader">
                    {__( 'Loading Card' )}
                </div> }
                <Dialog onRequestClose={onRequestClose}>
                    { !loading && ready && <CardLinker key={`${linkCard.id}-${toCard.id}`} {...this.props} onFinished={onRequestClose}/> }
                </Dialog>
            </Modal>
        );
    }
}

CardLinkModal.propTypes = {
    onRequestClose: PropTypes.func.isRequired
};

export default CardLinkerDataWrapper(CardLinkModal);
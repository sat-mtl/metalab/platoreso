"use strict";

import React, { Component, PropTypes } from "react";
import { Link } from 'react-router';

import { ConnectMeteorData } from '../../lib/ReactMeteorData'
import CardPreview from './CardPreview';

/**
 * Wrapper component to display a linked card from the CardLink passed to it in the "link" prop.
 * It wraps CardPreview with stuff related to card links.
 */
export class LinkedCard extends Component {

    constructor( props ) {
        super( props );

        this.showCard   = this.showCard.bind( this );
        this.unlinkCard = this.unlinkCard.bind( this );
    }

    /**
     * View the card details
     */
    showCard() {
        const { card, sourceCard, destinationCard } = this.props;
        const linkedCard = card.id == sourceCard.id ? destinationCard : sourceCard;
        if ( this.props.showCard ) {
            this.props.showCard( linkedCard.uri );
        } else {
            this.context.router.push( { pathname: '/card/' + linkedCard.uri } );
        }
    }

    /**
     * Unlink card
     */
    unlinkCard() {
        const { link, unlinkCard } = this.props;
        if ( link ) {
            unlinkCard( link._id );
        }
    }

    render() {
        const { link, card, sourceCard, destinationCard, canUnlink, unlinkCard, editable } = this.props;

        if ( !sourceCard || !destinationCard ) {
            return null;
        }

        // We get passed a link and a card (the for which the links are displayed)
        // So we have to check which of of the source/destination in the link to display
        const originalCard = card.id == sourceCard.id ? sourceCard : destinationCard;
        const linkedCard = card.id == sourceCard.id ? destinationCard : sourceCard;
        const isSnapshot = !sourceCard.current;

        // You can either unlink if you have permissions to edit the card or parent (card details in this case) says you can
        const unlinkable = unlinkCard && !isSnapshot && ( canUnlink || editable);

        return (
            <div className="linkedCard">
                {/* Card, yup the name are backwards since the preview considered the linked card to be the "other" card */}
                <CardPreview {...this.props} card={linkedCard} linkedCard={originalCard} showLinkBadges={true}/>

                {/* Unlink */}
                <div className="buttonsContainer">
                    <div className="ui mini cyan button unlink" onClick={this.showCard}>{__( 'View Card' )}</div>
                    { unlinkable &&
                      <div className="ui mini orange button unlink" onClick={this.unlinkCard}>{__( 'Unlink' )}</div> }
                </div>
            </div>
        );
    }

}

LinkedCard.contextTypes = {
    router: PropTypes.object.isRequired
};

LinkedCard.propTypes    = _.extend( {
    link:            PropTypes.object.isRequired,
    card:            PropTypes.object.isRequired,
    sourceCard:      PropTypes.object,
    destinationCard: PropTypes.object,
    canUnlink:       PropTypes.bool,
    unlinkCard:      PropTypes.func,
}, _.clone( CardPreview.propTypes ) );
LinkedCard.defaultProps = _.extend( {}, _.clone( CardPreview.defaultProps ) );

export default ConnectMeteorData( props => ({
    sourceCard:      props.link.getSourceLinkedCard(),
    destinationCard: props.link.getDestinationLinkedCard()
}) )( LinkedCard );
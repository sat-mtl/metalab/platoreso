"use strict";

import React, { Component, PropTypes } from "react";

export default class BaseCardPreview extends Component {

}

BaseCardPreview.propTypes = {
    project:          PropTypes.object.isRequired,
    phase:            PropTypes.object,
    card:             PropTypes.object.isRequired,
    author:           PropTypes.object,
    image:            PropTypes.object,
    editable:         PropTypes.bool.isRequired,
    //imageOrientation: PropTypes.string.isRequired,
    imageStyle:       PropTypes.object.isRequired,
    youLike:          PropTypes.bool.isRequired,
    youReported:      PropTypes.bool.isRequired,
    showCard:         PropTypes.func.isRequired,
    editCard:         PropTypes.func,
    likeCard:         PropTypes.func.isRequired,
    reportCard:       PropTypes.func.isRequired,
    showLinked:       PropTypes.bool.isRequired
};

BaseCardPreview.defaultProps = {
    editable: false,
    hasDetails: false,
    youLike: false,
    youReported: false
};
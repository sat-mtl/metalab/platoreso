"use strict";

import React, {Component, PropTypes} from "react";
import {Link} from 'react-router';
import classNames from "classnames";
import LiveDate from '../../widgets/LiveDate';
import CardTags from '../../card/view/CardTags';
import BaseCardPreview from './BaseCardPreview';

import UserHelpers from "/imports/accounts/UserHelpers";

const EXCERPT_LENGTH = 140;

export default class ListCardPreview extends BaseCardPreview {

    render() {
        const {
                  card,
                  author,
                  editable,
                  youLike,
                  showCard,
                  editCard,
                  likeCard
                  } = this.props;

        const hasAttachments = card.attachmentCount != null && card.attachmentCount > 0;

        return (
            <div className="cardLayout">

                {/* CARD CONTENT */}
                <div className="content" onClick={showCard}>

                    {/* TITLE */}
                    <div className="name" dangerouslySetInnerHTML={{__html: card.nameMarkdown }}></div>

                    {/* DETAILS */}
                    { card.hasExcerpt &&
                      <div className="body" dangerouslySetInnerHTML={{__html: card.excerptMarkdown(EXCERPT_LENGTH) }}></div> }

                    {/* TAGS */}
                    { card.tags && <CardTags tags={card.tags}/> }
                </div>

                {/* CARD META */}
                <div className="metadata">
                    <div className="info">
                        { author && <div className="author"><Link to={'/profile/'+author._id}>
                            {UserHelpers.getDisplayName( author )}
                        </Link>&nbsp;</div> }
                        <div className="created-date"><LiveDate date={ card.createdAt } relative={true}/></div>
                    </div>

                    {/* CARD STATS & ACTIONS */}
                    <div className="stats">
                        <div className="views stat" onClick={showCard}>
                            <i className={classNames('eye icon', { outline: (card.viewCount || 0) == 0 })}/>
                            { card.viewCount || 0 }
                        </div>
                        <div className={classNames("likes stat", {liked: youLike} )} onClick={likeCard}>
                            <i className={classNames('heart icon', { outline: (card.likeCount || 0) == 0})}/>
                            {card.likeCount || 0}
                        </div>
                        <div className="comments stat" onClick={showCard}>
                            <i className={classNames('comment icon', { outline: (card.commentCount || 0) == 0 })}/>
                            {card.commentCount || 0 }
                        </div>
                        { hasAttachments && <div className="attachments stat" onClick={showCard}>
                            <i className="attach icon"/>
                            { card.attachmentCount }
                        </div> }
                    </div>

                    {/* CARD ACTIONS */}
                    <div className="actions">
                        { editCard && editable &&
                          <div onClick={editCard} className="edit action"><i className="pencil icon"/></div> }
                    </div>
                </div>

            </div>
        );
    }
}
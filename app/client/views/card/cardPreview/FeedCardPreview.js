"use strict";

import React, {Component, PropTypes} from "react";
import {Link} from 'react-router';
import LiveDate from '../../widgets/LiveDate';
import Image from '../../widgets/Image';
import Avatar from '../../widgets/Avatar';
import BaseCardPreview from './BaseCardPreview';

import UserHelpers from "/imports/accounts/UserHelpers";

const EXCERPT_LENGTH = 400;

export default class FeedCardPreview extends BaseCardPreview {

    render() {
        const {
                  card,
                  image,
                  author,
                  imageStyle,
                  showCard
                  } = this.props;

        return (
            <div className="cardLayout">

                {/* CARD IMAGE */}
                <div className="cardImageContainer" style={ imageStyle } onClick={showCard}>
                    { image && <Image image={image} size="medium" className="cardImage" asBackground={false}/> }
                </div>

                {/* CARD CONTENT */}
                <div className="content" onClick={showCard}>

                    {/* TITLE */}
                    <div className="name" dangerouslySetInnerHTML={{__html: card.nameMarkdown }}></div>

                    {/* DETAILS */}
                    { card.hasExcerpt &&
                    <div className="body" dangerouslySetInnerHTML={{__html: card.excerptMarkdown(EXCERPT_LENGTH) }}></div> }

                </div>

                {/* CARD META */}
                <div className="metadata">
                    { author && <Link to={'/profile/'+author._id} className="profile"><Avatar user={author}/></Link> }
                    <div className="info">
                        { author && <div className="author"><Link to={'/profile/'+author._id}>
                            {UserHelpers.getDisplayName( author )}
                        </Link></div> }
                        <div className="created-date"><LiveDate date={ card.createdAt } relative={true}/></div>
                    </div>
                </div>

            </div>
        );
    }
}
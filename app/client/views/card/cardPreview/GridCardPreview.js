"use strict";

import React, {Component, PropTypes} from "react";
import {Link} from 'react-router';
import classNames from "classnames";
import LiveDate from '../../widgets/LiveDate';
import Image from '../../widgets/Image';
import Avatar from '../../widgets/Avatar';
import CardTags from '../view/CardTags';
import BaseCardPreview from './BaseCardPreview';

import UserHelpers from "/imports/accounts/UserHelpers";

const EXCERPT_LENGTH       = 400;
const IMAGE_EXCERPT_LENGTH = 280;

export default class GridCardPreview extends BaseCardPreview {

    render() {
        const {
                  card,
                  image,
                  author,
                  editable,
                  imageStyle,
                  youLike,
                  showCard,
                  editCard,
                  likeCard
                  } = this.props;

        let likeMessage = '';
        if ( youLike ) {
            if ( card.likeCount > 1 ) {
                likeMessage = __( 'you.like.count', { count: card.likeCount - 1 } );
            } else {
                likeMessage = __( 'You like this' );
            }
        } else {
            likeMessage = __( 'like.count', { count: card.likeCount || 0 } );
        }
        
        const hasAttachments = card.attachmentCount != null && card.attachmentCount > 0;

        return (
            <div className="cardLayout">

                {/* CARD IMAGE */}
                <div className="cardImageContainer" style={ imageStyle } onClick={showCard}>
                    { image && <Image image={image} size="medium" className="cardImage" asBackground={true}/> }
                </div>

                {/* CARD CONTENT */}
                <div className="content" onClick={showCard}>

                    {/* TITLE */}
                    <div className="name" dangerouslySetInnerHTML={{__html: card.nameMarkdown }}></div>

                    {/* DETAILS */}
                    { card.hasExcerpt &&
                      <div className="body" dangerouslySetInnerHTML={{__html: card.excerptMarkdown( image ? IMAGE_EXCERPT_LENGTH : EXCERPT_LENGTH) }}></div> }

                    {/* TAGS */}
                    { card.tags && <CardTags tags={card.tags}/> }

                </div>


                {/* CARD META */}
                <div className="metadata">
                    { author && <Link to={'/profile/'+author._id} className="profile"><Avatar user={author}/></Link> }
                    <div className="info">
                        { author && <div className="author"><Link to={'/profile/'+author._id}>
                            {UserHelpers.getDisplayName( author )}
                        </Link></div> }
                        <div className="created-date"><LiveDate date={ card.createdAt } relative={true}/></div>
                    </div>

                    {/* CARD STATS & ACTIONS */}
                    <div className="stats">
                        <div className="views stat" onClick={showCard}>
                            <i className={classNames('eye icon', { outline: (card.viewCount || 0) == 0 })}/>
                            {card.viewCount || 0 /*__( 'view.count', {count: card.viewCount || 0} )*/}
                        </div>
                        <div className={classNames("likes stat", {liked: youLike} )} onClick={likeCard}>
                            <i className={classNames('heart icon', { outline: (card.likeCount || 0) == 0})}/>
                            {card.likeCount || 0 /*likeMessage*/}
                        </div>
                        <div className="comments stat" onClick={showCard}>
                            <i className={classNames('comment icon', { outline: (card.commentCount || 0) == 0 })}/>
                            {card.commentCount || 0 /*__( 'comment.count', {count: card.commentCount || 0} )*/}
                        </div>
                        { hasAttachments && <div className="attachments stat" onClick={showCard}>
                            <i className="attach icon"/>
                            { card.attachmentCount }
                        </div> }
                    </div>

                    <div className="actions">
                        { editCard && editable &&
                          <div onClick={editCard} className="edit action"><i className="pencil icon"/></div> }
                    </div>
                </div>

            </div>
        );
    }
}
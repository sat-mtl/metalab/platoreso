"use strict";

import CardCollection from "/imports/cards/CardCollection";
import { ConnectMeteorData } from '../../lib/ReactMeteorData'

/**
 * Meteor Data Wrapper
 */
export default ConnectMeteorData( props => {
    const linkCardLoading = !!( props.linkCardId && !Meteor.subscribe( 'card', props.linkCardId ).ready() );
    const linkCard        = props.linkCardId ? CardCollection.findOne( { $or: [{ id: props.linkCardId }, { slug: props.linkCardId }], current: true } ) : null;
    const toCardLoading   = !!( props.toCardId && !Meteor.subscribe( 'card', props.toCardId ).ready() );
    const toCard          = props.toCardId ? CardCollection.findOne( { $or: [{ id: props.toCardId }, { slug: props.toCardId }], current: true } ) : null;
    const projectLoading  = !!( props.linkCardId && ( !linkCard || !Meteor.subscribe( 'project', linkCard.project ).ready() ) );
    return {
        loading:         linkCardLoading || toCardLoading || projectLoading,
        linkCard:        linkCard,
        linkCardImage:   linkCard && linkCard.getImage(),
        linkCardProject: linkCard && linkCard.getProject(),
        toCard:          toCard,
        toCardImage:     toCard && toCard.getImage(),
        project:         linkCard && linkCard.getProject()
    }
} );
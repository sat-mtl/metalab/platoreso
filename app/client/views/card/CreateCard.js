"use strict";

// NPM
import React, { Component, PropTypes } from "react";
import ReactDOM from "react-dom";
import classNames from "classnames";
import async from "async";
//import moment from "moment";

// IMPORTS
import { FS } from "meteor/cfs:base-package";
import CardMethods from "/imports/cards/CardMethods";
import CardImagesCollection from "/imports/cards/images/CardImagesCollection";
import CardAttachmentsCollection from "/imports/cards/attachments/CardAttachmentsCollection";

// JSX
import Modal from '../../components/Modal';
import Dialog from '../../components/Dialog';
import Form from '../../components/forms/Form';
import CardForm from './edit/CardForm';

const { File } = FS;

export class  CreateCard extends Component {

    constructor( props ) {
        super( props );

        this.pictureChanged = this.pictureChanged.bind( this );
        this.filesChanged   = this.filesChanged.bind( this );
        this.doUploads      = this.doUploads.bind( this );
        this.submitForm     = this.submitForm.bind( this );
        this.createCard     = this.createCard.bind( this );

        // Initial state
        this.state = {
            error:   null,
            saving:  null,
            picture: null,
            files:   []
        };
    }

    pictureChanged( files ) {
        this.setState( {
            picture: ( files && files.length > 0 ) ? files[0] : null
        } );
    }

    filesChanged( files ) {
        this.setState( {
            files: files
        } );
    }

    submitForm() {
        $( ReactDOM.findDOMNode( this.refs.cardCreationForm ) ).form( 'submit' );
    }

    createCard( values ) {
        if ( this.state.saving ) {
            return;
        }
        this.setState( { saving: true } );

        // Semantic-ui tags are tricky
        values.tags = values.tags || [];

        // Published date
        //values.publishedAt = moment(values.publishedDate).toDate();
        //values.publishedAt.setHours( values.publishedHour, values.publishedMinute, 0, 0 );
        //delete values.publishedDate;
        //delete values.publishedHour;
        //delete values.publishedMinute;
        
        CardMethods.create.call( values, ( err, cardId ) => {
            if ( err ) {
                this.setState( { saving: false, error: err } );
            } else {
                this.doUploads( cardId );
            }
        } );
    }

    doUploads( cardId ) {
        let tasks = [];

        // Card is created, we can now upload the image
        if ( this.state.picture ) {
            let file    = new File( this.state.picture );
            file.card = cardId;
            file.newCard = true;
            tasks.push( callback => CardImagesCollection.insert( file, callback ) );
        }

        if ( this.state.files ) {
            this.state.files.forEach( file => {
                //let f    = new File( file );
                file.card = cardId;
                file.newCard = true;
                tasks.push( callback => CardAttachmentsCollection.insert( file, callback ) );
            } );
        }

        async.parallelLimit( tasks, 3, ( err, result ) => {
            this.setState( { saving: false } );
            if ( err ) {
                this.setState( { error: err } );
            } else {
                if ( this.props.onFinished ) {
                    this.props.onFinished();
                }
            }
        } );
    }

    render() {
        const { project, phaseId } = this.props;
        const { saving } = this.state;

        if ( !project || !phaseId ) {
            return null;
        }

        const phase = project.phases.find( phase => phase._id == phaseId );
        if ( !phase ) {
            return null;
        }

        return (
            <div className="editor createCard">

                <div className="header">
                    {__( 'Create Card' )}
                </div>

                <Form id={"card-creation-form-"+phase._id}
                      ref="cardCreationForm"
                      key={phase._id}
                      error={this.state.error}
                      onSuccess={this.createCard}>

                    <input type="hidden" name="project" value={project._id}/>
                    <input type="hidden" name="phase" value={phase._id}/>

                    <CardForm project={project}
                              phase={phase}
                              pictureChanged={this.pictureChanged}
                              filesChanged={this.filesChanged}/>

                </Form>

                <div className="actions">
                    <div className="ui buttons">
                        <div className="ui cancel button" onClick={this.props.onFinished}>
                            <i className="remove icon"/>
                            {__( 'Cancel' )}
                        </div>
                        <div className={classNames("ui green button",{ loading: saving, disabled: saving })} onClick={this.submitForm}>
                            <i className="plus icon"/>
                            {__( 'Create' )}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

CreateCard.propTypes = {
    project:    PropTypes.object.isRequired,
    phaseId:    PropTypes.string.isRequired,
    onFinished: PropTypes.func
};

export class CreateCardModal extends Component {

    render() {
        const { project, phaseId, onRequestClose } = this.props;
        return (
            <Modal opened={ !!project && !!phaseId } onRequestClose={onRequestClose}>
                <Dialog onRequestClose={onRequestClose}>
                    { project && phaseId &&
                      <CreateCard key={project._id + '.' + phaseId} {...this.props} onFinished={onRequestClose}/> }
                </Dialog>
            </Modal>
        );
    }
}

CreateCardModal.propTypes = {
    project:        PropTypes.object.isRequired,
    phaseId:        PropTypes.string,
    onRequestClose: PropTypes.func.isRequired
};
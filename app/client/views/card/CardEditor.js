"use strict";

// NPM
import React, { Component, PropTypes } from "react";
import ReactDOM from "react-dom";
import update from "react-addons-update";
import classNames from "classnames";
import async from "async";
//import moment from "moment";
import { unflatten } from 'flat';

import { FS } from "meteor/cfs:base-package";

// IMPORTS
import formDiff from "/imports/utils/formDiff";
import CardMethods from "/imports/cards/CardMethods";
import CardImagesCollection from "/imports/cards/images/CardImagesCollection";
import CardAttachmentsCollection from "/imports/cards/attachments/CardAttachmentsCollection";

// JSX
import CardEditorDataWrapper from './CardEditorDataWrapper';
import Form from '../../components/forms/Form';
import CardForm from './edit/CardForm';

const { File } = FS;

export class CardEditor extends Component {

    constructor( props ) {
        super( props );

        // Bind methods, not automatic in current React version when using classes
        this.pictureChanged = this.pictureChanged.bind( this );
        this.filesChanged   = this.filesChanged.bind( this );
        this.fileRemoved    = this.fileRemoved.bind( this );
        this.submitForm     = this.submitForm.bind( this );
        this.updateCard     = this.updateCard.bind( this );
        this.onCancel       = this.onCancel.bind( this );
        this.onFinish       = this.onFinish.bind( this );
        this.confirmRemove  = this.confirmRemove.bind( this );
        this.cancelRemove   = this.cancelRemove.bind( this );
        this.remove         = this.remove.bind( this );

        // Initial state
        this.state = {
            error:             null,
            saving:            false,
            picture:           null,
            removedPicture:    false,
            confirmingRemoval: false,
            pendingFiles:      [],
            removedFiles:      []
        };
    }

    pictureChanged( files ) {
        this.setState( {
            picture:        ( files && files.length > 0 ) ? files[0] : null,
            removedPicture: files == null // Changing for null means removing as opposed to the default null of this.state.picture
        } );
    }

    filesChanged( pendingFiles ) {
        this.setState( {
            pendingFiles: pendingFiles
        } );
    }

    fileRemoved( removedFile ) {
        this.setState( {
            removedFiles: update( this.state.removedFiles, { $push: [removedFile._id] } )
        } );
    }

    submitForm() {
        $( ReactDOM.findDOMNode( this.refs.cardEditForm ) ).form( 'submit' );
    }

    updateCard( values ) {
        if ( this.state.saving ) {
            return;
        }
        this.setState( { saving: true } );

        const { card } = this.props;
        const { removedFiles, removedPicture } = this.state;

        let uploads = [];
        let tasks   = [];

        // Images
        if ( this.state.picture ) {
            let file  = new File( this.state.picture );
            file.card = card.id;
            uploads.push( callback => CardImagesCollection.insert( file, callback ) );
        }

        // Files
        if ( this.state.pendingFiles ) {
            this.state.pendingFiles.forEach( file => {
                file.card = card.id;
                uploads.push( callback => CardAttachmentsCollection.insert( file, callback ) );
            } );
        }

        // Push upload stack as a task
        tasks.push( callback => async.parallelLimit( uploads, 3, callback ) );

        // Data

        // Semantic-ui tags are tricky
        values.tags = values.tags || [];

        // Turn the form values into a proper object and compute diff
        const changes = formDiff( unflatten( values ), card );

        // Call update if we have changes or uploads pending
        if ( !_.isEmpty( changes ) || uploads.length || removedFiles.length || removedPicture ) {
            tasks.push( callback => CardMethods.update.call( {
                id:            card.id,
                                changes,
                changedPicture: !!this.state.picture,
                removedPicture: removedPicture,
                addedFiles:     (this.state.pendingFiles && this.state.pendingFiles.length > 0 ),
                removedFiles:   removedFiles
            }, callback ) );
        }

        // Do tasks
        async.series( tasks, ( err, result ) => {
            this.setState( { saving: false } );
            if ( err ) {
                console.error( err );
                this.setState( { error: err } );
            } else {
                this.onFinish();
            }
        } );
    }

    confirmRemove() {
        this.setState( {
            confirmingRemoval: true
        } );
    }

    cancelRemove() {
        this.setState( {
            confirmingRemoval: false
        } );
    }

    remove() {
        this.setState( {
            confirmingRemoval: false
        } );

        CardMethods.archive.call( { cardId: this.props.card.id }, ( err ) => {
            if ( err ) {
                this.setState( { error: err } );
            } else {
                this.onFinish();
            }
        } );
    }

    onCancel() {
        this.onFinish();
    }

    onFinish() {
        if ( this.props.onFinished ) {
            this.props.onFinished();
        } else {
            this.context.router.goBack();
        }
    }

    renderLoading() {
        return (
            <div className="card details loading">
                <div className="ui large inverted active indeterminate text loader">
                    {__( 'Loading Card' )}
                </div>
            </div>
        );
    }

    renderActions() {
        const { confirmingRemoval, saving } = this.state;

        if ( confirmingRemoval ) {
            return (
                <div className="actions">
                    {__( 'Are you sure you want to remove the selected card?' )}&nbsp;
                    <div className="ui buttons">
                        <div className="ui red button" onClick={this.cancelRemove}>
                            <i className="remove icon"/>
                            {__( 'No' )}
                        </div>
                        <div className="ui green cancel button" onClick={this.remove}>
                            <i className="checkmark icon"/>
                            {__( 'Yes' )}
                        </div>
                    </div>
                </div>
            );
        } else {
            return (
                <div className="actions">
                    <div className="ui red button" onClick={this.confirmRemove}>
                        <i className="archive icon"/>
                        {__( 'Remove' )}
                    </div>
                    <div className="ui buttons">
                        <div className="ui cancel button" onClick={this.onCancel}>
                            <i className="remove icon"/>
                            {__( 'Cancel' )}
                        </div>
                        <div className={classNames( "ui green button", { loading: saving, disabled: saving } )}
                             onClick={this.submitForm}>
                            <i className="plus icon"/>
                            {__( 'Save' )}
                        </div>
                    </div>
                </div>
            );
        }
    }

    renderCard() {
        const { project, card, image, files } = this.props;

        if ( !project || !card ) {
            return null;
        }

        const phase = project.phases.find( phase => phase._id == card.phase );
        if ( !phase ) {
            return null;
        }

        return (
            <div className={classNames( this.props.className, "editor editCard" )}>

                <div className="header">
                    {__( 'Edit Card' )}
                </div>

                <Form id={"card-edit-form-" + card.id}
                      ref="cardEditForm"
                      key={card.id}
                      error={this.state.error}
                      onSuccess={this.updateCard}>

                    <CardForm card={card}
                              project={project}
                              phase={phase}
                              image={image}
                              files={files}
                              pictureChanged={this.pictureChanged}
                              filesChanged={this.filesChanged}
                              fileRemoved={this.fileRemoved}/>

                </Form>

                { this.renderActions() }

            </div>
        );
    }

    render() {
        const { loading } = this.props;
        return loading ? this.renderLoading() : this.renderCard();
    }
}

CardEditor.propTypes = {
    loading:    PropTypes.bool.isRequired,
    card:       PropTypes.object.isRequired,
    image:      PropTypes.object,
    project:    PropTypes.object,
    onFinished: PropTypes.func
};

CardEditor.contextTypes = {
    router: PropTypes.object.isRequired
};

export default CardEditorDataWrapper( CardEditor );

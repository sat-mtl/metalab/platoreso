"use strict";

import React, { Component, PropTypes } from "react";

export default class Warning extends Component {
    render() {
        const { environment } = this.props;

        return (
            <a href="//platoreso.sat.qc.ca" className="warning nag">
                <div className="title">{__('Warning, __environmentName__ environment!', { environmentName: environment })}</div>
                <div className="message">{__('Click this message to be redirected to platoreso.sat.qc.ca')}</div>
            </a>
        );
    }
}

Warning.propTypes = {
    environment: PropTypes.string.isRequired
};
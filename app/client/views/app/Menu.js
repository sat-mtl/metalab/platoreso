"use strict";

import React, { Component, PropTypes } from "react";
import ReactDOM from "react-dom";
import { Link } from "react-router";
import classNames from "classnames";
import AdminMenu from "./menu/AdminMenu";
import ModerationMenu from "./menu/ModerationMenu";
import SettingsMenu from "./menu/SettingsMenu";
import AddMenu from "./menu/AddMenu";
import LanguageMenu from "./menu/LanguageMenu";
import MessagesMenu from "./menu/MessagesMenu";
import NotificationsMenu from "./menu/NotificationsMenu";
import ProfileMenu from "./menu/ProfileMenu";
import ProjectsMenu from "./menu/ProjectsMenu";

export default class Menu extends Component {
    constructor( props ) {
        super( props );
        this.toggle        = this.toggle.bind( this );
        this.closeOnResize = this.closeOnResize.bind( this );
        this.state         = { opened: false };
    }

    toggle() {
        this.setState( { opened: !this.state.opened } );
    }

    closeOnResize() {
        this.setState( { opened: false } );
    }

    componentDidMount() {
        window.addEventListener( 'resize', this.closeOnResize );
    }

    componentWillReceiveProps( nextProps ) {
        if ( this.props.location.pathname != nextProps.location.pathname ) {
            this.setState( { opened: false } );
        }
    }

    componentWillUnmount() {
        window.removeEventListener( 'resize', this.scrollWhereNeeded );
    }

    render() {
        const { user }                     = this.props;
        const { opened }                   = this.state;
        return (
            <nav ref="mainMenu" id="mainMenu" className={ classNames( "ui inverted small menu top fixed", { opened } )}>
                <div className="static item">
                    <Link to={ user ? "/dashboard" : "/" } className="image logo item">
                        <img srcSet="/images/logo@2x.png 2x" src="/images/logo.png" title={__( 'PlatoReso' )} className="ui image"/>
                    </Link>
                    <div className="open link item" onClick={this.toggle}><i className="content icon"/></div>
                </div>
                <ProjectsMenu/>
                { user && <AddMenu user={user}/> }
                <div className={ classNames( "menu", { right: !opened } )}>
                    { user && <ModerationMenu user={user}/> }
                    { user && <AdminMenu user={user}/> }
                    { user && <SettingsMenu user={user}/> }
                    { user && <MessagesMenu user={user}/> }
                    { user && <NotificationsMenu user={user}/> }
                    <LanguageMenu/>
                    <ProfileMenu user={user}/>
                </div>
            </nav>
        );
    }
}
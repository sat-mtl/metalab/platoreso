"use strict";

import React, { Component, PropTypes } from "react";
import {ConnectMeteorData} from '../../lib/ReactMeteorData';
import ContainedPage from '../layouts/ContainedPage';
import GroupList from '../group/common/GroupList';
import ProjectList, { RecentProjects, TopProjects, TrendingProjects } from "../project/common/ProjectList";
import Feed from '../feed/Feed';

export class Dashboard extends Component {

    render() {
        const { user } = this.props;

        return (
            <ContainedPage id="dashboard">

                <div className="eight wide column">
                    <div className="ui grid">
                        <div className="sixteen wide column groups-intro">
                            {__( 'Groups' )}
                        </div>
                        <GroupList/>

                        {/*<div className="sixteen wide column projects-intro">
                            {__( 'Projects' )}
                        </div>
                        <ProjectList/>*/}

                        <div className="sixteen wide column projects-intro">
                            {__( 'Trending Projects' )}
                        </div>
                        <TrendingProjects limit={3}/>

                        <div className="sixteen wide column projects-intro">
                            {__( 'Top Projects' )}
                        </div>
                        <TopProjects limit={3}/>

                        <div className="sixteen wide column projects-intro">
                            {__( 'Recently Added Projects' )}
                        </div>
                        <RecentProjects limit={3}/>

                    </div>
                </div>

                {/* Feed Last Column */}
                <div className="eight wide column feed user-feed">
                    <Feed entities={user._id}/>
                </div>

            </ContainedPage>
        );
    }

}

Dashboard.propTypes = {
    user: PropTypes.object.isRequired
};

export default ConnectMeteorData( props => {
    return {
        user: Meteor.user()
    }
} )( Dashboard );
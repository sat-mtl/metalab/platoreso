"use strict";

import React, { Component, PropTypes } from "react";
import {Link} from "react-router";
import { Counts } from "meteor/tmeasday:publish-counts";
import { ConnectMeteorData } from "../../lib/ReactMeteorData";
import ContainedPage from "../layouts/ContainedPage";
import GroupList from "../group/common/GroupList";
import { RecentProjects, TopProjects, TrendingProjects } from "../project/common/ProjectList";

export class Home extends Component {

    render() {
        const { ideaCountLoading, ideaCount, user } = this.props;

        return (
            <ContainedPage id="home">

                <header className="sixteen wide column welcome">
                    <h2 className="header">
                        <img srcSet="/images/home-logo@2x.png 2x" src="/images/home-logo.png" title={__( 'PlatoReso' )}/>
                        <div className="content">
                            {__( 'PlatoReso is a collaborative platform aiming to solve innovation challenges.' )}
                            <br/>
                            {__( 'Join the community and help transform inspiring ideas into amazing projects.' )}
                        </div>
                    </h2>
                </header>
                { !user &&
                  <div className="sixteen wide column join">
                      <div className="ui grid">
                          <div className="sixteen wide center aligned column">
                              <Link to="/login" className="ui massive olive button">{__( 'Join' )}</Link>
                          </div>
                      </div>
                  </div>
                }
                <div className="sixteen wide column statistics">
                    <div className="ui grid">
                        <div className="sixteen wide column ideas">
                            <i className="huge idea icon"/>
                            <div>{ ideaCountLoading ? __( 'Loading ideas...' ) : __( '__ideaCount__ ideas shared', { ideaCount: ideaCount } )}</div>
                        </div>
                    </div>
                </div>
                <div className="sixteen wide column">
                    <div className="ui grid">
                        <div className="sixteen wide column groups-intro">
                            {__( 'Groups' )}
                        </div>
                        <GroupList/>
                    </div>
                </div>

                <div className="sixteen wide column">
                    <div className="ui grid">
                        <div className="sixteen wide column projects-intro">
                            {__( 'Trending Projects' )}
                        </div>
                        <TrendingProjects limit={3}/>
                    </div>
                </div>

                <div className="sixteen wide column">
                    <div className="ui grid">
                        <div className="sixteen wide column projects-intro">
                            {__( 'Top Projects' )}
                        </div>
                        <TopProjects limit={3}/>
                    </div>
                </div>

                <div className="sixteen wide column">
                    <div className="ui grid">
                        <div className="sixteen wide column projects-intro">
                            {__( 'Recently Added Projects' )}
                        </div>
                        <RecentProjects limit={3}/>
                    </div>
                </div>

            </ContainedPage>
        );
    }

}

Home.propTypes = {
    ideaCountLoading: PropTypes.bool.isRequired,
    ideaCount:        PropTypes.number,
    user:             PropTypes.object
};

export default ConnectMeteorData( props => {
    return {
        ideaCountLoading: !Meteor.subscribe( 'card/count' ).ready(),
        ideaCount:        Counts.get( 'card/count' ),
        user:             Meteor.user()
    }
} )( Home );
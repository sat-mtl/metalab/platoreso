"use strict";

import UserHelpers from "/imports/accounts/UserHelpers";
import {Link} from 'react-router';
import links from '../../settings/menu/Links';
import DropDown from '../../../components/semanticui/modules/Dropdown';

import React, { Component, PropTypes } from "react";
import classNames from "classnames";

export default class SettingsMenu extends Component {
    render() {
        const {user} = this.props;

        if ( !UserHelpers.isAdmin( user ) ) {
            return null;
        }

        const { router } = this.context;

        return (
            <DropDown className={classNames("teal scrolling item",{selected: router.isActive({ pathname: '/settings' }, false)})}>
                <span><i className="settings icon"/><span className="name">{__('Settings')}</span></span>
                <i className="dropdown icon"/>
                <div className="menu">
                    <div className="header">
                        {__( 'Settings' )}
                    </div>
                    { links.filter( link => link.enabled ).map( link => (
                        <Link key={link.to}
                              to={link.to}
                              onlyActiveOnIndex={link.index}
                              className="teal item"
                              activeClassName="active">
                            <i className={ classNames( link.icon, 'icon' )}/>
                            {link.label}
                        </Link>
                    ) ) }
                </div>
            </DropDown>
        );
    }
}

SettingsMenu.propTypes = {
    user: PropTypes.object
};

SettingsMenu.contextTypes = {
    router: PropTypes.object.isRequired
};
"use strict";

import React, {Component, PropTypes} from "react";
import {Link} from "react-router";
import UserHelpers from "/imports/accounts/UserHelpers";
import AccountHelpers from "/imports/accounts/AccountHelpers";
import DropDown from "../../../components/semanticui/modules/Dropdown";
import Avatar from "../../widgets/Avatar";

export default class ProfileMenu extends Component {

    constructor( props ) {
        super( props );
        this.logout = this.logout.bind( this );
    }

    logout() {
        AccountHelpers.logout( () => {
            //TODO: This is exactly the kind of things that warrants a mediator of some kind
            this.context.router.push( { pathname: '/' } );
        } );
    }

    render() {
        const { user } = this.props;

        if ( user ) {
            const name = UserHelpers.getDisplayName( user );

            return (
                <DropDown className="profile scrolling item">
                    {name}
                    <div className="ui tiny teal label">
                        <i className="trophy icon"/>&nbsp;{user.points}
                    </div>
                    <Avatar user={user}/>
                    {/*<i className="dropdown icon"/>*/}
                    <div className="menu">
                        <Link to="/profile/edit"
                              className="teal item"
                              activeClassName="active">
                            <i className="user icon"/>
                            {__( 'Edit Profile' )}
                        </Link>
                        <Link to="/profile/edit/code"
                              className="teal item"
                              activeClassName="active">
                            <i className="key icon"/>
                            {__( 'Enter Code' )}
                        </Link>
                        <div className="red item" onClick={this.logout}>
                            <i className="sign out icon"/>
                            {__( 'Logout' )}
                        </div>
                    </div>
                </DropDown>
            );
        } else {
            return (
                <Link to="/login" className="item">{__( 'Login' )}</Link>
            );
        }
    }
}

ProfileMenu.propTypes = {
    user: PropTypes.object
};

ProfileMenu.contextTypes = {
    router: PropTypes.object.isRequired
};
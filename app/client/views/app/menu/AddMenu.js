"use strict";

import GroupSecurity from "/imports/groups/GroupSecurity";

import {Link} from 'react-router';

import UserHelpers from "/imports/accounts/UserHelpers";

import React, {Component, PropTypes} from "react";
import ReactDOM from "react-dom";

export default class AddMenu extends Component {

    constructor( props ) {
        super( props );
        this.setupMenu = this.setupMenu.bind( this );
        this.close     = this.close.bind( this );
    }

    componentDidMount() {
        this.setupMenu();
    }

    componentDidUpdate() {
        this.setupMenu();
    }

    componentWillUnmount() {
        $( ReactDOM.findDOMNode( this.refs.addMenu ) )
            .popup( 'hide' )
            .popup( 'destroy' );
    }

    setupMenu() {
        $( ReactDOM.findDOMNode( this.refs.addMenu ) )
            .popup( { on: 'click', position: 'bottom left' } );
    }

    close() {
        $( ReactDOM.findDOMNode( this.refs.addMenu ) ).popup( 'hide' );
    }

    render() {
        const { user } = this.props;

        if ( !GroupSecurity.canManageGroups( user ) ) {
            return null;
        }

        return (
            <div className="item">
                <div ref="addMenu" id="addMenu" className="addMenu ui olive compact right labeled icon dropdown button">
                    <i className="add icon"/>
                    <span className="optional-text">{__( 'Add' )}</span>
                </div>
                <div className="ui basic inverted popup">
                    <div className="ui inverted secondary vertical menu">
                        { UserHelpers.isSuper( user ) &&
                          <Link to={'/admin/user/create'} className="item" onClick={this.close}>
                              <i className="user icon"/> {__( 'User' )}
                          </Link> }
                        { UserHelpers.isExpert( user ) &&
                          <Link to={'/group/create'} className="item" onClick={this.close}>
                              <i className="group icon"/> {__( 'Group' )}
                          </Link> }
                        <Link to={'/project/create'} className="item" onClick={this.close}>
                            <i className="bullseye icon"/> {__( 'Project' )}
                        </Link>
                    </div>
                </div>
            </div>
        );
    }
}

AddMenu.propTypes = {
    user: PropTypes.object
};
"use strict";

import React, { Component, PropTypes } from "react";
import classNames from "classnames";
import i18n from "/imports/i18n/i18n";
import {ConnectMeteorData} from '../../../lib/ReactMeteorData';
import DropDown from '../../../components/semanticui/modules/Dropdown';

export class LanguageMenu extends Component {

    render() {
        const {language} = this.props;

        return (
            <DropDown className="scrolling item">
                {__( 'Language' )}
                <i className="dropdown icon"/>
                <div className="menu">
                    { _.map( i18n.getLanguages(), ( lang, key ) => (
                        <a key={key}
                           className={classNames('item', { 'active selected': key == language })}
                           onClick={e=>i18n.setLanguage(key, true)}>{lang.name}</a>
                    ) ) }
                </div>
            </DropDown>
        );
    }
}

LanguageMenu.propTypes = {
    language: PropTypes.string.isRequired
};

export default ConnectMeteorData( props => {
    return {
        language: i18n.getLanguage() || 'en'
    }
} )( LanguageMenu );
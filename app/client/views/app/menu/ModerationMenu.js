"use strict";

import GroupSecurity from "/imports/groups/GroupSecurity";

import {Link} from 'react-router';
import links from '../../moderation/menu/Links';
import DropDown from '../../../components/semanticui/modules/Dropdown';

import UserHelpers from "/imports/accounts/UserHelpers";

import React, { Component, PropTypes } from "react";
import classNames from "classnames";

export default class ModerationMenu extends Component {
    render() {
        const {user} = this.props;
        const topLevelModerator = UserHelpers.isModerator( user );
        if ( !topLevelModerator && !GroupSecurity.canModerateGroups( user ) ) {
            return null;
        }

        const { router } = this.context;

        return (
            <DropDown className={classNames("teal scrolling item",{selected: router.isActive({ pathname: '/moderation' }, false)})}>
                <span><i className="legal icon"/><span className="name">{__( 'Moderation' )}</span></span>
                <i className="dropdown icon"/>
                <div className="menu">
                    <div className="header">
                        {__( 'Moderation' )}
                    </div>
                    { links
                        .filter( link => link.enabled && ( topLevelModerator || link.groupModeratorsAllowed ) )
                        .map( link => (
                            <Link key={link.to}
                                  to={link.to}
                                  onlyActiveOnIndex={link.index}
                                  className="teal item"
                                  activeClassName="active">
                                <i className={ classNames( link.icon, 'icon' )}/>
                                {link.label}
                            </Link>
                        ) ) }
                </div>
            </DropDown>
        );
    }
}

ModerationMenu.propTypes = {
    user: PropTypes.object
};

ModerationMenu.contextTypes = {
    router: PropTypes.object.isRequired
};
"use strict";

import {Link} from 'react-router';
import links from '../../admin/menu/Links';
import DropDown from '../../../components/semanticui/modules/Dropdown';

import UserHelpers from "/imports/accounts/UserHelpers";
import React, { Component, PropTypes } from "react";
import classNames from "classnames";

export default class AdminMenu extends Component {
    render() {
        const {user} = this.props;
        if ( !UserHelpers.isSuper( user ) ) {
            return null;
        }

        const { router } = this.context;

        return (
            <DropDown className={classNames("teal scrolling item", {selected: router.isActive({ pathname: '/admin' }, false)})}>
                <span><i className="sitemap icon"/><span className="name">{__('Administration')}</span></span>
                <i className="dropdown icon"/>
                <div className="menu">
                    <div className="header">
                        {__( 'Administration' )}
                    </div>
                    { links
                        .map( link => (
                            <Link key={link.to}
                                  to={link.to}
                                  onlyActiveOnIndex={link.index}
                                  className="teal item"
                                  activeClassName="active">
                                <i className={ classNames( link.icon, 'icon' )}/>
                                {link.label}
                            </Link>
                        ) ) }
                </div>
            </DropDown>
        );
    }
}

AdminMenu.propTypes = {
    user: PropTypes.object
};

AdminMenu.contextTypes = {
    router: PropTypes.object.isRequired
};
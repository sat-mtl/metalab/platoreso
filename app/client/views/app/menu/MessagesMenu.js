"use strict";

import React, {Component, PropTypes} from "react";
import ReactDOM from "react-dom";
import {Counts} from "meteor/tmeasday:publish-counts";
import ConversationCollection from "/imports/messages/ConversationCollection";
import {ConnectMeteorData} from "../../../lib/ReactMeteorData";
import Scrollable from "../../../components/utils/Scrollable";
import MessageNotifications from "../../message/notifications/MessageNotifications";

export class MessagesMenu extends Component {

    constructor( props ) {
        super( props );

        this.close = this.close.bind(this);

        this.state = {
            opened: false
        }
    }

    componentDidMount() {
        $( ReactDOM.findDOMNode( this.refs.messagesMenu ) )
            .popup( {
                on:       'click',
                popup:    $( ReactDOM.findDOMNode( this.refs.messagesPopup ) ),
                position: 'bottom right',
                onShow:   () => this.setState( { opened: true } ),
                onHide:   () => this.setState( { opened: false } )
            } );
    }

    componentWillUnmount() {
        $( ReactDOM.findDOMNode( this.refs.messagesMenu ) )
            .popup( 'hide' )
            .popup( 'destroy' );
    }

    close() {
        $( ReactDOM.findDOMNode( this.refs.messagesMenu ) ).popup( 'hide' )
    }

    render() {
        const { conversations } = this.props;
        const { opened }        = this.state;

        const unreadCount = conversations
            .map( conversation=>conversation.participant.unreadCount )
            .reduce( ( prev = 0, cur = 0 ) => prev + cur, 0 );

        return (
            <a ref="messagesMenu" className="link icon item">
                <span><i className="talk icon"/><span className="name">{__('Messages')}</span></span>
                <i className="dropdown icon"/>
                { unreadCount > 0 && <div className="ui mini red circular label">{unreadCount}</div> }
                <div ref="messagesPopup" className="ui basic popup messages">
                    { opened && <Scrollable className="messagesScroller">
                        <MessageNotifications onClose={this.close}/>
                    </Scrollable> }
                </div>
            </a>
        );
    }
}

/**
 * Meteor Data Wrapper
 */
export default ConnectMeteorData( props => {
    return {
        loading:       !Meteor.subscribe( 'conversations' ).ready(),
        conversations: ConversationCollection.find( {} ).fetch(),
    }
} )( MessagesMenu );
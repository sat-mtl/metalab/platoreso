"use strict";

import React, { Component, PropTypes } from "react";
import { Link } from "react-router";
import ProjectCollection from "/imports/projects/ProjectCollection";
import ProjectImagesCollection from "/imports/projects/ProjectImagesCollection";
import { ConnectMeteorData } from "../../../lib/ReactMeteorData";
import DropDown from "../../../components/semanticui/modules/Dropdown";
import Image from "../../widgets/Image";

export class ProjectsMenu extends Component {

    render() {
        const { projects, recentProjects } = this.props;

        if ( !projects || !projects.length ) {
            return null;
        }

        return (
            <DropDown className="item scrolling projectsMenu">
                <span>{__( 'Projects' )}</span>
                <i className="dropdown icon"/>
                <div className="menu">
                    { recentProjects.length > 0 && <div className="header">
                        <i className="history icon"/>
                        {__( 'Recent Projects' )}
                    </div> }
                    { recentProjects.map( project => <WrappedProjectsMenuItem key={project._id} project={project}/> ) }
                    <div className="divider"></div>
                    <div className="header">
                        <i className="list icon"/>
                        {__( 'All Projects' )}
                    </div>
                    { projects.map( project => <WrappedProjectsMenuItem key={project._id} project={project}/> ) }
                </div>
            </DropDown>
        );
    }
}

ProjectsMenu.propTypes = {
    projectsLoading: PropTypes.bool.isRequired,
    project:         PropTypes.object
};

export default ConnectMeteorData( props => {

    const user           = Meteor.user();
    const recentIds      = user && user.recentProjects ? user.recentProjects.slice( 0, 3 ) : [];
    const recentProjects = recentIds.map( projectId => ProjectCollection.findOne( { _id: projectId } ) ).filter( project => project != null );

    return {
        user,
        projectsLoading: !Meteor.subscribe( 'project/list' ).ready(),
        projects:        ProjectCollection.find( {}, { sort: { updatedAt: -1 } } ).fetch(),
        recentProjects
    }
} )( ProjectsMenu );

class ProjectsMenuItem extends Component {

    render() {
        const { project, projectImage } = this.props;

        return (
            <Link key={project._id}
                  to={`/project/${(project.slug || project._id)}`}
                  className="item"
                  activeClassName="active">
                <Image image={ projectImage }
                       size="thumb"
                       tagOnly={true}
                       className="avatar"/>
                {project.name}
            </Link>
        );
    }
}

ProjectsMenuItem.propTypes = {
    project:      PropTypes.object.isRequired,
    projectImage: PropTypes.object
};

export const WrappedProjectsMenuItem = ConnectMeteorData( props => {
    const { project } = props;
    return {
        projectImage: ProjectImagesCollection.findOne( { owners: project._id } )
    }
} )( ProjectsMenuItem );
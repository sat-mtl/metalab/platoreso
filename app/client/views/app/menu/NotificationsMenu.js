"use strict";

import React, {Component, PropTypes} from "react";
import ReactDOM from "react-dom";
import {Counts} from "meteor/tmeasday:publish-counts";
import NotificationMethods from "/imports/notifications/NotificationMethods";
import {ConnectMeteorData} from "../../../lib/ReactMeteorData";
import Scrollable from "../../../components/utils/Scrollable";
import Notifications from "../../notification/Notifications";

export class NotificationsMenu extends Component {

    constructor( props ) {
        super( props );

        this.close = this.close.bind(this);

        this.state = {
            opened: false
        }
    }

    componentDidMount() {
        $( ReactDOM.findDOMNode( this.refs.notificationsMenu ) )
            .popup( {
                on:       'click',
                popup:    $( ReactDOM.findDOMNode( this.refs.notificationsPopup ) ),
                position: 'bottom right',
                onShow:   () => this.setState( { opened: true } ),
                onHide:   () => this.setState( { opened: false } ),
                onHidden: () => NotificationMethods.markAllAsRead.call()
            } );
    }

    componentWillUnmount() {
        $( ReactDOM.findDOMNode( this.refs.notificationsMenu ) )
            .popup( 'hide' )
            .popup( 'destroy' );
    }

    close() {
        $( ReactDOM.findDOMNode( this.refs.notificationsMenu ) ).popup( 'hide' )
    }

    render() {
        const { unreadCount } = this.props;
        const { opened }      = this.state;

        return (
            <a ref="notificationsMenu" className="link icon item">
                <span><i className="alarm icon"/><span className="name">{__('Notifications')}</span></span>
                <i className="dropdown icon"/>
                { unreadCount > 0 && <div className="ui mini red circular label">{unreadCount}</div> }
                <div ref="notificationsPopup" className="ui basic popup notifications">
                    { opened && <Scrollable className="notificationsScroller">
                        <Notifications onClose={this.close}/>
                    </Scrollable> }
                </div>
            </a>
        );
    }
}

/**
 * Meteor Data Wrapper
 */
export default ConnectMeteorData( props => {
    return {
        // We auto subscribe to notifications
        unreadCount: props.user ? Counts.get( 'notifications/' + props.user._id + '/unread' ) : 0
    }
} )( NotificationsMenu );
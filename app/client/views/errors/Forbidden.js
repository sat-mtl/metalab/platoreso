"use strict";

import React, {Component, PropTypes} from "react";

export default () => (
    <div className="error-page forbidden">
        <h2 className="ui inverted icon header">
            <i className="protect icon"/>
            <div className="content">
                {__( 'Forbidden' )}
                <div className="sub header">{__( 'You do not have access to this content.' )}</div>
            </div>
        </h2>
    </div>
);
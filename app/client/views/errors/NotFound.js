"use strict";

import React, { Component, PropTypes } from "react";
import {Link} from 'react-router';
import {ConnectMeteorData} from '../../lib/ReactMeteorData';

class NotFound extends Component {
    render() {
        const { user } = this.props;
        return (
            <div className="error-page notfound">
                <h2 className="ui inverted icon header">
                    <i className="meh icon"/>
                    <div className="content">
                        {__('Not Found')}
                        <div className="sub header">{__('The page you were looking for cannot be found.')}</div>
                    </div>
                </h2>
                { !user && <p><Link to="/login" className="ui olive button">{__('Try logging in')}</Link></p> }
            </div>
        );
    }
}

/**
 * Meteor Data Wrapper
 */
export default ConnectMeteorData( props => ({
    user:      Meteor.user()
}) )( NotFound );
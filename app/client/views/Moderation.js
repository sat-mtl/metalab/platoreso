"use strict";

import React, { Component, PropTypes } from "react";
import GroupSecurity from "/imports/groups/GroupSecurity";
import {ConnectMeteorData} from '../lib/ReactMeteorData';
import ManagementPage from './layouts/ManagementPage';
import ModerationMenu from './moderation/Menu';
import Forbidden from './errors/Forbidden';

export class Moderation extends Component {

    render() {
        const { user } = this.props;

        if ( !GroupSecurity.canModerateGroups( user ) ) {
            return <Forbidden/>
        }

        return (
            <ManagementPage id="moderation" className="moderation" menu={<ModerationMenu/>}>
                {this.props.children}
            </ManagementPage>
        );
    }

}

Moderation.propTypes = {
    user: PropTypes.object.isRequired
};

export default ConnectMeteorData( props => {
    return {
        user: Meteor.user()
    }
} )( Moderation );
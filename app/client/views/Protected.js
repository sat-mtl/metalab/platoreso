"use strict";

import React, { Component, PropTypes } from "react";
import {ConnectMeteorData} from '../lib/ReactMeteorData';

import AccountCreation from './account/AccountCreation';
import AccountDisabled from './account/AccountDisabled';
import LoggingIn from './authentication/LoggingIn';
import Login from './authentication/Login';

class Protected extends Component {

    render() {
        const { user, loggingIn } = this.props;

        if ( loggingIn || (user && (user.enabled === undefined || user.completed === undefined || user.acceptedTerms === undefined)) ) {
            return <LoggingIn/>;
        } else if ( user ) {
            if ( !user.enabled ) {
                return <AccountDisabled/>;
            } else if ( !user.completed || !user.acceptedTerms ) {
                return <AccountCreation/>;
            } else {
                return this.props.children;
            }
        } else {
            return <Login redirect={false}/>;
        }
    }

}

/**
 * Meteor Data Wrapper
 */
const ProtectedComponent = ConnectMeteorData( props => ({
    user:      Meteor.user(),
    loggingIn: Meteor.loggingIn()
}) )( Protected );

export default ProtectedComponent;
"use strict";

import React, { Component, PropTypes } from "react";

import ProjectCollection from "/imports/projects/ProjectCollection";
import CardCollection from "/imports/cards/CardCollection";
import CommentCollection from "/imports/comments/CommentCollection";
import { StoryTypes } from "/imports/comments/feed/CommentStories";

import { ConnectMeteorData } from '../../../lib/ReactMeteorData';
import StoryTypeBase from './StoryTypeBase';
import CardPreview from '../../card/CardPreview';

export class CardCommentStory extends StoryTypeBase {

    getStoryTitle() {
        const { story, card, project } = this.props;

        if ( !card || !project ) {
            return;
        }

        const i18nData = {
            card:    `<a href="/card/${card.uri}">${card.name}</a>`,
            project: `<a href="/project/${project.uri}">${project.name}</a>`
        };
        let html       = null;

        switch ( story.action ) {
            case StoryTypes.commented:
                html = __( 'commented on __card__ in __project__', i18nData );
                break;
            case StoryTypes.editedComment:
                html = __( 'edited a comment on __card__ in __project__', i18nData );
                break;
            case StoryTypes.replied:
                html = __( 'replied to a comment on __card__ in __project__', i18nData );
                break;
            case StoryTypes.editedReply:
                html = __( 'edited a reply to a comment on __card__ in __project__', i18nData );
                break;
            case StoryTypes.liked:
                html = __( 'liked a comment on __card__ in __project__', i18nData );
                break;
        }

        return html;
    }

    renderStory() {
        const { loading, comment, card, project, showPreview, grouped } = this.props;

        if ( loading || !card || !project ) {
            return null;
        } else {
            return (
                <div className="extra">
                    { comment &&
                      <blockquote className="quote" dangerouslySetInnerHTML={{ __html: comment.postMarkdown }}/>}
                    { !grouped && showPreview && <CardPreview key={card.id}
                                                              card={card}
                                                              layout="feed"/> }
                </div>
            );
        }
    }
}

CardCommentStory.propTypes = _.extend( {
    comment: PropTypes.object,
    card:    PropTypes.object,
    project: PropTypes.object,
    image:   PropTypes.object
}, StoryTypeBase.propTypes );

/**
 * Meteor Data Wrapper
 */
export default ConnectMeteorData( props => {
    const { story, showPreview } = props;

    const object = story.getObjectByType( CardCollection.entityName );
    const card = CardCollection.findOne( { id: object.id, current: !object.archived } );

    return _.extend( {
        loading: !story.group ? !Meteor.subscribe( 'card/comment/feed/story', story._id, showPreview ).ready() : false,
        comment: !story.group ? CommentCollection.findOne( { _id: story.data.comment } ) : null,
        card:    card,
        project: ProjectCollection.findOne( { _id: story.getObjectIdByType( ProjectCollection.entityName ) } )
    }, StoryTypeBase.getMeteorData( props ) );
} )( CardCommentStory );
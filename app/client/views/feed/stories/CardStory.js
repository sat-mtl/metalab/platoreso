"use strict";

import ProjectCollection from "/imports/projects/ProjectCollection";
import CardCollection from "/imports/cards/CardCollection";
import { StoryTypes } from "/imports/cards/feed/CardStories";

import { ConnectMeteorData } from '../../../lib/ReactMeteorData';
import StoryTypeBase from './StoryTypeBase';
import StoryHelpers from '../StoryHelpers';
import CardPreview from '../../card/CardPreview';

import React, { Component, PropTypes } from "react";

//const storyCards = new Meteor.Collection("storyCards");

function getBasicStoryData( story, cards, project ) {
    const card  = StoryHelpers.getCard( story, cards );
    const phase = StoryHelpers.getPhase( story, project, 'phase' );
    return {
        phase: phase,
        i18n:  {
            card:    StoryHelpers.cardSpan( project, card ),
            phase:   StoryHelpers.phaseSpan( phase ),
            project: StoryHelpers.projectSpan( project )
        }
    };
}

export class CardStory extends StoryTypeBase {

    constructor( props ) {
        super( props );
        this.renderPreview = this.renderPreview.bind( this );
    }

    get renderWhenGrouped() {
        return false;
    }

    getStoryTitle() {
        const { loading, story, cards, project } = this.props;

        if ( loading || !cards || cards.length == 0 || !project ) {
            return;
        }

        let data = null;
        let html = null;

        switch ( story.action ) {
            case StoryTypes.created:
                data = getBasicStoryData( story, cards, project );
                if ( data.phase ) {
                    html = __( 'added card __card__ to __phase__ in __project__', data.i18n );
                } else {
                    html = __( 'added card __card__ in __project__', data.i18n );
                }
                break;

            case StoryTypes.updated:
                data = getBasicStoryData( story, cards, project );
                if ( data.phase ) {
                    html = __( 'edited card __card__ from __phase__ in __project__', data.i18n );
                } else {
                    html = __( 'edited card __card__ in __project__', data.i18n );
                }
                break;

            case StoryTypes.liked:
                data = getBasicStoryData( story, cards, project );
                if ( data.phase ) {
                    html = __( 'liked card __card__ from __phase__ in __project__', data.i18n );
                } else {
                    html = __( 'liked card __card__ in __project__', data.i18n );
                }
                break;

            case StoryTypes.unliked:
                data = getBasicStoryData( story, cards, project );
                if ( data.phase ) {
                    html = __( 'unliked card __card__ from __phase__ in __project__', data.i18n );
                } else {
                    html = __( 'unliked card __card__ in __project__', data.i18n );
                }
                break;

            case StoryTypes.pinned:
                data = getBasicStoryData( story, cards, project );
                html = __( 'pinned card __card__ in __project__', data.i18n );
                break;

            case StoryTypes.unpinned:
                data = getBasicStoryData( story, cards, project );
                html = __( 'unpinned card __card__ in __project__', data.i18n );
                break;

            case StoryTypes.linked:
                const linkSourceCard = StoryHelpers.getCard( story, cards, 'source' );
                if ( !linkSourceCard ) {
                    return null;
                }
                const linkDestinationCard = StoryHelpers.getCard( story, cards, 'destination' );
                if ( !linkDestinationCard ) {
                    return null;
                }
                const linkedFromPhase = StoryHelpers.getPhase( story, project, 'fromPhase' );
                const linkedToPhase   = StoryHelpers.getPhase( story, project, 'toPhase' );
                const linkedI18n      = {
                    card:      StoryHelpers.cardSpan( project, linkSourceCard ),
                    toCard:    StoryHelpers.cardSpan( project, linkDestinationCard ),
                    linkType:  story.data.linkType,
                    fromPhase: StoryHelpers.phaseSpan( linkedFromPhase ),
                    toPhase:   StoryHelpers.phaseSpan( linkedToPhase ),
                    project:   StoryHelpers.projectSpan( project )
                };

                if ( story.data.linkType ) {
                    html = __( `story.card.linked.${story.data.linkType}`, linkedI18n );
                } else if ( linkedFromPhase && linkedToPhase ) {
                    html = __( 'associated card __card__ from __fromPhase__ to __toCard__ in __toPhase__ in __project__', linkedI18n );
                } else if ( linkedFromPhase ) {
                    html = __( 'associated card __card__ from __fromPhase__ to __toCard__ in __project__', linkedI18n );
                } else if ( linkedToPhase ) {
                    html = __( 'associated card __card__ to __toCard__ in __toPhase__ in __project__', linkedI18n );
                } else {
                    html = __( 'associated card __card__ to __toCard__ in __project__', linkedI18n );
                }
                break;

            case StoryTypes.unlinked:
                const unlinkedSourceCard = StoryHelpers.getCard( story, cards, 'source' );
                if ( !unlinkedSourceCard ) {
                    return null;
                }
                const unlinkedDestinationCard = StoryHelpers.getCard( story, cards, 'destination' );
                if ( !unlinkedDestinationCard ) {
                    return null;
                }
                const unlinkedFromPhase = StoryHelpers.getPhase( story, project, 'fromPhase' );
                const unlinkedToPhase   = StoryHelpers.getPhase( story, project, 'toPhase' );
                const unlinkedI18n      = {
                    card:      StoryHelpers.cardSpan( project, unlinkedSourceCard ),
                    fromCard:  StoryHelpers.cardSpan( project, unlinkedDestinationCard ),
                    linkType:  story.data.linkType,
                    fromPhase: StoryHelpers.phaseSpan( unlinkedFromPhase ),
                    toPhase:   StoryHelpers.phaseSpan( unlinkedToPhase ),
                    project:   StoryHelpers.projectSpan( project )
                };

                if ( story.data.linkType ) {
                    html = __( `story.card.unlinked.${story.data.linkType}`, unlinkedI18n );
                } else if ( unlinkedFromPhase && unlinkedToPhase ) {
                    html = __( 'unlinked card __card__ in __toPhase__ from __fromCard__ in __fromPhase__ in __project__', unlinkedI18n );
                } else if ( unlinkedFromPhase ) {
                    html = __( 'unlinked card __card__ in __toPhase__ from __fromCard__ in __project__', unlinkedI18n );
                } else if ( unlinkedToPhase ) {
                    html = __( 'unlinked card __card__ from __fromCard__ in __fromPhase__ in __project__', unlinkedI18n );
                } else {
                    html = __( 'unlinked card __card__ from __fromCard__ in __project__', unlinkedI18n );
                }
                break;

            case StoryTypes.moved:
                const movedCard = StoryHelpers.getCard( story, cards );
                if ( !movedCard ) {
                    return null;
                }
                const movedFromPhase = StoryHelpers.getPhase( story, project, 'fromPhase' );
                const movedToPhase   = StoryHelpers.getPhase( story, project, 'toPhase' );
                const movedI18n      = {
                    card:      StoryHelpers.cardSpan( project, movedCard ),
                    fromPhase: StoryHelpers.phaseSpan( movedFromPhase ),
                    toPhase:   StoryHelpers.phaseSpan( movedToPhase ),
                    project:   StoryHelpers.projectSpan( project )
                };
                if ( movedFromPhase && movedToPhase ) {
                    html = __( 'moved card __card__ from __fromPhase__ to __toPhase__ in __project__', movedI18n );
                } else if ( movedFromPhase ) {
                    html = __( 'moved card __card__ from __fromPhase__ in __project__', movedI18n );
                } else if ( movedToPhase ) {
                    html = __( 'moved card __card__ to __toPhase__ in __project__', movedI18n );
                } else {
                    html = __( 'moved card __card__ in __project__', movedI18n );
                }
                break;

            case StoryTypes.copied:
                const originalCard = StoryHelpers.getCard( story, cards, 'original' );
                if ( !originalCard ) {
                    return null;
                }
                const copiedCard = StoryHelpers.getCard( story, cards, 'copy' );
                if ( !copiedCard ) {
                    return null;
                }
                const copiedFromPhase = StoryHelpers.getPhase( story, project, 'fromPhase' );
                const copiedToPhase   = StoryHelpers.getPhase( story, project, 'toPhase' );
                const samePhase       = story.data.fromPhase == story.data.toPhase;
                const copiedI18n      = {
                    card:      StoryHelpers.cardSpan( project, originalCard ),
                    copy:      StoryHelpers.cardSpan( project, copiedCard ),
                    fromPhase: StoryHelpers.phaseSpan( copiedFromPhase ),
                    toPhase:   StoryHelpers.phaseSpan( copiedToPhase ),
                    project:   StoryHelpers.projectSpan( project )
                };
                if ( copiedFromPhase && copiedToPhase && !samePhase ) {
                    html = __( 'copied card __card__ from __fromPhase__ to __toPhase__ in __project__', copiedI18n );
                } else if ( copiedFromPhase ) {
                    html = __( 'copied card __card__ from __fromPhase__ in __project__', copiedI18n );
                } else if ( copiedToPhase ) {
                    html = __( 'copied card __card__ to __toPhase__ in __project__', copiedI18n );
                } else {
                    html = __( 'copied card __card__ in __project__', copiedI18n );
                }
                break;
        }

        return html;
    }

    renderPreview() {
        const { loading, story, cards, project } = this.props;

        if ( loading || !cards || cards.length == 0 || !project ) {
            return;
        }

        let data = null;
        let html = null;

        switch ( story.action ) {
            case StoryTypes.created:
            case StoryTypes.updated:
            case StoryTypes.liked:
            case StoryTypes.unliked:
            case StoryTypes.pinned:
            case StoryTypes.unpinned:
            case StoryTypes.moved:
                const card = StoryHelpers.getCard( story, cards );
                return <CardPreview card={card}
                                    project={project}
                                    layout="feed"/>;
                break;

            case StoryTypes.linked:
                const sourceCard = StoryHelpers.getCard( story, cards, 'source' );
                if ( !sourceCard ) {
                    return null;
                }
                const destinationCard = StoryHelpers.getCard( story, cards, 'destination' );
                if ( !destinationCard ) {
                    return null;
                }
                return (
                    <div className="previews">
                        <div className="previewContainer">
                            <div className="cardContainer destination">
                                <CardPreview card={destinationCard}
                                             project={project}
                                             layout="feed"/>
                            </div>
                        </div>
                        <div className="previewContainer">
                            <div className="cardContainer source">
                                <CardPreview card={sourceCard}
                                             project={project}
                                             layout="feed"/>
                            </div>
                        </div>
                    </div>
                );
                break;

            case StoryTypes.copied:
                const originalCard = StoryHelpers.getCard( story, cards, 'original' );
                if ( !originalCard ) {
                    return null;
                }
                const copiedCard = StoryHelpers.getCard( story, cards, 'copy' );
                if ( !copiedCard ) {
                    return null;
                }
                return (
                    <div className="previews">
                        <div className="previewContainer">
                            <div className="cardContainer original">
                                <CardPreview card={originalCard}
                                             project={project}
                                             layout="feed"/>
                            </div>
                        </div>
                        <div className="previewContainer">
                            <div className="cardContainer copy">
                                <CardPreview card={copiedCard}
                                             project={project}
                                             layout="feed"/>
                            </div>
                        </div>
                    </div>
                );
                break;
        }

        return html;
    }

    renderStory() {
        const { loading, cards, project, showPreview, grouped } = this.props;

        if ( loading || !cards || !project ) {
            return null;
        } else {
            return (
                <div className="extra">
                    { !grouped && showPreview && this.renderPreview() }
                </div>
            );
        }
    }
}

CardStory.propTypes = _.extend( {
    cards:   PropTypes.arrayOf( PropTypes.object ),
    project: PropTypes.object
}, StoryTypeBase.propTypes );

/**
 * Meteor Data Wrapper
 */
export default ConnectMeteorData( props => {
    const { story, showPreview } = props;

    const cards = CardCollection.find( { id: { $in: story.getObjectIdsByType( CardCollection.entityName, null ) } } ).fetch();

    return _.extend( {
        loading: !story.group ? !Meteor.subscribe( 'feed/card/story', story._id, showPreview ).ready() : false,
        cards:   cards,
        project: ProjectCollection.findOne( { _id: story.getObjectIdByType( ProjectCollection.entityName ) } )
    }, StoryTypeBase.getMeteorData( props ) );
} )( CardStory );
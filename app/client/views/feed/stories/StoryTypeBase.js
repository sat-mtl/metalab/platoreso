"use strict";

import UserCollection from "/imports/accounts/UserCollection";
import UserHelpers from "/imports/accounts/UserHelpers";
import moment from "moment";
import {Link} from 'react-router';

import Image from '../../widgets/Image';
import Avatar from '../../widgets/Avatar';

import React, { Component, PropTypes } from "react";
import classNames from "classnames";

export default class StoryTypeBase extends Component {

    constructor( props ) {
        super( props );
        this.getStoryTitle = this.getStoryTitle.bind( this );
        this.renderStory   = this.renderStory.bind( this );
    }

    static getMeteorData( props ) {
        if ( props.story.group ) {
            return {
                subjects: UserCollection.find( {_id: {$in: _.uniq(_.pluck( props.story.stories, 'subject' ) ) }} ).fetch()
            };
        } else {
            return {
                subject: UserCollection.findOne( {_id: props.story.subject} )
            };
        }

    }

    get renderWhenGrouped() {
        return true;
    }

    getStoryTitle() {
        return null;
    }

    renderStory() {
        return null;
    }

    renderSubject( subject ) {
        if ( !subject ) {
            return null;
        }
        
        return (
            <Link to={"/profile/" + subject._id} className="user">
                {UserHelpers.getDisplayName( subject )}
            </Link>
        );
    }

    renderSubjects( subjects ) {
        if ( !subjects ) {
            return null;
        }

        const len = subjects.length;
        return subjects.map( ( subject, index ) => (
            <span key={subject._id} className="subject">
                <Link to={"/profile/" + subject._id} className="user">
                    {UserHelpers.getDisplayName( subject )}
                </Link>{ index < len - 1 ? ( index == len - 2 ? __( ' and ' ) : __( ', ' ) ) : '' }
            </span>
        ) );
    }

    render() {
        const { story, subject, subjects, grouped } = this.props;

        if ( grouped && !this.renderWhenGrouped ) {
            return null
        } else {
            return (
                <div className={classNames("story", story.objects[0].type, story.action, this.props.className)}>
                    <div className="header">
                        { !story.group && <Avatar user={subject}/> }
                        <div className="content">
                            { !story.group && <div className="date">
                                {moment( story.createdAt ).fromNow()}
                            </div> }
                            <div className="summary">
                                { !story.group ? this.renderSubject( subject ) : this.renderSubjects( subjects ) }
                                &nbsp;{ !grouped &&
                            <span dangerouslySetInnerHTML={{__html: this.getStoryTitle()}}/> }
                            </div>
                        </div>
                    </div>
                    { this.renderStory() }
                </div>
            );
        }
    }

}

StoryTypeBase.propTypes = {
    loading:     PropTypes.bool,
    story:    PropTypes.object.isRequired,
    subject:     PropTypes.object,
    subjects:    PropTypes.arrayOf( PropTypes.object ),
    showPreview: PropTypes.bool,
    grouped:     PropTypes.bool
};

StoryTypeBase.defaultProps = {
    loading:     false,
    showPreview: true,
    grouped:     false
};
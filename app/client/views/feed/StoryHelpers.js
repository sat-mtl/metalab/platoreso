"use strict";

import CardCollection from "/imports/cards/CardCollection";

export default class StoryHelpers {

    static getCard( story, cards, role = null ) {
        return cards.find( card => card.id == story.getObjectIdByType( CardCollection.entityName, role ) );
    }

    static getPhase(story, project, dataKey) {
        return project.phases && story && story.data && story.data[dataKey]
            ? project.phases.find( phase => phase._id == story.data[dataKey] )
            : null;
    }

    static cardSpan(project, card) {
        return `<a href="/card/${card.uri}">${card.name}</a>`;
    }

    static phaseSpan(phase) {
        return phase ? `<span class="phase">${phase.name}</span>` : null;
    }

    static projectSpan(project) {
        return `<a href="/project/${project.uri}">${project.name}</a>`;
    }
}
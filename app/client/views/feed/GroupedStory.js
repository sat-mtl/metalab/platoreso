"use strict";

import Story from "./Story";
import React, {Component, PropTypes} from "react";

export default class GroupedStory extends Component {
    render() {
        const { story, showPreview } = this.props;
        return (
            <div className="grouped-story">
                <Story story={story} showPreview={showPreview} className="group"/>
                <div className="stories">
                    { story.stories.map( s =>
                        <Story key={s._id} story={s} showPreview={showPreview} grouped={true}/> ) }
                </div>
            </div>
        )
    }
}

GroupedStory.propTypes = {
    story:       PropTypes.object.isRequired,
    showPreview: PropTypes.bool
};

GroupedStory.defaultProps = {
    showPreview: true
};
"use strict";

import slugify from "underscore.string/slugify";
import { Counts } from "meteor/tmeasday:publish-counts";
import React, { Component, PropTypes } from "react";
import classNames from "classnames";

import FeedCollection from "/imports/feed/FeedCollection";
import Story from "/imports/feed/model/Story";

import {ConnectMeteorData} from '../../lib/ReactMeteorData';
import StoryComponent from './Story';
import GroupedStory from './GroupedStory';

const DEFAULT_FEED_LIMIT     = 10;
const DEFAULT_FEED_INCREMENT = DEFAULT_FEED_LIMIT;

export class Feed extends Component {

    constructor( props ) {
        super( props );
        this.state = {
            groupedFeed: []
        };
    }

    componentWillReceiveProps( nextProps ) {
        let groupedFeed = [];

        if ( nextProps.feed ) {
            let groupedStories = [];
            let lastSubject    = null;
            let inc            = 0;
            const count        = nextProps.feed.length;

            function sameStory( story ) {
                const lastStory = groupedStories[groupedStories.length - 1];
                if ( lastStory.action != story.action ) {
                    return false;
                } else if ( lastStory.objects.length != story.objects.length ) {
                    return false;
                } else {
                    return story.objects.every( ( object, index ) => lastStory.objects[index].id == object.id );
                }
            }

            function checkBuffer() {
                if ( groupedStories.length == 1 ) {
                    groupedFeed.push( groupedStories[0] );
                } else {
                    groupedFeed.push( new Story( {
                        _id:     'grouped_' + (inc++),
                        group:   true,
                        action:  groupedStories[0].action,
                        objects: groupedStories[0].objects,
                        stories: groupedStories
                    } ) );
                }
                lastSubject = null;
            }

            nextProps.feed.forEach( ( story, index ) => {
                if ( groupedStories.length == 0 || ( sameStory( story ) && story.subject != lastSubject ) ) {
                    groupedStories.push( story );
                    lastSubject = story.subject;
                } else {
                    checkBuffer();
                    groupedStories = [story];
                    lastSubject    = story.subject;
                }
                if ( index == count - 1 ) {
                    checkBuffer();
                }
            } );
        }

        this.setState( {groupedFeed: groupedFeed} );
    }

    render() {
        const { loading, showPreviews, feed, total } = this.props;
        const { groupedFeed } = this.state;

        if ( loading && _.isEmpty( groupedFeed ) ) {
            // Only show main loader on initial load, not for load more
            return (
                <div className="ui large active indeterminate text loader">
                    {__( 'Loading feed...' )}
                </div>
            );
        } else {
            return (
                <div className={classNames("feed", this.props.className)}>
                    { groupedFeed.map( story => story.group
                        ? <GroupedStory key={story._id} story={story} showPreview={showPreviews}/>
                        : <StoryComponent key={story._id} story={story} showPreview={showPreviews}/> ) }
                    { feed.length != total &&
                    <div className={classNames("ui basic fluid button", {loading: loading, disabled: loading})} onClick={this.props.loadMore}>{__( 'Load more' )}</div>
                    }
                </div>
            );
        }
    }
}

Feed.propTypes = {
    showPreviews: PropTypes.bool,
    loading:      PropTypes.bool,
    feed:         PropTypes.arrayOf( PropTypes.object ).isRequired,
    total:        PropTypes.number
};

Feed.defaultProps = {
    showPreviews: true
};

/**
 * Meteor Data Wrapper
 * With an es5 function to keep this.setState
 */
const FeedWrapper = ConnectMeteorData( function ( props ) {

    let query = {};
    let params = {
        internal: props.internal,
        archived: !!props.archived,
        limit:    this.state && this.state.limit ? this.state.limit : DEFAULT_FEED_LIMIT
    };

    if ( props.entities ) {
        params.entities = ( _.isArray( props.entities ) ? props.entities : [props.entities] );
    }

    if ( props.maxDate ) {
        params.maxDate = props.maxDate;
    }

    let options = {
        limit: params.limit,
        sort:  {
            createdAt: -1
        }
    };


    // Default is to not query for internal stories
    if ( !params.internal ) {
        query.internal = false
    }

    // Default is to not query for archived stories
    if ( !params.archived ) {
        query.archived = false;
    }

    if ( params.entities ) {
        query.$or = [
            {'subject': {$in: params.entities}},
            {'objects.id': {$in: params.entities}}
        ];
    }

    if ( props.maxDate ) {
        query.createdAt = {$lte: props.maxDate};
    }

    return {
        loading:  !Meteor.subscribe( 'feed', params ).ready(),
        feed:     FeedCollection.find( query, options ).fetch(),
        total:    Counts.get( 'feed/count/' + slugify( JSON.stringify( query ) ) ),
        loadMore: () => {
            this.setState( {limit: ( this.state && this.state.limit ? this.state.limit : DEFAULT_FEED_LIMIT ) + DEFAULT_FEED_INCREMENT} );
        }
    };
} )( Feed );

FeedWrapper.propTypes = {
    internal:     PropTypes.bool,
    archived:     PropTypes.bool,
    entities:     PropTypes.oneOfType( [
        PropTypes.string,
        PropTypes.arrayOf( PropTypes.string )
    ] ),
    showPreviews: PropTypes.bool,
    maxDate:      PropTypes.any
};

FeedWrapper.defaultProps = {
    internal:     false,
    archived:     false,
    showPreviews: true,
    maxDate:      null
};

export default FeedWrapper;
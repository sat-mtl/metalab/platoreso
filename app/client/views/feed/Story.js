"use strict";

import CardStory from "./stories/CardStory";
import CardCommentStory from "./stories/CardCommentStory";
import CardCollection from "/imports/cards/CardCollection";
import React, {Component, PropTypes} from "react";

export default class Story extends Component {

    constructor( props ) {
        super( props );
    }

    render() {
        const { story, showPreview, grouped } = this.props;
        let StoryComponent                    = null;
        switch ( story.objects[0].type ) {
            case CardCollection.entityName:
                if (
                    ( story.group && story.stories[0].data && story.stories[0].data.comment )
                    || (story.data && story.data.comment )
                ) {
                    StoryComponent = CardCommentStory;
                } else {
                    StoryComponent = CardStory;
                }
                break;
        }
        return StoryComponent ?
            <StoryComponent story={story}
                            showPreview={showPreview}
                            grouped={grouped}
                            className={this.props.className}/> : null;
    }
}

Story.propTypes = {
    story:       PropTypes.object.isRequired,
    showPreview: PropTypes.bool,
    grouped:     PropTypes.bool
};

Story.defaultProps = {
    showPreview: true,
    grouped:     false
};

"use strict";

import React, { Component, PropTypes } from "react";
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import { loadRoutes } from "/imports/router/Routes";

// GENERAL
import App from './views/App';
import Home from './views/app/Home';
import Protected from './views/Protected';
import Dashboard from './views/app/Dashboard';

// LEGAL
import Legal from "./views/Legal";

// PROFILE
import Profile from './views/profile/Profile';
import ProfileEdit from './views/profile/Edit';
import ProfileEditInfo from './views/profile/edit/ProfileInfo';
import ProfileEditPassword from './views/profile/edit/ProfilePassword';
import ProfileEditPicture from './views/profile/edit/ProfilePicture';
import ProfileEditBiography from './views/profile/edit/ProfileBiography';
import ProfileEditAccounts from './views/profile/edit/ProfileAccounts';
import ProfileEditNotifications from './views/profile/edit/ProfileNotifications';
import ProfileEditInvitationCode from './views/profile/edit/ProfileInvitationCode';

// GROUPS
import Group from './views/group/Group';
import GroupList from './views/group/List';
import GroupCreate from './views/group/Create';
import GroupEdit from './views/group/GroupEdit';

// PROJECTS
import ProjectWrapper from './views/project/ProjectWrapper';
import Project from './views/project/Project';
import ProjectBoard from './views/project/Board';
import ProjectCreate from './views/project/Create';
import ProjectEdit from './views/project/ProjectEdit';

// CARDS
import Card from './views/card/Card';
import CardEdit from './views/card/CardEdit';

// COMMENTS
import CommentEdit from './views/comment/CommentEdit';

// MESSAGES
import Messages from './views/message/Messages';

// MODERATION
import Moderation from './views/Moderation';
import TopLevelModeration from './views/TopLevelModeration';
import ModerationIndex from './views/moderation/Index';

// MODERATION - USERS
import ModerationUsersList from './views/moderation/user/List';

// MODERATION - GROUPS
import ModerationGroupList from './views/moderation/group/List';

// MODERATION - PROJECTS
import ModerationProjectList from './views/moderation/project/List';

// MODERATION - CARDS
import ModerationCardList from './views/moderation/card/List';

// MODERATION - COMMENTS
import ModerationCommentList from './views/moderation/comment/List';

// MODERATION - MESSAGES
import ModerationMessageList from './views/moderation/message/List';

// SETTINGS
import Settings from './views/Settings';
import SettingsIndex from './views/settings/Index';
import AnalyticsSettings from './views/settings/Analytics';
import GamificationSettings from './views/settings/Gamification';

// ADMIN
import Admin from './views/Admin';
import AdminIndex from './views/admin/Index';

// ADMIN - USERS
import AdminUsers from './views/admin/user/Users';
import AdminUserList from './views/admin/user/List';
import AdminUserCreate from './views/admin/user/Create';
import AdminUserEdit from './views/admin/user/Edit';
import AdminUserEditInfo from './views/admin/user/edit/UserInfo';
import AdminUserEditPicture from './views/admin/user/edit/UserPicture';
import AdminUserEditProfile from './views/admin/user/edit/UserProfile';
import AdminUserEditRoles from './views/admin/user/edit/UserRoles';
import AdminUserEditCards from './views/admin/user/edit/UserCards';
import AdminUserEditComments from './views/admin/user/edit/UserComments';

// ADMIN - GROUPS
import AdminGroupList from './views/admin/group/List';
import AdminGroupCreate from './views/admin/group/Create';
import AdminGroupEdit from './views/admin/group/Edit';
import AdminGroupEditInfo from './views/admin/group/edit/GroupInfo';
import AdminGroupEditPicture from './views/admin/group/edit/GroupPicture';
import AdminGroupEditPage from './views/admin/group/edit/GroupPage';
import AdminGroupEditProjects from './views/admin/group/edit/GroupProjects';
import AdminGroupEditExperts from './views/admin/group/edit/GroupExperts';
import AdminGroupEditModerators from './views/admin/group/edit/GroupModerators';
import AdminGroupEditMembers from './views/admin/group/edit/GroupMembers';

// ADMIN - PROJECT
import AdminProjectList from './views/admin/project/List';
import AdminProjectCreate from './views/admin/project/Create';
import AdminProjectEdit from './views/admin/project/Edit';
import AdminProjectEditInfo from './views/admin/project/edit/ProjectInfo';
import AdminProjectEditPicture from './views/admin/project/edit/ProjectPicture';
import AdminProjectEditPage from './views/admin/project/edit/ProjectPage';
import AdminProjectEditGroups from './views/admin/project/edit/ProjectGroups';
import AdminProjectEditPhases from './views/admin/project/edit/ProjectPhases';
import AdminProjectEditCards from './views/admin/project/edit/ProjectCards';

// ADMIN - CARDS
import AdminCardList from './views/admin/card/List';
import AdminCardCreate from './views/admin/card/Create';
import AdminCardEdit from './views/admin/card/Edit';
import AdminCardEditInfo from './views/admin/card/edit/CardInfo';
import AdminCardEditPicture from './views/admin/card/edit/CardPicture';
import AdminCardHistory from './views/admin/card/edit/CardHistory';

// ADMIN - COMMENTS
import AdminCommentList from './views/admin/comment/List';
import AdminCommentEdit from './views/admin/comment/Edit';
import AdminCommentEditInfo from './views/admin/comment/edit/CommentInfo';

// ADMIN - JOBS
import AdminJobList from './views/admin/job/List';

// LOGIN
import Login from './views/authentication/Login';

// ACCOUNT
import AccountCreation from './views/account/AccountCreation';
import EmailVerification from './views/account/EmailVerification';
import AccountEnrollment from './views/account/AccountEnrollment';
import LostPassword from './views/account/LostPassword';
import ResetPassword from './views/account/ResetPassword';
import Unsubscribe from './views/account/Unsubscribe';


// ERRORS
import NotFound from './views/errors/NotFound';

export default (
    <Router history={browserHistory}>
        {/* Application */}
        <Route path="/" component={App}>
            <IndexRoute component={Home}/>

            <Route path="login" component={Login}/>
            <Route path="register" component={AccountCreation}/>
            <Route path="lost-password" component={LostPassword}/>
            <Route path="unsubscribe/:token" component={Unsubscribe}/>

            {/* Legal */}
            <Route path="legal(/:document)" component={Legal}/>

            {/* Groups */}
            <Route path="group">
                <IndexRoute component={GroupList}/>
                <Route component={Protected}>
                    <Route path="create" component={GroupCreate}/>
                    <Route path=":id/edit" component={GroupEdit}/>
                </Route>
                <Route path=":id" component={Group}/>
            </Route>

            {/* Projects */}
            <Route path="project">
                <Route component={Protected}>
                    <Route path="create" component={ProjectCreate}/>
                    <Route path=":id/edit" component={ProjectEdit}/>
                </Route>
                <Route path=":id" component={ProjectWrapper}>
                    <IndexRoute component={Project}/>
                    {/* Edit has to come before the next rule so that we catch edit before a version number */}
                    <Route component={Protected}>
                        <Route path=":view/create/:cardCreationPhaseId" component={ProjectBoard}/>
                        <Route path=":view/edit/card/:editedCardId" component={ProjectBoard}/>
                        <Route path=":view/edit/phase/:editedPhaseId" component={ProjectBoard}/>
                    </Route>
                    <Route path=":view(/:cardId)(/:cardVersion)" component={ProjectBoard}/>
                </Route>
            </Route>

            {/* Cards */}
            <Route path="card">
                {/* Edit has to come before the next rule so that we catch edit before a version number */}
                <Route component={Protected}>
                    <Route path=":id/edit" component={CardEdit}/>
                </Route>
                <Route path=":id(/:cardVersion)">
                    <IndexRoute component={Card}/>
                </Route>
            </Route>

            {/* Comments */}
            <Route path="comment">
                <Route component={Protected}>
                    <Route path=":id/edit" component={CommentEdit}/>
                </Route>
            </Route>

            {/* Protected */}
            <Route component={Protected}>
                <Route path="dashboard" component={Dashboard}/>

                {/* Messages */}
                <Route path="messages(/:conversationId)" component={Messages}/>

                {/* Profile */}
                <Route path="profile/edit" component={ProfileEdit}>
                    <IndexRoute component={ProfileEditInfo}/>
                    <Route path="password" component={ProfileEditPassword}/>
                    <Route path="avatar" component={ProfileEditPicture}/>
                    <Route path="biography" component={ProfileEditBiography}/>
                    <Route path="accounts" component={ProfileEditAccounts}/>
                    <Route path="notifications" component={ProfileEditNotifications}/>
                    <Route path="code" component={ProfileEditInvitationCode}/>
                </Route>
                <Route path="profile/:id" component={Profile}/>

                {/* Settings */}
                <Route path="settings" component={Settings}>
                    <IndexRoute component={SettingsIndex}/>
                    <Route path="analytics" component={AnalyticsSettings}/>
                    <Route path="gamification" component={GamificationSettings}/>
                </Route>

                {/* Moderation */}
                <Route path="moderation" component={Moderation}>
                    <IndexRoute component={ModerationIndex}/>

                    {/* Projects */}
                    <Route path="project">
                        <IndexRoute component={ModerationProjectList}/>
                    </Route>

                    {/* Cards */}
                    <Route path="card">
                        <IndexRoute component={ModerationCardList}/>
                    </Route>

                    {/* Comments */}
                    <Route path="comment">
                        <IndexRoute component={ModerationCommentList}/>
                    </Route>

                    {/* Messages */}
                    <Route path="message">
                        <IndexRoute component={ModerationMessageList}/>
                    </Route>

                    <Route component={TopLevelModeration}>

                        {/* Users */}
                        <Route path="user">
                            <IndexRoute component={ModerationUsersList}/>
                        </Route>

                        {/* Groups */}
                        <Route path="group">
                            <IndexRoute component={ModerationGroupList}/>
                        </Route>

                    </Route>

                </Route>

                {/* Admin */}
                <Route path="admin" component={Admin}>
                    <IndexRoute component={AdminIndex}/>

                    {/* Users */}
                    <Route path="user" component={AdminUsers}>
                        <IndexRoute component={AdminUserList}/>
                        <Route path="create" component={AdminUserCreate}/>
                        <Route path=":id" component={AdminUserEdit}>
                            <IndexRoute component={AdminUserEditInfo}/>
                            <Route path="avatar" component={AdminUserEditPicture}/>
                            <Route path="profile" component={AdminUserEditProfile}/>
                            <Route path="roles" component={AdminUserEditRoles}/>
                            <Route path="cards" component={AdminUserEditCards}/>
                            <Route path="comments" component={AdminUserEditComments}/>
                        </Route>
                    </Route>

                    {/* Groups */}
                    <Route path="group">
                        <IndexRoute component={AdminGroupList}/>
                        <Route path="create" component={AdminGroupCreate}/>
                        <Route path=":id" component={AdminGroupEdit}>
                            <IndexRoute component={AdminGroupEditInfo}/>
                            <Route path="picture" component={AdminGroupEditPicture}/>
                            <Route path="page" component={AdminGroupEditPage}/>
                            <Route path="projects" component={AdminGroupEditProjects}/>
                            <Route path="experts" component={AdminGroupEditExperts}/>
                            <Route path="moderators" component={AdminGroupEditModerators}/>
                            <Route path="members" component={AdminGroupEditMembers}/>
                        </Route>
                    </Route>

                    {/* Projects */}
                    <Route path="project">
                        <IndexRoute component={AdminProjectList}/>
                        <Route path="create" component={AdminProjectCreate}/>
                        <Route path=":id" component={AdminProjectEdit}>
                            <IndexRoute component={AdminProjectEditInfo}/>
                            <Route path="picture" component={AdminProjectEditPicture}/>
                            <Route path="page" component={AdminProjectEditPage}/>
                            <Route path="groups" component={AdminProjectEditGroups}/>
                            <Route path="phases" component={AdminProjectEditPhases}/>
                            <Route path="cards" component={AdminProjectEditCards}/>
                        </Route>
                    </Route>

                    {/* Cards */}
                    <Route path="card">
                        <IndexRoute component={AdminCardList}/>
                        <Route path="create" component={AdminCardCreate}/>
                        <Route path=":id" component={AdminCardEdit}>
                            <IndexRoute component={AdminCardEditInfo}/>
                            <Route path="picture" component={AdminCardEditPicture}/>
                            <Route path="history" component={AdminCardHistory}/>
                        </Route>
                    </Route>

                    {/* Comments */}
                    <Route path="comment">
                        <IndexRoute component={AdminCommentList}/>
                        <Route path=":id" component={AdminCommentEdit}>
                            <IndexRoute component={AdminCommentEditInfo}/>
                        </Route>
                    </Route>

                    {/* Jobs */}
                    <Route path="job">
                        <IndexRoute component={AdminJobList}/>
                        {/*<Route path=":id" component={AdminJobEdit}>
                         <IndexRoute component={AdminJobEditInfo}/>
                         </Route>*/}
                    </Route>
                </Route>
            </Route>

            {/* Email Links */}
            <Route path="verify-email/:token" component={EmailVerification}/>
            <Route path="enroll-account/:token" component={AccountEnrollment}/>
            <Route path="reset-password/:token" component={ResetPassword}/>

        </Route>

        {/* Extra dynamic routes */}
        <Route path="/" getChildRoutes={loadRoutes}/>

        {/* Default, Not Found */}
        <Route path="*" component={NotFound}/>
    </Router>
);
"use strict";

import moment from "moment";

export default class DateUtil {

    static getShortDate( date, now ) {
        if ( !now ) {
            now = moment();
        } else if ( !moment.isMoment(now) ) {
            now = moment(now);
        }
        if ( !moment.isMoment(date)) {
            date = moment(date);
        }
        return date ? date.format( now.year() != date.year() ? __( 'date.medium' ) : __( 'date.short' ) ) : null
    }

    static getShortDateRange( start, end, now ) {
        if ( !now ) {
            now = moment();
        } else if ( !moment.isMoment(now) ) {
            now = moment(now);
        }
        if ( start && !moment.isMoment(start) ) {
            start = moment(start);
        }
        if ( end && !moment.isMoment(end) ) {
            end = moment(end);
        }
        if ( start && end ) {
            return __( 'From __startDate__ to __endDate__', {
                startDate: this.getShortDate( start, now ),
                endDate: this.getShortDate( end, now )
            } );
        } else if ( start ) {
            return __( 'From __date__', { date: this.getShortDate( start, now ) } );
        } else if ( end ) {
            return __( 'Until __date__', { date: this.getShortDate( end, now ) } );
        } else {
            return null;
        }
    }

}
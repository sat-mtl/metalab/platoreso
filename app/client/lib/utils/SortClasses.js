"use strict";

import React, {Component, PropTypes} from "react";
import classNames from "classnames";

/**
 * @deprecated
 */
export default class SortClasses {
    /**
     * @deprecated
     * @param field
     * @param orderBy
     * @param order
     * @returns {24}
     */
    static sortClasses(field, orderBy, order) {
        return classNames({sorted: field == orderBy, ascending: order == 1, descending: order == -1});
    }
}
"use strict";

import React, { Component, PropTypes } from "react";

export default class HiddenInput extends Component {

    constructor( props ) {
        super( props );
    }

    componentDidMount() {
        if ( this.context.refresh ) {
            this.context.refresh()
        } else {
            console.warn( 'Form input without context', this );
        }
    }

    componentWillUnmount() {
        if ( this.context.refresh ) {
            this.context.refresh()
        }
    }

    render() {
        const { name, array } = this.props;

        return (
            <input id={ ( !array && name ) ? name.replace(/\./g, '_') : null} type="hidden" {...this.props} ref="input"/>
        );
    }
}

HiddenInput.propTypes = {
    array: PropTypes.bool
};

HiddenInput.defaultProps = {
    array: false
};

HiddenInput.contextTypes = {
    refresh: PropTypes.func
};
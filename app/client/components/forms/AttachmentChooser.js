"use strict";

import ReactDOM from "react-dom";
import React, { Component, PropTypes } from "react";
import update from "react-addons-update";
import filesize from "filesize";
import { FS } from "meteor/cfs:base-package";

import HiddenInput from '../../components/forms/HiddenInput';

const { File, Utility: FileUtility } = FS;

export default class AttachmentChooser extends Component {

    constructor( props ) {
        super( props );

        this.chooseFiles  = this.chooseFiles.bind( this );
        this.filesChanged = this.filesChanged.bind( this );
        this.remove       = this.remove.bind( this );

        this.state = {
            pendingFiles: [],
            removedFiles: []
        };
    }

    chooseFiles() {
        $( ReactDOM.findDOMNode( this.refs.file ) ).trigger( 'click' );
    }

    filesChanged( e ) {
        let files = [];
        FileUtility.eachFile( e, f => {
            let file = new File( f );
            file._id = Random.id();
            files.push( file )
        } );
        const pendingFiles = this.state.pendingFiles.concat( files );
        this.setState( {
            pendingFiles: pendingFiles
        } );
        if ( this.props.filesChanged ) {
            this.props.filesChanged( pendingFiles );
        }
    }

    remove( removedFile ) {
        if ( removedFile.isUploaded() ) {
            this.setState( {
                removedFiles: update( this.state.removedFiles, {$push: [removedFile]} )
            } );
            if ( this.props.fileRemoved ) {
                this.props.fileRemoved(removedFile);
            }
        } else {
            const pendingFiles = this.state.pendingFiles.filter( file => file != removedFile );
            this.setState( {
                pendingFiles: pendingFiles
            } );

            if ( this.props.filesChanged ) {
                this.props.filesChanged( pendingFiles );
            }
        }
    }

    render() {
        const { allowMultiple, files } = this.props;
        const { pendingFiles, removedFiles } = this.state;

        // Keep only files that were not removed
        const remainingFiles = files.filter( file => removedFiles.find( removedFile => removedFile._id == file._id ) == null );
        // Concatenate non-removed files and pending files that are not already in non-removed files (upload in progress)
        const allFiles       = remainingFiles.concat(
            pendingFiles.filter( pendingFile => remainingFiles.find( remainingFiles => remainingFiles._id == pendingFile._id ) == null )
        );

        return (
            <div className="attachmentChooser">
                {/* Removed Files */}
                {/* removedFiles.map( file => (
                    <HiddenInput name="removedFiles[]"
                                 array={true}
                                 key={file._id}
                                 value={file._id}/>
                ) ) */}
                <input type="file" ref="file" accept="*/*" multiple={allowMultiple} style={{display:'none'}} onChange={this.filesChanged}/>
                <a className="ui small grey labeled icon button" onClick={this.chooseFiles}><i className="plus icon"/>{__( 'Add files' )}
                </a>

                <div className="attachments">
                    { allFiles.map( ( file, index ) => <div key={index} className="attachment">
                        <div className="fileIcon"><i className="ui file outline icon"/></div>
                        <div className="fileName">{ file.original.name }</div>
                        <div className="fileSize">{ filesize( file.original.size ) }</div>
                        <div className="fileActions">
                            <div className="ui mini red right labeled icon button" onClick={()=>this.remove(file)}>
                                <i className="remove icon"/>{__( 'Remove' )}
                            </div>
                        </div>
                    </div> ) }
                    {/*<div  key={file._id} className="item">
                     <div className="right floated content">
                     <div className="ui mini red button" onClick={()=>this.remove(file)}>{__( 'Remove' )}</div>
                     </div>
                     <i className="file outline icon"/>
                     <div className="content">
                     <div className="header">{file.original.name}</div>
                     <div className="description">{filesize( file.original.size )}</div>
                     </div>
                     </div>*/}
                </div>

            </div>
        );
    }

}

AttachmentChooser.propTypes = {
    allowMultiple: PropTypes.bool,
    fileRemoved:   PropTypes.func,
    filesChanged:  PropTypes.func,
    files:         PropTypes.arrayOf( PropTypes.instanceOf( File ) )
};

AttachmentChooser.defaultProps = {
    allowMultiple: false,
    files:         []
};
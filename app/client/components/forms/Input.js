"use strict";

import FormInput from './FormInput';

import ReactDOM from "react-dom";
import React, { Component, PropTypes } from "react";
import classNames from "classnames";

export default class Input extends FormInput {

    constructor( props ) {
        super( props );
        this.state = {};
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        super.componentDidMount();
        
        if ( this.props.autofocus ) {
            this.refs.input.focus();
        }
    }

    getValue() {
        return ReactDOM.findDOMNode( this.refs.input ).value;
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    render() {
        const { type, name, before, after, children, className, defaultValue, readOnly } = this.props;
        const { value } = this.state;

        return (
            <div className={ classNames('ui', 'input', className ) }>
                {before}
                <input ref="input"
                       id={name ? name.replace(/\./g, '_') : null}
                       name={name}
                       type={ type ? type : 'text' }
                       readOnly={readOnly}
                       value={value != undefined ? value : ( defaultValue || '' ) }
                       onChange={this.handleChange}/>
                {after}
                {children}
            </div>
        );
    }
}

Input.propTypes = _.defaults( {
    before: PropTypes.node,
    after:  PropTypes.node,
    autofocus: PropTypes.bool
}, FormInput.propTypes );

Input.defaultProps = _.defaults( {
    autofocus: false
}, FormInput.defaultProps );
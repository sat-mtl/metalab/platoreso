"use strict";

import Immutable from 'immutable';
import ReactDOM from "react-dom";
import React, { Component, PropTypes } from "react";
import classNames from "classnames";

export default class Form extends Component {

    constructor( props ) {
        super( props );

        this.register   = this.register.bind( this );
        this.unregister = this.unregister.bind( this );
        this.updateForm = this.updateForm.bind( this );

        this.fields = Immutable.Map();

        this.state = {
            fields: this.fields
        }
    }

    getChildContext() {
        return {
            error:      this.props.error,
            register:   this.register,
            unregister: this.unregister,
            refresh:    this.updateForm
        }
    }

    /**
     * Register a field's validation rules by name
     *
     * @param name
     * @param rules
     */
    register( name, rules ) {
        this.fields = this.fields.set( name, { identifier: name/*.replace(/\./g,'_')*/, rules: rules } );
        this.setState( {
            fields: this.fields
        } );
    }

    /**
     * Unregister a field's validation rules by name
     *
     * @param name
     */
    unregister( name ) {
        this.fields = this.state.fields.delete( name );
        this.setState( {
            fields: this.fields
        } );
    }

    /**
     * Update Semantic UI's form with the registered fields
     */
    updateForm() {
        // Initialize the Semantic UI form
        $( ReactDOM.findDOMNode( this.refs.form ) ).form( 'destroy' );
        $( ReactDOM.findDOMNode( this.refs.form ) ).form( {
            on:        this.props.on,
            inline:    this.props.inline,
            fields:    this.state.fields.toArray(),
            onSuccess: ( event, values ) => {
                if ( event ) {
                    // Sometimes there is no event to cancel...
                    event.preventDefault();
                }

                if ( this.props.onSuccess ) {
                    this.props.onSuccess( values );
                }
            },
            onFailure: this.props.onFailure,
            onValid:   this.props.onValid,
            onInvalid: this.props.onInvalid
        } );
    }

    componentDidMount() {
        this.updateForm();
    }

    componentDidUpdate() {
        this.updateForm();
    }

    renderError(error) {
        return <div className="ui error message">
            <div className="header">{error.title || __( 'Error' )}</div>
            { error.message && <p>{error.message}</p> }
            { error.keys && <ul>
                { _.map( error.keys, ( value, key ) => <li key={key}>{value.name}: {value.type}</li> ) }
            </ul> }
        </div>
    }

    render() {
        const { error } = this.props;
        return (
            <form ref="form" id={this.props.id} className={ classNames('ui form', this.props.className, { serverError: error != null } ) }>
                { error && this.renderError(error) }
                {this.props.children}
                { error && this.renderError(error) }
            </form>
        );
    }

}

Form.childContextTypes = {
    error:      PropTypes.object,
    register:   PropTypes.func,
    unregister: PropTypes.func,
    refresh:    PropTypes.func
};

Form.propTypes = {
    on:        PropTypes.string,
    inline:    PropTypes.bool,
    onSuccess: PropTypes.func,
    onFailure: PropTypes.func,
    onValid:   PropTypes.func,
    onInvalid: PropTypes.func
};

Form.defaultProps = {
    // Event used to trigger validation. Can be either submit, blur or change.
    on:     'blur',
    // Adds inline error on field validation error
    inline: true
};
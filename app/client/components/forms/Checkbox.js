"use strict";

import React, {Component, PropTypes} from "react";
import ReactDOM from "react-dom";
import classNames from "classnames";
import FormInput from './FormInput';

export default class CheckBox extends FormInput {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        super.componentDidMount();
        $( ReactDOM.findDOMNode( this.refs.checkbox ) ).checkbox();
    }

    componentWillUnmount() {
        $( ReactDOM.findDOMNode( this.refs.checkbox ) ).checkbox('destroy');
        super.componentWillUnmount();
    }

    getValue() {
        return $( ReactDOM.findDOMNode( this.refs.checkbox ) ).checkbox('is checked');
    }

    render() {
        let inputProps = {};
        if ( this.props.disabled ) {
            inputProps.disabled = "disabled";
        }
        return (
            <div ref="checkbox" className={ classNames('ui', 'checkbox', this.props.className ) }>
                <input id={this.props.name} name={this.props.name} type="checkbox" defaultChecked={this.props.defaultChecked}/>
                <label htmlFor={this.props.name}>{this.props.label}</label>
            </div>
        );
    }
}
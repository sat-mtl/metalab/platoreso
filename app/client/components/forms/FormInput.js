"use strict";

import React, { Component, PropTypes } from "react";

export default class FormInput extends Component {

    constructor(props) {
        super(props);
        this.getValue = this.getValue.bind(this);
    }

    getValue() {
        return null;
    }

    componentDidMount() {
        if ( this.context.register ) {
            // Register with the form
            this.context.register( this.props.id || this.props.name, this.props.rules );
        } else {
            console.warn('Form input without context', this);
        }
    }

    componentDidUpdate( prevProps, prevState ) {

    }

    componentWillUnmount() {
        if ( this.context.unregister ) {
            // Unregister with the form
            this.context.unregister( this.props.id || this.props.name );
        }
    }
}

FormInput.propTypes =  {
    id: PropTypes.string,
    name:  PropTypes.string,
    rules: PropTypes.array
};

FormInput.defaultProps = {

};

FormInput.contextTypes = {
    register: PropTypes.func,
    unregister: PropTypes.func
};
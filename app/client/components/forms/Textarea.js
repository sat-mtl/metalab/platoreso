"use strict";

import FormInput from './FormInput';

import ReactDOM from "react-dom";
import React, { Component, PropTypes } from "react";
import classNames from "classnames";
import autosize from "autosize";

export default class TextArea extends FormInput {

    constructor( props ) {
        super( props );
        this.state          = {};
        this.handleChange   = this.handleChange.bind( this );
        this.updateAutoSize = this.updateAutoSize.bind( this );
    }

    getValue() {
        return ReactDOM.findDOMNode( this.refs.textarea ).value;
    }

    componentDidMount() {
        super.componentDidMount();
        this.updateAutoSize();
    }

    componentDidUpdate( prevProps, prevState ) {
        super.componentDidUpdate( prevProps, prevState );
        if ( prevProps.autosize != this.props.autosize ) {
            this.updateAutoSize();
        } else if ( this.props.autosize ) {
            autosize.update( ReactDOM.findDOMNode( this.refs.textarea ) );
        }
    }

    handleChange( event ) {
        this.setState( { value: event.target.value } );
    }

    updateAutoSize() {
        if ( this.props.autosize ) {
            autosize( ReactDOM.findDOMNode( this.refs.textarea ) );
        } else {
            autosize.destroy( ReactDOM.findDOMNode( this.refs.textarea ) );
        }
    }

    render() {
        const { managed, name, rows = null, defaultValue } = this.props;
        const { value }    = this.state;

        const props = {};

        if ( managed ) {
            props.value = value != null ? value : ( defaultValue || '' );
            props.onChange = this.handleChange
        } else {
            props.defaultValue = defaultValue;
        }

        return (
            <textarea ref="textarea"
                      id={name ? name.replace( /\./g, '_' ) : null}
                      name={name}
                      rows={rows}
                      className={classNames( this.props.className, { autosize: this.props.autosize } )}
                      {...props}/>
        );
    }
}

TextArea.propTypes = _.extend( {
    autosize:     PropTypes.bool,
    defaultValue: PropTypes.string,
    managed:      PropTypes.bool,
    rows:         PropTypes.number
}, FormInput.propTypes );

TextArea.defaultProps = _.extend( {
    managed:  true,
    autosize: false,
    rows:     null
}, FormInput.defaultProps );
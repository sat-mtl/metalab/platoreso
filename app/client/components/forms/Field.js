"use strict";

import React, { Component, PropTypes } from "react";
import classNames from "classnames";

export default class Field extends Component {

    constructor(props) {
        super(props);
        this.findInputName = this.findInputName.bind(this);
    }

    componentDidMount() {
        this.inputName = this.findInputName(this.props.children);
    }

    render() {
        const { label } = this.props;
        const { error } = this.context;

        let errorComponent = null;
        if ( this.inputName && error && error.keys && error.keys[this.inputName] ) {
            errorComponent = <div className="ui red pointing prompt label transition visible">{error.keys[this.inputName].type}</div>;
        }

        let labelProps = {};
        if ( this.inputName ) {
            labelProps.htmlFor = this.inputName;
        }

        return (
            <div className={ classNames('field', this.props.className, { error : !!errorComponent } ) }>
                { label ? <label {...labelProps}>{label}</label> : null }
                {this.props.children}
                {errorComponent}
            </div>
        );
    }

    findInputName( children ) {
        let inputName = null;
        React.Children.forEach( children, ( child ) => {
            // We're assuming that having a name makes you an input here
            if ( child && child.props && inputName == null ) {
                if ( child.props.name ) {
                    inputName = child.props.name
                }

                if ( inputName == null && child.props.children ) {
                    inputName = this.findInputName( child.props.children );
                }
            }
        } );
        return inputName;
    }
}

Field.propTypes = {
    label: PropTypes.string
};

Field.contextTypes = {
    error: PropTypes.object
};

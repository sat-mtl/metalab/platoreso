"use strict";

import FormInput from './FormInput';

import ReactDOM from "react-dom";
import React, { Component, PropTypes } from "react";
import classNames from "classnames";

export default class Select extends FormInput {

    constructor( props ) {
        super( props );
        this.state = {
            currentValue: props.defaultValue
        }
    }

    getValue() {
        return ReactDOM.findDOMNode( this.refs.select ).value;
    }

    componentDidMount() {
        super.componentDidMount();

        let config = {
            onChange: ( value ) => {
                this.setState( { currentValue: value } );
                if ( this.props.onSelectChange ) {
                    this.props.onSelectChange( value );
                }
            }
        };
        if ( this.props.search ) {
            config.fullTextSearch = true;
        }
        if ( this.props.combo ) {
            config.action         = 'combo';
            config.allowAdditions = true;
            config.hideAdditions = false;
        }
        if ( this.props.label ) {
            config.label = this.props.label;
        }
        $( ReactDOM.findDOMNode( this.refs.select ) ).dropdown( config );
    }

    componentWillReceiveProps( nextProps ) {
        const { currentValue } = this.state;
        if ( ( !currentValue && nextProps.defaultValue ) || ( this.props.defaultValue != nextProps.defaultValue ) ) {
            this.setState( { currentValue: nextProps.defaultValue } );
        }
    }

    componentDidUpdate( prevProps, prevState ) {
        super.componentDidUpdate( prevProps, prevState );
        const $el = $( ReactDOM.findDOMNode( this.refs.select ) );
        $el.dropdown( 'refresh' );
        $el.dropdown( 'set selected', this.state.currentValue );
    }

    componentWillUnmount() {
        $( ReactDOM.findDOMNode( this.refs.select ) ).dropdown( 'destroy' );
        super.componentWillUnmount();
    }

    render() {
        return (
            <select ref="select"
                    id={this.props.id || this.props.name} name={this.props.name}
                    className={ classNames( 'ui', this.props.className, { search: this.props.search }, 'selection', 'dropdown' ) }
                    defaultValue={this.props.defaultValue}
                    multiple={this.props.multiple}
                    children={this.props.children}/>
        );
    }
}

Select.propTypes = {
    multiple:       PropTypes.bool,
    search:         PropTypes.bool,
    combo:          PropTypes.bool,
    label:          PropTypes.shape( {
        transition: PropTypes.string,
        duration:   PropTypes.number,
        variation:  PropTypes.oneOfType( [
            PropTypes.string,
            PropTypes.bool
        ] )
    } ),
    onSelectChange: PropTypes.func
};

Select.defaultProps = {
    multiple: false,
    search:   false,
    combo:    false,
    label:    null
};
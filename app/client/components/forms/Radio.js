"use strict";

import React, {Component, PropTypes} from "react";
import ReactDOM from "react-dom";
import classNames from "classnames";
import FormInput from './FormInput';

export default class Radio extends FormInput {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        super.componentDidMount();
        $( ReactDOM.findDOMNode( this.refs.radio ) ).checkbox();
    }

    componentWillUnmount() {
        $( ReactDOM.findDOMNode( this.refs.radio ) ).checkbox('destroy');
        super.componentWillUnmount();
    }

    getValue() {
        return $( ReactDOM.findDOMNode( this.refs.radio ) ).checkbox('is checked');
    }

    render() {
        return (
            <div ref="radio" className={ classNames('ui', 'radio', 'checkbox', this.props.className ) }>
                <input id={this.props.name} type="radio" {...this.props}/>
                <label htmlFor={this.props.name}>{this.props.label}</label>
            </div>
        );
    }
}
"use strict";

import ReactDOM from "react-dom";
import React, { Component, PropTypes } from "react";
import { FS } from "meteor/cfs:base-package";
import ImageCollections from "/imports/images/ImageCollections";

import ImageChooser from './ImageChooser';
import Progress from '../semanticui/modules/Progress';

export default class ImageUploader extends Component {

    constructor( props ) {
        super( props );

        this.choosePicture = this.choosePicture.bind( this );
        this.filesChanged  = this.filesChanged.bind( this );

        this.state = {
            error: null
        }
    }

    choosePicture() {
        $( ReactDOM.findDOMNode( this.refs.file ) ).trigger( 'click' );
    }

    filesChanged( files ) {
        files
            .map( file => new FS.File( file ) )
            .forEach( file => {
                file.owners = [ this.props.owner ];
                this.props.collection.insert( file, ( error, fileObj ) => {
                    this.setState( { error: error } );
                } );
            } );
    }

    renderError( error ) {
        if ( !error ) {
            return null;
        }

        return (
            <div className="ui negative message">
                <div className="header">
                    {__( 'Upload Error' )}
                </div>
                <p>{error.message}</p>
            </div>
        );
    }

    render() {
        const { file, size } = this.props;
        const { error } = this.state;

        const uploaded = file && file.isUploaded();
        const stored   = file && file.hasStored( file.collectionName + '_' + size );

        const uploading = file && !uploaded;
        const loading   = uploaded && !stored;
        const preview   = uploaded && stored;

        return (
            <div className="image-uploader">
                <ImageChooser preview={false} filesChanged={this.filesChanged}/>

                { loading &&
                  <div className="ui active inline indeterminate text loader">{__( 'Loading' )}</div> }

                { uploading &&
                  <Progress className="active" progress={ file ? file.uploadProgress() : 0 } total={100} label={__('Uploading')}/> }

                { error && this.renderError( error ) }

                { preview &&
                  <div className="preview">
                      <img className="ui medium image" src={file.url({store:file.collectionName + '_' + size})}/>
                  </div> }
            </div>
        );
    }
}

ImageUploader.propTypes = {
    collection: PropTypes.instanceOf( FS.Collection ).isRequired,
    file:       PropTypes.instanceOf( FS.File ),
    owner:      PropTypes.string.isRequired,
    size:       PropTypes.oneOf( ImageCollections.stores )
};

ImageUploader.defaultProps = {
    size: 'small'
};
"use strict";

import React, {Component, PropTypes} from "react";
import ReactDOM from "react-dom";
import {FS} from "meteor/cfs:base-package";
import Image from '../../views/widgets/Image';

export default class ImageChooser extends Component {

    constructor( props ) {
        super( props );

        this.choosePicture  = this.choosePicture.bind( this );
        this.removePicture  = this.removePicture.bind( this );
        this.filesChanged   = this.filesChanged.bind( this );
        this.previewPicture = this.previewPicture.bind( this );

        this.state = {
            hasPreview:   false,
            removedImage: false
        }
    }

    choosePicture() {
        $( ReactDOM.findDOMNode( this.refs.file ) ).trigger( 'click' );
    }

    removePicture() {
        this.setState( { removedImage: true } );

        if ( this.props.filesChanged ) {
            this.props.filesChanged( null );
        }
        if ( this.props.preview ) {
            this.previewPicture( null );
        }
    }

    previewPicture( files ) {
        const preview = ReactDOM.findDOMNode( this.refs.preview );
        $( preview ).empty();

        if ( files && files.length ) {
            files.forEach( file => {
                const imageType = /^image\//;
                if ( !imageType.test( file.type ) ) {
                    return;
                }

                let img  = document.createElement( "img" );
                img.file = file;
                preview.appendChild( img );

                let reader    = new FileReader();
                reader.onload = e => img.src = e.target.result;
                reader.readAsDataURL( file );

                this.setState( { hasPreview: true } );
            } );
        } else {
            this.setState( { hasPreview: false } );
        }
    }

    filesChanged( e ) {
        let files = [];
        FS.Utility.eachFile( e, f => {
            files.push( f );
        } );

        this.setState( { removedImage: false } );

        if ( this.props.filesChanged ) {
            this.props.filesChanged( files );
        }
        if ( this.props.preview ) {
            this.previewPicture( files );
        }
    }

    render() {
        const { preview, image } = this.props;
        const { hasPreview, removedImage } = this.state;
        const something   = ( hasPreview || image ) && !removedImage;
        //const showPreview = preview && !removedImage;
        const showImage   = preview && !hasPreview && image && !removedImage;
        return (
            <div className="imageChooser">
                <input type="file" ref="file" accept="image/*" style={{display:'none'}} onChange={this.filesChanged}/>
                <div className="previewContainer">
                    { /*showPreview && */<div ref="preview" className="preview"></div> }
                    { showImage && <Image image={image} size="small" className="preview"/> }
                    { something && <div className="change" onClick={this.choosePicture}>
                        { __( 'Change Picture' ) }
                    </div> }
                </div>
                { !something
                    ? <div className="ui fluid grey labeled icon button add" onClick={this.choosePicture}>
                      <i className="plus icon"/>{__( 'Add Picture' )}</div>
                    : <div className="ui fluid grey labeled icon button remove" onClick={this.removePicture}>
                      <i className="remove icon"/>{__( 'Remove Picture' )}</div>
                }

            </div>
        );
    }
}

ImageChooser.propTypes = {
    preview:      PropTypes.bool,
    image:        PropTypes.object,
    filesChanged: PropTypes.func.isRequired
};

ImageChooser.defaultProps = {
    preview: true
};
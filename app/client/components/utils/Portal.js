"use strict";

import { DragDropContext, HTML5Backend } from 'react-dnd';

import ReactDOM from "react-dom";
import React, { Component, PropTypes } from "react";

/**
 * The portal is a component that will render it's children in a new React tree.
 * This is used when the component's DOM will be heavily modified by a jQuery plugin for example.
 * Modals are an example of things that go into a portal so that they can be moved around the DOM
 * without React going ape-shit on us.
 */
export default class Portal extends Component {

    constructor(props) {
        super(props);

        this.state = {active: false};

        this.openPortal = this.openPortal.bind(this);
        this.closePortal = this.closePortal.bind(this);
        this.renderPortal = this.renderPortal.bind(this);

        this.portal = null;
        this.node = null;
    }

    componentWillMount() {
        this.openPortal(this.props);
    }

    componentWillReceiveProps(newProps) {
        if (this.state.active) {
            this.renderPortal(newProps);
        } else {
            this.openPortal(newProps);
        }
    }

    componentWillUnmount() {
        this.closePortal();
    }

    openPortal(props) {
        this.setState({active: true});
        this.renderPortal(props);
    }

    closePortal() {
        if (this.node) {
            ReactDOM.unmountComponentAtNode(this.node);
            $(this.node).remove();
        }
        this.portal = null;
        this.node = null;
        this.setState({active: false});
    }

    renderPortal(props) {
        if (!this.node) {
            this.node = document.createElement('div');
            if ( this.props.id ) {
                this.node.id = this.props.id;
            }
            $(this.node).addClass('portal');
            $(this.node).addClass(this.props.className);
            document.body.appendChild(this.node);
        }
        if ( props.children ) {
            const component = ReactDOM.render(
                <ContextWrapper context={this.context}>
                    {props.children}
                </ContextWrapper>,
                this.node
            );
        }
    }

    render() {
        return null;
    }
}

Portal.contextTypes = {
    dragDropManager: PropTypes.object,
    router: PropTypes.object
};

Portal.propTypes = {
    children: PropTypes.node.isRequired
};

/**
 * Context Wrapper
 *
 * This is done because the portal creates a new React tree unaware of the previous context.
 *
 * Drag & Drop
 * This portal component will take the parent's dragDropManager context as a prop and pass it down it's children.
 * This is done because there can only be one DragDropManager in the application.
 */

class ContextWrapper extends Component {
    getChildContext() {
        return this.props.context;
    }
    render() {
        return <div>{this.props.children}</div>;
    }
}

ContextWrapper.propTypes = {
    context: PropTypes.object.isRequired
};

ContextWrapper.childContextTypes = {
    dragDropManager: PropTypes.object,
    router: PropTypes.object
};
"use strict";

import ReactDOM from "react-dom";
import React, {Component, PropTypes} from "react";
import Ps from "perfect-scrollbar";
import classNames from "classnames";
import PureRenderMixin from "react-addons-pure-render-mixin";
const INTERVAL = 125;

function is_touch_device() {
    return (('ontouchstart' in window)
    || (navigator.MaxTouchPoints > 0)
    || (navigator.msMaxTouchPoints > 0));
}

export default class Scrollable extends Component {

    constructor( props ) {
        super( props );

        this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind( this );

        this.atTop    = props.fixTop;
        this.atBottom = props.fixBottom;

        this.scrollHeight = 0;
        this.scrollTop    = 0;

        this.scrollWhereNeeded  = this.scrollWhereNeeded.bind( this );
        this.onScroll           = this.onScroll.bind( this );
        this.updateScrollValues = this.updateScrollValues.bind( this );
        this.scrollWhereYouWere = this.scrollWhereYouWere.bind( this );
        this.scrollToBottom     = this.scrollToBottom.bind( this );

        this.state = {
            touch: is_touch_device()
        }
    }

    componentDidMount() {
        if ( !this.state.touch ) {
            Ps.initialize( ReactDOM.findDOMNode( this.refs.scrollable ), {
                suppressScrollX: this.props.axis == 'y',
                suppressScrollY: this.props.axis == 'x'
            } );
        }

        ReactDOM.findDOMNode( this.refs.scrollable ).addEventListener( 'resize', this.scrollWhereNeeded );

        this.updateScrollValues();
    }

    componentWillUpdate( nextProps, nextState ) {
        this.updateScrollValues();
    }

    componentDidUpdate( prevProps, prevState ) {

        // Auto scroll
        if ( !this.state.touch ) {
            if ( prevProps.scrollVelocity != this.props.scrollVelocity ) {
                if ( this.props.scrollVelocity && !this.scrollInterval ) {
                    this.scrollInterval = setInterval( this.scroll.bind( this ), INTERVAL );
                } else if ( !this.props.scrollVelocity && this.scrollInterval ) {
                    clearInterval( this.scrollInterval );
                    this.scrollInterval = null;
                }
            }
        }

        this.scrollWhereNeeded();
    }

    componentWillUnmount() {
        ReactDOM.findDOMNode( this.refs.scrollable ).removeEventListener( 'resize', this.scrollWhereNeeded );

        if ( !this.state.touch ) {
            Ps.destroy( ReactDOM.findDOMNode( this.refs.scrollable ) );
        }
    }

    scrollWhereNeeded() {
        const { fixTop, fixBottom } = this.props;

        if ( fixBottom && this.atBottom ) {
            // Stay at bottom
            this.scrollToBottom();

        } else if ( fixTop && this.atTop ) {
            // Stay where we were
            this.scrollWhereYouWere();

        } else {
            this.updateScrollValues();
        }
    }

    scroll() {
        const el = ReactDOM.findDOMNode( this.refs.scrollable );

        el.scrollTop += this.props.scrollVelocity * this.props.scrollSpeed;

        if ( !this.state.touch ) {
            Ps.update( el );
        }
    }

    updateScrollValues() {
        const el = ReactDOM.findDOMNode( this.refs.scrollable );

        this.scrollHeight = el.scrollHeight;
        this.scrollTop    = el.scrollTop;

        this.atTop    = el.scrollTop == 0;
        this.atBottom = el.scrollTop + el.offsetHeight >= el.scrollHeight;
    }

    scrollWhereYouWere() {
        const el = ReactDOM.findDOMNode( this.refs.scrollable );

        el.scrollTop = this.scrollTop + (el.scrollHeight - this.scrollHeight);
        this.updateScrollValues();

        if ( !this.state.touch ) {
            Ps.update( el );
        }
    }

    scrollToBottom() {
        const el = ReactDOM.findDOMNode( this.refs.scrollable );

        // Scroll to bottom
        el.scrollTop = el.scrollHeight;
        this.updateScrollValues();

        if ( !this.state.touch ) {
            Ps.update( el );
        }
    }

    onScroll() {
        const wasAtTop    = this.atTop;
        const wasAtBottom = this.atBottom;

        this.updateScrollValues();

        if ( !wasAtTop && this.atTop && this.props.topReached ) {
            this.props.topReached();
        }

        if ( !wasAtBottom && this.atBottom && this.props.bottomReached ) {
            this.props.bottomReached();
        }

        if ( this.props.onScroll ) {
            this.props.onScroll( {
                atTop:    this.atTop,
                atBottom: this.atBottom
            } );
        }
    }

    render() {
        return (
            <div ref="scrollable" className={classNames( "scrollable", this.props.className )} onScroll={this.onScroll}>
                <div ref="container" className="scrollableContainer">
                    {this.props.children}
                </div>
            </div>
        );
    }
}

Scrollable.propTypes = {
    axis:           PropTypes.oneOf( ['x', 'y', 'yx'] ),
    scrollVelocity: PropTypes.number,
    scrollSpeed:    PropTypes.number,
    fixTop:         PropTypes.bool,
    fixBottom:      PropTypes.bool,
    onScroll:       PropTypes.func,
    topReached:     PropTypes.func,
    bottomReached:  PropTypes.func
};

Scrollable.defaultProps = {
    axis:           'y',
    scrollVelocity: 0,
    scrollSpeed:    128,
    fixTop:         false,
    fixBottom:      false
};
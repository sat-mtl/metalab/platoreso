"use strict";

import React, {Component, PropTypes} from "react";
import FormInput from '../forms/FormInput';

export default class TimeMCE extends FormInput {

    componentDidMount() {
        super.componentDidMount();
        tinymce.init({
            selector: 'textarea#' + this.props.name,
            plugins: 'autolink link table fullscreen',
            height: 400
        });
    }

    componentWillUnmount() {
        super.componentWillUnmount();
        tinymce.get(this.props.name).remove();
    }


    getValue() {
        return tinyMCE.get(this.props.name).getContent();
    }

    render() {
        return (
            <textarea id={this.props.name} {...this.props} ref="tinymce"/>
        );
    }

}
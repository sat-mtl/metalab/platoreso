"use strict";

import React, { Component, PropTypes } from "react";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";
import classNames from "classnames";

export default class Dialog extends Component {

    constructor( props ) {
        super( props );
        this.close                  = this.close.bind( this );
        this.onBackgroundClickClose = this.onBackgroundClickClose.bind( this );
    }

    close() {
        if ( this.props.onRequestClose ) {
            this.props.onRequestClose();
        }
    }

    onBackgroundClickClose( event ) {
        if ( event.target == event.currentTarget ) {
            this.close();
        }
    }

    render() {
        return (
            <ReactCSSTransitionGroup transitionName="dialog"
                                transitionAppear={true}
                                transitionAppearTimeout={250}
                                transitionEnterTimeout={250}
                                transitionLeaveTimeout={250}
                                component="div"
                                className={ classNames("dialog", this.props.className ) }
                                onClick={this.onBackgroundClickClose}>
                { this.props.children }
            </ReactCSSTransitionGroup>
        );
    }
}

Dialog.propTypes = {
    onRequestClose: PropTypes.func
};

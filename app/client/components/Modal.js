"use strict";

import Portal from './utils/Portal';
import Mousetrap from "mousetrap";
import React, { Component, PropTypes } from "react";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";
import classNames from "classnames";

export default class Modal extends Component {

    constructor( props ) {
        super( props );
        this.close                  = this.close.bind( this );
        this.onEscapeClose          = this.onEscapeClose.bind( this );
        this.onBackgroundClickClose = this.onBackgroundClickClose.bind( this );
    }

    componentDidMount() {
        Mousetrap.bind( ['esc'], this.close );
    }

    componentWillUnmount() {
        Mousetrap.unbind( ['esc'], this.close );
    }

    close() {
        if ( this.props.onRequestClose ) {
            this.props.onRequestClose();
        }
    }

    onEscapeClose() {
        this.close();
    }

    onBackgroundClickClose( event ) {
        if ( event.target == event.currentTarget ) {
            this.close();
        }
    }

    render() {
        const { opened } = this.props;
        return (
            <Portal>
                <ReactCSSTransitionGroup transitionName="modal"
                                    transitionAppear={true}
                                    transitionAppearTimeout={250}
                                    transitionEnterTimeout={250}
                                    transitionLeaveTimeout={250}
                                    component="div">
                    { opened &&
                        <div key="modal" className={ classNames("modal", this.props.className ) } onClick={this.onBackgroundClickClose}>
                            { this.props.children }
                            { this.props.onRequestClose && <div key="close" className="close" onClick={this.props.onRequestClose}><i className="remove icon"/></div> }
                        </div>
                    }
                </ReactCSSTransitionGroup>
            </Portal>
        );
    }
}

Modal.propTypes = {
    opened:         PropTypes.bool,
    onRequestClose: PropTypes.func
};

Modal.defaultProps = {
    opened: true
};
"use strict";

import Chart from 'chart.js';
import ReactDOM from "react-dom";
import React, { Component } from 'react';

export default class ChartComponent extends Component {

    constructor(props) {
        super(props);

        // It has to stay out of props/state since
        this.config = { data: props.data };
    }

    componentDidMount() {
        const el = ReactDOM.findDOMNode(this);
        const ctx = el.getContext('2d');
        this.config.type = this.props.type;
        this.chart = new Chart(ctx, this.config);
        window.chart = this.chart;
    }

    componentWillReceiveProps(nextProps) {
        this.chart.config.data.labels = nextProps.data.labels;
        this.chart.config.data.datasets[0] = nextProps.data.datasets[0];
        this.chart && this.chart.update();
    }

    componentWillUnmount() {
        this.chart && this.chart.destroy();
    };

    render() {
        const { type, data, options, ...props } = this.props;
        return <canvas ref="canvas" {...props}/>;
    }
}
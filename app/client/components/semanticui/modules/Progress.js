"use strict";

import React, {Component, PropTypes} from "react";
import classNames from "classnames";

export default class Progress extends Component {

    render() {
        const { showProgress, progress, total, showLabel, label, autoState } = this.props;

        return (
            <div className={ classNames('ui', 'progress', this.props.className, { success: autoState && progress == total }) }
                 data-percent={progress}
                 data-total={total}>
                <div className="bar" style={ {
                    'transitionDuration': '300ms',
                    'WebkitTransitionDuration': '300ms',
                    'width': progress.toString() + '%'
                 } }>
                    { showProgress && <div className="progress">{progress.toString()}%</div> }
                </div>
                { showLabel && <div className="label">{label}</div> }
            </div>
        );
    }
}

Progress.propTypes = {
    progress:     PropTypes.number,
    total:        PropTypes.number,
    label:        PropTypes.string,
    showProgress: PropTypes.bool,
    showLabel:    PropTypes.bool,
    autoState:    PropTypes.bool
};

Progress.defaultProps = {
    progress:     0,
    total:        1,
    label:        null,
    showProgress: true,
    showLabel:    true,
    autoState:    true
};
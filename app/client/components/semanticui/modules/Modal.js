"use strict";

import Portal from '../../utils/Portal';

import React, { Component, PropTypes } from "react";
import classNames from "classnames";

export default class Modal extends Component {

    constructor( props ) {
        super( props );

        this.modal         = this.modal.bind( this );
        this.onModalHidden = this.onModalHidden.bind( this );

        this.state = {
            id:        Random.id(),
            renderDOM: props.opened
        };
    }

    componentDidMount() {
        this.modal( this.props.opened );
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.opened ) {
            this.setState( { renderDOM: true } );
        }
    }

    componentDidUpdate( prevProps, prevState ) {
        if ( prevProps.opened != this.props.opened ) {
            this.modal( this.props.opened );

        } else if ( this.props.opened ) {
            $( '#modal-' + this.state.id ).modal( 'refresh' );
        }
    }

    componentWillUnmount() {
        if ( this.props.opened ) {
            $( '#modal-' + this.state.id ).modal( 'hide' ).modal( 'destroy' );
        }
    }

    modal( show = true ) {
        // Cannot use ReactDom.findDOMNode() because of the portal
        $( '#modal-' + this.state.id ).modal( {
            //detachable: false,
            allowMultiple:  true,
            observeChanges: true,
            onShow:         this.props.onShow,
            onHide:         this.props.onHide,
            onHidden:       this.onModalHidden,
            onApprove:      this.props.onApprove,
            onDeny:         this.props.onDeny,
            selector:       {
                close: '.close' //Default modal has the dang '> .close' that we hate
            }
        } ).modal( show ? 'show' : 'hide' );
    }

    onModalHidden() {
        this.setState( { renderDOM: false } );

        if ( this.props.onHidden ) {
            this.props.onHidden();
        }
    }

    render() {
        return this.props.opened || this.state.renderDOM ? (
            <Portal ref="modal" id={"modal-"  + this.state.id} className={classNames('ui','modal', this.props.className)}>
                {this.props.children}
            </Portal>
        ) : null;
    }
}

Modal.propTypes = {
    opened:    PropTypes.bool.isRequired,
    onApprove: PropTypes.func,
    onDeny:    PropTypes.func,
    onShow:    PropTypes.func,
    onHide:    PropTypes.func,
    onHidden:  PropTypes.func
};

Modal.defaultProps = {
    opened: false
};
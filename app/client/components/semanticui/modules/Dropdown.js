"use strict";

import ReactDOM from "react-dom";
import React, { Component, PropTypes } from "react";
import classNames from "classnames";

export default class DropDown extends Component {

    componentDidMount() {
        $( ReactDOM.findDOMNode( this.refs.dropdown ) ).dropdown({
            className: {
                // We manage the active state with React,
                // semantic-ui only keeps one active item by default
                active: 'lastSelected'
            }
        });
    }

    componentWillUnmount() {
        $( ReactDOM.findDOMNode( this.refs.dropdown ) ).dropdown('destroy');
    }

    render() {
        return (
            <div ref="dropdown" className={ classNames( 'ui', 'dropdown', this.props.className ) }>
                {this.props.children}
            </div>
        );
    }
}
# PlatoReso

[![build status](https://gitlab.com/sat-metalab/platoreso/badges/master/build.svg)](https://gitlab.com/sat-metalab/platoreso/commits/master)

## Requirements (if using locally)
Image Manipulation Requirements

    $ sudo apt-get install graphicsmagick libcairo2-dev libjpeg8-dev libpango1.0-dev libgif-dev build-essential g++

## Launching in local dev

    cd app
    meteor --settings private/settings-local.json
    
or

    ./platoreso.sh
    
## Testing

    cd app
    npm test

## Migrations
When developing it is sometimes useful to rerun migrations 
and/or reset them when it crashes. Here are some useful commands
that can be used in the `meteor shell`:

- To go to a specific migration version:

        Migrations.migrateTo(__MIGRATION__)
        
- To rerun a specific migration

        Migrations.migrateTo('__MIGRATION__,rerun')
        
- When control is locked because a migration did not finish (and you are sure you won't destroy the production database), you can unlock it with:

        Migrations._setControl({version:__MIGRATION__, locked: false})

## Translations
Install `i18next-parser` globally.

    sudo npm install i18next-parser -g

### Updating strings

    cd app
    npm run i18n
    
Then Fill new strings in `app/i18n/fr/i18n.json`

## CI

The runner must be configured to use `docker:bind` and `overlay` the instructions are at https://docs.gitlab.com/ee/ci/docker/using_docker_build.html

### Test locally
- Install https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/blob/master/docs/install/bleeding-edge.md
- Run `gitlab-runner exec docker --cache-dir /cache --docker-volumes `pwd`/build-cache:/cache tests`

## Mailer

## Deployment Checklist

- Translations

## Deployment Considerations

### DB Watching and maintenance
Should probably run a separate container for those tasks

### Twitter stream
Only one machine can monitor the stream at the time
    
## Monkey Patches

* alanning:roles
    Patched in pr-base/monkey/roles.js to remove empty groups from users

# Social network apps
For those with access, api keys and secrets can be managed at these locations:
 
https://developers.facebook.com/apps/
https://apps.twitter.com/
https://console.developers.google.com/apis/dashboard
https://www.linkedin.com/developer/apps

# Useful commands

## Installing mongo org tools

    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
    echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
    sudo apt-get update
    sudo apt-get install -y mongodb-org

## Check licenses

    sudo npm install -g license-checker
    cd app
    license-checker | grep "licenses:" | sed -E 's/.*licenses: (.*)/\1/' | sort | uniq -c

























# Docker

## Building & Deploying

### Build the images
Before building the image, make sure all your changes are committed to the repository. 
The build script will create a tag in the git repository with the name provided (unless skipped with -s)

    ./docker-build.sh -t TAG_NAME
    
The options for this command are:
    - t: to specify the name of the tag to create in git and in docker.
    - s: to skip git status verification and tag committing (useful when only testing a build locally)

### Deploy to server
Use the following command to deploy the docker image to the server. This will not run the new image in any environment, it will only upload to the server and load the new image file into docker.

    ./docker-deploy.sh -t TAG_NAME
    
The options for this command are:
    - t: to specify the name of the tag to create in git and in docker.

### Run on server
On the server, clone the following git repository:

    git clone git@code.sat.qc.ca:resonance/platoreso.git
    
Then continue following instructions from that repo's README file.
     
## Test machine

Build

    docker build -f Dockerfile-dev -t metalab/platoreso:dev .
    
Run

    docker run -v /Users/ubald/Workspace/platoreso/data/mongo/db:/data/db:rw mongo:latest
    docker run -ti -v /Users/ubald/Workspace/platoreso/app:/opt/platoreso:rw --link MONGO_CONTAINER metalab/platoreso:dev

    MONGO_URL=mongodb://MONGO_CONTAINER ./platoreso.sh --port 80
    
Get machine ip (mac)

    docker-machine ip default
    
Prod machine (manual)

    docker run -ti --link drunk_cray -p 80:80 -e MONGO_URL=mongodb://drunk_cray metalab/platoreso:prod
    

    
